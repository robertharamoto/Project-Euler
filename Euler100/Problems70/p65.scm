#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 65                                     ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated July 21, 2022                                ###
;;;###                                                       ###
;;;###  updated March 6, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### srfi-1 for fold function
(use-modules ((srfi srfi-1)
              :renamer (symbol-prefix-proc 'srfi-1:)))

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### digits-module for split-digits-list function
(use-modules ((digits-module)
              :renamer (symbol-prefix-proc 'digits-module:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (factorial nn)
  (begin
    (cond
     ((< nn 0)
      (begin
        #f
        ))
     ((= nn 0)
      (begin
        1
        ))
     ((= nn 1)
      (begin
        1
        ))
     ((= nn 2)
      (begin
        2
        ))
     (else
      (begin
        (let ((result 1))
          (begin
            (do ((ii 2 (1+ ii)))
                ((> ii nn))
              (begin
                (set! result (* result ii))
                ))

            result
            ))
        )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-factorial-1 result-hash-table)
 (begin
   (let ((sub-name "test-factorial-1")
         (test-list
          (list
           (list 0 1) (list 1 1) (list 2 2)
           (list 3 6) (list 4 24) (list 5 120)
           (list 6 720)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-num (list-ref alist 0))
                  (shouldbe-num (list-ref alist 1)))
              (let ((result-num (factorial test-num)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : test-num=~a, "
                        sub-name test-label-index test-num))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe-num result-num)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-num result-num)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; need e as a rational number, use the series expansion
(define (calc-rational-e max-iterations)
  (begin
    (let ((sum 1))
      (begin
        (do ((ii 1 (+ ii 1)))
            ((> ii max-iterations))
          (begin
            (let ((fact (factorial ii)))
              (begin
                (set! sum (+ sum (/ 1 fact)))
                ))
            ))
        sum
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-calc-rational-e-1 result-hash-table)
 (begin
   (let ((sub-name "test-calc-rational-e-1")
         (test-list
          (list
           (list 0 1) (list 1 2) (list 2 (/ 5 2))
           (list 3 (/ 8 3))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-num (list-ref alist 0))
                  (shouldbe-num (list-ref alist 1)))
              (let ((result-num
                     (calc-rational-e test-num)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : test-num=~a, "
                        sub-name test-label-index test-num))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe-num result-num)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-num result-num)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (+ test-label-index 1))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (compute-nth-expansion nn rational-e)
  (begin
    (let ((sum 0)
          (local-num rational-e)
          (local-digit-list (list)))
      (begin
        (do ((ii 0 (+ ii 1)))
            ((> ii nn))
          (begin
            (let ((first-digit
                   (inexact->exact
                    (truncate
                     (exact->inexact local-num)))))
              (let ((drest (- local-num first-digit)))
                (begin
                  (set!
                   local-digit-list
                   (cons first-digit local-digit-list))

                  (set! local-num (/ 1 drest))
                  )))
            ))

        (let ((dlen (- (length local-digit-list) 1))
              (rational-num 0))
          (begin
            (do ((jj 0 (+ jj 1)))
                ((>= jj dlen))
              (begin
                (let ((this-num
                       (list-ref local-digit-list jj)))
                  (begin
                    (set!
                     rational-num
                     (/ 1 (+ rational-num this-num)))
                    ))
                ))
            (set!
             rational-num
             (+ rational-num (car (last-pair local-digit-list))))

            rational-num
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-compute-nth-expansion-1 result-hash-table)
 (begin
   (let ((sub-name "test-compute-nth-expansion-1")
         (test-list
          (list
           (list 0 10 2) (list 1 10 3)
           (list 2 20 (/ 8 3)) (list 3 20 (/ 11 4))
           (list 4 20 (/ 19 7)) (list 5 20 (/ 87 32))
           (list 6 20 (/ 106 39)) (list 7 20 (/ 193 71))
           (list 8 20 (/ 1264 465)) (list 9 20 (/ 1457 536))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((index (list-ref alist 0))
                  (max-iter (list-ref alist 1))
                  (shouldbe (list-ref alist 2)))
              (let ((rat-e (calc-rational-e max-iter)))
                (let ((result (compute-nth-expansion index rat-e)))
                  (let ((err-1
                         (format
                          #f "~a : error (~a) : "
                          sub-name test-label-index))
                        (err-2
                         (format
                          #f "index=~a, max-iter=~a, "
                          index max-iter))
                        (err-3
                         (format
                          #f "shouldbe=~a, result=~a"
                          shouldbe result)))
                    (begin
                      (unittest2:assert?
                       (equal? shouldbe result)
                       sub-name
                       (string-append
                        err-1 err-2 err-3)
                       result-hash-table)
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop max-num max-decimals debug-flag)
  (begin
    (let ((rational-e
           (calc-rational-e max-decimals))
          (digits-sum 0))
      (begin
        (if (equal? debug-flag #t)
            (begin
              (display
               (ice-9-format:format
                #f "the first ~:d terms in the sequence of "
                max-num))
              (display
               (ice-9-format:format
                #f "convergents for e are:~%"))

              (do ((ii 0 (+ ii 1)))
                  ((>= ii max-num))
                (begin
                  (let ((rat-term
                         (compute-nth-expansion ii rational-e)))
                    (begin
                      (if (= ii 0)
                          (begin
                            (display (format #f "~a" rat-term)))
                          (begin
                            (display (format #f ", ~a" rat-term))
                            ))
                      ))
                  ))
              (newline)
              ))

        (let ((this-term
               (compute-nth-expansion (- max-num 1) rational-e)))
          (let ((numer
                 (numerator this-term)))
            (let ((dlist
                   (digits-module:split-digits-list numer)))
              (let ((dsum (srfi-1:fold + 0 dlist)))
                (let ((dstring
                       (string-join
                        (map
                         (lambda (this-num)
                           (begin
                             (format #f "~a" this-num)
                             )) dlist)
                        "+")))
                  (begin
                    (display
                     (ice-9-format:format
                      #f "the sum of digits in the numerator of the ~:dth~%"
                      max-num))
                    (display
                     (ice-9-format:format
                      #f "convergent is ~a=~:d.~%"
                      dstring dsum))
                    (force-output)
                    ))
                ))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "The square root of 2 can be written "))
    (display
     (format #f "as an infinite~%"))
    (display
     (format #f "continued fraction.~%"))
    (newline)
    (display
     (format #f "sqrt(2) = 1+ 1/(2+1/(2+1/(2+1/(2+...))))~%"))
    (display
     (format #f "The infinite continued fraction can "))
    (display
     (format #f "be written, sqrt(2) =~%"))
    (display
     (format #f "[1;(2)], (2) indicates that 2 repeats "))
    (display
     (format #f "ad infinitum. In~%"))
    (display
     (format #f "a similar way, sqrt(23) = [4;(1,3,1,8)].~%"))
    (newline)
    (display
     (format #f "It turns out that the sequence of "))
    (display
     (format #f "partial values of~%"))
    (display
     (format #f "continued fractions for square roots "))
    (display
     (format #f "provide the best rational~%"))
    (display
     (format #f "approximations. Let us consider the "))
    (display
     (format #f "convergents for sqrt(2).~%"))
    (newline)
    (display
     (format #f "  1+1/2 = 3/2~%"))
    (display
     (format #f "  1+1/(2+1/2) = 7/5~%"))
    (display
     (format #f "  1+1/(2+1/(2+1/2)) = 17/12~%"))
    (display
     (format #f "  1+1/(2+1/(2+1/(2+1/2))) = 41/29~%"))
    (display
     (format #f "Hence the sequence of the first ten "))
    (display
     (format #f "convergents for 2 are:~%"))
    (newline)
    (display
     (format #f "What is most surprising is that the "))
    (display
     (format #f "important mathematical constant,~%"))
    (display
     (format #f "e = [2; 1,2,1, 1,4,1, 1,6,1 , ... , 1,2k,1, ...].~%"))
    (display
     (format #f "The first ten terms in the sequence of "))
    (display
     (format #f "convergents for e are:~%"))
    (display
     (format #f "2, 3, 8/3, 11/4, 19/7, 87/32, 106/39, "))
    (display
     (format #f "193/71, 1264/465, 1457/536, ...~%"))
    (display
     (format #f "The sum of digits in the numerator of "))
    (display
     (format #f "the 10th convergent is~%"))
    (display
     (format #f "1+4+5+7=17.~%"))
    (newline)
    (display
     (format #f "Find the sum of digits in the numerator "))
    (display
     (format #f "of the 100th convergent~%"))
    (display
     (format #f "of the continued fraction for e.~%"))
    (display
     (format #f "see https://projecteuler.net/problem=65~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((max-num 10)
          (max-decimals 20)
          (debug-flag #t))
      (begin
        (sub-main-loop max-num max-decimals debug-flag)
        ))

    (newline)
    (let ((max-num 100)
          (max-decimals 100)
          (debug-flag #f))
      (begin
        (sub-main-loop max-num max-decimals debug-flag)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 65 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (load "digits-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

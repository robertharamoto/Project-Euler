#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 68                                     ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated July 21, 2022                                ###
;;;###                                                       ###
;;;###  updated March 6, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (list-to-string llist)
  (begin
    (let ((stmp
           (string-join
            (map
             number->string llist)
            "")))
      (begin
        stmp
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-list-to-string-1 result-hash-table)
 (begin
   (let ((sub-name "test-list-to-string-1")
         (test-list
          (list
           (list (list 1) "1")
           (list (list 1 2) "12")
           (list (list 3 1) "31")
           (list (list 1 2 3) "123")
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((input-list (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result (list-to-string input-list)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : input-list=~a, "
                        sub-name test-label-index input-list))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; index labels for 3-gon
;;;  0
;;;   1
;;;  2 3 4
;;; 5
(define (three-array-to-list three-gon-array)
  (begin
    (let ((result-list
           (list (array-ref three-gon-array 0)
                 (array-ref three-gon-array 1)
                 (array-ref three-gon-array 3)
                 (array-ref three-gon-array 4)
                 (array-ref three-gon-array 3)
                 (array-ref three-gon-array 2)
                 (array-ref three-gon-array 5)
                 (array-ref three-gon-array 2)
                 (array-ref three-gon-array 1)
                 )))
      (begin
        result-list
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-three-array-to-list-1 result-hash-table)
 (begin
   (let ((sub-name "test-three-array-to-list-1")
         (test-list
          (list
           (list (list 4 3 1 2 6 5)
                 (list 4 3 2 6 2 1 5 1 3))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((input-list (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((tarray (list->array 1 input-list)))
                (let ((result (three-array-to-list tarray)))
                  (let ((err-1
                         (format
                          #f "~a : error (~a) : input-list=~a, "
                          sub-name test-label-index input-list))
                        (err-2
                         (format
                          #f "shouldbe=~a, result=~a"
                          shouldbe result)))
                    (begin
                      (unittest2:assert?
                       (equal? shouldbe result)
                       sub-name
                       (string-append err-1 err-2)
                       result-hash-table)
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; index labels for 3-gon
;;;  0
;;;   1
;;;  2 3 4
;;; 5
(define (three-array-to-string three-gon-array)
  (begin
    (let ((result-string
           (ice-9-format:format
            #f "~a,~a,~a; ~a,~a,~a; ~a,~a,~a"
            (array-ref three-gon-array 0)
            (array-ref three-gon-array 1)
            (array-ref three-gon-array 3)
            (array-ref three-gon-array 4)
            (array-ref three-gon-array 3)
            (array-ref three-gon-array 2)
            (array-ref three-gon-array 5)
            (array-ref three-gon-array 2)
            (array-ref three-gon-array 1)
            )))
      (begin
        result-string
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-three-array-to-string-1 result-hash-table)
 (begin
   (let ((sub-name "test-three-array-to-string-1")
         (test-list
          (list
           (list (list 4 3 1 2 6 5)
                 "4,3,2; 6,2,1; 5,1,3")
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((input-list (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((three-array (list->array 1 input-list)))
                (let ((result (three-array-to-string three-array)))
                  (let ((err-1
                         (format
                          #f "~a : error (~a) : input-list=~a, "
                          sub-name test-label-index input-list))
                        (err-2
                         (format
                          #f "shouldbe=~a, result=~a"
                          shouldbe result)))
                    (begin
                      (unittest2:assert?
                       (equal? shouldbe result)
                       sub-name
                       (string-append err-1 err-2)
                       result-hash-table)
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; index labels for 3-gon
;;;  0
;;;   1
;;;  2 3 4
;;; 5
(define (three-arrays-equivalent? array-1 list-2)
  (begin
    (cond
     ((and (= (array-ref array-1 0) (list-ref list-2 0))
           (= (array-ref array-1 1) (list-ref list-2 1))
           (= (array-ref array-1 2) (list-ref list-2 2))
           (= (array-ref array-1 3) (list-ref list-2 3))
           (= (array-ref array-1 4) (list-ref list-2 4))
           (= (array-ref array-1 5) (list-ref list-2 5)))
      (begin
        #t
        ))
     ((and (= (array-ref array-1 0) (list-ref list-2 4))
           (= (array-ref array-1 1) (list-ref list-2 3))
           (= (array-ref array-1 3) (list-ref list-2 2))
           (= (array-ref array-1 2) (list-ref list-2 1))
           (= (array-ref array-1 4) (list-ref list-2 5))
           (= (array-ref array-1 5) (list-ref list-2 0)))
      (begin
        #t
        ))
     ((and (= (array-ref array-1 0) (list-ref list-2 5))
           (= (array-ref array-1 1) (list-ref list-2 2))
           (= (array-ref array-1 2) (list-ref list-2 3))
           (= (array-ref array-1 3) (list-ref list-2 1))
           (= (array-ref array-1 4) (list-ref list-2 0))
           (= (array-ref array-1 5) (list-ref list-2 4)))
      (begin
        #t
        ))
     (else
      (begin
        #f
        )))
    ))

;;;#############################################################
;;;#############################################################
;;; constraint propagation method
;;; index labels for 3-gon
;;;  0
;;;   1
;;;  2 3 4
;;; 5
(define (find-max-3gon-string ndigits debug-flag)
  (define (local-calc-two-sum depth three-gon-array)
    (begin
      (cond
       ((= depth 4)
        (begin
          (let ((result
                 (+ (array-ref three-gon-array 2)
                    (array-ref three-gon-array 3))))
            (begin
              result
              ))
          ))
       ((= depth 5)
        (begin
          (let ((result
                 (+ (array-ref three-gon-array 1)
                    (array-ref three-gon-array 2))))
            (begin
              result
              ))
          ))
       (else
        (begin
          -1
          )))
      ))
  (define (local-calc-three-sum depth three-gon-array)
    (begin
      (cond
       ((= depth 3)
        (begin
          (let ((result
                 (+ (array-ref three-gon-array 0)
                    (array-ref three-gon-array 1)
                    (array-ref three-gon-array 3))))
            (begin
              result
              ))
          ))
       ((= depth 4)
        (begin
          (let ((result
                 (+ (array-ref three-gon-array 2)
                    (array-ref three-gon-array 3)
                    (array-ref three-gon-array 4))))
            (begin
              result
              ))
          ))
       ((= depth 5)
        (begin
          (let ((result
                 (+ (array-ref three-gon-array 1)
                    (array-ref three-gon-array 2)
                    (array-ref three-gon-array 5))))
            (begin
              result
              ))
          ))
       (else
        (begin
          -1
          )))
      ))
  (define (local-constraint-loop
           depth max-depth three-gon-array
           valid-set-list three-sum acc-list debug-flag)
    (begin
      (cond
       ((>= depth max-depth)
        (begin
          (let ((equiv-flag #f))
            (begin
              (for-each
               (lambda (a-list)
                 (begin
                   (if (three-arrays-equivalent?
                        three-gon-array a-list)
                       (begin
                         (set! equiv-flag #t)
                         ))
                   )) acc-list)
              (if (equal? equiv-flag #f)
                  (begin
                    (set!
                     acc-list
                     (cons (list-copy
                            (array->list three-gon-array))
                           acc-list))
                    ))
              ))
          acc-list
          ))
       ((<= depth 3)
        (begin
          (for-each
           (lambda (this-num)
             (begin
               (array-set! three-gon-array this-num depth)
               (let ((next-depth (1+ depth))
                     (next-three-sum
                      (local-calc-three-sum depth three-gon-array))
                     (next-set-list
                      (delete this-num valid-set-list)))
                 (let ((next-acc-list
                        (local-constraint-loop
                         next-depth max-depth three-gon-array
                         next-set-list next-three-sum acc-list debug-flag)))
                   (begin
                     (set! acc-list next-acc-list)
                     )))
               )) valid-set-list)

          acc-list
          ))
       ((or (= depth 4)
            (= depth 5))
        (begin
          (let ((this-sum
                 (local-calc-two-sum depth three-gon-array)))
            (let ((next-num (- three-sum this-sum))
                  (elem-0 (array-ref three-gon-array 0)))
              (let ((ltmp
                     (list-head
                      (list-copy (array->list three-gon-array))
                      depth)))
                (begin
                  (if (>= elem-0 next-num)
                      (begin
                        (set! next-num -1)
                        ))

                  (if (and
                       (not
                        (equal?
                         (member next-num valid-set-list)
                         #f))
                       (equal? (member next-num ltmp) #f))
                      (begin
                        (array-set! three-gon-array next-num depth)
                        (let ((next-depth (1+ depth))
                              (next-three-sum
                               (local-calc-three-sum
                                depth three-gon-array)))
                          (let ((next-set-list
                                 (delete next-num valid-set-list)))
                            (begin
                              (let ((next-acc-list
                                     (local-constraint-loop
                                      next-depth max-depth
                                      three-gon-array next-set-list
                                      next-three-sum acc-list debug-flag)))
                                (begin
                                  (set! acc-list next-acc-list)
                                  ))
                              )))
                        ))
                  ))
              ))
          acc-list
          )))
      ))
  (begin
    (let ((three-gon-array (make-array 0 6))
          (end-index 5)
          (max-depth 6)
          (set-list (list 1 2 3 4 5 6)))
      (let ((acc-list
             (local-constraint-loop
              0 max-depth three-gon-array
              set-list -1 (list) debug-flag)))
        (begin
          (let ((max-string ""))
            (begin
              (for-each
               (lambda (a-list)
                 (begin
                   (let ((a-array (list->array 1 a-list)))
                     (let ((cstring
                            (three-array-to-string a-array))
                           (cat-string
                            (list-to-string
                             (three-array-to-list a-array)))
                           (three-sum
                            (local-calc-three-sum 3 a-array)))
                       (begin
                         (if (equal? debug-flag #t)
                             (begin
                               (display
                                (format #f "  ~a    ~a~%"
                                        three-sum cstring))
                               (force-output)
                               ))
                         (if (= (string-length cat-string) ndigits)
                             (begin
                               (if (string-ci>? cat-string max-string)
                                   (begin
                                     (set! max-string cat-string)
                                     ))
                               ))
                         )))
                   )) (sort
                       acc-list
                       (lambda (a b)
                         (begin
                           (let ((a1 (list-ref a 1))
                                 (b1 (list-ref b 1)))
                             (let ((a-sum
                                    (+ (list-ref a 0)
                                       (list-ref a 1)
                                       (list-ref a 3)))
                                   (b-sum
                                    (+ (list-ref b 0)
                                       (list-ref b 1)
                                       (list-ref b 3))))
                               (begin
                                 (cond
                                  ((< a-sum b-sum)
                                   (begin
                                     #t
                                     ))
                                  ((> a-sum b-sum)
                                   (begin
                                     #f
                                     ))
                                  (else
                                   (begin
                                     (< a1 b1)
                                     )))
                                 )))
                           ))
                       ))
              max-string
              ))
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (main-3gon-loop ndigits debug-flag)
  (begin
    (let ((rstring
           (find-max-3gon-string ndigits debug-flag)))
      (begin
        (display
         (ice-9-format:format
          #f "Maximum magic 3-gon string = ~a, (digits = ~:d)~%"
          rstring ndigits))
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
;;; index labels for 5-gon
;;;  0
;;;    1   3
;;;  8    2
;;;9  (6 4   5)
;;;     7
(define (five-array-to-list five-gon-array)
  (begin
    (let ((result-list
           (list (array-ref five-gon-array 0)
                 (array-ref five-gon-array 1)
                 (array-ref five-gon-array 2)
                 (array-ref five-gon-array 3)
                 (array-ref five-gon-array 2)
                 (array-ref five-gon-array 4)
                 (array-ref five-gon-array 5)
                 (array-ref five-gon-array 4)
                 (array-ref five-gon-array 6)
                 (array-ref five-gon-array 7)
                 (array-ref five-gon-array 6)
                 (array-ref five-gon-array 8)
                 (array-ref five-gon-array 9)
                 (array-ref five-gon-array 8)
                 (array-ref five-gon-array 1)
                 )))
      (begin
        result-list
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-five-array-to-list-1 result-hash-table)
 (begin
   (let ((sub-name "test-five-array-to-list-1")
         (test-list
          (list
           (list (list 1 2 3 4 5 6 7 8 9 10)
                 (list 1 2 3 4 3 5 6 5 7 8 7 9 10 9 2))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((input-list (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((tarray (list->array 1 input-list)))
                (let ((result
                       (five-array-to-list tarray)))
                  (let ((err-1
                         (format
                          #f "~a : error (~a) : input-list=~a, "
                          sub-name test-label-index input-list))
                        (err-2
                         (format
                          #f "shouldbe=~a, result=~a"
                          shouldbe result)))
                    (begin
                      (unittest2:assert?
                       (equal? shouldbe result)
                       sub-name
                       (string-append err-1 err-2)
                       result-hash-table)
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; index labels for 5-gon
;;;  0
;;;    1   3
;;;  8    2
;;;9  (6 4   5)
;;;     7
(define (five-array-to-string five-gon-array)
  (begin
    (let ((result-string
           (ice-9-format:format
            #f "~a,~a,~a; ~a,~a,~a; ~a,~a,~a; ~a,~a,~a; ~a,~a,~a"
            (array-ref five-gon-array 0)
            (array-ref five-gon-array 1)
            (array-ref five-gon-array 2)
            (array-ref five-gon-array 3)
            (array-ref five-gon-array 2)
            (array-ref five-gon-array 4)
            (array-ref five-gon-array 5)
            (array-ref five-gon-array 4)
            (array-ref five-gon-array 6)
            (array-ref five-gon-array 7)
            (array-ref five-gon-array 6)
            (array-ref five-gon-array 8)
            (array-ref five-gon-array 9)
            (array-ref five-gon-array 8)
            (array-ref five-gon-array 1)
            )))
      (begin
        result-string
        ))
    ))

;;;#############################################################
;;;#############################################################
;;; index labels for 5-gon
;;;  0
;;;    1   3
;;;  8    2
;;;9  (6 4   5)
;;;     7
(unittest2:define-tests-macro
 (test-five-array-to-string-1 result-hash-table)
 (begin
   (let ((sub-name "test-five-array-to-string-1")
         (test-list
          (list
           (list (list 1 2 3 4 5 6 7 8 9 10)
                 "1,2,3; 4,3,5; 6,5,7; 8,7,9; 10,9,2")
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((input-list (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((five-array
                     (list->array 1 input-list)))
                (let ((result
                       (five-array-to-string five-array)))
                  (let ((err-1
                         (format
                          #f "~a : error (~a) : input-list=~a, "
                          sub-name test-label-index input-list))
                        (err-2
                         (format
                          #f "shouldbe=~a, result=~a"
                          shouldbe result)))
                    (begin
                      (unittest2:assert?
                       (equal? shouldbe result)
                       sub-name
                       (string-append err-1 err-2)
                       result-hash-table)
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; index labels for 5-gon
;;;  0
;;;    1   3
;;;  8    2
;;;9  (6 4   5)
;;;     7
(define (five-arrays-equivalent? array-1 list-2)
  (begin
    (cond
     ((and (= (array-ref array-1 0) (list-ref list-2 0))
           (= (array-ref array-1 1) (list-ref list-2 1))
           (= (array-ref array-1 2) (list-ref list-2 2))
           (= (array-ref array-1 3) (list-ref list-2 3))
           (= (array-ref array-1 4) (list-ref list-2 4))
           (= (array-ref array-1 5) (list-ref list-2 5))
           (= (array-ref array-1 6) (list-ref list-2 6))
           (= (array-ref array-1 7) (list-ref list-2 7))
           (= (array-ref array-1 8) (list-ref list-2 8))
           (= (array-ref array-1 9) (list-ref list-2 9)))
      (begin
        #t
        ))
     ((and (= (array-ref array-1 0) (list-ref list-2 3))
           (= (array-ref array-1 1) (list-ref list-2 2))
           (= (array-ref array-1 2) (list-ref list-2 4))
           (= (array-ref array-1 4) (list-ref list-2 6))
           (= (array-ref array-1 6) (list-ref list-2 8))
           (= (array-ref array-1 8) (list-ref list-2 1))
           (= (array-ref array-1 3) (list-ref list-2 5))
           (= (array-ref array-1 5) (list-ref list-2 7))
           (= (array-ref array-1 7) (list-ref list-2 9))
           (= (array-ref array-1 9) (list-ref list-2 0)))
      (begin
        #t
        ))
     ((and (= (array-ref array-1 0) (list-ref list-2 5))
           (= (array-ref array-1 1) (list-ref list-2 4))
           (= (array-ref array-1 2) (list-ref list-2 6))
           (= (array-ref array-1 4) (list-ref list-2 8))
           (= (array-ref array-1 6) (list-ref list-2 1))
           (= (array-ref array-1 8) (list-ref list-2 2))
           (= (array-ref array-1 3) (list-ref list-2 7))
           (= (array-ref array-1 5) (list-ref list-2 9))
           (= (array-ref array-1 7) (list-ref list-2 0))
           (= (array-ref array-1 9) (list-ref list-2 3)))
      (begin
        #t
        ))
     ((and (= (array-ref array-1 0) (list-ref list-2 7))
           (= (array-ref array-1 1) (list-ref list-2 6))
           (= (array-ref array-1 2) (list-ref list-2 8))
           (= (array-ref array-1 4) (list-ref list-2 1))
           (= (array-ref array-1 6) (list-ref list-2 2))
           (= (array-ref array-1 8) (list-ref list-2 4))
           (= (array-ref array-1 3) (list-ref list-2 9))
           (= (array-ref array-1 5) (list-ref list-2 0))
           (= (array-ref array-1 7) (list-ref list-2 3))
           (= (array-ref array-1 9) (list-ref list-2 5)))
      (begin
        #t
        ))
     ((and (= (array-ref array-1 0) (list-ref list-2 9))
           (= (array-ref array-1 1) (list-ref list-2 8))
           (= (array-ref array-1 2) (list-ref list-2 1))
           (= (array-ref array-1 4) (list-ref list-2 2))
           (= (array-ref array-1 6) (list-ref list-2 4))
           (= (array-ref array-1 8) (list-ref list-2 6))
           (= (array-ref array-1 3) (list-ref list-2 0))
           (= (array-ref array-1 5) (list-ref list-2 3))
           (= (array-ref array-1 7) (list-ref list-2 5))
           (= (array-ref array-1 9) (list-ref list-2 7)))
      (begin
        #t
        ))
     (else
      (begin
        #f
        )))
    ))

;;;#############################################################
;;;#############################################################
;;; constraint propagation method - reduces number of possibilities to examine
;;; index labels for 5-gon
;;;  0
;;;    1   3
;;;  8    2
;;;9  (6 4   5)
;;;     7
(define (find-max-5gon-string ndigits debug-flag)
  (define (local-five-calc-two-sum depth five-gon-array)
    (begin
      (cond
       ((= depth 4)
        (begin
          (let ((result
                 (+ (array-ref five-gon-array 2)
                    (array-ref five-gon-array 3))))
            (begin
              result
              ))
          ))
       ((= depth 6)
        (begin
          (let ((result
                 (+ (array-ref five-gon-array 4)
                    (array-ref five-gon-array 5))))
            (begin
              result
              ))
          ))
       ((= depth 8)
        (begin
          (let ((result
                 (+ (array-ref five-gon-array 6)
                    (array-ref five-gon-array 7))))
            (begin
              result
              ))
          ))
       ((= depth 9)
        (begin
          (let ((result
                 (+ (array-ref five-gon-array 1)
                    (array-ref five-gon-array 8))))
            (begin
              result
              ))
          ))
       (else
        (begin
          -1
          )))
      ))
  (define (local-five-calc-three-sum depth five-gon-array)
    (begin
      (cond
       ((= depth 2)
        (begin
          (let ((result
                 (+ (array-ref five-gon-array 0)
                    (array-ref five-gon-array 1)
                    (array-ref five-gon-array 2))))
            (begin
              result
              ))
          ))
       ((= depth 4)
        (begin
          (let ((result
                 (+ (array-ref five-gon-array 2)
                    (array-ref five-gon-array 3)
                    (array-ref five-gon-array 4))))
            (begin
              result
              ))
          ))
       ((= depth 6)
        (begin
          (let ((result
                 (+ (array-ref five-gon-array 4)
                    (array-ref five-gon-array 5)
                    (array-ref five-gon-array 6))))
            (begin
              result
              ))
          ))
       ((= depth 8)
        (begin
          (let ((result
                 (+ (array-ref five-gon-array 6)
                    (array-ref five-gon-array 7)
                    (array-ref five-gon-array 8))))
            (begin
              result
              ))
          ))
       ((= depth 9)
        (begin
          (let ((result
                 (+ (array-ref five-gon-array 1)
                    (array-ref five-gon-array 8)
                    (array-ref five-gon-array 9))))
            (begin
              result
              ))
          ))
       ((>= depth 2)
        (begin
          (let ((result
                 (+ (array-ref five-gon-array 0)
                    (array-ref five-gon-array 1)
                    (array-ref five-gon-array 2))))
            (begin
              result
              ))
          ))
       (else
        (begin
          -1
          )))
      ))
  (define (local-five-constraint-loop
           depth max-depth five-gon-array
           valid-set-list three-sum acc-list debug-flag)
    (begin
      (cond
       ((>= depth max-depth)
        (begin
          (let ((equiv-flag #f))
            (begin
              (for-each
               (lambda (a-list)
                 (begin
                   (if (five-arrays-equivalent?
                        five-gon-array a-list)
                       (begin
                         (set! equiv-flag #t)
                         ))
                   )) acc-list)

              (if (equal? equiv-flag #f)
                  (begin
                    (set!
                     acc-list
                     (cons
                      (list-copy
                       (array->list five-gon-array))
                      acc-list))
                    ))
              ))
          acc-list
          ))
       ((or (<= depth 3)
            (= depth 5)
            (= depth 7))
        (begin
          (for-each
           (lambda (this-num)
             (begin
               (let ((elem-0
                      (array-ref five-gon-array 0)))
                 (begin
                   (if (or
                        (<= depth 2)
                        (and (or (= depth 3)
                                 (= depth 5)
                                 (= depth 7))
                             (< elem-0 this-num)))
                       (begin
                         (array-set! five-gon-array this-num depth)
                         (let ((next-depth (1+ depth))
                               (next-three-sum
                                (local-five-calc-three-sum
                                 depth five-gon-array))
                               (next-set-list
                                (delete this-num valid-set-list)))
                           (let ((next-acc-list
                                  (local-five-constraint-loop
                                   next-depth max-depth five-gon-array
                                   next-set-list next-three-sum
                                   acc-list debug-flag)))
                             (begin
                               (set! acc-list next-acc-list)
                               )))
                         ))
                   ))
               )) valid-set-list)

          acc-list
          ))
       ((or (= depth 4)
            (= depth 6)
            (= depth 8)
            (= depth 9))
        (begin
          (let ((this-sum
                 (local-five-calc-two-sum
                  depth five-gon-array)))
            (let ((next-num (- three-sum this-sum))
                  (elem-0 (array-ref five-gon-array 0)))
              (let ((ltmp
                     (list-head
                      (list-copy
                       (array->list five-gon-array))
                      depth)))
                (begin
                  (if (and (= depth 9)
                           (>= elem-0 next-num))
                      (begin
                        (set! next-num -1)
                        ))

                  (if (and
                       (not
                        (equal?
                         (member next-num valid-set-list)
                         #f))
                       (equal?
                        (member next-num ltmp)
                        #f))
                      (begin
                        (array-set!
                         five-gon-array next-num depth)
                        (let ((next-depth (1+ depth))
                              (next-three-sum
                               (local-five-calc-three-sum
                                depth five-gon-array)))
                          (let ((next-set-list
                                 (delete next-num valid-set-list)))
                            (let ((next-acc-list
                                   (local-five-constraint-loop
                                    next-depth max-depth
                                    five-gon-array next-set-list
                                    next-three-sum acc-list debug-flag)))
                              (begin
                                (set! acc-list next-acc-list)
                                ))
                            ))
                        ))
                  ))
              ))
          acc-list
          )))
      ))
  (begin
    (let ((five-gon-array (make-array 0 10))
          (max-depth 10)
          (set-list (list 1 2 3 4 5 6 7 8 9 10)))
      (let ((acc-list
             (local-five-constraint-loop
              0 max-depth five-gon-array
              set-list -1 (list) debug-flag)))
        (begin
          (let ((max-string ""))
            (begin
              (for-each
               (lambda (a-list)
                 (begin
                   (let ((a-array (list->array 1 a-list)))
                     (let ((cstring
                            (five-array-to-string a-array))
                           (cat-string
                            (list-to-string
                             (five-array-to-list a-array)))
                           (three-sum
                            (local-five-calc-three-sum 3 a-array)))
                       (begin
                         (if (equal? debug-flag #t)
                             (begin
                               (display
                                (format
                                 #f "  ~a    ~a~%"
                                 three-sum cstring))
                               (force-output)
                               ))
                         (if (= (string-length cat-string) ndigits)
                             (begin
                               (if (string-ci>? cat-string max-string)
                                   (begin
                                     (set! max-string cat-string)
                                     ))
                               ))
                         )))
                   )) (sort
                       acc-list
                       (lambda (a b)
                         (begin
                           (let ((a1
                                  (+ (* 10 (list-ref a 0))
                                     (list-ref a 1)))
                                 (b1
                                  (+ (* 10 (list-ref b 0))
                                     (list-ref b 1))))
                             (let ((a-sum
                                    (+ (list-ref a 0)
                                       (list-ref a 1)
                                       (list-ref a 2)))
                                   (b-sum
                                    (+ (list-ref b 0)
                                       (list-ref b 1)
                                       (list-ref b 2))))
                               (begin
                                 (cond
                                  ((< a-sum b-sum)
                                   (begin
                                     #t
                                     ))
                                  ((> a-sum b-sum)
                                   (begin
                                     #f
                                     ))
                                  (else
                                   (begin
                                     (< a1 b1)
                                     )))
                                 )))
                           ))
                       ))
              max-string
              ))
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (main-5gon-loop ndigits debug-flag)
  (begin
    (let ((rstring
           (find-max-5gon-string ndigits debug-flag)))
      (begin
        (display
         (ice-9-format:format
          #f "Maximum magic 5-gon string = ~a, (digits = ~:d)~%"
          rstring ndigits))
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Consider the following 'magic' 3-gon "))
    (display
     (format #f "ring, filled with the~%"))
    (display
     (format #f "numbers 1 to 6, and each line adding "))
    (display
     (format #f "to nine.~%"))
    (newline)
    (display
     (format #f "Working clockwise, and starting from "))
    (display
     (format #f "the group of three~%"))
    (display
     (format #f "with the numerically lowest external "))
    (display
     (format #f "node (4,3,2 in this~%"))
    (display
     (format #f "example), each solution can be described "))
    (display
     (format #f "uniquely. For example,~%"))
    (display
     (format #f "the above solution can be described by "))
    (display
     (format #f "the set:~%"))
    (display
     (format #f "4,3,2; 6,2,1; 5,1,3.~%"))
    (newline)
    (display
     (format #f "It is possible to complete the ring "))
    (display
     (format #f "with four different totals:~%"))
    (display
     (format #f "9, 10, 11, and 12. There are eight "))
    (display
     (format #f "solutions in total.~%"))
    (display
     (format #f "Total    Solution Set~%"))
    (display
     (format #f "  9      4,2,3; 5,3,1; 6,1,2~%"))
    (display
     (format #f "  9      4,3,2; 6,2,1; 5,1,3~%"))
    (display
     (format #f " 10      2,3,5; 4,5,1; 6,1,3~%"))
    (display
     (format #f " 10      2,5,3; 6,3,1; 4,1,5~%"))
    (display
     (format #f " 11      1,4,6; 3,6,2; 5,2,4~%"))
    (display
     (format #f " 11      1,6,4; 5,4,2; 3,2,6~%"))
    (display
     (format #f " 12      1,5,6; 2,6,4; 3,4,5~%"))
    (display
     (format #f " 12      1,6,5; 3,5,4; 2,4,6~%"))
    (newline)
    (display
     (format #f "By concatenating each group it is "))
    (display
     (format #f "possible to form~%"))
    (display
     (format #f "9-digit strings; the maximum string "))
    (display
     (format #f "for a 3-gon ring~%"))
    (display
     (format #f "is 432621513.~%"))
    (newline)
    (display
     (format #f "Using the numbers 1 to 10, and "))
    (display
     (format #f "depending on arrangements,~%"))
    (display
     (format #f "it is possible to~%"))
    (display
     (format #f "form 16- and 17-digit strings. What "))
    (display
     (format #f "is the maximum~%"))
    (display
     (format #f "16-digit string for a 'magic' "))
    (display
     (format #f "5-gon ring?~%"))
    (newline)
    (display
     (format #f "The solution is discussed at~%"))
    (display
     (format #f "https://pythonandr.com/2015/09/13/magic-5-gon-ring-project-euler-problem-68/~%"))
    (display
     (format #f "see https://projecteuler.net/problem=68~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((ndigits 9)
          (debug-flag #t))
      (begin
        (main-3gon-loop ndigits debug-flag)
        ))

    (newline)
    (let ((ndigits 16)
          (debug-flag #t))
      (begin
        (main-5gon-loop ndigits debug-flag)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 68 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

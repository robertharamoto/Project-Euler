#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 66                                     ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated July 21, 2022                                ###
;;;###                                                       ###
;;;###  updated March 6, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### srfi-11 for let-values function
(use-modules ((srfi srfi-11)
              :renamer (symbol-prefix-proc 'srfi-11:)))

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
;;; https://en.wikipedia.org/wiki/Methods_of_computing_square_roots
(define-syntax sqrt-cf-iterate
  (syntax-rules ()
    ((sqrt-cf-iterate int-dd int-sqrt-dd m0 d0 a0)
     (begin
       (let ((mn (- (* d0 a0) m0)))
         (let ((dn (euclidean/ (- int-dd (* mn mn)) d0)))
           (let ((an (euclidean/ (+ int-sqrt-dd mn) dn)))
             (begin
               (set! m0 mn)
               (set! d0 dn)
               (set! a0 an)
               ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
;;; method from
;;; https://en.wikipedia.org/wiki/Methods_of_computing_square_roots
(define (minimal-solution-pells-equation dd)
  (begin
    (srfi-11:let-values
     (((int-sqrt-num int-sqrt-remainder)
       (exact-integer-sqrt dd)))
     (begin
       (if (zero? int-sqrt-remainder)
           (begin
             #f)
           (begin
             (let ((m0 0)
                   (d0 1)
                   (a0 int-sqrt-num)
                   (hnm1 1)
                   (hnm2 0)
                   (knm1 0)
                   (knm2 1)
                   (xx 0)
                   (yy 0)
                   (loop-continue-flag #t))
               (begin
                 (while
                  (equal? loop-continue-flag #t)
                  (begin
                    (let ((hn0 (+ (* a0 hnm1) hnm2))
                          (kn0 (+ (* a0 knm1) knm2)))
                      (let ((gcf (gcd hn0 kn0)))
                        (let ((hn1 (euclidean/ hn0 gcf))
                              (kn1 (euclidean/ kn0 gcf)))
                          (begin
                            (set! hnm2 hnm1)
                            (set! knm2 knm1)
                            (set! hnm1 hn1)
                            (set! knm1 kn1)
                            (set! xx hn1)
                            (set! yy kn1)

                            (let ((pell-eqn
                                   (- (* hn1 hn1) (* dd kn1 kn1))))
                              (begin
                                (if (= pell-eqn 1)
                                    (begin
                                      (set! loop-continue-flag #f)
                                      ))
                                ))

                             ;;; find next m0, d0, a0 for next term in the continued fraction
                            (sqrt-cf-iterate
                             dd int-sqrt-num m0 d0 a0)
                            ))
                        ))
                    ))

                 (list xx yy)
                 ))
             ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-minimal-solution-pells-equation-1 result-hash-table)
 (begin
   (let ((sub-name "test-minimal-solution-pells-equation-1")
         (test-list
          (list
           (list 2 (list 3 2)) (list 3 (list 2 1))
           (list 4 #f) (list 5 (list 9 4))
           (list 6 (list 5 2)) (list 7 (list 8 3))
           (list 9 #f)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((dd (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result
                     (minimal-solution-pells-equation dd)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : dd=~a, "
                        sub-name test-label-index dd))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop start-num end-num debug-flag)
  (begin
    (let ((largest-xx -1)
          (largest-dd -1))
      (begin
        (do ((dd start-num (1+ dd)))
            ((> dd end-num))
          (begin
            (let ((result-list
                   (minimal-solution-pells-equation dd)))
              (begin
                (if (not (equal? result-list #f))
                    (begin
                      (let ((xx (list-ref result-list 0))
                            (yy (list-ref result-list 1)))
                        (begin
                          (if (equal? debug-flag #t)
                              (begin
                                (display
                                 (ice-9-format:format
                                  #f "  ~:d^2 - ~:dx~:d^2 = 1~%"
                                  xx dd yy))
                                (force-output)
                                ))

                          (if (> xx largest-xx)
                              (begin
                                (set! largest-xx xx)
                                (set! largest-dd dd)
                                ))
                          ))
                      ))
                ))
            ))
        (display
         (ice-9-format:format
          #f "The largest minimal solution is x = ~:d, when D = ~:d~%"
          largest-xx largest-dd))
        (display
         (ice-9-format:format
          #f "for x^2-Dy^2=1 (between ~:d <= D <= ~:d)~%"
          start-num end-num))
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Consider quadratic Diophantine equations "))
    (display
     (format #f "of the form:~%"))
    (newline)
    (display
     (format #f "    x^2 - Dy^2 = 1~%"))
    (display
     (format #f "For example, when D=13, the minimal "))
    (display
     (format #f "solution in x is~%"))
    (display
     (format #f "649^2 - 13x180^2 = 1.~%"))
    (display
     (format #f "It can be assumed that there are no "))
    (display
     (format #f "solutions in positive~%"))
    (display
     (format #f "integers when D is square.~%"))
    (newline)
    (display
     (format #f "By finding minimal solutions in x for "))
    (display
     (format #f "D = {2, 3, 5, 6, 7},~%"))
    (display
     (format #f "we obtain the following:~%"))
    (display
     (format #f "    3^2 - 2x2^2 = 1~%"))
    (display
     (format #f "    2^2 - 3x12 = 1~%"))
    (display
     (format #f "    9^2 - 5x4^2 = 1~%"))
    (display
     (format #f "    5^2 - 6x2^2 = 1~%"))
    (display
     (format #f "    8^2 - 7x3^2 = 1~%"))
    (newline)
    (display
     (format #f "Hence, by considering minimal solutions "))
    (display
     (format #f "in x for D <= 7,~%"))
    (display
     (format #f "the largest x is obtained when D=5.~%"))
    (newline)
    (display
     (format #f "Find the value of D <= 1000 in minimal "))
    (display
     (format #f "solutions of x for which~%"))
    (display
     (format #f "the largest value of x is obtained.~%"))
    (newline)
    (display
     (format #f "The solution was described at~%"))
    (display
     (format #f "https://martin-ueding.de/posts/project-euler-solution-66-diophantine-equation/~%"))
    (newline)
    (display
     (format #f "First, find the convergents to the "))
    (display
     (format #f "continued fraction for~%"))
    (display
     (format #f "sqrt(n), hi/ki, using the continued "))
    (display
     (format #f "fraction representation.~%"))
    (display
     (format #f "See https://en.wikipedia.org/wiki/Continued_fraction~%"))
    (display
     (format #f "see https://projecteuler.net/problem=66~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((start-num 2)
          (end-num 7)
          (debug-flag #t))
      (begin
        (sub-main-loop start-num end-num debug-flag)
        ))

    (newline)
    (let ((start-num 2)
          (end-num 1000)
          (debug-flag #f))
      (begin
        (sub-main-loop start-num end-num debug-flag)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 66 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 61                                     ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated July 21, 2022                                ###
;;;###                                                       ###
;;;###  updated March 6, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### srfi-1 for fold function
(use-modules ((srfi srfi-1)
              :renamer (symbol-prefix-proc 'srfi-1:)))

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### digits-module for split-digits-list function
(use-modules ((digits-module)
              :renamer (symbol-prefix-proc 'digits-module:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (triangular-number this-num)
  (begin
    (let ((t1 (+ this-num 1)))
      (let ((t2 (* this-num t1)))
        (let ((t3 (euclidean/ t2 2)))
          (begin
            t3
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-triangular-number-1 result-hash-table)
 (begin
   (let ((sub-name "test-triangular-number-1")
         (test-list
          (list
           (list 1 1) (list 2 3) (list 3 6) (list 4 10)
           (list 5 15)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-num (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result (triangular-number test-num)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : test-num=~a, "
                        sub-name test-label-index test-num))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (square-number this-num)
  (begin
    (* this-num this-num)
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-square-number-1 result-hash-table)
 (begin
   (let ((sub-name "test-square-number-1")
         (test-list
          (list
           (list 1 1) (list 2 4) (list 3 9) (list 4 16)
           (list 5 25)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-num (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result (square-number test-num)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : test-num=~a, "
                        sub-name test-label-index test-num))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (pentagonal-number this-num)
  (begin
    (let ((t1 (- (* 3 this-num) 1)))
      (let ((t2 (* this-num t1)))
        (let ((t3 (euclidean/ t2 2)))
          (begin
            t3
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-pentagonal-number-1 result-hash-table)
 (begin
   (let ((sub-name "test-pentagonal-number-1")
         (test-list
          (list
           (list 1 1) (list 2 5) (list 3 12) (list 4 22)
           (list 5 35) (list 6 51) (list 7 70) (list 8 92)
           (list 9 117) (list 10 145)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-num (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result (pentagonal-number test-num)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : test-num=~a, "
                        sub-name test-label-index test-num))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a~%"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (hexagonal-number this-num)
  (begin
    (let ((t1 (- (* 2 this-num) 1)))
      (let ((t2 (* this-num t1)))
        (begin
          t2
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-hexagonal-number-1 result-hash-table)
 (begin
   (let ((sub-name "test-hexagonal-number-1")
         (test-list
          (list
           (list 1 1) (list 2 6) (list 3 15) (list 4 28)
           (list 5 45)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-num (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result (hexagonal-number test-num)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : test-num=~a, "
                        sub-name test-label-index test-num))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (heptagonal-number this-num)
  (begin
    (let ((t1 (- (* 5 this-num) 3)))
      (let ((t2 (* this-num t1)))
        (let ((t3 (euclidean/ t2 2)))
          (begin
            t3
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-heptagonal-number-1 result-hash-table)
 (begin
   (let ((sub-name "test-heptagonal-number-1")
         (test-list
          (list
           (list 1 1) (list 2 7) (list 3 18) (list 4 34)
           (list 5 55)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-num (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result (heptagonal-number test-num)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : test-num=~a, "
                        sub-name test-label-index test-num))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (octagonal-number this-num)
  (begin
    (let ((t1 (- (* 3 this-num) 2)))
      (let ((t2 (* this-num t1)))
        (begin
          t2
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-octagonal-number-1 result-hash-table)
 (begin
   (let ((sub-name "test-octagonal-number-1")
         (test-list
          (list
           (list 1 1) (list 2 8) (list 3 21) (list 4 40)
           (list 5 65)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-num (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result (octagonal-number test-num)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : test-num=~a, "
                        sub-name test-label-index test-num))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (populate-htable! p-htable this-function max-num)
  (begin
    (let ((break-flag #f))
      (begin
        (do ((jj 1 (+ jj 1)))
            ((or (> jj max-num)
                 (equal? break-flag #t)))
          (begin
            (let ((this-num (this-function jj)))
              (begin
                (if (> this-num max-num)
                    (begin
                      (set! break-flag #t))
                    (begin
                      (hash-set! p-htable this-num jj)
                      ))
                ))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
;;; format of list-list is (order, index, value)
(define (is-order-in-list-list? this-order alist-lists)
  (begin
    (let ((result #f)
          (alen (length alist-lists))
          (break-flag #f))
      (begin
        (do ((ii 0 (+ ii 1)))
            ((or (>= ii alen)
                 (equal? break-flag #t)))
          (begin
            (let ((this-elem (list-ref alist-lists ii)))
              (let ((elem-order (list-ref this-elem 0)))
                (begin
                  (if (equal? this-order elem-order)
                      (begin
                        (set! result #t)
                        (set! break-flag #t)
                        ))
                  )))
            ))
        result
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-is-order-in-list-list-1 result-hash-table)
 (begin
   (let ((sub-name "test-is-order-in-list-list-1")
         (test-list
          (list
           (list 3 (list
                    (list 1 2 3) (list 2 4 55)
                    (list 3 4 4)) #t)
           (list 2 (list
                    (list 1 2 3) (list 2 4 55)
                    (list 3 4 4)) #t)
           (list 4 (list
                    (list 1 2 3) (list 2 4 55)
                    (list 3 4 4)) #f)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-order (list-ref alist 0))
                  (test-alist (list-ref alist 1))
                  (shouldbe (list-ref alist 2)))
              (let ((result
                     (is-order-in-list-list? test-order test-alist)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "test-order=~a, test-alist=~a, "
                        test-order test-alist))
                      (err-3
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append
                      err-1 err-2 err-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (get-order-index
         this-num
         p3-htable p4-htable p5-htable
         p6-htable p7-htable p8-htable)
  (begin
    (let ((result-list (list)))
      (begin
        (if (not
             (equal?
              (hash-ref p3-htable this-num #f)
              #f))
            (begin
              (set!
               result-list
               (cons
                (list
                 3 (hash-ref
                    p3-htable this-num #f) this-num)
                result-list))
              ))
        (if (not
             (equal?
              (hash-ref p4-htable this-num #f)
              #f))
            (begin
              (set!
               result-list
               (cons
                (list
                 4 (hash-ref
                    p4-htable this-num #f) this-num)
                result-list))
              ))
        (if (not
             (equal?
              (hash-ref p5-htable this-num #f) #f))
            (begin
              (set!
               result-list
               (cons
                (list
                 5 (hash-ref
                    p5-htable this-num #f) this-num)
                result-list))
              ))
        (if (not
             (equal?
              (hash-ref p6-htable this-num #f) #f))
            (begin
              (set!
               result-list
               (cons
                (list
                 6 (hash-ref
                    p6-htable this-num #f) this-num)
                result-list))
              ))
        (if (not
             (equal?
              (hash-ref p7-htable this-num #f) #f))
            (begin
              (set!
               result-list
               (cons
                (list
                 7 (hash-ref
                    p7-htable this-num #f) this-num)
                result-list))
              ))
        (if (not
             (equal?
              (hash-ref p8-htable this-num #f) #f))
            (begin
              (set!
               result-list
               (cons
                (list
                 8 (hash-ref
                    p8-htable this-num #f) this-num)
                result-list))
              ))

        (reverse result-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-get-order-index-1 result-hash-table)
 (begin
   (let ((sub-name "test-get-order-index-1")
         (max-num 10000)
         (test-list
          (list
           (list 1281 (list
                       (list 8 21 1281)))
           (list 2512 (list
                       (list 7 32 2512)))
           (list 8128 (list
                       (list 3 127 8128)
                       (list 6 64 8128)))
           (list 2882 (list
                       (list 5 44 2882)))
           (list 5625 (list
                       (list 4 75 5625)))
           (list 8256 (list
                       (list 3 128 8256)))
           ))
         (test-label-index 0))
     (let ((p3-htable (make-hash-table max-num))
           (p4-htable (make-hash-table max-num))
           (p5-htable (make-hash-table max-num))
           (p6-htable (make-hash-table max-num))
           (p7-htable (make-hash-table max-num))
           (p8-htable (make-hash-table max-num)))
       (begin
         (populate-htable!
          p3-htable triangular-number max-num)
         (populate-htable!
          p4-htable square-number max-num)
         (populate-htable!
          p5-htable pentagonal-number max-num)
         (populate-htable!
          p6-htable hexagonal-number max-num)
         (populate-htable!
          p7-htable heptagonal-number max-num)
         (populate-htable!
          p8-htable octagonal-number max-num)

         (for-each
          (lambda (alist)
            (begin
              (let ((test-num (list-ref alist 0))
                    (shouldbe (list-ref alist 1)))
                (let ((result
                       (get-order-index
                        test-num
                        p3-htable p4-htable p5-htable
                        p6-htable p7-htable p8-htable)))
                  (let ((err-1
                         (format
                          #f "~a : error (~a) : test-num=~a, "
                          sub-name test-label-index test-num))
                        (err-2
                         (format
                          #f "shouldbe=~a, result=~a"
                          shouldbe result)))
                    (begin
                      (unittest2:assert?
                       (equal? shouldbe result)
                       sub-name
                       (string-append err-1 err-2)
                       result-hash-table)
                      ))
                  ))
              (set! test-label-index (1+ test-label-index))
              )) test-list)
         )))
   ))

;;;#############################################################
;;;#############################################################
;;; acc-list contains list of 4-digit numbers, full-acc-list contains
;;; lists of lists of the form (order index number)
(define (sub-main-loop start-num end-num max-depth)
  (define (local-calc-3-8 this-depth max-depth
                          this-num dlist
                          acc-list full-acc-list
                          p3-htable p4-htable p5-htable
                          p6-htable p7-htable p8-htable)
    (begin
      (let ((local-possible-orders
             (get-order-index
              this-num
              p3-htable p4-htable p5-htable
              p6-htable p7-htable p8-htable))
            (local-results #f)
            (d1 (list-tail dlist 2)))
        (begin
          (cond
           ((and (>= this-depth max-depth)
                 (equal? max-depth (length acc-list))
                 (equal? max-depth (length full-acc-list)))
            (begin
              ;;; require that the first and last numbers are cyclic
              (let ((first-d1
                     (digits-module:split-digits-list
                      (car (last-pair acc-list))))
                    (last-d1
                     (digits-module:split-digits-list
                      (car acc-list))))
                (let ((f1 (list-head first-d1 2))
                      (d1 (list-tail last-d1 2)))
                  (begin
                    (if (equal? f1 d1)
                        (begin
                          (let ((require-flag #t))
                            (begin
                            ;;; require that the first max-depth orders are represented
                              (do ((ii 3 (+ ii 1)))
                                  ((> ii (+ max-depth 2)))
                                (begin
                                  (if (not
                                       (is-order-in-list-list?
                                        ii full-acc-list))
                                      (begin
                                        (set! require-flag #f)
                                        ))
                                  ))

                              (if (equal? require-flag #t)
                                  (begin
                                    (set!
                                     local-results
                                     (list (reverse acc-list)
                                           (reverse full-acc-list)))
                                    local-results)
                                  (begin
                                    #f
                                    ))
                              )))
                        (begin
                          #f
                          ))
                    )))
              ))
           (else
            (begin
              (if (and
                   (not (equal? local-possible-orders #f))
                   (list? local-possible-orders)
                   (> (length local-possible-orders) 0))
                  (begin
                    (for-each
                     (lambda (this-list)
                       (begin
                         (let ((local-order
                                (list-ref this-list 0))
                               (local-nn
                                (list-ref this-list 1))
                               (local-num
                                (list-ref this-list 2))
                               (break-flag #f))
                           (begin
                             (if (equal?
                                  (is-order-in-list-list?
                                   local-order full-acc-list)
                                  #f)
                                 (begin
                                   (do ((ii 10 (+ ii 1)))
                                       ((or (> ii 99)
                                            (equal? break-flag #t)))
                                     (begin
                                       (let ((d2
                                              (digits-module:split-digits-list ii)))
                                         (let ((d3 (append d1 d2)))
                                           (let ((next-num
                                                  (digits-module:digit-list-to-number
                                                   d3)))
                                             (if (and (>= next-num 1000)
                                                      (<= next-num 9999))
                                                 (begin
                                                   (let ((results-list
                                                          (local-calc-3-8
                                                           (+ this-depth 1) max-depth
                                                           next-num d3
                                                           (cons this-num acc-list)
                                                           (cons
                                                            (list
                                                             local-order local-nn
                                                             this-num)
                                                            full-acc-list)
                                                           p3-htable p4-htable p5-htable
                                                           p6-htable p7-htable p8-htable)))
                                                     (begin
                                                       (if (not (equal? results-list #f))
                                                           (begin
                                                             (set! local-results results-list)
                                                             (set! break-flag #t)
                                                             ))
                                                       ))
                                                   ))
                                             )))
                                       ))
                                   ))
                             ))
                         )) local-possible-orders)
                    ))
              local-results
              )))
          ))
      ))
  (begin
    (let ((counter 0)
          (p3-htable (make-hash-table end-num))
          (p4-htable (make-hash-table end-num))
          (p5-htable (make-hash-table end-num))
          (p6-htable (make-hash-table end-num))
          (p7-htable (make-hash-table end-num))
          (p8-htable (make-hash-table end-num))
          (break-flag #f))
      (begin
        (populate-htable!
         p3-htable triangular-number end-num)
        (populate-htable!
         p4-htable square-number end-num)
        (populate-htable!
         p5-htable pentagonal-number end-num)
        (populate-htable!
         p6-htable hexagonal-number end-num)
        (populate-htable!
         p7-htable heptagonal-number end-num)
        (populate-htable!
         p8-htable octagonal-number end-num)

        (do ((nn start-num (+ nn 1)))
            ((> nn end-num))
          (begin
            (let ((dlist
                   (digits-module:split-digits-list nn)))
              (let ((results
                     (local-calc-3-8
                      1 max-depth nn dlist
                      (list) (list)
                      p3-htable p4-htable p5-htable
                      p6-htable p7-htable p8-htable)))
                (begin
                  (if (and
                       (not (equal? results #f))
                       (list? results)
                       (> (length results) 0))
                      (begin
                        (let ((num-list (list-ref results 0))
                              (full-num-list (list-ref results 1)))
                          (begin
                            (set! counter (+ counter 1))
                            (display
                             (ice-9-format:format
                              #f "(~:d)  sum = ~:d : "
                              counter
                              (srfi-1:fold + 0 num-list)))
                            (display
                             (ice-9-format:format
                              #f "list of numbers = ~a~%"
                              num-list))
                            (force-output)
                            (for-each
                             (lambda(this-list)
                               (begin
                                 (let ((order (list-ref this-list 0))
                                       (nn (list-ref this-list 1))
                                       (num (list-ref this-list 2)))
                                   (begin
                                     (display
                                      (ice-9-format:format
                                       #f "  P(~:d, ~:d) = ~:d~%"
                                       order nn num))
                                     ))
                                 )) full-num-list)
                            (force-output)
                            ))
                        ))
                  )))
            ))

        (newline)
        (display
         (ice-9-format:format
          #f "number of ~:d cyclic 4-digit numbers found = ~:d~%"
          max-depth counter))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Triangle, square, pentagonal, "))
    (display
     (format #f "hexagonal, heptagonal,~%"))
    (display
     (format #f "and octagonal numbers are all "))
    (display
     (format #f "figurate (polygonal) numbers~%"))
    (display
     (format #f "and are generated by the "))
    (display
     (format #f "following formulae:~%"))
    (newline)
    (display
     (format #f "Triangle:    P(3,n)=n(n+1)/2     "))
    (display
     (format #f "1, 3, 6, 10, 15, ...~%"))
    (display
     (format #f "Square:      P(4,n)=n^2          "))
    (display
     (format #f "1, 4, 9, 16, 25, ...~%"))
    (display
     (format #f "Pentagonal:  P(5,n)=n(3n-1)/2    "))
    (display
     (format #f "1, 5, 12, 22, 35, ...~%"))
    (display
     (format #f "Hexagonal:   P(6,n)=n(2n-1)      "))
    (display
     (format #f "1, 6, 15, 28, 45, ...~%"))
    (display
     (format #f "Heptagonal:  P(7,n)=n(5n-3)/2    "))
    (display
     (format #f "1, 7, 18, 34, 55, ...~%"))
    (display
     (format #f "Octagonal:   P(8,n)=n(3n-2)      "))
    (display
     (format #f "1, 8, 21, 40, 65, ...~%"))
    (newline)
    (display
     (format #f "The ordered set of three 4-digit "))
    (display
     (format #f "numbers: 8128,~%"))
    (display
     (format #f "2882, 8281, has three interesting "))
    (display
     (format #f "properties.~%"))
    (display
     (format #f "  1. The set is cyclic, in that the "))
    (display
     (format #f "last two digits of~%"))
    (display
     (format #f "each number is the first two digits "))
    (display
     (format #f "of the next number~%"))
    (display
     (format #f "(including the last number with "))
    (display
     (format #f "the first).~%"))
    (display
     (format #f "  2. Each polygonal type: triangle "))
    (display
     (format #f "(P3,127=8128),~%"))
    (display
     (format #f "square (P4,91=8281), and pentagonal "))
    (display
     (format #f "(P5,44=2882), is~%"))
    (display
     (format #f "represented by a different number "))
    (display
     (format #f "in the set.~%"))
    (display
     (format #f "  3. This is the only set of 4-digit "))
    (display
     (format #f "numbers with this property.~%"))
    (newline)
    (display
     (format #f "Find the sum of the only ordered set "))
    (display
     (format #f "of six cyclic 4-digit~%"))
    (display
     (format #f "numbers for which each polygonal type: "))
    (display
     (format #f "triangle, square,~%"))
    (display
     (format #f "pentagonal, hexagonal, heptagonal, and "))
    (display
     (format #f "octagonal, is represented~%"))
    (display
     (format #f "by a different number in the set.~%"))
    (display
     (format #f "see https://projecteuler.net/problem=61~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((start-num 1000)
          (end-num 9999)
          (max-depth 3))
      (begin
        (sub-main-loop start-num end-num max-depth)
        ))

    (newline)
    (let ((start-num 1000)
          (end-num 9999)
          (max-depth 6))
      (begin
        (sub-main-loop start-num end-num max-depth)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 61 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (load "digits-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 63                                     ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated July 21, 2022                                ###
;;;###                                                       ###
;;;###  updated March 6, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### digits-module for split-digits-list function
(use-modules ((digits-module)
              :renamer (symbol-prefix-proc 'digits-module:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (integer-power this-number this-exponent)
  (begin
    (cond
     ((= this-exponent 0)
      (begin
        1
        ))
     ((= this-exponent 1)
      (begin
        this-number
        ))
     ((< this-exponent 0)
      (begin
        -1
        ))
     (else
      (begin
        (let ((result-num this-number)
              (max-iter (- this-exponent 1)))
          (begin
            (do ((ii 0 (+ ii 1)))
                ((>= ii max-iter))
              (begin
                (set! result-num (* result-num this-number))
                ))
            result-num
            ))
        )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-integer-power-1 result-hash-table)
 (begin
   (let ((sub-name "test-integer-power-1")
         (test-list
          (list
           (list 10 0 1) (list 11 0 1) (list 12 0 1)
           (list 10 1 10) (list 11 1 11) (list 12 1 12)
           (list 10 2 100) (list 11 2 121) (list 12 2 144)
           (list 10 3 1000) (list 10 4 10000) (list 10 5 100000)
           (list 2 2 4) (list 2 3 8) (list 2 4 16) (list 2 5 32)
           (list 2 6 64) (list 2 7 128) (list 2 8 256) (list 2 9 512)
           (list 2 10 1024)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-num (list-ref alist 0))
                  (test-exp (list-ref alist 1))
                  (shouldbe-num (list-ref alist 2)))
              (let ((result-num
                     (integer-power test-num test-exp)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "test-num=~a, test-exp=~a, "
                        test-num test-exp))
                      (err-3
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe-num result-num)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-num result-num)
                     sub-name
                     (string-append
                      err-1 err-2 err-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop max-num debug-flag)
  (begin
    (let ((ndigit-counter 0)
          (result-list-list (list)))
      (begin
        (do ((ii-exp 1 (+ ii-exp 1)))
            ((> ii-exp max-num))
          (begin
            (let ((rlist (list)))
              (begin
                (do ((ii-base 1 (+ ii-base 1)))
                    ((> ii-base 9))
                  (begin
                    (let ((this-num
                           (integer-power ii-base ii-exp)))
                      (let ((dlist
                             (digits-module:split-digits-list this-num)))
                        (let ((dlength (length dlist)))
                          (begin
                            (if (equal? dlength ii-exp)
                                (begin
                                  (set!
                                   ndigit-counter (+ ndigit-counter 1))
                                  (set!
                                   rlist
                                   (cons
                                    (list ii-base ii-exp this-num)
                                    rlist))
                                  (if (equal? debug-flag #t)
                                      (begin
                                        (display
                                         (ice-9-format:format
                                          #f "    (~:d) ~:d^~:d = ~:d~%"
                                          ndigit-counter ii-base
                                          ii-exp this-num))
                                        ))
                                  ))
                            ))
                        ))
                    ))

                (set!
                 result-list-list
                 (cons (reverse rlist) result-list-list))
                ))
            ))

        (newline)
        (display
         (ice-9-format:format
          #f "There are ~:d n-digit postive integers which~%"
          ndigit-counter))
        (display
         (format #f "are also an nth power, (for exponents less~%"))
        (display
         (ice-9-format:format
          #f "than ~:d)~%"
          max-num))

        (let ((ncount 0)
              (index 0))
          (begin
            (for-each
             (lambda (this-list)
               (begin
                 (if (and (list? this-list)
                          (> (length this-list) 0))
                     (begin
                       (let ((first-elem (car this-list))
                             (sub-count (length this-list)))
                         (let ((this-exp (list-ref first-elem 1)))
                           (begin
                             (set! index (+ index 1))
                             (display
                              (ice-9-format:format
                               #f "  (~:d) : ~:dth power has ~:d "
                               index this-exp sub-count))
                             (display
                              (ice-9-format:format
                               #f "~:d-digit numbers~%"
                               this-exp))
                             (set! ncount (+ ncount sub-count))
                             )))
                       ))
                 )) (reverse result-list-list))

            (display
             (ice-9-format:format
              #f "totals: number of n-digit positive integers that are~%"))
            (display
             (ice-9-format:format
              #f "also an n-th power = ~:d~%"
              ncount))
            (newline)
            (force-output)
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "The 5-digit number, 16807=7^5, is "))
    (display
     (format #f "also a fifth power.~%"))
    (display
     (format #f "Similarly, the 9-digit number, "))
    (display
     (format #f "134217728=8^9,~%"))
    (display
     (format #f "is a ninth power.~%"))
    (newline)
    (display
     (format #f "How many n-digit positive integers "))
    (display
     (format #f "exist which are also~%"))
    (display
     (format #f "an nth power?~%"))
    (newline)
    (display
     (format #f "Note: since 10^1=10 has two digits, "))
    (display
     (format #f "10^2=100 has 3 digits,~%"))
    (display
     (format #f "10^3=1000 has 4 digits,...~%"))
    (display
     (format #f "only need to consider bases that are "))
    (display
     (format #f "less than 10.~%"))
    (display
     (format #f "see https://projecteuler.net/problem=63~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((max-num 3)
          (debug-flag #t))
      (begin
        (sub-main-loop max-num debug-flag)
        ))

    (newline)
    (let ((max-num 1000)
          (debug-flag #f))
      (begin
        (sub-main-loop max-num debug-flag)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 63 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (load "digits-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 62                                     ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated July 21, 2022                                ###
;;;###                                                       ###
;;;###  updated March 6, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### digits-module for split-digits-list function
(use-modules ((digits-module)
              :renamer (symbol-prefix-proc 'digits-module:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
;;; result list of the form (list (list num num-cubed digit-list) ...
(define (populate-cube-hashes! count-htable num-lists-htable max-num)
  (begin
    (hash-clear! count-htable)
    (hash-clear! num-lists-htable)

    (do ((ii 1 (1+ ii)))
        ((> ii max-num))
      (begin
        (let ((cubed-num (* ii ii ii)))
          (let ((dlist
                 (digits-module:split-digits-list cubed-num)))
            (let ((slist (sort dlist >)))
              (let ((snum
                     (digits-module:digit-list-to-number slist)))
                (let ((c-count
                       (hash-ref count-htable snum 0))
                      (this-data (list ii cubed-num))
                      (data-list
                       (hash-ref num-lists-htable snum (list))))
                  (begin
                    (hash-set! count-htable snum (1+ c-count))
                    (hash-set!
                     num-lists-htable
                     snum (cons this-data data-list))
                    ))
                ))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-populate-cube-hashes-1 result-hash-table)
 (begin
   (let ((sub-name "test-populate-cube-hashes-1")
         (test-list
          (list
           (list 1 1 (list (list 1 1)))
           (list 8 1 (list (list 2 8)))
           (list 72 1 (list (list 3 27)))
           (list 64 1 (list (list 4 64)))
           ))
         (count-htable (make-hash-table 10))
         (num-lists-htable (make-hash-table 10))
         (max-num 4)
         (test-label-index 0))
     (begin
       (populate-cube-hashes!
        count-htable num-lists-htable max-num)

       (for-each
        (lambda (alist)
          (begin
            (let ((sorted-num (list-ref alist 0))
                  (shouldbe-count (list-ref alist 1))
                  (shouldbe-list-list (list-ref alist 2)))
              (let ((result-count
                     (hash-ref count-htable sorted-num 0))
                    (result-list-list
                     (hash-ref num-lists-htable sorted-num (list))))
                (let ((slen (length shouldbe-list-list))
                      (rlen (length result-list-list)))
                  (let ((err-1
                         (format
                          #f "~a : error (~a) : sorted-num=~a, "
                          sub-name test-label-index sorted-num))
                        (err-2
                         (format
                          #f "shouldbe length=~a, result length=~a"
                          slen rlen)))
                    (begin
                      (unittest2:assert?
                       (equal? slen rlen)
                       sub-name
                       (string-append err-1 err-2)
                       result-hash-table)

                      (for-each
                       (lambda (slist)
                         (begin
                           (let ((err-11
                                  (format
                                   #f "~a : error (~a) : sorted-num=~a, "
                                   sub-name test-label-index sorted-num))
                                 (err-12
                                  (format
                                   #f "shouldbe=~a, result=~a"
                                   slist result-list-list)))
                             (begin
                               (unittest2:assert?
                                (not
                                 (equal?
                                  (member slist result-list-list)
                                  #f))
                                sub-name
                                (string-append err-11 err-12)
                                result-hash-table)
                               ))
                           )) shouldbe-list-list)
                      ))
                  )))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop max-num num-perm)
  (begin
    (let ((count-htable (make-hash-table 1000))
          (num-lists-htable (make-hash-table 1000)))
      (begin
        (populate-cube-hashes!
         count-htable num-lists-htable max-num)

        (let ((key-list (list)))
          (begin
            (hash-for-each
             (lambda (key count)
               (begin
                 (if (= count num-perm)
                     (begin
                       (set! key-list (cons key key-list))
                       ))
                 )) count-htable)

            (let ((smallest-cube -1)
                  (smallest-num -1)
                  (smallest-cubes-list (list)))
              (begin
                (for-each
                 (lambda (snum)
                   (begin
                     (let ((scube-list-list
                            (hash-ref
                             num-lists-htable snum (list)))
                           (min-cube -1)
                           (min-num -1))
                       (begin
                         (for-each
                          (lambda (alist)
                            (begin
                              (let ((anum (list-ref alist 0))
                                    (acube (list-ref alist 1)))
                                (begin
                                  (if (or (< min-cube 0)
                                          (< acube min-cube))
                                      (begin
                                        (set! min-cube acube)
                                        (set! min-num anum)
                                        ))
                                  ))
                              )) scube-list-list)
                         (if (or (< smallest-cube 0)
                                 (< min-cube smallest-cube))
                             (begin
                               (set! smallest-cube min-cube)
                               (set! smallest-num min-num)
                               (set! smallest-cubes-list scube-list-list)
                               ))
                         ))
                     )) key-list)

                (if (> smallest-cube 0)
                    (begin
                      (display
                       (ice-9-format:format
                        #f "the smallest cube is ~:d^3 = ~:d~%"
                        smallest-num smallest-cube))
                      (display
                       (ice-9-format:format
                        #f "with exactly ~:d permutations~%"
                        num-perm))

                      (for-each
                       (lambda (this-list)
                         (begin
                           (let ((anum (list-ref this-list 0))
                                 (acube (list-ref this-list 1)))
                             (begin
                               (display
                                (ice-9-format:format
                                 #f "  ~:d^3 = ~:d~%" anum acube))
                               ))
                           )) (sort
                               smallest-cubes-list
                               (lambda (a b)
                                 (begin
                                   (< (car a) (car b))
                                   ))))

                      (newline)
                      (force-output))
                    (begin
                      (display
                       (ice-9-format:format
                        #f "no ~:d permutations found "
                        num-perm))
                      (display
                       (ice-9-format:format
                        #f "(less than ~:d)~%" max-num))
                      (force-output)
                      ))
                ))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "The cube, 41063625 (345^3), can be "))
    (display
     (format #f "permuted to produce~%"))
    (display
     (format #f "two other cubes: 56623104 (384^3) "))
    (display
     (format #f "and 66430125 (405^3).~%"))
    (display
     (format #f "In fact, 41063625 is the smallest "))
    (display
     (format #f "cube which has~%"))
    (display
     (format #f "exactly three permutations of its "))
    (display
     (format #f "digits which are~%"))
    (display
     (format #f "also cube.~%"))
    (newline)
    (display
     (format #f "Find the smallest cube for which "))
    (display
     (format #f "exactly five~%"))
    (display
     (format #f "permutations of its digits are cube.~%"))
    (newline)
    (display
     (format #f "This program uses a hash table "))
    (display
     (format #f "to collect all cubes~%"))
    (display
     (format #f "which have the same digits, then "))
    (display
     (format #f "sorts through which~%"))
    (display
     (format #f "elements have the required number "))
    (display
     (format #f "of permutations.~%"))
    (display
     (format #f "see https://projecteuler.net/problem=62~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((max-num 1000)
          (num-perm 3))
      (begin
        (sub-main-loop max-num num-perm)
        ))

    (newline)
    (let ((max-num 10000)
          (num-perm 5))
      (begin
        (sub-main-loop max-num num-perm)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 62 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (load "digits-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

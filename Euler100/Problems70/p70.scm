#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 70                                     ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated July 21, 2022                                ###
;;;###                                                       ###
;;;###  updated March 6, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### srfi-1 for fold function
(use-modules ((srfi srfi-1)
              :renamer (symbol-prefix-proc 'srfi-1:)))

;;;### srfi-19 for current-julian-day functions
(use-modules ((srfi srfi-19)
              :renamer (symbol-prefix-proc 'srfi-19:)))

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### prime-module for is-prime-array? function
(use-modules ((prime-module)
              :renamer (symbol-prefix-proc 'prime-module:)))

;;;### digits-module for split-digits-list function
(use-modules ((digits-module)
              :renamer (symbol-prefix-proc 'digits-module:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
;;; make a list of primes less than n
(define (make-prime-list max-num)
  (begin
    (let ((int-array (make-array 0 (1+ max-num)))
          (result-list (list)))
      (begin
        (do ((ii 0 (1+ ii)))
            ((> ii max-num))
          (begin
            (array-set! int-array ii ii)
            ))

        (do ((ii 2 (1+ ii)))
            ((> ii max-num))
          (begin
            (let ((ii-num (array-ref int-array ii)))
              (begin
                (if (= ii-num ii)
                    (begin
                      (set! result-list (cons ii-num result-list))

                      (do ((jj (+ ii ii) (+ jj ii)))
                          ((> jj max-num))
                        (begin
                          (array-set! int-array -1 jj)
                          ))
                      ))
                ))
            ))

        (reverse result-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-make-prime-list-1 result-hash-table)
 (begin
   (let ((sub-name "test-make-prime-list-1")
         (test-list
          (list
           (list 2 (list 2)) (list 3 (list 2 3)) (list 4 (list 2 3))
           (list 5 (list 2 3 5)) (list 6 (list 2 3 5))
           (list 7 (list 2 3 5 7)) (list 8 (list 2 3 5 7))
           (list 9 (list 2 3 5 7)) (list 10 (list 2 3 5 7))
           (list 11 (list 2 3 5 7 11)) (list 13 (list 2 3 5 7 11 13))
           (list 17 (list 2 3 5 7 11 13 17))
           (list 19 (list 2 3 5 7 11 13 17 19))
           (list 23 (list 2 3 5 7 11 13 17 19 23))
           ))
         (test-label-index 0)
         (ok-flag #t))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((test-num (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result (make-prime-list test-num)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : test-num=~a, "
                        sub-name test-label-index test-num))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (divide-out-number this-num pnum)
  (begin
    (let ((result this-num))
      (begin
        (while
         (zero? (modulo result pnum))
         (begin
           (set! result (euclidean/ result pnum))
           ))
        result
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-divide-out-number-1 result-hash-table)
 (begin
   (let ((sub-name "test-divide-out-number-1")
         (test-list
          (list
           (list 4 2 1) (list 6 3 2) (list 9 3 1)
           (list 10 2 5) (list 10 5 2) (list 11 7 11)
           (list 28 2 7) (list 28 7 4)
           (list 36 2 9) (list 36 3 4)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((input-num (list-ref this-list 0))
                  (pnum (list-ref this-list 1))
                  (shouldbe (list-ref this-list 2)))
              (let ((result
                     (divide-out-number input-num pnum)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "input-num=~a, pnum=~a, "
                        input-num pnum))
                      (err-3
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append
                      err-1 err-2 err-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (single-prime-factor-divisors-list input-number prime-list)
  (begin
    (let ((result-list (list)))
      (begin
        (cond
         ((or (<= input-number 1)
              (not (list? prime-list))
              (< (length prime-list) 1))
          (begin
            (list)
            ))
         ((not
           (equal?
            (member input-number prime-list)
            #f))
          (begin
            (list input-number)
            ))
         (else
          (begin
            (let ((result-list (list))
                  (plen (length prime-list))
                  (pmax (car (last-pair prime-list)))
                  (ll-max (1+ (exact-integer-sqrt input-number)))
                  (this-prime (car prime-list))
                  (local-num input-number))
              (begin
                (do ((ii 0 (1+ ii)))
                    ((or (>= ii plen)
                         (> this-prime ll-max)))
                  (begin
                    (set! this-prime (list-ref prime-list ii))

                    (if (zero? (modulo local-num this-prime))
                        (begin
                          (if (equal?
                               (member this-prime result-list)
                               #f)
                              (begin
                                (set!
                                 result-list
                                 (cons this-prime result-list))
                                ))

                          (let ((div
                                 (euclidean/ local-num this-prime)))
                            (begin
                              (if (prime-module:is-prime? div)
                                  (begin
                                    (if (and
                                         (> div this-prime)
                                         (equal?
                                          (member div result-list)
                                          #f))
                                        (begin
                                          (set!
                                           result-list
                                           (cons div result-list))
                                          ))

                                    (set!
                                     local-num
                                     (divide-out-number local-num this-prime))
                                    ))
                              ))

                          (set!
                           local-num
                           (divide-out-number local-num this-prime))
                          ))
                    ))

                (if (< pmax ll-max)
                    (begin
                      (display
                       (ice-9-format:format
                        #f "warning max generated prime = ~:d~%"
                        pmax))
                      (display
                       (ice-9-format:format
                        #f "less than required sqrt(~:d) = ~a,~%"
                        input-number (sqrt input-number)))
                      (display
                       (ice-9-format:format
                        #f "ll-max=~:d~%" ll-max))
                      (display
                       (format #f "quitting...~%"))
                      (quit)
                      ))

                (sort result-list <)
                ))
            )))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-single-prime-factor-divisors-list-1 result-hash-table)
 (begin
   (let ((sub-name "test-single-prime-factor-divisors-list-1")
         (test-list
          (list
           (list 2 (list 2)) (list 3 (list 3))
           (list 4 (list 2)) (list 5 (list 5))
           (list 6 (list 2 3)) (list 7 (list 7))
           (list 8 (list 2)) (list 9 (list 3))
           (list 10 (list 2 5)) (list 11 (list 11))
           (list 12 (list 2 3)) (list 13 (list 13))
           (list 14 (list 2 7)) (list 15 (list 3 5))
           (list 16 (list 2)) (list 17 (list 17))
           (list 18 (list 2 3)) (list 19 (list 19))
           (list 20 (list 2 5)) (list 21 (list 3 7))
           (list 22 (list 2 11)) (list 23 (list 23))
           (list 24 (list 2 3)) (list 25 (list 5))
           (list 26 (list 2 13)) (list 27 (list 3))
           (list 28 (list 2 7)) (list 29 (list 29))
           (list 30 (list 2 3 5))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((test-num (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((plist (make-prime-list test-num)))
                (let ((result
                       (single-prime-factor-divisors-list
                        test-num plist)))
                  (let ((err-1
                         (format
                          #f "~a : error (~a) : test-num=~a, "
                          sub-name test-label-index test-num))
                        (err-2
                         (format
                          #f "shouldbe=~a, result=~a"
                          shouldbe result)))
                    (begin
                      (unittest2:assert?
                       (equal? shouldbe result)
                       sub-name
                       (string-append err-1 err-2)
                       result-hash-table)
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; count the number of relatively prime numbers to n
;;; if n even, all even numbers are not relatively prime
(define (totient-function nn prime-list)
  (begin
    (let ((tproduct nn)
          (ll-max (1+ (exact-integer-sqrt nn)))
          (pmax (car (last-pair prime-list)))
          (sprime-list
           (single-prime-factor-divisors-list nn prime-list)))
      (let ((splen (length sprime-list)))
        (begin
          (cond
           ((or (<= nn 1)
                (> ll-max pmax))
            (begin
              (set! tproduct -1)
              ))
           (else
            (begin
              (do ((ii 0 (1+ ii)))
                  ((>= ii splen))
                (begin
                  (set!
                   tproduct
                   (* tproduct
                      (- 1 (/ 1 (list-ref sprime-list ii))
                         )))
                  ))
              )))
          tproduct
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-totient-function-1 result-hash-table)
 (begin
   (let ((sub-name "test-totient-function-1")
         (test-list
          (list
           (list 2 1) (list 3 2) (list 4 2)
           (list 5 4) (list 6 2) (list 7 6)
           (list 8 4) (list 9 6) (list 10 4)
           (list 11 10) (list 12 4) (list 13 12)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-num (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((prime-list (make-prime-list test-num)))
                (let ((result
                       (totient-function test-num prime-list)))
                  (let ((err-1
                         (format
                          #f "~a : error (~a) : test-num=~a, "
                          sub-name test-label-index test-num))
                        (err-2
                         (format
                          #f "shouldbe=~a, result=~a"
                          shouldbe result)))
                    (begin
                      (unittest2:assert?
                       (equal? shouldbe result)
                       sub-name
                       (string-append err-1 err-2)
                       result-hash-table)
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (totient-list-function nn)
  (begin
    (let ((result-list (list)))
      (begin
        (cond
         ((<= nn 1)
          (begin
            #f
            ))
         ((even? nn)
          (begin
            (do ((ii 1 (+ ii 2)))
                ((>= ii nn))
              (begin
                (if (equal? (gcd nn ii) 1)
                    (begin
                      (set! result-list (cons ii result-list))
                      ))
                ))
            ))
         (else
          (begin
            (do ((ii 1 (+ ii 1)))
                ((>= ii nn))
              (begin
                (if (equal? (gcd nn ii) 1)
                    (begin
                      (set! result-list (cons ii result-list))
                      ))
                ))
            )))
        (reverse result-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-totient-list-function-1 result-hash-table)
 (begin
   (let ((sub-name "test-totient-list-function-1")
         (test-list
          (list
           (list 2 (list 1)) (list 3 (list 1 2))
           (list 4 (list 1 3)) (list 5 (list 1 2 3 4))
           (list 6 (list 1 5)) (list 7 (list 1 2 3 4 5 6))
           (list 8 (list 1 3 5 7)) (list 9 (list 1 2 4 5 7 8))
           (list 10 (list 1 3 7 9))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-num (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result
                     (totient-list-function test-num)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : test-num=~a, "
                        sub-name test-label-index test-num))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (populate-digits-hash! digits-htable prime-list)
  (begin
    (hash-clear! digits-htable)

    (for-each
     (lambda (aprime)
       (begin
         (let ((dlist
                (digits-module:split-digits-list aprime)))
           (begin
             (hash-set! digits-htable aprime dlist)
             ))
         )) prime-list)
    ))

;;;#############################################################
;;;#############################################################
(define (is-num1-permutation-num2? num1 num2 digits-htable)
  (begin
    (let ((num1-dlist
           (hash-ref digits-htable num1 #f))
          (num2-dlist
           (hash-ref digits-htable num2 #f)))
      (begin
        (if (equal? num1-dlist #f)
            (begin
              (let ((dlist
                     (digits-module:split-digits-list num1)))
                (begin
                  (hash-set! digits-htable num1 dlist)
                  (set! num1-dlist dlist)
                  ))
              ))
        (if (equal? num2-dlist #f)
            (begin
              (let ((dlist
                     (digits-module:split-digits-list num2)))
                (begin
                  (hash-set! digits-htable num2 dlist)
                  (set! num2-dlist dlist)
                  ))
              ))

        (let ((a1-dlist (sort num1-dlist <))
              (a2-dlist (sort num2-dlist <)))
          (begin
            (equal? a1-dlist a2-dlist)
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-is-num1-permutation-num2-1 result-hash-table)
 (begin
   (let ((sub-name "test-is-num1-permutation-num2-1")
         (test-list
          (list
           (list 2 2 #t) (list 9 9 #t)
           (list 11 11 #t) (list 12 21 #t)
           (list 13 31 #t) (list 99 99 #t)
           (list 123 321 #t) (list 123 124 #f)
           ))
         (split-list (list 2 9 11 12 13))
         (digits-htable (make-hash-table 20))
         (test-label-index 0))
     (begin
       (populate-digits-hash! digits-htable split-list)

       (for-each
        (lambda (alist)
          (begin
            (let ((num1 (list-ref alist 0))
                  (num2 (list-ref alist 1))
                  (shouldbe (list-ref alist 2)))
              (let ((result
                     (is-num1-permutation-num2?
                      num1 num2 digits-htable)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "num1=~a, num2=~a, "
                        num1 num2))
                      (err-3
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append
                      err-1 err-2 err-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (reproduce-problem-statement)
  (begin
    (let ((num 87109)
          (max-prime 1000)
          (digits-htable (make-hash-table 10)))
      (let ((prime-list
             (make-prime-list max-prime)))
        (let ((totient
               (totient-function num prime-list)))
          (let ((totient-ratio
                 (exact->inexact (/ num totient)))
                (perm-flag
                 (is-num1-permutation-num2?
                  num totient digits-htable)))
            (begin
              (display
               (ice-9-format:format
                #f "n=~:d, totient = ~:d, ratio = ~1,6f~%"
                num totient totient-ratio))
              (display
               (ice-9-format:format
                #f "is-permutation=~a~%"
                (if (equal? perm-flag #t) "true" "false")))
              (force-output)
              ))
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (find-min-totient-ratio
         depth max-depth index end-index
         max-num prime-list digits-htable
         current-num current-list
         acc-min-ratio acc-list)
  (begin
    (if (>= depth max-depth)
        (begin
          (let ((totient
                 (srfi-1:fold
                  (lambda (anum prev)
                    (begin
                      (* (1- anum) prev)
                      ))
                  1 current-list)))
            (begin
              (let ((ratio
                     (exact->inexact (/ current-num totient))))
                (begin
                  (if (or (< acc-min-ratio 0)
                          (< ratio acc-min-ratio))
                      (begin
                        (if (and
                             (<= current-num max-num)
                             (is-num1-permutation-num2?
                              current-num totient digits-htable))
                            (begin
                              (set! acc-min-ratio ratio)
                              (set!
                               acc-list
                               (list current-num totient
                                     ratio current-list))
                              ))
                        ))
                  ))
              (list acc-min-ratio acc-list)
              )))
        (begin
          (let ((next-depth (1+ depth))
                (break-flag #f))
            (begin
              (do ((ii index (1+ ii)))
                  ((or (>= ii end-index)
                       (equal? break-flag #t)))
                (begin
                  (let ((aprime (list-ref prime-list ii)))
                    (let ((next-index (1+ ii))
                          (next-current-list
                           (cons aprime current-list))
                          (nn (* current-num aprime)))
                      (begin
                        (if (<= nn max-num)
                            (begin
                              (let ((result-list
                                     (find-min-totient-ratio
                                      next-depth max-depth
                                      next-index end-index
                                      max-num prime-list digits-htable
                                      nn next-current-list
                                      acc-min-ratio acc-list)))
                                (begin
                                  (if (and (list? result-list)
                                           (> (length result-list) 0))
                                      (begin
                                        (let ((next-ratio
                                               (list-ref result-list 0))
                                              (next-acc-list
                                               (list-ref result-list 1)))
                                          (begin
                                            (set! acc-min-ratio next-ratio)
                                            (set! acc-list next-acc-list)
                                            ))
                                        ))
                                  )))
                            (begin
                              (set! break-flag #t)
                              ))
                        )))
                  ))
              (list acc-min-ratio acc-list)
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop start-num end-num max-prime max-num)
  (begin
    (let ((prime-list
           (make-prime-list max-prime))
          (digits-htable (make-hash-table 1000))
          (max-nn -1)
          (max-totient -1)
          (dmax-ratio -1)
          (factors-list (list)))
      (let ((end-index (length prime-list)))
        (begin
          (populate-digits-hash! digits-htable prime-list)

          (do ((ii start-num (1+ ii)))
              ((> ii end-num))
            (begin
              (let ((rlist
                     (find-min-totient-ratio
                      0 ii 0 end-index max-num
                      prime-list digits-htable
                      1 (list) dmax-ratio (list))))
                (begin
                  (if (and (list? rlist)
                           (> (length rlist) 0))
                      (begin
                        (let ((ii-min-ratio (list-ref rlist 0))
                              (acc-list (list-ref rlist 1)))
                          (begin
                            (if (and (list? acc-list)
                                     (> (length acc-list) 0))
                                (begin
                                  (let ((nn (list-ref acc-list 0))
                                        (totient (list-ref acc-list 1))
                                        (rr (list-ref acc-list 2))
                                        (flist (list-ref acc-list 3)))
                                    (begin
                                      (if (or (<= dmax-ratio 0)
                                              (< ii-min-ratio dmax-ratio))
                                          (begin
                                            (set! dmax-ratio ii-min-ratio)
                                            (set! max-nn nn)
                                            (set! max-totient totient)
                                            (set! factors-list flist)
                                            ))
                                      ))
                                  ))
                            ))
                        ))
                  ))

              (display
               (ice-9-format:format
                #f "completed ~:d : ratio so far=~1,6f~%"
                ii dmax-ratio))
              (display
               (ice-9-format:format
                #f "nn=~:d, totient=~:d~%"
                max-nn max-totient))
              (display
               (ice-9-format:format
                #f "factors list = ~a~%"
                factors-list))
              (newline)
              (force-output)
              ))

          (if (> dmax-ratio 0)
              (begin
                (display
                 (ice-9-format:format
                  #f "for numbers that are products of primes~%"))
                (display
                 (ice-9-format:format
                  #f "ranging from ~:d to ~:d factors~%"
                  start-num end-num))
                (let ((perm-flag
                       (is-num1-permutation-num2?
                        max-nn max-totient digits-htable)))
                  (begin
                    (display
                     (ice-9-format:format
                      #f "min ratio = ~1,6f, num=~:d, totient=~:d~%"
                      dmax-ratio max-nn max-totient))
                    (display
                     (ice-9-format:format
                      #f "factors-list=~a, is-permutation=~a~%"
                      factors-list (if (equal? perm-flag #t) "true" "false")))
                    (force-output)
                    )))
              (begin
                (display
                 (ice-9-format:format
                  #f "no numbers found between ~:d to ~:d factors~%"
                  start-num end-num))
                ))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Euler's Totient function, phi(n) "))
    (display
     (format #f "[sometimes called the~%"))
    (display
     (format #f "phi function], is used to determine "))
    (display
     (format #f "the number of positive~%"))
    (display
     (format #f "numbers less than or equal to n which "))
    (display
     (format #f "are relatively prime~%"))
    (display
     (format #f "to n. For example, as 1, 2, 4, 5, 7, "))
    (display
     (format #f "and 8, are all~%"))
    (display
     (format #f "less than nine and relatively prime "))
    (display
     (format #f "to nine, phi(9)=6.~%"))
    (newline)
    (display
     (format #f "The number 1 is considered to be "))
    (display
     (format #f "relatively prime to every~%"))
    (display
     (format #f "positive number, so phi(1)=1.~%"))
    (newline)
    (display
     (format #f "Interestingly, phi(87109)=79180, and "))
    (display
     (format #f "it can be seen that~%"))
    (display
     (format #f "87109 is a permutation of 79180.~%"))
    (newline)
    (display
     (format #f "Find the value of n, 1 < n < 10^7, "))
    (display
     (format #f "for which phi(n) is~%"))
    (display
     (format #f "a permutation of n and the ratio n/phi(n) "))
    (display
     (format #f "produces a minimum.~%"))
    (newline)
    (display
     (format #f "The solution can be found at~%"))
    (display
     (format #f "https://martin-ueding.de/posts/project-euler-solution-70-totient-permutation/~%"))
    (display
     (format #f "and the description of the totient "))
    (display
     (format #f "function at~%"))
    (display
     (format #f "https://en.wikipedia.org/wiki/Totient_function~%"))
    (display
     (format #f "phi(n) = n x (1 - 1/p1) x (1 - 1/p2) x "))
    (display
     (format #f "... x (1 - 1/pk)~%"))
    (display
     (format #f "where the prime factors of n are "))
    (display
     (format #f "p1, p2, ..., pk.~%"))
    (newline)
    (display
     (format #f "Since we are looking for a minimum "))
    (display
     (format #f "ratio n/phi(n), this~%"))
    (display
     (format #f "means that we should look for the "))
    (display
     (format #f "smallest n, with the~%"))
    (display
     (format #f "largest phi(n).~%"))
    (display
     (format #f "The largest value of phi occurs when n "))
    (display
     (format #f "contains a few large~%"))
    (display
     (format #f "primes. This program looks for the "))
    (display
     (format #f "minimum ratio of~%"))
    (display
     (format #f "n/phi(n) that can be found for 2, 3, 4, "))
    (display
     (format #f "... prime factors.~%"))
    (display
     (format #f "see https://projecteuler.net/problem=70~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (reproduce-problem-statement)

    (newline)
    (let ((start-num 2)
          (end-num 4)
          (max-prime 10000)
          (max-num 10000000))
      (begin
        (sub-main-loop start-num end-num max-prime max-num)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 70 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (load "prime-module-tests-1.scm")
                 (load "digits-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

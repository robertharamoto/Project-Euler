#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 64                                     ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated July 21, 2022                                ###
;;;###                                                       ###
;;;###  updated March 6, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### srfi-11 for let-values functions
(use-modules ((srfi srfi-11)
              :renamer (symbol-prefix-proc 'srfi-11:)))

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
;;; define a macro to simplify code
(define-syntax sqrt-cf-iteration-macro
  (syntax-rules ()
    ((sqrt-cf-iteration-macro
      int-num int-sqrt-num m0 d0 a0
      seen-htable seen-list
      result-list loop-continue-flag)
     (begin
       (let ((mn (- (* d0 a0) m0)))
         (let ((dn (euclidean/ (- int-num (* mn mn)) d0)))
           (let ((an (euclidean/ (+ int-sqrt-num mn) dn)))
             (begin
               (let ((next-triple (list mn dn an)))
                 (let ((hflag
                        (hash-ref seen-htable next-triple #f)))
                   (begin
                     (if (equal? hflag #f)
                         (begin
                           (hash-set! seen-htable next-triple #t))
                         (begin
                           (set! loop-continue-flag #f)
                           ))
                     (set! m0 mn)
                     (set! d0 dn)
                     (set! a0 an)
                     (set! seen-list (cons next-triple seen-list))
                     (set! result-list (cons an result-list)))
                   ))
               ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
;;; method from
;;; https://en.wikipedia.org/wiki/Methods_of_computing_square_roots
(define (sqrt-continued-fraction-list int-num)
  (begin
    (srfi-11:let-values
     (((int-sqrt-num int-sqrt-remainder)
       (exact-integer-sqrt int-num)))
     (begin
       (if (zero? int-sqrt-remainder)
           (begin
             #f)
           (begin
             (let ((m0 0)
                   (d0 1)
                   (a0 int-sqrt-num)
                   (result-list (list int-sqrt-num))
                   (seen-htable (make-hash-table))
                   (seen-list (list (list 0 1 int-sqrt-num)))
                   (loop-continue-flag #t))
               (begin
                 (hash-set! seen-htable (list m0 d0 a0) #t)

                 (while
                  (equal? loop-continue-flag #t)
                  (begin
                    (sqrt-cf-iteration-macro
                     int-num int-sqrt-num m0 d0 a0
                     seen-htable seen-list
                     result-list loop-continue-flag)
                    ))

                 (let ((last-triple (list m0 d0 a0))
                       (rseen-list (reverse seen-list)))
                   (let ((llist (member last-triple rseen-list)))
                     (begin
                       (if (list? llist)
                           (begin
                             (let ((period (- (length llist) 1)))
                               (begin
                                 (list period (reverse result-list))
                                 )))
                           (begin
                             #f
                             ))
                       )))
                 ))
             ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-sqrt-continued-fraction-list-1 result-hash-table)
 (begin
   (let ((sub-name "test-sqrt-continued-fraction-list-1")
         (test-list
          (list
           (list 2 (list 1 (list 1 2 2)))
           (list 3 (list 2 (list 1 1 2 1)))
           (list 4 #f)
           (list 5 (list 1 (list 2 4 4)))
           (list 6 (list 2 (list 2 2 4 2)))
           (list 7 (list 4 (list 2 1 1 1 4 1)))
           (list 8 (list 2 (list 2 1 4 1)))
           (list 9 #f)
           (list 10 (list 1 (list 3 6 6)))
           (list 11 (list 2 (list 3 3 6 3)))
           (list 12 (list 2 (list 3 2 6 2)))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-num (list-ref alist 0))
                  (shouldbe-list (list-ref alist 1)))
              (let ((result-list
                     (sqrt-continued-fraction-list test-num)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : test-num=~a, "
                        sub-name test-label-index test-num))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a~%"
                        shouldbe-list result-list)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-list result-list)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (cf-list-to-string alist)
  (begin
    (let ((alen (length alist)))
      (let ((blist (list-head alist (- alen 1))))
        (let ((cvalue (car blist))
              (clist (cdr blist)))
          (begin
            (let ((s1-tmp
                   (string-join
                    (map
                     (lambda (anum)
                       (begin
                         (number->string anum)
                         )) clist)
                    ", ")))
              (let ((s2-tmp
                     (string-append
                      (number->string cvalue)
                      " ; (" s1-tmp ")")))
                (begin
                  (string-append "[ " s2-tmp " ]")
                  )))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-cf-list-to-string-1 result-hash-table)
 (begin
   (let ((sub-name "test-cf-list-to-string-1")
         (test-list
          (list
           (list (list 2 1 1) "[ 2 ; (1) ]")
           (list (list 1 1 2 1) "[ 1 ; (1, 2) ]")
           (list (list 2 4 4) "[ 2 ; (4) ]")
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((input-list (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result (cf-list-to-string input-list)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : input-list=~a, "
                        sub-name test-label-index input-list))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a~%"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop max-num debug-flag)
  (begin
    (let ((ntotals 0)
          (nsquares 0)
          (ncontinued 0)
          (nodds 0))
      (begin
        (do ((ii 2 (+ ii 1)))
            ((> ii max-num))
          (begin
            (set! ntotals (+ ntotals 1))

            (let ((results-list
                   (sqrt-continued-fraction-list ii)))
              (begin
                (if (not (equal? results-list #f))
                    (begin
                      (let ((period (list-ref results-list 0))
                            (cf-list (list-ref results-list 1)))
                        (begin
                          (set! ncontinued (+ ncontinued 1))

                          (if (equal? debug-flag #t)
                              (begin
                                (display
                                 (ice-9-format:format
                                  #f "    sqrt(~:d) = ~a, "
                                  ii (cf-list-to-string cf-list)))
                                (display
                                 (ice-9-format:format
                                  #f "period length = ~:d~%" period))
                                (force-output)
                                ))
                          (if (odd? period)
                              (begin
                                (set! nodds (+ nodds 1))
                                ))
                          )))

                    (begin
                      (set! nsquares (+ nsquares 1))
                      ))
                ))
            ))

        (newline)
        (display
         (ice-9-format:format
          #f "For 2 <= n <= ~:d~%" max-num))
        (display
         (ice-9-format:format
          #f "There are ~:d continued fractions with an odd period,~%"
          nodds))
        (display
         (ice-9-format:format
          #f "out of ~:d continued fractions. In addition~%"
          ncontinued))
        (display
         (ice-9-format:format
          #f "there were ~:d squares, for a total = ~:d~%"
          nsquares ntotals))
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "All square roots are periodic when "))
    (display
     (format #f "written as continued~%"))
    (display
     (format #f "fractions and can be written in "))
    (display
     (format #f "the form:~%"))
    (newline)
    (display
     (format #f "sqrt(N) = a0 + 1/(a1 + 1/(a2 + "))
    (display
     (format #f "1/(a3 + ...)))~%"))
    (display
     (format #f "The process can be summarised as follows:~%"))
    (display
     (format #f "a0=4, 1/(sqrt(23) - 4) = (sqrt(23) + 4)/7 "))
    (display
     (format #f "= 1 + (sqrt(23) - 3)/7~%"))
    (display
     (format #f "a1=1, 7/(sqrt(23) - 3) = (sqrt(23) + 3)/2 "))
    (display
     (format #f "= 3 + (sqrt(23) - 3)/2~%"))
    (display
     (format #f "a2=3, 2/(sqrt(23) - 3) = (sqrt(23) + 3)/7 "))
    (display
     (format #f "= 1 + (sqrt(23) - 4)/7~%"))
    (display
     (format #f "a3=1, 7/(sqrt(23) - 4) = 7*(sqrt(23) + 4)/7 "))
    (display
     (format #f "= 8 + (sqrt(23) - 4)~%"))
    (display
     (format #f "a4=8, 1/(sqrt(23) - 4) = (sqrt(23) + 4)/7 "))
    (display
     (format #f "= 1 + (sqrt(23) - 3)/7~%"))
    (display
     (format #f "a5=1, 7/(sqrt(23) - 3) = (sqrt(23) + 3)/2 "))
    (display
     (format #f "= 3 + (sqrt(23) - 3)/2~%"))
    (display
     (format #f "a6=3, 2/(sqrt(23) - 3) = 2*(sqrt(23) + 4)/14 "))
    (display
     (format #f "= 1 + (sqrt(23) - 4)/7~%"))
    (display
     (format #f "a7=1, 7/(sqrt(23) - 4) = 7*(sqrt(23) + 4)/7 "))
    (display
     (format #f "= 8 + (sqrt(23) - 4)~%"))
    (newline)
    (display
     (format #f "It can be seen that the sequence "))
    (display
     (format #f "is repeating. For~%"))
    (display
     (format #f "conciseness, we use the notation "))
    (display
     (format #f "23 = [4;(1,3,1,8)], to~%"))
    (display
     (format #f "indicate that the block (1,3,1,8) repeats "))
    (display
     (format #f "indefinitely.~%"))
    (display
     (format #f "The first ten continued fraction "))
    (display
     (format #f "representations of~%"))
    (display
     (format #f "(irrational) square roots are:~%"))
    (display
     (format #f " sqrt(2)=[1;(2)], period=1~%"))
    (display
     (format #f " sqrt(3)=[1;(1,2)], period=2~%"))
    (display
     (format #f " sqrt(5)=[2;(4)], period=1~%"))
    (display
     (format #f " sqrt(6)=[2;(2,4)], period=2~%"))
    (display
     (format #f " sqrt(7)=[2;(1,1,1,4)], period=4~%"))
    (display
     (format #f " sqrt(8)=[2;(1,4)], period=2~%"))
    (display
     (format #f " sqrt(10)=[3;(6)], period=1~%"))
    (display
     (format #f " sqrt(11)=[3;(3,6)], period=2~%"))
    (display
     (format #f " sqrt(12)= [3;(2,6)], period=2~%"))
    (display
     (format #f " sqrt(13)=[3;(1,1,1,1,6)], period=5~%"))
    (newline)
    (display
     (format #f "Exactly four continued fractions, "))
    (display
     (format #f "for N <= 13, have~%"))
    (display
     (format #f "an odd period.~%"))
    (newline)
    (display
     (format #f "How many continued fractions for "))
    (display
     (format #f "N <= 10000 have~%"))
    (display
     (format #f "an odd period?~%"))
    (newline)
    (display
     (format #f "Solved this problem using the iterative "))
    (display
     (format #f "algorithm described at~%"))
    (display
     (format #f "https://en.wikipedia.org/wiki/Methods_of_computing_square_roots~%"))
    (display
     (format #f "see https://projecteuler.net/problem=64~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((max-num 13)
          (debug-flag #t))
      (begin
        (sub-main-loop max-num debug-flag)
        ))

    (newline)
    (let ((max-num 10000)
          (debug-flag #f))
      (begin
        (sub-main-loop max-num debug-flag)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 64 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 69                                     ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated July 21, 2022                                ###
;;;###                                                       ###
;;;###  updated March 6, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path
              (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### prime-module for is-prime-array? function
(use-modules ((prime-module)
              :renamer (symbol-prefix-proc 'prime-module:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
;;; make a list of primes less than n
(define (make-prime-list this-num)
  (begin
    (let ((plist (list 2)))
      (begin
        (do ((ii 3 (+ ii 2)))
            ((> ii this-num))
          (begin
            (if (prime-module:is-prime? ii)
                (begin
                  (set! plist (cons ii plist))
                  ))
            ))
        (reverse plist)
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-make-prime-list-1 result-hash-table)
 (begin
   (let ((sub-name "test-make-prime-list-1")
         (test-list
          (list
           (list 2 (list 2)) (list 3 (list 2 3)) (list 4 (list 2 3))
           (list 5 (list 2 3 5)) (list 6 (list 2 3 5))
           (list 7 (list 2 3 5 7)) (list 8 (list 2 3 5 7))
           (list 9 (list 2 3 5 7)) (list 10 (list 2 3 5 7))
           (list 11 (list 2 3 5 7 11)) (list 13 (list 2 3 5 7 11 13))
           (list 17 (list 2 3 5 7 11 13 17))
           (list 19 (list 2 3 5 7 11 13 17 19))
           (list 23 (list 2 3 5 7 11 13 17 19 23))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((test-num (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result (make-prime-list test-num)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : test-num=~a, "
                        sub-name test-label-index test-num))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a~%"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (single-prime-factor-divisors-list input-number prime-list)
  (begin
    (let ((result-list (list)))
      (begin
        (cond
         ((or (<= input-number 1)
              (not (list? prime-list))
              (< (length prime-list) 1))
          (begin
            (list)
            ))
         ((not
           (equal?
            (member input-number prime-list)
            #f))
          (begin
            (list input-number)
            ))
         (else
          (begin
            (let ((result-list (list))
                  (plen (length prime-list))
                  (pmax (car (last-pair prime-list)))
                  (ll-max (/ input-number 2))
                  (this-prime (car prime-list)))
              (begin
                (do ((ii 0 (1+ ii)))
                    ((or (>= ii plen)
                         (> this-prime ll-max)))
                  (begin
                    (set!
                     this-prime (list-ref prime-list ii))

                    (if (zero?
                         (modulo input-number this-prime))
                        (begin
                          (set!
                           result-list
                           (cons this-prime result-list))
                          ))
                    ))

                (if (< pmax ll-max)
                    (begin
                      (display
                       (ice-9-format:format
                        #f "warning max generated prime = ~:d~%"
                        pmax))
                      (display
                       (ice-9-format:format
                        #f "less than required sqrt(~:d) = ~:d~%"
                        input-number ll-max))
                      (display
                       (format #f "quitting...~%"))
                      (quit)
                      ))

                (reverse result-list)
                ))
            )))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-single-prime-factor-divisors-list-1 result-hash-table)
 (begin
   (let ((sub-name "test-single-prime-factor-divisors-list-1")
         (test-list
          (list
           (list 2 (list 2))
           (list 3 (list 3))
           (list 4 (list 2))
           (list 5 (list 5))
           (list 6 (list 2 3))
           (list 7 (list 7))
           (list 8 (list 2))
           (list 9 (list 3))
           (list 10 (list 2 5))
           (list 11 (list 11))
           (list 12 (list 2 3))
           (list 13 (list 13))
           (list 14 (list 2 7))
           (list 15 (list 3 5))
           (list 16 (list 2))
           (list 17 (list 17))
           (list 18 (list 2 3))
           (list 19 (list 19))
           (list 20 (list 2 5))
           (list 21 (list 3 7))
           (list 22 (list 2 11))
           (list 23 (list 23))
           (list 24 (list 2 3))
           (list 25 (list 5))
           (list 26 (list 2 13))
           (list 27 (list 3))
           (list 28 (list 2 7))
           (list 29 (list 29))
           (list 30 (list 2 3 5))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((test-num (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((plist (make-prime-list test-num)))
                (let ((result
                       (single-prime-factor-divisors-list
                        test-num plist)))
                  (let ((err-1
                         (format
                          #f "~a : error (~a) : test-num=~a, "
                          sub-name test-label-index test-num))
                        (err-2
                         (format
                          #f "shouldbe=~a, result=~a"
                          shouldbe result)))
                    (begin
                      (unittest2:assert?
                       (equal? shouldbe result)
                       sub-name
                       (string-append err-1 err-2)
                       result-hash-table)
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; count the number of relatively prime numbers to n
;;; if n even, all even numbers are not relatively prime
(define (totient-function nn prime-list)
  (begin
    (let ((tproduct nn)
          (ll-max (/ nn 2))
          (pmax (car (last-pair prime-list)))
          (sprime-list
           (single-prime-factor-divisors-list
            nn prime-list)))
      (let ((splen (length sprime-list)))
        (begin
          (cond
           ((or (<= nn 1)
                (> ll-max pmax))
            (begin
              (set! tproduct -1)
              ))
           (else
            (begin
              (do ((ii 0 (1+ ii)))
                  ((>= ii splen))
                (begin
                  (set! tproduct
                        (* tproduct
                           (- 1 (/ 1 (list-ref sprime-list ii)))
                           ))
                  ))
              )))
          tproduct
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-totient-function-1 result-hash-table)
 (begin
   (let ((sub-name "test-totient-function-1")
         (test-list
          (list
           (list 2 1) (list 3 2) (list 4 2)
           (list 5 4) (list 6 2) (list 7 6)
           (list 8 4) (list 9 6) (list 10 4)
           (list 11 10) (list 12 4) (list 13 12)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-num (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((prime-list (make-prime-list test-num)))
                (let ((result
                       (totient-function test-num prime-list)))
                  (let ((err-1
                         (format
                          #f "~a : error (~a) : test-num=~a, "
                          sub-name test-label-index test-num))
                        (err-2
                         (format
                          #f "shouldbe=~a, result=~a"
                          shouldbe result)))
                    (begin
                      (unittest2:assert?
                       (equal? shouldbe result)
                       sub-name
                       (string-append err-1 err-2)
                       result-hash-table)
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (totient-list-function nn)
  (begin
    (let ((result-list (list)))
      (begin
        (do ((ii 1 (+ ii 1)))
            ((>= ii nn))
          (begin
            (if (equal? (gcd nn ii) 1)
                (begin
                  (set! result-list (cons ii result-list))
                  ))
            ))
        (reverse result-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-totient-list-function-1 result-hash-table)
 (begin
   (let ((sub-name "test-totient-list-function-1")
         (test-list
          (list
           (list 2 (list 1)) (list 3 (list 1 2))
           (list 4 (list 1 3)) (list 5 (list 1 2 3 4))
           (list 6 (list 1 5)) (list 7 (list 1 2 3 4 5 6))
           (list 8 (list 1 3 5 7)) (list 9 (list 1 2 4 5 7 8))
           (list 10 (list 1 3 7 9))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-num (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result
                     (totient-list-function test-num)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : test-num=~a, "
                        sub-name test-label-index test-num))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (range-loop start-num end-num prime-list)
  (begin
    (let ((max-nn -1)
          (max-totient -1)
          (max-ratio -1))
      (begin
        (do ((ii start-num (1+ ii)))
            ((>= ii end-num))
          (begin
            (let ((totient
                   (totient-function ii prime-list)))
              (let ((totient-ratio (/ ii totient)))
                (begin
                  (if (> totient-ratio max-ratio)
                      (begin
                        (set! max-nn ii)
                        (set! max-totient totient)
                        (set! max-ratio totient-ratio)
                        ))
                  )))
            ))
        (list max-nn max-totient max-ratio)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (exhaustive-method max-prime start-num end-num)
  (begin
    (let ((prime-list
           (make-prime-list max-prime)))
      (let ((result-list
             (range-loop start-num end-num prime-list)))
        (let ((max-nn (list-ref result-list 0))
              (max-totient (list-ref result-list 1))
              (max-ratio (list-ref result-list 2)))
          (let ((dmax-ratio
                 (* 0.00010
                    (truncate
                     (* 10000.0 max-ratio)))))
            (begin
              (display
               (ice-9-format:format
                #f "exhaustive method : n = ~:d, produces a~%"
                max-nn))
              (display
               (ice-9-format:format
                #f "maximum n/phi(n) = ~:d / ~:d = ~a~%"
                max-nn max-totient dmax-ratio))
              (display
               (ice-9-format:format
                #f "(for n <= ~:d)~%"
                end-num))
              (force-output)
              )))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop max-prime max-num)
  (begin
    (let ((max-nn 2)
          (max-ratio (/ 1 2))
          (prime-list (list 2))
          (loop-continue-flag #t))
      (begin
        (do ((ii 3 (+ ii 2)))
            ((or (> ii max-prime)
                 (equal? loop-continue-flag #f)))
          (begin
            (if (prime-module:is-prime? ii)
                (begin
                  (let ((next-nn (* max-nn ii)))
                    (begin
                      (if (< next-nn max-num)
                          (begin
                            (set! max-nn next-nn)
                            (set!
                             max-ratio
                             (* max-ratio
                                (- 1 (/ 1 ii))))
                            (set! prime-list (cons ii prime-list)))
                          (begin
                            (set! loop-continue-flag #f)
                            ))
                      ))
                  ))
            ))

        (let ((max-totient (* max-nn max-ratio))
              (div-string
               (string-join
                (map
                 (lambda (num)
                   (begin
                     (ice-9-format:format #f "~:d" num)
                     )) (reverse prime-list))
                ", "))
              (dmax-ratio
               (* 0.00010
                  (truncate
                   (/ 10000.0 max-ratio)))))
          (begin
            (display
             (ice-9-format:format
              #f "n = ~:d, produces a maximum "
              max-nn))
            (display
             (ice-9-format:format
              #f "n/phi(n) = ~:d / ~:d = ~a.~%"
              max-nn max-totient
              dmax-ratio))
            (display
             (ice-9-format:format
              #f "prime factors of n = ~a : (for n <= ~:d)~%"
              div-string max-num))
            (force-output)
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Euler's Totient function, phi(n) "))
    (display
     (format #f "[sometimes called the~%"))
    (display
     (format #f "phi function], is used to determine "))
    (display
     (format #f "the number of numbers~%"))
    (display
     (format #f "less than n which are relatively "))
    (display
     (format #f "prime to n. For~%"))
    (display
     (format #f "example, as 1, 2, 4, 5, 7, and 8, "))
    (display
     (format #f "are all less than~%"))
    (display
     (format #f "nine and relatively prime to nine, "))
    (display
     (format #f "phi(9)=6.~%"))
    (newline)
    (display
     (format #f "n      Relatively Prime        phi(n)  "))
    (display
     (format #f "n/phi(n)~%"))
    (display
     (format #f "2      1       1       2~%"))
    (display
     (format #f "3      1,2     2       1.5~%"))
    (display
     (format #f "4      1,3     2       2~%"))
    (display
     (format #f "5      1,2,3,4 4       1.25~%"))
    (display
     (format #f "6      1,5     2       3~%"))
    (display
     (format #f "7      1,2,3,4,5,6     6       1.1666...~%"))
    (display
     (format #f "8      1,3,5,7 4       2~%"))
    (display
     (format #f "9      1,2,4,5,7,8     6       1.5~%"))
    (display
     (format #f "10     1,3,7,9 4       2.5~%"))
    (newline)
    (display
     (format #f "It can be seen that n=6 produces a "))
    (display
     (format #f "maximum n/phi(n) for~%"))
    (display
     (format #f "n <= 10.~%"))
    (newline)
    (display
     (format #f "Find the value of n <= 1,000,000 for "))
    (display
     (format #f "which n/phi(n) is a~%"))
    (display
     (format #f "maximum.~%"))
    (newline)
    (display
     (format #f "From the definition of Euler's totient "))
    (display
     (format #f "function https://en.wikipedia.org/wiki/Totient_function~%"))
    (newline)
    (display
     (format #f "phi(n) = n x (1 - 1/p1) x (1 - 1/p2) x "))
    (display
     (format #f "... x (1 - 1/pk)~%"))
    (display
     (format #f "where the prime factors of n are "))
    (display
     (format #f "p1, p2, ..., pk.~%"))
    (newline)
    (display
     (format #f "The solution was given at~%"))
    (display
     (format #f "https://blog.dreamshire.com/project-euler-69-solution/~%"))
    (display
     (format #f "and notes that to maximize the ratio "))
    (display
     (format #f "n / phi(n), you need~%"))
    (display
     (format #f "a number that is as close as possible "))
    (display
     (format #f "to 1 million, but~%"))
    (display
     (format #f "as small a phi(n) as possible. This happens "))
    (display
     (format #f "when n is made up~%"))
    (display
     (format #f "of a product of primes that is closest "))
    (display
     (format #f "to 1 million.~%"))
    (newline)
    (display
     (format #f "The problem was solved this way in this "))
    (display
     (format #f "program.~%"))
    (display
     (format #f "see https://projecteuler.net/problem=69~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((max-prime 1000)
          (start-num 1)
          (end-num 100))
      (begin
        (exhaustive-method max-prime start-num end-num)
        (newline)
        (sub-main-loop max-prime end-num)
        ))

    (newline)
    (let ((max-prime 10000)
          (end-num 1000000))
      (begin
        (sub-main-loop max-prime end-num)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 69 (version ~a)"
                     version-string)))
        (begin
         ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (load "prime-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

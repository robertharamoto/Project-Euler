#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 75                                     ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated July 21, 2022                                ###
;;;###                                                       ###
;;;###  updated March 7, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
;;; define a macro to simplify code
(define-syntax process-mm-nn
  (syntax-rules ()
    ((process-mm-nn
      mm nn mm-2 nn-2 max-perimeter
      perimeter-htable result-count)
     (begin
       (let ((aa (- mm-2 nn-2))
             (bb (* 2 mm nn))
             (cc (+ mm-2 nn-2)))
         (let ((pp (+ aa bb cc))
               (tmp-pp (+ aa bb cc)))
           (begin
             (while
              (< tmp-pp max-perimeter)
              (begin
                (let ((count
                       (hash-ref perimeter-htable tmp-pp 0)))
                  (begin
                    (cond
                     ((= count 0)
                      (begin
                        (set! result-count (1+ result-count))
                        (hash-set! perimeter-htable tmp-pp 1)
                        ))
                     ((= count 1)
                      (begin
                        (set! result-count (1- result-count))
                        (hash-set! perimeter-htable tmp-pp 2)
                        )))

                    (hash-set! perimeter-htable tmp-pp (1+ count))
                    ))
                (set! tmp-pp (+ tmp-pp pp))
                ))
             )))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (calculate-number-of-elements max-perimeter max-kk)
  (begin
    (let ((mm-max
           (1+ (exact-integer-sqrt
                (euclidean/ max-perimeter 2))))
          (perimeter-htable (make-hash-table 10000))
          (result-count 0))
      (begin
        (do ((mm 1 (1+ mm)))
            ((> mm mm-max))
          (begin
            (let ((mm-2 (* mm mm)))
              (begin
                (do ((nn 1 (1+ nn)))
                    ((>= nn mm))
                  (begin
                    (if (and (equal? (gcd mm nn) 1)
                             (odd? (- mm nn)))
                        (begin
                          (let ((nn-2 (* nn nn)))
                            (begin
                              (process-mm-nn
                               mm nn mm-2 nn-2 max-perimeter
                               perimeter-htable result-count)
                              ))
                          ))
                    ))
                ))
            ))

        result-count
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-calculate-number-of-elements-1 result-hash-table)
 (begin
   (let ((sub-name "test-calculate-number-of-elements-1"))
     (let ((test-list
            (list
             (list 15 4 1)
             (list 20 4 1)
             (list 25 4 2)
             (list 32 4 3)
             (list 38 4 4)
             (list 42 4 5)
             (list 50 4 6)
             ))
           (test-label-index 0))
       (for-each
        (lambda (alist)
          (begin
            (let ((max-perimeter (list-ref alist 0))
                  (max-kk (list-ref alist 1))
                  (shouldbe (list-ref alist 2)))
              (let ((result
                     (calculate-number-of-elements
                      max-perimeter max-kk)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "max-perimeter=~a, max-kk=~a, "
                        max-perimeter max-kk))
                      (err-3
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append
                      err-1 err-2 err-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop max-perimeter max-kk)
  (begin
    (let ((result
           (calculate-number-of-elements
            max-perimeter max-kk)))
      (begin
        (display
         (ice-9-format:format
          #f "there are ~:d elements with exactly 1 integer~%"
          result))
        (display
         (ice-9-format:format
          #f "sided triangle for L <= ~:d~%"
          max-perimeter))
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "It turns out that 12 cm is the "))
    (display
     (format #f "smallest length of~%"))
    (display
     (format #f "wire that can be bent to form an "))
    (display
     (format #f "integer sided right~%"))
    (display
     (format #f "angle triangle in exactly one way, "))
    (display
     (format #f "but there are~%"))
    (display
     (format #f "many more examples.~%"))
    (newline)
    (display
     (format #f "  12 cm: (3,4,5)~%"))
    (display
     (format #f "  24 cm: (6,8,10)~%"))
    (display
     (format #f "  30 cm: (5,12,13)~%"))
    (display
     (format #f "  36 cm: (9,12,15)~%"))
    (display
     (format #f "  40 cm: (8,15,17)~%"))
    (display
     (format #f "  48 cm: (12,16,20)~%"))
    (newline)
    (display
     (format #f "In contrast, some lengths of wire, "))
    (display
     (format #f "like 20 cm, cannot~%"))
    (display
     (format #f "be bent to form an integer sided "))
    (display
     (format #f "right angle triangle,~%"))
    (display
     (format #f "and other lengths allow more than "))
    (display
     (format #f "one solution to be~%"))
    (display
     (format #f "found; for example, using 120 cm it is "))
    (display
     (format #f "possible to form~%"))
    (display
     (format #f "exactly three different integer sided "))
    (display
     (format #f "right angle triangles.~%"))
    (display
     (format #f "120 cm: (30,40,50), (20,48,52), (24,45,51)~%"))
    (newline)
    (display
     (format #f "Given that L is the length of the "))
    (display
     (format #f "wire, for how many~%"))
    (display
     (format #f "values of L <= 1,500,000 can exactly "))
    (display
     (format #f "one integer sided~%"))
    (display
     (format #f "right angle triangle be formed?~%"))
    (display
     (format #f "Note: This problem has been changed "))
    (display
     (format #f "recently, please check~%"))
    (display
     (format #f "that you are using the right parameters.~%"))
    (newline)
    (display
     (format #f "The solution has been described at~%"))
    (display
     (format #f "https://euler.stephan-brumme.com/75/~%"))
    (newline)
    (display
     (format #f "And the generation of pythagorean "))
    (display
     (format #f "triples can be found at~%"))
    (display
     (format #f "https://en.wikipedia.org/wiki/Pythagorean_triple~%"))
    (display
     (format #f "see https://projecteuler.net/problem=75~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((max-perimeter 50)
          (max-kk 4))
      (begin
        (sub-main-loop max-perimeter max-kk)
        ))

    (newline)
    (let ((max-perimeter 1500000)
          (max-kk 10))
      (begin
        (sub-main-loop max-perimeter max-kk)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 75 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

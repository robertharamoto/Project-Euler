#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 71                                     ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated July 21, 2022                                ###
;;;###                                                       ###
;;;###  updated March 7, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop max-num target-numer target-denom)
  (begin
    (let ((left-numer 0)
          (left-denom 1)
          (right-numer 1)
          (right-denom 1)
          (mid-numer 0)
          (mid-denom 0))
      (begin
        (while
         (<= (+ left-denom right-denom) max-num)
         (begin
            ;;; compute the mediant
           (set! mid-numer (+ left-numer right-numer))
           (set! mid-denom (+ left-denom right-denom))

            ;;; if mid ratio is greater than 3/7, then mid-numer*7 > mid-denom*3
           (if (or
                (and (= mid-numer target-numer)
                     (= mid-denom target-denom))
                (>= (* mid-numer target-denom)
                    (* mid-denom target-numer)))
               (begin
                 (set! right-numer mid-numer)
                 (set! right-denom mid-denom))
               (begin
                 (set! left-numer mid-numer)
                 (set! left-denom mid-denom)
                 ))
           ))

        (let ((string1
               (ice-9-format:format
                #f "~:d/~:d is the fraction that's immediately~%"
                left-numer left-denom))
              (string2
               (ice-9-format:format
                #f "to the left of ~:d/~:d, (for denominators <= ~:d)"
                target-numer target-denom max-num)))
          (begin
            (display
             (ice-9-format:format #f "~a~a~%" string1 string2))
            (force-output)
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Consider the fraction, n/d, where n "))
    (display
     (format #f "and d are positive~%"))
    (display
     (format #f "integers. If nd and HCF(n,d)=1, it "))
    (display
     (format #f "is called a~%"))
    (display
     (format #f "reduced proper fraction.~%"))
    (newline)
    (display
     (format #f "If we list the set of reduced proper "))
    (display
     (format #f "fractions for d <= 8~%"))
    (display
     (format #f "in ascending order of size, we get:~%"))
    (newline)
    (display (format #f "  1/8, 1/7, 1/6, 1/5, 1/4, 2/7, "))
    (display
     (format #f "1/3, 3/8, 2/5, 3/7,~%"))
    (display
     (format #f "1/2, 4/7, 3/5, 5/8, 2/3, 5/7, 3/4, "))
    (display
     (format #f "4/5, 5/6, 6/7, 7/8~%"))
    (newline)
    (display
     (format #f "It can be seen that 2/5 is the fraction "))
    (display
     (format #f "immediately to the~%"))
    (display
     (format #f "left of 3/7.~%"))
    (newline)
    (display
     (format #f "By listing the set of reduced proper "))
    (display
     (format #f "fractions for d <=~%"))
    (display
     (format #f "1,000,000 in ascending order of size, "))
    (display
     (format #f "find the numerator~%"))
    (display
     (format #f "of the fraction immediately to "))
    (display
     (format #f "the left of 3/7.~%"))
    (newline)
    (display
     (format #f "Several solutions can be found at~%"))
    (display
     (format #f "https://euler.stephan-brumme.com/71/~%"))
    (newline)
    (display
     (format #f "This program binary search of the Farey "))
    (display
     (format #f "sequence to find the~%"))
    (display
     (format #f "fraction less than 3/7. See the "))
    (display
     (format #f "wikipedia for a~%"))
    (display
     (format #f "description of the Farey sequence and "))
    (display
     (format #f "it's generation~%"))
    (display
     (format #f "https://en.wikipedia.org/wiki/Farey_sequence~%"))
    (display
     (format #f "see https://projecteuler.net/problem=71~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((max-num 8)
          (target-numer 3)
          (target-denom 7))
      (begin
        (sub-main-loop max-num target-numer target-denom)
        ))

    (newline)
    (let ((max-num 1000000)
          (target-numer 3)
          (target-denom 7))
      (begin
        (sub-main-loop max-num target-numer target-denom)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 71 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

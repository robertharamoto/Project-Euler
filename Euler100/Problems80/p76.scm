#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 76                                     ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated July 21, 2022                                ###
;;;###                                                       ###
;;;###  updated March 7, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (array-to-string this-array)
  (begin
    (let ((stmp "")
          (amax (car (array-dimensions this-array))))
      (begin
        (do ((ii 0 (1+ ii)))
            ((>= ii amax))
          (begin
            (let ((this-num (array-ref this-array ii)))
              (begin
                (if (= ii 0)
                    (begin
                      (let ((this-str
                             (ice-9-format:format
                              #f "~:d" this-num)))
                        (begin
                          (set!
                           stmp (string-append stmp this-str))
                          )))
                    (begin
                      (let ((this-str
                             (ice-9-format:format
                              #f ", ~:d" this-num)))
                        (begin
                          (set!
                           stmp (string-append stmp this-str))
                          ))
                      ))
                ))
            ))
        (string-append "[ " stmp " ]")
        ))
    ))

;;;#############################################################
;;;#############################################################
;;; 1-dimensional dynamic array
(define (compute-sum-count max-num debug-flag)
  (begin
    (let ((dynamic-array (make-array 0 (1+ max-num))))
      (begin
        ;;; initialize
        (array-set! dynamic-array 1 0)

        (if (equal? debug-flag #t)
            (begin
              ;;; print header
              (do ((ii 0 (1+ ii)))
                  ((> ii max-num))
                (begin
                  (if (= ii 0)
                      (begin
                        (display (format #f "        ~:d" ii)))
                      (begin
                        (display (format #f ", ~:d" ii))
                        ))
                  ))
              (newline)
              (force-output)
              ))

        ;;; main loop
        (do ((ii 1 (1+ ii)))
            ((>= ii max-num))
          (begin
            (do ((jj ii (1+ jj)))
                ((> jj max-num))
              (begin
                ;;; then add previous value
                (let ((this-sum (array-ref dynamic-array jj))
                      (sum-left (- jj ii)))
                  (let ((sub-target
                         (array-ref dynamic-array sum-left)))
                    (begin
                      (array-set!
                       dynamic-array
                       (+ this-sum sub-target) jj)
                      )))
                ))

            (if (equal? debug-flag #t)
                (begin
                  (display
                   (ice-9-format:format
                    #f "(~:d) : ~a~%"
                    ii (array-to-string dynamic-array)))
                  (force-output)
                  ))
            ))

        (array-ref dynamic-array max-num)
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-compute-sum-count-1 result-hash-table)
 (begin
   (let ((sub-name "test-compute-sum-count-1")
         (test-list
          (list
           (list 2 1)
           (list 3 2)
           (list 4 4)
           (list 5 6)
           ))
         (debug-flag #f)
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-num (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result
                     (compute-sum-count test-num debug-flag)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : test-num=~a, "
                        sub-name test-label-index test-num))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop max-num debug-flag)
  (begin
    (let ((rlen
           (compute-sum-count max-num debug-flag)))
      (begin
        (display
         (ice-9-format:format
          #f "there are ~:d different ways to write ~:d~%"
          rlen max-num))
        (display
         (ice-9-format:format
          #f "as a sum of at least two positive integers~%"))
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "It is possible to write five as a "))
    (display
     (format #f "sum in exactly six~%"))
    (display
     (format #f "different ways:~%"))
    (newline)
    (display
     (format #f "  4 + 1~%"))
    (display
     (format #f "  3 + 2~%"))
    (display
     (format #f "  3 + 1 + 1~%"))
    (display
     (format #f "  2 + 2 + 1~%"))
    (display
     (format #f "  2 + 1 + 1 + 1~%"))
    (display
     (format #f "  1 + 1 + 1 + 1 + 1~%"))
    (newline)
    (display
     (format #f "How many different ways can one "))
    (display
     (format #f "hundred be written as~%"))
    (display
     (format #f "a sum of at least two positive integers?~%"))
    (newline)
    (display
     (format #f "The solution is described at~%"))
    (display
     (format #f "https://euler.stephan-brumme.com/76/~%"))
    (display
     (format #f "see https://projecteuler.net/problem=76~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((debug-flag #t))
      (begin
        (do ((ii 2 (+ ii 1)))
            ((> ii 5))
          (begin
            (sub-main-loop ii debug-flag)
            (newline)
            ))
        ))

    (newline)
    (let ((max-num 100)
          (debug-flag #f))
      (begin
        (sub-main-loop max-num debug-flag)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 76 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

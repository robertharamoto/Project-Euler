#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 79                                     ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated July 21, 2022                                ###
;;;###                                                       ###
;;;###  updated March 7, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### srfi-1 for fold function
(use-modules ((srfi srfi-1)
              :renamer (symbol-prefix-proc 'srfi-1:)))

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### ice-9 rdelim for reading delimited files
(use-modules ((ice-9 rdelim)
              :renamer (symbol-prefix-proc 'ice-9-rdelim:)))

;;;### digits-module for split-digits-list function
(use-modules ((digits-module)
              :renamer (symbol-prefix-proc 'digits-module:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
;;; returns a list of lists of digits
(define (read-in-keylogs filename)
  (begin
    (let ((result-list (list)))
      (begin
        (if (file-exists? filename)
            (begin
              (with-input-from-file filename
                (lambda ()
                  (do ((line
                        (ice-9-rdelim:read-delimited "\n\r")
                        (ice-9-rdelim:read-delimited "\n\r")))
                      ((eof-object? line))
                    (begin
                      (if (not (eof-object? line))
                          (begin
                            (let ((tmp-line (string-copy line)))
                              (let ((this-string-num
                                     (string-trim-both tmp-line)))
                                (begin
                                  (if (not
                                       (equal?
                                        (string->number this-string-num) #f))
                                      (begin
                                        (let ((num
                                               (string->number this-string-num)))
                                          (let ((dlist
                                                 (digits-module:split-digits-list num)))
                                            (begin
                                              (set!
                                               result-list
                                               (cons dlist result-list))
                                              )))
                                        ))
                                  )))
                            ))
                      ))
                  ))
              ))
        (reverse result-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (make-index-list pwd-list key-list)
  (begin
    (let ((index-list
           (map
            (lambda (this-key)
              (begin
                (srfi-1:list-index
                 (lambda (x)
                   (begin
                     (= x this-key)
                     )) pwd-list)
                )) key-list)
           ))
      (begin
        index-list
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-make-index-list-1 result-hash-table)
 (begin
   (let ((sub-name "test-make-index-list-1")
         (test-list
          (list
           (list (list 3 4 5) (list 3 1 2) (list 0 #f #f))
           (list (list 1 2 3 4 5) (list 5 6 3) (list 4 #f 2))
           (list (list 1 2 3 4 5) (list 7 2 9) (list #f 1 #f))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((pwd-list (list-ref alist 0))
                  (key-list (list-ref alist 1))
                  (shouldbe-list (list-ref alist 2)))
              (let ((result-list (make-index-list pwd-list key-list)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "pwd-list=~a, key-list=~a, "
                        pwd-list key-list))
                      (err-3
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe-list result-list)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-list result-list)
                     sub-name
                     (string-append
                      err-1 err-2 err-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (add-new-keys input-list key-list)
  (begin
    (let ((pwd-list (list-copy input-list))
          (last-elem-2 #f)
          (last-elem-1 (car key-list))
          (elem-tail-list (cdr key-list)))
      (let ((last-index-1
             (srfi-1:list-index
              (lambda (x)
                (begin
                  (= x last-elem-1)
                  )) pwd-list))
            (last-index-2 #f))
        (let ((tlen (length elem-tail-list)))
          (begin
            (do ((jj 0 (1+ jj)))
                ((>= jj tlen))
              (begin
                (let ((this-elem
                       (list-ref elem-tail-list jj)))
                  (let ((this-index
                         (srfi-1:list-index
                          (lambda (x)
                            (begin
                              (= x this-elem)
                              )) pwd-list)))
                    (begin
                      (cond
                       ((and (equal? last-index-1 #f)
                             (equal? this-index #f))
                        (begin
                          ;;; just append last-elem to the end
                          (if (equal? jj (- tlen 1))
                              (begin
                                (let ((insert-list
                                       (list last-elem-1 this-elem)))
                                  (begin
                                    (if (not (equal? last-elem-2 #f))
                                        (begin
                                          (set!
                                           insert-list
                                           (cons last-elem-2 insert-list))
                                          (set! last-elem-2 #f)
                                          (set! last-index-2 #f)
                                          ))
                                    (set!
                                     pwd-list
                                     (append pwd-list insert-list))
                                    )))
                              (begin
                                (if (equal? last-elem-2 #f)
                                    (begin
                                      (set! last-elem-2 last-elem-1)
                                      (set! last-index-2 last-index-1))
                                    (begin
                                      (set!
                                       pwd-list
                                       (append pwd-list (list last-elem-2)))
                                      (set! last-elem-2 #f)
                                      (set! last-index-2 #f)
                                      ))
                                ))
                          ))
                       ((and (equal? last-index-1 #f)
                             (not (equal? this-index #f)))
                        (begin
                          ;;; add last-elem just before this-elem
                          (let ((insert-list (list last-elem-1)))
                            (begin
                              (if (not (equal? last-elem-2 #f))
                                  (begin
                                    (set!
                                     insert-list
                                     (cons last-elem-2 insert-list))
                                    (set! last-elem-2 #f)
                                    (set! last-index-2 #f)
                                    ))
                              (let ((hlist
                                     (list-head pwd-list this-index))
                                    (tlist
                                     (list-tail pwd-list this-index)))
                                (let ((next-list
                                       (append hlist insert-list tlist)))
                                  (begin
                                    (set! pwd-list next-list)
                                    )))
                              ))
                          ))
                       ((and (not (equal? last-index-1 #f))
                             (equal? this-index #f))
                        (begin
                          ;;; just append last-elem to the end
                          (if (equal? jj (- tlen 1))
                              (begin
                                (set!
                                 pwd-list
                                 (append pwd-list (list this-elem)))
                                ))
                          ))
                       (else
                        (begin
                          ;;; both indicies were found in pwd-list
                          ;;; may need to swap places to ensure consistancy
                          (if (> last-index-1 this-index)
                              (begin
                                (list-set!
                                 pwd-list this-index last-elem-1)
                                (list-set!
                                 pwd-list last-index-1 this-elem)
                                ))
                          )))

                      (set! last-elem-1 this-elem)
                      (set! last-index-1 this-index)
                      )))
                ))
            pwd-list
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-add-new-keys-1 result-hash-table)
 (begin
   (let ((sub-name "test-add-new-keys-1")
         (test-list
          (list
           (list (list 3 4 5 8) (list 1 2 6) (list 3 4 5 8 1 2 6))
           (list (list 3 4 5 8) (list 1 4 6) (list 3 1 4 5 8 6))
           (list (list 3 4 5 8) (list 1 4 8) (list 3 1 4 5 8))
           (list (list 3 4 5 8) (list 3 1 5) (list 3 4 1 5 8))
           (list (list 3 4 5 8) (list 5 1 9) (list 3 4 5 8 1 9))
           (list (list 3 4 5 8) (list 1 2 3) (list 1 2 3 4 5 8))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((pwd-list (list-ref alist 0))
                  (key-list (list-ref alist 1))
                  (shouldbe-list (list-ref alist 2)))
              (let ((result-list
                     (add-new-keys pwd-list key-list)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "pwd-list=~a, key-list=~a, "
                        pwd-list key-list))
                      (err-3
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe-list result-list)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-list result-list)
                     sub-name
                     (string-append
                      err-1 err-2 err-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; password list is a list of digits
(define (calc-password-list kl-list-list)
  (begin
    (let ((pwd-list (list-copy (car kl-list-list)))
          (first-list (car kl-list-list))
          (tail-list (cdr kl-list-list))
          (last-index 0))
      (begin
        ;;; insert into pwd-list
        (for-each
         (lambda (a-list)
           (begin
             (let ((next-list
                    (add-new-keys pwd-list a-list)))
               (begin
                 (set! pwd-list next-list)
                 ))
             ))
         tail-list)

        pwd-list
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-calc-password-list-1 result-hash-table)
 (begin
   (let ((sub-name "test-calc-password-list-1")
         (test-list
          (list
           (list (list (list 3 1 9) (list 6 8 0))
                 (list 3 1 9 6 8 0))
           (list (list (list 3 1 9) (list 6 8 0) (list 1 8 0))
                 (list 3 1 9 6 8 0))
           (list (list (list 3 1 9) (list 6 8 0) (list 1 8 0) (list 6 9 0))
                 (list 3 1 6 9 8 0))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((key-list (list-ref alist 0))
                  (shouldbe-list (list-ref alist 1)))
              (let ((result-list
                     (calc-password-list key-list)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : key-list=~a, "
                        sub-name test-label-index key-list))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe-list result-list)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-list result-list)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; password list is a list of digits
(define (check-password-consistency pwd-list kl-list-list)
  (begin
    (let ((bresult #t)
          (missing-error-count 0)
          (placement-error-count 0))
      (begin
        ;;; insert into pwd-list
        (for-each
         (lambda (a-list)
           (begin
             (let ((index-list
                    (make-index-list pwd-list a-list))
                   (llen (length a-list)))
               (begin
                 (if (and (list? index-list)
                          (> (length index-list) 0))
                     (begin
                       (let ((last-index (car index-list)))
                         (begin
                           (do ((jj 1 (1+ jj)))
                               ((>= jj llen))
                             (begin
                               (let ((this-index
                                      (list-ref index-list jj)))
                                 (begin
                                   (cond
                                    ((equal? this-index #f)
                                     (begin
                                       (set! bresult #f)
                                       (set!
                                        missing-error-count
                                        (1+ missing-error-count))
                                       ))
                                    ((< this-index last-index)
                                     (begin
                                       (set! bresult #f)
                                       (set!
                                        placement-error-count
                                        (1+ placement-error-count))
                                       )))
                                   (set! last-index this-index)
                                   ))
                               ))
                           ))
                       ))
                 ))
             )) kl-list-list)

        (if (or (> missing-error-count 0)
                (> placement-error-count 0))
            (begin
              (display
               (format
                #f "check-password-consistency for ~a~%"
                pwd-list))
              (display
               (format
                #f "missing error count = ~a~%"
                missing-error-count))
              (display
               (format
                #f "placement-error-count = ~a~%"
                placement-error-count))
              (force-output)
              ))
        bresult
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop filename)
  (begin
    (let ((key-list (read-in-keylogs filename)))
      (begin
        (let ((pwd-list
               (calc-password-list key-list)))
          (let ((bresult
                 (check-password-consistency pwd-list key-list)))
            (begin
              (display
               (ice-9-format:format
                #f "the hidden passcode is ~a~%"
                pwd-list))
              (force-output)
              )))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "A common security method used for "))
    (display
     (format #f "online banking is to~%"))
    (display
     (format #f "ask the user for three random "))
    (display
     (format #f "characters from a~%"))
    (display
     (format #f "passcode. For example, if the passcode "))
    (display
     (format #f "was 531278, they~%"))
    (display
     (format #f "may ask for the 2nd, 3rd, and 5th "))
    (display
     (format #f "characters; the expected~%"))
    (display
     (format #f "reply would be: 317.~%"))
    (newline)
    (display
     (format #f "The text file, keylog.txt, contains "))
    (display
     (format #f "fifty successful login~%"))
    (display
     (format #f "attempts.~%"))
    (newline)
    (display
     (format #f "Given that the three characters are "))
    (display
     (format #f "always asked for in~%"))
    (display
     (format #f "order, analyse the file so as to "))
    (display
     (format #f "determine the shortest~%"))
    (display
     (format #f "possible secret passcode of unknown "))
    (display
     (format #f "length.~%"))
    (display
     (format #f "see https://projecteuler.net/problem=79~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((filename "keylog.txt"))
      (begin
        (timer-module:time-code-macro
         (begin
           (sub-main-loop filename)
           (newline)
           ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 79 (version ~a)"
                     version-string)))
        (begin
         ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (load "digits-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 74                                     ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated July 21, 2022                                ###
;;;###                                                       ###
;;;###  updated March 7, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### digits-module for split-digits-list function
(use-modules ((digits-module)
              :renamer (symbol-prefix-proc 'digits-module:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (factorial nn)
  (begin
    (cond
     ((< nn 0)
      (begin
        #f
        ))
     ((= nn 0)
      (begin
        1
        ))
     ((= nn 1)
      (begin
        1
        ))
     ((= nn 2)
      (begin
        2
        ))
     ((= nn 3)
      (begin
        6
        ))
     ((= nn 4)
      (begin
        24
        ))
     ((= nn 5)
      (begin
        120
        ))
     ((= nn 6)
      (begin
        720
        ))
     ((= nn 7)
      (begin
        5040
        ))
     ((= nn 8)
      (begin
        40320
        ))
     ((= nn 9)
      (begin
        362880
        ))
     (else
      (begin
        (* nn (factorial (- nn 1)))
        )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-factorial-1 result-hash-table)
 (begin
   (let ((sub-name "test-factorial-1")
         (test-list
          (list
           (list 0 1) (list 1 1) (list 2 2)
           (list 3 6) (list 4 24) (list 5 120)
           (list 6 720) (list 7 5040) (list 8 40320)
           (list 9 362880)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-num (list-ref alist 0))
                  (shouldbe-num (list-ref alist 1)))
              (let ((result-num (factorial test-num)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : test-num=~a, "
                        sub-name test-label-index test-num))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe-num result-num)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-num result-num)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (digits-factorial this-num)
  (begin
    (let ((dlist
           (digits-module:split-digits-list this-num))
          (sum 0))
      (begin
        (for-each
         (lambda (this-digit)
           (begin
             (let ((this-fact
                    (factorial this-digit)))
               (begin
                 (set! sum (+ sum this-fact))
                 ))
             )) dlist)

        sum
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-digits-factorial-1 result-hash-table)
 (begin
   (let ((sub-name "test-digits-factorial-1")
         (test-list
          (list
           (list 145 145) (list 169 363601)
           (list 363601 1454) (list 1454 169)
           (list 871 45361) (list 45361 871)
           (list 872 45362) (list 45362 872)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-num (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result
                     (digits-factorial test-num)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : test-num=~a, "
                        sub-name test-label-index test-num))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (calculate-sequences
         start-num next-seq-htable acc-list)
  (begin
    (cond
     ((not (equal? (memq start-num acc-list) #f))
      (begin
        (reverse (cons start-num acc-list))
        ))
     (else
      (begin
        (let ((next-num
               (hash-ref
                next-seq-htable start-num #f)))
          (begin
            (if (equal? next-num #f)
                (begin
                  (let ((this-num
                         (digits-factorial start-num)))
                    (begin
                      (hash-set!
                       next-seq-htable start-num this-num)
                      (set! next-num this-num)
                      ))
                  ))

            (let ((next-list (cons start-num acc-list)))
              (begin
                (calculate-sequences
                 next-num next-seq-htable next-list)
                ))
            ))
        )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-calculate-sequences-1 result-hash-table)
 (begin
   (let ((sub-name "test-calculate-sequences-1")
         (test-list
          (list
           (list 145 (list 145 145))
           (list 169 (list 169 363601 1454 169))
           (list 363601 (list 363601 1454 169 363601))
           (list 1454 (list 1454 169 363601 1454))
           (list 871 (list 871 45361 871))
           (list 872 (list 872 45362 872))
           (list 69 (list 69 363600 1454 169 363601 1454))
           (list 78 (list 78 45360 871 45361 871))
           (list 540 (list 540 145 145))
           ))
         (next-seq-htable (make-hash-table 100))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-num (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result
                     (calculate-sequences
                      test-num next-seq-htable (list))))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : test-num=~a, "
                        sub-name test-label-index test-num))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop
         max-num target-length status-num debug-flag)
  (begin
    (let ((target-num-list (list))
          (target-sequence (list))
          (target-count 0)
          (next-seq-htable (make-hash-table 1000)))
      (begin
        (do ((ii 1 (1+ ii)))
            ((> ii max-num))
          (begin
            (let ((slist
                   (calculate-sequences
                    ii next-seq-htable (list))))
              (let ((slen (- (length slist) 1)))
                (begin
                  (if (equal? slen target-length)
                      (begin
                        (set! target-num-list (cons ii target-num-list))
                        (set! target-sequence slist)
                        (set! target-count (1+ target-count))

                        (if (equal? debug-flag #t)
                            (begin
                              (display
                               (format
                                #f "start = ~a, sequence = ~a~%"
                                ii target-sequence))
                              (display
                               (format
                                #f "length = ~a, so far = ~a~%"
                                slen target-num-list))
                              ))
                        ))
                  )))
            ))

        (display
         (ice-9-format:format
          #f "the number of chains that contain exactly ~:d~%"
          target-length))
        (display
         (ice-9-format:format
          #f "non-repeating terms is ~:d~%"
          target-count))
        (display
         (ice-9-format:format
          #f "(for numbers less than ~:d)~%"
          max-num))
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "The number 145 is well known for the "))
    (display
     (format #f "property that the~%"))
    (display
     (format #f "sum of the factorial of its digits is "))
    (display
     (format #f "equal to 145:~%"))
    (display
     (format #f "1! + 4! + 5! = 1 + 24 + 120 = 145~%"))
    (newline)
    (display
     (format #f "Perhaps less well known is 169, in "))
    (display
     (format #f "that it produces the~%"))
    (display
     (format #f "longest chain of numbers that link back "))
    (display
     (format #f "to 169; it turns~%"))
    (display
     (format #f "out that there are only three such "))
    (display
     (format #f "loops that exist:~%"))
    (display
     (format #f "    169 -> 363601 -> 1454 -> 169~%"))
    (display
     (format #f "    871 -> 45361 -> 871~%"))
    (display
     (format #f "    872 -> 45362 -> 872~%"))
    (newline)
    (display
     (format #f "It is not difficult to prove that EVERY "))
    (display
     (format #f "starting number will~%"))
    (display
     (format #f "eventually get stuck in a loop. For "))
    (display
     (format #f "example,~%"))
    (display
     (format #f "    69 -> 363600 -> 1454 -> 169 -> "))
    (display
     (format #f "363601 (-> 1454)~%"))
    (display
     (format #f "    78 -> 45360 -> 871 -> 45361 "))
    (display
     (format #f "(-> 871)~%"))
    (display
     (format #f "    540 -> 145 (-> 145)~%"))
    (newline)
    (display
     (format #f "Starting with 69 produces a chain of "))
    (display
     (format #f "five non-repeating terms,~%"))
    (display
     (format #f "but the longest non-repeating chain "))
    (display
     (format #f "with a starting~%"))
    (display
     (format #f "number below one million is "))
    (display
     (format #f "sixty terms.~%"))
    (newline)
    (display
     (format #f "How many chains, with a starting number "))
    (display
     (format #f "below one million,~%"))
    (display
     (format #f "contain exactly sixty non-repeating "))
    (display
     (format #f "terms?~%"))
    (newline)
    (display
     (format #f "Pre-computing the factorials from 1 "))
    (display
     (format #f "through 9, and~%"))
    (display
     (format #f "storing terms in a sequence in a hash "))
    (display
     (format #f "table, was the key~%"))
    (display
     (format #f "to making this program run fast. A more "))
    (display
     (format #f "detailed description~%"))
    (display
     (format #f "of the solution can be found at~%"))
    (display
     (format #f "https://euler.stephan-brumme.com/74/~%"))
    (display
     (format #f "see https://projecteuler.net/problem=74~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((max-num 100)
          (target-length 5)
          (status-num 1000)
          (debug-flag #t))
      (begin
        (sub-main-loop
         max-num target-length status-num debug-flag)
        ))

    (newline)
    (let ((max-num 1000000)
          (target-length 60)
          (status-num 200000)
          (debug-flag #f))
      (begin
        (sub-main-loop
         max-num target-length status-num debug-flag)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 74 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (load "digits-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 72                                     ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated July 21, 2022                                ###
;;;###                                                       ###
;;;###  updated March 7, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### srfi-1 for fold function
(use-modules ((srfi srfi-1)
              :renamer (symbol-prefix-proc 'srfi-1:)))

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
;;; sieve of eratosthenes, to calculate all prime divisors
;;; of all numbers less than max-num
;;; sprime-htable : key = num, value = (list prime factors)
(define (populate-single-prime-hash! sprime-htable max-num)
  (begin
    (let ((num-array (make-array 0 (+ max-num 1))))
      (begin
        (hash-clear! sprime-htable)

        (do ((ii 1 (1+ ii)))
            ((> ii max-num))
          (begin
            (array-set! num-array ii ii)
            ))

        (do ((ii 2 (1+ ii)))
            ((> ii max-num))
          (begin
            (let ((this-num (array-ref num-array ii)))
              (begin
                (if (= this-num ii)
                    (begin
                      (do ((jj ii (+ jj ii)))
                          ((> jj max-num))
                        (begin
                          (let ((this-list
                                 (hash-ref sprime-htable jj (list))))
                            (begin
                              (hash-set!
                               sprime-htable jj (cons ii this-list))
                              ))

                          (let ((jnum (array-ref num-array ii)))
                            (begin
                              (array-set!
                               num-array (euclidean/ jnum ii) jj)
                              ))
                          ))
                      ))
                ))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-populate-single-prime-hash-1 result-hash-table)
 (begin
   (let ((sub-name "test-populate-single-prime-hash-1")
         (max-num 30)
         (prime-htable (make-hash-table 30))
         (shouldbe-list-list
          (list
           (list 2 (list 2)) (list 3 (list 3)) (list 4 (list 2))
           (list 5 (list 5)) (list 6 (list 2 3))
           (list 7 (list 7)) (list 8 (list 2))
           (list 9 (list 3)) (list 10 (list 2 5))
           (list 11 (list 11)) (list 12 (list 2 3))
           (list 13 (list 13)) (list 14 (list 2 7))
           (list 15 (list 3 5)) (list 16 (list 2))
           (list 17 (list 17)) (list 18 (list 2 3))
           (list 19 (list 19)) (list 20 (list 2 5))
           (list 21 (list 3 7)) (list 22 (list 2 11))
           (list 23 (list 23)) (list 24 (list 2 3))
           (list 25 (list 5)) (list 26 (list 2 13))
           (list 27 (list 3)) (list 28 (list 2 7))
           (list 29 (list 29)) (list 30 (list 2 3 5))
           ))
         (test-label-index 0))
     (begin
       (populate-single-prime-hash! prime-htable max-num)

       (for-each
        (lambda (s-list)
          (begin
            (let ((num (list-ref s-list 0))
                  (shouldbe-plist (list-ref s-list 1)))
              (let ((result-plist
                     (sort
                      (hash-ref prime-htable num (list))
                      <)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : num=~a, "
                        sub-name test-label-index num))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe-plist result-plist)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-plist result-plist)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) shouldbe-list-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; count the number of relatively prime numbers to n
;;; if n even, all even numbers are not relatively prime
(define (totient-function nn sprime-htable)
  (begin
    (let ((tproduct nn)
          (ll-max (1+ (exact-integer-sqrt nn)))
          (pmax nn)
          (sprime-list (hash-ref sprime-htable nn #f)))
      (begin
        (if (and (equal? sprime-list #f)
                 (> nn 1))
            (begin
              (display
               (ice-9-format:format
                #f "totient-function error : for number ~:d~%" nn))
              (display
               (ice-9-format:format
                #f "no single prime list in sprime-htable.~%"))
              (display
               (format #f "quitting...~%"))
              (force-output)
              (quit)
              ))

        (cond
         ((or (<= nn 1)
              (> ll-max pmax))
          (begin
            (set! tproduct 1)
            ))
         (else
          (begin
            (set!
             pmax
             (srfi-1:fold
              (lambda (this-num prev)
                (begin
                  (if (> this-num prev)
                      (begin
                        this-num)
                      (begin
                        prev
                        ))
                  ))
              0 sprime-list))

            (let ((splen (length sprime-list)))
              (begin
                (do ((ii 0 (1+ ii)))
                    ((>= ii splen))
                  (begin
                    (set!
                     tproduct
                     (* tproduct
                        (- 1 (/ 1 (list-ref sprime-list ii)))
                        ))
                    ))
                ))
            )))

        tproduct
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-totient-function-1 result-hash-table)
 (begin
   (let ((sub-name "test-totient-function-1")
         (test-list
          (list
           (list 2 1) (list 3 2) (list 4 2)
           (list 5 4) (list 6 2) (list 7 6)
           (list 8 4) (list 9 6) (list 10 4)
           (list 11 10) (list 12 4) (list 13 12)
           (list 14 6) (list 15 8) (list 16 8)
           (list 17 16) (list 18 6) (list 19 18)
           (list 20 8) (list 21 12) (list 22 10)
           (list 23 22) (list 24 8) (list 25 20)
           (list 90 24) (list 91 72) (list 92 44)
           (list 93 60) (list 94 46) (list 95 72)
           ))
         (max-num 100)
         (sprime-htable (make-hash-table 100))
         (test-label-index 0))
     (begin
       (populate-single-prime-hash! sprime-htable max-num)

       (for-each
        (lambda (alist)
          (begin
            (let ((test-num (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result
                     (totient-function test-num sprime-htable)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : test-num=~a, "
                        sub-name test-label-index test-num))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; note: method doesn't count 0 or 1
(define (sieve-of-eratosthenes-method max-num)
  (begin
    (let ((num-array (make-array 0 (+ max-num 1)))
          (result-sum 0))
      (begin
        (do ((ii 0 (1+ ii)))
            ((> ii max-num))
          (begin
            (array-set! num-array ii ii)
            ))

        (do ((ii 2 (1+ ii)))
            ((> ii max-num))
          (begin
            (let ((num (array-ref num-array ii)))
              (begin
                (if (= num ii)
                    (begin
                      (let ((factor (/ (- ii 1) ii)))
                        (begin
                          (do ((jj ii (+ jj ii)))
                              ((> jj max-num))
                            (begin
                              (let ((this-num
                                     (array-ref num-array jj)))
                                (let ((next-num
                                       (* this-num factor)))
                                  (begin
                                    (array-set!
                                     num-array next-num jj)
                                    )))
                              ))
                          ))
                      ))
                ))

            (set!
             result-sum
             (+ result-sum
                (array-ref num-array ii)))
            ))
        result-sum
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-sieve-of-eratosthenes-method-1 result-hash-table)
 (begin
   (let ((sub-name "test-sieve-of-eratosthenes-method-1")
         (test-list
          (list
           (list 2 1) (list 3 3)
           (list 4 5) (list 5 9)
           (list 6 11) (list 7 17)
           (list 8 21)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((max-num (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result
                     (sieve-of-eratosthenes-method max-num)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : max-num=~a, "
                        sub-name test-label-index max-num))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; note: method does count 0 and 1
(define (calculate-number-of-elements max-num sprime-htable)
  (begin
    (let ((elements-count 1))
      (begin
        (do ((mm 1 (1+ mm)))
            ((> mm max-num))
          (begin
            (let ((totient
                   (totient-function mm sprime-htable)))
              (begin
                (set!
                 elements-count
                 (+ elements-count totient))
                ))
            ))
        elements-count
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-calculate-number-of-elements-1 result-hash-table)
 (begin
   (let ((sub-name "test-calculate-number-of-elements-1")
         (test-list
          (list
           (list 2 10 3) (list 3 10 5)
           (list 4 10 7) (list 5 10 11)
           (list 6 10 13) (list 7 10 19)
           (list 8 10 23)
           ))
         (max-num 20)
         (sprime-htable (make-hash-table 20))
         (test-label-index 0))
     (begin
       (populate-single-prime-hash! sprime-htable max-num)

       (for-each
        (lambda (alist)
          (begin
            (let ((this-num (list-ref alist 0))
                  (this-prime (list-ref alist 1))
                  (shouldbe (list-ref alist 2)))
              (let ((result
                     (calculate-number-of-elements
                      this-num sprime-htable)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "this-num=~a, this-prime=~a, "
                        this-num this-prime))
                      (err-3
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append
                      err-1 err-2 err-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop max-num max-prime)
  (begin
    (let ((count
           (sieve-of-eratosthenes-method max-num)))
      (begin
        (display
         (ice-9-format:format
          #f "there are ~:d elements in the set of reduced~%"
          count))
        (display
         (ice-9-format:format
          #f "proper fractions for d <= ~:d~%"
          max-num))
        (display
         (format #f "(sieve of eratosthenes method)~%"))
        (force-output)
        ))

    (newline)
    (let ((sprime-htable (make-hash-table max-num)))
      (begin
        (populate-single-prime-hash! sprime-htable max-num)

        (let ((count (calculate-number-of-elements max-num sprime-htable)))
          (let ((without-0-1-count (- count 2)))
            (begin
              (display
               (ice-9-format:format
                #f "there are ~:d elements in the set of reduced~%"
                without-0-1-count))
              (display
               (ice-9-format:format
                #f "proper fractions for d <= ~:d~%"
                max-num))
              (display
               (ice-9-format:format
                #f "(eulers totient method)~%"))
              (force-output)
              ))
          )))

    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Consider the fraction, n/d, where n "))
    (display
     (format #f "and d are positive~%"))
    (display
     (format #f "integers. If n<d and HCF(n,d)=1, it "))
    (display
     (format #f "is called a reduced~%"))
    (display
     (format #f "proper fraction.~%"))
    (newline)
    (display
     (format #f "If we list the set of reduced proper "))
    (display
     (format #f "fractions for d <= 8~%"))
    (display
     (format #f "in ascending order of size, we get:~%"))
    (newline)
    (display
     (format #f "  1/8, 1/7, 1/6, 1/5, 1/4, 2/7, 1/3, "))
    (display
     (format #f "3/8, 2/5, 3/7,~%"))
    (display
     (format #f "1/2, 4/7, 3/5, 5/8, 2/3, 5/7, 3/4, "))
    (display
     (format #f "4/5, 5/6, 6/7, 7/8~%"))
    (newline)
    (display
     (format #f "It can be seen that there are 21 "))
    (display
     (format #f "elements in this set.~%"))
    (newline)
    (display
     (format #f "How many elements would be contained "))
    (display
     (format #f "in the set of~%"))
    (display
     (format #f "reduced proper fractions for d <= "))
    (display
     (format #f "1,000,000?~%"))
    (newline)
    (force-output)
    (newline)
    (display
     (format #f "Several solutions can be found at~%"))
    (display
     (format #f "https://euler.stephan-brumme.com/72/~%"))
    (newline)
    (display
     (format #f "This program uses the sieve of "))
    (display
     (format #f "Eratosthenes method and~%"))
    (display
     (format #f "a straight-forward totient method for "))
    (display
     (format #f "counting the number~%"))
    (display
     (format #f "of elements in a Farey sequence. See the "))
    (display
     (format #f "wikipedia for a~%"))
    (display
     (format #f "description of the Farey sequence and it's "))
    (display
     (format #f "generation~%"))
    (display
     (format #f "https://en.wikipedia.org/wiki/Farey_sequence~%"))
    (display
     (format #f "see https://projecteuler.net/problem=72~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((max-num 8)
          (max-prime 100))
      (begin
        (sub-main-loop max-num max-prime)
        ))

    (newline)
    (let ((max-num 1000000)
          (max-prime 1000000))
      (begin
        (sub-main-loop max-num max-prime)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 72 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 78                                     ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated July 21, 2022                                ###
;;;###                                                       ###
;;;###  updated March 7, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (populate-partition-function-hash! results-htable max-num)
  (begin
    (let ((p-array (make-array 0 (+ max-num 1))))
      (begin
        (hash-clear! results-htable)
        (array-set! p-array 1 0)

        (do ((ii 1 (1+ ii)))
            ((> ii max-num))
          (begin
            (let ((jj 1)
                  (kk 1)
                  (ss 0))
              (begin
                (while
                 (> jj 0)
                 (begin
                   (let ((kk-tmp (* 3 kk kk)))
                     (let ((next-jj
                            (- ii
                               (euclidean/ (+ kk-tmp kk) 2)))
                           (sign-factor -1))
                       (begin
                         (set! jj next-jj)
                         (if (>= jj 0)
                             (begin
                               (if (even? kk)
                                   (begin
                                     (set! sign-factor 1)
                                     ))
                               (set!
                                ss
                                (- ss
                                   (*
                                    sign-factor
                                    (array-ref p-array jj))))
                               ))
                         (set!
                          jj
                          (- ii (euclidean/
                                 (- kk-tmp kk) 2)))
                         (if (>= jj 0)
                             (begin
                               (if (even? kk)
                                   (begin
                                     (set! sign-factor 1))
                                   (begin
                                     (set! sign-factor -1)
                                     ))
                               (set!
                                ss
                                (- ss
                                   (* sign-factor
                                      (array-ref p-array jj))))
                               ))

                         (set! kk (1+ kk))
                         )))
                   ))
                (array-set! p-array ss ii)
                (hash-set! results-htable ii ss)
                ))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-populate-partition-function-hash-1 result-hash-table)
 (begin
   (let ((sub-name "test-populate-partition-function-hash-1")
         (test-list
          (list
           (list 2 2) (list 3 3)
           (list 4 5) (list 5 7)
           (list 6 11) (list 7 15)
           (list 8 22) (list 9 30)
           (list 10 42) (list 11 56)
           (list 12 77) (list 13 101)
           (list 14 135) (list 15 176)
           (list 16 231) (list 17 297)
           (list 18 385) (list 19 490)
           ))
         (max-num 20)
         (results-htable (make-hash-table 20))
         (mod-num 10000)
         (debug-flag #f)
         (test-label-index 0))
     (begin
       (populate-partition-function-hash! results-htable max-num)

       (for-each
        (lambda (alist)
          (begin
            (let ((test-num (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result
                     (hash-ref results-htable test-num -1)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : test-num=~a, "
                        sub-name test-label-index test-num))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop max-num min-ways)
  (begin
    (let ((loop-continue-flag #t)
          (results-htable (make-hash-table max-num))
          (start-num 1)
          (max-iterations max-num)
          (ii-result 0)
          (last-num-ways 0))
      (begin
        (while
         (equal? loop-continue-flag #t)
         (begin
           (populate-partition-function-hash! results-htable max-iterations)

           (do ((ii start-num (1+ ii)))
               ((or (> ii max-iterations)
                    (equal? loop-continue-flag #f)))
             (begin
               (let ((rnum (hash-ref results-htable ii -1)))
                 (begin
                   (if (zero? (modulo rnum min-ways))
                       (begin
                         (set! loop-continue-flag #f)
                         (set! ii-result ii)
                         (display
                          (ice-9-format:format
                           #f "~:d is the least value for which p(n)~%"
                           ii-result))
                         (display
                          (ice-9-format:format
                           #f "= ~:d (~1,5e) is divisible by ~:d~%"
                           rnum (exact->inexact rnum) min-ways))
                         (force-output)
                         ))
                   (set! last-num-ways rnum)
                   ))
               ))

           (set! start-num max-iterations)
           (set! max-iterations (+ max-iterations max-num))
           ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Let p(n) represent the number of "))
    (display
     (format #f "different ways in which~%"))
    (display
     (format #f "n coins can be separated into piles. "))
    (display
     (format #f "For example, five~%"))
    (display
     (format #f "coins can separated into piles in "))
    (display
     (format #f "exactly seven different~%"))
    (display
     (format #f "ways, so p(5)=7.~%"))
    (newline)
    (display
     (format #f "  5~%"))
    (display
     (format #f "  4 & 1~%"))
    (display
     (format #f "  3 & 2~%"))
    (display
     (format #f "  3 & 1 & 1~%"))
    (display
     (format #f "  2 & 2 & 1~%"))
    (display
     (format #f "  2 & 1 & 1 & 1~%"))
    (display
     (format #f "  1 & 1 & 1 & 1 & 1~%"))
    (newline)
    (display
     (format #f "Find the least value of n for which "))
    (display
     (format #f "p(n) is divisible~%"))
    (display
     (format #f "by one million.~%"))
    (newline)
    (display
     (format #f "A fast algorithm was found at~%"))
    (display
     (format #f "https://www.numericana.com/answer/numbers.htm#partitions~%"))
    (display
     (format #f "see https://projecteuler.net/problem=78~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((max-num 10)
          (min-ways 5)
          (debug-flag #t))
      (begin
        (sub-main-loop max-num min-ways)
        ))

    (newline)
    (let ((max-num 10000)
          (min-ways 1000000))
      (begin
        (sub-main-loop max-num min-ways)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 78 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 80                                     ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated July 21, 2022                                ###
;;;###                                                       ###
;;;###  updated March 7, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### srfi-1 for fold function
(use-modules ((srfi srfi-1)
              :renamer (symbol-prefix-proc 'srfi-1:)))

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
;;; returns a rational number
(define (sqrt-herons-method input-number epsilon)
  (begin
    (let ((closest-int-sqrt (exact-integer-sqrt input-number))
          (delta 1.0))
      (let ((xn closest-int-sqrt))
        (begin
          (while
           (> delta epsilon)
           (begin
             (let ((xnp1
                    (/ (+ xn (/ input-number xn)) 2)))
               (let ((next-delta
                      (abs (exact->inexact (- xnp1 xn)))))
                 (begin
                   (set! xn xnp1)
                   (set! delta next-delta)
                   )))
             ))
          xn
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-sqrt-herons-method-1 result-hash-table)
 (begin
   (let ((sub-name "test-sqrt-herons-method-1")
         (test-list
          (list
           (list 2 1.0e-4 1.4142)
           (list 2 1.0e-10 1.4142135624)
           (list 3 1.0e-4 1.7321)
           (list 4 1.0e-4 2.0000)
           (list 5 1.0e-4 2.2361)
           (list 6 1.0e-4 2.4495)
           (list 6 1.0e-5 2.44949)
           (list 6 1.0e-6 2.449490)
           (list 7 1.0e-4 2.6458)
           (list 8 1.0e-4 2.8284)
           (list 9 1.0e-4 3.0000)
           (list 10 1.0e-4 3.1622)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-num (list-ref alist 0))
                  (delta (list-ref alist 1))
                  (shouldbe-decimal (list-ref alist 2)))
              (let ((result-rational
                     (sqrt-herons-method test-num delta)))
                (let ((result-decimal
                       (exact->inexact result-rational)))
                  (let ((err-1
                         (format
                          #f "~a : error (~a) : "
                          sub-name test-label-index))
                        (err-2
                         (format
                          #f "test-num=~a, delta=~a, "
                          test-num delta))
                        (err-3
                         (format
                          #f "shouldbe=~a, result=~a"
                          shouldbe-decimal result-decimal)))
                    (begin
                      (unittest2:assert?
                       (<= (abs
                            (- shouldbe-decimal result-decimal))
                           delta)
                       sub-name
                       (string-append
                        err-1 err-2 err-3)
                       result-hash-table)
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (calc-decimal-digits-list this-rational num-digits)
  (begin
    (cond
     ((integer? this-rational)
      (begin
        (list)
        ))
     (else
      (begin
      ;;; first take care of the integer part
        (let ((integral-part (truncate this-rational))
              (num-decimals num-digits)
              (results-list (list)))
          (begin
            (while
             (>= integral-part 1.0)
             (begin
               (let ((num
                      (inexact->exact
                       (* 10.0
                          (truncate
                           (* 0.10 integral-part))))))
                 (let ((first-digit
                        (- integral-part num)))
                   (let ((next-num
                          (truncate (* 0.10 num))))
                     (begin
                       (set!
                        results-list
                        (cons first-digit results-list))
                       (set!
                        integral-part next-num)
                       (set!
                        num-decimals
                        (1- num-decimals))
                       ))
                   ))
               ))

            ;;; next, take care of the decimal part
            (let ((integral-part
                   (truncate this-rational)))
              (let ((fractional-part
                     (- this-rational integral-part))
                    (dresults-list (list)))
                (begin
                  (do ((ii 0 (1+ ii)))
                      ((>= ii num-decimals))
                    (begin
                      (let ((next-int
                             (inexact->exact
                              (truncate
                               (* 10.0 fractional-part)))))
                        (let ((ten-decimals
                               (* 10 fractional-part)))
                          (begin
                            (set!
                             dresults-list
                             (cons next-int dresults-list))
                            (set!
                             fractional-part
                             (- ten-decimals next-int))
                            )))
                      ))
                  (set!
                   results-list
                   (append
                    results-list
                    (reverse dresults-list)))
                  )))

            results-list
            ))
        )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-calc-decimal-digits-list-1 result-hash-table)
 (begin
   (let ((sub-name "test-calc-decimal-digits-list-1")
         (test-list
          (list
           (list 2 1.0e-4 1.4142 4 (list 1 4 1 4))
           (list 3 1.0e-4 1.7320 4 (list 1 7 3 2))
           (list 4 1.0e-4 2.0000 4 (list))
           (list 5 1.0e-4 2.2360 4 (list 2 2 3 6))
           (list 6 1.0e-4 2.4494 4 (list 2 4 4 9))
           (list 6 1.0e-5 2.44948 5 (list 2 4 4 9 4))
           (list 6 1.0e-6 2.449489 6 (list 2 4 4 9 4 8))
           (list 7 1.0e-4 2.6457 4 (list 2 6 4 5))
           (list 8 1.0e-4 2.8284 4 (list 2 8 2 8))
           (list 9 1.0e-4 3.0000 4 (list) )
           (list 10 1.0e-4 3.1622 4 (list 3 1 6 2))
           (list 100 1.0e-4 10.0 4 (list))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-num (list-ref alist 0))
                  (delta (list-ref alist 1))
                  (sqrt-decimal (list-ref alist 2))
                  (num-digits (list-ref alist 3))
                  (shouldbe-list (list-ref alist 4)))
              (let ((result-rational
                     (sqrt-herons-method test-num delta)))
                (let ((result-list
                       (calc-decimal-digits-list
                        result-rational num-digits)))
                  (let ((err-1
                         (format
                          #f "~a : error (~a) : "
                          sub-name test-label-index))
                        (err-2
                         (format
                          #f "test-num=~a, delta=~a, "
                          test-num delta))
                        (err-3
                         (format
                          #f "sqrt-decimal=~a, num-digits=~a, "
                          sqrt-decimal num-digits))
                        (err-4
                         (format
                          #f "shouldbe=~a, result=~a"
                          shouldbe-list result-list)))
                    (begin
                      (unittest2:assert?
                       (equal? shouldbe-list result-list)
                       sub-name
                       (string-append
                        err-1 err-2 err-3 err-4)
                       result-hash-table)
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop
         start-num end-num delta num-digits debug-flag)
  (begin
    (let ((result-sum 0))
      (begin
        (do ((ii start-num (1+ ii)))
            ((> ii end-num))
          (begin
            (let ((sqrt-rational
                   (sqrt-herons-method ii delta)))
              (let ((decimals-list
                     (calc-decimal-digits-list sqrt-rational num-digits)))
                (let ((this-sum
                       (srfi-1:fold + 0 decimals-list)))
                  (begin
                    (set! result-sum (+ result-sum this-sum))

                    (if (equal? debug-flag #t)
                        (begin
                          (display
                           (ice-9-format:format
                            #f "  sqrt(~:d) = ~a~%"
                            ii (exact->inexact sqrt-rational)))
                          (display
                           (ice-9-format:format
                            #f "  decimals-list = ~a, "
                            decimals-list))
                          (display
                           (ice-9-format:format
                            #f "decimals sum = ~a~%" this-sum))
                          (force-output)
                          ))
                    ))
                ))
            ))

        (display
         (ice-9-format:format
          #f "the sum of the first ~:d decimals "
          num-digits))
        (display
         (ice-9-format:format
          #f "of the square root is ~:d~%"
          result-sum))
        (display
         (ice-9-format:format
          #f "(between ~:d and ~:d)~%"
          start-num end-num))
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "It is well known that if the "))
    (display
     (format #f "square root of a~%"))
    (display
     (format #f "natural number is not an integer, "))
    (display
     (format #f "then it is irrational.~%"))
    (display
     (format #f "The decimal expansion of such square "))
    (display
     (format #f "roots is infinite without~%"))
    (display
     (format #f "any repeating pattern at all.~%"))
    (newline)
    (display
     (format #f "The square root of two is~%"))
    (display
     (format #f "1.41421356237309504880...~%"))
    (display
     (format #f "and the digital sum of the first one "))
    (display
     (format #f "hundred decimal digits~%"))
    (display
     (format #f "is 475.~%"))
    (newline)
    (display
     (format #f "For the first one hundred natural "))
    (display
     (format #f "numbers, find the~%"))
    (display
     (format #f "total of the digital sums of the "))
    (display
     (format #f "first one hundred~%"))
    (display
     (format #f "decimal digits for all the "))
    (display
     (format #f "irrational square roots.~%"))
    (newline)
    (display
     (format #f "Used Heron's method, see~%"))
    (display
     (format #f "https://en.wikipedia.org/wiki/Methods_of_computing_square_roots~%"))
    (display
     (format #f "for more details~%"))
    (display
     (format #f "see https://projecteuler.net/problem=80~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((start-num 2)
          (end-num 2)
          (delta 1.0e-120)
          (num-digits 100)
          (debug-flag #t))
      (begin
        (sub-main-loop
         start-num end-num delta num-digits debug-flag)
        ))

    (newline)
    (let ((start-num 1)
          (end-num 100)
          (delta 1.0e-120)
          (num-digits 100)
          (debug-flag #f))
      (begin
        (sub-main-loop
         start-num end-num delta num-digits debug-flag)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 80 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

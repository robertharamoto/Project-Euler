#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 73                                     ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated July 21, 2022                                ###
;;;###                                                       ###
;;;###  updated March 7, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (brute-force-counting
         max-num start-numer start-denom end-numer end-denom)
  (begin
    (let ((count 0))
      (begin
        (do ((dd 4 (1+ dd)))
            ((> dd max-num))
          (begin
            (let ((start-num (1+ (euclidean/ dd start-denom)))
                  (end-num (euclidean/ dd end-denom)))
              (begin
                (do ((nn start-num (1+ nn)))
                    ((> nn end-num))
                  (begin
                    (let ((div (gcd nn dd)))
                      (begin
                        (if (= div 1)
                            (begin
                              (set! count (1+ count))
                              ))
                        ))
                    ))
                ))
            ))
        count
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-brute-force-counting-1 result-hash-table)
 (begin
   (let ((sub-name "test-brute-force-counting-1"))
     (let ((test-list
            (list
             (list 5 1 3 1 2 1)
             (list 6 1 3 1 2 1)
             (list 7 1 3 1 2 2)
             (list 8 1 3 1 2 3)
             ))
           (test-label-index 0))
       (for-each
        (lambda (alist)
          (begin
            (let ((max-num (list-ref alist 0))
                  (start-numer (list-ref alist 1))
                  (start-denom (list-ref alist 2))
                  (end-numer (list-ref alist 3))
                  (end-denom (list-ref alist 4))
                  (shouldbe (list-ref alist 5)))
              (let ((result
                     (brute-force-counting
                      max-num start-numer start-denom
                      end-numer end-denom)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : max-num=~a, "
                        sub-name test-label-index max-num))
                      (err-2
                       (format
                        #f "start-numer=~a, start-denom=~a, "
                        start-numer start-denom))
                      (err-3
                       (format
                        #f "end-numer=~a, end-denom=~a, "
                        end-numer end-denom))
                      (err-4
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append
                      err-1 err-2 err-3 err-4)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (find-ratio-immediate-right
         max-num start-numer start-denom end-numer end-denom)
  (begin
    (let ((left-numer start-numer)
          (left-denom start-denom)
          (right-numer end-numer)
          (right-denom end-denom)
          (mid-numer 0)
          (mid-denom 0)
          (target-numer start-numer)
          (target-denom start-denom))
      (begin
        (while
         (<= (+ left-denom right-denom) max-num)
         (begin
            ;;; compute the mediant
           (set! mid-numer (+ left-numer right-numer))
           (set! mid-denom (+ left-denom right-denom))

            ;;; if mid ratio is greater than 3/7, then mid-numer*7 > mid-denom*3
           (if (or
                (and (= mid-numer target-numer)
                     (= mid-denom target-denom))
                (< (* mid-numer target-denom)
                   (* mid-denom target-numer)))
               (begin
                 (set! left-numer mid-numer)
                 (set! left-denom mid-denom))
               (begin
                 (set! right-numer mid-numer)
                 (set! right-denom mid-denom)
                 ))
           ))

        (list right-numer right-denom)
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-find-ratio-immediate-right-1 result-hash-table)
 (begin
   (let ((sub-name "test-calculate-number-in-range-1")
         (test-list
          (list
           (list 5 1 3 1 2 (list 2 5))
           (list 6 1 3 1 2 (list 2 5))
           (list 7 1 3 1 2 (list 2 5))
           (list 8 1 3 1 2 (list 3 8))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((max-num (list-ref alist 0))
                  (start-numer (list-ref alist 1))
                  (start-denom (list-ref alist 2))
                  (end-numer (list-ref alist 3))
                  (end-denom (list-ref alist 4))
                  (shouldbe (list-ref alist 5)))
              (let ((result
                     (find-ratio-immediate-right
                      max-num start-numer start-denom
                      end-numer end-denom)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : max-num=~a, "
                        sub-name test-label-index max-num))
                      (err-2
                       (format
                        #f "start-numer=~a, start-denom=~a, "
                        start-numer start-denom))
                      (err-3
                       (format
                        #f "end-numer=~a, end-denom=~a, "
                        end-numer end-denom))
                      (err-4
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append
                      err-1 err-2 err-3 err-4)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (calculate-number-in-range
         max-num start-numer start-denom end-numer end-denom)
  (begin
    (let ((count 0)
          (next-list
           (find-ratio-immediate-right
            max-num start-numer start-denom
            end-numer end-denom)))
      (let ((next-numer (list-ref next-list 0))
            (next-denom (list-ref next-list 1)))
        (let ((aa start-numer)
              (bb start-denom)
              (cc next-numer)
              (dd next-denom))
          (begin
            (while
             (not (and (= cc end-numer)
                       (= dd end-denom)))
             (begin
               (let ((kk (euclidean/ (+ max-num bb) dd)))
                 (let ((next-cc (- (* kk cc) aa))
                       (next-dd (- (* kk dd) bb)))
                   (begin
                     (set! count (1+ count))
                     (set! aa cc)
                     (set! bb dd)
                     (set! cc next-cc)
                     (set! dd next-dd)
                     )))
               ))

            count
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-calculate-number-in-range-1 result-hash-table)
 (begin
   (let ((sub-name "test-calculate-number-in-range-1")
         (test-list
          (list
           (list 5 1 3 1 2 1)
           (list 6 1 3 1 2 1)
           (list 7 1 3 1 2 2)
           (list 8 1 3 1 2 3)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((max-num (list-ref alist 0))
                  (start-numer (list-ref alist 1))
                  (start-denom (list-ref alist 2))
                  (end-numer (list-ref alist 3))
                  (end-denom (list-ref alist 4))
                  (shouldbe (list-ref alist 5)))
              (let ((result
                     (calculate-number-in-range
                      max-num start-numer start-denom
                      end-numer end-denom)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : max-num=~a, "
                        sub-name test-label-index max-num))
                      (err-2
                       (format
                        #f "start-numer=~a, start-denom=~a, "
                        start-numer start-denom))
                      (err-3
                       (format
                        #f "end-numer=~a, end-denom=~a, "
                        end-numer end-denom))
                      (err-4
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append
                      err-1 err-2 err-3 err-4)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop
         max-num start-numer start-denom end-numer end-denom)
  (begin
    (let ((result-num
           (calculate-number-in-range
            max-num start-numer start-denom
            end-numer end-denom)))
      (begin
        (display
         (ice-9-format:format
          #f "there are ~:d elements the set of reduced proper~%"
          result-num))
        (display
         (ice-9-format:format
          #f "fractions that are strictly between ~:d/~:d "
          start-numer start-denom))
        (display
         (ice-9-format:format
          #f "and ~:d/~:d,~%"
          end-numer end-denom))
        (display
         (ice-9-format:format
          #f "with denominators less than ~:d~%"
          max-num))
        (display
         (format #f "(farey next-neighbors method)~%"))
        (newline)
        (force-output)
        ))


    (newline)
    (let ((result-num
           (brute-force-counting
            max-num start-numer start-denom
            end-numer end-denom)))
      (begin
        (display
         (ice-9-format:format
          #f "there are ~:d elements the set of reduced proper~%"
          result-num))
        (display
         (ice-9-format:format
          #f "fractions that are strictly between ~:d/~:d "
          start-numer start-denom))
        (display
         (ice-9-format:format
          #f "and ~:d/~:d,~%" end-numer end-denom))
        (display
         (ice-9-format:format
          #f "with denominators less than ~:d~%"
          max-num))
        (display
         (format #f "(brute-force method)~%"))
        ))

    (force-output)
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Consider the fraction, n/d, where n "))
    (display
     (format #f "and d are positive~%"))
    (display
     (format #f "integers. If n<d and HCF(n,d)=1, it "))
    (display
     (format #f "is called a reduced~%"))
    (display
     (format #f "proper fraction.~%"))
    (newline)
    (display
     (format #f "If we list the set of reduced proper "))
    (display
     (format #f "fractions for d <= 8~%"))
    (display
     (format #f "in ascending order of size, we get:~%"))
    (newline)
    (display
     (format #f "  1/8, 1/7, 1/6, 1/5, 1/4, 2/7, 1/3, 3/8, "))
    (display
     (format #f "2/5, 3/7,~%"))
    (display
     (format #f "1/2, 4/7, 3/5, 5/8, 2/3, 5/7, 3/4, 4/5, "))
    (display
     (format #f "5/6, 6/7, 7/8~%"))
    (newline)
    (display
     (format #f "It can be seen that there are 3 fractions "))
    (display
     (format #f "between 1/3 and 1/2.~%"))
    (newline)
    (display
     (format #f "How many fractions lie between 1/3 and 1/2 "))
    (display
     (format #f "in the sorted set~%"))
    (display
     (format #f "of reduced proper fractions for "))
    (display
     (format #f "d <= 12,000?~%"))
    (display
     (format #f "Note: The upper limit has been changed "))
    (display
     (format #f "recently.~%"))
    (newline)
    (display
     (format #f "This program uses the nearest neighbor "))
    (display
     (format #f "method for counting~%"))
    (display
     (format #f "the number of elements in a Farey sequence. "))
    (display
     (format #f "See the wikipedia~%"))
    (display
     (format #f "for a description of the Farey sequence "))
    (display
     (format #f "and it's generation.~%"))
    (display
     (format #f "https://en.wikipedia.org/wiki/Farey_sequence~%"))
    (newline)
    (display
     (format #f "For a description of the solution see:~%"))
    (display
     (format #f "https://euler.stephan-brumme.com/73/~%"))
    (display
     (format #f "see https://projecteuler.net/problem=73~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((max-num 8)
          (start-numer 1)
          (start-denom 3)
          (end-numer 1)
          (end-denom 2))
      (begin
        (sub-main-loop
         max-num start-numer start-denom
         end-numer end-denom)
        ))

    (newline)
    (let ((max-num 12000)
          (start-numer 1)
          (start-denom 3)
          (end-numer 1)
          (end-denom 2))
      (begin
        (sub-main-loop
         max-num start-numer start-denom
         end-numer end-denom)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 73 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 37                                     ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated June 12, 2022                                ###
;;;###                                                       ###
;;;###  updated March 4, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### srfi-1 for fold functions
(use-modules ((srfi srfi-1)
              :renamer (symbol-prefix-proc 'srfi-1:)))

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### prime-module for make-prime-array and is-prime-array? functions
(use-modules ((prime-module)
              :renamer (symbol-prefix-proc 'prime-module:)))

;;;### digits-module for split-digits-list function
(use-modules ((digits-module)
              :renamer (symbol-prefix-proc 'digits-module:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
;;; returns an empty list if one of the numbers are not prime
(define (construct-primes-list this-num dlist prime-array)
  (begin
    (if (< this-num 10)
        (begin
          #f)
        (begin
          (let ((dlen (length dlist))
                (result-list (list))
                (ok-flag #t))
            (begin
              (do ((ii 0 (1+ ii)))
                  ((or (>= ii dlen)
                       (equal? ok-flag #f)))
                (begin
                  (let ((this-list (list-tail dlist ii)))
                    (let ((this-num
                           (srfi-1:fold
                            (lambda (this-elem prev-elem)
                              (+ this-elem (* 10 prev-elem)))
                            0 this-list)))
                      (begin
                        (if (prime-module:is-array-prime?
                             this-num prime-array)
                            (begin
                              (set! result-list
                                    (cons this-num result-list)))
                            (begin
                              (set! result-list (list))
                              (set! ok-flag #f)
                              ))
                        )))
                  ))

              (do ((ii (- dlen 1) (1- ii)))
                  ((or (<= ii 0)
                       (equal? ok-flag #f)))
                (begin
                  (let ((this-list (list-head dlist ii)))
                    (let ((this-num
                           (srfi-1:fold
                            (lambda (this-elem prev-elem)
                              (begin
                                (+ this-elem (* 10 prev-elem))
                                ))
                            0 this-list)))
                      (begin
                        (if (prime-module:is-array-prime?
                             this-num prime-array)
                            (begin
                              (set! result-list
                                    (cons this-num result-list)))
                            (begin
                              (set! result-list (list))
                              (set! ok-flag #f)
                              ))
                        )))
                  ))

              (reverse result-list)
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-construct-primes-list-1 result-hash-table)
 (begin
   (let ((sub-name "test-construct-primes-list-1")
         (test-list
          (list
           (list 23 (list 23 3 2))
           (list 237 (list))
           (list 3797 (list 3797 797 97 7 379 37 3))
           ))
         (prime-array (prime-module:make-prime-array 100))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-num (list-ref alist 0))
                  (shouldbe-num (list-ref alist 1)))
              (let ((dlist
                     (digits-module:split-digits-list
                      test-num)))
                (let ((result-num
                       (construct-primes-list
                        test-num dlist prime-array)))
                  (let ((err-1
                         (format
                          #f "~a :: (~a) error : test-num=~a, "
                          sub-name test-label-index test-num))
                        (err-2
                         (format
                          #f "shouldbe=~a, result=~a"
                          shouldbe-num result-num)))
                    (begin
                      (unittest2:assert?
                       (equal? shouldbe-num result-num)
                       sub-name
                       (string-append err-1 err-2)
                       result-hash-table)
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop max-num debug-flag)
  (begin
    (let ((counter 0)
          (sum 0)
          (prime-array
           (prime-module:make-prime-array
            (* 2 (exact-integer-sqrt max-num))))
          (null-list (list)))
      (begin
        (do ((ii 11 (+ ii 2)))
            ((or (> ii max-num)
                 (>= counter 11)))
          (begin
            (if (equal?
                 (prime-module:is-array-prime?
                  ii prime-array)
                 #t)
                (begin
                  (let ((dlist
                         (digits-module:split-digits-list ii))
                        (ok-flag #t))
                    (begin
                      (if (or
                           (not (equal? (member 0 dlist) #f))
                           (not (equal? (member 4 dlist) #f))
                           (not (equal? (member 6 dlist) #f))
                           (not (equal? (member 8 dlist) #f)))
                          (begin
                            (set! ok-flag #f)
                            ))

                      (if (equal? ok-flag #t)
                          (begin
                            (let ((rlist
                                   (construct-primes-list
                                    ii dlist prime-array)))
                              (begin
                                (if (not (equal? rlist null-list))
                                    (begin
                                      (set! counter (+ counter 1))
                                      (set! sum (+ sum ii))

                                      (if (equal? debug-flag #t)
                                          (begin
                                            (let ((s1
                                                   (string-join
                                                    (map
                                                     (lambda (this-elem)
                                                       (begin
                                                         (ice-9-format:format
                                                          #f "~:d" this-elem)
                                                         )) rlist)
                                                    ", ")))
                                              (begin
                                                (display
                                                 (ice-9-format:format
                                                  #f "(~:d) ~a are primes, "
                                                  counter s1))
                                                (display
                                                 (ice-9-format:format
                                                  #f "sum so far = ~:d~%"
                                                  sum))
                                                (force-output)
                                                ))
                                            ))
                                      ))
                                ))
                            ))
                      ))
                  ))
            ))

        (display
         (ice-9-format:format
          #f "sum = ~:d and there are ~:d truncatable "
          sum counter))
        (display
         (ice-9-format:format
          #f "primes less than ~:d~%"
          max-num))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "The number 3797 has an interesting "))
    (display
     (format #f "property. Being prime~%"))
    (display
     (format #f "itself, it is possible to continuously "))
    (display
     (format #f "remove digits from~%"))
    (display
     (format #f "left to right, and remain prime at "))
    (display
     (format #f "each stage: ~%"))
    (display
     (format #f "3797, 797, 97, and 7. "))
    (display
     (format #f "Similarly we can work~%"))
    (display
     (format #f "from right to left: 3797, 379, 37, "))
    (display
     (format #f "and 3.~%"))
    (newline)
    (display
     (format #f "Find the sum of the only eleven primes "))
    (display
     (format #f "that are both truncatable~%"))
    (display
     (format #f "from left to right and right to left.~%"))
    (newline)
    (display
     (format #f "NOTE: 2, 3, 5, and 7 are not "))
    (display
     (format #f "considered to be~%"))
    (display
     (format #f "truncatable primes.~%"))
    (display
     (format #f "see https://projecteuler.net/problem=37~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((max-num 1000)
          (debug-flag #t))
      (begin
        (sub-main-loop max-num debug-flag)
        ))

    (newline)
    (let ((max-num 1000000)
          (debug-flag #t))
      (begin
        (sub-main-loop max-num debug-flag)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 37 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (load "prime-module-tests-1.scm")
                 (load "digits-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

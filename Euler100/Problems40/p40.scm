#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 40                                     ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated June 12, 2022                                ###
;;;###                                                       ###
;;;###  updated March 4, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 receive - receive multiple values
(use-modules ((ice-9 receive)
              :renamer (symbol-prefix-proc 'ice-9-receive:)))

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### digits-module for split-digits-list function
(use-modules ((digits-module)
              :renamer (symbol-prefix-proc 'digits-module:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (find-digit-index digit-index)
  (define (local-interp-formula
           min-index max-index digit-index min-num ndigits)
    (begin
      (cond
       ((or (< digit-index min-index)
            (> digit-index max-index))
        (begin
          (display
           (format
            #f "find-digit-index error: digit index ~a~%"
            digit-index))
          (display
           (format
            #f "must be between ~a and ~a~%"
            min-index max-index))
          (display (format #f "quitting...~%"))
          (quit)
          ))
       (else
        (begin
          (let ((tmp1 0)
                (rtmp1 0))
            (begin
              (ice-9-receive:receive
               (tmp1 rtmp1)
               (euclidean/ (- digit-index min-index) ndigits)
               (begin
                 (let ((this-num (+ tmp1 min-num)))
                   (let ((dlist
                          (digits-module:split-digits-list this-num)))
                     (begin
                       (if (zero? rtmp1)
                           (begin
                             (list-ref dlist 0))
                           (begin
                             (list-ref dlist rtmp1)
                             ))
                       )))
                 ))
              ))
          )))
      ))
  (begin
    (let ((num-1-digits 9)
          (num-2-digits (- 99 9))                  ;;; 90, number of 2-digit numbers
          (num-3-digits (- 999 99))                ;;; 900, number of 3-digit numbers
          (num-4-digits (- 9999 999))              ;;; 9,000
          (num-5-digits (- 99999 9999))            ;;; 90,000
          (num-6-digits (- 999999 99999))          ;;; 900,000
          (num-7-digits (- 9999999 999999)))       ;;; 9,000,000
      (let* ((tr-1-index (+ num-1-digits 1))                    ;;; 10, transition from 1-digit numbers to 2-digits
             (tr-2-index (+ tr-1-index (* 2 num-2-digits)))     ;;; 190, transition from 2-digit numbers to 3-digits
             (tr-3-index (+ tr-2-index (* 3 num-3-digits)))     ;;; 2,890
             (tr-4-index (+ tr-3-index (* 4 num-4-digits)))     ;;; 38,890
             (tr-5-index (+ tr-4-index (* 5 num-5-digits)))     ;;; 488,890
             (tr-6-index (+ tr-5-index (* 6 num-6-digits)))     ;;; 5,888,890
             (tr-7-index (+ tr-6-index (* 7 num-7-digits))))    ;;; 68,888,890
        (let ((dresult 0))
          (begin
            (cond
             ((<= digit-index 0)
              (begin
                #f
                ))
             ((and (>= digit-index 0)
                   (< digit-index tr-1-index))
              (begin
                digit-index
                ))
             ((and (>= digit-index tr-1-index)
                   (< digit-index tr-2-index))
              (begin
                (local-interp-formula
                 tr-1-index tr-2-index digit-index 10 2)
                ))
             ((and (>= digit-index tr-2-index)
                   (< digit-index tr-3-index))
              (begin
                (local-interp-formula
                 tr-2-index tr-3-index digit-index 100 3)
                ))
             ((and (>= digit-index tr-3-index)
                   (< digit-index tr-4-index))
              (begin
                (local-interp-formula
                 tr-3-index tr-4-index digit-index 1000 4)
                ))
             ((and (>= digit-index tr-4-index)
                   (< digit-index tr-5-index))
              (begin
                (local-interp-formula
                 tr-4-index tr-5-index digit-index 10000 5)
                ))
             ((and (>= digit-index tr-5-index)
                   (< digit-index tr-6-index))
              (begin
                (local-interp-formula
                 tr-5-index tr-6-index digit-index 100000 6)
                ))
             ((and (>= digit-index tr-6-index)
                   (< digit-index tr-7-index))
              (begin
                (local-interp-formula
                 tr-6-index tr-7-index digit-index 1000000 7)
                ))
             (else
              (begin
                (display
                 (format
                  #f "find-digit-index error: digit index "))
                (display
                 (format
                  #f "~a must be less than ~a~%"
                  digit-index tr-7-index))
                (display (format #f "quitting...~%"))
                (quit)
                )))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-find-digit-index-1 result-hash-table)
 (begin
   (let ((sub-name "test-find-digit-index-1")
         (test-list
          (list
           (list 0 #f) (list 1 1) (list 2 2) (list 3 3) (list 4 4)
           (list 5 5) (list 6 6) (list 7 7) (list 8 8) (list 9 9)
           (list 10 1) (list 11 0) (list 12 1) (list 13 1)
           (list 14 1) (list 15 2) (list 16 1) (list 17 3)
           (list 18 1) (list 19 4) (list 20 1) (list 21 5)
           (list 22 1) (list 23 6) (list 24 1) (list 25 7)
           (list 100 5) (list 1000 3)
           (list 10000 7)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-num (list-ref alist 0))
                  (shouldbe-num (list-ref alist 1)))
              (let ((result-num
                     (find-digit-index test-num)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : test-num=~a, "
                        sub-name test-label-index test-num))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a~%"
                        shouldbe-num result-num)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-num result-num)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (split-method max-num debug-flag)
  (begin
    (let ((this-dlist (list 0 1 2 3 4 5 6 7 8 9)))
      (begin
        (do ((ii 10 (+ ii 1)))
            ((> ii max-num))
          (begin
            (let ((dlist
                   (digits-module:split-digits-list ii)))
              (begin
                (set! this-dlist (append this-dlist dlist))
                ))
            ))

        (let ((d1 (list-ref this-dlist 1))
              (d10 (list-ref this-dlist 10))
              (d100 (list-ref this-dlist 100))
              (d1000 (list-ref this-dlist 1000))
              (d10000 (list-ref this-dlist 10000))
              (d100000 (list-ref this-dlist 100000))
              (d1000000 (list-ref this-dlist 1000000)))
          (let ((product
                 (* d1 d10 d100 d1000 d10000 d100000 d1000000)))
            (begin
              (display
               (format
                #f "the product d(1) x d(10) x d(100) x d(1,000) x "))
              (display
               (format
                #f "d(10,000) x d(100,000) x d(1,000,000)~%"))
              (display
               (ice-9-format:format
                #f "~a x ~a x ~a x ~a x ~a x ~a x ~a = ~:d~%"
                d1 d10 d100 d1000 d10000 d100000 d1000000 product))
              (force-output)
              )))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "An irrational decimal fraction is "))
    (display
     (format #f "created by concatenating~%"))
    (display
     (format #f "the positive integers:~%"))
    (newline)
    (display
     (format #f "0.123456789101112131415161718192021...~%"))
    (newline)
    (display
     (format #f "It can be seen that the 12th digit "))
    (display
     (format #f "of the fractional part is 1.~%"))
    (display
     (format #f "If dn represents the nth digit of "))
    (display
     (format #f "the fractional part, find~%"))
    (display
     (format #f "the value of the following expression.~%"))
    (display
     (format #f "d1 x d10 x d100 x d1000 x d10000 x d100000 x d1000000~%"))
    (newline)

    (display
     (format #f "This program works using a simple "))
    (display
     (format #f "counting method to~%"))
    (display
     (format #f "find the nth decimal place. First "))
    (display
     (format #f "consider that the~%"))
    (display
     (format #f "first 9 digits go from 1 through 9. "))
    (display
     (format #f "Then digit number~%"))
    (display
     (format #f "10 is the start of number 10, and "))
    (display
     (format #f "digit 11 is the~%"))
    (display
     (format #f "second digit of 10. Since each "))
    (display
     (format #f "concatenated~%"))
    (display
     (format #f "number between 10 and 99 contributes "))
    (display
     (format #f "2 digits, and~%"))
    (display
     (format #f "each number between 100 and 999 "))
    (display
     (format #f "contributes 3 digits,~%"))
    (display
     (format #f "and each number between 1000 and 9999 "))
    (display
     (format #f "contribute 4 digits,~%"))
    (display
     (format #f "it's possible to calculate each digit "))
    (display
     (format #f "manually. Since there~%"))
    (display
     (format #f "are 90 numbers between 99 and 10 "))
    (display
     (format #f "(inclusive), there are~%"))
    (display
     (format #f "90*2=180 digits that are added to the "))
    (display
     (format #f "decimal (on top of~%"))
    (display
     (format #f "the original 9).~%"))
    (newline)
    (display
     (format #f "d100: to find the nth digit between "))
    (display
     (format #f "index 10 and 189,~%"))
    (display
     (format #f "simply use an interpolating formula, "))
    (display
     (format #f "index ten you will~%"))
    (display
     (format #f "find the number 10, at index 190 you "))
    (display
     (format #f "will find the~%"))
    (display
     (format #f "number 100. What number will you find "))
    (display
     (format #f "at index 12?~%"))
    (display
     (format #f "index 10 + 2*(x-10) = 12 where x is "))
    (display
     (format #f "the number to find.~%"))
    (display
     (format #f "to see that f(x)=10+2*(x-10) is the "))
    (display
     (format #f "right formula, examine~%"))
    (display
     (format #f "the end points: f(x=10)=10, and "))
    (display
     (format #f "f(x=100)=190~%"))
    (display
     (format #f "then 2*(x-10)=2, and x = 11. For "))
    (display
     (format #f "index 14,~%"))
    (display
     (format #f "10 + 2*(x-10) = 14, gives 2*(x-10)=4, "))
    (display
     (format #f "or x=12.~%"))
    (display
     (format #f "This means that at d12=1, d13=1, "))
    (display
     (format #f "d14=1, d15=2. So~%"))
    (display
     (format #f "to find d100, 10 + 2*(x-10) = 100, "))
    (display
     (format #f "2*(x-10)=90, x=55,~%"))
    (display
     (format #f "and d100=5.~%"))
    (newline)
    (display
     (format #f "d1000: this same type of formula works "))
    (display
     (format #f "for finding d1000,~%"))
    (display
     (format #f "except now the formulas change, as we "))
    (display
     (format #f "enter 3-digit territory.~%"))
    (display
     (format #f "position 190 is the first occurance "))
    (display
     (format #f "of 100, and~%"))
    (display
     (format #f "2890 is the first occurance of the "))
    (display
     (format #f "number 1000. The~%"))
    (display
     (format #f "index formula becomes 190 + 3*(x-100) "))
    (display
     (format #f "= 1000. Note that~%"))
    (display
     (format #f "the difference between the indicies, "))
    (display
     (format #f "190 < 1000 < 2890,~%"))
    (display
     (format #f "and the concatenated numbers which fall "))
    (display
     (format #f "in this range~%"))
    (display
     (format #f "(100 - 999). The interpolation formula is~%"))
    (display
     (format #f "now f(x) = 190 + 3*(x-100).~%"))
    (display
     (format #f "at the end points, f(x=100)=190, "))
    (display
     (format #f "f(x=1000)=2890.~%"))
    (display
     (format #f "Solving for the concatenated number "))
    (display
     (format #f "x, 3*(x-100)=810,~%"))
    (display
     (format #f "x-100=270, or x=370, and d1000=3.~%"))
    (newline)
    (display
     (format #f "d10000: the number 1000 is at index 2890, "))
    (display
     (format #f "and the number~%"))
    (display
     (format #f "10000 is at index 38890. "))
    (display
     (format #f "f(x)=2890+4*(x-1000)~%"))
    (display
     (format #f "f(x=1000)=2890, f(x=10000)=38890. "))
    (display
     (format #f "Since 2890 < 10,000 < 38,890,~%"))
    (display
     (format #f "we can use this equation to find the x "))
    (display
     (format #f "which gives us the~%"))
    (display
     (format #f "index 10,000, or 2890+4*(x-1000)=10000, "))
    (display
     (format #f "4*(x-1000)=7110,~%"))
    (display
     (format #f "or x-1000=1777.5, x=2777.5. The extra 0.5 "))
    (display
     (format #f "means that 10,000~%"))
    (display
     (format #f "straddles a 4-digit number.~%"))
    (display
     (format #f "so look at the index 2 before, "))
    (display
     (format #f "2890+4*(x-1000)=9998,~%"))
    (display
     (format #f "4*(x-1000)=7108, x-1000=1777, x=2777.~%"))
    (display
     (format #f "therefore the 2 appears at index 9998, "))
    (display
     (format #f "7 appears at 9999,~%"))
    (display
     (format #f "and d10000=7 appears at 10,000.~%"))
    (newline)
    (display
     (format #f "d100000: the number 10000 is at "))
    (display
     (format #f "index 38890, the number~%"))
    (display
     (format #f "100000 is at index 488,890, this is a "))
    (display
     (format #f "region of 5-digit~%"))
    (display
     (format #f "numbers, f(x)=38890+5*(x-10000), "))
    (display
     (format #f "f(x=10000)=38890~%"))
    (display
     (format #f "f(x=100000)=38890+5*90000=488,890, "))
    (display
     (format #f "which implies that we~%"))
    (display
     (format #f "can use this formula for d100,000. "))
    (display
     (format #f "Find the x~%"))
    (display
     (format #f "which is at position 100,000:~%"))
    (display
     (format #f "38890+5*(x-10000)=100,000, "))
    (display
     (format #f "5*(x-10000)=61110,~%"))
    (display
     (format #f "x-10000=12222, x=22222, so d100000=2~%"))
    (newline)
    (display
     (format #f "d1million: the number 100,000 is at "))
    (display
     (format #f "index 488,890 and~%"))
    (display
     (format #f "the number 1,000,000 is at index "))
    (display
     (format #f "5,888,890, a region~%"))
    (display
     (format #f "of 6-digit numbers, so~%"))
    (display
     (format #f "f(x)=488,890+6*(x-100,000)~%"))
    (display
     (format #f "consistency check: f(x=100,000)=488,890,~%"))
    (display
     (format #f "f(x=1,000,000)=488,890+6*900,000=5,888,890.~%"))
    (display
     (format #f "find the x which is at position 1,000,000:~%"))
    (display
     (format #f "488,890+6*(x-100,000)=1,000,000~%"))
    (display
     (format #f "6*(x-100,000)=511,110, "))
    (display
     (format #f "(x-100,000)=85185, x=185185,~%"))
    (display
     (format #f "so d1million=1~%"))
    (newline)
    (display
     (format #f "Solution d(1) x d(10) x d(100) x d(1000) "))
    (display
     (format #f "x d(10000) x d(100000) x d(1000000)~%"))
    (display
     (format #f "= 1x1x5x3x7x2x1 = 210~%"))
    (display
     (format #f "see https://projecteuler.net/problem=40~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((d1 (find-digit-index 1))
          (d10 (find-digit-index 10))
          (d100 (find-digit-index 100))
          (d1000 (find-digit-index 1000))
          (d10000 (find-digit-index 10000))
          (d100000 (find-digit-index 100000))
          (d1000000 (find-digit-index 1000000)))
      (let ((product
             (* d1 d10 d100 d1000 d10000 d100000 d1000000)))
        (begin
          (display
           (format
            #f "the product d(1) x d(10) x d(100) x d(1,000) x "))
          (display
           (format
            #f "d(10,000) x d(100,000) x d(1,000,000)~%"))

          (display
           (ice-9-format:format
            #f "~a x ~a x ~a x ~a x ~a x ~a x ~a = ~:d~%"
            d1 d10 d100 d1000 d10000 d100000 d1000000 product))
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 40 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (load "digits-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 34                                     ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated June 12, 2022                                ###
;;;###                                                       ###
;;;###  updated March 4, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### srfi-1 for fold functions
(use-modules ((srfi srfi-1)
              :renamer (symbol-prefix-proc 'srfi-1:)))

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### digits-module for split-digits-list function
(use-modules ((digits-module)
              :renamer (symbol-prefix-proc 'digits-module:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (factorial nn)
  (begin
    (cond
     ((< nn 0)
      (begin
        #f
        ))
     ((= nn 0)
      (begin
        1
        ))
     (else
      (begin
        (* nn (factorial (- nn 1)))
        )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-factorial-1 result-hash-table)
 (begin
   (let ((sub-name "test-factorial-1")
         (test-list
          (list
           (list 0 1) (list 1 1) (list 2 2)
           (list 3 6) (list 4 24) (list 5 120)
           (list 6 720)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-num (list-ref alist 0))
                  (shouldbe-num (list-ref alist 1)))
              (let ((result-num (factorial test-num)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : test-num=~a, "
                        sub-name test-label-index test-num))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a~%"
                        shouldbe-num result-num)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-num result-num)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (populate-factorial-hash! factorial-htable)
  (begin
    (do ((ii 0 (1+ ii)))
        ((> ii 9))
      (begin
        (let ((ifac (factorial ii)))
          (begin
            (hash-set! factorial-htable ii ifac)
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (sum-factorial-digits anum factorial-htable)
  (begin
    (let ((dlist
           (digits-module:split-digits-list anum)))
      (let ((dflist
             (map
              (lambda (this-digit)
                (begin
                  (let ((fn
                         (hash-ref
                          factorial-htable this-digit 1)))
                    (begin
                      fn
                      ))
                  )) dlist)))
        (let ((fac-sum (srfi-1:fold + 0 dflist)))
          (begin
            fac-sum
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-sum-factorial-digits-1 result-hash-table)
 (begin
   (let ((sub-name "test-sum-factorial-digits-1")
         (test-list
          (list
           (list 0 1) (list 1 1) (list 2 2)
           (list 9 362880) (list 10 2) (list 11 2)
           (list 19 362881) (list 29 362882)
           (list 123 9) (list 1234 33)
           (list 99 725760)
           ))
         (factorial-htable (make-hash-table 10))
         (test-label-index 0))
     (begin
       (populate-factorial-hash! factorial-htable)

       (for-each
        (lambda (alist)
          (begin
            (let ((test-num (list-ref alist 0))
                  (shouldbe-num (list-ref alist 1)))
              (let ((result-num
                     (sum-factorial-digits
                      test-num factorial-htable)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : test-num=~a, "
                        sub-name test-label-index test-num))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a~%"
                        shouldbe-num result-num)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-num result-num)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (debug-test-print)
  (begin
    (let ((t-list
           (list
            2540160 2540159 2540158 1999999 1999998
            1999988 1999888 1998888 1988888 1888888)))
      (let ((st-list (sort t-list >))
            (factorial-htable (make-hash-table 10)))
        (begin
          (populate-factorial-hash! factorial-htable)

          (for-each
           (lambda (anum)
             (begin
               (let ((sum
                      (sum-factorial-digits
                       anum factorial-htable)))
                 (begin
                   (display
                    (ice-9-format:format
                     #f "The sum of factorial of digits for "))
                   (display
                    (ice-9-format:format
                     #f "~:d is ~:d~%" anum sum))
                   ))
               )) st-list)
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop max-num debug-flag)
  (begin
    (let ((sum 0)
          (counter 0)
          (factorial-htable (make-hash-table 20)))
      (begin
        (populate-factorial-hash! factorial-htable)

        (do ((ii 3 (+ ii 1)))
            ((> ii max-num))
          (begin
            (let ((fac-sum
                   (sum-factorial-digits
                    ii factorial-htable)))
              (begin
                (if (= fac-sum ii)
                    (begin
                      (if (equal? debug-flag #t)
                          (begin
                            (let ((ii-list
                                   (digits-module:split-digits-list ii)))
                              (let ((s1
                                     (string-join
                                      (map
                                       (lambda (this-elem)
                                         (begin
                                           (format #f "~a!" this-elem)
                                           )) ii-list)
                                      " + "))
                                    (s2
                                     (string-join
                                      (map
                                       (lambda (this-elem)
                                         (begin
                                           (ice-9-format:format
                                            #f "~:d"
                                            (hash-ref factorial-htable this-elem 1))
                                           )) ii-list)
                                      " + ")))
                                (begin
                                  (display
                                   (ice-9-format:format
                                    #f "~a = ~a = ~:d~%"
                                    s1 s2 ii))
                                  (force-output)
                                  )))
                            ))
                      (set! sum (+ sum ii))
                      (set! counter (1+ counter))
                      ))
                ))
            ))

        (display
         (ice-9-format:format
          #f "sum of all numbers that are equal to the sum of~%"))
        (display
         (ice-9-format:format
          #f "their digits = ~:d, " sum))
        (display
         (ice-9-format:format
          #f "(~:d numbers found, less than ~:d)~%"
          counter max-num))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "145 is a curious number, as~%"))
    (display
     (format #f "1! + 4! + 5! = 1 + 24 + 120 = 145.~%"))
    (newline)
    (display
     (format #f "Find the sum of all numbers which "))
    (display
     (format #f "are equal to the~%"))
    (display
     (format #f "sum of the factorial of their digits.~%"))
    (newline)
    (display
     (format #f "Note: as 1! = 1 and 2! = 2 are not "))
    (display
     (format #f "sums they are not~%"))
    (display
     (format #f "included.~%"))
    (newline)
    (display
     (format #f "The sum of factorial of digits means "))
    (display
     (format #f "that the digits~%"))
    (display
     (format #f "matter, not the order. 9! is 362,880, "))
    (display
     (format #f "so the largest~%"))
    (display
     (format #f "number that can be a sum of it's "))
    (display
     (format #f "factorials of it's~%"))
    (display
     (format #f "digits is 7x9! = 2,540,160, since an "))
    (display
     (format #f "8-digit number would~%"))
    (display
     (format #f "have at most a 7-digit sum of digits.~%"))
    (display
     (format #f "The sum of factorial of digits for "))
    (display
     (format #f "2,540,160 is 869.~%"))
    (display
     (format #f "The sum of factorial of digits for "))
    (display
     (format #f "2,540,159 is 363,148.~%"))
    (display
     (format #f "The sum of factorial of digits for "))
    (display
     (format #f "2,540,158 is 40,588.~%"))
    (display
     (format #f "The sum of factorial of digits of "))
    (display
     (format #f "1,999,999 is 2,177,281.~%"))
    (display
     (format #f "The sum of factorial of digits of "))
    (display
     (format #f "1,999,998 is 1,854,721.~%"))
    (display
     (format #f "The sum of factorial of digits of "))
    (display
     (format #f "1,999,988 is 1,532,161.~%"))
    (display
     (format #f "The sum of factorial of digits of "))
    (display
     (format #f "1,999,888 is 1,209,601.~%"))
    (display
     (format #f "The sum of factorial of digits of "))
    (display
     (format #f "1,998,888 is 887,041.~%"))
    (display
     (format #f "The sum of factorial of digits of "))
    (display
     (format #f "1,988,888 is 564,481.~%"))
    (display
     (format #f "The sum of factorial of digits of "))
    (display
     (format #f "1,888,888 is 241,921.~%"))
    (display
     (format #f "So from this we can see that the "))
    (display
     (format #f "maximum number that~%"))
    (display
     (format #f "we need to consider is around 250,000, "))
    (display
     (format #f "since there is no~%"))
    (display
     (format #f "other way to get a larger sum of "))
    (display
     (format #f "digits (less "))
    (display
     (format #f "than 2,540,160).~%"))
    (display
     (format #f "see https://projecteuler.net/problem=34~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((max-num 1000)
          (debug-flag #t))
      (begin
        (sub-main-loop max-num debug-flag)
        ))

    (let ((max-num 250000)
          (debug-flag #t))
      (begin
        (sub-main-loop max-num debug-flag)
        (newline)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 34 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (load "digits-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 36                                     ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated June 12, 2022                                ###
;;;###                                                       ###
;;;###  updated March 4, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (palindrome? nn base)
  (begin
    (cond
     ((< nn 0)
      (begin
        #f
        ))
     (else
      (begin
        (let ((tstring (number->string nn base)))
          (let ((rstring (string-reverse tstring)))
            (begin
              (string-ci=? tstring rstring)
              )))
        )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-palindrome-1 result-hash-table)
 (begin
   (let ((sub-name "test-palindrome")
         (test-list
          (list
           (list 3 10 #t) (list 3 2 #t)
           (list 7 10 #t) (list 7 2 #t)
           (list 10 10 #f) (list 11 10 #t)
           (list 12 10 #f) (list 13 10 #f)
           (list 20 10 #f) (list 21 10 #f)
           (list 22 10 #t) (list 23 10 #f)
           (list 313 10 #t) (list 323 10 #t)
           (list 333 10 #t) (list 334 10 #f)
           (list 585 10 #t) (list 585 2 #t)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((test-num (list-ref this-list 0))
                  (test-base (list-ref this-list 1))
                  (shouldbe (list-ref this-list 2)))
              (let ((result
                     (palindrome? test-num test-base)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "test-num=~a, test-base=~a, "
                        test-num test-base))
                      (err-3
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append
                      err-1 err-2 err-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop max-num debug-flag)
  (begin
    (let ((sum 0)
          (counter 0)
          (null-list (list)))
      (begin
        (do ((ii 1 (+ ii 1)))
            ((> ii max-num))
          (begin
            (if (and
                 (palindrome? ii 10)
                 (palindrome? ii 2))
                (begin
                  (set! counter (+ counter 1))
                  (set! sum (+ sum ii))

                  (if (equal? debug-flag #t)
                      (begin
                        (let ((s1 (number->string ii 10))
                              (s2 (number->string ii 2)))
                          (begin
                            (display
                             (ice-9-format:format
                              #f "(~:d) ~a = ~a (binary) "
                              counter s1 s2))
                            (display
                             (ice-9-format:format
                              #f "(sum so far = ~:d)~%"
                              sum))
                            (force-output)
                            ))
                        ))
                  ))
            ))

        (display
         (ice-9-format:format
          #f "The sum of all palindromic numbers in base 10~%"
          sum))
        (display
         (ice-9-format:format
          #f "and base 2 = ~:d, and there are~%"
          sum))
        (display
         (ice-9-format:format
          #f "~:d such numbers less than ~:d~%"
          counter max-num))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "The decimal number, 585 = "))
    (display
     (format #f "1001001001 (binary), is~%"))
    (display
     (format #f "palindromic in both bases.~%"))
    (newline)
    (display
     (format #f "Find the sum of all numbers, less "))
    (display
     (format #f "than one million, which~%"))
    (display
     (format #f "are palindromic in base 10 and "))
    (display
     (format #f "base 2.~%"))
    (newline)
    (display
     (format #f "Please note that the palindromic "))
    (display
     (format #f "number, in either base,~%"))
    (display
     (format #f "may not include leading zeros.)~%"))
    (display
     (format #f "see https://projecteuler.net/problem=36~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((max-num 10)
          (debug-flag #t))
      (begin
        (sub-main-loop max-num debug-flag)
        ))

    (let ((max-num 1000000)
          (debug-flag #t))
      (begin
        (sub-main-loop max-num debug-flag)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 36 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

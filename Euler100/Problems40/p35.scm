#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 35                                     ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated June 12, 2022                                ###
;;;###                                                       ###
;;;###  updated March 6, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### srfi-1 for fold functions
(use-modules ((srfi srfi-1)
              :renamer (symbol-prefix-proc 'srfi-1:)))

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### prime-module for make-prime-array and is-prime-array? function
(use-modules ((prime-module)
              :renamer (symbol-prefix-proc 'prime-module:)))

;;;### digits-module for split-digits-list function
(use-modules ((digits-module)
              :renamer (symbol-prefix-proc 'digits-module:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (rotate-digits-left this-num)
  (begin
    (let ((dlist
           (digits-module:split-digits-list this-num)))
      (let ((dlen (length dlist)))
        (let ((max-index (1- dlen))
              (result-list (list (list-ref dlist 0))))
          (begin
            (do ((ii max-index (- ii 1)))
                ((<= ii 0))
              (begin
                (set!
                 result-list
                 (cons (list-ref dlist ii) result-list))
                ))
            (srfi-1:fold
             (lambda (this-elem previous)
               (begin
                 (+ this-elem (* 10 previous))))
             0 result-list)
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-rotate-digits-left-1 result-hash-table)
 (begin
   (let ((sub-name "test-rotate-digits-left-1")
         (test-list
          (list
           (list 197 971) (list 971 719) (list 719 197)
           (list 13 31) (list 17 71) (list 37 73) (list 73 37)
           (list 1234 2341) (list 2341 3412)
           (list 3412 4123) (list 4123 1234)
           (list 12345 23451)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-num (list-ref alist 0))
                  (shouldbe-num (list-ref alist 1)))
              (let ((result-num
                     (rotate-digits-left test-num)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : test-num=~a, "
                        sub-name test-label-index test-num))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a~%"
                        shouldbe-num result-num)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-num result-num)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (rotate-digits-list-left dlist)
  (begin
    (let ((result-list
           (append
            (list-tail dlist 1)
            (list-head dlist 1))))
      (begin
        result-list
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-rotate-digits-list-left-1 result-hash-table)
 (begin
   (let ((sub-name "test-rotate-digits-list-left-1")
         (test-list
          (list
           (list (list 1 3) (list 3 1))
           (list (list 1 7) (list 7 1))
           (list (list 3 7) (list 7 3))
           (list (list 7 3) (list 3 7))
           (list (list 1 9 7) (list 9 7 1))
           (list (list 9 7 1) (list 7 1 9))
           (list (list 7 1 9) (list 1 9 7))
           (list (list 1 2 3 4) (list 2 3 4 1))
           (list (list 2 3 4 1) (list 3 4 1 2))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((input-list (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result
                     (rotate-digits-list-left input-list)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : input-list=~a, "
                        sub-name test-label-index input-list))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a~%"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; return empty list if not a circular prime
(define (circular-prime-list this-num dlist prime-array)
  (begin
    (let ((dlen (- (length dlist) 1))
          (digit-list (list-copy dlist))
          (result-list (list this-num))
          (break-flag #f)
          (local-num this-num))
      (begin
        (do ((ii 0 (+ ii 1)))
            ((or (>= ii dlen)
                 (equal? break-flag #t)))
          (begin
            (let ((next-dlist
                   (rotate-digits-list-left digit-list)))
              (let ((next-num
                     (digits-module:digit-list-to-number
                      next-dlist)))
                (let ((pflag
                       (prime-module:is-array-prime?
                        next-num prime-array)))
                  (begin
                    (if (equal? pflag #t)
                        (begin
                          (set!
                           result-list
                           (cons next-num result-list))
                          (set! digit-list next-dlist))
                        (begin
                          (set! result-list (list))
                          (set! break-flag #t)
                          ))
                    ))
                ))
            ))
        (reverse result-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-circular-prime-list-1 result-hash-table)
 (begin
   (let ((sub-name "test-circular-prime-list")
         (test-list
          (list
           (list 197 (list 197 971 719))
           (list 13 (list 13 31))
           (list 17 (list 17 71))
           (list 37 (list 37 73))
           (list 1234 (list))
           ))
         (prime-array (prime-module:make-prime-array 20))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-num (list-ref alist 0))
                  (shouldbe-list (list-ref alist 1)))
              (let ((dlist
                     (digits-module:split-digits-list
                      test-num)))
                (let ((result-list
                       (circular-prime-list
                        test-num dlist prime-array)))
                  (let ((err-1
                         (format
                          #f "~a :: (~a) error : test-num=~a, "
                          sub-name test-label-index test-num))
                        (err-2
                         (format
                          #f "shouldbe=~a, result=~a~%"
                          shouldbe-list result-list)))
                    (begin
                      (unittest2:assert?
                       (equal? shouldbe-list result-list)
                       sub-name
                       (string-append err-1 err-2)
                       result-hash-table)
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define-syntax process-unseen-prime-macro
  (syntax-rules ()
    ((process-unseen-prime-macro
      seen-htable prime-array ii dlist
      counter null-list debug-flag)
     (begin
       (let ((rlist
              (circular-prime-list
               ii dlist prime-array)))
         (begin
           (if (not (equal? rlist null-list))
               (begin
                 (set! counter (+ counter 1))
                 (for-each
                  (lambda (pnum)
                    (begin
                      (hash-set! seen-htable pnum #t)
                      )) rlist)

                 (if (equal? debug-flag #t)
                     (begin
                       (let ((s1
                              (string-join
                               (map
                                (lambda (this-elem)
                                  (begin
                                    (ice-9-format:format
                                     #f "~:d" this-elem)
                                    )) rlist)
                               ", ")))
                         (begin
                           (display
                            (ice-9-format:format
                             #f "(~:d) ~a are prime~%"
                             counter s1))
                           (force-output)
                           ))
                       ))
                 ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop max-num debug-flag)
  (begin
    (let ((counter 1)
          (seen-htable (make-hash-table))
          (prime-array
           (prime-module:make-prime-array
            (* 2 (exact-integer-sqrt max-num))))
          (null-list (list)))
      (begin
        (if (equal? debug-flag #t)
            (begin
              (display
               (ice-9-format:format
                #f "(~:d) ~a are prime~%"
                counter 2))
              ))

        (do ((ii 3 (+ ii 2)))
            ((> ii max-num))
          (begin
            (let ((pflag
                   (prime-module:is-array-prime?
                    ii prime-array)))
              (begin
                (if (equal? pflag #t)
                    (begin
                      (let ((sflag
                             (hash-ref seen-htable ii #f)))
                        (begin
                          (if (equal? sflag #f)
                              (begin
                                (let ((ok-flag #t)
                                      (dlist
                                       (digits-module:split-digits-list ii)))
                                  (begin
                                    (if (or
                                         (not (equal? (member 0 dlist) #f))
                                         (not (equal? (member 2 dlist) #f))
                                         (not (equal? (member 4 dlist) #f))
                                         (not (equal? (member 6 dlist) #f))
                                         (not (equal? (member 8 dlist) #f))
                                         (and
                                          (not (= ii 5))
                                          (not (equal? (member 5 dlist) #f))))
                                        (begin
                                          (set! ok-flag #f)
                                          ))
                                    (if (equal? ok-flag #t)
                                        (begin
                                          (process-unseen-prime-macro
                                           seen-htable prime-array ii dlist
                                           counter null-list debug-flag)
                                          ))
                                    )))
                              (begin
                                (set! counter (1+ counter))
                                ))
                          ))
                      ))
                ))
            ))
        (display
         (ice-9-format:format
          #f "There are ~:d circular primes less than ~:d~%"
          counter max-num))
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "The number, 197, is called a circular "))
    (display
     (format #f "prime because all~%"))
    (display
     (format #f "rotations of the digits: 197, 971, "))
    (display
     (format #f "and 719, are~%"))
    (display
     (format #f "themselves prime.~%"))
    (newline)
    (display
     (format #f "There are thirteen such primes "))
    (display
     (format #f "below 100:~%"))
    (display
     (format #f "2, 3, 5, 7, 11, 13, 17, 31, 37, 71, "))
    (display
     (format #f "73, 79, and 97.~%"))
    (newline)
    (display
     (format #f "How many circular primes are there "))
    (display
     (format #f "below one million?~%"))
    (display
     (format #f "see https://projecteuler.net/problem=35~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((max-num 100)
          (debug-flag #t))
      (begin
        (sub-main-loop max-num debug-flag)
        ))

    (let ((max-num 1000000)
          (debug-flag #f))
      (begin
        (sub-main-loop max-num debug-flag)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 35 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (load "digits-module-tests-1.scm")
                 (load "prime-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 31                                     ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated June 12, 2022                                ###
;;;###                                                       ###
;;;###  updated March 4, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (dynamic-change-list target-amount coin-list debug-flag)
  (begin
    (let ((dynamic-array
           (make-array 0 (+ target-amount 1) (length coin-list)))
          (coin-array (list->array 1 coin-list))
          (coin-length (length coin-list)))
      (begin
        ;;; populate the first row with 1's
        (do ((ii 0 (1+ ii)))
            ((>= ii coin-length))
          (begin
            (array-set! dynamic-array 1 0 ii)
            ))

        (do ((ii 0 (1+ ii)))
            ((> ii target-amount))
          (begin
            (array-set! dynamic-array 1 ii 0)

            (if (equal? debug-flag #t)
                (begin
                  (display
                   (ice-9-format:format
                    #f "(~:d) : 1 " ii))
                  (force-output)
                  ))

            (do ((jj 1 (1+ jj)))
                ((>= jj coin-length))
              (begin
                (if (>= ii (array-ref coin-array jj))
                    (begin
                      ;;; then add previous value
                      (let ((prev-sum
                             (array-ref dynamic-array ii (- jj 1)))
                            (sub-coin
                             (- ii (array-ref coin-array jj))))
                        (let ((sub-target
                               (array-ref
                                dynamic-array sub-coin jj)))
                          (begin
                            (array-set!
                             dynamic-array
                             (+ prev-sum sub-target) ii jj)
                            ))))
                    (begin
                      ;;; can't make change with this coin, so just copy previous result
                      (array-set!
                       dynamic-array
                       (array-ref dynamic-array ii (- jj 1))
                       ii jj)
                      ))

                (if (equal? debug-flag #t)
                    (begin
                      (if (= jj 1)
                          (begin
                            (display
                             (ice-9-format:format
                              #f "~:d"
                              (array-ref dynamic-array ii jj))))
                          (begin
                            (display
                             (ice-9-format:format
                              #f " ~:d"
                              (array-ref dynamic-array ii jj)))
                            ))
                      (force-output)
                      ))
                ))

            (if (equal? debug-flag #t)
                (begin
                  (newline)
                  (force-output)
                  ))
            ))

        (let ((final-value
               (array-ref
                dynamic-array
                target-amount (- coin-length 1))))
          (begin
            final-value
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-dynamic-change-list-1 result-hash-table)
 (begin
   (let ((sub-name "test-dynamic-change-list-1")
         (test-list
          (list
           (list 1 (list 1 2 5 10 20 50 100 200) 1)
           (list 2 (list 1 2 5 10 20 50 100 200) 2)
           (list 3 (list 1 2 5 10 20 50 100 200) 2)
           (list 4 (list 1 2 5 10 20 50 100 200) 3)
           (list 5 (list 1 2 5 10 20 50 100 200) 4)
           (list 6 (list 1 2 5 10 20 50 100 200) 5)
           (list 7 (list 1 2 5 10 20 50 100 200) 6)
           (list 8 (list 1 2 5 10 20 50 100 200) 7)
           (list 9 (list 1 2 5 10 20 50 100 200) 8)
           (list 10 (list 1 2 5 10 20 50 100 200) 11)
           (list 11 (list 1 2 5 10 20 50 100 200) 12)
           (list 195 (list 1 2 5 10 20 50 100 200) 65934)
           (list 196 (list 1 2 5 10 20 50 100 200) 67425)
           (list 197 (list 1 2 5 10 20 50 100 200) 68916)
           (list 198 (list 1 2 5 10 20 50 100 200) 70407)
           (list 199 (list 1 2 5 10 20 50 100 200) 71898)
           (list 200 (list 1 2 5 10 20 50 100 200) 73682)
           ))
         (test-label-index 0)
         (debug-flag #f))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((target-num (list-ref alist 0))
                  (coin-list (list-ref alist 1))
                  (shouldbe (list-ref alist 2)))
              (let ((result
                     (dynamic-change-list
                      target-num coin-list debug-flag)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : target-num=~a, coin-list=~a, "
                        sub-name test-label-index
                        target-num coin-list))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a~%"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (dynamic-main-loop
         target-amount coin-list debug-flag)
  (begin
    (let ((num-ways
           (dynamic-change-list
            target-amount coin-list debug-flag)))
      (begin
        (display
         (ice-9-format:format
          #f "~:d = number of ways to make change for ~:d : "
          num-ways target-amount))
        (display
         (ice-9-format:format
          #f "with coin list ~a (dynamic method)~%"
          coin-list))
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "In England the currency is made up "))
    (display
     (format #f "of pound, and~%"))
    (display
     (format #f "pence, p, and there are eight coins "))
    (display
     (format #f "in general circulation:~%"))
    (newline)
    (display
     (format #f "1p, 2p, 5p, 10p, 20p, 50p, £1 (100p) and £2 (200p).~%"))
    (display
     (format #f "It is possible to make £2 in the "))
    (display
     (format #f "following way:~%"))
    (display
     (format #f "1x£1 + 1x50p + 2x20p + 1x5p + 1x2p + 3x1p~%"))
    (newline)
    (display
     (format #f "How many different ways can 200p be "))
    (display
     (format #f "made using any~%"))
    (display
     (format #f "number of coins?~%"))
    (newline)
    (display
     (format #f "The solution can be found from~%"))
    (display
     (format #f "https://ttsiodras.github.com/euler31.html~%"))
    (newline)
    (display
     (format #f "This program uses the dynamic "))
    (display
     (format #f "programming method to~%"))
    (display
     (format #f "find the number of ways to make change.~%"))
    (display
     (format #f "see https://projecteuler.net/problem=31~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((target-amount 10)
          (coin-list (list 1 2 5 10))
          (debug-flag #t))
      (begin
        (dynamic-main-loop target-amount coin-list debug-flag)
        ))

    (newline)
    (let ((target-amount 200)
          (coin-list (list 1 2 5 10 20 50 100 200))
          (debug-flag #f))
      (begin
        (dynamic-main-loop target-amount coin-list debug-flag)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 31 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (force-output)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

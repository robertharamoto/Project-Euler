#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 38                                     ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated June 12, 2022                                ###
;;;###                                                       ###
;;;###  updated March 6, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### srfi-1 for fold functions
(use-modules ((srfi srfi-1)
              :renamer (symbol-prefix-proc 'srfi-1:)))

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### digits-module for split-digits-list function
(use-modules ((digits-module)
              :renamer (symbol-prefix-proc 'digits-module:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (is-list-1-9-pandigital? digit-list)
  (begin
    (let ((max-digit 9)
          (ok-flag #t))
      (let ((llen (length digit-list)))
        (begin
          (if (not (= llen 9))
              (begin
                #f)
              (begin
                (do ((ii 1 (+ ii 1)))
                    ((or (> ii max-digit)
                         (equal? ok-flag #f)))
                  (begin
                    (if (equal? (member ii digit-list) #f)
                        (begin
                          (set! ok-flag #f)
                          #f
                          ))
                    ))
                ok-flag
                ))
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-is-list-1-9-pandigital-1 result-hash-table)
 (begin
   (let ((sub-name "test-is-list-1-9-pandigital-1")
         (test-list
          (list
           (list (list 1 2 3 4 5 6 7 8 9) #t)
           (list (list 9 2 3 4 5 6 7 8 1) #t)
           (list (list 9 2 3 5 4 6 7 8 1) #t)
           (list (list 8 2 3 4 5 6 7 8 9) #f)
           (list (list 8 2 3 4 5 1 7 8 9) #f)
           (list (list 1 2 3 4 5 6 7 8 9 3) #f)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((input-list (list-ref alist 0))
                  (shouldbe-bool (list-ref alist 1)))
              (let ((result-bool
                     (is-list-1-9-pandigital? input-list)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : input-list=~a, "
                        sub-name test-label-index input-list))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe-bool result-bool)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-bool result-bool)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; hardwire 9 since we are looking for a 1-9 pandigital
(define (concatenated-product-list this-num)
  (begin
    (let ((dlist
           (digits-module:split-digits-list this-num))
          (acc-num-list (list))
          (result-list (list))
          (acc-dlist (list))
          (break-flag #f))
      (let ((max-iter
             (1+ (euclidean/ 9 (length dlist)))))
        (begin
          (do ((jj 1 (+ jj 1)))
              ((or (> jj max-iter)
                   (equal? break-flag #t)))
            (begin
              (let ((aa (* this-num jj)))
                (let ((alist
                       (digits-module:split-digits-list aa)))
                  (begin
                    (set! result-list (cons jj result-list))
                    (set! acc-num-list (cons aa acc-num-list))
                    (set! acc-dlist (append acc-dlist alist))
                    (if (>= (length acc-dlist) 9)
                        (begin
                          (set! break-flag #t)
                          ))
                    )))
              ))
          (if (equal?
               (is-list-1-9-pandigital? acc-dlist)
               #t)
              (begin
                (let ((this-pandigital
                       (digits-module:digit-list-to-number
                        acc-dlist)))
                  (begin
                    (append
                     (list this-pandigital)
                     (list (reverse result-list)))
                    )))
              (begin
                (list)
                ))
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-concatenated-product-list-1 result-hash-table)
 (begin
   (let ((sub-name "test-concatenated-product-list-1")
         (test-list
          (list
           (list 9 (list 918273645 (list 1 2 3 4 5)))
           (list 10 (list))
           (list 192 (list 192384576 (list 1 2 3)))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-num (list-ref alist 0))
                  (shouldbe-list (list-ref alist 1)))
              (let ((result-list
                     (concatenated-product-list test-num)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : test-num=~a, "
                        sub-name test-label-index test-num))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a~%"
                        shouldbe-list result-list)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-list result-list)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop max-num debug-flag)
  (begin
    (let ((counter 0)
          (largest-list (list))
          (largest-generator 0)
          (largest-pandigital 0)
          (null-list (list)))
      (begin
        (do ((ii 1 (+ ii 1)))
            ((> ii max-num))
          (begin
            (let ((result-index-list
                   (concatenated-product-list ii)))
              (begin
                (if (and (list? result-index-list)
                         (> (length result-index-list) 0))
                    (begin
                      (let ((pandigital-num
                             (list-ref result-index-list 0))
                            (index-list
                             (list-ref result-index-list 1)))
                        (let ((index-length
                               (length index-list)))
                          (begin
                            (set! counter (+ counter 1))

                            (if (> pandigital-num largest-pandigital)
                                (begin
                                  (set! largest-list index-list)
                                  (set! largest-generator ii)
                                  (set! largest-pandigital pandigital-num)
                                  ))

                            (if (equal? debug-flag #t)
                                (begin
                                  (display
                                   (ice-9-format:format
                                    #f "(~:d)  pandigital number ~:d~%"
                                    counter pandigital-num))
                                  (for-each
                                   (lambda (this-elem)
                                     (begin
                                       (display
                                        (ice-9-format:format
                                         #f "    ~:d x ~:d = ~:d~%"
                                         ii this-elem
                                         (* ii this-elem)))
                                       )) index-list)
                                  (force-output)
                                  ))
                            )))
                      ))
                ))
            ))

        (display
         (ice-9-format:format
          #f "~:d is the largest 1 to 9 pandigital~%"
          largest-pandigital))
        (display
         (ice-9-format:format
          #f "number that can be formed as the~%"))
        (display
         (ice-9-format:format
          #f "concatenated product ~:d with a~%"
          largest-generator))
        (display
         (ice-9-format:format
          #f "consecutive integer series, where n = ~:d > 1~%"
          (length largest-list)))

        (display
         (ice-9-format:format
          #f "pandigital number ~:d : (total found = ~:d)~%"
          largest-pandigital counter))

        (for-each
         (lambda (this-elem)
           (begin
             (display
              (ice-9-format:format
               #f "    ~:d x ~:d = ~:d~%"
               largest-generator this-elem
               (* largest-generator this-elem)))
             )) largest-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Take the number 192 and multiply it "))
    (display
     (format #f "by each of 1, 2,~%"))
    (display
     (format #f "and 3:~%"))
    (newline)
    (display
     (format #f "192 x 1 = 192~%"))
    (display
     (format #f "192 x 2 = 384~%"))
    (display
     (format #f "192 x 3 = 576~%"))
    (newline)
    (display
     (format #f "By concatenating each product we get "))
    (display
     (format #f "the 1 to 9 pandigital,~%"))
    (display
     (format #f "192384576. We will call 192384576 the "))
    (display
     (format #f "concatenated product~%"))
    (display
     (format #f "of 192 and (1,2,3)~%"))
    (newline)
    (display
     (format #f "The same can be achieved by starting with "))
    (display
     (format #f "9 and multiplying by~%"))
    (display
     (format #f "1, 2, 3, 4, and 5, giving the pandigital, "))
    (display
     (format #f "918273645, which is~%"))
    (display
     (format #f "the concatenated product of 9 and "))
    (display
     (format #f "(1,2,3,4,5).~%"))
    (newline)
    (display
     (format #f "What is the largest 1 to 9 pandigital "))
    (display
     (format #f "9-digit number that~%"))
    (display
     (format #f "can be formed as the concatenated "))
    (display
     (format #f "product of an integer~%"))
    (display
     (format #f "with (1,2, ... , n) where n >= 1?~%"))
    (display
     (format #f "see https://projecteuler.net/problem=38~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((max-num 100)
          (debug-flag #t))
      (begin
        (sub-main-loop max-num debug-flag)
        ))

    (newline)
    (let ((max-num 100000)
          (debug-flag #t))
      (begin
        (sub-main-loop max-num debug-flag)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 38 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (load "digits-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

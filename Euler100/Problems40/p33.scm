#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 33                                     ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated June 12, 2022                                ###
;;;###                                                       ###
;;;###  updated March 4, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### digits-module for split-digits-list function
(use-modules ((digits-module)
              :renamer (symbol-prefix-proc 'digits-module:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (is-2d-cancellable-nt? numerator denominator)
  (begin
    (let ((nlist
           (digits-module:split-digits-list numerator))
          (dlist
           (digits-module:split-digits-list denominator))
          (ratio1 (/ numerator denominator)))
      (let ((ok-flag #t))
        (let ((nlen (length nlist))
              (dlen (length dlist)))
          (begin
            (if (and (= nlen 2) (= dlen 2))
                (begin
                  ;;; is there a common digit
                  (let ((n1 (list-ref nlist 0))
                        (n2 (list-ref nlist 1)))
                    (begin
                      (cond
                       ((not (equal? (member 0 nlist) #f))
                        (begin
                          #f
                          ))
                       ((not (equal? (member 0 dlist) #f))
                        (begin
                          #f
                          ))
                       ((not (equal? (member n1 dlist) #f))
                        (begin
                          (let ((reduced-list
                                 (delq1! n1 (list-copy dlist))))
                            (let ((d2 (list-ref reduced-list 0)))
                              (let ((ratio2 (/ n2 d2)))
                                (begin
                                  (if (equal? ratio1 ratio2)
                                      (begin
                                        #t)
                                      (begin
                                        #f
                                        ))
                                  ))
                              ))
                          ))
                       ((not (equal? (member n2 dlist) #f))
                        (begin
                          (let ((reduced-list
                                 (delq1! n2 (list-copy dlist))))
                            (let ((d1 (list-ref reduced-list 0)))
                              (let ((ratio2 (/ n1 d1)))
                                (begin
                                  (if (equal? ratio1 ratio2)
                                      (begin
                                        #t)
                                      (begin
                                        #f
                                        ))
                                  ))
                              ))
                          ))
                       (else
                        (begin
                          #f
                          )))
                      )))
                (begin
                  #f
                  ))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-is-2d-cancellable-nt-1 result-hash-table)
 (begin
   (let ((sub-name "test-is-2d-cancellable-nt-1")
         (test-list
          (list
           (list 10 20 #f) (list 11 22 #f) (list 12 24 #f)
           (list 49 98 #t)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-num (list-ref alist 0))
                  (test-den (list-ref alist 1))
                  (shouldbe-bool (list-ref alist 2)))
              (let ((result-bool
                     (is-2d-cancellable-nt? test-num test-den)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : test-num=~a, test-den=~a, "
                        sub-name test-label-index test-num test-den))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a~%"
                        shouldbe-bool result-bool)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-bool result-bool)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (make-2d-cancellable-list numerator denominator)
  (begin
    (let ((nlist
           (digits-module:split-digits-list numerator))
          (dlist
           (digits-module:split-digits-list denominator))
          (ratio1 (/ numerator denominator))
          (null-list (list)))
      (let ((ok-flag #t))
        (let ((nlen (length nlist))
              (dlen (length dlist)))
          (begin
            (if (and (= nlen 2) (= dlen 2))
                (begin
                  ;;; is there a common digit
                  (let ((n1 (list-ref nlist 0))
                        (n2 (list-ref nlist 1)))
                    (begin
                      (cond
                       ((not (equal? (member 0 nlist) #f))
                        (begin
                          null-list
                          ))
                       ((not (equal? (member 0 dlist) #f))
                        (begin
                          null-list
                          ))
                       ((not (equal? (member n1 dlist) #f))
                        (begin
                          (let ((reduced-list
                                 (delq1! n1 (list-copy dlist))))
                            (let ((d2
                                   (list-ref reduced-list 0)))
                              (let ((ratio2 (/ n2 d2)))
                                (begin
                                  (if (equal? ratio1 ratio2)
                                      (begin
                                        (list n2 d2))
                                      (begin
                                        null-list
                                        ))
                                  ))
                              ))
                          ))
                       ((not (equal? (member n2 dlist) #f))
                        (begin
                          (let ((reduced-list
                                 (delq1! n2 (list-copy dlist))))
                            (let ((d1 (list-ref reduced-list 0)))
                              (let ((ratio2 (/ n1 d1)))
                                (begin
                                  (if (equal? ratio1 ratio2)
                                      (begin
                                        (list n1 d1))
                                      (begin
                                        null-list
                                        ))
                                  ))
                              ))
                          ))
                       (else
                        (begin
                          null-list
                          )))
                      )))
                (begin
                  null-list
                  ))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-make-2d-cancellable-list-1 result-hash-table)
 (begin
   (let ((sub-name "test-make-2d-cancellable-list-1")
         (test-list
          (list
           (list 10 20 (list))
           (list 11 22 (list))
           (list 12 24 (list))
           (list 49 98 (list 4 8))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-num (list-ref alist 0))
                  (test-den (list-ref alist 1))
                  (shouldbe-list (list-ref alist 2)))
              (let ((result-list
                     (make-2d-cancellable-list test-num test-den)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : test-num=~a, test-den=~a, "
                        sub-name test-label-index test-num test-den))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a~%"
                        shouldbe-list result-list)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-list result-list)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop max-num debug-flag)
  (begin
    (let ((prod 1)
          (counter 0))
      (begin
        (do ((ii 10 (+ ii 1)))
            ((> ii max-num))
          (begin
            (do ((jj (+ ii 1) (+ jj 1)))
                ((> jj max-num))
              (begin
                (if (is-2d-cancellable-nt? ii jj)
                    (begin
                      (let ((this-ratio (/ ii jj)))
                        (let ((this-num (numerator this-ratio))
                              (this-den (denominator this-ratio)))
                          (begin
                            (if (equal? debug-flag #t)
                                (begin
                                  (let ((rlist (make-2d-cancellable-list ii jj)))
                                    (let ((rnum (list-ref rlist 0))
                                          (rden (list-ref rlist 1)))
                                      (begin
                                        (display
                                         (ice-9-format:format
                                          #f "~:d/~:d = ~:d/~:d = ~:d/~:d~%"
                                          ii jj rnum rden this-num this-den))
                                        )))
                                  ))
                            (set! prod (* prod this-ratio))
                            (set! counter (1+ counter))
                            )))
                      ))
                ))
            ))

        (display
         (ice-9-format:format
          #f "the product of the ~:d fractions, common denominator = ~a~%"
          counter (denominator prod)))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "The fraction 49/98 is a curious "))
    (display
     (format #f "fraction, as an~%"))
    (display
     (format #f "inexperienced mathematician in "))
    (display
     (format #f "attempting to simplify it~%"))
    (display
     (format #f "may incorrectly believe that 49/98 = 4/8, "))
    (display
     (format #f "which is correct, is~%"))
    (display
     (format #f "obtained by cancelling the 9s.~%"))
    (newline)
    (display
     (format #f "We shall consider fractions like, "))
    (display
     (format #f "30/50 = 3/5,~%"))
    (display
     (format #f "to be trivial examples.~%"))
    (display
     (format #f "There are exactly four non-trivial "))
    (display
     (format #f "examples of this type~%"))
    (display
     (format #f "of fraction, less than one in value, "))
    (display
     (format #f "and containing two~%"))
    (display
     (format #f "digits in the numerator and "))
    (display
     (format #f "denominator.~%"))
    (newline)
    (display
     (format #f "If the product of these four fractions "))
    (display
     (format #f "is given in its~%"))
    (display
     (format #f "lowest common terms, find the value "))
    (display
     (format #f "of the denominator.~%"))
    (display
     (format #f "see https://projecteuler.net/problem=33~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((max-num 10)
          (debug-flag #t))
      (begin
        (sub-main-loop max-num debug-flag)
        ))

    (let ((max-num 100)
          (debug-flag #t))
      (begin
        (sub-main-loop max-num debug-flag)
        (newline)
        ))


    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 33 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (load "digits-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

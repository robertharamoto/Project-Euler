#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 39                                     ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated June 12, 2022                                ###
;;;###                                                       ###
;;;###  updated March 6, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 receive - receive multiple values
(use-modules ((ice-9 receive)
              :renamer (symbol-prefix-proc 'ice-9-receive:)))

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (calc-triangular-list aa bb)
  (begin
    (let ((cc 0)
          (rr 0))
      (begin
        (ice-9-receive:receive
         (cc rr)
         (exact-integer-sqrt (+ (* aa aa) (* bb bb)))
         (begin
           (if (zero? rr)
               (begin
                 (list cc rr (+ aa bb cc)))
               (begin
                 (list cc rr 0)
                 ))
           ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-calc-triangular-list-1 result-hash-table)
 (begin
   (let ((sub-name "test-calc-triangular-list-1")
         (test-list
          (list
           (list 3 4 (list 5 0 12))
           (list 3 5 (list 5 9 0))
           (list 4 5 (list 6 5 0))
           (list 20 48 (list 52 0 120))
           (list 24 45 (list 51 0 120))
           (list 30 40 (list 50 0 120))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-aa (list-ref alist 0))
                  (test-bb (list-ref alist 1))
                  (shouldbe-list (list-ref alist 2)))
              (let ((result-list
                     (calc-triangular-list test-aa test-bb)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "test-aa=~a, test-bb=~a, "
                        test-aa test-bb))
                      (err-3
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe-list result-list)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-list result-list)
                     sub-name
                     (string-append
                      err-1 err-2 err-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (list-to-set-string llist)
  (begin
    (let ((stmp
           (string-join
            (map
             (lambda (num)
               (begin
                 (ice-9-format:format #f "~:d" num)
                 )) llist)
            ", ")))
      (let ((stmp2
             (string-append "{ " stmp " }")))
        (begin
          stmp2
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-list-to-set-string-1 result-hash-table)
 (begin
   (let ((sub-name "test-list-to-set-string-1")
         (test-list
          (list
           (list (list 1) "{ 1 }")
           (list (list 1 2) "{ 1, 2 }")
           (list (list 1 2 3) "{ 1, 2, 3 }")
           (list (list 4 5 6 7) "{ 4, 5, 6, 7 }")
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((input-list (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result
                     (list-to-set-string input-list)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : input-list=~a, "
                        sub-name test-label-index input-list))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a~%"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop max-num max-perimeter debug-flag)
  (begin
    (let ((counter 0)
          (perimeter-htable (make-hash-table 1000)))
      (begin
        (do ((aa 1 (+ aa 1)))
            ((>= aa max-num))
          (begin
            (do ((bb aa (+ bb 1)))
                ((> bb max-num))
              (begin
                (let ((result-list
                       (calc-triangular-list aa bb)))
                  (let ((cc (list-ref result-list 0))
                        (rr (list-ref result-list 1))
                        (perim (list-ref result-list 2)))
                    (begin
                      (if (zero? rr)
                          (begin
                            ;;; have a triangular triple, (aa, bb, cc)
                            (let ((tlist
                                   (hash-ref perimeter-htable perim #f))
                                  (current-list
                                   (list aa bb cc perim)))
                              (begin
                                (if (equal? tlist #f)
                                    (begin
                                      (hash-set!
                                       perimeter-htable
                                       perim (list current-list)))
                                    (begin
                                      (let ((next-list
                                             (append
                                              tlist (list current-list))))
                                        (begin
                                          (hash-set!
                                           perimeter-htable
                                           perim next-list)))
                                      ))
                                ))

                            (if (equal? debug-flag #t)
                                (begin
                                  (display
                                   (ice-9-format:format
                                    #f "  perimeter = ~:d : "
                                    perim))
                                  (display
                                   (ice-9-format:format
                                    #f "list = ~a~%"
                                    result-list))
                                  (force-output)
                                  ))

                            (set! counter (1+ counter))
                            ))
                      )))
                ))
            ))

        (let ((hkey-list
               (sort
                (hash-map->list
                 (lambda (key value)
                   (begin
                     key
                     )) perimeter-htable)
                <))
              (largest-length 0)
              (largest-list (list))
              (largest-perimeter 0))
          (begin
            (for-each
             (lambda (this-key)
               (begin
                 (let ((this-list-list
                        (hash-ref
                         perimeter-htable this-key #f)))
                   (begin
                     (if (and
                          (not (equal? this-list-list #f))
                          (<= this-key max-perimeter))
                         (begin
                           (let ((num-ways
                                  (length this-list-list)))
                             (begin
                               (if (> num-ways largest-length)
                                   (begin
                                     (set! largest-length num-ways)
                                     (set! largest-list this-list-list)
                                     (set! largest-perimeter this-key)
                                     ))
                               ))
                           ))
                     ))
                 )) hkey-list)

            (display
             (ice-9-format:format
              #f "perimeter = ~:d has the largest number of "
              largest-perimeter))
            (display
             (ice-9-format:format
              #f "solutions (~:d), for a~%"
              largest-length))
            (display
             (ice-9-format:format
              #f "right triangle, with perimeter <= ~:d, "
              max-perimeter))
            (display
             (ice-9-format:format
              #f "and a, b < ~:d~%"
              max-num))

            (let ((this-string ""))
              (begin
                (for-each
                 (lambda (this-list)
                   (begin
                     (let ((set-string
                            (list-to-set-string
                             (list-head this-list 3))))
                       (begin
                         (if (string-ci=? this-string "")
                             (begin
                               (set!
                                this-string
                                (format #f "~a" set-string)))
                             (begin
                               (set!
                                this-string
                                (format
                                 #f "~a, ~a"
                                 this-string set-string))
                               ))
                         ))
                     )) largest-list)

                (display
                 (ice-9-format:format
                  #f "  ~:d : number of solutions = ~:d : ~a~%"
                  largest-perimeter largest-length
                  this-string))
                ))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "If p is the perimeter of a right "))
    (display
     (format #f "angle triangle with~%"))
    (display
     (format #f "integral length sides, {a,b,c}, there "))
    (display
     (format #f "are exactly three~%"))
    (display
     (format #f "solutions for p = 120.~%"))
    (newline)
    (display
     (format #f "{20,48,52}, {24,45,51}, {30,40,50}~%"))
    (newline)
    (display
     (format #f "For which value of p <= 1000, is "))
    (display
     (format #f "the number of solutions~%"))
    (display
     (format #f "maximised?~%"))
    (display
     (format #f "see https://projecteuler.net/problem=39~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((max-num 10)
          (max-perimeter 100)
          (debug-flag #t))
      (begin
        (sub-main-loop
         max-num max-perimeter debug-flag)
        ))

    (newline)
    (let ((max-num 1000)
          (max-perimeter 1000)
          (debug-flag #f))
      (begin
        (sub-main-loop
         max-num max-perimeter debug-flag)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 39 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

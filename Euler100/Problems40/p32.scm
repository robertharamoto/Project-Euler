#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 32                                     ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated June 12, 2022                                ###
;;;###                                                       ###
;;;###  updated March 4, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### srfi-1 for fold functions
(use-modules ((srfi srfi-1)
              :renamer (symbol-prefix-proc 'srfi-1:)))

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### digits-module for split-digits-list function
(use-modules ((digits-module)
              :renamer (symbol-prefix-proc 'digits-module:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (is-triple-pandigital? multiplicand multiplier product)
  (begin
    (let ((m1-list
           (digits-module:split-digits-list multiplicand))
          (m2-list
           (digits-module:split-digits-list multiplier))
          (p-list
           (digits-module:split-digits-list product)))
      (let ((all-list (append m1-list m2-list p-list))
            (max-digit 9)
            (digit-hash (make-hash-table 10))
            (ok-flag #t))
        (let ((llen (length all-list)))
          (begin
            (if (not (= llen max-digit))
                (begin
                  #f)
                (begin
                  (hash-clear! digit-hash)

                  (do ((ii 0 (1+ ii)))
                      ((>= ii max-digit))
                    (begin
                      (let ((this-digit
                             (list-ref all-list ii)))
                        (let ((this-count
                               (hash-ref digit-hash this-digit 0)))
                          (begin
                            (if (>= this-count 1)
                                (begin
                                  (set! ok-flag #f)
                                  ))

                            (hash-set!
                             digit-hash this-digit (1+ this-count))
                            )))
                      ))

                  (if (equal? ok-flag #t)
                      (begin
                        (do ((ii 1 (1+ ii)))
                            ((or (> ii max-digit)
                                 (equal? ok-flag #f)))
                          (begin
                            (let ((this-digit
                                   (hash-ref digit-hash ii #f)))
                              (begin
                                (if (or (equal? this-digit #f)
                                        (> this-digit 1)
                                        (< this-digit 1))
                                    (begin
                                      (set! ok-flag #f))
                                    )))
                            ))
                        ))

                  ok-flag
                  ))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-is-triple-pandigital-1 result-hash-table)
 (begin
   (let ((sub-name "test-is-triple-pandigital-1")
         (test-list
          (list
           (list 3 5 15 #f)
           (list 12 483 5796 #t)
           (list 39 186 7254 #t)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-num1 (list-ref alist 0))
                  (test-num2 (list-ref alist 1))
                  (test-prod (list-ref alist 2))
                  (shouldbe-bool (list-ref alist 3)))
              (let ((result-bool
                     (is-triple-pandigital?
                      test-num1 test-num2 test-prod)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : test-num1=~a, test-num2=~a, test-prod=~a, "
                        sub-name test-label-index
                        test-num1 test-num2 test-prod))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe-bool result-bool)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-bool result-bool)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (is-trip-list-pandigital? m1-list m2-list p-list)
  (begin
    (let ((all-list (append m1-list m2-list p-list))
          (max-digit 9)
          (ok-flag #t))
      (let ((llen (length all-list)))
        (begin
          (if (not (= llen max-digit))
              (begin
                #f)
              (begin
                (do ((ii 1 (1+ ii)))
                    ((or (> ii max-digit)
                         (equal? ok-flag #f)))
                  (begin
                    (let ((bflag (member ii all-list)))
                      (begin
                        (if (equal? bflag #f)
                            (begin
                              (set! ok-flag #f)
                              ))
                        ))
                    ))

                ok-flag
                ))
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-is-trip-list-pandigital-1 result-hash-table)
 (begin
   (let ((sub-name "test-is-trip-list-pandigital-1")
         (test-list
          (list
           (list (list 3) (list 5) (list 1 5) #f)
           (list (list 1 2) (list 4 8 3) (list 5 7 9 6) #t)
           (list (list 3 9) (list 1 8 6) (list 7 2 5 4) #t)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((mlist1 (list-ref alist 0))
                  (mlist2 (list-ref alist 1))
                  (plist (list-ref alist 2))
                  (shouldbe-bool (list-ref alist 3)))
              (let ((result-bool
                     (is-trip-list-pandigital?
                      mlist1 mlist2 plist)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : mlist1=~a, mlist2=~a, plist=~a, "
                        sub-name test-label-index
                        mlist1 mlist2 plist))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe-bool result-bool)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-bool result-bool)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (estimate-start ii)
  (begin
    (cond
     ((< ii 10)
      (begin
        1000
        ))
     ((< ii 100)
      (begin
        100
        ))
     (else
      (begin
        (1+ ii)
        )))
    ))

;;;#############################################################
;;;#############################################################
(define (test-estimate-start-1)
  (begin
    (let ((sub-name "test-estimate-start-1")
          (test-list
           (list
            (list 3 1000)
            (list 5 1000)
            (list 99 100)
            (list 101 102)
            (list 1000 1001)
            ))
          (test-label-index 0))
      (begin
        (for-each
         (lambda (alist)
           (begin
             (let ((ii (list-ref alist 0))
                   (shouldbe (list-ref alist 1)))
               (let ((result (estimate-start ii)))
                 (let ((err-1
                        (format
                         #f "~a :: (~a) error : num=~a, "
                         sub-name test-label-index ii))
                       (err-2
                        (format
                         #f "shouldbe=~a, result=~a~%"
                         shouldbe result)))
                   (begin
                     (unittest2:assert?
                      (equal? shouldbe result)
                      sub-name
                      (string-append err-1 err-2)
                      result-hash-table)
                     ))
                 ))
             (set! test-label-index (1+ test-label-index))
             )) test-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (estimate-max-jj ii max-num)
  (begin
    (cond
     ((< ii 10)
      (begin
        (min 9999 max-num)
        ))
     (else
      (begin
        (min 999 max-num)
        )))
    ))

;;;#############################################################
;;;#############################################################
;;; define a macro to simplify code
(define-syntax process-inner-loop-macro
  (syntax-rules ()
    ((process-inner-loop-macro
      ii ii-list jj jj-list
      sum result-htable)
     (begin
       (let ((jj-list
              (digits-module:split-digits-list jj))
             (product (* ii jj)))
         (let ((pp-list
                (digits-module:split-digits-list product))
               (ok-ii-jj-flag #t)
               (ok-ii-pp-flag #t)
               (ok-jj-pp-flag #t))
           (begin
             (for-each
              (lambda (ii-elem)
                (begin
                  (if (not (equal? (member ii-elem jj-list) #f))
                      (begin
                        (set! ok-ii-jj-flag #f)
                        ))
                  (if (not (equal? (member ii-elem pp-list) #f))
                      (begin
                        (set! ok-ii-pp-flag #f)
                        ))
                  )) ii-list)
             (if (and ok-ii-jj-flag
                      ok-ii-pp-flag)
                 (begin
                   (for-each
                    (lambda (jj-elem)
                      (begin
                        (if (not (equal? (member jj-elem pp-list) #f))
                            (begin
                              (set! ok-jj-pp-flag #f)
                              ))
                        (if (= jj-elem 0)
                            (begin
                              (set! ok-jj-pp-flag #f)
                              ))
                        )) jj-list)
                   (if ok-jj-pp-flag
                       (begin
                         (for-each
                          (lambda (pp-elem)
                            (begin
                              (if (= pp-elem 0)
                                  (begin
                                    (set! ok-jj-pp-flag #f)
                                    ))
                              )) pp-list)

                         (if (and
                              (equal? ok-jj-pp-flag #t)
                              (is-trip-list-pandigital?
                               ii-list jj-list pp-list))
                             (begin
                               (let ((this-list
                                      (hash-ref
                                       result-htable product #f)))
                                 (begin
                                   (if (equal? this-list #f)
                                       (begin
                                         (set! sum (+ sum product))
                                         (hash-set!
                                          result-htable
                                          product (list ii jj product))
                                         ))
                                   ))
                               ))
                         ))
                   ))
             )))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop max-num debug-flag)
  (begin
    (let ((sum 0)
          (max-ii (min 999 max-num))
          (digit-htable (make-hash-table 10))
          (result-htable (make-hash-table 100)))
      (begin
        (do ((ii 1 (1+ ii)))
            ((> ii max-ii))
          (begin
            (let ((ii-list
                   (digits-module:split-digits-list ii))
                  (jstart (estimate-start ii)))
              (let ((ii2-list (srfi-1:delete-duplicates ii-list))
                    (ii-flag (member 0 ii-list))
                    (max-jj (estimate-max-jj ii max-num)))
                (begin
                  (if (and (equal? ii-list ii2-list)
                           (equal? ii-flag #f))
                      (begin
                        (do ((jj jstart (1+ jj)))
                            ((> jj max-jj))
                          (begin
                            (process-inner-loop-macro
                             ii ii-list jj jj-list
                             sum result-htable)
                            ))
                        ))
                  )))
            ))

        (if (equal? debug-flag #t)
            (begin
              (let ((key-list
                     (sort
                      (hash-map->list
                       (lambda (key value)
                         (begin
                           key
                           )) result-htable) <))
                    (local-sum 0)
                    (local-sum-list (list)))
                (begin
                  (for-each
                   (lambda (key)
                     (begin
                       (let ((vlist (hash-ref result-htable key (list))))
                         (begin
                           (set! local-sum (+ local-sum key))
                           (set! local-sum-list
                                 (append local-sum-list (list vlist)))

                           (let ((mult1 (list-ref vlist 0))
                                 (mult2 (list-ref vlist 1))
                                 (prod (list-ref vlist 2)))
                             (begin
                               (display
                                (ice-9-format:format
                                 #f "    ~:d x ~:d = ~:d~%"
                                 mult1 mult2 prod))
                               ))
                           ))
                       )) key-list)
                  ))
              ))

        (display
         (ice-9-format:format
          #f "sum of all products = ~:d~%"
          sum))
        (display
         (ice-9-format:format
          #f "(where the mulitpliers are less than ~:d)~%"
          max-num))
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "We shall say that an n-digit number "))
    (display
     (format #f "is pandigital if~%"))
    (display
     (format #f "it makes use of all the digits 1 to n "))
    (display
     (format #f "exactly once; for~%"))
    (display
     (format #f "example, the 5-digit number, 15234, "))
    (display
     (format #f "is 1 through 5~%"))
    (display
     (format #f "pandigital.~%"))
    (newline)
    (display
     (format #f "The product 7254 is unusual, as the "))
    (display
     (format #f "identity,~%"))
    (display
     (format #f "39 x 186 = 7254, containing multiplicand, "))
    (display
     (format #f "multiplier, and~%"))
    (display
     (format #f "product is 1 through 9 pandigital. "))
    (display
     (format #f "Find the sum of~%"))
    (display
     (format #f "all products whose~%"))
    (display
     (format #f "multiplicand/multiplier/product identity "))
    (display
     (format #f "can be written~%"))
    (display
     (format #f "as a 1 through 9 pandigital.~%"))
    (newline)
    (display
     (format #f "HINT: Some products can be obtained in "))
    (display
     (format #f "more than one way~%"))
    (display
     (format #f "so be sure to only include it once "))
    (display
     (format #f "in your sum.~%"))
    (newline)
    (display
     (format #f "Uses the fact that the number of "))
    (display
     (format #f "digits of the first~%"))
    (display
     (format #f "number multiplied by the second, "))
    (display
     (format #f "plus the product digits~%"))
    (display
     (format #f "should be equal to 9. If the "))
    (display
     (format #f "first multiplier < second~%"))
    (display
     (format #f "then the first multiplier can at "))
    (display
     (format #f "most be 3-digits long,~%"))
    (display
     (format #f "and the product must be 3-digits "))
    (display
     (format #f "long as well.~%"))
    (display
     (format #f "see https://projecteuler.net/problem=32~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((max-num 1000)
          (debug-flag #t))
      (begin
        (sub-main-loop max-num debug-flag)
        ))

    (newline)
    (let ((max-num 10000)
          (debug-flag #t))
      (begin
        (sub-main-loop max-num debug-flag)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 32 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (load "digits-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

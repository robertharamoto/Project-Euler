#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 84                                     ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated July 26, 2022                                ###
;;;###                                                       ###
;;;###  updated March 8, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### srfi-1 for fold function
(use-modules ((srfi srfi-1)
              :renamer (symbol-prefix-proc 'srfi-1:)))

;;;### ice-9 copy-tree for copy-tree
(use-modules ((ice-9 copy-tree)
              :renamer (symbol-prefix-proc 'ice-9-copy-tree:)))

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (initialize-random-state)
  (begin
    (let ((time (gettimeofday)))
      (begin
        (set!
         *random-state*
         (seed->random-state
          (+ (car time)
             (cdr time))))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (roll-two-dice max-num)
  (begin
    (let ((dnum1
           (inexact->exact
            (truncate (+ 1.0 (random max-num)))))
          (dnum2
           (inexact->exact
            (truncate (+ 1.0 (random max-num))))))
      (let ((sum (+ dnum1 dnum2)))
        (begin
          (if (= dnum1 dnum2)
              (begin
                (list sum #t))
              (begin
                (list sum #f)
                ))
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (display-random-two-dice-tests max-rolls)
  (begin
    (let ((max-dice-num 6)
          (results-htable (make-hash-table 20)))
      (begin
        (do ((ii 0 (1+ ii)))
            ((> ii max-rolls))
          (begin
            (let ((roll-list
                   (roll-two-dice max-dice-num)))
              (let ((this-roll
                     (car roll-list)))
                (let ((this-count
                       (hash-ref results-htable this-roll 0)))
                  (begin
                    (hash-set!
                     results-htable this-roll (1+ this-count))
                    ))
                ))
            ))
        (hash-for-each
         (lambda(key value)
           (begin
             (display
              (ice-9-format:format
               #f "(~a) roll = ~:d~%" key value))
             )) results-htable)
        ))
    ))

;;;#############################################################
;;;#############################################################
;;; data-list format
;;; board format (list place-number place-code count total pcnt-double)
;;; checked - 12/23/2011, 12/28/2011, 3/11/2012
(define (initialize-list-data)
  (begin
    (let ((code-list
           (list
            (list 0 "go" 0 0 0.0) (list 1 "a1" 0 0 0.0)
            (list 2 "cc1" 0 0 0.0) (list 3 "a2" 0 0 0.0)
            (list 4 "t1" 0 0 0.0) (list 5  "r1"   0 0 0.0)
            (list 6 "b1"   0 0 0.0) (list 7 "ch1"   0 0 0.0)
            (list 8 "b2"   0 0 0.0) (list 9 "b3"   0 0 0.0)
            (list 10 "jail"  0 0 0.0) (list 11 "c1"   0 0 0.0)
            (list 12 "u1"   0 0 0.0) (list 13 "c2"   0 0 0.0)
            (list 14 "c3"   0 0 0.0) (list 15 "r2"   0 0 0.0)
            (list 16 "d1"   0 0 0.0) (list 17 "cc2"   0 0 0.0)
            (list 18 "d2"   0 0 0.0) (list 19 "d3"   0 0 0.0)
            (list 20 "fp"   0 0 0.0) (list 21 "e1"   0 0 0.0)
            (list 22 "ch2"  0 0 0.0) (list 23 "e2"   0 0 0.0)
            (list 24 "e3"   0 0 0.0) (list 25 "r3"   0 0 0.0)
            (list 26 "f1"   0 0 0.0) (list 27 "f2"   0 0 0.0)
            (list 28 "u2"   0 0 0.0) (list 29 "f3"   0 0 0.0)
            (list 30 "g2j"  0 0 0.0) (list 31 "g1"   0 0 0.0)
            (list 32 "g2"   0 0 0.0) (list 33 "cc3"  0 0 0.0)
            (list 34 "g3"   0 0 0.0) (list 35 "r4"   0 0 0.0)
            (list 36 "ch3"  0 0 0.0) (list 37 "h1"   0 0 0.0)
            (list 38 "t2"   0 0 0.0) (list 39 "h2"   0 0 0.0)
            )))
      (begin
        code-list
        ))
    ))

;;;#############################################################
;;;#############################################################
;;; cc-deck format (list op-code go-to-destination code-name)
(define (initialize-community-chest-deck)
  (begin
    (let ((cc-list
           (list
            (list 1 0 "go")
            (list 2 10 "jail")
            (list 3 -1 "nop")
            (list 4 -1 "nop")
            (list 5 -1 "nop")
            (list 6 -1 "nop")
            (list 7 -1 "nop")
            (list 8 -1 "nop")
            (list 9 -1 "nop")
            (list 10 -1 "nop")
            (list 11 -1 "nop")
            (list 12 -1 "nop")
            (list 13 -1 "nop")
            (list 14 -1 "nop")
            (list 15 -1 "nop")
            (list 16 -1 "nop"))
           ))
      (let ((nlist-items (length cc-list)))
        (let ((remaining-list
               (ice-9-copy-tree:copy-tree cc-list))
              (remaining-items nlist-items)
              (final-list (list)))
          (begin
            (do ((ii 0 (1+ ii)))
                ((>= ii nlist-items))
              (begin
                (if (> remaining-items 1)
                    (begin
                      (let ((this-index
                             (inexact->exact
                              (truncate (random remaining-items)))))
                        (let ((this-item
                               (list-ref remaining-list this-index)))
                          (begin
                            (set!
                             final-list (cons this-item final-list))
                            (set!
                             remaining-list
                             (delete1! this-item remaining-list))
                            (set!
                             remaining-items (1- remaining-items))
                            ))))
                    (begin
                      (let ((this-item (car remaining-list)))
                        (begin
                          (set!
                           final-list (cons this-item final-list))
                          (delete1!
                           this-item remaining-list)
                          (set!
                           remaining-items (1- remaining-items))
                          ))
                      ))
                ))

            (reverse final-list)
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
;;; cc-deck format (list op-code go-to-destination code-name)
(define (initialize-chance-deck)
  (begin
    (let ((move-list
           (list
            (list 1 0 "go")
            (list 2 10 "jail")
            (list 3 11 "c1")
            (list 4 24 "e3")
            (list 5 39 "h2")
            (list 6 5 "r1")
            (list 7 -1 "r")
            (list 8 -1 "r")
            (list 9 -1 "u")
            (list 10 -3 "back 3")
            (list 11 -1 "nop")
            (list 12 -1 "nop")
            (list 13 -1 "nop")
            (list 14 -1 "nop")
            (list 15 -1 "nop")
            (list 16 -1 "nop")
            )))
      (let ((nlist-items (length move-list)))
        (let ((remaining-list
               (ice-9-copy-tree:copy-tree move-list))
              (remaining-items nlist-items)
              (final-list (list)))
          (begin
            (do ((ii 0 (1+ ii)))
                ((>= ii nlist-items))
              (begin
                (if (> remaining-items 1)
                    (begin
                      (let ((this-index
                             (inexact->exact
                              (truncate (random remaining-items)))))
                        (let ((this-item (list-ref remaining-list this-index)))
                          (begin
                            (set! final-list (cons this-item final-list))
                            (set! remaining-list (delete1! this-item remaining-list))
                            (set! remaining-items (1- remaining-items))
                            ))))
                    (begin
                      (let ((this-item (car remaining-list)))
                        (begin
                          (set! final-list (cons this-item final-list))
                          (delete1! this-item remaining-list)
                          (set! remaining-items (1- remaining-items))
                          ))
                      ))
                ))

            (reverse final-list)
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
;;; define a macro to simplify simulation loop
;;; board format (list place-number place-code count total pcnt-double)
(define-syntax update-position-data-macro
  (syntax-rules ()
    ((update-position-data-macro
      board-list max-length new-position)
     (begin
       (let ((this-elem (list-ref board-list new-position)))
         (let ((next-count (1+ (list-ref this-elem 2))))
           (begin
             (list-set! this-elem 2 next-count)
             (list-set! board-list new-position this-elem)
             )))
       ))
    ))

;;;#############################################################
;;;#############################################################
;;; (list place-number place-code count total pcnt-double)
(unittest2:define-tests-macro
 (test-update-position-data-1 result-hash-table)
 (begin
   (let ((sub-name "test-update-position-data-1")
         (test-list
          (list
           (list
            (list (list 0 "go" 0 0 0.0) (list 1 "a1" 0 0 0.0) (list 2 "a2" 0 0 0.0))
            0
            (list (list 0 "go" 1 0 0.0) (list 1 "a1" 0 0 0.0) (list 2 "a2" 0 0 0.0)))
           (list
            (list (list 0 "go" 0 0 0.0) (list 1 "a1" 0 0 0.0) (list 2 "a2" 0 0 0.0))
            1
            (list (list 0 "go" 0 0 0.0) (list 1 "a1" 1 0 0.0) (list 2 "a2" 0 0 0.0)))
           (list
            (list (list 0 "go" 0 0 0.0) (list 1 "a1" 0 0 0.0) (list 2 "a2" 0 0 0.0))
            2
            (list (list 0 "go" 0 0 0.0) (list 1 "a1" 0 0 0.0) (list 2 "a2" 1 0 0.0)))
           (list
            (list (list 0 "go" 1 0 0.0) (list 1 "a1" 0 0 0.0) (list 2 "a2" 0 0 0.0))
            0
            (list (list 0 "go" 2 0 0.0) (list 1 "a1" 0 0 0.0) (list 2 "a2" 0 0 0.0)))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (a-list)
          (begin
            (let ((board-list (list-ref a-list 0))
                  (new-position (list-ref a-list 1))
                  (shouldbe-list (list-ref a-list 2)))
              (let ((old-board-list
                     (ice-9-copy-tree:copy-tree board-list))
                    (board-length (length board-list)))
                (begin
                  (update-position-data-macro
                   board-list board-length new-position)

                  (let ((err-1
                         (format
                          #f "~a : error (~a) : old-board-list=~a, new-position=~a, "
                          sub-name test-label-index old-board-list new-position))
                        (err-2
                         (format
                          #f "shouldbe=~a, result=~a~%"
                          shouldbe-list board-list)))
                    (begin
                      (unittest2:assert?
                       (equal? shouldbe-list board-list)
                       sub-name
                       (string-append err-1 err-2)
                       result-hash-table)
                      ))
                  )))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; define a macro to simplify simulation loop
(define-syntax top-card-to-bottom-macro
  (syntax-rules ()
    ((top-card-to-bottom-macro card-list)
     (begin
       (let ((top-card (car card-list))
             (tail-list (cdr card-list)))
         (let ((next-list
                (append tail-list (list top-card))))
           (begin
             (set! card-list next-list)
             )))
       ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-top-card-to-bottom-1 result-hash-table)
 (begin
   (let ((sub-name "test-top-card-to-bottom-1")
         (test-list
          (list
           (list
            (list (list 15 -1 "nop") (list 1 0 "go") (list 2 10 "jail"))
            (list (list 1 0 "go") (list 2 10 "jail") (list 15 -1 "nop"))
            )))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (a-list)
          (begin
            (let ((test-deck (list-ref a-list 0))
                  (shouldbe-deck (list-ref a-list 1)))
              (let ((old-test-deck
                     (ice-9-copy-tree:copy-tree test-deck)))
                (begin
                  (top-card-to-bottom-macro test-deck)

                  (let ((err-1
                         (format
                          #f "~a : error (~a) : old-test-deck=~a, "
                          sub-name test-label-index old-test-deck))
                        (err-2
                         (format
                          #f "shouldbe=~a, result=~a~%"
                          shouldbe-deck test-deck)))
                    (begin
                      (unittest2:assert?
                       (equal? shouldbe-deck test-deck)
                       sub-name
                       (string-append err-1 err-2)
                       result-hash-table)
                      ))
                  )))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; board format (list place-number place-code count total pcnt-double)
;;; speed up the simulation by hard-wiring the goto-next r or u cards
;;; (r-pos-list (list 5 15 25 35))
;;; (u-pos-list (list 12 28)))
(define (find-next-position
         board-list board-length
         current-position code-string)
  (begin
    (let ((next-position -1))
      (begin
        (if (string-ci=? code-string "r")
            (begin
              (cond
               ((< current-position 5)
                (begin
                  (set! next-position 5)
                  ))
               ((< current-position 15)
                (begin
                  (set! next-position 15)
                  ))
               ((< current-position 25)
                (begin
                  (set! next-position 25)
                  ))
               ((< current-position 35)
                (begin
                  (set! next-position 35)
                  ))
               (else
                (begin
                  (set! next-position 5)
                  )))
              ))
        (if (string-ci=? code-string "u")
            (begin
              (cond
               ((< current-position 12)
                (begin
                  (set! next-position 12)
                  ))
               ((< current-position 28)
                (begin
                  (set! next-position 28)
                  ))
               (else
                (begin
                  (set! next-position 12)
                  )))
              ))
        next-position
        ))
    ))

;;;#############################################################
;;;#############################################################
;;; board format (list place-number place-code count total pcnt-double)
(unittest2:define-tests-macro
 (test-find-next-position-1 result-hash-table)
 (begin
   (let ((sub-name "test-find-next-position-1")
         (board-list (initialize-list-data))
         (test-list
          (list
           (list 0 "r" 5) (list 1 "r" 5) (list 2 "r" 5) (list 3 "r" 5)
           (list 4 "r" 5) (list 5 "r" 15) (list 16 "r" 25)
           (list 26 "r" 35) (list 36 "r" 5) (list 39 "r" 5)
           (list 1 "u" 12) (list 11 "u" 12) (list 12 "u" 28)
           (list 29 "u" 12)
           ))
         (test-label-index 0))
     (let ((board-length (length board-list)))
       (begin
         (for-each
          (lambda (a-list)
            (begin
              (let ((curr-pos (list-ref a-list 0))
                    (code-string (list-ref a-list 1))
                    (shouldbe-position (list-ref a-list 2)))
                (let ((result-position
                       (find-next-position
                        board-list board-length
                        curr-pos code-string)))
                  (begin
                    (let ((err-1
                           (format
                            #f "~a : error (~a) : curr-pos=~a, code-string=~a, "
                            sub-name test-label-index curr-pos code-string))
                          (err-2
                           (format
                            #f "shouldbe=~a, result=~a~%"
                            shouldbe-position result-position)))
                      (begin
                        (unittest2:assert?
                         (equal? shouldbe-position result-position)
                         sub-name
                         (string-append err-1 err-2)
                         result-hash-table)
                        ))
                    )))
              (set! test-label-index (1+ test-label-index))
              )) test-list)
         )))
   ))

;;;#############################################################
;;;#############################################################
;;; community-chest-deck format (list op-code go-to-destination code-name)
(define-syntax handle-community-chest-macro
  (syntax-rules ()
    ((handle-community-chest-macro
      board-list board-length
      position-counter next-position
      consecutive-doubles-count
      community-deck)
     (begin
       (let ((top-card (car community-deck)))
         (let ((dest (list-ref top-card 1))
               (code-name (list-ref top-card 2)))
           (begin
             (cond
              ((string-ci=? code-name "go")
               (begin
                 (set! position-counter dest)
                 (update-position-data-macro
                  board-list board-length position-counter)
                 ))
              ((string-ci=? code-name "jail")
               (begin
                 (set! consecutive-doubles-count 0)
                 (set! position-counter dest)
                 (update-position-data-macro
                  board-list board-length position-counter)
                 ))
              (else
               (begin
                 (set! position-counter next-position)
                 (update-position-data-macro
                  board-list board-length position-counter)
                 )))
             )))
       (top-card-to-bottom-macro community-deck)
       ))
    ))

;;;#############################################################
;;;#############################################################
;;; board-list format (list place-number place-code count total pcnt-double)
(unittest2:define-tests-macro
 (test-handle-community-chest-1 result-hash-table)
 (begin
   (let ((sub-name "test-handle-community-chest-1")
         (board-list (initialize-list-data))
         (test-list
          (list
           (list
            (list (list 1 0 "go") (list 2 10 "jail") (list 3 -1 "nop"))
            0 5 0
            0 0 (list 0 "go" 1 0 0.0))
           (list
            (list (list 1 0 "go") (list 2 10 "jail") (list 3 -1 "nop"))
            1 5 0
            0 0 (list 0 "go" 1 0 0.0))
           (list
            (list (list 2 10 "jail") (list 3 -1 "nop") (list 1 0 "go"))
            1 7 1
            10 0 (list 10 "jail" 1 0 0.0))
           (list
            (list (list 2 10 "jail") (list 3 -1 "nop") (list 1 0 "go"))
            12 18 1
            10 0 (list 10 "jail" 1 0 0.0))
           ))
         (test-label-index 0))
     (let ((board-length (length board-list)))
       (begin
         (for-each
          (lambda (a-list)
            (begin
              (let ((cc-deck (list-ref a-list 0))
                    (position-counter (list-ref a-list 1))
                    (next-position (list-ref a-list 2))
                    (consecutive-doubles-count (list-ref a-list 3))
                    (shouldbe-position (list-ref a-list 4))
                    (shouldbe-consecutive-doubles-count (list-ref a-list 5))
                    (shouldbe-list (list-ref a-list 6))
                    (this-board
                     (ice-9-copy-tree:copy-tree board-list)))
                (begin
                  (handle-community-chest-macro
                   this-board board-length
                   position-counter next-position
                   consecutive-doubles-count
                   cc-deck)

                  (let ((err-1
                         (format
                          #f "~a : error (~a) : cc-deck=~a : "
                          sub-name test-label-index cc-deck))
                        (err-2
                         (format
                          #f "shouldbe-position=~a, result position=~a, "
                          shouldbe-position position-counter))
                        (err-3
                         (format
                          #f "shouldbe-consecutive-doubles-count=~a, "
                          shouldbe-consecutive-doubles-count))
                        (err-4
                         (format
                          #f "result consecuritve-doubles-count=~a, "
                          consecutive-doubles-count)))
                    (begin
                      (unittest2:assert?
                       (equal? shouldbe-position position-counter)
                       sub-name
                       (string-append err-1 err-2)
                       result-hash-table)

                      (unittest2:assert?
                       (equal? shouldbe-consecutive-doubles-count
                               consecutive-doubles-count)
                       sub-name
                       (string-append
                        err-1 err-3 err-4)
                       result-hash-table)

                      (let ((result-list
                             (list-ref this-board shouldbe-position)))
                        (let ((err-5
                               (format
                                #f "shouldbe-list=~a, result-list=~a"
                                shouldbe-list result-list)))
                          (begin
                            (unittest2:assert?
                             (equal? shouldbe-list result-list)
                             sub-name
                             (string-append
                              err-1 err-5)
                             result-hash-table)
                            )))
                      ))
                  ))
              (set! test-label-index (1+ test-label-index))
              )) test-list)
         )))
   ))

;;;#############################################################
;;;#############################################################
;;; chance-deck format (list op-code go-to-destination code-name)
(define-syntax handle-chance-macro
  (syntax-rules ()
    ((handle-chance-macro
      board-list board-length
      position-counter next-position
      consecutive-doubles-count
      chance-deck cc-deck)
     (begin
       (let ((top-card (car chance-deck)))
         (let ((dest (list-ref top-card 1))
               (code-name (list-ref top-card 2)))
           (begin
             (cond
              ((string-ci=? code-name "go")
               (begin
                 (set! position-counter dest)
                 (update-position-data-macro
                  board-list board-length position-counter)
                 ))
              ((string-ci=? code-name "jail")
               (begin
                 (set! consecutive-doubles-count 0)
                 (set! position-counter dest)
                 (update-position-data-macro
                  board-list board-length position-counter)
                 ))
              ((or (string-ci=? code-name "c1")
                   (string-ci=? code-name "e3")
                   (string-ci=? code-name "h2")
                   (string-ci=? code-name "r1"))
               (begin
                 (set! position-counter dest)
                 (update-position-data-macro
                  board-list board-length position-counter)
                 ))
              ((or (string-ci=? code-name "r")
                   (string-ci=? code-name "u"))
               (begin
                 (let ((next-position-counter
                        (find-next-position
                         board-list board-length
                         position-counter code-name)))
                   (begin
                     (set! position-counter next-position-counter)
                     (update-position-data-macro
                      board-list board-length position-counter)
                     ))
                 ))
              ((string-ci=? code-name "back 3")
               (begin
                 (let ((next-position-counter (- next-position 3)))
                   (begin
                     (if (< next-position-counter 0)
                         (begin
                           (set!
                            next-position-counter
                            (+ next-position-counter board-length))
                           ))

                     (cond
                      ((= next-position-counter 30)
                       (begin
                         ;;; go to jail
                         (set! consecutive-doubles-count 0)
                         (set! next-position-counter 10)
                         ))
                      ((or (= next-position-counter 2)
                           (= next-position-counter 17)
                           (= next-position-counter 33))
                       (begin
                         ;;; chance deck says go back 3 spaces,
                         ;;; and we land on community chest
                         (handle-community-chest-macro
                          board-list board-length
                          position-counter next-position-counter
                          consecutive-doubles-count
                          cc-deck)
                         ))
                      (else
                       (begin
                         (set!
                          position-counter
                          next-position-counter)
                         (update-position-data-macro
                          board-list board-length position-counter)
                         )))
                     ))
                 ))
              (else
               (begin
                 (set! position-counter next-position)
                 (update-position-data-macro
                  board-list board-length position-counter)
                 )))

             (top-card-to-bottom-macro chance-deck)
             )))
       ))
    ))

;;;#############################################################
;;;#############################################################
;;; board format (list place-number place-code count total pcnt-double)
(unittest2:define-tests-macro
 (test-handle-chance-1 result-hash-table)
 (begin
   (let ((sub-name "test-handle-chance-1")
         (board-list (initialize-list-data))
         (test-list
          (list
            ;;; test 0
           (list
            (list (list 1 0 "go") (list 2 10 "jail") (list 3 -1 "nop"))
            (list (list 1 0 "go") (list 2 10 "jail") (list 3 -1 "nop"))
            0 5 0
            0 0 (list 0 "go" 1 0 0.0))
           (list
            (list (list 1 0 "go") (list 2 10 "jail") (list 3 -1 "nop"))
            (list (list 1 0 "go") (list 2 10 "jail") (list 3 -1 "nop"))
            1 12 0
            0 0 (list 0 "go" 1 0 0.0))
           (list
            (list (list 2 10 "jail") (list 3 -1 "nop") (list 1 0 "go"))
            (list (list 1 0 "go") (list 2 10 "jail") (list 3 -1 "nop"))
            1 12 1
            10 0 (list 10 "jail" 1 0 0.0))
           (list
            (list (list 2 10 "jail") (list 3 -1 "nop") (list 1 0 "go"))
            (list (list 1 0 "go") (list 2 10 "jail") (list 3 -1 "nop"))
            12 20 1
            10 0 (list 10 "jail" 1 0 0.0))
           (list
            (list (list 3 11 "c1") (list 7 -1 "nop") (list 1 0 "go"))
            (list (list 1 0 "go") (list 2 10 "jail") (list 3 -1 "nop"))
            1 10 0
            11 0 (list 11 "c1" 1 0 0.0))
            ;;; test 5
           (list
            (list (list 3 11 "c1") (list 3 -1 "nop") (list 1 0 "go"))
            (list (list 1 0 "go") (list 2 10 "jail") (list 3 -1 "nop"))
            12 15 0
            11 0 (list 11 "c1" 1 0 0.0))
           (list
            (list (list 4 24 "e3") (list 7 -1 "nop") (list 1 0 "go"))
            (list (list 1 0 "go") (list 2 10 "jail") (list 3 -1 "nop"))
            1 15 0
            24 0 (list 24 "e3" 1 0 0.0))
           (list
            (list (list 4 24 "e3") (list 3 -1 "nop") (list 1 0 "go"))
            (list (list 1 0 "go") (list 2 10 "jail") (list 3 -1 "nop"))
            25 35 0
            24 0 (list 24 "e3" 1 0 0.0))
           (list
            (list (list 5 39 "h2") (list 7 -1 "nop") (list 1 0 "go"))
            (list (list 1 0 "go") (list 2 10 "jail") (list 3 -1 "nop"))
            1 5 0
            39 0 (list 39 "h2" 1 0 0.0))
           (list
            (list (list 5 39 "h2") (list 3 -1 "nop") (list 1 0 "go"))
            (list (list 1 0 "go") (list 2 10 "jail") (list 3 -1 "nop"))
            39 5 0
            39 0 (list 39 "h2" 1 0 0.0))
            ;;; test 10
           (list
            (list (list 6 5 "r1") (list 7 -1 "nop") (list 1 0 "go"))
            (list (list 1 0 "go") (list 2 10 "jail") (list 3 -1 "nop"))
            1 6 0
            5 0 (list 5 "r1" 1 0 0.0))
           (list
            (list (list 6 5 "r1") (list 3 -1 "nop") (list 1 0 "go"))
            (list (list 1 0 "go") (list 2 10 "jail") (list 3 -1 "nop"))
            6 7 0
            5 0 (list 5 "r1" 1 0 0.0))
           (list
            (list (list 7 -1 "r") (list 7 -1 "nop") (list 1 0 "go"))
            (list (list 1 0 "go") (list 2 10 "jail") (list 3 -1 "nop"))
            1 8 0
            5 0 (list 5 "r1" 1 0 0.0))
           (list
            (list (list 7 -1 "r") (list 3 -1 "nop") (list 1 0 "go"))
            (list (list 1 0 "go") (list 2 10 "jail") (list 3 -1 "nop"))
            6 9 0
            15 0 (list 15 "r2" 1 0 0.0))
           (list
            (list (list 7 -1 "r") (list 3 -1 "nop") (list 1 0 "go"))
            (list (list 1 0 "go") (list 2 10 "jail") (list 3 -1 "nop"))
            16 10 0
            25 0 (list 25 "r3" 1 0 0.0))
            ;;; test 15
           (list
            (list (list 7 -1 "r") (list 3 -1 "nop") (list 1 0 "go"))
            (list (list 1 0 "go") (list 2 10 "jail") (list 3 -1 "nop"))
            26 31 0
            35 0 (list 35 "r4" 1 0 0.0))
           (list
            (list (list 8 -1 "r") (list 3 -1 "nop") (list 1 0 "go"))
            (list (list 1 0 "go") (list 2 10 "jail") (list 3 -1 "nop"))
            36 38 0
            5 0 (list 5 "r1" 1 0 0.0))
           (list
            (list (list 9 -1 "u") (list 3 -1 "nop") (list 1 0 "go"))
            (list (list 1 0 "go") (list 2 10 "jail") (list 3 -1 "nop"))
            1 39 0
            12 0 (list 12 "u1" 1 0 0.0))
           (list
            (list (list 9 -1 "u") (list 3 -1 "nop") (list 1 0 "go"))
            (list (list 1 0 "go") (list 2 10 "jail") (list 3 -1 "nop"))
            13 23 0
            28 0 (list 28 "u2" 1 0 0.0))
           (list
            (list (list 9 -1 "u") (list 3 -1 "nop") (list 1 0 "go"))
            (list (list 1 0 "go") (list 2 10 "jail") (list 3 -1 "nop"))
            29 33 0
            12 0 (list 12 "u1" 1 0 0.0))
            ;;; test 20
           (list
            (list (list 10 -3 "back 3") (list 3 -1 "nop") (list 1 0 "go"))
            (list (list 1 0 "go") (list 2 10 "jail") (list 3 -1 "nop"))
            5 7 0
            4 0 (list 4 "t1" 1 0 0.0))
           (list
            (list (list 10 -3 "back 3") (list 3 -1 "nop") (list 1 0 "go"))
            (list (list 1 0 "go") (list 2 10 "jail") (list 3 -1 "nop"))
            39 7 0
            4 0 (list 4 "t1" 1 0 0.0))
           ))
         (test-label-index 0))
     (let ((board-length (length board-list)))
       (begin
         (for-each
          (lambda (a-list)
            (begin
              (let ((chance-deck (list-ref a-list 0))
                    (cc-deck (list-ref a-list 1))
                    (position-counter (list-ref a-list 2))
                    (next-position (list-ref a-list 3))
                    (consecutive-doubles-count (list-ref a-list 4))
                    (shouldbe-position (list-ref a-list 5))
                    (shouldbe-consecutive-doubles-count (list-ref a-list 6))
                    (shouldbe-list (list-ref a-list 7))
                    (this-board
                     (ice-9-copy-tree:copy-tree board-list)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : chance-deck=~a, cc-deck=~a, "
                        sub-name test-label-index chance-deck cc-deck)))
                  (begin
                    (handle-chance-macro
                     this-board board-length
                     position-counter next-position
                     consecutive-doubles-count
                     chance-deck cc-deck)

                    (let ((err-2
                           (format
                            #f "shouldbe-position=~a, result=~a~%"
                            shouldbe-position position-counter)))
                      (begin
                        (unittest2:assert?
                         (equal? shouldbe-position position-counter)
                         sub-name
                         (string-append err-1 err-2)
                         result-hash-table)
                        ))

                    (let ((err-3
                           (format
                            #f "shouldbe-consecutive-doubles-count=~a, "
                            shouldbe-consecutive-doubles-count))
                          (err-4
                           (format
                            #f "result count=~a"
                            consecutive-doubles-count)))
                      (begin
                        (unittest2:assert?
                         (equal? shouldbe-consecutive-doubles-count
                                 consecutive-doubles-count)
                         sub-name
                         (string-append
                          err-1 err-3 err-4)
                         result-hash-table)
                        ))

                    (let ((result-list
                           (list-ref this-board shouldbe-position)))
                      (let ((err-5
                             (format
                              #f "shouldbe=~a, result=~a"
                              shouldbe-list result-list)))
                        (begin
                          (unittest2:assert?
                           (equal? shouldbe-list result-list)
                           sub-name
                           (string-append
                            err-1 err-5)
                           result-hash-table)
                          )))
                    )))
              (set! test-label-index (1+ test-label-index))
              )) test-list)
         )))
   ))

;;;#############################################################
;;;#############################################################
;;; board format (list place-number place-code count total pcnt-double)
(define (calculate-percentiles board-list num-orbits)
  (begin
    (let ((final-list
           (map
            (lambda (alist)
              (begin
                (let ((count (list-ref alist 2)))
                  (begin
                    (list-set! alist 3 num-orbits)
                    (let ((pcnt
                           (* 100.0
                              (/ count num-orbits))))
                      (begin
                        (list-set! alist 4 pcnt)
                        ))
                    alist
                    ))
                )) board-list)))
      (begin
        final-list
        ))
    ))

;;;#############################################################
;;;#############################################################
;;; board format (list place-number place-code count total pcnt-double)
(unittest2:define-tests-macro
 (test-calculate-percentiles-1 result-hash-table)
 (begin
   (let ((sub-name "test-calculate-percentiles-1")
         (board-list (initialize-list-data))
         (test-list
          (list
           (list
            (list
             (list 0 "a1" 2 0 0.0)
             (list 1 "a2" 3 0 0.0)
             (list 2 "a3" 4 0 0.0))
            10
            (list
             (list 0 "a1" 2 10 20.0)
             (list 1 "a2" 3 10 30.0)
             (list 2 "a3" 4 10 40.0))
            )))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (a-list)
          (begin
            (let ((board-list (list-ref a-list 0))
                  (num-orbits (list-ref a-list 1))
                  (shouldbe-list-list (list-ref a-list 2)))
              (begin
                (let ((result-list-list
                       (calculate-percentiles board-list num-orbits)))
                  (let ((shouldbe-length (length shouldbe-list-list))
                        (result-length (length result-list-list)))
                    (let ((err-1
                           (format
                            #f "~a : error (~a) : "
                            sub-name test-label-index))
                          (err-2
                           (format
                            #f "board-list=~a, num-orbits=~a, "
                            board-list num-orbits))
                          (err-3
                           (format
                            #f "shouldbe-length=~a, result=~a"
                            shouldbe-length result-length)))
                      (begin
                        (unittest2:assert?
                         (equal? shouldbe-length result-length)
                         sub-name
                         (string-append
                          err-1 err-2 err-3)
                         result-hash-table)

                        (do ((ii 0 (1+ ii)))
                            ((>= ii shouldbe-length))
                          (begin
                            (let ((shouldbe-alist (list-ref shouldbe-list-list ii))
                                  (result-alist (list-ref result-list-list ii)))
                              (let ((err-4
                                     (format
                                      #f "(~a) shouldbe-alist=~a, "
                                      ii shouldbe-alist))
                                    (err-5
                                     (format
                                      #f "result-alist=~a"
                                      result-alist)))
                                (begin
                                  (unittest2:assert?
                                   (equal? shouldbe-alist result-alist)
                                   sub-name
                                   (string-append
                                    err-1 err-4 err-5)
                                   result-hash-table)
                                  )))
                            ))
                        ))
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; board-list format (list place-number place-code count total pcnt-double)
(define (top-three-list-items board-list)
  (begin
    (let ((result-list (list)))
      (let ((sorted-board-list
             (sort-list
              board-list
              (lambda(a b)
                (begin
                  (> (list-ref a 2) (list-ref b 2))
                  ))
              )))
        (begin
          (do ((ii 0 (1+ ii)))
              ((>= ii 3))
            (begin
              (set!
               result-list
               (append
                result-list
                (list (list-ref sorted-board-list ii))))
              ))
          result-list
          )))
    ))

;;;#############################################################
;;;#############################################################
;;; board-list format (list place-number place-code count total pcnt-double)
(unittest2:define-tests-macro
 (test-top-three-list-items-1 result-hash-table)
 (begin
   (let ((sub-name "test-top-three-list-items-1")
         (board-list (initialize-list-data))
         (test-list
          (list
           (list
            (list
             (list 0 "a1" 2 0 0.0)
             (list 1 "a2" 3 0 0.0)
             (list 2 "a3" 4 0 0.0)
             (list 3 "a4" 1 0 0.0)
             (list 4 "a5" 1 0 0.0)
             (list 5 "a6" 5 0 0.0))
            (list
             (list 5 "a6" 5 0 0.0)
             (list 2 "a3" 4 0 0.0)
             (list 1 "a2" 3 0 0.0))
            )))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (a-list)
          (begin
            (let ((board-list (list-ref a-list 0))
                  (shouldbe-list-list (list-ref a-list 1)))
              (let ((result-list-list
                     (top-three-list-items board-list)))
                (let ((shouldbe-length
                       (length shouldbe-list-list))
                      (result-length
                       (length result-list-list)))
                  (let ((err-1
                         (format
                          #f "~a : error (~a) : board-list=~a, "
                          sub-name test-label-index board-list))
                        (err-2
                         (format
                          #f "shouldbe-length=~a, result-length=~a"
                          shouldbe-length result-length)))
                    (begin
                      (unittest2:assert?
                       (equal? shouldbe-length result-length)
                       sub-name
                       (string-append err-1 err-2)
                       result-hash-table)

                      (do ((ii 0 (1+ ii)))
                          ((>= ii shouldbe-length))
                        (begin
                          (let ((shouldbe-alist (list-ref shouldbe-list-list ii))
                                (result-alist (list-ref result-list-list ii)))
                            (let ((err-3
                                   (format
                                    #f "(~a) shouldbe-alist=~a, "
                                    ii shouldbe-alist))
                                  (err-4
                                   (format
                                    #f "result-alist=~a"
                                    result-alist)))
                              (begin
                                (unittest2:assert?
                                 (equal? shouldbe-alist result-alist)
                                 sub-name
                                 (string-append
                                  err-1 err-3 err-4)
                                 result-hash-table)
                                )))
                          ))
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; board-list format (list place-number place-code count total pcnt-double)
(define (top-three-list-to-number-code three-list)
  (begin
    (let ((result-string ""))
      (begin
        (do ((ii 0 (1+ ii)))
            ((>= ii 3))
          (begin
            (let ((this-elem (list-ref three-list ii)))
              (let ((this-num (list-ref this-elem 0)))
                (let ((num-string
                       (ice-9-format:format #f "~2,'0d" this-num)))
                  (begin
                    (set!
                     result-string
                     (string-append result-string num-string))
                    ))
                ))
            ))
        result-string
        ))
    ))

;;;#############################################################
;;;#############################################################
;;; board-list format (list place-number place-code count total pcnt-double)
(unittest2:define-tests-macro
 (test-top-three-list-to-number-code-1 result-hash-table)
 (begin
   (let ((sub-name "test-top-three-list-to-number-code-1")
         (board-list (initialize-list-data))
         (test-list
          (list
           (list
            (list
             (list 5 "a6" 5 0 0.0)
             (list 2 "a3" 4 0 0.0)
             (list 1 "a2" 3 0 0.0))
            "050201")
           (list
            (list
             (list 5 "a6" 5 0 0.0)
             (list 2 "a3" 4 0 0.0)
             (list 11 "a2" 3 0 0.0))
            "050211")
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (a-list)
          (begin
            (let ((three-list (list-ref a-list 0))
                  (shouldbe-string (list-ref a-list 1)))
              (let ((result-string
                     (top-three-list-to-number-code three-list)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : three-list=~a, "
                        sub-name test-label-index three-list))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe-string result-string)))
                  (begin
                    (unittest2:assert?
                     (string-ci=? shouldbe-string result-string)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; board-list format (list place-number place-code count total pcnt-double)
(define (simulation-loop board-list max-dice-rolls max-dice-num)
  (begin
    (let ((board-length (length board-list))
          (chance-deck (initialize-chance-deck))
          (community-deck (initialize-community-chest-deck))
          (position-counter 0)
          (consecutive-doubles-count 0))
      (begin
        (do ((ii 0 (1+ ii)))
            ((>= ii max-dice-rolls))
          (begin
            (let ((roll-list (roll-two-dice max-dice-num)))
              (let ((roll-sum (list-ref roll-list 0))
                    (is-double (list-ref roll-list 1)))
                (let ((next-position
                       (modulo
                        (+ position-counter roll-sum)
                        board-length)))
                  (begin
                    (if (equal? is-double #t)
                        (begin
                          (set!
                           consecutive-doubles-count
                           (1+ consecutive-doubles-count)))
                        (begin
                          (set! consecutive-doubles-count 0)
                          ))

                    (if (>= consecutive-doubles-count 3)
                        (begin
                          ;;; go directly to jail
                          (set! consecutive-doubles-count 0)
                          (set! position-counter 10)
                          (update-position-data-macro
                           board-list board-length position-counter))
                        (begin
                          ;;; (list place-number place-code count total-orbits pcnt-double)
                          (let ((this-elem
                                 (list-ref board-list next-position)))
                            (let ((position-code-name
                                   (list-ref this-elem 1)))
                              (begin
                                ;;; handle exceptions (community chest, chance, and goto jail)
                                (cond
                                 ((string-ci=?
                                   position-code-name "g2j")
                                  (begin
                                    (set! consecutive-doubles-count 0)
                                    (set! position-counter 10)
                                    (update-position-data-macro
                                     board-list board-length position-counter)
                                    ))
                                 ((not
                                   (equal?
                                    (string-contains-ci
                                     position-code-name "cc")
                                    #f))
                                  (begin
                                    ;;; community chest
                                    (handle-community-chest-macro
                                     board-list board-length
                                     position-counter next-position
                                     consecutive-doubles-count
                                     community-deck)
                                    ))
                                 ((not
                                   (equal?
                                    (string-contains-ci
                                     position-code-name "ch")
                                    #f))
                                  (begin
                                    ;;; chance
                                    (handle-chance-macro
                                     board-list board-length
                                     position-counter next-position
                                     consecutive-doubles-count
                                     chance-deck community-deck)
                                    ))
                                 (else
                                  (begin
                                    (set! position-counter next-position)
                                    (update-position-data-macro
                                     board-list board-length position-counter)
                                    )))
                                )))
                          ))
                    ))
                ))
            ))

        (if (> (list-ref (list-ref board-list 30) 2) 0)
            (begin
              (display
               (format
                #f "warning goto jail board results = ~a~%"
                (list-ref board-list 30)))
              (force-output)
              ))

        (calculate-percentiles board-list max-dice-rolls)
        (ice-9-copy-tree:copy-tree board-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (calc-std-error float-list)
  (begin
    (let ((flength (length float-list)))
      (begin
        (if (> flength 1)
            (begin
              (let ((favg
                     (/ (srfi-1:fold + 0.0 float-list)
                        flength))
                    (flen-m1 (- flength 1.0)))
                (let ((ssq
                       (/
                        (srfi-1:fold
                         (lambda (this-num prev)
                           (begin
                             (+ (* this-num this-num) prev)
                             ))
                         0.0 float-list)
                        flen-m1))
                      (favg2
                       (/ (* flength favg favg) flen-m1)))
                  (let ((std-err (sqrt (- ssq favg2))))
                    (begin
                      (if (< ssq favg2)
                          (begin
                            (display
                             (format
                              #f "calc-std-error : error for list~%"))
                            (display
                             (format
                              #f "list=~a, avg^2=~a, ssq=~a~%"
                              float-list favg2 ssq))
                            (force-output)
                            ))
                      std-err
                      ))
                  )))
            (begin
              -1
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
;;; board-list format (list place-number place-code count total pcnt-double)
(unittest2:define-tests-macro
 (test-calc-std-error-1 result-hash-table)
 (begin
   (let ((sub-name "test-calc-std-error-1")
         (board-list (initialize-list-data))
         (test-list
          (list
           (list (list 1.0 2.0 3.0) 1.0)
           (list (list 1.0 2.0 3.0 4.0) 1.290994)
           ))
         (test-label-index 0)
         (tolerance 1e-6))
     (begin
       (for-each
        (lambda (a-list)
          (begin
            (let ((float-list (list-ref a-list 0))
                  (shouldbe-std-err (list-ref a-list 1)))
              (let ((result-std-err
                     (calc-std-error float-list)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : float-list=~a, "
                        sub-name test-label-index float-list))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe-std-err result-std-err)))
                  (begin
                    (unittest2:assert?
                     (<= (abs (- shouldbe-std-err result-std-err))
                         tolerance)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (display-single-board-element a-list)
  (begin
    (let ((place-number (list-ref a-list 0))
          (place-name (list-ref a-list 1))
          (count (list-ref a-list 2))
          (total (list-ref a-list 3))
          (pcnt-list (list-ref a-list 4)))
      (let ((favg
             (/ (srfi-1:fold + 0.0 pcnt-list) (length pcnt-list)))
            (fstd-err (calc-std-error pcnt-list)))
        (begin
          (display
           (ice-9-format:format
            #f "    ~a (~1,2f%) = square ~2,'0d~%"
            place-name favg place-number))
          (display
           (ice-9-format:format
            #f "    (~:d out of ~:d, std error = ~4,2e)~%"
            count total fstd-err))
          )))
    ))

;;;#############################################################
;;;#############################################################
;;; board-list format (list place-number place-code count total pcnt-double)
;;; with-error-list format (list place-number place-code cummulative-count cummulative-totals pcnt-list)
(define (sub-main-loop
         board-list max-dice-rolls split-num max-dice-num)
  (begin
    (let ((result-htable (make-hash-table split-num)))
      (begin
        (do ((ii 0 (1+ ii)))
            ((>= ii split-num))
          (begin
            (let ((this-board
                   (ice-9-copy-tree:copy-tree board-list)))
              (let ((revised-board-list
                     (simulation-loop
                      this-board max-dice-rolls max-dice-num)))
                (begin
                  (for-each
                   (lambda (a-list)
                     (begin
                       (let ((place-number (list-ref a-list 0))
                             (place-name (list-ref a-list 1))
                             (count (list-ref a-list 2))
                             (total (list-ref a-list 3))
                             (pcnt-double (list-ref a-list 4)))
                         (let ((this-list
                                (hash-ref
                                 result-htable place-name
                                 (list place-number place-name 0 0 (list)))))
                           (begin
                             (list-set!
                              this-list 2 (+ count (list-ref this-list 2)))
                             (list-set!
                              this-list 3 (+ total (list-ref this-list 3)))
                             (list-set!
                              this-list 4
                              (cons
                               (* 100.0 (/ count total))
                               (list-ref this-list 4)))
                             (hash-set!
                              result-htable place-name this-list)
                             )))
                       )) revised-board-list)
                  )))
            ))

        (let ((result-list
               (hash-map->list
                (lambda (key value)
                  (begin
                    value
                    )) result-htable)))
          (let ((sorted-list
                 (sort
                  result-list
                  (lambda (a b)
                    (begin
                      (> (list-ref a 2) (list-ref b 2))
                      ))
                  )))
            (let ((top-three-list (list-head sorted-list 3))
                  (last-four-list
                   (list-tail sorted-list (- (length board-list) 4))))
              (let ((top-three-code
                     (top-three-list-to-number-code top-three-list)))
                (begin
                  (display
                   (format
                    #f "three most popular squares~%"))
                  (for-each
                   (lambda (a-list)
                     (begin
                       (display-single-board-element a-list)
                       )) top-three-list)

                  (display
                   (format
                    #f "three most popular squares modal "))
                  (display
                   (format
                    #f "string = ~a~%"
                    top-three-code))
                  (force-output)

                  (display
                   (format
                    #f "least popular squares~%"))
                  (for-each
                   (lambda (a-list)
                     (begin
                       (display-single-board-element a-list)
                       )) last-four-list)
                  (force-output)
                  )))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "In the game, Monopoly, the standard "))
    (display
     (format #f "board is set up~%"))
    (display
     (format #f "in the following way:~%"))
    (newline)
    (display
     (format #f "  GO  A1  CC1  A2  T1  R1  B1  CH1  B2  B3  JAIL~%"))
    (display
     (format #f "  H2                                         C1~%"))
    (display
     (format #f "  T2                                         U1~%"))
    (display
     (format #f "  H1                                         C2~%"))
    (display
     (format #f "  CH3                                        C3~%"))
    (display
     (format #f "  R4                                         R2~%"))
    (display
     (format #f "  G3                                         D1~%"))
    (display
     (format #f "  CC3                                       CC2~%"))
    (display
     (format #f "  G2                                         D2~%"))
    (display
     (format #f "  G1                                         D3~%"))
    (display
     (format #f "  G2J  F3  U2  F2  F1  R3  E3  E2  CH2  E1   FP~%"))
    (newline)
    (display
     (format #f "A player starts on the GO square "))
    (display
     (format #f "and adds the scores~%"))
    (display
     (format #f "on two 6-sided dice to determine "))
    (display
     (format #f "the number of squares~%"))
    (display
     (format #f "they advance in a clockwise direction. "))
    (display
     (format #f "Without any further~%"))
    (display
     (format #f "rules we would expect to visit each "))
    (display
     (format #f "square with equal~%"))
    (display
     (format #f "probability: 2.5%. However, landing "))
    (display
     (format #f "on G2J (Go To Jail),~%"))
    (display
     (format #f "CC (community chest), and CH (chance)~%"))
    (display
     (format #f "changes this distribution.~%"))
    (newline)
    (display
     (format #f "In addition to G2J, and one card from "))
    (display
     (format #f "each of CC and CH,~%"))
    (display
     (format #f "that orders the player to go directly "))
    (display
     (format #f "to jail, if a~%"))
    (display
     (format #f "player rolls three consecutive doubles, "))
    (display
     (format #f "they do not advance~%"))
    (display
     (format #f "the result of their 3rd roll. Instead "))
    (display
     (format #f "they proceed directly~%"))
    (display
     (format #f "to jail.~%"))
    (newline)
    (display
     (format #f "At the beginning of the game, the CC "))
    (display
     (format #f "and CH cards are~%"))
    (display
     (format #f "shuffled. When a player lands on CC "))
    (display
     (format #f "or CH they take~%"))
    (display
     (format #f "a card from the top of the respective "))
    (display
     (format #f "pile and, after~%"))
    (display
     (format #f "following the instructions, it is "))
    (display
     (format #f "returned to the bottom~%"))
    (display
     (format #f "of the pile. There are sixteen cards "))
    (display
     (format #f "in each pile, but~%"))
    (display
     (format #f "for the purpose of this problem we "))
    (display
     (format #f "are only concerned~%"))
    (display
     (format #f "with cards that order a movement; any "))
    (display
     (format #f "instruction not~%"))
    (display
     (format #f "concerned with movement will be ignored "))
    (display
     (format #f "and the player~%"))
    (display
     (format #f "will remain on the CC/CH square.~%"))
    (newline)
    (display
     (format #f "Community Chest (2/16 cards):~%"))
    (display
     (format #f "  1. Advance to GO~%"))
    (display
     (format #f "  2. Go to JAIL~%"))
    (display
     (format #f "Chance (10/16 cards):~%"))
    (display
     (format #f "  1. Advance to GO~%"))
    (display
     (format #f "  2. Go to JAIL~%"))
    (display
     (format #f "  3. Go to C1~%"))
    (display
     (format #f "  4. Go to E3~%"))
    (display
     (format #f "  5. Go to H2~%"))
    (display
     (format #f "  6. Go to R1~%"))
    (display
     (format #f "  7. Go to next R (railway company)~%"))
    (display
     (format #f "  8. Go to next R~%"))
    (display
     (format #f "  9. Go to next U (utility company)~%"))
    (display
     (format #f " 10. Go back 3 squares.~%"))
    (newline)
    (display
     (format #f "The heart of this problem concerns "))
    (display
     (format #f "the likelihood of visiting~%"))
    (display
     (format #f "a particular square. That is, the "))
    (display
     (format #f "probability of finishing~%"))
    (display
     (format #f "at that square after a roll. For "))
    (display
     (format #f "this reason it should~%"))
    (display
     (format #f "be clear that, with the exception of "))
    (display
     (format #f "G2J for which the~%"))
    (display
     (format #f "probability of finishing~%"))
    (display
     (format #f "on it is zero,~%"))
    (display
     (format #f "the CH squares will have the lowest "))
    (display
     (format #f "probabilities, as 5/8~%"))
    (display
     (format #f "request a movement to another square, "))
    (display
     (format #f "and it is the~%"))
    (display
     (format #f "final square that the player finishes "))
    (display
     (format #f "at on each roll~%"))
    (display
     (format #f "that we are interested in. We shall "))
    (display
     (format #f "make no distinction~%"))
    (display
     (format #f "between 'Just Visiting' and being "))
    (display
     (format #f "sent to JAIL, and~%"))
    (display
     (format #f "we shall also ignore the rule about "))
    (display
     (format #f "requiring a double to~%"))
    (display
     (format #f "'get out of jail', assuming that they pay "))
    (display
     (format #f "to get out on~%"))
    (display
     (format #f "their next turn.~%"))
    (newline)
    (display
     (format #f "By starting at GO and numbering the "))
    (display
     (format #f "squares sequentially~%"))
    (display
     (format #f "from 00 to 39 we can concatenate "))
    (display
     (format #f "these two-digit~%"))
    (display
     (format #f "numbers to produce strings that "))
    (display
     (format #f "correspond with sets~%"))
    (display
     (format #f "of squares.~%"))
    (newline)
    (display
     (format #f "Statistically it can be shown that "))
    (display
     (format #f "the three most~%"))
    (display
     (format #f "popular squares, in order, are JAIL "))
    (display
     (format #f "(6.24%) = Square 10,~%"))
    (display
     (format #f "E3 (3.18%) = Square 24, and GO (3.09%) "))
    (display
     (format #f "= Square 00. So~%"))
    (display
     (format #f "these three most popular squares can "))
    (display
     (format #f "be listed with the~%"))
    (display
     (format #f "six-digit modal string: 102400.~%"))
    (newline)
    (display
     (format #f "If, instead of using two 6-sided dice, "))
    (display
     (format #f "two 4-sided dice are~%"))
    (display
     (format #f "used, find the six-digit modal string.~%"))
    (display
     (format #f "see https://projecteuler.net/problem=84~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((max-tests 100000))
      (begin
        (display
         (format #f "testing random two dice~%"))
        (display-random-two-dice-tests max-tests)
        ))

    (newline)
    (let ((board-list (initialize-list-data))
          (max-dice-rolls 100000)
          (split-num 1000)
          (max-dice-num 6))
      (begin
        (initialize-random-state)
        (display (format #f "6-sided dice~%"))
        (sub-main-loop
         board-list max-dice-rolls split-num max-dice-num)
        (newline)
        ))

    (newline)
    (let ((board-list (initialize-list-data))
          (max-dice-rolls 100000)
          (split-num 1000)
          (max-dice-num 4))
      (begin
        (initialize-random-state)
        (display (format #f "4-sided dice~%"))
        (sub-main-loop
         board-list max-dice-rolls split-num max-dice-num)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 84 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 88                                     ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated July 26, 2022                                ###
;;;###                                                       ###
;;;###  updated March 8, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### srfi-1 for fold function
(use-modules ((srfi srfi-1)
              :renamer (symbol-prefix-proc 'srfi-1:)))

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define-syntax construct-factors-level-1-macro
  (syntax-rules ()
    ((construct-factors-level-1-macro
      local-array ii jj)
     (begin
       (let ((this-list (array-ref local-array jj)))
         (let ((div (euclidean/ jj ii)))
           (let ((prev-list (array-ref local-array div)))
             (begin
               (for-each
                (lambda (a-list)
                  (begin
                    (if (list? a-list)
                        (begin
                          (set!
                           this-list
                           (cons (cons ii a-list) this-list)))
                        (begin
                          (set!
                           this-list
                           (cons (list ii a-list) this-list))
                          ))
                    )) prev-list)

               (array-set! local-array this-list jj)
               ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax construct-factors-level-2-macro
  (syntax-rules ()
    ((construct-factors-level-2-macro
      elem ii result-list-list)
     (begin
       (for-each
        (lambda (a-list)
          (begin
            (if (and (list? a-list)
                     (> (length a-list) 1))
                (begin
                  (let ((stmp (sort a-list <)))
                    (begin
                      (set!
                       result-list-list
                       (cons (list ii stmp)
                             result-list-list))
                      ))
                  ))
            )) elem)
       ))
    ))

;;;#############################################################
;;;#############################################################
;;; from a list of divisors of nn, make a list of lists that
;;; 6 -> (list (list 4 (list 2 2)) (list 6 (list 2 3)))
;;; 8 -> (list (list 4 (list 2 2)) (list 6 (list 2 3))
;;;        (list 8 (list 2 2 2)) (list 8 (list 2 4)))
(define (construct-factors-list-list end-num)
  (begin
    (let ((local-array (make-array (list) (1+ end-num))))
      (begin
        (do ((ii 2 (1+ ii)))
            ((> ii end-num))
          (begin
            (let ((ii-list (array-ref local-array ii)))
              (begin
                (if (equal? ii-list (list))
                    (begin
                      (array-set! local-array (list ii) ii))
                    (begin
                      (array-set! local-array
                                  (cons (list ii) ii-list) ii)
                      ))

                (do ((jj (+ ii ii) (+ jj ii)))
                    ((> jj end-num))
                  (begin
                    (construct-factors-level-1-macro
                     local-array ii jj)
                    ))
                ))
            ))

        (let ((result-list-list (list)))
          (begin
            (do ((ii 0 (1+ ii)))
                ((> ii end-num))
              (begin
                (let ((elem (array-ref local-array ii)))
                  (begin
                    (if (and (list? elem) (> (length elem) 0))
                        (begin
                          (construct-factors-level-2-macro
                           elem ii result-list-list)
                          ))
                    ))
                ))

            (reverse result-list-list)
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-construct-factors-list-list-1 result-hash-table)
 (begin
   (let ((sub-name "test-construct-factors-list-list-1")
         (test-list
          (list
           (list 2 (list))
           (list 3 (list))
           (list 4 (list (list 4 (list 2 2))))
           (list 5 (list (list 4 (list 2 2))))
           (list 6 (list (list 4 (list 2 2))
                         (list 6 (list 2 3))))
           (list 10 (list (list 4 (list 2 2))
                          (list 6 (list 2 3))
                          (list 8 (list 2 2 2))
                          (list 8 (list 2 4))
                          (list 9 (list 3 3))
                          (list 10 (list 2 5))))
           (list 12 (list (list 4 (list 2 2))
                          (list 6 (list 2 3))
                          (list 8 (list 2 2 2))
                          (list 8 (list 2 4))
                          (list 9 (list 3 3))
                          (list 10 (list 2 5))
                          (list 12 (list 2 2 3))
                          (list 12 (list 2 6))
                          (list 12 (list 3 4))))
           (list 20 (list (list 4 (list 2 2))
                          (list 6 (list 2 3))
                          (list 8 (list 2 4))
                          (list 8 (list 2 2 2))
                          (list 9 (list 3 3))
                          (list 10 (list 2 5))
                          (list 12 (list 2 2 3))
                          (list 12 (list 2 6))
                          (list 12 (list 3 4))
                          (list 14 (list 2 7))
                          (list 15 (list 3 5))
                          (list 16 (list 2 2 2 2))
                          (list 16 (list 2 8))
                          (list 16 (list 2 2 4))
                          (list 16 (list 4 4))
                          (list 18 (list 2 9))
                          (list 18 (list 3 6))
                          (list 18 (list 2 3 3))
                          (list 20 (list 2 10))
                          (list 20 (list 2 2 5))
                          (list 20 (list 4 5))))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((max-num (list-ref alist 0))
                  (shouldbe-list-list (list-ref alist 1)))
              (let ((result-list-list
                     (construct-factors-list-list max-num)))
                (let ((slen (length shouldbe-list-list))
                      (rlen (length result-list-list)))
                  (let ((err-1
                         (format
                          #f "~a : error (~a) : max-num=~a, "
                          sub-name test-label-index max-num))
                        (err-2
                         (format
                          #f "shouldbe length=~a, result=~a~%"
                          slen rlen)))
                    (begin
                      (unittest2:assert?
                       (equal? slen rlen)
                       sub-name
                       (string-append err-1 err-2)
                       result-hash-table)

                      (for-each
                       (lambda (s-list)
                         (begin
                           (let ((err-3
                                  (format
                                   #f "s-list~a, " s-list))
                                 (err-4
                                  (format
                                   #f "result-list-list=~a"
                                   result-list-list)))
                             (begin
                               (unittest2:assert?
                                (not
                                 (equal?
                                  (member s-list result-list-list)
                                  #f))
                                sub-name
                                (string-append
                                 err-1 err-3 err-4)
                                result-hash-table)
                               ))
                           )) shouldbe-list-list)
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; tack on one's til the sum is equal
;;; { 8, (list 2 4) } -> (list 1 1 2 4)
(define (tack-on-ones nn factor-list)
  (begin
    (let ((this-sum (srfi-1:fold + 0 factor-list)))
      (begin
        (cond
         ((> this-sum nn)
          (begin
            #f
            ))
         ((= this-sum nn)
          (begin
            factor-list
            ))
         (else
          (begin
            (let ((one-list
                   (make-list (- nn this-sum) 1)))
              (begin
                (append one-list factor-list)
                ))
            )))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-tack-on-ones-1 result-hash-table)
 (begin
   (let ((sub-name "test-tack-on-ones-1")
         (test-list
          (list
           (list 4 (list 2 2) (list 2 2))
           (list 5 (list 1 5) #f)
           (list 6 (list 2 3) (list 1 2 3))
           (list 8 (list 2 4) (list 1 1 2 4))
           (list 8 (list 2 2 2) (list 1 1 2 2 2))
           (list 12 (list 2 6) (list 1 1 1 1 2 6))
           (list 12 (list 3 4) (list 1 1 1 1 1 3 4))
           (list 12 (list 2 2 3) (list 1 1 1 1 1 2 2 3))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((nn (list-ref alist 0))
                  (div-list (list-ref alist 1))
                  (shouldbe (list-ref alist 2)))
              (let ((result (tack-on-ones nn div-list)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "nn=~a, div-list=~a, "
                        nn div-list))
                      (err-3
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append
                      err-1 err-2 err-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; div-list-list format (list (list 6 (list 2 3)) (list 8 (list 2 4)) ...
;;; primary-list-list -> (list (list num kk product sum nfactors factor-list)
;;; (list num kk product sum nfactors factor-list) ...)
(define (make-primary-list-list div-list-list)
  (begin
    (let ((result-list (list)))
      (begin
        (for-each
         (lambda (d-list)
           (begin
             (let ((ddnum (list-ref d-list 0))
                   (ddlist (list-ref d-list 1)))
               (begin
                 (if (and (list? ddlist)
                          (> (length ddlist) 1))
                     (begin
                       (let ((product
                              (srfi-1:fold * 1 ddlist))
                             (sum
                              (srfi-1:fold + 0 ddlist))
                             (nfactors
                              (length ddlist)))
                         (let ((kk
                                (+ nfactors (- product sum))))
                           (let ((tlist
                                  (list ddnum kk product
                                        sum nfactors ddlist)))
                             (begin
                               (set!
                                result-list
                                (cons tlist result-list))
                               ))
                           ))
                       ))
                 ))
             )) div-list-list)

        (reverse result-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
;;; primary-list-list -> (list (list num kk product sum nfactors factor-list)
(unittest2:define-tests-macro
 (test-make-primary-list-list-1 result-hash-table)
 (begin
   (let ((sub-name "test-make-primary-list-list-1")
         (test-list
          (list
           (list (list (list 4 (list 2 2)) (list 6 (list 2 3)))
                 (list (list 4 2 4 4 2 (list 2 2))
                       (list 6 3 6 5 2 (list 2 3))))
           (list (list (list 8 (list 2 4)) (list 8 (list 2 2 2)))
                 (list (list 8 5 8 6 3 (list 2 2 2))
                       (list 8 4 8 6 2 (list 2 4))))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((div-list (list-ref alist 0))
                  (shouldbe-list-list (list-ref alist 1)))
              (let ((result-list-list
                     (make-primary-list-list div-list)))
                (let ((slen (length shouldbe-list-list))
                      (rlen (length result-list-list))
                      (max-len (length shouldbe-list-list)))
                  (let ((err-1
                         (format
                          #f "~a : error (~a) : div-list=~a, "
                          sub-name test-label-index div-list))
                        (err-2
                         (format
                          #f "shouldbe length=~a, result=~a"
                          slen rlen)))
                    (begin
                      (unittest2:assert?
                       (equal? slen rlen)
                       sub-name
                       (string-append err-1 err-2)
                       result-hash-table)

                      (do ((ii 0 (1+ ii)))
                          ((>= ii max-len))
                        (begin
                          (let ((shouldbe-elem
                                 (list-ref shouldbe-list-list ii)))
                            (let ((err-3
                                   (format
                                    #f "shouldbe-elem=~a, "
                                    shouldbe-elem))
                                  (err-4
                                   (format
                                    #f "result-list-list=~a~%"
                                    result-list-list)))
                              (begin
                                (unittest2:assert?
                                 (not
                                  (equal?
                                   (member shouldbe-elem
                                           result-list-list)
                                   #f))
                                 sub-name
                                 (string-append
                                  err-1 err-3 err-4)
                                 result-hash-table)
                                )))
                          ))
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (list-to-string llist inner-string)
  (begin
    (let ((this-string
           (string-join
            (map
             (lambda (this-elem)
               (begin
                 (ice-9-format:format #f "~:d" this-elem)
                 )) llist)
            inner-string)))
      (begin
        this-string
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-list-to-string-1 result-hash-table)
 (begin
   (let ((sub-name "test-list-to-string-1")
         (test-list
          (list
           (list (list 1) " + " "1")
           (list (list 1 2) " + " "1 + 2")
           (list (list 1 2 3) " * " "1 * 2 * 3")
           (list (list 4 5 6 7) " x " "4 x 5 x 6 x 7")
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((llist (list-ref this-list 0))
                  (inner-string (list-ref this-list 1))
                  (shouldbe (list-ref this-list 2)))
              (let ((result
                     (list-to-string llist inner-string)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "llist=~a, inner-string=~s, "
                        llist inner-string))
                      (err-3
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append
                      err-1 err-2 err-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define-syntax main-loop-sort-list-macro
  (syntax-rules ()
    ((main-loop-sort-list-macro
      primary-list-list
      sorted-primary-list)
     (begin
       (let ((this-list
              (sort primary-list-list
                    (lambda (a b)
                      (begin
                        (cond
                         ((< (list-ref a 1)
                             (list-ref b 1))
                          (begin
                            #t
                            ))
                         ((> (list-ref a 1)
                             (list-ref b 1))
                          (begin
                            #f
                            ))
                         (else
                          (begin
                            (< (list-ref a 0)
                               (list-ref b 0))
                            )))
                        )))
              ))
         (begin
           (set! sorted-primary-list this-list)
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax main-loop-print-debug-macro
  (syntax-rules ()
    ((main-loop-print-debug-macro
      debug-flag
      kk dnum min-prod-sum-set
      kk-plist-htable)
     (begin
       (if (equal? debug-flag #t)
           (begin
             (let ((plist
                    (hash-ref kk-plist-htable kk)))
               (let ((dkk (list-ref plist 1))
                     (dproduct (list-ref plist 2))
                     (dsum (list-ref plist 3))
                     (dfactors (list-ref plist 4))
                     (dlist (list-ref plist 5)))
                 (let ((factors-list
                        (tack-on-ones dnum dlist)))
                   (begin
                     (display
                      (ice-9-format:format
                       #f "  k=~:d : ~:d = ~a = ~a~%"
                       kk dnum
                       (list-to-string factors-list " x ")
                       (list-to-string factors-list " + ")))
                     (force-output)
                     ))
                 ))
             ))
       ))
    ))


;;;#############################################################
;;;#############################################################
;;; div-list-list format (list (list 6 (list 2 3)) (list 8 (list 2 4)) ...
;;; primary-list-list -> (list (list num kk product sum nfactors factor-list)
(define (sub-main-loop
         max-kk max-num status-num debug-flag)
  (begin
    (let ((min-sum 0)
          (min-prod-sum-set (list))
          (div-list-list (list))
          (kk-dnum-htable (make-hash-table max-kk))
          (kk-plist-htable (make-hash-table max-kk))
          (continue-loop-flag #t)
          (primary-list-list (list))
          (plen -1))
      (begin
        ;;; fact-list-list -> (list (list 4 (list 2 2)) (list 6 (list 2 3)) (list 8 (list 2 2 2)) (list 8 (list 2 4)),...)
        (let ((fac-list-list
               (construct-factors-list-list max-num)))
          (begin
            (set! div-list-list fac-list-list)
            ))

        ;;; primary-list-list -> (list (list num kk product sum nfactors factor-list),...)
        (let ((prim-list-list
               (make-primary-list-list div-list-list)))
          (let ((pp-len (length prim-list-list)))
            (begin
              (set! primary-list-list prim-list-list)
              (set! plen pp-len)
              )))

        (let ((sorted-primary-list (list)))
          (begin
            (main-loop-sort-list-macro
             primary-list-list
             sorted-primary-list)

            (do ((ii 0 (1+ ii)))
                ((>= ii plen))
              (begin
                (let ((plist (list-ref sorted-primary-list ii)))
                  (let ((dkk (list-ref plist 1)))
                    (let ((min-num (hash-ref kk-dnum-htable dkk -1)))
                      (begin
                        (if (< min-num 0)
                            (begin
                              (hash-set!
                               kk-dnum-htable dkk (list-ref plist 0))
                              (hash-set!
                               kk-plist-htable dkk plist)
                              ))
                        ))
                    ))
                ))

            (do ((kk 2 (1+ kk)))
                ((> kk max-kk))
              (begin
                (let ((dnum (hash-ref kk-dnum-htable kk -1)))
                  (begin
                    (if (> dnum 0)
                        (begin
                          (set!
                           min-prod-sum-set
                           (cons dnum min-prod-sum-set))

                          (main-loop-print-debug-macro
                           debug-flag
                           kk dnum min-prod-sum-set
                           kk-plist-htable))
                        (begin
                          (display
                           (ice-9-format:format
                            #f "  warning: no results for k=~:d "
                            kk))
                          (display
                           (ice-9-format:format
                            #f "and max-num = ~a~%"
                            max-num))
                          (force-output)
                          ))
                    ))
                ))

            (set!
             min-prod-sum-set
             (srfi-1:delete-duplicates
              (reverse min-prod-sum-set)))

            (if (equal? debug-flag #t)
                (begin
                  (display
                   (ice-9-format:format
                    #f "the complete set of minimal product-sum "))
                  (display
                   (ice-9-format:format
                    #f "numbers for 2 <= k <= ~:d " max-kk))
                  (display
                   (ice-9-format:format
                    #f "is  { ~a }, the sum is ~:d.~%"
                    (list-to-string min-prod-sum-set ", ")
                    (srfi-1:fold + 0 min-prod-sum-set))))
                (begin
                  (display
                   (ice-9-format:format
                    #f "the complete sum of minimal product-sum "))
                  (display
                   (ice-9-format:format
                    #f "numbers is ~:d, "
                    (srfi-1:fold + 0 min-prod-sum-set)))
                  (display
                   (ice-9-format:format
                    #f "for 2 <= k <= ~:d.~%"
                    max-kk))
                  ))
            (force-output)
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "A natural number, N, that can be "))
    (display
     (format #f "written as the sum~%"))
    (display
     (format #f "and product of a given set of at "))
    (display
     (format #f "least two natural numbers,~%"))
    (display
     (format #f "{a1, a2, ... , ak} is called a "))
    (display
     (format #f "product-sum number:~%"))
    (display
     (format #f "N = a1 + a2 + ... + ak "))
    (display
     (format #f "= a1 x a2 x ... x ak.~%"))
    (newline)
    (display
     (format #f "For example, 6 = 1 + 2 + 3 "))
    (display
     (format #f "= 1 x 2 x 3.~%"))
    (display
     (format #f "For a given set of size, k, we "))
    (display
     (format #f "shall call the smallest~%"))
    (display
     (format #f "N with this property a minimal "))
    (display
     (format #f "product-sum number. The~%"))
    (display
     (format #f "minimal product-sum numbers for "))
    (display
     (format #f "sets of size,~%"))
    (display
     (format #f "k = 2, 3, 4, 5, and 6 are "))
    (display
     (format #f "as follows.~%"))
    (newline)
    (display
     (format #f "  k=2: 4 = 2 x 2 = 2 + 2~%"))
    (display
     (format #f "  k=3: 6 = 1 x 2 x 3 = 1 + 2 + 3~%"))
    (display
     (format #f "  k=4: 8 = 1 x 1 x 2 x 4 = 1 + 1 + 2 + 4~%"))
    (display
     (format #f "  k=5: 8 = 1 x 1 x 2 x 2 x 2 = 1 + 1 + 2 + 2 + 2~%"))
    (display
     (format #f "  k=6: 12 = 1 x 1 x 1 x 1 x 2 x 6 = 1 + 1 + 1 + 1 + 2 + 6~%"))
    (newline)
    (display
     (format #f "Hence for 2<=k<=6, the sum of all "))
    (display
     (format #f "the minimal product-sum~%"))
    (display
     (format #f "numbers is 4+6+8+12 = 30; note that "))
    (display
     (format #f "8 is only~%"))
    (display
     (format #f "counted once in the sum.~%"))
    (newline)
    (display
     (format #f "In fact, as the complete set of "))
    (display
     (format #f "minimal product-sum~%"))
    (display
     (format #f "numbers for 2<=k<=12 is~%"))
    (display
     (format #f "{4, 6, 8, 12, 15, 16}, the sum is 61.~%"))
    (newline)
    (display
     (format #f "What is the sum of all the minimal "))
    (display
     (format #f "product-sum numbers~%"))
    (display
     (format #f "for 2<=k<=12000?~%"))
    (newline)
    (display
     (format #f "This solution uses dynamic programming, "))
    (display
     (format #f "similar to~%"))
    (display
     (format #f "problem 76.  A single array is "))
    (display
     (format #f "used to store a~%"))
    (display
     (format #f "list of lists of combinations of "))
    (display
     (format #f "divisors of n, between~%"))
    (display
     (format #f "2 and max-num.  Then the sum and "))
    (display
     (format #f "product are computed~%"))
    (display
     (format #f "from the list of lists, sorted, "))
    (display
     (format #f "and the minimal~%"))
    (display
     (format #f "k is extracted.~%"))
    (display
     (format #f "see https://projecteuler.net/problem=88~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((max-kk 12)
          (max-num 100)
          (status-num 1000)
          (debug-flag #t))
      (begin
        (sub-main-loop
         max-kk max-num status-num debug-flag)
        ))

    (newline)
    (let ((max-kk 12000)
          (max-num 15000)
          (status-num 1000)
          (debug-flag #f))
      (begin
        (sub-main-loop
         max-kk max-num status-num debug-flag)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 88 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

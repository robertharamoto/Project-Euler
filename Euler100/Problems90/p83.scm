#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 83                                     ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated July 26, 2022                                ###
;;;###                                                       ###
;;;###  updated March 8, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 rdelim for read-line functions
(use-modules ((ice-9 rdelim)
              :renamer (symbol-prefix-proc 'ice-9-rdelim:)))

;;;### srfi-1 for fold function
(use-modules ((srfi srfi-1)
              :renamer (symbol-prefix-proc 'srfi-1:)))

;;;### srfi-9 for define record type
(use-modules ((srfi srfi-9)
              :renamer (symbol-prefix-proc 'srfi-9:)))

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(srfi-9:define-record-type
 node
 (make-node row col nvalue fvalue gvalue hvalue
            parent-row parent-col)
 node?
 (row get-node-row set-node-row!)
 (col get-node-col set-node-col!)
 (nvalue get-node-nvalue set-node-nvalue!)
 (fvalue get-node-fvalue set-node-fvalue!)
 (gvalue get-node-gvalue set-node-gvalue!)
 (hvalue get-node-hvalue set-node-hvalue!)
 (parent-row get-node-parent-row set-node-parent-row!)
 (parent-col get-node-parent-col set-node-parent-col!))

;;;#############################################################
;;;#############################################################
(define (minimum-element two-d-array max-rows max-cols)
  (begin
    (let ((min-element (array-ref two-d-array 0 0)))
      (begin
        (do ((ii 0 (1+ ii)))
            ((>= ii max-rows))
          (begin
            (do ((jj 0 (1+ jj)))
                ((>= jj max-rows))
              (begin
                (let ((this-elem (array-ref two-d-array ii jj)))
                  (begin
                    (if (< this-elem min-element)
                        (begin
                          (set! min-element this-elem)
                          ))
                    ))
                ))
            ))
        min-element
        ))
    ))

;;;#############################################################
;;;#############################################################
;;; acc-list format lists of
;;; (list current-length current-sum end-row end-col (list current path))
(unittest2:define-tests-macro
 (test-minimum-element-1 result-hash-table)
 (begin
   (let ((sub-name "test-minimum-element-1")
         (test-list
          (list
           (list (list (list 4 5 6) (list 7 8 9) (list 10 11 3))
                 3 3 3)
           (list (list (list 14 5 6 7) (list 7 8 9 10)
                       (list 10 11 13 7) (list 22 33 44 6))
                 4 4 5)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (a-list)
          (begin
            (let ((tlist-list (list-ref a-list 0))
                  (max-rows (list-ref a-list 1))
                  (max-cols (list-ref a-list 2))
                  (shouldbe (list-ref a-list 3)))
              (let ((tarray (list->array 2 tlist-list)))
                (let ((result
                       (minimum-element tarray max-rows max-cols)))
                  (let ((err-1
                         (format
                          #f "~a : error (~a) : tlist-list=~a, "
                          sub-name test-label-index tlist-list))
                        (err-2
                         (format
                          #f "shouldbe=~a, result=~a~%"
                          shouldbe result)))
                    (begin
                      (unittest2:assert?
                       (equal? shouldbe result)
                       sub-name
                       (string-append err-1 err-2)
                       result-hash-table)
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; node definition : (row col nvalue fvalue gvalue hvalue parent-row parent-col)
(define (find-lowest-fvalue-in-hash node-htable)
  (begin
    (let ((min-node #f)
          (min-key #f)
          (min-fvalue -1))
      (begin
        (hash-for-each
         (lambda (this-key this-node)
           (begin
             (let ((this-fvalue (get-node-fvalue this-node)))
               (begin
                 (if (or (< min-fvalue 0)
                         (< this-fvalue min-fvalue))
                     (begin
                       (set! min-fvalue this-fvalue)
                       (set! min-node this-node)
                       (set! min-key this-key)
                       ))
                 ))
             )) node-htable)
        (list min-key min-node)
        ))
    ))

;;;#############################################################
;;;#############################################################
;;; node definition : (row col nvalue fvalue gvalue hvalue parent-row parent-col)
(unittest2:define-tests-macro
 (test-find-lowest-fvalue-in-hash-1 result-hash-table)
 (begin
   (let ((sub-name "test-find-lowest-fvalue-in-hash-1")
         (test-list
          (list
           (list (list (make-node 0 0 10 4 2 2 -1 -1)
                       (make-node 0 1 11 5 2 3 0 0)
                       (make-node 1 0 12 6 3 3 0 0)
                       (make-node 1 1 13 7 2 2 1 0))
                 (list (list 0 0) (make-node 0 0 10 4 2 2 -1 -1)))
           (list (list (make-node 0 0 10 8 2 2 -1 -1)
                       (make-node 0 1 11 5 2 3 0 0)
                       (make-node 1 0 12 6 3 3 0 0)
                       (make-node 1 1 13 4 2 2 1 0))
                 (list (list 1 1) (make-node 1 1 13 4 2 2 1 0)))
           (list (list (make-node 0 0 10 88 2 2 -1 -1)
                       (make-node 0 1 11 5 2 3 0 0)
                       (make-node 1 0 12 6 3 3 0 0)
                       (make-node 1 1 13 99 2 2 1 0))
                 (list (list 0 1) (make-node 0 1 11 5 2 3 0 0)))
           ))
         (node-htable (make-hash-table 10))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (a-list)
          (begin
            (let ((node-list (list-ref a-list 0))
                  (shouldbe (list-ref a-list 1)))
              (begin
                (hash-clear! node-htable)
                (for-each
                 (lambda (this-node)
                   (begin
                     (let ((this-row (get-node-row this-node))
                           (this-col (get-node-col this-node)))
                       (begin
                         (hash-set!
                          node-htable
                          (list this-row this-col) this-node)
                         ))
                     )) node-list)

                (let ((result
                       (find-lowest-fvalue-in-hash node-htable)))
                  (let ((err-1
                         (format
                          #f "~a : error (~a) : node-list=~a, "
                          sub-name test-label-index node-list))
                        (err-2
                         (format
                          #f "shouldbe=~a, result=~a~%"
                          shouldbe result)))
                    (begin
                      (unittest2:assert?
                       (equal? shouldbe result)
                       sub-name
                       (string-append err-1 err-2)
                       result-hash-table)
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (move-from-open-to-closed
         lkey node open-htable closed-htable)
  (begin
    (hash-remove! open-htable lkey)
    (hash-set! closed-htable lkey node)
    ))

;;;#############################################################
;;;#############################################################
(define-syntax is-a-better-path
  (syntax-rules ()
    ((is-a-better-path
      p-row p-col max-rows max-cols grid-array
      node open-htable closed-htable
      this-grid-value this-gvalue this-hvalue)
     (begin
       (if (and (>= p-row 0)
                (< p-row max-rows)
                (>= p-col 0)
                (< p-col max-cols))
           (begin
             (let ((p-key (list p-row p-col))
                   (p-grid-value
                    (array-ref grid-array p-row p-col)))
               (let ((node-1
                      (hash-ref closed-htable p-key #f)))
                 (begin
                   (if (equal? node-1 #f)
                       (begin
                         (set!
                          node-1 (hash-ref open-htable p-key))
                         ))
                   (if (not (equal? node-1 #f))
                       (begin
                         (let ((p-gvalue
                                (get-node-gvalue node-1)))
                           (let ((next-gvalue
                                  (+ p-gvalue this-grid-value)))
                             (begin
                               (if (< next-gvalue this-gvalue)
                                   (begin
                                     (set! this-gvalue next-gvalue)
                                     (set-node-gvalue! node next-gvalue)
                                     (set-node-parent-row! node p-row)
                                     (set-node-parent-col! node p-col)
                                     (set-node-fvalue!
                                      node (+ next-gvalue this-hvalue))
                                     ))
                               )))
                         ))
                   )))
             ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (check-for-better-paths
         lkey node open-htable closed-htable
         grid-array max-rows max-cols)
  (begin
    (let ((this-row (get-node-row node))
          (this-col (get-node-col node))
          (this-gvalue (get-node-gvalue node))
          (this-hvalue (get-node-hvalue node)))
      (let ((this-grid-value
             (array-ref grid-array this-row this-col)))
        (begin
          (let ((parent-list
                 (list
                  (list (1- this-row) this-col)
                  (list (1+ this-row) this-col)
                  (list this-row (1- this-col))
                  (list this-row (1+ this-col)))))
            (begin
              (for-each
               (lambda (lkey)
                 (begin
                   (let ((p-row (list-ref lkey 0))
                         (p-col (list-ref lkey 1)))
                     (begin
                       (is-a-better-path
                        p-row p-col max-rows max-cols
                        grid-array node open-htable closed-htable
                        this-grid-value this-gvalue this-hvalue)
                       ))
                   )) parent-list)
              ))

          (hash-set! open-htable lkey node)
          )))
    ))

;;;#############################################################
;;;#############################################################
;;; node definition : (row col nvalue fvalue gvalue hvalue parent-row parent-col)
(unittest2:define-tests-macro
 (test-check-for-better-paths-1 result-hash-table)
 (begin
   (let ((sub-name "test-check-for-better-paths-1")
         (test-list
          (list
           (list 3 3
                 (list (list 1 2 3)
                       (list 99 98 1)
                       (list 120 123 4))
                 (list 1 1)
                 (make-node 1 1 98 200 198 2 1 0)
                 (list (make-node 0 0 1 5 1 4 -1 -1)
                       (make-node 0 1 2 6 3 3 0 0)
                       (make-node 1 0 99 103 100 3 0 0)
                       (make-node 1 1 13 7 2 2 1 0))
                 (make-node 1 1 98 103 101 2 0 1))
           ))
         (open-htable (make-hash-table 10))
         (closed-htable (make-hash-table 10))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (a-list)
          (begin
            (let ((max-rows (list-ref a-list 0))
                  (max-cols (list-ref a-list 1))
                  (grid-list-list (list-ref a-list 2))
                  (lkey (list-ref a-list 3))
                  (node (list-ref a-list 4))
                  (open-node-list (list-ref a-list 5))
                  (shouldbe-node (list-ref a-list 6)))
              (let ((grid-array
                     (list->array 2 grid-list-list)))
                (begin
                  (hash-clear! open-htable)
                  (hash-clear! closed-htable)

                  (for-each
                   (lambda (this-node)
                     (begin
                       (let ((this-row (get-node-row this-node))
                             (this-col (get-node-col this-node)))
                         (begin
                           (hash-set!
                            open-htable
                            (list this-row this-col) this-node)
                           ))
                       )) open-node-list)

                  (check-for-better-paths
                   lkey node open-htable closed-htable
                   grid-array max-rows max-cols)

                  (let ((result-node
                         (hash-ref open-htable lkey #f)))
                    (let ((err-1
                           (format
                            #f "~a : error (~a) : grid-list-list=~a, "
                            sub-name test-label-index grid-list-list))
                          (err-2
                           (format
                            #f "shouldbe=~a, result=~a~%"
                            shouldbe-node result-node)))
                      (begin
                        (unittest2:assert?
                         (equal? shouldbe-node result-node)
                         sub-name
                         (string-append err-1 err-2)
                         result-hash-table)
                        )))
                  )))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define-syntax process-next-node
  (syntax-rules ()
    ((process-next-node
      next-row next-col parent-row parent-col
      max-rows max-cols grid-array
      min-grid-value
      node open-htable closed-htable
      this-grid-value this-gvalue this-hvalue)
     (begin
       (let ((next-key (list next-row next-col)))
         (begin
           (if (and
                (>= next-row 0) (< next-row max-rows)
                (>= next-col 0) (< next-col max-cols)
                (equal? (hash-ref closed-htable next-key #f) #f))
               (begin
                 (if (equal? (hash-ref open-htable next-key #f) #f)
                     (begin
                       (let ((next-value
                              (array-ref
                               grid-array next-row next-col))
                             (next-hvalue
                              (* min-grid-value
                                 (+ (- max-rows parent-row)
                                    (- max-cols parent-col))))
                             (parent-gvalue
                              (get-node-gvalue node)))
                         (let ((next-gvalue
                                (+ parent-gvalue next-value))
                               (next-fvalue
                                (+ parent-gvalue
                                   next-value next-hvalue)))
                           (let ((next-node
                                  (make-node
                                   next-row next-col next-value
                                   next-fvalue next-gvalue
                                   next-hvalue
                                   parent-row parent-col)))
                             (begin
                               (hash-set!
                                open-htable next-key next-node)
                               ))
                           )))
                     (begin
                       (let ((next-node
                              (hash-ref open-htable next-key #f)))
                         (begin
                           (if (not (equal? next-node #f))
                               (begin
                                 (check-for-better-paths
                                  next-key next-node
                                  open-htable closed-htable
                                  grid-array max-rows max-cols)
                                 ))
                           ))
                       ))
                 ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
;;; node definition : (row col nvalue fvalue gvalue hvalue parent-row parent-col)
(define (astar-iteration-loop
         grid-array max-rows max-cols
         init-row init-col closed-htable)
  (begin
    (let ((open-htable (make-hash-table max-rows))
          (current-row init-row)
          (current-col init-col)
          (end-row (- max-rows 1))
          (end-col (- max-cols 1))
          (min-grid-value
           (minimum-element grid-array max-rows max-cols))
          (loop-continue-flag #t))
      (let ((gvalue
             (array-ref grid-array init-row init-col))
            (hvalue
             (* min-grid-value
                (+ (- max-rows init-row)
                   (- max-cols init-col)))))
        (let ((current-node
               (make-node
                init-row init-col
                gvalue (+ gvalue hvalue)
                gvalue hvalue -1 -1)))
          (begin
            (hash-clear! open-htable)
            (hash-clear! closed-htable)

            (hash-set!
             open-htable
             (list current-row current-col) current-node)

            (while
             (equal? loop-continue-flag #t)
             (begin
               (let ((min-list
                      (find-lowest-fvalue-in-hash open-htable)))
                 (let ((lkey (list-ref min-list 0))
                       (min-node (list-ref min-list 1)))
                   (let ((parent-row (list-ref lkey 0))
                         (parent-col (list-ref lkey 1)))
                     (begin
                       (hash-remove! open-htable lkey)
                       (hash-set! closed-htable lkey min-node)

                       (if (and (= parent-row end-row)
                                (= parent-col end-col))
                           (begin
                             (set! loop-continue-flag #f)
                             ))

                       (let ((neighbor-list
                              (list
                               (list (- parent-row 1) parent-col)
                               (list (+ parent-row 1) parent-col)
                               (list parent-row (- parent-col 1))
                               (list parent-row (+ parent-col 1)))))
                         (begin
                           (for-each
                            (lambda (neighbor-key)
                              (begin
                                (let ((next-row (list-ref neighbor-key 0))
                                      (next-col (list-ref neighbor-key 1)))
                                  (begin
                                    (process-next-node
                                     next-row next-col
                                     parent-row parent-col
                                     max-rows max-cols grid-array
                                     min-grid-value min-node
                                     open-htable closed-htable
                                     this-grid-value this-gvalue
                                     this-hvalue)
                                    ))
                                )) neighbor-list)
                           ))
                       ))
                   ))
               ))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (work-backwards-path
         closed-htable max-rows max-cols)
  (begin
    (let ((current-row (1- max-rows))
          (current-col (1- max-cols))
          (result-list (list))
          (loop-continue-flag #t))
      (begin
        (while
         (equal? loop-continue-flag #t)
         (begin
           (let ((lkey (list current-row current-col)))
             (let ((node (hash-ref closed-htable lkey #f)))
               (begin
                 (if (not (equal? node #f))
                     (begin
                       (set! result-list (cons node result-list))
                       (if (and (<= current-row 0)
                                (<= current-col 0))
                           (begin
                             (set! loop-continue-flag #f))
                           (begin
                             (set!
                              current-row (get-node-parent-row node))
                             (set!
                              current-col (get-node-parent-col node))
                             )))
                     (begin
                       (set! loop-continue-flag #f)
                       ))
                 )))
           ))

        (reverse result-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (display-node-path node-list)
  (begin
    (let ((llen (length node-list))
          (value-list (list)))
      (begin
        (for-each
         (lambda (node)
           (begin
             (let ((node-value (get-node-nvalue node)))
               (begin
                 (set! value-list (cons node-value value-list))
                 ))
             )) node-list)

        (let ((vstring
               (string-join
                (map
                 (lambda (num)
                   (begin
                     (ice-9-format:format #f "~:d" num)
                     )) value-list)
                ", "))
              (vsum (srfi-1:fold + 0 value-list)))
          (begin
            (display
             (ice-9-format:format
              #f "path sum = ~:d, path = { ~a }~%" vsum vstring))
            (force-output)
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (display-2d-array this-array max-row max-col)
  (begin
    (cond
     ((not (array? this-array))
      (begin
        (display
         (format
          #f "display-array error: expecting array~%"))
        (display
         (format
          #f "instead received ~a~%" this-array))
        (display
         (format
          #f "quitting...~%"))
        (quit)
        ))
     (else
      (begin
        (do ((ii-row 0 (1+ ii-row)))
            ((>= ii-row max-row))
          (begin
            (do ((ii-col 0 (1+ ii-col)))
                ((>= ii-col max-col))
              (begin
                (display
                 (ice-9-format:format
                  #f "~4:d " (array-ref this-array ii-row ii-col)))
                ))
            (newline)
            ))
        )))
    ))

;;;#############################################################
;;;#############################################################
(define (process-grid grid-array max-rows max-cols)
  (begin
    (let ((closed-htable (make-hash-table max-rows)))
      (begin
        ;;; find path, store it in closed-htable
        (astar-iteration-loop
         grid-array max-rows max-cols
         0 0 closed-htable)

        ;;; get path as a list of nodes from closed-htable
        (let ((node-path-list
               (work-backwards-path
                closed-htable max-rows max-cols)))
          (begin
            node-path-list
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
;;; returns a list of lists
(define (read-in-file fname)
  (begin
    (let ((results-list-list (list))
          (counter 0))
      (begin
        (if (file-exists? fname)
            (begin
              (with-input-from-file fname
                (lambda ()
                  (begin
                    (do ((line
                          (ice-9-rdelim:read-delimited "\r\n")
                          (ice-9-rdelim:read-delimited "\r\n")))
                        ((eof-object? line))
                      (begin
                        (cond
                         ((and (not (eof-object? line))
                               (> (string-length line) 0))
                          (begin
                            (let ((this-string-list
                                   (string-split
                                    (string-trim-both line) #\,)))
                              (begin
                                (let ((this-list
                                       (map
                                        (lambda (this-string)
                                          (begin
                                            (if (and
                                                 (string? this-string)
                                                 (not
                                                  (equal?
                                                   (string->number this-string) #f)))
                                                (begin
                                                  (string->number this-string))
                                                (begin
                                                  -1
                                                  ))
                                            )) this-string-list)))
                                  (begin
                                    (set!
                                     results-list-list
                                     (cons this-list results-list-list))
                                    ))
                                ))
                            )))
                        ))
                    )))
              (reverse results-list-list))
            (begin
              (list)
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop
         grid-list-list max-rows max-cols debug-flag)
  (begin
    (let ((grid-array
           (list->array 2 grid-list-list)))
      (let ((node-list
             (process-grid grid-array max-rows max-cols)))
        (begin
          (if (equal? debug-flag #t)
              (begin
                (display-2d-array grid-array max-rows max-cols)
                ))

          (display-node-path node-list)
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "NOTE: This problem is a significantly "))
    (display
     (format #f "more challenging version~%"))
    (display
     (format #f "of Problem 81.~%"))
    (display
     (format #f "In the 5 by 5 matrix below, the minimal "))
    (display
     (format #f "path sum from~%"))
    (display
     (format #f "the top left to the bottom right, by "))
    (display
     (format #f "moving left, right, up,~%"))
    (display
     (format #f "and down, is indicated in bold red "))
    (display
     (format #f "and is equal~%"))
    (display
     (format #f "to 2297.~%"))
    (newline)
    (display
     (format #f "  131  673  234  103  18~%"))
    (display
     (format #f "  201  96   342  965  150~%"))
    (display
     (format #f "  630  803  746  422  111~%"))
    (display
     (format #f "  537  699  497  121  956~%"))
    (display
     (format #f "  805  732  524  37   331~%"))
    (newline)
    (display
     (format #f "Find the minimal path sum, in matrix.txt "))
    (display
     (format #f "(right click and~%"))
    (display
     (format #f "'Save Link/Target As...'), a 31K text "))
    (display
     (format #f "file containing a~%"))
    (display
     (format #f "80 by 80 matrix, from the top left to "))
    (display
     (format #f "the bottom right by~%"))
    (display
     (format #f "moving left, right, up, and down.~%"))
    (newline)
    (display
     (format #f "The solution follows the A* method, "))
    (display
     (format #f "shown in this article~%"))
    (display
     (format #f "https://en.wikipedia.org/wiki/A*_search_algorithm~%"))
    (display
     (format #f "see https://projecteuler.net/problem=83~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((tlist
           (list
            (list 131 673 234 103 18)
            (list 201 96 342 965 150)
            (list 630 803 746 422 111)
            (list 537 699 497 121 956)
            (list 805 732 524 37 331)))
          (max-rows 5)
          (max-cols 5)
          (debug-flag #t))
      (begin
        (sub-main-loop
         tlist max-rows max-cols debug-flag)
        ))

    (newline)
    (let ((filename "matrix.txt")
          (debug-flag #f))
      (let ((tlist (read-in-file filename)))
        (let ((max-rows (length tlist))
              (max-cols (length (car tlist))))
          (begin
            (display
             (format
              #f "read in file~%" filename))
            (display
             (format
              #f "rows = ~a, cols = ~a~%"
              max-rows max-cols))

            (sub-main-loop
             tlist max-rows max-cols debug-flag)
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 83 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 86                                     ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated July 26, 2022                                ###
;;;###                                                       ###
;;;###  updated March 8, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path
              (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### timer-module for time-code function
(use-modules ((utils-module)
              :renamer (symbol-prefix-proc 'utils-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (partial-count-cuboids aa bb)
  (begin
    (if (<= bb aa)
        (begin
          (let ((b-half (euclidean/ bb 2)))
            (begin
              b-half
              )))
        (begin
          (let ((bp1 (euclidean/ (+ bb 1) 2)))
            (begin
              (let ((cube-count (+ 1 (- aa bp1))))
                (begin
                  (max 0 cube-count)
                  ))
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-partial-count-cuboids-1 result-hash-table)
 (begin
   (let ((sub-name "test-partial-count-cuboids-1")
         (test-list
          (list
           (list 3 4 2)
           (list 6 8 3)
           (list 9 12 4)
           (list 5 12 0)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (a-list)
          (begin
            (let ((aa (list-ref a-list 0))
                  (bb (list-ref a-list 1))
                  (shouldbe (list-ref a-list 2)))
              (let ((result (partial-count-cuboids aa bb)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "aa=~a, bb=~a, " aa bb))
                      (err-3
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append
                      err-1 err-2 err-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (calculate-cuboids-fixed-max aa)
  (begin
    (let ((count 0)
          (max-bb (* 2 aa))
          (aa-2 (* aa aa)))
      (begin
        (do ((bb 1 (1+ bb)))
            ((> bb max-bb))
          (begin
            (let ((bb-2 (* bb bb)))
              (let ((dd-sqrt
                     (sqrt (+ aa-2 bb-2))))
                (let ((ii-sqrt
                       (truncate dd-sqrt)))
                  (begin
                    (if (equal? dd-sqrt ii-sqrt)
                        (begin
                          (let ((cube-count
                                 (partial-count-cuboids aa bb)))
                            (begin
                              (set! count (+ count cube-count))
                              ))
                          ))
                    ))
                ))
            ))
        count
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-calculate-cuboids-fixed-max-1 result-hash-table)
 (begin
   (let ((sub-name "test-calculate-cuboids-fixed-max-1")
         (test-list
          (list
           (list 1 0) (list 2 0)
           (list 3 2) (list 4 1)
           (list 100 85)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (a-list)
          (begin
            (let ((aa (list-ref a-list 0))
                  (shouldbe (list-ref a-list 1)))
              (let ((result
                     (calculate-cuboids-fixed-max aa)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : aa=~a, "
                        sub-name test-label-index aa))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; use euclids formula to generate pythagorean triples
(define (cummulative-count-cuboids max-len)
  (begin
    (let ((cuboid-count 0))
      (begin
        (do ((ii 1 (1+ ii)))
            ((> ii max-len))
          (begin
            (let ((c-count
                   (calculate-cuboids-fixed-max ii)))
              (begin
                (if (> c-count 0)
                    (begin
                      (set!
                       cuboid-count (+ cuboid-count c-count))
                      ))
                ))
            ))

        cuboid-count
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-cummulative-count-cuboids-1 result-hash-table)
 (begin
   (let ((sub-name "test-cummulative-count-cuboids-1")
         (test-list
          (list
           (list 1 0) (list 2 0)
           (list 3 2) (list 4 3)
           (list 5 3) (list 6 6)
           (list 7 6) (list 8 10)
           (list 9 14) (list 10 14)
           (list 11 14) (list 12 25)
           (list 13 25) (list 14 25)
           (list 15 35) (list 20 67)
           (list 25 113)
           (list 99 1975)
           (list 100 2060)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (a-list)
          (begin
            (let ((max-len (list-ref a-list 0))
                  (shouldbe (list-ref a-list 1)))
              (let ((result
                     (cummulative-count-cuboids max-len)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : max-len=~a, "
                        sub-name test-label-index max-len))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop
         max-length target-number-solutions status-num)
  (begin
    (let ((current-mm 0)
          (current-number-solutions 0)
          (previous-mm 0)
          (previous-number-solutions 0)
          (continue-loop-flag #t))
      (begin
        (do ((max-len 1 (1+ max-len)))
            ((or (> max-len max-length)
                 (equal? continue-loop-flag #f)))
          (begin
            (let ((cuboid-count
                   (calculate-cuboids-fixed-max max-len)))
              (begin
                (set! previous-mm current-mm)
                (set!
                 previous-number-solutions
                 current-number-solutions)
                (set! current-mm max-len)
                (set!
                 current-number-solutions
                 (+ current-number-solutions cuboid-count))
                (if (> current-number-solutions
                       target-number-solutions)
                    (begin
                      (set! continue-loop-flag #f)
                      ))
                ))

            (if (zero? (modulo max-len status-num))
                (begin
                  (display
                   (ice-9-format:format
                    #f "completed ~:d out of ~:d : "
                    current-mm max-length))
                  (display
                   (ice-9-format:format
                    #f "so far cuboids = ~:d : "
                    current-number-solutions))
                  (display
                   (ice-9-format:format
                    #f "~a~%"
                    (timer-module:current-date-time-string)))
                  (force-output)
                  ))
            ))

        (if (equal? continue-loop-flag #t)
            (begin
              (display
               (ice-9-format:format
                #f "no results found for max-mm = ~:d~%"
                max-length))
              (display
               (ice-9-format:format
                #f "  last mm = ~:d, " previous-mm))
              (display
               (ice-9-format:format
                #f "last number of solutions = ~:d~%"
                previous-number-solutions))
              (force-output))
            (begin
              (display
               (ice-9-format:format
                #f "max m=~:d : has exactly ~:d cuboids~%"
                current-mm current-number-solutions))
              (display
               (ice-9-format:format
                #f "  for which the shortest distance "))
              (display
               (ice-9-format:format
                #f "is an integer~%"))
              (display
               (ice-9-format:format
                #f "previous max m=~:d : has exactly ~:d cuboids~%"
                previous-mm previous-number-solutions))
              (display
               (ice-9-format:format
                #f "  for which the shortest distance "))
              (display
               (ice-9-format:format
                #f "is an integer~%"))
              (force-output)
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "A spider, S, sits in one corner of "))
    (display
     (format #f "a cuboid room,~%"))
    (display
     (format #f "measuring 6 by 5 by 3, and a fly, F, "))
    (display
     (format #f "sits in the~%"))
    (display
     (format #f "opposite corner. By travelling on "))
    (display
     (format #f "the surfaces of the~%"))
    (display
     (format #f "room the shortest straight line "))
    (display
     (format #f "distance from S to F~%"))
    (display
     (format #f "is 10 and the path is shown on "))
    (display
     (format #f "the diagram.~%"))
    (newline)
    (display
     (format #f "However, there are up to three "))
    (display
     (format #f "shortest path candidates for~%"))
    (display
     (format #f "any given cuboid and the shortest "))
    (display
     (format #f "route is not always~%"))
    (display
     (format #f "integer.~%"))
    (newline)
    (display
     (format #f "By considering all cuboid rooms "))
    (display
     (format #f "with integer dimensions,~%"))
    (display
     (format #f "up to a maximum size of M by M by M, "))
    (display
     (format #f "there are exactly~%"))
    (display
     (format #f "2060 cuboids for which the shortest "))
    (display
     (format #f "distance is integer~%"))
    (display
     (format #f "when M=100, and this is the least "))
    (display
     (format #f "value of M for~%"))
    (display
     (format #f "which the number of solutions first "))
    (display
     (format #f "exceeds two thousand;~%"))
    (display
     (format #f "the number of solutions is 1975 "))
    (display
     (format #f "when M=99.~%"))
    (newline)
    (display
     (format #f "Find the least value of M such that "))
    (display
     (format #f "the number of solutions~%"))
    (display
     (format #f "first exceeds one million.~%"))
    (newline)

    (display
     (format #f "for a good understanding of the "))
    (display
     (format #f "problem see~%"))
    (display
     (format #f "https://keyzero.wordpress.com/2011/05/24/project-euler-problem-86/~%"))
    (display
     (format #f "see https://projecteuler.net/problem=86~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((max-length 1000)
          (target-solutions 2000)
          (status-num 1000))
      (begin
        (sub-main-loop
         max-length target-solutions status-num)
        ))

    (newline)
    (let ((max-length 100000)
          (target-solutions 1000000)
          (status-num 1000))
      (begin
        (sub-main-loop
         max-length target-solutions status-num)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 86 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

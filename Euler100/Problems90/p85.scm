#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 85                                     ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated July 26, 2022                                ###
;;;###                                                       ###
;;;###  updated March 8, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (calc-number-of-rectangles grid-rows grid-cols)
  (begin
    (let ((term1
           (euclidean/
            (* (1+ grid-rows) grid-rows) 2))
          (term2
           (euclidean/
            (* (1+ grid-cols) grid-cols) 2)))
      (let ((nblocks (* term1 term2)))
        (begin
          nblocks
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-calc-number-of-rectangles-1 result-hash-table)
 (begin
   (let ((sub-name "test-calc-number-of-rectangles-1")
         (test-list
          (list
           (list 1 1 1)
           (list 1 2 3)
           (list 1 3 6)
           (list 1 4 10)
           (list 2 3 18)
           (list 2 4 30)
           (list 3 3 36)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (a-list)
          (begin
            (let ((grid-rows (list-ref a-list 0))
                  (grid-cols (list-ref a-list 1))
                  (shouldbe (list-ref a-list 2)))
              (let ((result
                     (calc-number-of-rectangles
                      grid-rows grid-cols)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "grid-rows=~a, grid-cols=~a, "
                        grid-rows grid-cols))
                      (err-3
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append
                      err-1 err-2 err-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (loop-over-grids max-grid-rows max-grid-cols target-count)
  (begin
    (let ((min-count -1)
          (min-rows -1)
          (min-cols -1)
          (min-difference target-count)
          (second-count -1)
          (second-rows -1)
          (second-cols -1)
          (second-difference target-count)
          (break-flag #f))
      (begin
        (do ((grid-rows 1 (1+ grid-rows)))
            ((or (> grid-rows max-grid-rows)
                 (equal? break-flag #t)))
          (begin
            (do ((grid-cols grid-rows (1+ grid-cols)))
                ((or (> grid-cols max-grid-cols)
                     (equal? break-flag #t)))
              (begin
                (let ((this-count
                       (calc-number-of-rectangles
                        grid-rows grid-cols)))
                  (let ((this-difference
                         (abs (- target-count this-count))))
                    (begin
                      (cond
                       ((< this-difference min-difference)
                        (begin
                          (set! second-count min-count)
                          (set! second-difference min-difference)
                          (set! second-rows min-rows)
                          (set! second-cols min-cols)

                          (set! min-count this-count)
                          (set! min-difference this-difference)
                          (set! min-rows grid-rows)
                          (set! min-cols grid-cols)
                          ))
                       ((< this-difference second-difference)
                        (begin
                          (set! second-count this-count)
                          (set! second-difference this-difference)
                          (set! second-rows grid-rows)
                          (set! second-cols grid-cols)
                          )))

                      (if (= this-difference 0)
                          (begin
                            (set! break-flag #t)
                            ))
                      )))
                ))
            ))

        (list
         min-count min-rows min-cols min-difference
         second-count second-rows second-cols second-difference)
        ))
    ))

;;;#############################################################
;;;#############################################################
;;;  (list min-count min-rows min-cols min-difference)
(define (sub-main-loop
         max-grid-rows max-grid-cols target-count)
  (begin
    (let ((best-count
           (loop-over-grids
            max-grid-rows max-grid-cols target-count)))
      (let ((min-count (list-ref best-count 0))
            (min-rows (list-ref best-count 1))
            (min-cols (list-ref best-count 2))
            (min-difference (list-ref best-count 3))
            (second-count (list-ref best-count 4))
            (second-rows (list-ref best-count 5))
            (second-cols (list-ref best-count 6))
            (second-difference (list-ref best-count 7)))
        (begin
          (display
           (ice-9-format:format
            #f "grid [~:d, ~:d], area = ~:d~%"
            min-rows min-cols (* min-rows min-cols)))
          (display
           (ice-9-format:format
            #f "  contains ~:d rectangles~%" min-count))
          (display
           (ice-9-format:format
            #f "  difference of ~:d from ~:d~%"
            min-difference target-count))
          (display
           (ice-9-format:format
            #f "next grid [~:d, ~:d], area = ~:d~%"
            second-rows second-cols (* second-rows second-cols)))
          (display
           (ice-9-format:format
            #f "  contains ~:d rectangles~%"
            second-count))
          (display
           (ice-9-format:format
            #f "  difference of ~:d from ~:d~%"
            second-difference target-count))

          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "By counting carefully it can be "))
    (display
     (format #f "seen that a rectangular~%"))
    (display
     (format #f "grid measuring 3 by 2 contains "))
    (display
     (format #f "eighteen rectangles:~%"))
    (newline)
    (display
     (format #f "Although there exists no rectangular "))
    (display
     (format #f "grid that contains~%"))
    (display
     (format #f "exactly two million rectangles, find "))
    (display
     (format #f "the area of the~%"))
    (display
     (format #f "grid with the nearest solution.~%"))
    (newline)
    (display
     (format #f "This problem can be solved by two "))
    (display
     (format #f "independent sums, one~%"))
    (display
     (format #f "over the min range and one over the "))
    (display
     (format #f "maximum range.~%"))
    (display
     (format #f "Over the min range, one needs to "))
    (display
     (format #f "sum over the~%"))
    (display
     (format #f "size of the blocks as well as the "))
    (display
     (format #f "position of the block.~%"))
    (display
     (format #f "Sum_size=1_to_n(Sum_pos=1_to_(n-size+1) "))
    (display
     (format #f "(1))~%"))
    (display
     (format #f "= Sum_size=1_to_n((n-size+1)) = (n+1)*n/2. "))
    (display
     (format #f "The sum over~%"))
    (display
     (format #f "the maximum is similar.~%"))
    (display
     (format #f "The number of rectangles in an nxm "))
    (display
     (format #f "grid is~%"))
    (display
     (format #f "(n+1)*n/2 * (m+1)*m/2~%"))
    (display
     (format #f "see https://projecteuler.net/problem=85~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((max-grid-rows 10)
          (max-grid-cols 10)
          (target-count 100))
      (begin
        (sub-main-loop
         max-grid-rows max-grid-cols target-count)
        ))

    (newline)
    (let ((max-grid-rows 100)
          (max-grid-cols 100)
          (target-count 2000000))
      (begin
        (sub-main-loop
         max-grid-rows max-grid-cols target-count)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 85 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

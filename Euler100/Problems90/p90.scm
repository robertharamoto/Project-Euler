#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 90                                     ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated July 26, 2022                                ###
;;;###                                                       ###
;;;###  updated March 8, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (are-numbers-in-separate-lists? aa bb cube-list-1 cube-list-2)
  (begin
    (cond
     ((and
       (not (equal? (member aa cube-list-1) #f))
       (not (equal? (member bb cube-list-2) #f)))
      (begin
        #t
        ))
     ((and
       (not (equal? (member aa cube-list-2) #f))
       (not (equal? (member bb cube-list-1) #f)))
      (begin
        #t
        ))
     (else
      (begin
        #f
        )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-are-numbers-in-separate-lists-1 result-hash-table)
 (begin
   (let ((sub-name "test-are-numbers-in-separate-lists-1")
         (test-list
          (list
           (list 1 4 (list 1 2 3) (list 4 5 6) #t)
           (list 4 1 (list 1 2 3) (list 4 5 6) #t)
           (list 2 5 (list 1 2 3) (list 4 5 6) #t)
           (list 3 6 (list 1 2 3) (list 4 5 6) #t)
           (list 1 2 (list 1 2 3) (list 4 5 6) #f)
           (list 3 5 (list 1 2 3) (list 4 5 6) #t)
           (list 4 5 (list 1 2 3) (list 4 5 6) #f)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((aa (list-ref alist 0))
                  (bb (list-ref alist 1))
                  (llist1 (list-ref alist 2))
                  (llist2 (list-ref alist 3))
                  (shouldbe (list-ref alist 4)))
              (let ((result
                     (are-numbers-in-separate-lists? aa bb llist1 llist2)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "aa=~a, bb=~a, " aa bb))
                      (err-3
                       (format
                        #f "llist1=~a, llist2=~a, "
                        llist1 llist2))
                      (err-4
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append
                      err-1 err-2 err-3 err-4)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; digits on one cube { 0, 1, 2, 3, 4, 6, 8 }
;;; digits on the second { 1, 4, 9, 6, 5, 6, 9, 4, 1}
;;; 01 -> 0 and 1 must be separate sets
;;; 04 -> 0 and 4 must be in separate sets
;;; 09 -> 0 and 9 or 6 must be in separate sets
;;; 16 -> 1 and 6 must be separate
;;; 25 -> 2 and 5 must be separate
;;; 36 -> 3 and 6 must be separate
;;; 49 -> 4 and 9 must be separate
;;; 64 -> 4 and 6 must be separate
;;; 81 -> 1 and 8 must be separate
(define (is-valid-list-pair? cube-list-1 cube-list-2)
  (begin
    (if (and (list? cube-list-1)
             (>= (length cube-list-1) 6)
             (list? cube-list-2)
             (>= (length cube-list-2) 6))
        (begin
        ;;; criteria 1 - each die has a different digit
          (cond
           ((equal?
             (are-numbers-in-separate-lists?
              0 1 cube-list-1 cube-list-2)
             #f)
            (begin
              #f
              ))
           ((equal?
             (are-numbers-in-separate-lists?
              0 4 cube-list-1 cube-list-2)
             #f)
            (begin
              #f
              ))
           ((and
             (equal?
              (are-numbers-in-separate-lists?
               0 9 cube-list-1 cube-list-2)
              #f)
             (equal?
              (are-numbers-in-separate-lists?
               0 6 cube-list-1 cube-list-2)
              #f))
            (begin
              #f
              ))
           ((and
             (equal?
              (are-numbers-in-separate-lists?
               1 6 cube-list-1 cube-list-2)
              #f)
             (equal?
              (are-numbers-in-separate-lists?
               1 9 cube-list-1 cube-list-2)
              #f))
            (begin
              #f
              ))
           ((equal?
             (are-numbers-in-separate-lists?
              2 5 cube-list-1 cube-list-2)
             #f)
            (begin
              #f
              ))
           ((and
             (equal?
              (are-numbers-in-separate-lists?
               3 6 cube-list-1 cube-list-2)
              #f)
             (equal?
              (are-numbers-in-separate-lists?
               3 9 cube-list-1 cube-list-2)
              #f))
            (begin
              #f
              ))
           ((and
             (equal?
              (are-numbers-in-separate-lists?
               4 9 cube-list-1 cube-list-2)
              #f)
             (equal?
              (are-numbers-in-separate-lists?
               4 6 cube-list-1 cube-list-2)
              #f))
            (begin
              #f
              ))
           ((and
             (equal?
              (are-numbers-in-separate-lists?
               6 4 cube-list-1 cube-list-2)
              #f)
             (equal?
              (are-numbers-in-separate-lists?
               9 4 cube-list-1 cube-list-2)
              #f))
            (begin
              #f
              ))
           ((equal?
             (are-numbers-in-separate-lists?
              8 1 cube-list-1 cube-list-2)
             #f)
            (begin
              #f
              ))
           (else
            (begin
            ;;; if we reach this point then everything is ok
              #t
              ))))
        (begin
          #f
          ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-is-valid-list-pair-1 result-hash-table)
 (begin
   (let ((sub-name "test-is-valid-list-pair-1")
         (test-list
          (list
           (list (list 0 5 6 7 8 9) (list 1 2 3 4 8 9) #t)
           (list (list 0 5 6 7 8 9) (list 1 2 3 2 8 9) #f)
           (list (list 0 5 6 7 8 9) (list 1 2 3 5 8 9) #f)
           (list (list 0 5 6 7 8 9) (list 7 2 3 4 8 9) #f)
           (list (list 0 5 6 7 8 4) (list 1 2 3 4 8 9) #t)
           (list (list 0 5 3 7 8 4) (list 1 2 3 4 8 9) #f)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((llist-1 (list-ref alist 0))
                  (llist-2 (list-ref alist 1))
                  (shouldbe (list-ref alist 2)))
              (let ((result
                     (is-valid-list-pair? llist-1 llist-2)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "llist-1=~a, llist-2=~a, "
                        llist-1 llist-2))
                      (err-3
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append
                      err-1 err-2 err-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (count-label-pairs-lists debug-flag)
  (define (local-recursive-loop
           depth max-depth start-1 end-1 start-2 end-2
           llist-1 llist-2 acc-list)
    (begin
      (if (>= depth max-depth)
          (begin
            (if (is-valid-list-pair? llist-1 llist-2)
                (begin
                  (let ((s1-list (sort llist-1 <))
                        (s2-list (sort llist-2 <)))
                    (let ((next1-list (list s1-list s2-list))
                          (next2-list (list s2-list s1-list)))
                      (begin
                        (if (and
                             (equal? (member next1-list acc-list) #f)
                             (equal? (member next2-list acc-list) #f))
                            (begin
                              (let ((next-acc-list
                                     (cons next1-list acc-list)))
                                (begin
                                  (if (equal? debug-flag #t)
                                      (begin
                                        (display
                                         (ice-9-format:format
                                          #f "debug found pair ~a, ~a : "
                                          s1-list s2-list))
                                        (display
                                         (ice-9-format:format
                                          #f "number so far = ~:d~%"
                                          (length next-acc-list)))
                                        (force-output)
                                        ))
                                  next-acc-list
                                  )))
                            (begin
                              acc-list
                              ))
                        ))))
                (begin
                  acc-list
                  )))
          (begin
            (do ((ii start-1 (1+ ii)))
                ((> ii end-1))
              (begin
                (if (equal? (member ii llist-1) #f)
                    (begin
                      (let ((next-list-1 (cons ii llist-1)))
                        (begin
                          (do ((jj start-2 (1+ jj)))
                              ((> jj end-2))
                            (begin
                              (if (equal? (member jj llist-2) #f)
                                  (begin
                                    (let ((next-list-2 (cons jj llist-2)))
                                      (begin
                                        (let ((next-acc-list
                                               (local-recursive-loop
                                                (1+ depth) max-depth
                                                (1+ ii) end-1
                                                (1+ jj) end-2
                                                next-list-1 next-list-2
                                                acc-list)))
                                          (begin
                                            (set! acc-list next-acc-list)
                                            ))
                                        ))
                                    ))
                              ))
                          ))
                      ))
                ))
            acc-list
            ))
      ))
  (begin
    (let ((total-acc-lists
           (local-recursive-loop
            0 6 0 9 0 9
            (list) (list) (list))))
      (begin
        (length total-acc-lists)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop debug-flag)
  (begin
    (let ((count
           (count-label-pairs-lists debug-flag)))
      (begin
        (display
         (ice-9-format:format
          #f "Number of distinct arrangements of the "))
        (display
         (ice-9-format:format
          #f "two cubes that allow~%"))
        (display
         (ice-9-format:format
          #f "for all the square numbers (less "))
        (display
         (ice-9-format:format
          #f "than 100,) to be displayed = ~:d~%"
          count))
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Each of the six faces on a cube "))
    (display
     (format #f "has a different digit~%"))
    (display
     (format #f "(0 to 9) written on it; the same "))
    (display
     (format #f "is done to a~%"))
    (display
     (format #f "second cube. By placing the two "))
    (display
     (format #f "cubes side-by-side in~%"))
    (display
     (format #f "different positions we can form "))
    (display
     (format #f "a variety of~%"))
    (display
     (format #f "2-digit numbers.~%"))
    (newline)
    (display
     (format #f "For example, the square number 64 "))
    (display
     (format #f "could be formed:~%"))
    (newline)
    (display
     (format #f "In fact, by carefully choosing "))
    (display
     (format #f "the digits on both~%"))
    (display
     (format #f "cubes it is possible to display "))
    (display
     (format #f "all of the square~%"))
    (display
     (format #f "numbers below one-hundred: 01, 04, "))
    (display
     (format #f "09, 16, 25, 36,~%"))
    (display
     (format #f "49, 64, and 81.~%"))
    (newline)
    (display
     (format #f "For example, one way this can be "))
    (display
     (format #f "achieved is by placing~%"))
    (display
     (format #f "{0, 5, 6, 7, 8, 9} on one cube and~%"))
    (display
     (format #f "{1, 2, 3, 4, 8, 9} on the other cube.~%"))
    (newline)
    (display
     (format #f "However, for this problem we shall "))
    (display
     (format #f "allow the 6 or 9~%"))
    (display
     (format #f "to be turned upside-down so that an "))
    (display
     (format #f "arrangement like~%"))
    (display
     (format #f "{0, 5, 6, 7, 8, 9} and {1, 2, 3, 4, 6, 7} "))
    (display
     (format #f "allows for~%"))
    (display
     (format #f "all nine square numbers to be displayed; "))
    (display
     (format #f "otherwise it~%"))
    (display
     (format #f "would be impossible to obtain 09.~%"))
    (newline)
    (display
     (format #f "In determining a distinct arrangement "))
    (display
     (format #f "we are interested in~%"))
    (display
     (format #f "the digits on each cube, not the order.~%"))
    (newline)
    (display
     (format #f "  {1, 2, 3, 4, 5, 6} is equivalent to "))
    (display
     (format #f "{3, 6, 4, 1, 2, 5}~%"))
    (display
     (format #f "  {1, 2, 3, 4, 5, 6} is distinct "))
    (display
     (format #f "from {1, 2, 3, 4, 5, 9}~%"))
    (newline)
    (display
     (format #f "But because we are allowing 6 "))
    (display
     (format #f "and 9 to be~%"))
    (display
     (format #f "reversed, the two distinct sets "))
    (display
     (format #f "in the last example~%"))
    (display
     (format #f "both represent the extended set "))
    (display
     (format #f "{1, 2, 3, 4, 5, 6, 9}~%"))
    (display
     (format #f "for the purpose of forming "))
    (display
     (format #f "2-digit numbers.~%"))
    (newline)
    (display
     (format #f "How many distinct arrangements of "))
    (display
     (format #f "the two cubes allow~%"))
    (display
     (format #f "for all of the square numbers to "))
    (display
     (format #f "be displayed?~%"))
    (newline)
    (display
     (format #f "The key to solving this problem is "))
    (display
     (format #f "to realize that there~%"))
    (display
     (format #f "are just Choose(10, 6) = 210 possibilities "))
    (display
     (format #f "for each die,~%"))
    (display
     (format #f "which means that the program does not "))
    (display
     (format #f "have to work~%"))
    (display
     (format #f "as hard.~%"))
    (display
     (format #f "see https://projecteuler.net/problem=90~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((debug-flag #f))
      (begin
        (sub-main-loop debug-flag)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 90 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

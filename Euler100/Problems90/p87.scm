#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 87                                     ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated July 26, 2022                                ###
;;;###                                                       ###
;;;###  updated March 8, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
;;; make a list of primes less than or equal to n
;;; sieve of eratosthenes method
(define (make-prime-list max-num)
  (begin
    (let ((intermediate-array (make-array 0 (1+ max-num)))
          (result-list (list)))
      (begin
        (do ((ii 0 (1+ ii)))
            ((> ii max-num))
          (begin
            (array-set! intermediate-array ii ii)
            ))

        (do ((ii 2 (1+ ii)))
            ((> ii max-num))
          (begin
            (let ((ii-num
                   (array-ref intermediate-array ii)))
              (begin
                (if (= ii-num ii)
                    (begin
                      (set! result-list (cons ii result-list))
                      ))

                (do ((jj (+ ii ii) (+ jj ii)))
                    ((> jj max-num))
                  (begin
                    (array-set! intermediate-array -1 jj)
                    ))
                ))
            ))
        (reverse result-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-make-prime-list-1 result-hash-table)
 (begin
   (let ((sub-name "test-make-prime-list-1")
         (test-list
          (list
           (list 2 (list 2))
           (list 3 (list 2 3))
           (list 4 (list 2 3))
           (list 5 (list 2 3 5))
           (list 6 (list 2 3 5))
           (list 7 (list 2 3 5 7))
           (list 8 (list 2 3 5 7))
           (list 9 (list 2 3 5 7))
           (list 10 (list 2 3 5 7))
           (list 11 (list 2 3 5 7 11))
           (list 13 (list 2 3 5 7 11 13))
           (list 17 (list 2 3 5 7 11 13 17))
           (list 19 (list 2 3 5 7 11 13 17 19))
           (list 23 (list 2 3 5 7 11 13 17 19 23))
           (list 31 (list 2 3 5 7 11 13 17 19 23 29 31))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((test-num (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result (make-prime-list test-num)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : test-num=~a, "
                        sub-name test-label-index test-num))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; define a macro to simplify code
(define-syntax process-squares-sublevel-2-macro
  (syntax-rules ()
    ((process-squares-sublevel-2-macro
      result-htable
      debug-flag
      counter this-num
      prime-2 prime-3 prime-4)
     (begin
       (let ((rflag
              (hash-ref result-htable this-num #f)))
         (begin
           (if (equal? rflag #f)
               (begin
                 (set! counter (1+ counter))
                 (hash-set! result-htable this-num 1)
                 (if (equal? debug-flag #t)
                     (begin
                       (display
                        (ice-9-format:format
                         #f "  ~:d = ~:d^2 + ~:d^3 + ~:d^4~%"
                         this-num prime-2 prime-3 prime-4))
                       (force-output)
                       ))
                 ))
           ))
       ))
    ))


;;;#############################################################
;;;#############################################################
;;; define a macro to simplify code
(define-syntax process-squares-level-2-macro
  (syntax-rules ()
    ((process-squares-level-2-macro
      parray plen target-number
      p3 p4 prime-3 prime-4
      counter result-htable debug-flag)
     (begin
       (let ((continue-loop-2-flag #t))
         (begin
           (do ((ii-2 0 (1+ ii-2)))
               ((or (>= ii-2 plen)
                    (equal? continue-loop-2-flag #f)))
             (begin
               (let ((prime-2 (array-ref parray ii-2)))
                 (let ((p2 (* prime-2 prime-2)))
                   (let ((this-num (+ p2 p3 p4)))
                     (begin
                       (if (< this-num target-number)
                           (begin
                             (process-squares-sublevel-2-macro
                              result-htable
                              debug-flag
                              counter this-num
                              prime-2 prime-3 prime-4))
                           (begin
                             (set! continue-loop-2-flag #f)
                             ))
                       ))
                   ))
               ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
;;; define a macro to simplify code
(define-syntax process-cubes-level-3-macro
  (syntax-rules ()
    ((process-cubes-level-3-macro
      parray plen target-number
      p4 prime-4 counter result-htable debug-flag)
     (begin
       (let ((continue-loop-3-flag #t))
         (begin
           (do ((ii-3 0 (1+ ii-3)))
               ((or (>= ii-3 plen)
                    (equal? continue-loop-3-flag #f)))
             (begin
               (let ((prime-3 (array-ref parray ii-3)))
                 (let ((p3 (* prime-3 prime-3 prime-3)))
                   (begin
                     (if (> (+ p3 p4) target-number)
                         (begin
                           (set! continue-loop-3-flag #f))
                         (begin
                           (process-squares-level-2-macro
                            parray plen target-number
                            p3 p4 prime-3 prime-4
                            counter result-htable debug-flag)
                           ))
                     )))
               ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
;;; define a macro to simplify code
(define-syntax process-quartics-level-4-macro
  (syntax-rules ()
    ((process-quartics-level-4-macro
      parray plen target-number counter result-htable debug-flag)
     (begin
       (let ((continue-loop-4-flag #t))
         (begin
           (do ((ii-4 0 (1+ ii-4)))
               ((or (>= ii-4 plen)
                    (equal? continue-loop-4-flag #f)))
             (begin
               (let ((prime-4 (array-ref parray ii-4)))
                 (let ((p4 (* prime-4 prime-4 prime-4 prime-4)))
                   (begin
                     (if (> p4 target-number)
                         (begin
                           (set! continue-loop-4-flag #f))
                         (begin
                           (process-cubes-level-3-macro
                            parray plen target-number
                            p4 prime-4 counter result-htable
                            debug-flag)
                           ))
                     )))
               ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop
         max-prime target-number debug-flag)
  (begin
    (let ((prime-list (make-prime-list max-prime))
          (result-htable (make-hash-table 10000))
          (counter 0))
      (let ((plen (length prime-list))
            (parray (list->array 1 prime-list)))
        (begin
          (process-quartics-level-4-macro
           parray plen target-number counter result-htable debug-flag)

          (display
           (ice-9-format:format
            #f "there are ~:d numbers less than ~:d~%"
            counter target-number))
          (display
           (ice-9-format:format
            #f "  that are expressible as the sum of a "))
          (display
           (format #f "prime square,~%"))
          (display
           (format #f "prime cube, and a prime fourth power.~%"))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "The smallest number expressible "))
    (display
     (format #f "as the sum of~%"))
    (display
     (format #f "a prime square, prime cube, and "))
    (display
     (format #f "prime fourth power~%"))
    (display
     (format #f "is 28. In fact, there are exactly "))
    (display
     (format #f "four numbers below~%"))
    (display
     (format #f "fifty that can be expressed in "))
    (display
     (format #f "such a way:~%"))
    (newline)
    (display
     (format #f "  28 = 2^2 + 2^3 + 2^4~%"))
    (display
     (format #f "  33 = 3^2 + 2^3 + 2^4~%"))
    (display
     (format #f "  49 = 5^2 + 2^3 + 2^4~%"))
    (display
     (format #f "  47 = 2^2 + 3^3 + 2^4~%"))
    (newline)
    (display
     (format #f "How many numbers below fifty million "))
    (display
     (format #f "can be expressed~%"))
    (display
     (format #f "as the sum of a prime square, prime "))
    (display
     (format #f "cube, and~%"))
    (display
     (format #f "prime fourth power?~%"))
    (display
     (format #f "see https://projecteuler.net/problem=87~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((max-prime 20)
          (target-number 50)
          (debug-flag #t))
      (begin
        (sub-main-loop max-prime target-number debug-flag)
        ))

    (newline)

    ;;; note: sqrt(50 million) is about 7,000
    ;;; so only need primes less than 10,000
    (let ((max-prime 10000)
          (target-number 50000000)
          (status-num 100)
          (debug-flag #f))
      (begin
        (sub-main-loop
         max-prime target-number debug-flag)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 87 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

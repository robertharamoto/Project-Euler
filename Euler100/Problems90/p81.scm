#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 81                                     ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated July 26, 2022                                ###
;;;###                                                       ###
;;;###  updated March 8, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 rdelim for reading in delimited files
(use-modules ((ice-9 rdelim)
              :renamer (symbol-prefix-proc 'ice-9-rdelim:)))

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
;;; define a macro to simplify code
(define-syntax process-row-col-macro
  (syntax-rules ()
    ((process-row-col-macro
      local-array ii jj end-rows-index end-cols-index)
     (begin
       (if (not (and (= ii end-rows-index)
                     (= jj end-cols-index)))
           (begin
             (let ((this-elem (array-ref local-array ii jj))
                   (min-elem 0))
               (begin
                 (cond
                  ((equal? jj end-cols-index)
                   (begin
                     (let ((prev-elem1
                            (array-ref local-array (+ ii 1) jj)))
                       (begin
                         (set! min-elem prev-elem1)
                         ))
                     ))
                  ((equal? ii end-rows-index)
                   (begin
                     (let ((prev-elem2
                            (array-ref local-array ii (+ jj 1))))
                       (begin
                         (set! min-elem prev-elem2)
                         ))
                     ))
                  (else
                   (begin
                     (let ((prev-elem1
                            (array-ref local-array (+ ii 1) jj))
                           (prev-elem2
                            (array-ref local-array ii (+ jj 1))))
                       (begin
                         (set!
                          min-elem (min prev-elem1 prev-elem2))
                         ))
                     )))
                 (array-set!
                  local-array (+ min-elem this-elem) ii jj)
                 ))
             ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (dynamic-method array-2d max-rows max-cols)
  (begin
    (let ((local-array (make-array 0 max-rows max-cols))
          (end-rows-index (- max-rows 1))
          (end-cols-index (- max-cols 1)))
      (begin
        (array-copy! array-2d local-array)

        (cond
         ((<= max-rows 1)
          (begin
            -1
            ))
         (else
          (begin
            (do ((ii end-rows-index (- ii 1)))
                ((< ii 0))
              (begin
                (do ((jj end-cols-index (- jj 1)))
                    ((< jj 0))
                  (begin
                    (process-row-col-macro
                     local-array ii jj
                     end-rows-index end-cols-index)
                    ))
                ))

            (array-ref local-array 0 0)
            )))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-dynamic-method-1 result-hash-table)
 (begin
   (let ((sub-name "test-dynamic-method-1")
         (test-list
          (list
           (list (list (list 1 2 3 4)
                       (list 3 5 9 8)
                       (list 7 11 13 19)
                       (list 44 33 17 8))
                 45)
           (list (list (list 131 673 234 103 18)
                       (list 201 96 342 965 150)
                       (list 630 803 746 422 111)
                       (list 537 699 497 121 956)
                       (list 805 732 524 37 331))
                 2427)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((input-list-list (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((array-2d (list->array 2 input-list-list))
                    (max-rows (length input-list-list))
                    (max-cols (length input-list-list)))
                (let ((result
                       (dynamic-method
                        array-2d max-rows max-cols)))
                  (let ((err-1
                         (format
                          #f "~a : error (~a) : input-list-list=~a, "
                          sub-name test-label-index input-list-list))
                        (err-2
                         (format
                          #f "shouldbe=~a, result=~a~%"
                          shouldbe result)))
                    (begin
                      (unittest2:assert?
                       (equal? shouldbe result)
                       sub-name
                       (string-append err-1 err-2)
                       result-hash-table)
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (display-2d-array this-array max-row max-col)
  (begin
    (cond
     ((not (array? this-array))
      (begin
        (display
         (format
          #f "display-array error: expecting array~%"
          this-array))
        (display
         (format
          #f "instead received ~a~%"
          this-array))
        (quit)))
     (else
      (begin
        (do ((ii-row 0 (1+ ii-row)))
            ((>= ii-row max-row))
          (begin
            (do ((ii-col 0 (1+ ii-col)))
                ((>= ii-col max-col))
              (begin
                (display
                 (ice-9-format:format
                  #f "~4:d " (array-ref this-array ii-row ii-col)))
                ))
            (newline)
            ))
        )))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax process-file-line-macro
  (syntax-rules ()
    ((process-file-line-macro
      line results-list-list)
     (begin
       (let ((this-string-list
              (string-split
               (string-trim-both line) #\,)))
         (begin
           (let ((this-list
                  (map
                   (lambda (this-string)
                     (begin
                       (if (and
                            (string? this-string)
                            (not
                             (equal?
                              (string->number this-string) #f)))
                           (begin
                             (string->number this-string))
                           (begin
                             -1
                             ))
                       )) this-string-list)
                  ))
             (begin
               (set!
                results-list-list
                (cons this-list results-list-list))
               ))
           ))
       ))
    ))


;;;#############################################################
;;;#############################################################
;;; returns a list of lists
(define (read-in-file fname)
  (begin
    (let ((results-list-list (list))
          (counter 0))
      (begin
        (if (file-exists? fname)
            (begin
              (with-input-from-file fname
                (lambda ()
                  (begin
                    (do ((line
                          (ice-9-rdelim:read-delimited "\r\n")
                          (ice-9-rdelim:read-delimited "\r\n")))
                        ((eof-object? line))
                      (begin
                        (if (and (not (eof-object? line))
                                 (> (string-length line) 0))
                            (begin
                              (process-file-line-macro
                               line results-list-list)
                              ))
                        ))
                    )))

              (reverse results-list-list))
            (begin
              (list)
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop
         tarray max-rows max-cols debug-flag)
  (begin
    (let ((path-sum
           (dynamic-method
            tarray max-rows max-cols)))
      (begin
        (if (equal? debug-flag #t)
            (begin
              (display-2d-array
               tarray max-rows max-cols)
              ))

        (display
         (ice-9-format:format
          #f "minimal path sum = ~:d~%"
          path-sum))
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "In the 5 by 5 matrix below, the "))
    (display
     (format #f "minimal path sum~%"))
    (display
     (format #f "from the top left to the bottom "))
    (display
     (format #f "right, by only~%"))
    (display
     (format #f "moving to the right and down, is "))
    (display
     (format #f "indicated in bold~%"))
    (display
     (format #f "red and is equal to 2427.~%"))
    (newline)
    (display
     (format #f "  131  673  234  103  18~%"))
    (display
     (format #f "  201  96   342  965  150~%"))
    (display
     (format #f "  630  803  746  422  111~%"))
    (display
     (format #f "  537  699  497  121  956~%"))
    (display
     (format #f "  805  732  524  37   331~%"))
    (newline)
    (display
     (format #f "Find the minimal path sum, in "))
    (display
     (format #f "matrix.txt (right click~%"))
    (display
     (format #f "and 'Save Link/Target As...'), a "))
    (display
     (format #f "31K text file~%"))
    (display
     (format #f "containing a 80 by 80 matrix, from "))
    (display
     (format #f "the top left to~%"))
    (display
     (format #f "the bottom right by only moving "))
    (display
     (format #f "right and down.~%"))
    (newline)
    (display
     (format #f "The solution method can be found at~%"))
    (display
     (format #f "https://euler.stephan-brumme.com/81/~%"))
    (display
     (format #f "see https://projecteuler.net/problem=81~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((tlist
           (list
            (list 131 673 234 103 18)
            (list 201 96 342 965 150)
            (list 630 803 746 422 111)
            (list 537 699 497 121 956)
            (list 805 732 524 37 331)))
          (max-rows 5)
          (max-cols 5)
          (debug-flag #t))
      (let ((tarray (list->array 2 tlist)))
        (begin
          (sub-main-loop
           tarray max-rows max-cols debug-flag)
          )))

    (newline)
    (let ((filename "matrix.txt"))
      (let ((tlist (read-in-file filename)))
        (let ((max-rows (length tlist))
              (max-cols (length (car tlist)))
              (debug-flag #f))
          (let ((tarray (list->array 2 tlist)))
            (begin
              (display
               (format
                #f "read in file ~a, rows = ~a, "
                filename max-rows))
              (display
               (format
                #f "cols = ~a~%"
                max-cols))
              (force-output)
              (sub-main-loop
               tarray max-rows max-cols debug-flag)
              ))
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 81 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

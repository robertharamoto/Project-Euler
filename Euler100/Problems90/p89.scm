#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 89                                     ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated July 26, 2022                                ###
;;;###                                                       ###
;;;###  updated March 8, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 rdelim for reading in delimited files
(use-modules ((ice-9 rdelim)
              :renamer (symbol-prefix-proc 'ice-9-rdelim:)))

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
;;; assume input-string is in lower-case
(define (populate-roman-char-hash-table! roman-char-htable)
  (begin
    (let ((rlist
           (list
            (list #\i 1) (list #\v 5)
            (list #\x 10) (list #\l 50)
            (list #\c 100) (list #\d 500)
            (list #\m 1000))))
      (begin
        (hash-clear! roman-char-htable)

        (for-each
         (lambda (a-list)
           (begin
             (let ((this-char (list-ref a-list 0))
                   (this-value (list-ref a-list 1)))
               (begin
                 (hash-set! roman-char-htable this-char this-value)
                 ))
             )) rlist)
        ))
    ))

;;;#############################################################
;;;#############################################################
;;; assume input-string is in lower-case
(define (roman-string-to-number input-string roman-char-htable)
  (begin
    (let ((input-length (string-length input-string))
          (char-list (string->list input-string))
          (result-num 0)
          (previous-num -1))
      (begin
        (do ((ii 0 (1+ ii)))
            ((>= ii input-length))
          (begin
            (let ((this-char
                   (list-ref char-list ii)))
              (let ((this-num
                     (hash-ref roman-char-htable this-char -1)))
                (begin
                  (if (< this-num 0)
                      (begin
                        (display
                         (format
                          #f "roman-string-to-number(~a) error : "
                          input-string))
                        (display
                         (format
                          #f "invalid character ~a~%" this-char))
                        (display
                         (format #f "quitting...~%"))
                        (force-output)
                        (quit)
                        ))

                  (if (< previous-num 0)
                      (begin
                        (set! previous-num this-num))
                      (begin
                        (if (< previous-num this-num)
                            (begin
                              ;;; then we have a subtraction pair, like "IV" or "IX"
                              (let ((sub-num (- this-num previous-num)))
                                (begin
                                  (set! result-num (+ result-num sub-num))
                                  (set! previous-num -1)
                                  )))
                            (begin
                              (set! result-num (+ result-num previous-num))
                              (set! previous-num this-num)
                              ))
                        ))
                  )))
            ))
        (if (> previous-num 0)
            (begin
              (set! result-num (+ result-num previous-num))
              ))

        result-num
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-roman-string-to-number-1 result-hash-table)
 (begin
   (let ((sub-name "test-roman-string-to-number-1")
         (test-list
          (list
           (list "i" 1) (list "ii" 2) (list "iii" 3)
           (list "iv" 4) (list "v" 5) (list "vi" 6)
           (list "vii" 7) (list "viii" 8) (list "ix" 9)
           (list "x" 10) (list "xi" 11) (list "xii" 12)
           (list "xiii" 13) (list "xiv" 14) (list "xv" 15)
           (list "xvi" 16) (list "xvii" 17) (list "xviii" 18)
           (list "xix" 19) (list "xx" 20)
           (list "xxix" 29) (list "il" 49) (list "lil" 99)
           (list "ic" 99) (list "ci" 101) (list "cix" 109)
           (list "cxl" 140) (list "cil" 149) (list "lc" 50)
           (list "cxc" 190) (list "cic" 199) (list "d" 500)
           (list "id" 499) (list "m" 1000) (list "mmx" 2010)
           (list "iiiiiiiiiiiiiiii" 16) (list "viiiiiiiiiii" 16)
           (list "vviiiiii" 16) (list "xiiiiii" 16)
           (list "vvvi" 16) (list "xvi" 16)
           ))
         (roman-char-htable (make-hash-table 10))
         (test-label-index 0))
     (begin
       (populate-roman-char-hash-table! roman-char-htable)

       (for-each
        (lambda (alist)
          (begin
            (let ((rstring (string-downcase (list-ref alist 0)))
                  (shouldbe (list-ref alist 1)))
              (let ((result
                     (roman-string-to-number
                      rstring roman-char-htable)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : rstring=~s, "
                        sub-name test-label-index rstring))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; use most efficient representation
(define (number-to-roman-string input-num)
  (begin
    (let ((tmp-num input-num)
          (result-string ""))
      (begin
        (while
         (> tmp-num 0)
         (begin
           (cond
            ((>= tmp-num 1000)
             (begin
               (set!
                result-string
                (string-append result-string "m"))
               (set! tmp-num (- tmp-num 1000))
               ))
            ((and (< tmp-num 1000)
                  (>= tmp-num 900))
             (begin
                ;;; should use a subtractive pair CM if it's 900 <= n < 1000
               (set!
                result-string
                (string-append result-string "cm"))
               (set! tmp-num (- tmp-num 900))
               ))
            ((>= tmp-num 500)
             (begin
               (set!
                result-string
                (string-append result-string "d"))
               (set! tmp-num (- tmp-num 500))
               ))
            ((and (< tmp-num 500)
                  (>= tmp-num 400))
             (begin
                ;;; should use a subtractive pair CD if it's 400 <= n < 500
               (set!
                result-string
                (string-append result-string "cd"))
               (set! tmp-num (- tmp-num 400))
               ))
            ((>= tmp-num 100)
             (begin
               (set!
                result-string
                (string-append result-string "c"))
               (set! tmp-num (- tmp-num 100))
               ))
            ((and (< tmp-num 100)
                  (>= tmp-num 90))
             (begin
                ;;; should use a subtractive pair XC if it's 90 <= n < 100
               (set!
                result-string
                (string-append result-string "xc"))
               (set! tmp-num (- tmp-num 90))
               ))
            ((>= tmp-num 50)
             (begin
               (set!
                result-string
                (string-append result-string "l"))
               (set! tmp-num (- tmp-num 50))
               ))
            ((and (< tmp-num 50)
                  (>= tmp-num 40))
             (begin
                ;;; should use a subtractive pair XL if it's 40 <= n < 50
               (set!
                result-string
                (string-append result-string "xl"))
               (set! tmp-num (- tmp-num 40))
               ))
            ((>= tmp-num 10)
             (begin
               (set!
                result-string
                (string-append result-string "x"))
               (set! tmp-num (- tmp-num 10))
               ))
            ((= tmp-num 9)
             (begin
                ;;; should use a subtractive pair IX if it's 9 = n
               (set!
                result-string
                (string-append result-string "ix"))
               (set! tmp-num (- tmp-num 9))
               ))
            ((>= tmp-num 5)
             (begin
               (set!
                result-string
                (string-append result-string "v"))
               (set! tmp-num (- tmp-num 5))
               ))
            ((= tmp-num 4)
             (begin
                ;;; should use a subtractive pair IV if it's 4 = n
               (set!
                result-string
                (string-append result-string "iv"))
               (set! tmp-num (- tmp-num 4))
               ))
            ((>= tmp-num 1)
             (begin
               (set!
                result-string
                (string-append result-string "i"))
               (set! tmp-num (- tmp-num 1))
               )))
           ))

        result-string
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-number-to-roman-string-1 result-hash-table)
 (begin
   (let ((sub-name "test-number-to-roman-string-1")
         (test-list
          (list
           (list "i" 1) (list "ii" 2) (list "iii" 3)
           (list "iv" 4) (list "v" 5) (list "vi" 6)
           (list "vii" 7) (list "viii" 8) (list "ix" 9)
           (list "x" 10) (list "xi" 11) (list "xii" 12)
           (list "xiii" 13) (list "xiv" 14) (list "xv" 15)
           (list "xvi" 16) (list "xvii" 17) (list "xviii" 18)
           (list "xix" 19) (list "xx" 20) (list "xxix" 29)
           (list "xxxix" 39) (list "xlix" 49) (list "lix" 59)
           (list "lxviii" 68) (list "lxxviii" 78) (list "lxxxix" 89)
           (list "xc" 90) (list "xci" 91) (list "xcviii" 98)
           (list "xcix" 99) (list "c" 100) (list "ci" 101)
           (list "cix" 109) (list "cxix" 119) (list "cxxix" 129)
           (list "cxl" 140) (list "cxlix" 149) (list "l" 50)
           (list "cxc" 190) (list "cxcix" 199) (list "d" 500)
           (list "cdxcix" 499) (list "m" 1000) (list "mmx" 2010)
           (list "mmmm" 4000) (list "cmxcix" 999)
           (list "mmmmmmmmmcmxcix" 9999)
           (list "xvi" 16)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((shouldbe (list-ref alist 0))
                  (test-num (list-ref alist 1)))
              (let ((result
                     (number-to-roman-string test-num)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : test-num=~a, "
                        sub-name test-label-index test-num))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; assume input-string is in lower-case
(define (simplify-roman-string input-string roman-char-htable)
  (begin
    (let ((num-value
           (roman-string-to-number input-string roman-char-htable)))
      (let ((simple-string
             (number-to-roman-string num-value)))
        (begin
          simple-string
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-simplify-roman-string-1 result-hash-table)
 (begin
   (let ((sub-name "test-simplify-roman-string-1")
         (test-list
          (list
           (list "MMMMDCLXXII" "mmmmdclxxii")
           (list "MMDCCCLXXXIII" "mmdccclxxxiii")
           (list "MMMDLXVIIII" "mmmdlxix")
           (list "MMMMDXCV" "mmmmdxcv")
           (list "DCCCLXXII" "dccclxxii")
           (list "MMCCCVI" "mmcccvi")
           (list "iiiiiiiiiiiiiiii" "xvi") (list "viiiiiiiiiii" "xvi")
           (list "vviiiiii" "xvi") (list "xiiiiii" "xvi")
           (list "vvvi" "xvi") (list "xvi" "xvi")
           ))
         (roman-char-htable (make-hash-table 10))
         (test-label-index 0))
     (begin
       (populate-roman-char-hash-table! roman-char-htable)

       (for-each
        (lambda (alist)
          (begin
            (let ((roman-string (string-downcase (list-ref alist 0)))
                  (shouldbe (list-ref alist 1)))
              (let ((result
                     (simplify-roman-string
                      roman-string roman-char-htable)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : roman-string=~s, "
                        sub-name test-label-index roman-string))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; assume input-string is in lower-case
(define (count-chars-saved input-string roman-char-htable)
  (let ((simple-string (simplify-roman-string input-string roman-char-htable)))
    (let ((old-slen (string-length input-string))
          (new-slen (string-length simple-string)))
      (begin
        (- old-slen new-slen)
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-count-chars-saved-1 result-hash-table)
 (begin
   (let ((sub-name "test-count-chars-saved-1")
         (test-list
          (list
           (list "MMMMDCLXXII" 0)
           (list "MMDCCCLXXXIII" 0)
           (list "MMMDLXVIIII" 3)
           (list "MMMMDXCV" 0)
           (list "DCCCLXXII" 0)
           (list "MMCCCVI" 0)
           (list "iiiiiiiiiiiiiiii" 13) (list "viiiiiiiiiii" 9)
           (list "vviiiiii" 5) (list "xiiiiii" 4)
           (list "vvvi" 1) (list "xvi" 0)
           ))
         (roman-char-htable (make-hash-table 10))
         (test-label-index 0))
     (begin
       (populate-roman-char-hash-table! roman-char-htable)

       (for-each
        (lambda (alist)
          (begin
            (let ((roman-string (string-downcase (list-ref alist 0)))
                  (shouldbe (list-ref alist 1)))
              (let ((result
                     (count-chars-saved roman-string roman-char-htable)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : roman-string=~a, "
                        sub-name test-label-index roman-string))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; returns a list of names, lower case
;;;       (list "iiiiiiiiiiiiiiii" 13) (list "viiiiiiiiiii" 9)
;;;       (list "vviiiiii" 5) (list "xiiiiii" 4)
;;;       (list "vvvi" 1) (list "xvi" 0)
(define (list-loop roman-numerals-list debug-flag)
  (begin
    (let ((roman-char-htable (make-hash-table 10))
          (chars-saved 0)
          (counter 0))
      (begin
        (populate-roman-char-hash-table! roman-char-htable)

        (for-each
         (lambda(rstring)
           (begin
             (let ((this-string
                    (string-downcase rstring)))
               (let ((this-saved
                      (count-chars-saved
                       this-string roman-char-htable)))
                 (begin
                   (set! chars-saved (+ chars-saved this-saved))
                   (set! counter (1+ counter))
                   (if (equal? debug-flag #t)
                       (begin
                         (let ((next-string
                                (simplify-roman-string
                                 this-string roman-char-htable)))
                           (begin
                             (display
                              (ice-9-format:format
                               #f "(~:d) ~s -> ~s : chars saved = "
                               counter this-string next-string))
                             (display
                              (ice-9-format:format
                               #f "~:d : so far chars saved = ~:d~%"
                               this-saved chars-saved))
                             (force-output)
                             ))
                         ))
                   )))
             )) roman-numerals-list)

        (display
         (ice-9-format:format
          #f "total number of roman numerals = ~:d~%" counter))
        (display
         (ice-9-format:format
          #f "total characters saved = ~:d~%" chars-saved))
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
;;; returns a list of names, lower case
(define (read-in-file fname debug-flag)
  (begin
    (let ((roman-list (list)))
      (begin
        (if (file-exists? fname)
            (begin
              (with-input-from-file fname
                (lambda ()
                  (begin
                    (do ((line
                          (ice-9-rdelim:read-delimited ",\n\r")
                          (ice-9-rdelim:read-delimited ",\n\r")))
                        ((eof-object? line))
                      (begin
                        (if (not (eof-object? line))
                            (begin
                              (let ((this-string
                                     (string-downcase
                                      (string-delete #\" line))))
                                (begin
                                  (if (and (string? this-string)
                                           (> (string-length
                                               this-string) 0))
                                      (begin
                                        (set!
                                         roman-list
                                         (cons this-string roman-list))
                                        ))
                                  ))
                              ))
                        ))
                    )))
              ))

        (if (and (list? roman-list)
                 (> (length roman-list) 0))
            (begin
              (set! roman-list (reverse roman-list))
              (list-loop roman-list debug-flag)
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "The rules for writing Roman "))
    (display
     (format #f "numerals allow for~%"))
    (display
     (format #f "many ways of writing each number "))
    (display
     (format #f "(see About Roman~%"))
    (display
     (format #f "Numerals...). However there is always "))
    (display
     (format #f "a 'best' way of~%"))
    (display
     (format #f "writing a particular number.~%"))
    (newline)
    (display
     (format #f "For example, the following represent "))
    (display
     (format #f "all of the~%"))
    (display
     (format #f "legitimate ways of writing the "))
    (display
     (format #f "number sixteen:~%"))
    (newline)
    (display
     (format #f "IIIIIIIIIIIIIIII~%"))
    (display
     (format #f "VIIIIIIIIIII~%"))
    (display
     (format #f "VVIIIIII~%"))
    (display
     (format #f "XIIIIII~%"))
    (display
     (format #f "VVVI~%"))
    (display
     (format #f "XVI~%"))
    (newline)
    (display
     (format #f "The last example being considered "))
    (display
     (format #f "the most efficient,~%"))
    (display
     (format #f "as it uses the least number of "))
    (display
     (format #f "numerals.~%"))
    (newline)
    (display
     (format #f "The 11K text file, roman.txt (right "))
    (display
     (format #f "click and~%"))
    (display
     (format #f "'Save Link/Target As...'), contains "))
    (display
     (format #f "one thousand numbers~%"))
    (display
     (format #f "written in valid, but not necessarily "))
    (display
     (format #f "minimal, Roman numerals;~%"))
    (display
     (format #f "that is, they are arranged in descending "))
    (display
     (format #f "units and obey the~%"))
    (display
     (format #f "subtractive pair rule (see About Roman "))
    (display
     (format #f "Numerals... for the~%"))
    (display
     (format #f "definitive rules for this problem).~%"))
    (newline)
    (display
     (format #f "Find the number of characters saved "))
    (display
     (format #f "by writing each of~%"))
    (display
     (format #f "these in their minimal form.~%"))
    (newline)
    (display
     (format #f "Note: You can assume that all the "))
    (display
     (format #f "Roman numerals in the~%"))
    (display
     (format #f "file contain no more than four "))
    (display
     (format #f "consecutive identical units.~%"))
    (display
     (format #f "see https://projecteuler.net/problem=89~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((test-list
           (list "iiiiiiiiiiiiiiii" "viiiiiiiiiii"
                 "vviiiiii" "xiiiiii" "vvvi" "xvi"))
          (debug-flag #t))
      (begin
        (list-loop test-list debug-flag)
        ))

    (newline)
    (let ((filename "roman.txt")
          (debug-flag #f))
      (begin
        (display
         (format #f "processing file ~a~%" filename))
        (force-output)
        (read-in-file filename debug-flag)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 89 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

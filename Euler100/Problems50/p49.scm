#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 49                                     ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated June 14, 2022                                ###
;;;###                                                       ###
;;;###  updated March 6, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### srfi-1 for fold function
(use-modules ((srfi srfi-1)
              :renamer (symbol-prefix-proc 'srfi-1:)))

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### prime-module for is-prime? function
(use-modules ((prime-module)
              :renamer (symbol-prefix-proc 'prime-module:)))

;;;### digits-module for split-digits-list function
(use-modules ((digits-module)
              :renamer (symbol-prefix-proc 'digits-module:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (make-prime-permutation-list input-number)
  (define (local-loop
           digit-list dlength current-list clength acc-list)
    (begin
      (if (>= clength dlength)
          (begin
            (let ((sc-list (sort current-list <)))
              (begin
                (if (and
                     (not (zero? (car current-list)))
                     (equal? sc-list digit-list))
                    (begin
                      (let ((this-num
                             (digits-module:digit-list-to-number
                              current-list)))
                        (begin
                          (if (prime-module:is-prime? this-num)
                              (begin
                                (cons this-num acc-list))
                              (begin
                                acc-list
                                ))
                          )))
                    (begin
                      acc-list
                      ))
                )))
          (begin
            (for-each
             (lambda (tmp-digit)
               (begin
                 (let ((next-current-list
                        (cons tmp-digit current-list)))
                   (let ((next-clength
                          (length next-current-list)))
                     (begin
                       (let ((next-acc-list
                              (local-loop
                               digit-list dlength
                               next-current-list next-clength
                               acc-list)))
                         (begin
                           (set! acc-list next-acc-list)
                           ))
                       )))
                 )) digit-list)

            acc-list
            ))
      ))
  (begin
    (let ((dlist
           (sort
            (digits-module:split-digits-list input-number)
            <)))
      (let ((dlen (length dlist)))
        (let ((pperm-list
               (local-loop dlist dlen (list) 0 (list))))
          (begin
            pperm-list
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-make-prime-permutation-list-1 result-hash-table)
 (begin
   (let ((sub-name "test-make-prime-permutation-list-1")
         (test-list
          (list
           (list 3 (list 3)) (list 4 (list))
           (list 5 (list 5)) (list 13 (list 13 31))
           (list 14 (list 41)) (list 15 (list))
           (list 23 (list 23)) (list 24 (list))
           (list 25 (list)) (list 173 (list 173 137 317))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((num (list-ref alist 0))
                  (shouldbe-list (list-ref alist 1)))
              (let ((result-list
                     (make-prime-permutation-list num)))
                (let ((slen (length shouldbe-list))
                      (rlen (length result-list)))
                  (let ((err-1
                         (format
                          #f "~a :: (~a) error : num=~a, "
                          sub-name test-label-index num))
                        (err-2
                         (format
                          #f "shouldbe len=~a, result len=~a"
                          slen rlen))
                        (err-3
                         (format
                          #f "shouldbe=~a, result=~a, "
                          shouldbe-list result-list)))
                    (begin
                      (unittest2:assert?
                       (equal? slen rlen)
                       sub-name
                       (string-append err-1 err-2)
                       result-hash-table)

                      (if (> slen 0)
                          (begin
                            (for-each
                             (lambda (s-elem)
                               (begin
                                 (let ((mflag
                                        (member
                                         s-elem result-list))
                                       (err-4
                                        (format
                                         #f "missing element ~a"
                                         s-elem)))
                                   (begin
                                     (unittest2:assert?
                                      (not (equal? mflag #f))
                                      sub-name
                                      (string-append
                                       err-1 err-3 err-4)
                                      result-hash-table)
                                     ))
                                 )) shouldbe-list)
                            ))
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; example list (list (list 1487 4817) (list 4817 8147))
;;; so 1487 and 8147 are the outside nodes, and 4817 is the internal node
(define (is-list-list-connected? input-list-list)
  (begin
    (let ((llen (length input-list-list))
          (result-flag #f))
      (let ((num-count-htable (make-hash-table llen)))
        (begin
          (for-each
           (lambda (a-list)
             (begin
               (let ((a1 (list-ref a-list 0))
                     (a2 (list-ref a-list 1)))
                 (begin
                   (let ((count1
                          (hash-ref num-count-htable a1 0))
                         (count2
                          (hash-ref num-count-htable a2 0)))
                     (begin
                       (hash-set!
                        num-count-htable a1 (+ count1 1))
                       (hash-set!
                        num-count-htable a2 (+ count2 1))
                       ))
                   ))
               )) input-list-list)

          (let ((num-1 0)
                (num-2 0)
                (num-other 0)
                (total 0)
                (ok-flag #f))
            (begin
              (hash-for-each
               (lambda (num count)
                 (begin
                   (set! total (1+ total))
                   (cond
                    ((= count 1)
                     (begin
                       (set! num-1 (1+ num-1))
                       ))
                    ((= count 2)
                     (begin
                       (set! num-2 (1+ num-2))
                       ))
                    (else
                     (begin
                       (set! num-other (1+ num-other))
                       )))
                   )) num-count-htable)

              (if (and (= num-1 2)
                       (= total (+ num-1 num-2)))
                  (begin
                    (set! ok-flag #t)
                    ))
              ok-flag
              ))
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-is-list-list-connected-1 result-hash-table)
 (begin
   (let ((sub-name "test-is-list-list-connected-1")
         (test-list
          (list
           (list (list (list 1 3) (list 3 4)) #t)
           (list (list (list 1 2) (list 2 3) (list 3 4)) #t)
           (list (list (list 1 2) (list 4 9) (list 3 4)) #f)
           (list (list (list 1 2) (list 3 4) (list 5 6)
                       (list 7 8) (list 9 10)) #f)
           (list (list (list 1 2) (list 3 4) (list 5 6)
                       (list 7 8) (list 9 10) (list 2 3)) #f)
           (list (list (list 1 2) (list 3 4) (list 5 6)
                       (list 7 8) (list 9 10) (list 2 3)
                       (list 4 5)) #f)
           (list (list (list 1 2) (list 3 4) (list 5 6)
                       (list 7 8) (list 9 10) (list 2 3)
                       (list 4 5) (list 6 7) (list 8 9)) #t)
           (list (list (list 1309 3091) (list 9031 3091)) #t)
           (list (list (list 3109 9103) (list 3019 9013)) #f)
           (list (list (list 9013 9103) (list 3019 3109)) #f)
           ))
         (test-label-index 0)
         (ok-flag #t))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((llist (list-ref alist 0))
                  (shouldbe-bool (list-ref alist 1)))
              (let ((result-bool
                     (is-list-list-connected? llist)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : llist=~a, "
                        sub-name test-label-index llist))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a~%"
                        shouldbe-bool result-bool)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-bool result-bool)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (make-constant-increasing-list perm-list)
  (begin
    (let ((sp-list (sort perm-list <))
          (sp-len (length perm-list)))
      (let ((sp-len-m1 (- sp-len 1))
            (diff-htable (make-hash-table sp-len))
            (result-diff -1)
            (result-len -1)
            (result-list (list (list))))
        (begin
          (do ((ii 0 (1+ ii)))
              ((>= ii sp-len-m1))
            (begin
              ;;; find the difference
              (let ((first-num (list-ref sp-list ii)))
                (begin
                  (do ((jj (+ ii 1) (1+ jj)))
                      ((>= jj sp-len))
                    (begin
                      (let ((second-num (list-ref sp-list jj)))
                        (let ((this-diff (- second-num first-num)))
                          (let ((saved-list
                                 (hash-ref
                                  diff-htable this-diff (list)))
                                (next-list
                                 (list first-num second-num)))
                            (begin
                              (if (and (> this-diff 0)
                                       (equal?
                                        (member next-list saved-list)
                                        #f))
                                  (begin
                                    (set!
                                     saved-list
                                     (cons
                                      (list first-num second-num)
                                      saved-list))
                                    (hash-set!
                                     diff-htable
                                     this-diff saved-list)
                                    ))
                              ))
                          ))
                      ))
                  ))
              ))

          (hash-for-each
           (lambda (diff llist)
             (begin
               (let ((llen (length llist)))
                 (begin
                   (if (and
                        (> llen 1)
                        (is-list-list-connected? llist))
                       (begin
                         (if (<= result-len 0)
                             (begin
                               (set! result-diff diff)
                               (set! result-len llen)
                               (set! result-list (list llist)))
                             (begin
                               (if (>= llen result-len)
                                   (begin
                                     (set! result-diff diff)
                                     (set! result-len llen)
                                     (set!
                                      result-list
                                      (cons llist result-list))
                                     ))
                               ))
                         ))
                   ))
               )) diff-htable)

          (list result-diff result-list)
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-make-constant-increasing-list-1 result-hash-table)
 (begin
   (let ((sub-name "test-make-constant-increasing-list-1")
         (test-list
          (list
           (list 173 (list 0 (list (list))))
           (list 1487 (list 3330
                            (list (list (list 1487 4817)
                                        (list 4817 8147)))))
           ))
         (test-label-index 0)
         (ok-flag #t))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((num (list-ref alist 0))
                  (shouldbe-list (list-ref alist 1)))
              (let ((perm-list
                     (make-prime-permutation-list num)))
                (let ((result-list
                       (make-constant-increasing-list
                        perm-list)))
                  (let ((s-diff (list-ref shouldbe-list 0))
                        (s-list (car (list-ref shouldbe-list 1)))
                        (r-diff (list-ref result-list 0))
                        (r-list (car (list-ref result-list 1))))
                    (let ((slen (length s-list))
                          (rlen (length r-list)))
                      (let ((err-1
                             (format
                              #f "~a :: (~a) error : num=~a, "
                              sub-name test-label-index num))
                            (err-2
                             (format
                              #f "shouldbe len=~a, result len=~a"
                              slen rlen))
                            (err-3
                             (format
                              #f "shouldbe=~a, result=~a"
                              shouldbe-list result-list)))
                        (begin
                          (unittest2:assert?
                           (equal? slen rlen)
                           sub-name
                           (string-append
                            err-1 err-2)
                           result-hash-table)

                          (for-each
                           (lambda (s-elem)
                             (begin
                               (unittest2:assert?
                                (not
                                 (equal?
                                  (member s-elem r-list) #f))
                                sub-name
                                (string-append
                                 err-1 err-3)
                                result-hash-table)
                               )) s-list)
                          )))
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (connected-list-list-to-sequence c-list-list)
  (begin
    (let ((result-list (list)))
      (begin
        (for-each
         (lambda (c-list)
           (begin
             (let ((c1 (list-ref c-list 0))
                   (c2 (list-ref c-list 1)))
               (begin
                 (if (equal? (member c1 result-list) #f)
                     (begin
                       (set! result-list (cons c1 result-list))
                       ))
                 (if (equal? (member c2 result-list) #f)
                     (begin
                       (set! result-list (cons c2 result-list))
                       ))
                 ))
             )) c-list-list)

        (sort result-list <)
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-connected-list-list-to-sequence-1 result-hash-table)
 (begin
   (let ((sub-name "test-connected-list-list-to-sequence-1")
         (test-list
          (list
           (list (list (list 1 2) (list 2 3) (list 3 4))
                 (list 1 2 3 4))
           (list (list (list 1487 4817)
                       (list 4817 8147))
                 (list 1487 4817 8147))
           ))
         (test-label-index 0)
         (ok-flag #t))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((llist (list-ref alist 0))
                  (shouldbe-list (list-ref alist 1)))
              (let ((result-list
                     (connected-list-list-to-sequence llist)))
                (let ((slen (length shouldbe-list))
                      (rlen (length result-list)))
                  (let ((err-1
                         (format
                          #f "~a :: (~a) error : llist=~a, "
                          sub-name test-label-index llist))
                        (err-2
                         (format
                          #f "shouldbe len=~a, result len=~a"
                          slen rlen))
                        (err-3
                         (format
                          #f "shouldbe=~a, result=~a"
                          shouldbe-list result-list)))
                    (begin
                      (unittest2:assert?
                       (equal? slen rlen)
                       sub-name
                       (string-append err-1 err-2)
                       result-hash-table)

                      (if (> slen 0)
                          (begin
                            (for-each
                             (lambda (s-elem)
                               (begin
                                 (unittest2:assert?
                                  (not
                                   (equal?
                                    (member s-elem result-list)
                                    #f))
                                  sub-name
                                  (string-append err-1 err-3)
                                  result-hash-table)
                                 )) shouldbe-list)
                            ))
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (concatenate-numbers num-list)
  (begin
    (let ((all-list (list)))
      (begin
        (for-each
         (lambda (elem)
           (begin
             (let ((dlist
                    (digits-module:split-digits-list elem)))
               (begin
                 (set! all-list (append all-list dlist))
                 ))
             )) num-list)

        (let ((conc-num
               (srfi-1:fold
                (lambda (num prev)
                  (begin
                    (+ num (* 10 prev))
                    ))
                0 all-list)))
          (begin
            conc-num
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-concatenate-numbers-1 result-hash-table)
 (begin
   (let ((sub-name "test-concatenate-numbers-1")
         (test-list
          (list
           (list (list 33 44 55) 334455)
           (list (list 12 34 56) 123456)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((num-list (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result
                     (concatenate-numbers num-list)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : num-list=~a, "
                        sub-name test-label-index num-list))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a~%"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (list-to-sequence-string llist)
  (begin
    (let ((this-string
           (string-join
            (map
             (lambda (this-elem)
               (begin
                 (ice-9-format:format #f "~:d" this-elem)
                 )) llist)
            ", ")))
      (begin
        (string-append "{ " this-string " }")
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-list-to-sequence-string-1 result-hash-table)
 (begin
   (let ((sub-name "test-list-to-sequence-string-1")
         (test-list
          (list
           (list (list 1) "{ 1 }")
           (list (list 1 2) "{ 1, 2 }")
           (list (list 1 2 3) "{ 1, 2, 3 }")
           (list (list 4 5 6 7) "{ 4, 5, 6, 7 }")
           ))
         (test-label-index 0)
         (ok-flag #t))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((input-list (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result
                     (list-to-sequence-string input-list)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : input-list=~a, "
                        sub-name test-label-index input-list))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a~%"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (string-ci=? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (display-results ii sequence cdiff)
  (begin
    (let ((con-num
           (concatenate-numbers sequence))
          (seq-string
           (list-to-sequence-string sequence)))
      (let ((str-1
             (ice-9-format:format
              #f "(~:d)  arithmetic sequence ~a"
              ii seq-string))
            (str-2
             (ice-9-format:format
              #f "constant difference = ~:d"
              cdiff))
            (str-3
             (ice-9-format:format
              #f "12-digit number = ~:d"
              con-num)))
        (begin
          (display
           (format #f "~a~%" str-1))
          (display
           (format #f "~a~%" str-2))
          (display
           (format #f "~a~%" str-3))

          (for-each
           (lambda (elem)
             (begin
               (let ((pflag
                      (prime-module:is-prime? elem)))
                 (begin
                   (display
                    (ice-9-format:format
                     #f "  prime?(~:d) = ~a~%"
                     elem (if (equal? pflag #t) "true" "false")))
                   ))
               )) sequence)

          (newline)
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (main-secondary-loop ii seq-seen-htable prime-seen-htable)
  (begin
    (let ((perm-list (make-prime-permutation-list ii))
          (num-results 0))
      (let ((cseq-list
             (make-constant-increasing-list perm-list)))
        (let ((c-diff (list-ref cseq-list 0))
              (c-list (list-ref cseq-list 1)))
          (let ((c-len (length c-list)))
            (begin
              (for-each
               (lambda (this-prime)
                 (begin
                   (hash-set! prime-seen-htable this-prime #t)
                   )) perm-list)

              (if (and (list? c-list)
                       (> c-len 0)
                       (> c-diff 0))
                  (begin
                    (for-each
                     (lambda (c2-list)
                       (begin
                         (let ((sequence
                                 (connected-list-list-to-sequence
                                  c2-list)))
                           (begin
                             (if (and (list? sequence)
                                      (> (length sequence) 0))
                                 (begin
                                   (let ((seen-flag
                                          (hash-ref
                                           seq-seen-htable sequence #f)))
                                     (begin
                                       (if (equal? seen-flag #f)
                                           (begin
                                             (set!
                                              num-results (1+ num-results))
                                             (display-results
                                              ii sequence c-diff)
                                             (hash-set!
                                              seq-seen-htable sequence #t)
                                             ))
                                       ))
                                   ))
                             ))
                         )) c-list)
                    ))
              num-results
              )))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop start-num end-num)
  (begin
    (let ((num-results 0)
          (seq-seen-htable (make-hash-table 100))
          (prime-seen-htable (make-hash-table 100)))
      (begin
        (do ((ii start-num (1+ ii)))
            ((> ii end-num))
          (begin
            (let ((pflag (hash-ref prime-seen-htable ii)))
              (begin
                (if (equal? pflag #f)
                    (begin
                      (let ((this-num
                             (main-secondary-loop
                              ii seq-seen-htable prime-seen-htable)))
                        (begin
                          (set!
                           num-results (+ num-results this-num))
                          ))
                      ))
                ))
            ))

        (display
         (ice-9-format:format
          #f "total number of results found = ~:d~%"
          num-results))
        (display
         (ice-9-format:format
          #f "(between ~:d and ~:d)~%"
          start-num end-num))
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "The arithmetic sequence, 1487, 4817, "))
    (display
     (format #f "8147, in which~%"))
    (display
     (format #f "each of the terms increases by 3330, is "))
    (display
     (format #f "unusual in two ways:~%"))
    (display
     (format #f "(i) each of the three terms are prime, "))
    (display
     (format #f "and, (ii) each of~%"))
    (display
     (format #f "the 4-digit numbers are permutations of "))
    (display
     (format #f "one another.~%"))
    (newline)
    (display
     (format #f "There are no arithmetic sequences "))
    (display
     (format #f "made up of three~%"))
    (display
     (format #f "1-, 2-, or 3-digit primes, exhibiting "))
    (display
     (format #f "this property, but~%"))
    (display
     (format #f "there is one other 4-digit increasing "))
    (display
     (format #f "sequence.~%"))
    (newline)
    (display
     (format #f "What 12-digit number do you form by "))
    (display
     (format #f "concatenating the three~%"))
    (display
     (format #f "terms in this sequence?~%"))
    (display
     (format #f "see https://projecteuler.net/problem=49~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((start-num 1000)
          (end-num 10000))
      (begin
        (sub-main-loop start-num end-num)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 49 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (load "prime-module-tests-1.scm")
                 (load "digits-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 44                                     ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated June 14, 2022                                ###
;;;###                                                       ###
;;;###  updated March 5, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (pentagonal-number this-num)
  (begin
    (let ((t1 (- (* 3 this-num) 1)))
      (let ((t2 (* this-num t1)))
        (let ((t3 (euclidean/ t2 2)))
          (begin
            t3
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-pentagonal-number-1 result-hash-table)
 (begin
   (let ((sub-name "test-pentagonal-number-1")
         (test-list
          (list
           (list 1 1) (list 2 5) (list 3 12) (list 4 22)
           (list 5 35) (list 6 51) (list 7 70) (list 8 92)
           (list 9 117) (list 10 145)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-num (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result
                     (pentagonal-number test-num)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : test-num=~a, "
                        sub-name test-label-index test-num))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (inverse-pentagonal-number xx)
  (begin
    (let ((result #f))
      (let ((tmp1 (sqrt (1+ (* 24 xx)))))
        (let ((tmp2 (/ (1+ tmp1) 6)))
          (begin
            (if (integer? tmp2)
                (begin
                  (set! result (inexact->exact tmp2))
                  ))
            result
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-inverse-pentagonal-number-1 result-hash-table)
 (begin
   (let ((sub-name "test-inverse-pentagonal-number-1")
         (test-list
          (list
           (list 1 1) (list 5 2) (list 12 3) (list 22 4)
           (list 35 5) (list 51 6) (list 70 7) (list 92 8)
           (list 117 9) (list 145 10)
           (list 1560090 1020)
           (list 23 #f) (list 24 #f)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-num (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result
                     (inverse-pentagonal-number test-num)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : test-num=~a, "
                        sub-name test-label-index test-num))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (make-pentagonal-list max-num)
  (begin
    (let ((result-list (list)))
      (begin
        (do ((jj 1 (+ jj 1)))
            ((> jj max-num))
          (begin
            (let ((this-pentagonal-num
                   (pentagonal-number jj)))
              (begin
                (set!
                 result-list
                 (cons this-pentagonal-num result-list))
                ))
            ))

        (reverse result-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-make-pentagonal-list-1 result-hash-table)
 (begin
   (let ((sub-name "test-make-pentagonal-list-1")
         (test-list
          (list
           (list 1 (list 1))
           (list 2 (list 1 5))
           (list 3 (list 1 5 12))
           (list 4 (list 1 5 12 22))
           (list 5 (list 1 5 12 22 35))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((test-num (list-ref this-list 0))
                  (shouldbe-list (list-ref this-list 1)))
              (let ((result-list
                     (make-pentagonal-list test-num)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : test-num=~a, "
                        sub-name test-label-index test-num))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe-list result-list)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-list result-list)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (populate-pentagonal-htable! p-htable max-num)
  (begin
    (do ((jj 1 (+ jj 1)))
        ((> jj max-num))
      (begin
        (let ((this-pentagonal-num
               (pentagonal-number jj)))
          (begin
            (hash-set! p-htable this-pentagonal-num jj)
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax debug-display-macro
  (syntax-rules ()
    ((debug-display-macro
      ii jj pi-elem pj-elem pdiff
      min-pj min-jj min-pk min-kk
      min-sum min-sum-index min-diff min-diff-index)
     (begin
       (display
        (ice-9-format:format
         #f "debug p(~:d) = ~:d, p(~:d) = ~:d, diff = ~:d~%"
         ii pi-elem jj pj-elem pdiff))
       (display
        (ice-9-format:format
         #f "p(~:d) + p(~:d) = ~:d + ~:d = ~:d = p(~:d)~%"
         min-kk min-jj min-pk min-pj min-sum min-sum-index))
       (display
        (ice-9-format:format
         #f "p(~:d) - p(~:d) = ~:d - ~:d = ~:d = p(~:d)~%"
         min-kk min-jj min-pk min-pj min-diff min-diff-index))
       (force-output)
       ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax inner-process-macro
  (syntax-rules ()
    ((inner-process-macro
      ii jj pi-elem pj-elem pent-htable
      min-pj min-jj min-pk min-kk
      min-sum min-sum-index min-diff min-diff-index
      inner-break-flag debug-flag)
     (begin
       (let ((psum (+ pj-elem pi-elem))
             (pdiff (- pj-elem pi-elem)))
         (let ((psum-flag (hash-ref pent-htable psum #f))
               (pdiff-flag (hash-ref pent-htable pdiff #f)))
           (begin
             (if (equal? psum #f)
                 (begin
                   (let ((inv-psum
                          (inverse-pentagonal-number psum)))
                     (begin
                       (if (not (equal? inv-psum #f))
                           (begin
                             (hash-set! pent-htable psum inv-psum)
                             ))
                       ))
                   ))
             (if (equal? pdiff #f)
                 (begin
                   (let ((inv-pdiff
                          (inverse-pentagonal-number pdiff)))
                     (begin
                       (if (not (equal? inv-pdiff #f))
                           (begin
                             (hash-set! pent-htable pdiff inv-psum)
                             ))
                       ))
                   ))

             (if (and
                  (not (equal? psum-flag #f))
                  (not (equal? pdiff-flag #f)))
                 (begin
                   (if (< pdiff min-diff)
                       (begin
                         (set! min-pj pi-elem)
                         (set! min-jj ii)
                         (set! min-pk pj-elem)
                         (set! min-kk jj)
                         (set! min-sum psum)
                         (set! min-sum-index psum-flag)
                         (set! min-diff pdiff)
                         (set! min-diff-index pdiff-flag)
                         (set! inner-break-flag #t)
                         ))

                   (if (equal? debug-flag #t)
                       (begin
                         (debug-display-macro
                          ii jj pi-elem pj-elem pdiff
                          min-pj min-jj min-pk min-kk
                          min-sum min-sum-index min-diff min-diff-index)
                         ))
                   ))

             (if (> pdiff min-diff)
                 (begin
                   (set! inner-break-flag #t)
                   ))
             )))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop max-num debug-flag)
  (begin
    (let ((counter 0)
          (pent-array
           (list->array 1 (make-pentagonal-list max-num)))
          (pent-htable (make-hash-table max-num))
          (max-pent (pentagonal-number max-num))
          (min-diff (pentagonal-number max-num))
          (min-pj 0)
          (min-jj 0)
          (min-pk 0)
          (min-kk 0)
          (min-sum 0)
          (min-sum-index 0)
          (min-diff-index 0)
          (break-flag #f))
      (let ((plen
             (car (array-dimensions pent-array))))
        (begin
          (populate-pentagonal-htable! pent-htable max-num)

          (do ((ii 0 (1+ ii)))
              ((>= ii plen))
            (begin
              (let ((pi-elem (array-ref pent-array ii))
                    (inner-break-flag #f))
                (begin
                  (do ((jj (1+ ii) (1+ jj)))
                      ((or (>= jj plen)
                           (equal? inner-break-flag #t)))
                    (begin
                      (let ((pj-elem (array-ref pent-array jj)))
                        (begin
                          (inner-process-macro
                           ii jj pi-elem pj-elem pent-htable
                           min-pj min-jj min-pk min-kk
                           min-sum min-sum-index min-diff min-diff-index
                           inner-break-flag debug-flag)
                          ))
                      ))
                  ))
              ))

          (if (< min-diff max-pent)
              (begin
                (display
                 (ice-9-format:format
                  #f "the pair of pentagonal numbers, pj and pk,~%"))
                (display
                 (ice-9-format:format
                  #f "for which their sum and difference is pentagonal~%"))
                (display
                 (ice-9-format:format
                  #f "and D = |pk - pj| is minimised, "))
                (display
                 (ice-9-format:format
                  #f "(for index < ~:d, p < ~:d)~%"
                  max-num max-pent))
                (display
                 (ice-9-format:format
                  #f "p(~:d) + p(~:d) = ~:d + ~:d = ~:d = p(~:d)~%"
                  min-kk min-jj min-pk min-pj min-sum min-sum-index))
                (display
                 (ice-9-format:format
                  #f "p(~:d) - p(~:d) = ~:d - ~:d = ~:d = p(~:d)~%"
                  min-kk min-jj min-pk min-pj min-diff min-diff-index))
                (display
                 (ice-9-format:format
                  #f "value of difference = ~:d~%" min-diff)))
              (begin
                (display
                 (ice-9-format:format
                  #f "no pentagonal pairs found for which their~%"))
                (display
                 (ice-9-format:format
                  #f "sum and differences are also pentagonal~%"))
                (display
                 (ice-9-format:format
                  #f "(for index < ~:d, p < ~:d)~%"
                  max-num max-pent))
                ))
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Pentagonal numbers are generated by "))
    (display
     (format #f "the formula, Pn=n(3n-1)/2.~%"))
    (display
     (format #f "The first ten pentagonal numbers are:~%"))
    (newline)
    (display
     (format #f "    1, 5, 12, 22, 35, 51, 70, 92, 117, 145, ...~%"))
    (newline)
    (display
     (format #f "It can be seen that P4 + P7 = 22 + 70 "))
    (display
     (format #f "= 92 = P8. However,~%"))
    (display
     (format #f "their difference, 70 - 22 = 48, is not "))
    (display
     (format #f "pentagonal.~%"))
    (newline)
    (display
     (format #f "Find the pair of pentagonal numbers, Pj "))
    (display
     (format #f "and Pk, for which~%"))
    (display
     (format #f "their sum and difference is pentagonal "))
    (display
     (format #f "and D = |Pk - Pj| is~%"))
    (display
     (format #f "minimised; what is the value of D?~%"))
    (newline)
    (display
     (format #f "The inverse of the pentagonal function "))
    (display
     (format #f "can be found for~%"))
    (display
     (format #f "any number x, by considering x = "))
    (display
     (format #f "n*(3n-1)/2,~%"))
    (display
     (format #f "and solving the quadratic equation.~%"))
    (display
     (format #f "n = (1 + sqrt(1 + 24*x))/6.~%"))
    (display
     (format #f "see https://projecteuler.net/problem=44~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((max-num 100)
          (debug-flag #t))
      (begin
        (sub-main-loop max-num debug-flag)
        ))

    (newline)
    (let ((max-num 10000)
          (debug-flag #t))
      (begin
        (sub-main-loop max-num debug-flag)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 44 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

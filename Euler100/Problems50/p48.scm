#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 48                                     ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated June 14, 2022                                ###
;;;###                                                       ###
;;;###  updated March 5, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### digits-module for split-digits-list function
(use-modules ((digits-module)
              :renamer (symbol-prefix-proc 'digits-module:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (integer-power this-number this-exponent)
  (begin
    (cond
     ((= this-exponent 0)
      (begin
        1
        ))
     ((= this-exponent 1)
      (begin
        this-number
        ))
     ((< this-exponent 0)
      (begin
        -1
        ))
     (else
      (begin
        (let ((result-num this-number)
              (max-iter (- this-exponent 1)))
          (begin
            (do ((ii 0 (+ ii 1)))
                ((>= ii max-iter))
              (begin
                (set!
                 result-num
                 (* result-num this-number))
                ))
            result-num
            ))
        )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-integer-power-1 result-hash-table)
 (begin
   (let ((sub-name "test-integer-power-1")
         (test-list
          (list
           (list 10 0 1) (list 11 0 1) (list 12 0 1)
           (list 10 1 10) (list 11 1 11) (list 12 1 12)
           (list 10 2 100) (list 11 2 121) (list 12 2 144)
           (list 2 2 4) (list 2 3 8) (list 2 4 16) (list 2 5 32)
           (list 2 6 64) (list 2 7 128) (list 2 8 256) (list 2 9 512)
           (list 2 10 1024)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-num (list-ref alist 0))
                  (test-exp (list-ref alist 1))
                  (shouldbe-num (list-ref alist 2)))
              (let ((result-num
                     (integer-power test-num test-exp)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "test-num=~a, test-exp=~a, "
                        test-num test-exp))
                      (err-3
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe-num result-num)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-num result-num)
                     sub-name
                     (string-append
                      err-1 err-2 err-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (integer-power-mod this-number this-exponent this-mod)
  (begin
    (cond
     ((= this-exponent 0)
      (begin
        1
        ))
     ((= this-exponent 1)
      (begin
        (modulo this-number this-mod)
        ))
     ((< this-exponent 0)
      (begin
        -1
        ))
     (else
      (begin
        (let ((result-num this-number)
              (max-iter (- this-exponent 1)))
          (begin
            (do ((ii 0 (+ ii 1)))
                ((>= ii max-iter))
              (begin
                (set!
                 result-num
                 (modulo
                  (* result-num this-number)
                  this-mod))
                ))
            (modulo result-num this-mod)
            ))
        )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-integer-power-mod-1 result-hash-table)
 (begin
   (let ((sub-name "test-integer-power-mod-1")
         (test-list
          (list
           (list 10 0 100 1) (list 11 0 100 1)
           (list 12 0 100 1)
           (list 10 1 100 10) (list 11 1 100 11)
           (list 12 1 100 12)
           (list 10 2 100 0) (list 11 2 100 21)
           (list 12 2 100 44)
           (list 2 2 100 4) (list 2 3 100 8)
           (list 2 4 100 16) (list 2 5 100 32)
           (list 2 6 100 64) (list 2 7 100 28)
           (list 2 8 100 56) (list 2 9 100 12)
           (list 2 10 100 24)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-num (list-ref alist 0))
                  (test-exp (list-ref alist 1))
                  (test-mod (list-ref alist 2))
                  (shouldbe-num (list-ref alist 3)))
              (let ((result-num
                     (integer-power-mod
                      test-num test-exp test-mod)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "test-num=~a, test-exp=~a, mod=~a, "
                        test-num test-exp test-mod))
                      (err-3
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe-num result-num)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-num result-num)
                     sub-name
                     (string-append
                      err-1 err-2 err-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop max-num)
  (begin
    (let ((sum 0))
      (begin
        (do ((nn 1 (+ nn 1)))
            ((> nn max-num))
          (begin
            (let ((this-pow (integer-power nn nn)))
              (begin
                (set! sum (+ sum this-pow))
                ))
            ))

        (let ((dlist
               (digits-module:split-digits-list sum)))
          (let ((dlen (length dlist))
                (final-list (list)))
            (begin
              (do ((ii (- dlen 1) (- ii 1)))
                  ((< ii (- dlen 10)))
                (begin
                  (set!
                   final-list
                   (cons (list-ref dlist ii) final-list))
                  ))

              (display
               (ice-9-format:format
                #f "last ten digits of 1^1 + 2^2 + 3^3 + ... "))
              (display
               (ice-9-format:format
                #f "+ ~:d^~:d = ~a~%"
                max-num max-num final-list))
              )))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop-2 max-num)
  (begin
    (let ((sum 0)
          (max-mod (integer-power 10 11)))
      (begin
        (do ((nn 1 (+ nn 1)))
            ((> nn max-num))
          (begin
            (let ((this-pow
                   (integer-power-mod nn nn max-mod)))
              (begin
                (set! sum
                      (modulo
                       (+ sum this-pow) max-mod))
                ))
            ))

        (let ((dlist
               (digits-module:split-digits-list sum)))
          (let ((dlen (length dlist))
                (final-list (list)))
            (begin
              (do ((ii (- dlen 1) (- ii 1)))
                  ((< ii (- dlen 10)))
                (begin
                  (set!
                   final-list
                   (cons (list-ref dlist ii) final-list))
                  ))

              (display
               (ice-9-format:format
                #f "last ten digits of 1^1 + 2^2 + 3^3 + ... "))
              (display
               (ice-9-format:format
                #f "+ ~:d^~:d = ~a (modulo)~%"
                max-num max-num final-list))
              )))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "The series, 1^1 + 2^2 + 3^3 + ... + 10^10 "))
    (display
     (format #f "= 10405071317.~%"))
    (newline)
    (display
     (format #f "Find the last ten digits of the series, "))
    (display
     (format #f "1^1 + 2^2 + 3^3 + ...~%"))
    (display
     (format #f "+ 1000^1000.~%"))
    (display
     (format #f "see https://projecteuler.net/problem=48~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((max-num 10))
      (begin
        (sub-main-loop max-num)
        ))

    (newline)
    (let ((max-num 1000))
      (begin
        (sub-main-loop max-num)
        ))

    (newline)

    (let ((max-num 1000))
      (begin
        (display
         (format
          #f "keep track of the last 11 digits only~%"))
        (sub-main-loop-2 1000)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 48 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (load "digits-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

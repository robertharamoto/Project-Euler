#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 50                                     ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated June 14, 2022                                ###
;;;###                                                       ###
;;;###  updated March 6, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### prime-module for is-prime-array? function
(use-modules ((prime-module)
              :renamer (symbol-prefix-proc 'prime-module:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
;;; make a list of primes less than or equal to n
;;; sieve of eratosthenes method
(define (make-prime-list max-num)
  (begin
    (let ((intermediate-array (make-array 0 (1+ max-num)))
          (result-list (list)))
      (begin
        (do ((ii 0 (1+ ii)))
            ((> ii max-num))
          (begin
            (array-set! intermediate-array ii ii)
            ))

        (do ((ii 2 (1+ ii)))
            ((> ii max-num))
          (begin
            (let ((this-num
                   (array-ref intermediate-array ii)))
              (begin
                (if (= this-num ii)
                    (begin
                      (set! result-list (cons ii result-list))

                      (do ((jj (+ ii ii) (+ jj ii)))
                          ((> jj max-num))
                        (begin
                          (array-set! intermediate-array -1 jj)
                          ))
                      ))
                ))
            ))

        (reverse result-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-make-prime-list-1 result-hash-table)
 (begin
   (let ((sub-name "test-make-prime-list-1")
         (test-list
          (list
           (list 2 (list 2)) (list 3 (list 2 3)) (list 4 (list 2 3))
           (list 5 (list 2 3 5)) (list 6 (list 2 3 5))
           (list 7 (list 2 3 5 7)) (list 8 (list 2 3 5 7))
           (list 9 (list 2 3 5 7)) (list 10 (list 2 3 5 7))
           (list 11 (list 2 3 5 7 11)) (list 13 (list 2 3 5 7 11 13))
           (list 17 (list 2 3 5 7 11 13 17))
           (list 19 (list 2 3 5 7 11 13 17 19))
           (list 23 (list 2 3 5 7 11 13 17 19 23))
           (list 31 (list 2 3 5 7 11 13 17 19 23 29 31))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((test-num (list-ref this-list 0))
                  (shouldbe-list (list-ref this-list 1)))
              (let ((result-list (make-prime-list test-num)))
                (let ((slen (length shouldbe-list))
                      (rlen (length result-list)))
                  (let ((err-1
                         (format
                          #f "~a :: (~a) error : test-num=~a, "
                          sub-name test-label-index test-num))
                        (err-2
                         (format
                          #f "shouldbe len=~a, result len=~a"
                          slen rlen))
                        (err-3
                         (format
                          #f "shouldbe=~a, result=~a, "
                          shouldbe-list result-list)))
                    (begin
                      (unittest2:assert?
                       (equal? slen rlen)
                       sub-name
                       (string-append err-1 err-2)
                       result-hash-table)

                      (if (> slen 0)
                          (begin
                            (for-each
                             (lambda (s-elem)
                               (begin
                                 (let ((mflag
                                        (member s-elem result-list))
                                       (err-4
                                        (format
                                         #f "missing element ~a" s-elem)))
                                   (begin
                                     (unittest2:assert?
                                      (not (equal? mflag #f))
                                      sub-name
                                      (string-append
                                       err-1 err-3 err-4)
                                      result-hash-table)
                                     ))
                                 )) shouldbe-list)
                            ))
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (list-to-sum-string llist)
  (begin
    (let ((this-string
           (string-join
            (map
             (lambda (this-elem)
               (begin
                 (ice-9-format:format #f "~:d" this-elem)
                 )) llist)
            " + ")))
      (begin
        this-string
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-list-to-sum-string-1 result-hash-table)
 (begin
   (let ((sub-name "test-list-to-sum-string-1")
         (test-list
          (list
           (list (list 1) "1")
           (list (list 1 2) "1 + 2")
           (list (list 1 2 3) "1 + 2 + 3")
           (list (list 4 5 6 7) "4 + 5 + 6 + 7")
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((input-list (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result
                     (list-to-sum-string input-list)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : input-num=~a, "
                        sub-name test-label-index input-list))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop max-num max-sum)
  (begin
    (let ((prime-list (make-prime-list max-num))
          (sum 0)
          (sum-list (list))
          (sum-count 0))
      (let ((prime-vector (list->vector prime-list))
            (prime-array (list->array 1 prime-list)))
        (let ((vec-len (vector-length prime-vector)))
          (begin
            (do ((ii-start 0 (+ ii-start 1)))
                ((>= ii-start vec-len))
              (begin
                (let ((inner-break-flag #f)
                      (current-sum 0)
                      (current-list (list))
                      (current-length 0))
                  (begin
                    (do ((jj ii-start (+ jj 1)))
                        ((or (>= jj vec-len)
                             (equal? inner-break-flag #t)))
                      (begin
                        (let ((this-prime
                               (vector-ref prime-vector jj)))
                          (begin
                            (set!
                             current-sum
                             (+ current-sum this-prime))
                            (set!
                             current-list
                             (cons this-prime current-list))
                            (set!
                             current-length
                             (+ current-length 1))

                            (if (> current-sum max-sum)
                                (begin
                                  (set! inner-break-flag #t))
                                (begin
                                  (if (and
                                       (> current-length sum-count)
                                       (prime-module:is-array-prime?
                                        current-sum prime-array))
                                      (begin
                                        (set!
                                         sum current-sum)
                                        (set!
                                         sum-list (reverse current-list))
                                        (set!
                                         sum-count current-length)
                                        ))
                                  ))
                            ))
                        ))
                    ))
                ))

            (if (<= sum 0)
                (begin
                  (display
                   (ice-9-format:format
                    #f "no sum found (for numbers less than ~:d)~%"
                    max-num)))
                (begin
                  (display
                   (ice-9-format:format
                    #f "the sum of the most consecutive primes (~:d) "
                    sum-count))
                  (display
                   (ice-9-format:format
                    #f "less than ~:d is:~%"
                    max-sum))
                  (display
                   (ice-9-format:format
                    #f "    ~:d = ~a~%"
                    sum (list-to-sum-string sum-list)))
                  ))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "The prime 41, can be written as "))
    (display
     (format #f "the sum of six~%"))
    (display
     (format #f "consecutive primes:~%"))
    (newline)
    (display
     (format #f "41 = 2 + 3 + 5 + 7 + 11 + 13~%"))
    (newline)
    (display
     (format #f "This is the longest sum of "))
    (display
     (format #f "consecutive primes that~%"))
    (display
     (format #f "adds to a prime below one-hundred. The "))
    (display
     (format #f "longest sum of~%"))
    (display
     (format #f "consecutive primes below one-thousand "))
    (display
     (format #f "that adds to a prime,~%"))
    (display
     (format #f "contains 21 terms, and is equal to 953.~%"))
    (newline)
    (display
     (format #f "Which prime, below one-million, can be "))
    (display
     (format #f "written as the sum~%"))
    (display
     (format #f "of the most consecutive primes?~%"))
    (display
     (format #f "see https://projecteuler.net/problem=50~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((max-num 100))
      (begin
        (sub-main-loop max-num max-num)
        ))

    (newline)
    (let ((max-num 1000000))
      (begin
        (sub-main-loop max-num max-num)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 50 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (load "prime-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (force-output)

          (newline)


          (force-output)

          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

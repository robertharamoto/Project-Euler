#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 43                                     ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated June 14, 2022                                ###
;;;###                                                       ###
;;;###  updated March 5, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### srfi-1 for fold function
(use-modules ((srfi srfi-1)
              :renamer (symbol-prefix-proc 'srfi-1:)))

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (reverse-vector-k-to-n this-vector k)
  (begin
    (let ((vlen (vector-length this-vector))
          (result-vector (vector-copy this-vector)))
      (let ((index-diff (- vlen k)))
        (begin
          (cond
           ((< index-diff 0)
            (begin
              result-vector
              ))
           (else
            (begin
              (let ((ii1 k)
                    (ii2 (- vlen 1))
                    (half-diff
                     (euclidean/ index-diff 2)))
                (begin
                  (do ((jj 0 (+ jj 1)))
                      ((or (>= jj half-diff)
                           (>= ii1 ii2)
                           (>= ii1 vlen)
                           (< ii2 0)
                           ))
                    (begin
                      (let ((v1 (vector-ref result-vector ii1))
                            (v2 (vector-ref result-vector ii2)))
                        (begin
                          (vector-set! result-vector ii1 v2)
                          (vector-set! result-vector ii2 v1)
                          (set! ii1 (+ ii1 1))
                          (set! ii2 (- ii2 1))
                          ))
                      ))
                  result-vector
                  ))
              )))
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (test-reverse-vector-k-to-n-1)
  (begin
    (let ((sub-name "test-reverse-vector-k-to-n-1")
          (test-list
           (list
            (list (vector 0 1 2) 1 (vector 0 2 1))
            (list (vector 0 1 2) 0 (vector 2 1 0))
            (list (vector 0 1 2 3) 2 (vector 0 1 3 2))
            (list (vector 0 1 2 3) 1 (vector 0 3 2 1))
            (list (vector 0 1 2 3) 0 (vector 3 2 1 0))
            (list (vector 0 1 2 3 4) 3 (vector 0 1 2 4 3))
            (list (vector 0 1 2 3 4) 2 (vector 0 1 4 3 2))
            (list (vector 0 1 2 3 4) 1 (vector 0 4 3 2 1))
            (list (vector 0 1 2 3 4) 0 (vector 4 3 2 1 0))
            ))
          (test-label-index 0))
      (begin
        (for-each
         (lambda (this-list)
           (begin
             (let ((test-vec (list-ref this-list 0))
                   (test-ii (list-ref this-list 1))
                   (shouldbe-vec (list-ref this-list 2)))
               (let ((result-vec (reverse-vector-k-to-n test-vec test-ii)))
                 (let ((err-1
                        (format
                         #f "~a :: (~a) error : test-vec=~a, "
                         sub-name test-label-index test-vec))
                       (err-2
                        (format
                         #f "shouldbe=~a, result=~a"
                         shouldbe-vec result-vec)))
                   (begin
                     (unittest2:assert?
                      (equal? shouldbe-vec result-vec)
                      sub-name
                      (string-append err-1 err-2)
                      result-hash-table)
                     ))
                 ))
             (set! test-label-index (1+ test-label-index))
             )) test-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
;;; note assumes that this-vector is sorted in ascending order
(define (next-lexicographic-permutation this-vector)
  (begin
    (let ((vlen (1- (vector-length this-vector)))
          (result-vector (vector-copy this-vector))
          (kk 0)
          (aakk 0)
          (ll 0)
          (aall 0)
          (break-flag #f)
          (permutation-exists #f))
      (begin
        ;;; 1) find largest kk such that a[kk] < a[kk+1]
        (do ((ii 0 (1+ ii)))
            ((>= ii vlen))
          (begin
            (let ((v1 (vector-ref result-vector ii))
                  (v2 (vector-ref result-vector (1+ ii))))
              (begin
                (if (< v1 v2)
                    (begin
                      (set! permutation-exists #t)
                      (set! kk ii)
                      (set! aakk v1)
                      ))
                ))
            ))

        ;;; 2) find the largest ll such that a[kk] < a[ll]
        (if (equal? permutation-exists #t)
            (begin
              (do ((ii (+ kk 1) (1+ ii)))
                  ((> ii vlen))
                (begin
                  (let ((v1 (vector-ref result-vector ii)))
                    (begin
                      (if (< aakk v1)
                          (begin
                            (set! ll ii)
                            (set! aall v1)
                            ))
                      ))
                  ))

              ;;; 3) swap a[kk] with a[ll]
              (vector-set! result-vector kk aall)
              (vector-set! result-vector ll aakk)

              ;;; 4) reverse the sequence from (k+1) on
              (let ((final-result
                     (reverse-vector-k-to-n
                      result-vector (+ kk 1))))
                (begin
                  final-result
                  )))
            (begin
              #f
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-next-lexicographic-permutation-1 result-hash-table)
 (begin
   (let ((sub-name "test-next-lexicographic-permutation-1")
         (test-list
          (list
           (list (vector 0 1 2) (vector 0 2 1))
           (list (vector 0 2 1) (vector 1 0 2))
           (list (vector 1 0 2) (vector 1 2 0))
           (list (vector 1 2 0) (vector 2 0 1))
           (list (vector 2 0 1) (vector 2 1 0))
           (list (vector 0 1 2 3 4 5) (vector 0 1 2 3 5 4))
           (list (vector 0 1 2 3 5 4) (vector 0 1 2 4 3 5))
           (list (vector 0 1 2 4 3 5) (vector 0 1 2 4 5 3))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((test-vec (list-ref this-list 0))
                  (shouldbe-vec (list-ref this-list 1)))
              (let ((result-vec
                     (next-lexicographic-permutation test-vec)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : test-vec=~a, "
                        sub-name test-label-index test-vec))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe-vec result-vec)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-vec result-vec)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; most significant digit in position 0
(define (turn-digit-list-to-number dlist)
  (begin
    (let ((this-num
           (srfi-1:fold
            (lambda (this-elem prev-elem)
              (begin
                (+ this-elem (* 10 prev-elem))
                ))
            0 dlist)))
      (begin
        this-num
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-turn-digit-list-to-number-1 result-hash-table)
 (begin
   (let ((sub-name "test-turn-digit-list-to-number-1")
         (test-list
          (list
           (list (list 1 2) 12)
           (list (list 2 1) 21)
           (list (list 1 2 3) 123)
           (list (list 3 2 1) 321)
           (list (list 1 2 3 4) 1234)
           (list (list 4 3 2 1) 4321)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((input-list (list-ref alist 0))
                  (shouldbe-num (list-ref alist 1)))
              (let ((result-num
                     (turn-digit-list-to-number input-list)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : input-list=~a, "
                        sub-name test-label-index input-list))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe-num result-num)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-num result-num)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; return empty list if not a circular prime
(define (check-pandigital-property digit-list prime-list)
  (begin
    (let ((dlen (length digit-list))
          (result-list (list))
          (break-flag #f)
          (d4 (list-ref digit-list 3))
          (d6 (list-ref digit-list 5)))
      (begin
        (if (and
             (even? d4)
             (or
              (= d6 0)
              (= d6 5)))
            (begin
              (let ((first-num (list-ref digit-list 1))
                    (second-num (list-ref digit-list 2)))
                (begin
                  (do ((ii 3 (1+ ii)))
                      ((or (>= ii dlen)
                           (equal? break-flag #t)))
                    (begin
                      (let ((third-num (list-ref digit-list ii)))
                        (let ((this-num
                               (+ (* 100 first-num)
                                  (* 10 second-num)
                                  third-num)))
                          (let ((this-prime
                                 (list-ref prime-list (- ii 2))))
                            (begin
                              (if (zero?
                                   (modulo this-num this-prime))
                                  (begin
                                    (set!
                                     result-list
                                     (cons (list this-num this-prime)
                                           result-list)))
                                  (begin
                                    (set! result-list #f)
                                    (set! break-flag #t)
                                    ))

                              (set! first-num second-num)
                              (set! second-num third-num)
                              ))
                          ))
                      ))
                  ))

              (if (equal? result-list #f)
                  (begin
                    #f)
                  (begin
                    (reverse result-list)
                    )))
            (begin
              #f
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-check-pandigital-property-1 result-hash-table)
 (begin
   (let ((sub-name "test-check-pandigital-property-1")
         (test-list
          (list
           (list (list 1 4 0 6 3 5 7 2 8 9)
                 (list (list 406 2) (list 063 3) (list 635 5)
                       (list 357 7) (list 572 11) (list 728 13)
                       (list 289 17)))
           ))
         (prime-list (list 1 2 3 5 7 11 13 17))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((input-list (list-ref alist 0))
                  (shouldbe-list (list-ref alist 1)))
              (let ((result-list
                     (check-pandigital-property
                      input-list prime-list)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : input-list=~a, "
                        sub-name test-label-index input-list))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe-list result-list)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-list result-list)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (exchange-aa-for-bb-list pan-sub-list aa bb)
  (begin
    (let ((llen (length pan-sub-list))
          (result-list (list)))
      (begin
        (do ((ii 0 (1+ ii)))
            ((>= ii llen))
          (begin
            (let ((ii-elem (list-ref pan-sub-list ii)))
              (begin
                (cond
                 ((equal? ii-elem aa)
                  (begin
                    (set! result-list (cons bb result-list))
                    ))
                 ((equal? ii-elem bb)
                  (begin
                    (set! result-list (cons aa result-list))
                    ))
                 (else
                  (begin
                    (set! result-list (cons ii-elem result-list))
                    )))
                ))
            ))
        (reverse result-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-exchange-aa-for-bb-list-1 result-hash-table)
 (begin
   (let ((sub-name "test-exchange-aa-for-bb-list-1")
         (test-list
          (list
           (list (list 1 4 0 6 3 5 7 2 8 9)
                 1 9
                 (list 9 4 0 6 3 5 7 2 8 1))
           (list (list 1 4 0 6 3 5 7 2 8 9)
                 4 8
                 (list 1 8 0 6 3 5 7 2 4 9))
           (list (list 1 4 0 6 3 5 7 2 8 9)
                 0 2
                 (list 1 4 2 6 3 5 7 0 8 9))
           (list (list 1 4 0 6 3 5 7 2 8 9)
                 6 7
                 (list 1 4 0 7 3 5 6 2 8 9))
           (list (list 1 4 0 6 3 5 7 2 8 9)
                 3 5
                 (list 1 4 0 6 5 3 7 2 8 9))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((input-list (list-ref alist 0))
                  (aa (list-ref alist 1))
                  (bb (list-ref alist 2))
                  (shouldbe-list (list-ref alist 3)))
              (let ((result-list
                     (exchange-aa-for-bb-list input-list aa bb)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : input-list=~a, "
                        sub-name test-label-index input-list))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe-list result-list)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-list result-list)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (make-fixed-list pan-sub-list dd-4 dd-6)
  (begin
    (let ((sub-1-list (list-head pan-sub-list 3))
          (tail-2-list (list-tail pan-sub-list 3)))
      (let ((sub-2-list (list-head tail-2-list 1))
            (sub-3-list (list-tail tail-2-list 1)))
        (let ((result-list
               (append
                sub-1-list (list dd-4)
                sub-2-list (list dd-6)
                sub-3-list)))
          (begin
            result-list
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-make-fixed-list-1 result-hash-table)
 (begin
   (let ((sub-name "test-make-fixed-list-1")
         (test-list
          (list
           (list (list 0 1 2 4 6 7 8 9)
                 3 5
                 (list 0 1 2 3 4 5 6 7 8 9))
           (list (list 0 1 3 4 6 7 8 5)
                 2 9
                 (list 0 1 3 2 4 9 6 7 8 5))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((input-list (list-ref alist 0))
                  (aa (list-ref alist 1))
                  (bb (list-ref alist 2))
                  (shouldbe-list (list-ref alist 3)))
              (let ((result-list
                     (make-fixed-list input-list aa bb)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : input-list=~a, "
                        sub-name test-label-index input-list))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe-list result-list)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-list result-list)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define-syntax sub-process-list-macro
  (syntax-rules ()
    ((sub-process-list-macro
      rr-list prime-list
      pan-count sum debug-flag)
     (begin
       (let ((result-list
              (check-pandigital-property
               rr-list prime-list)))
         (begin
           (if (and
                (list? result-list)
                (> (length result-list) 0))
               (begin
                 (let ((next-num
                        (turn-digit-list-to-number
                         rr-list)))
                   (begin
                     (set! pan-count (1+ pan-count))
                     (set! sum (+ sum next-num))
                     (if (equal? debug-flag #t)
                         (begin
                           (let ((this-index 2))
                             (begin
                               (display
                                (ice-9-format:format
                                 #f "(~:d)  ~a pandigital~%"
                                 pan-count next-num))
                               (for-each
                                (lambda (this-list)
                                  (begin
                                    (let ((num
                                           (list-ref this-list 0))
                                          (prime
                                           (list-ref this-list 1)))
                                      (begin
                                        (display
                                         (format
                                          #f "    (~a) ~a is divisible "
                                          this-index num))
                                        (display
                                         (format
                                          #f "by ~a~%" prime))
                                        (force-output)
                                        (set! this-index (1+ this-index))
                                        ))
                                    )) result-list)
                               ))
                           ))
                     ))
                 ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax process-valid-vector-macro
  (syntax-rules ()
    ((process-valid-vector-macro
      next-vector prime-list
      pan-count sum debug-flag)
     (begin
       (let ((sub-list (vector->list next-vector)))
         (let ((full-list (make-fixed-list sub-list 2 5)))
           (let ((full-0-list
                  (exchange-aa-for-bb-list full-list 5 0)))
             (let ((ll-list-list
                    (list
                     (list-copy full-list)
                     (exchange-aa-for-bb-list full-list 0 2)
                     (exchange-aa-for-bb-list full-list 4 2)
                     (exchange-aa-for-bb-list full-list 6 2)
                     (exchange-aa-for-bb-list full-list 8 2)
                     (list-copy full-0-list)
                     (exchange-aa-for-bb-list full-0-list 4 2)
                     (exchange-aa-for-bb-list full-0-list 6 2)
                     (exchange-aa-for-bb-list full-0-list 8 2))))
               (begin
                 (for-each
                  (lambda (rr-list)
                    (begin
                      (sub-process-list-macro
                       rr-list prime-list pan-count sum debug-flag)
                      )) ll-list-list)
                 ))
             )))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop debug-flag)
  (begin
    (let ((counter 0)
          (sum 0)
          (pan-count 0)
          (prime-list (list 1 2 3 5 7 11 13 17))
          (init-vector (vector 0 1 3 4 6 7 8 9))
          (this-vector #f)
          (break-flag #f))
      (begin
        (set! this-vector init-vector)

        (while (equal? break-flag #f)
               (begin
                 (let ((next-vector
                        (next-lexicographic-permutation this-vector)))
                   (begin
                     (if (equal? next-vector #f)
                         (begin
                           (set! break-flag #t))
                         (begin
                           (set! this-vector next-vector)
                           (set! counter (1+ counter))

                           (process-valid-vector-macro
                            next-vector prime-list pan-count sum debug-flag)
                           ))
                     ))
                 ))

        (display
         (ice-9-format:format
          #f "Sum of all 0 to 9 pandigital numbers = ~:d~%" sum))
        (display
         (ice-9-format:format
          #f "There were ~:d pandigital numbers out of~%"
          pan-count))
        (display
         (ice-9-format:format
          #f "~:d configurations that satisfied the property.~%"
          counter))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "The number, 1406357289, is a 0 to 9 "))
    (display
     (format #f "pandigital number~%"))
    (display
     (format #f "because it is made up of each of the "))
    (display
     (format #f "digits 0 to 9 in~%"))
    (display
     (format #f "some order, but it also has a rather "))
    (display
     (format #f "interesting sub-string~%"))
    (display
     (format #f "divisibility property.~%"))
    (newline)
    (display
     (format #f "Let d1 be the 1st digit, d2 be the 2nd "))
    (display
     (format #f "digit, and so on.~%"))
    (display
     (format #f "In this way, we note the following:~%"))
    (display
     (format #f "d2d3d4=406 is divisible by 2~%"))
    (display
     (format #f "d3d4d5=063 is divisible by 3~%"))
    (display
     (format #f "d4d5d6=635 is divisible by 5~%"))
    (display
     (format #f "d5d6d7=357 is divisible by 7~%"))
    (display
     (format #f "d6d7d8=572 is divisible by 11~%"))
    (display
     (format #f "d7d8d9=728 is divisible by 13~%"))
    (display
     (format #f "d8d9d10=289 is divisible by 17~%"))
    (newline)
    (display
     (format #f "Find the sum of all 0 to 9 pandigital "))
    (display
     (format #f "numbers with this property.~%"))
    (newline)
    (display
     (format #f "The key to making this algorithm go "))
    (display
     (format #f "fast is to reduce~%"))
    (display
     (format #f "the number of permutations to check. "))
    (display
     (format #f "Since there are 10~%"))
    (display
     (format #f "digits, there are 10!=3.6 million "))
    (display
     (format #f "permutations.~%"))
    (display
     (format #f "By fixing 1 digit, we can see that "))
    (display
     (format #f "there will be just~%"))
    (display
     (format #f "9!=362,880 permutations, and by "))
    (display
     (format #f "fixing 2 digits, there~%"))
    (display
     (format #f "are 8!=40,320 permutations. From the "))
    (display
     (format #f "above, we can see~%"))
    (display
     (format #f "that d4 must be even, and d6 must be "))
    (display
     (format #f "0 or 5. So the~%"))
    (display
     (format #f "algorithm fixes 2 and 5 (keeping them "))
    (display
     (format #f "at position 4 and~%"))
    (display
     (format #f "position 6,) and permutes the rest of "))
    (display
     (format #f "the digits.  In~%"))
    (display
     (format #f "order to examine all possibilites an "))
    (display
     (format #f "exchange routine switches~%"))
    (display
     (format #f "2 with 0, 4, 6, 8, and switches 5 "))
    (display
     (format #f "with 0. This will~%"))
    (display
     (format #f "ensure that wherever 0 was, 2 will "))
    (display
     (format #f "now occupy the~%"))
    (display
     (format #f "0-space, and since 0 will be permutated "))
    (display
     (format #f "through all positions,~%"))
    (display
     (format #f "so will 2.~%"))
    (display
     (format #f "see https://projecteuler.net/problem=43~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((debug-flag #t))
      (begin
        (sub-main-loop debug-flag)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 43 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

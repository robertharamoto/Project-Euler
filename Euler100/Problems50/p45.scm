#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 45                                     ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated June 14, 2022                                ###
;;;###                                                       ###
;;;###  updated March 5, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (triangular-number this-num)
  (begin
    (let ((t1 (+ this-num 1)))
      (let ((t2 (* this-num t1)))
        (let ((t3 (euclidean/ t2 2)))
          (begin
            t3
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-triangular-number-1 result-hash-table)
 (begin
   (let ((sub-name "test-triangular-number-1")
         (test-list
          (list
           (list 1 1) (list 2 3) (list 3 6) (list 4 10)
           (list 5 15)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-num (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result
                     (triangular-number test-num)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : test-num=~a, "
                        sub-name test-label-index test-num))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (pentagonal-number this-num)
  (begin
    (let ((t1 (- (* 3 this-num) 1)))
      (let ((t2 (* this-num t1)))
        (let ((t3 (euclidean/ t2 2)))
          (begin
            t3
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-pentagonal-number-1 result-hash-table)
 (begin
   (let ((sub-name "test-pentagonal-number-1")
         (test-list
          (list
           (list 1 1) (list 2 5) (list 3 12) (list 4 22)
           (list 5 35) (list 6 51) (list 7 70) (list 8 92)
           (list 9 117) (list 10 145)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-num (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result
                     (pentagonal-number test-num)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : test-num=~a, "
                        sub-name test-label-index test-num))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (hexagonal-number this-num)
  (begin
    (let ((t1 (- (* 2 this-num) 1)))
      (let ((t2 (* this-num t1)))
        (begin
          t2
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-hexagonal-number-1 result-hash-table)
 (begin
   (let ((sub-name "test-hexagonal-number-1")
         (test-list
          (list
           (list 1 1) (list 2 6) (list 3 15) (list 4 28)
           (list 5 45)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-num (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result
                     (hexagonal-number test-num)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : test-num=~a, "
                        sub-name test-label-index test-num))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (populate-htable! p-htable this-function max-num)
  (begin
    (do ((jj 1 (+ jj 1)))
        ((> jj max-num))
      (begin
        (let ((this-num (this-function jj)))
          (begin
            (hash-set! p-htable this-num jj)
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop max-num debug-flag)
  (begin
    (let ((counter 0)
          (p-htable (make-hash-table max-num))
          (h-htable (make-hash-table max-num))
          (break-flag #f))
      (begin
        (populate-htable! p-htable pentagonal-number max-num)
        (populate-htable! h-htable hexagonal-number max-num)

        (do ((nn 2 (+ nn 1)))
            ((> nn max-num))
          (begin
            (let ((tr-num (triangular-number nn)))
              (let ((p-flag (hash-ref p-htable tr-num #f))
                    (h-flag (hash-ref h-htable tr-num #f)))
                (begin
                  (if (and (not (equal? p-flag #f))
                           (not (equal? h-flag #f)))
                      (begin
                        (set! counter (+ counter 1))
                        (if (equal? debug-flag #t)
                            (begin
                              (display
                               (ice-9-format:format
                                #f "(~:d)  T(~:d) = P(~:d) "
                                counter nn p-flag))
                              (display
                               (ice-9-format:format
                                #f "= H(~:d) = ~:d~%"
                                h-flag tr-num))
                              (force-output)
                              ))
                        ))
                  )))
            ))

        (display
         (ice-9-format:format
          #f "number of triples found = ~:d, "
          counter))
        (display
         (ice-9-format:format
          #f "for indexes less than ~:d~%"
          max-num))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Triangle, pentagonal, and hexagonal "))
    (display
     (format #f "numbers are generated~%"))
    (display
     (format #f "by the following formulae:~%"))
    (newline)
    (display
     (format #f "Triangle:  Tn=n(n+1)/2     1, 3, 6, 10, 15, ...~%"))
    (display
     (format #f "Pentagonal:  Pn=n(3n1)/2   1, 5, 12, 22, 35, ...~%"))
    (display
     (format #f "Hexagonal:  Hn=n(2n1)      1, 6, 15, 28, 45, ...~%"))
    (newline)
    (display
     (format #f "It can be verified that T285 = P165 "))
    (display
     (format #f "= H143 = 40755.~%"))
    (newline)
    (display
     (format #f "Find the next triangle number that is "))
    (display
     (format #f "also pentagonal and~%"))
    (display
     (format #f "hexagonal.~%"))
    (display
     (format #f "see https://projecteuler.net/problem=45~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((max-num 100)
          (debug-flag #t))
      (begin
        (sub-main-loop max-num debug-flag)
        ))

    (newline)
    (let ((max-num 1000000)
          (debug-flag #t))
      (begin
        (sub-main-loop max-num debug-flag)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 45 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

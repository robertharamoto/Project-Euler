#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 41                                     ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated June 14, 2022                                ###
;;;###                                                       ###
;;;###  updated March 6, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### prime-module for make-prime-array and is-prime-array? functions
(use-modules ((prime-module)
              :renamer (symbol-prefix-proc 'prime-module:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (reverse-vector-k-to-n this-vector k)
  (begin
    (let ((vlen (vector-length this-vector))
          (result-vector (vector-copy this-vector)))
      (let ((index-diff (- vlen k)))
        (begin
          (cond
           ((< index-diff 0)
            (begin
              result-vector
              ))
           (else
            (begin
              (let ((ii1 k)
                    (ii2 (- vlen 1))
                    (half-diff (euclidean/ index-diff 2)))
                (begin
                  (do ((jj 0 (+ jj 1)))
                      ((or (>= jj half-diff)
                           (>= ii1 ii2)
                           (>= ii1 vlen)
                           (< ii2 0)
                           ))
                    (begin
                      (let ((v1
                             (vector-ref result-vector ii1))
                            (v2
                             (vector-ref result-vector ii2)))
                        (begin
                          (vector-set! result-vector ii1 v2)
                          (vector-set! result-vector ii2 v1)
                          (set! ii1 (+ ii1 1))
                          (set! ii2 (- ii2 1))
                          ))
                      ))
                  result-vector
                  ))
              )))
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-reverse-vector-k-to-n-1 result-hash-table)
 (begin
   (let ((sub-name "test-reverse-vector-k-to-n-1")
         (test-list
          (list
           (list (vector 0 1 2) 1 (vector 0 2 1))
           (list (vector 0 1 2) 0 (vector 2 1 0))
           (list (vector 0 1 2 3) 2 (vector 0 1 3 2))
           (list (vector 0 1 2 3) 1 (vector 0 3 2 1))
           (list (vector 0 1 2 3) 0 (vector 3 2 1 0))
           (list (vector 0 1 2 3 4) 3 (vector 0 1 2 4 3))
           (list (vector 0 1 2 3 4) 2 (vector 0 1 4 3 2))
           (list (vector 0 1 2 3 4) 1 (vector 0 4 3 2 1))
           (list (vector 0 1 2 3 4) 0 (vector 4 3 2 1 0))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((test-vec (list-ref this-list 0))
                  (test-ii (list-ref this-list 1))
                  (shouldbe-vec (list-ref this-list 2)))
              (let ((result-vec
                     (reverse-vector-k-to-n test-vec test-ii)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "test-vec=~a, test-ii=~a, "
                        test-vec test-ii))
                      (err-3
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe-vec result-vec)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-vec result-vec)
                     sub-name
                     (string-append
                      err-1 err-2 err-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (+ test-label-index 1))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; note assumes that this-vector is sorted in ascending order
(define (next-lexicographic-permutation this-vector)
  (begin
    (let ((vlen (vector-length this-vector))
          (result-vector (vector-copy this-vector))
          (kk 0)
          (aakk 0)
          (ll 0)
          (aall 0)
          (break-flag #f)
          (permutation-exists #f))
      (begin
        ;;; 1) find largest kk such that a[kk] < a[kk+1]
        (do ((ii 0 (+ ii 1)))
            ((>= ii (- vlen 1)))
          (begin
            (let ((v1 (vector-ref result-vector ii))
                  (v2 (vector-ref result-vector (+ ii 1))))
              (begin
                (if (< v1 v2)
                    (begin
                      (set! permutation-exists #t)
                      (set! kk ii)
                      (set! aakk v1)
                      ))
                ))
            ))

        ;;; 2) find the largest ll such that a[kk] < a[ll]
        (if (equal? permutation-exists #t)
            (begin
              (do ((ii (+ kk 1) (+ ii 1)))
                  ((> ii (- vlen 1)))
                (begin
                  (let ((v1 (vector-ref result-vector ii)))
                    (begin
                      (if (< aakk v1)
                          (begin
                            (set! ll ii)
                            (set! aall v1)
                            ))
                      ))
                  ))

              ;;; 3) swap a[kk] with a[ll]
              (vector-set! result-vector kk aall)
              (vector-set! result-vector ll aakk)

              ;;; 4) reverse the sequence from (k+1) on
              (let ((final-result
                     (reverse-vector-k-to-n result-vector (+ kk 1))))
                (begin
                  final-result
                  )))
            (begin
              #f
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-next-lexicographic-permutation-1 result-hash-table)
 (begin
   (let ((sub-name "test-next-lexicographic-permutation-1")
         (test-list
          (list
           (list (vector 0 1 2) (vector 0 2 1))
           (list (vector 0 2 1) (vector 1 0 2))
           (list (vector 1 0 2) (vector 1 2 0))
           (list (vector 1 2 0) (vector 2 0 1))
           (list (vector 2 0 1) (vector 2 1 0))
           (list (vector 2 1 0) #f)
           (list (vector 0 1 2 3 4 5) (vector 0 1 2 3 5 4))
           (list (vector 0 1 2 3 5 4) (vector 0 1 2 4 3 5))
           (list (vector 0 1 2 4 3 5) (vector 0 1 2 4 5 3))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((test-vec (list-ref this-list 0))
                  (shouldbe-vec (list-ref this-list 1)))
              (let ((result-vec
                     (next-lexicographic-permutation test-vec)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : test-vec=~a, "
                        sub-name test-label-index test-vec))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe-vec result-vec)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-vec result-vec)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; most significant digit in position 0
(define (turn-digit-vector-to-number dvector max-len)
  (begin
    (let ((result-num 0))
      (begin
        (do ((ii 0 (1+ ii)))
            ((>= ii max-len))
          (begin
            (let ((this-elem (vector-ref dvector ii)))
              (begin
                (set!
                 result-num
                 (+ (* 10 result-num) this-elem))
                ))
            ))
        result-num
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-turn-digit-vector-to-number-1 result-hash-table)
 (begin
   (let ((sub-name "test-turn-digit-vector-to-number-1")
         (test-list
          (list
           (list (list 1 2) 12)
           (list (list 2 1) 21)
           (list (list 1 2 3) 123)
           (list (list 3 2 1) 321)
           (list (list 1 2 3 4) 1234)
           (list (list 4 3 2 1) 4321)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((input-list (list-ref alist 0))
                  (shouldbe-num (list-ref alist 1)))
              (let ((test-vector (list->vector input-list))
                    (tlen (length input-list)))
                (let ((result-num
                       (turn-digit-vector-to-number
                        test-vector tlen)))
                  (let ((err-1
                         (format
                          #f "~a :: (~a) error : input-list=~a, "
                          sub-name test-label-index input-list))
                        (err-2
                         (format
                          #f "shouldbe=~a, result=~a~%"
                          shouldbe-num result-num)))
                    (begin
                      (unittest2:assert?
                       (equal? shouldbe-num result-num)
                       sub-name
                       (string-append err-1 err-2)
                       result-hash-table)
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; most significant digit in position 0
(define (largest-n-digit-prime nn prime-array)
  (begin
    (let ((result-num 0)
          (dlist (list))
          (max-index (- nn 1))
          (iter-vector #f))
      (begin
        (do ((ii 1 (1+ ii)))
            ((> ii nn))
          (begin
            (set! dlist (cons ii dlist))
            ))
        (set! dlist (reverse dlist))
        (set! iter-vector (list->vector dlist))

        (while (not (equal? iter-vector #f))
               (begin
                 (let ((this-num
                        (turn-digit-vector-to-number iter-vector nn)))
                   (begin
                     (if (and
                          (odd? this-num)
                          (> this-num result-num)
                          (prime-module:is-array-prime?
                           this-num prime-array))
                         (begin
                           (set! result-num this-num)
                           ))
                     (let ((next-vector
                            (next-lexicographic-permutation iter-vector)))
                       (begin
                         (set! iter-vector next-vector)
                         ))
                     ))
                 ))
        result-num
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-largest-n-digit-prime-1 result-hash-table)
 (begin
   (let ((sub-name "test-largest-n-digit-prime-1")
         (test-list
          (list
           (list 2 0)
           (list 3 0)
           (list 4 4231)
           ))
         (prime-array
          (prime-module:make-prime-array 100))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-nn (list-ref alist 0))
                  (shouldbe-num (list-ref alist 1)))
              (let ((result-num
                     (largest-n-digit-prime test-nn prime-array)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : test-nn=~a, "
                        sub-name test-label-index test-nn))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe-num result-num)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-num result-num)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "We shall say that an n-digit number "))
    (display
     (format #f "is pandigital if it~%"))
    (display
     (format #f "makes use of all the digits 1 to n "))
    (display
     (format #f "exactly once. For~%"))
    (display
     (format #f "example, 2143 is a 4-digit pandigital "))
    (display
     (format #f "and is also prime.~%"))
    (newline)
    (display
     (format #f "What is the largest n-digit pandigital "))
    (display
     (format #f "prime that exists?~%"))
    (display
     (format #f "see https://projecteuler.net/problem=41~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((counter 0)
          (largest-nn 0)
          (largest-prime-pandigital 0)
          (prime-array
           (prime-module:make-prime-array 500000))
          (break-flag #f))
      (begin
        (do ((ii 9 (- ii 1)))
            ((or (< ii 2)
                 (equal? break-flag #t)))
          (begin
            (let ((rnum
                   (largest-n-digit-prime ii prime-array)))
              (begin
                (if (> rnum largest-prime-pandigital)
                    (begin
                      (set! largest-prime-pandigital rnum)
                      (set! largest-nn ii)
                      (set! break-flag #f)
                      ))
                ))
            ))

        (display
         (ice-9-format:format
          #f "largest n-digit pandigital prime "))
        (display
         (ice-9-format:format
          #f "= ~:d (~:d digits)~%"
          largest-prime-pandigital largest-nn))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 41 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (load "prime-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

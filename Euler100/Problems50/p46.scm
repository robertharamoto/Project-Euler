#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 46                                     ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated June 14, 2022                                ###
;;;###                                                       ###
;;;###  updated March 5, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### prime-module for is-prime? function
(use-modules ((prime-module)
              :renamer (symbol-prefix-proc 'prime-module:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (make-prime-list max-num)
  (begin
    (let ((result-list (list 2)))
      (begin
        (do ((ii 3 (+ ii 2)))
            ((> ii max-num))
          (begin
            (if (prime-module:is-prime? ii)
                (begin
                  (set! result-list (cons ii result-list))
                  ))
            ))

        (reverse result-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-make-prime-list-1 result-hash-table)
 (begin
   (let ((sub-name "test-make-prime-list-1")
         (test-list
          (list
           (list 5 (list 2 3 5))
           (list 10 (list 2 3 5 7))
           (list 20 (list 2 3 5 7 11 13 17 19))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((test-num (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result (make-prime-list test-num)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : test-num=~a, "
                        sub-name test-label-index test-num))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (populate-htable! sq-htable max-num)
  (begin
    (do ((ii 1 (+ ii 1)))
        ((> ii max-num))
      (begin
        (let ((tnum (* 2 ii ii)))
          (begin
            (hash-set! sq-htable tnum ii)
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (sum-and-square-list this-num prime-list sq-htable)
  (begin
    (let ((break-flag #f)
          (result-list #f)
          (dlen (length prime-list)))
      (begin
        (do ((ii 0 (+ ii 1)))
            ((or (>= ii dlen)
                 (equal? break-flag #t)))
          (begin
            (let ((this-prime (list-ref prime-list ii)))
              (let ((this-diff (- this-num this-prime)))
                (begin
                  (if (< this-diff 0)
                      (begin
                        (set! break-flag #t))
                      (begin
                        (let ((d-flag
                               (hash-ref sq-htable this-diff #f)))
                          (begin
                            (if (not (equal? d-flag #f))
                                (begin
                                  (set! break-flag #t)
                                  (set!
                                   result-list
                                   (list this-num this-prime d-flag))
                                  ))
                            ))
                        ))
                  )))
            ))
        result-list
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-sum-and-square-list-1 result-hash-table)
 (begin
   (let ((sub-name "test-sum-and-square-list-1")
         (test-list
          (list
           (list 9 100 (list 9 7 1))
           (list 15 100 (list 15 7 2))
           (list 21 100 (list 21 3 3))
           (list 25 100 (list 25 7 3))
           (list 27 100 (list 27 19 2))
           (list 33 100 (list 33 31 1))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((test-num (list-ref this-list 0))
                  (test-max (list-ref this-list 1))
                  (shouldbe (list-ref this-list 2))
                  (sq-htable (make-hash-table 100)))
              (let ((prime-list (make-prime-list test-max)))
                (begin
                  (populate-htable! sq-htable test-max)

                  (let ((result
                         (sum-and-square-list
                          test-num prime-list sq-htable)))
                    (let ((err-1
                           (format
                            #f "~a :: (~a) error : "
                            sub-name test-label-index))
                          (err-2
                           (format
                            #f "test-num=~a, test-max=~a, "
                            test-num test-max))
                          (err-3
                           (format
                            #f "shouldbe=~a, result=~a"
                            shouldbe result)))
                      (begin
                        (unittest2:assert?
                         (equal? shouldbe result)
                         sub-name
                         (string-append
                          err-1 err-2 err-3)
                         result-hash-table)
                        )))
                  )))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop max-num debug-flag)
  (begin
    (let ((smallest-odd-composite max-num)
          (sq-htable (make-hash-table max-num))
          (prime-list (make-prime-list max-num))
          (break-flag #f))
      (begin
        (populate-htable! sq-htable max-num)

        (do ((nn 9 (+ nn 2)))
            ((or (> nn max-num)
                 (equal? break-flag #t)))
          (begin
            (if (not
                 (prime-module:is-prime? nn))
                (begin
                  (let ((result-list
                         (sum-and-square-list
                          nn prime-list sq-htable)))
                    (begin
                      (if (equal? result-list #f)
                          (begin
                            (if (< nn smallest-odd-composite)
                                (begin
                                  (set! break-flag #t)
                                  (set! smallest-odd-composite nn)
                                  ))
                            ))
                      ))
                  ))
            ))

        (if (equal? smallest-odd-composite max-num)
            (begin
              (display
               (ice-9-format:format
                #f "no smallest composite found "))
              (display
               (ice-9-format:format
                #f "(for numbers less than ~:d)~%"
                max-num)))
            (begin
              (display
               (ice-9-format:format
                #f "smallest composite found = ~:d "
                smallest-odd-composite))
              (display
               (ice-9-format:format
                #f "(for numbers less than ~:d)~%"
                max-num))
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "It was proposed by Christian Goldbach "))
    (display
     (format #f "that every odd~%"))
    (display
     (format #f "composite number can be written as the "))
    (display
     (format #f "sum of a prime~%"))
    (display
     (format #f "and twice a square.~%"))
    (newline)
    (display
     (format #f "9 = 7 + 2x1^2~%"))
    (display
     (format #f "15 = 7 + 2x2^2~%"))
    (display
     (format #f "21 = 3 + 2x3^2~%"))
    (display
     (format #f "25 = 7 + 2x3^2~%"))
    (display
     (format #f "27 = 19 + 2x2^2~%"))
    (display
     (format #f "33 = 31 + 2x1^2~%"))
    (newline)
    (display
     (format #f "It turns out that the conjecture "))
    (display
     (format #f "was false.~%"))
    (newline)
    (display
     (format #f "What is the smallest odd composite that "))
    (display
     (format #f "cannot be written~%"))
    (display
     (format #f "as the sum of a prime and twice a square?~%"))
    (display
     (format #f "see https://projecteuler.net/problem=46~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((max-num 1000)
          (debug-flag #t))
      (begin
        (sub-main-loop max-num debug-flag)
        ))

    (newline)
    (let ((max-num 100000)
          (debug-flag #t))
      (begin
        (sub-main-loop max-num debug-flag)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 46 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (load "prime-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

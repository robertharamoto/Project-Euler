#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 47                                     ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated June 14, 2022                                ###
;;;###                                                       ###
;;;###  updated March 6, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### srfi-1 for fold function
(use-modules ((srfi srfi-1)
              :renamer (symbol-prefix-proc 'srfi-1:)))

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### prime-module for is-prime? function
(use-modules ((prime-module)
              :renamer (symbol-prefix-proc 'prime-module:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (divide-all-factors this-num this-factor)
  (begin
    (let ((ll-num this-num)
          (acc-list (list)))
      (begin
        (while (and
                (zero? (modulo ll-num this-factor))
                (> ll-num 1))
               (begin
                 (set! ll-num (euclidean/ ll-num this-factor))
                 (set! acc-list (cons this-factor acc-list))
                 ))

        (list ll-num acc-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-divide-all-factors-1 result-hash-table)
 (begin
   (let ((sub-name "test-divide-all-factors-1")
         (test-list
          (list
           (list 0 2 (list 0 (list)))
           (list 2 2 (list 1 (list 2)))
           (list 4 2 (list 1 (list 2 2)))
           (list 8 2 (list 1 (list 2 2 2)))
           (list 12 2 (list 3 (list 2 2)))
           (list 20 2 (list 5 (list 2 2)))
           (list 33 3 (list 11 (list 3)))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((num (list-ref this-list 0))
                  (factor (list-ref this-list 1))
                  (shouldbe (list-ref this-list 2)))
              (let ((result (divide-all-factors num factor)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "num=~a, factor=~a, "
                        num factor))
                      (err-3
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append
                      err-1 err-2 err-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; populate a hash with numbers and a list of prime factors
;;; sieve of eratosthenes method
(define (populate-prime-div-list-hash! factor-htable max-num)
  (begin
    (let ((prime-array
           (make-array 0 (1+ max-num)))
          (factor-list-array
           (make-array (list) (1+ max-num))))
      (begin
        (hash-clear! factor-htable)

        (do ((ii 0 (1+ ii)))
            ((> ii max-num))
          (begin
            (array-set! prime-array ii ii)
            ))

        (do ((ii 2 (1+ ii)))
            ((> ii max-num))
          (begin
            (let ((ii-num (array-ref prime-array ii)))
              (begin
                (if (equal? ii-num ii)
                    (begin
                      (do ((jj (+ ii ii) (+ jj ii)))
                          ((> jj max-num))
                        (begin
                          (let ((jj-num
                                 (array-ref prime-array jj)))
                            (begin
                              (array-set! prime-array -1 jj)

                              (let ((rlist
                                     (divide-all-factors jj ii)))
                                (let ((fact-list (cadr rlist))
                                      (this-list
                                       (array-ref
                                        factor-list-array jj)))
                                  (begin
                                    (array-set!
                                     factor-list-array
                                     (append this-list fact-list) jj)
                                    )))
                              ))
                          ))
                      ))
                ))

            (let ((this-list (array-ref factor-list-array ii))
                  (tflag (array-ref prime-array ii)))
              (begin
                (if (and (equal? this-list (list))
                         (equal? tflag ii))
                    (begin
                      (hash-set! factor-htable ii (list ii)))
                    (begin
                      (hash-set! factor-htable ii this-list)
                      ))
                ))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-populate-prime-div-list-hash-1 result-hash-table)
 (begin
   (let ((sub-name "test-populate-prime-div-list-hash-1")
         (test-list
          (list
           (list 2 (list 2)) (list 3 (list 3))
           (list 4 (list 2 2))
           (list 5 (list 5)) (list 6 (list 2 3))
           (list 7 (list 7)) (list 8 (list 2 2 2))
           (list 9 (list 3 3)) (list 10 (list 2 5))
           (list 11 (list 11)) (list 12 (list 2 2 3))
           (list 13 (list 13)) (list 14 (list 2 7))
           (list 15 (list 3 5)) (list 16 (list 2 2 2 2))
           (list 17 (list 17)) (list 18 (list 2 3 3))
           (list 19 (list 19)) (list 20 (list 2 2 5))
           ))
         (factors-htable (make-hash-table 20))
         (max-num 20)
         (test-label-index 0))
     (begin
       (populate-prime-div-list-hash! factors-htable max-num)

       (for-each
        (lambda (this-list)
          (begin
            (let ((num (list-ref this-list 0))
                  (shouldbe-list (list-ref this-list 1)))
              (let ((result-list
                     (hash-ref factors-htable num #f)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : num=~a, "
                        sub-name test-label-index num))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a~%"
                        shouldbe-list result-list)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-list result-list)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define-syntax divide-process-macro
  (syntax-rules ()
    ((divide-process-macro
      ll-num div-factor constant-number result-list)
     (begin
       (if (prime-module:is-prime? div-factor)
           (begin
             (let ((partial-list
                    (divide-all-factors ll-num div-factor)))
               (begin
                 (set! ll-num (car partial-list))
                 (set!
                  result-list
                  (append result-list (cadr partial-list)))
                 ))
             ))

       (let ((this-divisor
              (euclidean/ constant-number div-factor)))
         (begin
           (if (and (> this-divisor div-factor)
                    (equal? (memq this-divisor result-list) #f)
                    (prime-module:is-prime? this-divisor))
               (begin
                 (let ((partial-list
                        (divide-all-factors ll-num this-divisor)))
                   (begin
                     (set! ll-num (car partial-list))
                     (set!
                      result-list
                      (append result-list (cadr partial-list)))
                     ))
                 ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (prime-factor-list input-number)
  (begin
    (if (<= input-number 1)
        (begin
          (list))
        (begin
          (let ((result-list (list))
                (constant-number input-number)
                (ll-num input-number)
                (ll-max
                 (1+ (exact-integer-sqrt input-number))))
            (begin
              (if (even? constant-number)
                  (begin
                    (divide-process-macro
                     ll-num 2 constant-number result-list)
                    ))

              (do ((ii 3 (+ ii 2)))
                  ((or (> ii ll-max) (<= ll-num 1)))
                (begin
                  (if (zero? (modulo constant-number ii))
                      (begin
                        (divide-process-macro
                         ll-num ii constant-number result-list)
                        ))
                  ))

              (if (< (length result-list) 1)
                  (begin
                    (list input-number))
                  (begin
                    (sort result-list <)
                    ))
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-prime-factor-list-1 result-hash-table)
 (begin
   (let ((sub-name "test-prime-factor-list-1")
         (test-list
          (list
           (list 2 (list 2)) (list 3 (list 3))
           (list 4 (list 2 2)) (list 5 (list 5))
           (list 6 (list 2 3)) (list 7 (list 7))
           (list 8 (list 2 2 2)) (list 9 (list 3 3))
           (list 10 (list 2 5)) (list 11 (list 11))
           (list 12 (list 2 2 3)) (list 13 (list 13))
           (list 14 (list 2 7)) (list 15 (list 3 5))
           (list 16 (list 2 2 2 2)) (list 17 (list 17))
           (list 18 (list 2 3 3)) (list 19 (list 19))
           (list 20 (list 2 2 5)) (list 21 (list 3 7))
           (list 22 (list 2 11)) (list 100 (list 2 2 5 5))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-num (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result (prime-factor-list test-num)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : test-num=~a, "
                        sub-name test-label-index test-num))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a~%"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (contains-nn-distinct-primes p1-list nn)
  (begin
    (let ((p1-distinct
           (srfi-1:delete-duplicates p1-list)))
      (let ((p1-len (length p1-distinct)))
        (begin
          (= nn p1-len)
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-contains-nn-distinct-primes-1 result-hash-table)
 (begin
   (let ((sub-name "test-contains-nn-distinct-primes-1")
         (test-list
          (list
           (list (list 2 7) 2 #t)
           (list (list 3 5) 2 #t)
           (list (list 2 3 5) 3 #t)
           (list (list 7 11 13) 3 #t)
           (list (list 2 2 7 23) 3 #t)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((input-list (list-ref this-list 0))
                  (test-seq (list-ref this-list 1))
                  (shouldbe (list-ref this-list 2)))
              (let ((result
                     (contains-nn-distinct-primes
                      input-list test-seq)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "input-list=~a, test-seq=~a, "
                        input-list test-seq))
                      (err-3
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append
                      err-1 err-2 err-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop max-num seq-num debug-flag)
  (begin
    (let ((break-flag #f)
          (factors-htable (make-hash-table max-num))
          (seq-counter 0)
          (seq-list (list)))
      (begin
        (populate-prime-div-list-hash! factors-htable max-num)

        (do ((nn 1 (1+ nn)))
            ((or (> nn max-num)
                 (equal? break-flag #t)))
          (begin
            (let ((this-list
                   (hash-ref factors-htable nn (list))))
              (begin
                (if (contains-nn-distinct-primes
                     this-list seq-num)
                    (begin
                      (set! seq-counter (+ seq-counter 1))
                      (set!
                       seq-list
                       (cons (list nn this-list) seq-list))
                      (if (>= seq-counter seq-num)
                          (begin
                            (set! break-flag #t)
                            (for-each
                             (lambda (this-list)
                               (begin
                                 (let ((this-num
                                        (list-ref this-list 0))
                                       (plist
                                        (list-ref this-list 1)))
                                   (let ((pstring
                                          (string-join
                                           (map
                                            (lambda (num)
                                              (begin
                                                (ice-9-format:format #f "~:d" num)
                                                )) plist)
                                           " x ")))
                                     (begin
                                       (display
                                        (ice-9-format:format
                                         #f "~:d = ~a~%"
                                         this-num pstring))
                                       )))
                                 )) (reverse seq-list))
                            )))
                    (begin
                      (set! seq-counter 0)
                      (set! seq-list (list))
                      ))
                ))
            ))

        (if (< seq-counter seq-num)
            (begin
              (display
               (ice-9-format:format
                #f "no ~:d sequence found "
                seq-num))
              (display
               (ice-9-format:format
                #f "(for numbers less than ~:d)~%"
                max-num)))
            (begin
              (display
               (ice-9-format:format
                #f "min ~:d sequence found~%" seq-num))
              ))
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "The first two consecutive numbers "))
    (display
     (format #f "to have two distinct~%"))
    (display
     (format #f "prime factors are:~%"))
    (newline)
    (display
     (format #f "14 = 2 x 7~%"))
    (display
     (format #f "15 = 3 x 5~%"))
    (newline)
    (display
     (format #f "The first three consecutive numbers "))
    (display
     (format #f "to have three distinct~%"))
    (display
     (format #f "prime factors are:~%"))
    (display
     (format #f "644 = 2^2 x 7 x 23~%"))
    (display
     (format #f "645 = 3 x 5 x 43~%"))
    (display
     (format #f "646 = 2 x 17 x 19~%"))
    (newline)
    (display
     (format #f "Find the first four consecutive "))
    (display
     (format #f "integers to have four~%"))
    (display
     (format #f "distinct primes factors. What is the "))
    (display
     (format #f "first of these~%"))
    (display
     (format #f "numbers?~%"))
    (newline)
    (display
     (format #f "see https://projecteuler.net/problem=47~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((max-num 1000)
          (nn 3)
          (debug-flag #t))
      (begin
        (sub-main-loop max-num nn debug-flag)
        ))

    (newline)
    (let ((max-num 200000)
          (nn 4)
          (debug-flag #t))
      (begin
        (sub-main-loop max-num nn debug-flag)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 47 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (load "prime-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

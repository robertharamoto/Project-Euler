#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 42                                     ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated June 14, 2022                                ###
;;;###                                                       ###
;;;###  updated March 5, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 rdelim - for read-delimited functions
(use-modules ((ice-9 rdelim)
              :renamer (symbol-prefix-proc 'ice-9-rdelim:)))

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (populate-alpha-hash! alpha-htable)
  (begin
    (let ((alist
           (list
            (list #\a 1) (list #\b 2) (list #\c 3) (list #\d 4)
            (list #\e 5) (list #\f 6) (list #\g 7) (list #\h 8)
            (list #\i 9) (list #\j 10) (list #\k 11) (list #\l 12)
            (list #\m 13) (list #\n 14) (list #\o 15) (list #\p 16)
            (list #\q 17) (list #\r 18) (list #\s 19) (list #\t 20)
            (list #\u 21) (list #\v 22) (list #\w 23) (list #\x 24)
            (list #\y 25) (list #\z 26))))
      (begin
        (for-each
         (lambda (this-list)
           (begin
             (let ((tchar (list-ref this-list 0))
                   (tnum (list-ref this-list 1)))
               (begin
                 (hash-set! alpha-htable tchar tnum)
                 ))
             )) alist)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (populate-triangle-hash! triangle-htable max-num)
  (begin
    (let ((max-tn 0))
      (begin
        (do ((ii max-num (- ii 1)))
            ((<= ii 0))
          (begin
            (let ((tn
                   (euclidean/ (* ii (+ ii 1)) 2)))
              (begin
                (if (> tn max-tn)
                    (begin
                      (set! max-tn tn)
                      ))
                (hash-set! triangle-htable tn ii)
                ))
            ))
        max-tn
        ))
    ))

;;;#############################################################
;;;#############################################################
;;; returns a list of names, lower case
(define (read-in-file fname)
  (begin
    (let ((name-list (list))
          (counter 0))
      (begin
        (if (file-exists? fname)
            (begin
              (with-input-from-file fname
                (lambda ()
                  (begin
                    (do ((line
                          (ice-9-rdelim:read-delimited ",\n")
                          (ice-9-rdelim:read-delimited ",\n")))
                        ((eof-object? line))
                      (begin
                        (if (not (eof-object? line))
                            (begin
                              (let ((this-string
                                     (string-downcase
                                      (string-delete #\" line))))
                                (begin
                                  (set!
                                   name-list
                                   (cons this-string name-list))
                                  (set! counter (1+ counter))
                                  ))
                              ))
                        ))
                    )))

              (display
               (ice-9-format:format
                #f "number of names read = ~:d~%" counter))
              (force-output)
              (set! name-list (sort name-list string-ci<?))
              name-list)
            (begin
              (list)
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
;;; returns a list of names, lower case
(define (process-list name-list max-num)
  (begin
    (let ((list-counter 0)
          (tn-name-count 0)
          (char-htable (make-hash-table 30))
          (triangle-htable
           (make-hash-table max-num)))
      (let ((max-triangle-num
             (populate-triangle-hash!
              triangle-htable max-num)))
        (begin
          (populate-alpha-hash! char-htable)

          (for-each
           (lambda (this-name)
             (begin
               (set! list-counter (1+ list-counter))

               (let ((slist (string->list this-name))
                     (this-sum 0))
                 (begin
                   (for-each
                    (lambda (this-char)
                      (begin
                        (let ((this-value
                               (hash-ref char-htable this-char 0)))
                          (begin
                            (set! this-sum (+ this-sum this-value))
                            ))
                        )) slist)

                   (if (> this-sum max-triangle-num)
                       (begin
                         (display
                          (ice-9-format:format
                           #f "process-list() error for name=~a~%"
                           this-name))
                         (display
                          (ice-9-format:format
                           #f "this-sum=~:d > max-triangle-num=~:d~%"
                           this-sum max-triangle-num))
                         (display
                          (ice-9-format:format
                           #f "re-run with larger max-num=~:d~%"
                           max-num))
                         (display
                          (format #f "quitting...~%"))
                         (quit)
                         ))

                   (let ((tnum
                          (hash-ref triangle-htable this-sum #f)))
                     (begin
                       (if (not (equal? tnum #f))
                           (begin
                             (set! tn-name-count (1+ tn-name-count))
                             ))
                       ))
                   ))
               )) name-list)

          tn-name-count
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-process-list-1 result-hash-table)
 (begin
   (let ((sub-name "test-process-list-1")
         (test-list
          (list
           (list (list "sky") 1000 1)
           (list (list "a" "sky") 1000 2)
           (list (list "a" "b" "sky") 1000 2)
           (list (list "a" "b" "c" "sky") 1000 3)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((input-list (list-ref this-list 0))
                  (max-num (list-ref this-list 1))
                  (shouldbe (list-ref this-list 2)))
              (let ((result
                     (process-list input-list max-num)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : input-list=~a, "
                        sub-name test-label-index input-list))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop filename max-num)
  (begin
    (let ((words-list (read-in-file filename)))
      (let ((triangle-word-count
             (process-list words-list max-num)))
        (begin
          (display
           (ice-9-format:format
            #f "number of triangle words = ~:d~%"
            triangle-word-count))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "The nth term of the sequence of triangle "))
    (display
     (format #f "numbers is given by,~%"))
    (display
     (format #f "tn = ½n(n+1); so the first ten triangle "))
    (display
     (format #f "numbers are:~%"))
    (newline)
    (display
     (format #f "  1, 3, 6, 10, 15, 21, 28, 36, 45, 55, ...~%"))
    (newline)
    (display
     (format #f "By converting each letter in a word to "))
    (display
     (format #f "a number corresponding~%"))
    (display
     (format #f "to its alphabetical position and adding "))
    (display
     (format #f "these values we form~%"))
    (display
     (format #f "a word value. For example, the word "))
    (display
     (format #f "value for SKY is~%"))
    (display
     (format #f "19 + 11 + 25 = 55 = t10. If the word "))
    (display
     (format #f "value is a triangle~%"))
    (display
     (format #f "number then we shall call the word a "))
    (display
     (format #f "triangle word.~%"))
    (newline)
    (display
     (format #f "Using words.txt (right click and "))
    (display
     (format #f "'Save Link/Target As...'),~%"))
    (display
     (format #f "a 16K text file containing nearly "))
    (display
     (format #f "two-thousand common English~%"))
    (display
     (format #f "words, how many are triangle words?~%"))
    (display
     (format #f "see https://projecteuler.net/problem=42~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((filename "words.txt")
          (max-num 10000))
      (begin
        (sub-main-loop filename max-num)
        (newline)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 42 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

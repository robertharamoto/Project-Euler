#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 18                                     ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated June 10, 2022                                ###
;;;###                                                       ###
;;;###  updated March 4, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
;;; note: this algorithm expects a triangular list of lists
;;; each list element must be 1 greater than the list element before it
(define (display-triangle triangle-list-list margin)
  (begin
    (let ((nelements (length triangle-list-list))
          (margin-string (make-string margin #\space))
          (spacer-string "  "))
      (begin
        (do ((ii 0 (+ ii 1)))
            ((>= ii nelements))
          (begin
            (let ((max-index ii)
                  (this-level-list
                   (list-ref triangle-list-list ii))
                  (tstring ""))
              (begin
                (let ((extra-space
                       (make-string
                        (- nelements ii) #\space)))
                  (begin
                    (set!
                     tstring
                     (string-append
                      margin-string extra-space))
                    (for-each
                     (lambda (this-elem)
                       (begin
                         (let ((elem-string
                                (format #f "~a" this-elem)))
                           (begin
                             (set!
                              tstring
                              (string-append
                               tstring elem-string spacer-string))
                             ))
                         )) this-level-list)
                    (display
                     (format #f "~a~%"
                             (string-trim-right tstring)))
                    ))
                ))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
;;; note: this algorithm expects a triangular list of lists
;;; each list element must be 1 greater than the list element before it
;;; acc-list (list sum1 current-index-1 (list sum-path-1))
;;; acc-list contains largest path so far
(define (brute-force-largest-sum-triangle triangle-list-list)
  (define (local-recursive-loop
           parent-row parent-col max-row
           triangle-list curr-list acc-list)
    (begin
      (cond
       ((>= parent-row max-row)
        (begin
          (let ((curr-sum (list-ref curr-list 0))
                (npath (+ (list-ref acc-list 0) 1))
                (acc-sum (list-ref acc-list 1)))
            (begin
              (if (> curr-sum acc-sum)
                  (begin
                    (list
                     npath curr-sum
                     (reverse (list-ref curr-list 1))))
                  (begin
                    (list npath acc-sum (list-ref acc-list 2))
                    ))
              ))
          ))
       (else
        (begin
          (let ((child-row (+ parent-row 1))
                (child1-col parent-col)
                (child2-col (+ parent-col 1))
                (this-row
                 (list-ref triangle-list parent-row)))
            (let ((curr-sum (list-ref curr-list 0))
                  (curr-path (list-ref curr-list 1))
                  (c1-elem (list-ref this-row child1-col)))
              (let ((next-curr-list
                     (list (+ curr-sum c1-elem)
                           (cons c1-elem curr-path))))
                (begin
                  ;;;; child 1
                  (let ((next-acc-list
                         (local-recursive-loop
                          child-row child1-col max-row
                          triangle-list next-curr-list acc-list)))
                    (begin
                      (set! acc-list next-acc-list)
                      ))

                  (if (<= child2-col parent-row)
                      (begin
                        (let ((c2-elem
                               (list-ref this-row child2-col)))
                          (let ((next-curr-list
                                 (list (+ curr-sum c2-elem)
                                       (cons c2-elem curr-path))))
                            (let ((next-acc-list
                                   (local-recursive-loop
                                    child-row child2-col max-row
                                    triangle-list next-curr-list acc-list)))
                              (begin
                                (set! acc-list next-acc-list)
                                ))
                            ))
                        ))
                  acc-list
                  ))
              ))
          )))
      ))
  (begin
    (let ((nelements (length triangle-list-list)))
      (let ((acc-list (list 0 0 (list)))
            (curr-list (list 0 (list))))
        (let ((max-sum-paths
               (local-recursive-loop
                0 0 nelements
                triangle-list-list curr-list acc-list)))
          (begin
            max-sum-paths
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-brute-force-largest-sum-triangle-1 result-hash-table)
 (begin
   (let ((sub-name "test-brute-force-largest-sum-triangle-1")
         (test-list
          (list
           (list (list (list 1) (list 2 3) (list 4 5 6))
                 (list 4 10 (list 1 3 6)))
           (list (list (list 1) (list 3 2) (list 6 5 4))
                 (list 4 10 (list 1 3 6)))
           (list (list (list 3) (list 7 4) (list 2 4 6) (list 8 5 9 3))
                 (list 8 23 (list 3 7 4 9)))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-triangle (list-ref alist 0))
                  (shouldbe-list (list-ref alist 1)))
              (let ((result-list
                     (brute-force-largest-sum-triangle test-triangle)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : test-triangle=~a, "
                        sub-name test-label-index test-triangle))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe-list result-list)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-list result-list)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; triangle list in the form (list (list elem1) (list elem1 elem2), ...)
(define (dynamic-method triangle-list-list)
  (begin
    (let ((llen (length triangle-list-list))
          (prev-result-list (list)))
      (begin
        (cond
         ((<= llen 2)
          (begin
            -1
            ))
         (else
          (begin
            (let ((this-row
                   (list-ref triangle-list-list (- llen 1))))
              (begin
                (do ((ii (- llen 2) (- ii 1)))
                    ((< ii 0))
                  (begin
                    (let ((prev-row
                           (list-ref triangle-list-list ii))
                          (this-len (length this-row))
                          (previous-list (list)))
                      (begin
                        (do ((jj 0 (1+ jj)))
                            ((>= jj (- this-len 1)))
                          (begin
                            (let ((elem1 (list-ref this-row jj))
                                  (elem2 (list-ref this-row (+ jj 1)))
                                  (prev-elem (list-ref prev-row jj)))
                              (let ((max-elem (max elem1 elem2)))
                                (let ((new-prev-elem
                                       (+ prev-elem max-elem)))
                                  (begin
                                    (set!
                                     previous-list
                                     (cons new-prev-elem previous-list))
                                    ))
                                ))
                            ))

                        (set! prev-result-list
                              (reverse previous-list))
                        (set! this-row
                              prev-result-list)
                        ))
                    ))
                (car prev-result-list)
                ))
            )))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-dynamic-method-1 result-hash-table)
 (begin
   (let ((sub-name "test-dynamic-method-1")
         (test-list
          (list
           (list (list (list 1) (list 2 3) (list 4 5 6)) 10)
           (list (list (list 1) (list 3 2) (list 6 5 4)) 10)
           (list (list (list 3) (list 7 4)
                       (list 2 4 6) (list 8 5 9 3)) 23)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-triangle (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result (dynamic-method test-triangle)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : test-triangle=~a, "
                        sub-name test-label-index test-triangle))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a~%"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (list-to-sum-string llist)
  (begin
    (let ((stmp
           (string-join
            (map
             (lambda (this-num)
               (begin
                 (ice-9-format:format
                  #f "~:d" this-num)
                 ))
             llist)
            " + ")))
      (begin
        stmp
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-list-to-sum-string-1 result-hash-table)
 (begin
   (let ((sub-name "test-list-to-sum-string-1")
         (test-list
          (list
           (list (list 1) "1")
           (list (list 1 2) "1 + 2")
           (list (list 1 2 3) "1 + 2 + 3")
           (list (list 4 5 6 7) "4 + 5 + 6 + 7")
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((input-list (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result (list-to-sum-string input-list)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : input-list=~a, "
                        sub-name test-label-index input-list))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a~%"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (string-ci=? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "By starting at the top of the "))
    (display
     (format #f "triangle below~%"))
    (display
     (format #f "and moving to adjacent numbers on "))
    (display
     (format #f "the row below,~%"))
    (display
     (format #f "the maximum total from top to "))
    (display
     (format #f "bottom is 23.~%"))
    (newline)
    (display
     (format #f "That is, 3 + 7 + 4 + 9 = 23.~%"))
    (display
     (format #f "Find the maximum total from top "))
    (display
     (format #f "to bottom~%"))
    (display
     (format #f "of the triangle below:~%"))
    (newline)
    (display
     (format #f "NOTE: As there are only 16384 routes, "))
    (display
     (format #f "it is possible~%"))
    (display
     (format #f "to solve this problem by trying every "))
    (display
     (format #f "route. However,~%"))
    (display
     (format #f "Problem 67, is the same challenge with a "))
    (display
     (format #f "triangle~%"))
    (display
     (format #f "containing one-hundred rows; it cannot "))
    (display
     (format #f "be solved by~%"))
    (display
     (format #f "brute force, and requires a clever "))
    (display
     (format #f "method! ;o)~%"))
    (newline)
    (display
     (format #f "the solution, using a dynamic programming "))
    (display
     (format #f "method can be~%"))
    (display
     (format #f "found at https://lucidmanager.org/data-science/project-euler-18/~%"))
    (display
     (format #f "see https://projecteuler.net/problem=18~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop triangle-list-list debug-flag)
  (begin
    (if (equal? debug-flag #t)
        (begin
          (let ((result
                 (brute-force-largest-sum-triangle
                  triangle-list-list))
                (nsize
                 (length triangle-list-list)))
            (let ((npaths (list-ref result 0))
                  (sum (list-ref result 1))
                  (path (list-ref result 2)))
              (begin
                (display
                 (ice-9-format:format
                  #f "number of paths = ~:d~%" npaths))
                (display
                 (ice-9-format:format
                  #f "the largest sum : ~a = ~:d~%"
                  (list-to-sum-string path) sum))
                (force-output)
                )))
          ))
    (let ((largest-sum
           (dynamic-method triangle-list-list)))
      (begin
        (display
         (ice-9-format:format
          #f "the largest sum = ~:d "
          largest-sum))
        (display
         (ice-9-format:format
          #f "(dynamic programming method)~%"))
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((test-list-list
           (list
            (list (list 3) (list 7 4) (list 2 4 6) (list 8 5 9 3))
            (list (list 3) (list 7 4) (list 2 4 9) (list 8 5 6 3))
            ))
          (debug-flag #t))
      (begin
        (for-each
         (lambda (list1)
           (begin
             (let ((nsize (length list1)))
               (begin
                 (display-triangle list1 nsize)
                 ))
             (sub-main-loop list1 debug-flag)
             )) test-list-list)
        ))

    (newline)
    (let ((list1
           (list (list 75) (list 95 64) (list 17 47 82)
                 (list 18 35 87 10) (list 20 04 82 47 65)
                 (list 19 01 23 75 03 34) (list 88 02 77 73 07 63 67)
                 (list 99 65 04 28 06 16 70 92) (list 41 41 26 56 83 40 80 70 33)
                 (list 41 48 72 33 47 32 37 16 94 29)
                 (list 53 71 44 65 25 43 91 52 97 51 14)
                 (list 70 11 33 28 77 73 17 78 39 68 17 57)
                 (list 91 71 52 38 17 14 91 43 58 50 27 29 48)
                 (list 63 66 04 68 89 53 67 30 73 16 69 87 40 31)
                 (list 04 62 98 27 23 09 70 98 73 93 38 53 60 04 23)
                 ))
          (debug-flag #f))
      (let ((nsize (length list1)))
        (begin
          (display-triangle list1 nsize)

          (sub-main-loop list1 debug-flag)
          (newline)
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 18 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

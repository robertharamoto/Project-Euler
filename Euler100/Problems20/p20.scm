#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 20                                     ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated June 10, 2022                                ###
;;;###                                                       ###
;;;###  updated March 5, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### srfi-1 for fold function
(use-modules ((srfi srfi-1)
              :renamer (symbol-prefix-proc 'srfi-1:)))

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### digits-module for split-digits-list function
(use-modules ((digits-module)
              :renamer (symbol-prefix-proc 'digits-module:)))

;;;### timer-module for time-code function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (factorial nn)
  (begin
    (cond
     ((< nn 0)
      (begin
        #f
        ))
     ((= nn 0)
      (begin
        1
        ))
     (else
      (begin
        (* nn (factorial (- nn 1)))
        )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-factorial-1 result-hash-table)
 (begin
   (let ((sub-name "test-factorial-1")
         (test-list
          (list
           (list 0 1) (list 1 1) (list 2 2)
           (list 3 6) (list 4 24) (list 5 120)
           (list 6 720)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-num (list-ref alist 0))
                  (shouldbe-num (list-ref alist 1)))
              (let ((result-num (factorial test-num)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : num=~a, "
                        sub-name test-label-index test-num))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a~%"
                        shouldbe-num result-num)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-num result-num)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (list-to-sum-string llist)
  (begin
    (let ((stmp
           (string-join
            (map
             (lambda (this-num)
               (begin
                 (ice-9-format:format #f "~:d" this-num)
                 ))
             llist)
            " + ")))
      (begin
        stmp
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-list-to-sum-string-1 result-hash-table)
 (begin
   (let ((sub-name "test-list-to-sum-string-1")
         (test-list
          (list
           (list (list 1) "1")
           (list (list 1 2) "1 + 2")
           (list (list 1 2 3) "1 + 2 + 3")
           (list (list 4 5 6 7) "4 + 5 + 6 + 7")
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (a-list)
          (begin
            (let ((input-list (list-ref a-list 0))
                  (shouldbe (list-ref a-list 1)))
              (let ((result (list-to-sum-string input-list)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : input-list=~a, "
                        sub-name test-label-index input-list))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "n! means n x (n - 1) x ... x 3 x 2 x 1~%"))
    (newline)
    (display
     (format #f "For example, 10! = 10 x 9 x "))
    (display
     (format #f "... x 3 x 2 x 1~%"))
    (display
     (format #f "= 3,628,800,~%"))
    (display
     (format #f "and the sum of the digits in the "))
    (display
     (format #f "number 10!~%"))
    (display
     (format #f "is 3 + 6 + 2 + 8 + 8 "))
    (display
     (format #f "+ 0 + 0 = 27.~%"))
    (newline)
    (display
     (format #f "Find the sum of the digits in the "))
    (display
     (format #f "number 100!~%"))
    (display
     (format #f "see https://projecteuler.net/problem=20~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop this-num)
  (begin
    (let ((nfac (factorial this-num)))
      (let ((flist
             (digits-module:split-digits-list nfac)))
        (let ((fsum (srfi-1:fold + 0 flist))
              (fstring (list-to-sum-string flist)))
          (begin
            (display
             (ice-9-format:format
              #f "this-num = ~:d, ~:d! = ~:d~%"
              this-num this-num nfac))
            (display
             (ice-9-format:format
              #f "digit sum = ~:a = ~:d~%"
              fstring fsum))
            (force-output)
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((max-num 10))
      (begin
        (sub-main-loop max-num)
        ))

    (let ((max-num 100))
      (begin
        (sub-main-loop max-num)
        (newline)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 20 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (load "digits-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)

          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 12                                     ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated June 10, 2022                                ###
;;;###                                                       ###
;;;###  updated March 4, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (triangular-number nn)
  (begin
    (euclidean/ (* nn (+ nn 1)) 2)
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-triangular-number-1 result-hash-table)
 (begin
   (let ((sub-name "test-triangular-number-1")
         (test-list
          (list
           (list 1 1) (list 2 3) (list 3 6) (list 4 10)
           (list 5 15) (list 6 21) (list 7 28)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((test-num (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result (triangular-number test-num)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : test-num=~a, "
                        sub-name test-label-index test-num))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (divisor-list this-num)
  (begin
    (let ((div-list (list 1 this-num))
          (max-iter (1+ (exact-integer-sqrt this-num))))
      (begin
        (if (equal? this-num 1)
            (begin
              (set! div-list (list 1)))
            (begin
              (do ((ii 2 (+ ii 1)))
                  ((> ii max-iter))
                (begin
                  (if (zero? (modulo this-num ii))
                      (begin
                        (let ((other-div (euclidean/ this-num ii)))
                          (begin
                            (if (< ii other-div)
                                (begin
                                  (set! div-list (cons ii div-list ))
                                  (set! div-list (cons other-div div-list)))
                                (begin
                                  (if (= ii other-div)
                                      (begin
                                        (set! div-list (cons ii div-list))
                                        ))
                                  ))
                            ))
                        ))
                  ))
              ))
        div-list
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-divisor-list-1 result-hash-table)
 (begin
   (let ((sub-name "test-divisor-list-1")
         (test-list
          (list
           (list 1 (list 1)) (list 3 (list 1 3))
           (list 6 (list 1 2 3 6)) (list 10 (list 1 2 5 10))
           (list 15 (list 1 3 5 15)) (list 21 (list 1 3 7 21))
           (list 28 (list 1 2 4 7 14 28))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((test-num (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result
                     (sort (divisor-list test-num) <)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : test-num=~a, "
                        sub-name test-label-index test-num))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "The sequence of triangle numbers is "))
    (display
     (format #f "generated by adding~%"))
    (display
     (format #f "the natural numbers. So the 7th "))
    (display
     (format #f "triangle number would~%"))
    (display
     (format #f "be~%"))
    (display
     (format #f "1 + 2 + 3 + 4 + 5 + 6 + 7 = 28.~%"))
    (display
     (format #f "The first ten terms would be:~%"))
    (display
     (format #f "1, 3, 6, 10, 15, 21, 28, 36, 45, 55, ...~%"))
    (display
     (format #f "Let us list the factors of the "))
    (display
     (format #f "first seven triangle~%"))
    (display
     (format #f "numbers:~%"))
    (display
     (format #f "1: 1~%"))
    (display
     (format #f "3: 1,3~%"))
    (display
     (format #f "6: 1,2,3,6~%"))
    (display
     (format #f "10: 1,2,5,10~%"))
    (display
     (format #f "15: 1,3,5,15~%"))
    (display
     (format #f "21: 1,3,7,21~%"))
    (display
     (format #f "28: 1,2,4,7,14,28~%"))
    (display
     (format #f "We can see that 28 is the first triangle "))
    (display
     (format #f "number to have~%"))
    (display
     (format #f "over five divisors. What is the value "))
    (display
     (format #f "of the first triangle~%"))
    (display
     (format #f "number to have over five hundred "))
    (display
     (format #f "divisors?~%"))
    (display
     (format #f "see https://projecteuler.net/problem=12~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop max-digits max-ii)
  (begin
    (let ((max-found-flag #f)
          (found-ii 0)
          (found-num 0)
          (found-div-list (list))
          (found-div-len 0))
      (begin
        (do ((ii 1 (+ ii 1)))
            ((or (> ii max-ii)
                 (equal? max-found-flag #t)))
          (begin
            (let ((tnum (triangular-number ii)))
              (let ((divisors (divisor-list tnum)))
                (let ((llen (length divisors)))
                  (begin
                    (if (> llen max-digits)
                        (begin
                          (set! found-ii ii)
                          (set! found-num tnum)
                          (set! found-div-list
                                (sort divisors <))
                          (set! found-div-len llen)
                          (set! max-found-flag #t)
                          ))
                    ))
                ))
            ))

        (if (equal? max-found-flag #t)
            (begin
              (display
               (ice-9-format:format
                #f "(~:d) ~:d : number of divisors = ~a : divisor list = ~a~%"
                found-ii found-num found-div-len found-div-list)))
            (begin
              (display
               (ice-9-format:format
                #f "no results found for max-digits = ~:d, max-ii = ~:d~%"
                max-digits max-ii))
              ))

        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (do ((ii 1 (+ ii 1)))
        ((> ii 8))
      (begin
        (let ((tnum (triangular-number ii)))
          (let ((divisors (divisor-list tnum)))
            (begin
              (display (format #f "(~a) ~a : ~a~%" ii tnum divisors))
              )))
        ))
    (newline)

    (let ((max-digits 500)
          (max-ii 1000000))
      (begin
        (sub-main-loop max-digits max-ii)
        (newline)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 12 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 15                                     ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated June 10, 2022                                ###
;;;###                                                       ###
;;;###  updated March 4, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
;;; use this code to see the path over the verticies of the grid
(define (rec-down-count max-row max-col debug-flag)
  (define (local-counter-loop
           this-row this-col max-row max-col
           curr-list acc-list)
    (begin
      (cond
       ((and (= this-row max-row)
             (= this-col max-col))
        (begin
          (let ((this-count (+ (car acc-list) 1))
                (next-list (list-ref acc-list 1)))
            (begin
              (if (equal? debug-flag #t)
                  (begin
                    (display
                     (format
                      #f "  debug this-row/col=(~a/~a), this-count=~a, next-list=~a, curr-list=~a, acc-list=~a~%"
                      this-row this-col this-count next-list
                      curr-list acc-list))
                    ))

              (list this-count (cons curr-list next-list))
              ))
          ))
       ((or (> this-row max-row)
            (> this-col max-col))
        (begin
          (let ((next-acc
                 (local-counter-loop
                  max-row max-col max-row max-col
                  curr-list acc-list)))
            (begin
              next-acc
              ))
          ))
       (else
        (begin
          (let ((next-row this-row)
                (next-col (+ this-col 1))
                (next-curr-list curr-list)
                (second-row (+ this-row 1))
                (second-col this-col)
                (second-curr-list curr-list))
            (begin
              (if (equal? debug-flag #t)
                  (begin
                    (display
                     (format
                      #f "debug this-row/col=~a/~a, next-row/col=~a/~a, second-row/col=~a/~a, curr-list=~a, acc-list=~a~%"
                      this-row this-col next-row next-col second-row second-col
                      curr-list acc-list))
                    ))

              ;;; recursively iterate over first path
              (cond
               ((>= next-col max-col)
                (begin
                  (if (< this-row max-row)
                      (begin
                        (do ((ii this-row (+ ii 1)))
                            ((> ii max-row))
                          (begin
                            (set!
                             next-curr-list
                             (append next-curr-list
                                     (list (list ii max-col))))
                            ))

                        (let ((acc2-list
                               (local-counter-loop
                                max-row max-col max-row max-col
                                next-curr-list acc-list)))
                          (begin
                            (set! acc-list acc2-list)
                            acc-list
                            )))
                      (begin
                        acc-list
                        ))
                  ))
               (else
                (begin
                  (let ((acc2-list
                         (local-counter-loop
                          next-row next-col max-row max-col
                          (append
                           next-curr-list
                           (list (list next-row next-col)))
                          acc-list)))
                    (begin
                      (set! acc-list acc2-list)
                      ))
                  acc-list
                  )))

              ;;; second path
              (cond
               ((= second-row max-row)
                (begin
                  (if (not (= second-col next-col))
                      (begin
                        (do ((ii second-col (+ ii 1)))
                            ((> ii max-col))
                          (begin
                            (set!
                             second-curr-list
                             (append
                              second-curr-list
                              (list (list max-row ii))))
                            ))
                        (let ((acc2-list
                               (local-counter-loop
                                max-row max-col max-row max-col
                                second-curr-list acc-list)))
                          (begin
                            (set! acc-list acc2-list)
                            ))
                        acc-list)
                      (begin
                        acc-list
                        ))
                  ))
               ((> second-row max-row)
                (begin
                  acc-list
                  ))
               (else
                (begin
                  (let ((acc2-list
                         (local-counter-loop
                          second-row second-col max-row max-col
                          (append
                           second-curr-list
                           (list (list second-row second-col)))
                          acc-list)))
                    (begin
                      (set! acc-list acc2-list)
                      ))
                  acc-list
                  )))
              ))
          )))
      ))
  (begin
    (local-counter-loop
     0 0 max-row max-col
     (list (list 0 0)) (list 0 (list)))
    ))

;;;#############################################################
;;;#############################################################
(define (dynamic-count max-rows max-cols)
  (begin
    (let ((end-row-index (1+ max-rows))
          (end-col-index (1+ max-cols)))
      (let ((grid-array
             (make-array 0 end-row-index end-col-index)))
        (begin
          (do ((ii 0 (1+ ii)))
              ((>= ii end-row-index))
            (begin
              (array-set! grid-array 1 ii 0)
              ))

          (do ((jj 0 (1+ jj)))
              ((>= jj end-col-index))
            (begin
              (array-set! grid-array 1 0 jj)
              ))

          (do ((ii 1 (1+ ii)))
              ((>= ii end-row-index))
            (begin
              (do ((jj 1 (1+ jj)))
                  ((>= jj end-col-index))
                (begin
                  (let ((prev-1
                         (array-ref grid-array (- ii 1) jj))
                        (prev-2
                         (array-ref grid-array ii (- jj 1))))
                    (begin
                      (array-set!
                       grid-array (+ prev-1 prev-2) ii jj)
                      ))
                  ))
              ))

          (let ((result
                 (array-ref grid-array max-rows max-cols)))
            (begin
              result
              ))
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-dynamic-count-1 result-hash-table)
 (begin
   (let ((sub-name "test-dynamic-count-1")
         (test-list
          (list
           (list 1 1 2)
           (list 2 2 6)
           (list 3 3 20)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((max-rows (list-ref this-list 0))
                  (max-cols (list-ref this-list 1))
                  (shouldbe (list-ref this-list 2)))
              (let ((result (dynamic-count max-rows max-cols)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : max rows=~a, max cols=~a, "
                        sub-name test-label-index max-rows max-cols))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a~%"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (factorial num)
  (begin
    (cond
     ((< num 0)
      (begin
        -1
        ))
     ((= num 0)
      (begin
        1
        ))
     ((= num 1)
      (begin
        1
        ))
     (else
      (begin
        (let ((result 1))
          (begin
            (do ((ii 2 (1+ ii)))
                ((> ii num))
              (begin
                (set! result (* result ii))
                ))

            result
            ))
        )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-factorial-1 result-hash-table)
 (begin
   (let ((sub-name "test-factorial-1")
         (test-list
          (list
           (list 0 1) (list 1 1) (list 2 2)
           (list 3 6) (list 4 24) (list 5 120)
           (list 6 720)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((num (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result (factorial num)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : num=~a, "
                        sub-name test-label-index num))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a~%"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (combinatorial-count max-rows max-cols)
  (begin
    (let ((total-factorial
           (factorial (+ max-rows max-cols)))
          (rows-factorial (factorial max-rows))
          (cols-factorial (factorial max-cols)))
      (let ((result
             (/ total-factorial
                (* rows-factorial cols-factorial))))
        (begin
          result
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-combinatorial-count-1 result-hash-table)
 (begin
   (let ((sub-name "test-combinatorial-count-1")
         (test-list
          (list
           (list 1 1 2)
           (list 2 2 6)
           (list 3 3 20)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((max-rows (list-ref this-list 0))
                  (max-cols (list-ref this-list 1))
                  (shouldbe (list-ref this-list 2)))
              (let ((result
                     (combinatorial-count max-rows max-cols)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : max-rows=~a, max-cols=~a, "
                        sub-name test-label-index max-rows max-cols))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a~%"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Starting in the top left corner of a "))
    (display
     (format #f "2x2 grid,~%"))
    (display
     (format #f "there are 6 routes (without "))
    (display
     (format #f "backtracking)~%"))
    (display
     (format #f "to the bottom right corner.~%"))
    (newline)
    (display
     (format #f "How many routes are there "))
    (display
     (format #f "through a~%"))
    (display
     (format #f "20x20 grid?~%"))
    (newline)
    (display
     (format #f "There are three solutions used, "))
    (display
     (format #f "a brute force~%"))
    (display
     (format #f "method, using recursion, a clever "))
    (display
     (format #f "method that~%"))
    (display
     (format #f "uses dynamic programming, and a "))
    (display
     (format #f "combinatorial~%"))
    (display
     (format #f "method.~%"))
    (newline)
    (display
     (format #f "The brute force method traverses "))
    (display
     (format #f "each and every~%"))
    (display
     (format #f "path, counting each path when it "))
    (display
     (format #f "reaches the end~%"))
    (display
     (format #f "node.~%"))
    (newline)
    (display
     (format #f "The dynamic programming method~%"))
    (display
     (format #f "(https://lucidmanager.org/data-science/project-euler-15/)~%"))
    (display
     (format #f "is much more efficient.  It counts "))
    (display
     (format #f "the number of~%"))
    (display
     (format #f "paths to the final node as the sum "))
    (display
     (format #f "of the number~%"))
    (display
     (format #f "of ways to reach it's two nearest "))
    (display
     (format #f "neighbors. For~%"))
    (display
     (format #f "example, in the 2x2 grid, the number "))
    (display
     (format #f "of ways to~%"))
    (display
     (format #f "reach the bottom right corner (2, 2) "))
    (display
     (format #f "is 6, which~%"))
    (display
     (format #f "is the number of ways to reach (1, 2), "))
    (display
     (format #f "which is 3,~%"))
    (display
     (format #f "plus the number of ways to reach (2, 1), "))
    (display
     (format #f "which is 3.~%"))
    (display
     (format #f "The number of ways to reach (1, 2), "))
    (display
     (format #f "which is 3,~%"))
    (display
     (format #f "is the number of ways to reach (0, 2) "))
    (display
     (format #f "which is 1,~%"))
    (display
     (format #f "plus the number of ways to reach (1, 1) "))
    (display
     (format #f "which is 2.~%"))
    (display
     (format #f "The number of ways to reach (2, 1) is "))
    (display
     (format #f "similar to~%"))
    (display
     (format #f "calculating the number of ways to reach (1, 2). "))
    (display
     (format #f "The~%"))
    (display
     (format #f "number of ways to reach (1, 1), which "))
    (display
     (format #f "is 2, is~%"))
    (display
     (format #f "the number of ways to reach (0, 1), "))
    (display
     (format #f "which is 1,~%"))
    (display
     (format #f "plus the number of ways to reach (1, 0), "))
    (display
     (format #f "which is 1.~%"))
    (newline)
    (display
     (format #f "The simplest solution was described in "))
    (display
     (format #f "wikipedia~%"))
    (display
     (format #f "(https://en.wikipedia.org/wiki/Lattice_path)~%"))
    (display
     (format #f "as a lattice path/combinatorial approach.~%"))
    (display
     (format #f "Every path must make 20 down moves and "))
    (display
     (format #f "20 right~%"))
    (display
     (format #f "moves, in order to reach the bottom "))
    (display
     (format #f "right corner.~%"))
    (display
     (format #f "So the total number of moves is 40, and the "))
    (display
     (format #f "number~%"))
    (display
     (format #f "of unique permutations of 20 downs and "))
    (display
     (format #f "20 right~%"))
    (display
     (format #f "moves is 40! / (20! x 20!)~%"))
    (display
     (format #f "see also: https://martin-ueding.de/posts/project-euler-solution-15-lattice-paths/~%"))
    (display
     (format #f "see https://projecteuler.net/problem=15~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop max-rows max-cols debug-flag)
  (begin
    (if (equal? debug-flag #t)
        (begin
          (let ((results
                 (rec-down-count max-rows max-cols debug-flag)))
            (begin
              (display
               (ice-9-format:format
                #f "brute-force method: results ~ax~a : number of paths = ~a~%"
                max-rows max-cols results))
              ))
          ))

    (let ((results (dynamic-count max-rows max-cols)))
      (begin
        (display
         (ice-9-format:format
          #f "dynamic method: results ~ax~a : number of paths = ~:d~%"
          max-rows max-cols results))
        ))

    (let ((results
           (combinatorial-count max-rows max-cols)))
      (begin
        (display
         (ice-9-format:format
          #f "combinatorial method: results ~ax~a : number of paths = ~:d~%"
          max-rows max-cols results))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((test-lists
           (list
            (list 1 1 #t)
            (list 2 2 #f)
            (list 3 3 #f)
            (list 4 4 #f)
            (list 10 10 #f))))
      (begin
        (for-each
         (lambda (a-list)
           (begin
             (let ((max-rows (list-ref a-list 0))
                   (max-cols (list-ref a-list 1))
                   (debug-flag (list-ref a-list 2)))
               (begin
                 (sub-main-loop max-rows max-cols debug-flag)
                 (gc)
                 (newline)
                 ))
             )) test-lists)
        ))

    (let ((max-rows 20)
          (max-cols 20)
          (debug-flag #f))
      (begin
        (sub-main-loop max-rows max-cols debug-flag)
        (newline)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 15 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

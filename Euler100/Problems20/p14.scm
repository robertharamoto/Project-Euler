#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 14                                     ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated June 10, 2022                                ###
;;;###                                                       ###
;;;###  updated March 4, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (next-num-in-seq this-num)
  (begin
    (if (even? this-num)
        (begin
          (euclidean/ this-num 2))
        (begin
          (+ (* 3 this-num) 1)
          ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-next-num-in-seq-1 result-hash-table)
 (begin
   (let ((sub-name "test-next-num-in-seq-1")
         (test-list
          (list
           (list 1 4) (list 2 1) (list 3 10) (list 4 2)
           (list 5 16) (list 6 3) (list 7 22) (list 8 4)
           (list 9 28) (list 10 5) (list 11 34) (list 12 6)
           (list 13 40) (list 14 7) (list 15 46) (list 16 8)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-num (list-ref alist 0))
                  (shouldbe-num (list-ref alist 1)))
              (let ((result-num (next-num-in-seq test-num)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : test-num=~a, "
                        sub-name test-label-index test-num))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a~%"
                        shouldbe-num result-num)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-num result-num)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (list-to-seq-string llist)
  (begin
    (let ((stmp
           (string-join
            (map
             (lambda (this-num)
               (begin
                 (ice-9-format:format #f "~:d" this-num)
                 ))
             llist) " -> ")))
      (begin
        stmp
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-list-to-seq-string-1 result-hash-table)
 (begin
   (let ((sub-name "test-list-to-seq-string-1")
         (test-list
          (list
           (list (list 1) "1")
           (list (list 1 2) "1 -> 2")
           (list (list 1 2 3) "1 -> 2 -> 3")
           (list (list 4 5 6 7) "4 -> 5 -> 6 -> 7")
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((input-list (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result (list-to-seq-string input-list)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : input-list=~a, "
                        sub-name test-label-index input-list))
                      (err-2
                       (format
                        #f "shouldbe=~s, result=~s~%"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (string-ci=? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (calc-seq-list start-num previous-htable)
  (define (local-iter-loop this-num previous-htable acc-list)
    (begin
      (cond
       ((<= this-num 1)
        (begin
          (reverse (cons 1 acc-list))
          ))
       ((list? (member this-num acc-list))
        (begin
          (reverse (cons this-num acc-list))
          ))
       (else
        (begin
          (let ((next-num (next-num-in-seq this-num)))
            (let ((prev-list
                   (hash-ref previous-htable next-num #f)))
              (begin
                (if (not (equal? prev-list #f))
                    (begin
                      (append
                       (reverse acc-list) (list this-num) prev-list))
                    (begin
                      (local-iter-loop
                       next-num previous-htable (cons this-num acc-list))
                      ))
                )))
          )))
      ))
  (begin
    (let ((result-list
           (local-iter-loop
            start-num previous-htable (list))))
      (begin
        (hash-set! previous-htable start-num result-list)
        result-list
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-calc-seq-list-1 result-hash-table)
 (begin
   (let ((sub-name "test-calc-seq-list-1")
         (test-list
          (list
           (list 2 (list 2 1))
           (list 3 (list 3 10 5 16 8 4 2 1))
           (list 4 (list 4 2 1))
           (list 5 (list 5 16 8 4 2 1))
           (list 6 (list 6 3 10 5 16 8 4 2 1))
           (list 7 (list 7 22 11 34 17 52 26 13 40 20 10 5 16 8 4 2 1))
           (list 8 (list 8 4 2 1))
           (list 9 (list 9 28 14 7 22 11 34 17 52 26 13 40 20 10 5 16 8 4 2 1))
           (list 10 (list 10 5 16 8 4 2 1))
           (list 11 (list 11 34 17 52 26 13 40 20 10 5 16 8 4 2 1))
           ))
         (prev-htable (make-hash-table 10))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((test-num (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result (calc-seq-list test-num prev-htable)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : test-num=~a, "
                        sub-name test-label-index test-num))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a~%"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "The following iterative sequence is "))
    (display
     (format #f "defined for the~%"))
    (display
     (format #f "set of positive integers:~%"))
    (newline)
    (display
     (format #f "n -> n/2 (n is even)~%"))
    (display
     (format #f "n -> 3n + 1 (n is odd)~%"))
    (newline)
    (display
     (format #f "Using the rule above and starting "))
    (display
     (format #f "with 13, we~%"))
    (display
     (format #f "generate the following sequence:~%"))
    (display
     (format #f "13 -> 40 -> 20 -> 10 -> 5 -> 16 "))
    (display
     (format #f "-> 8 -> 4 -> 2 -> 1~%"))
    (display
     (format #f "It can be seen that this sequence "))
    (display
     (format #f "(starting at~%"))
    (display
     (format #f "13 and finishing at 1) contains 10 "))
    (display
     (format #f "terms. Although~%"))
    (display
     (format #f "it has not been proved yet (Collatz "))
    (display
     (format #f "Problem), it~%"))
    (display
     (format #f "is thought that all starting "))
    (display
     (format #f "numbers finish~%"))
    (display
     (format #f "at 1.~%"))
    (newline)
    (display
     (format #f "Which starting number, under one "))
    (display
     (format #f "million, produces~%"))
    (display
     (format #f "the longest chain?~%"))
    (display
     (format #f "NOTE: Once the chain starts the "))
    (display
     (format #f "terms are allowed~%"))
    (display
     (format #f "to go above one million.~%"))
    (newline)
    (display
     (format #f "The key to making this program run "))
    (display
     (format #f "fast is to~%"))
    (display
     (format #f "use memoization.~%"))
    (display
     (format #f "See http://community.schemewiki.org/?memoization~%"))
    (display
     (format #f "see https://projecteuler.net/problem=14~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (longest-chain-loop max-num)
  (begin
    (let ((longest-num 0)
          (longest-list (list))
          (longest-len 0)
          (prev-htable (make-hash-table 100)))
      (begin
        (do ((ii 2 (+ ii 1)))
            ((> ii max-num))
          (begin
            (let ((this-list (calc-seq-list ii prev-htable)))
              (let ((this-len (length this-list)))
                (begin
                  (if (> this-len longest-len)
                      (begin
                        (set! longest-num ii)
                        (set! longest-list this-list)
                        (set! longest-len this-len)
                        ))

                  (if (not
                       (equal?
                        (car (last-pair this-list))
                        1))
                      (begin
                        (display
                         (format
                          #f "surprise! ~a circular : ~a : length = ~a~%"
                          ii
                          (list-to-seq-string this-list)
                          this-len))
                        (force-output)
                        ))
                  )))
            ))

        (display
         (ice-9-format:format
          #f "Longest chain less than ~:d~%" max-num))
        (display
         (ice-9-format:format
          #f "~:d : ~a : length = ~:d~%"
          longest-num
          (list-to-seq-string longest-list)
          longest-len))
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((start-num 13)
          (prev-htable (make-hash-table 100)))
      (let ((result-list (calc-seq-list start-num prev-htable)))
        (let ((rstring (list-to-seq-string result-list)))
          (begin
            (display
             (ice-9-format:format
              #f "~:d : ~a : length = ~a~%"
              start-num rstring (length result-list)))
            ))
        ))

    (let ((max-num 20))
      (begin
        (longest-chain-loop max-num)
        ))

    (let ((max-num 100))
      (begin
        (longest-chain-loop max-num)
        ))

    (let ((max-num 1000000))
      (begin
        (longest-chain-loop max-num)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 14 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

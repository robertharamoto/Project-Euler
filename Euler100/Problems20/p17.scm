#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 17                                     ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated June 10, 2022                                ###
;;;###                                                       ###
;;;###  updated March 6, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### srfi-1 for fold function
(use-modules ((srfi srfi-1)
              :renamer (symbol-prefix-proc 'srfi-1:)))

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
;;; turns a list of a list of a list into a flat list
(define (flatten-list llist)
  (define (local-recurse llist acc-list)
    (begin
      (if (and (list? llist) (> (length llist) 0))
          (begin
            (let ((this-elem (car llist))
                  (tail-list (cdr llist)))
              (begin
                (if (list? this-elem)
                    (begin
                      (let ((this-list
                             (local-recurse this-elem (list))))
                        (begin
                          (set! acc-list (append this-list acc-list))
                          )))
                    (begin
                      (set! acc-list (cons this-elem acc-list))
                      ))
                (local-recurse tail-list acc-list)
                )))
          (begin
            acc-list
            ))
      ))
  (begin
    (reverse (local-recurse llist (list)))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-flatten-list-1 result-hash-table)
 (begin
   (let ((sub-name "test-flatten-list-1")
         (test-list
          (list
           (list (list (list 1 2 3)
                       (list 4 5 6) 7 8 9)
                 (list 1 2 3 4 5 6 7 8 9))
           (list (list (list 1 2 3)
                       (list (list 4 5 6) (list 7 8 9)))
                 (list 1 2 3 4 5 6 7 8 9))
           (list (list (list 1 2 3)
                       (list (list 4 5 6) 7 8 9))
                 (list 1 2 3 4 5 6 7 8 9))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((input-list (list-ref alist 0))
                  (shouldbe-list (list-ref alist 1)))
              (let ((result-list (flatten-list input-list)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : input-list=~a, "
                        sub-name test-label-index input-list))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a~%"
                        shouldbe-list result-list)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-list result-list)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; takes two lists of strings and turns them into a product list of strings
;;; note: first element assumed to be left alone, e.g. twenty, twenty-one, ...
(define (prod-string-lists prefix-list suffix-list connector-string)
  (begin
    (let ((out-list
           (map-in-order
            (lambda (this-prefix)
              (begin
                (srfi-1:fold
                 (lambda (this-suffix prev-2)
                   (begin
                     (if (string-ci=? this-suffix "zzz")
                         (begin
                           (cons this-prefix prev-2))
                         (begin
                           (cons
                            (string-append
                             this-prefix connector-string this-suffix)
                            prev-2)
                           ))
                     ))
                 (list) (reverse (cons "zzz" suffix-list)))
                ))
            prefix-list)))
      (begin
        (flatten-list out-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-prod-string-lists-1 result-hash-table)
 (begin
   (let ((sub-name "test-prod-string-lists-1")
         (test-list
          (list
           (list
            (list "twenty" "thirty")
            (list "one" "two")
            "-"
            (list "twenty" "twenty-one" "twenty-two"
                  "thirty" "thirty-one" "thirty-two"))
           (list
            (list "forty" "fifty")
            (list "three" "four")
            "-"
            (list "forty" "forty-three" "forty-four"
                  "fifty" "fifty-three" "fifty-four"))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((prefix-list (list-ref alist 0))
                  (suffix-list (list-ref alist 1))
                  (connector-string (list-ref alist 2))
                  (shouldbe-list (list-ref alist 3)))
              (let ((result-list
                     (prod-string-lists
                      prefix-list suffix-list connector-string)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "prefix=~a, suffix=~a, "
                        prefix-list suffix-list))
                      (err-3
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe-list result-list)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-list result-list)
                     sub-name
                     (string-append
                      err-1 err-2 err-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (build-numbers-list debug-flag)
  (begin
    (let ((elist
           (list "one" "two" "three" "four"
                 "five" "six" "seven" "eight" "nine")))
      (let ((teens-list
             (list "ten" "eleven" "twelve" "thirteen"
                   "fourteen" "fifteen" "sixteen" "seventeen"
                   "eighteen" "nineteen")))
        (let ((tens-list
               (list "twenty" "thirty" "forty" "fifty"
                     "sixty" "seventy" "eighty" "ninety")))
          (let ((others-list
                 (prod-string-lists tens-list elist "-")))
            (begin
              (let ((sub-one-hundred-list
                     (append elist teens-list others-list)))
                (begin
                  (if (equal? debug-flag #t)
                      (display
                       (format #f "~a~%" sub-one-hundred-list)))

                  (let ((hundreds-prefix-list
                         (list
                          "one hundred" "two hundred"
                          "three hundred" "four hundred"
                          "five hundred" "six hundred"
                          "seven hundred" "eight hundred"
                          "nine hundred")))
                    (let ((upper-list
                           (prod-string-lists
                            hundreds-prefix-list
                            sub-one-hundred-list " and ")))
                      (let ((final-list
                             (append
                              sub-one-hundred-list
                              upper-list (list "one thousand"))))
                        (begin
                          final-list
                          ))
                      ))
                  ))
              )))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (string-list-count slist)
  (begin
    (let ((t1-string (string-join slist)))
      (let ((t2-string (string-delete #\space t1-string)))
        (let ((t3-string (string-delete #\- t2-string)))
          (let ((tcount (string-length t3-string)))
            (begin
              tcount
              )))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-string-list-count-1 result-hash-table)
 (begin
   (let ((sub-name "test-string-list-count-1")
         (test-list
          (list
           (list (list "one" "two" "three") 11)
           (list (list "one" "two" "three" "four") 15)
           (list (list "one" "two" "three" "four" "five") 19)
           (list (list "three hundred and forty-two") 23)
           (list (list "one hundred and fifteen") 20)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((input-list (list-ref alist 0))
                  (shouldbe-num (list-ref alist 1)))
              (let ((result-num
                     (string-list-count input-list)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : input-list=~a, "
                        sub-name test-label-index input-list))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a~%"
                        shouldbe-num result-num)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-num result-num)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "If the numbers 1 to 5 are written "))
    (display
     (format #f "out in words:~%"))
    (display
     (format #f "one, two, three, four, five, then "))
    (display
     (format #f "there are~%"))
    (display
     (format #f "3 + 3 + 5 + 4 + 4 = 19 letters used "))
    (display
     (format #f "in total.~%"))
    (newline)
    (display (format #f "If all the numbers from 1 to "))
    (display
     (format #f "1000 (one~%"))
    (display
     (format #f "thousand) inclusive were written out "))
    (display
     (format #f "in words,~%"))
    (display
     (format #f "how many letters would be used?~%"))
    (newline)
    (display
     (format #f "NOTE: Do not count spaces or hyphens. "))
    (display
     (format #f "For example,~%"))
    (display
     (format #f "342 (three hundred and forty-two) "))
    (display
     (format #f "contains 23 letters~%"))
    (display
     (format #f "and 115 (one hundred and fifteen) "))
    (display
     (format #f "contains 20 letters.~%"))
    (display
     (format #f "The use of 'and' when writing out "))
    (display
     (format #f "numbers is in~%"))
    (display
     (format #f "compliance with British usage.~%"))
    (display
     (format #f "see https://projecteuler.net/problem=17~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop num-list range-string)
  (begin
    (let ((scount (string-list-count num-list)))
      (begin
        (display
         (ice-9-format:format
          #f "Number of characters from ~a = ~:d~%"
          range-string scount))
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((num-list
           (list "one" "two" "three" "four" "five"))
          (range-string "1 to 5"))
      (begin
        (sub-main-loop num-list range-string)
        ))

    (let ((debug-flag #f))
      (let ((num-list (build-numbers-list debug-flag))
            (range-string "1 to 1000"))
        (begin
          (sub-main-loop num-list range-string)
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 17 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)

          (newline)
          (force-output)

          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

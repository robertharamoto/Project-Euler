#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 19                                     ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated June 10, 2022                                ###
;;;###                                                       ###
;;;###  updated March 4, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (populate-month-hash! mhash)
  (begin
    (hash-set! mhash 1 31)   ;;; january
    (hash-set! mhash 2 28)   ;;; february
    (hash-set! mhash 3 31)   ;;; march
    (hash-set! mhash 4 30)   ;;; april
    (hash-set! mhash 5 31)   ;;; may
    (hash-set! mhash 6 30)   ;;; june
    (hash-set! mhash 7 31)   ;;; july
    (hash-set! mhash 8 31)   ;;; august
    (hash-set! mhash 9 30)   ;;; september
    (hash-set! mhash 10 31)  ;;; october
    (hash-set! mhash 11 30)  ;;; november
    (hash-set! mhash 12 31)  ;;; december
    ))

;;;#############################################################
;;;#############################################################
;;; date data structure (list day month year dow yyyymmdd)
(define (increment-date my-date-list mdate-htable)
  (define (local-make-list
           next-day next-month next-year next-dow last-day-of-month)
    (begin
      (if (<= next-day last-day-of-month)
          (begin
            (list next-day next-month next-year next-dow
                  (+ (* 10000 next-year)
                     (* 100 next-month) next-day)))
          (begin
            (set! next-day 1)
            (set! next-month (+ next-month 1))
            (if (<= next-month 12)
                (begin
                  (list next-day next-month next-year next-dow
                        (+ (* 10000 next-year) (* 100 next-month) next-day)))
                (begin
                  (set! next-month 1)
                  (set! next-year (+ next-year 1))
                  (list next-day next-month next-year next-dow
                        (+ (* 10000 next-year) (* 100 next-month) next-day))
                  ))
            ))
      ))
  (begin
    (let ((this-day (list-ref my-date-list 0))
          (this-month (list-ref my-date-list 1))
          (this-year (list-ref my-date-list 2))
          (this-dow (list-ref my-date-list 3)))
      (let ((last-day-of-month
             (hash-ref mdate-htable this-month 1))
            (next-day (+ this-day 1))
            (next-month this-month)
            (next-year this-year)
            (next-dow (modulo (+ this-dow 1) 7)))
        (begin
          (cond
           ((= this-month 2)
            (begin
              (let ((is-leap-year #f)
                    (div-by-four (zero? (modulo next-year 4))))
                (begin
                  (if (equal? div-by-four #t)
                      (begin
                        (let ((div-by-hundred
                               (zero? (modulo next-year 100)))
                              (div-by-four-hundred
                               (zero? (modulo next-year 400))))
                          (begin
                            (if (not (equal? div-by-hundred #t))
                                (begin
                                  (set! is-leap-year #t))
                                (begin
                                  (if (equal? div-by-four-hundred #t)
                                      (begin
                                        (set! is-leap-year #t)
                                        ))
                                  ))
                            ))
                        ))
                  (if (equal? is-leap-year #t)
                      (begin
                        (set! last-day-of-month 29)
                        ))

                  (local-make-list
                   next-day next-month next-year
                   next-dow last-day-of-month)
                  ))
              ))
           (else
            (begin
              (local-make-list
               next-day next-month next-year
               next-dow last-day-of-month)
              )))
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-increment-date-1 result-hash-table)
 (begin
   (let ((sub-name "test-increment-date-1")
         (test-list
          (list
           (list (list 1 1 1900 0 19000101)
                 (list 2 1 1900 1 19000102))
           (list (list 2 1 1900 1 19000102)
                 (list 3 1 1900 2 19000103))
           (list (list 29 1 1900 1 19000129)
                 (list 30 1 1900 2 19000130))
           (list (list 31 1 1900 3 19000131)
                 (list 1 2 1900 4 19000201))
           (list (list 28 2 1900 6 19000128)
                 (list 1 3 1900 0 19000301))
           (list (list 28 2 1901 6 19010128)
                 (list 1 3 1901 0 19010301))
           (list (list 28 2 1902 6 19020128)
                 (list 1 3 1902 0 19020301))
           (list (list 28 2 1903 6 19030128)
                 (list 1 3 1903 0 19030301))
           (list (list 28 2 1904 6 19040128)
                 (list 29 2 1904 0 19040229))
           (list (list 29 2 1904 0 19040129)
                 (list 1 3 1904 1 19040301))
           (list (list 31 3 1900 5 19000331)
                 (list 1 4 1900 6 19000401))
           (list (list 30 4 1900 5 19000430)
                 (list 1 5 1900 6 19000501))
           (list (list 31 5 1900 5 19000531)
                 (list 1 6 1900 6 19000601))
           (list (list 30 6 1900 5 19000630)
                 (list 1 7 1900 6 19000701))
           (list (list 31 7 1900 5 19000731)
                 (list 1 8 1900 6 19000801))
           (list (list 31 8 1900 5 19000831)
                 (list 1 9 1900 6 19000901))
           (list (list 30 9 1900 5 19000930)
                 (list 1 10 1900 6 19001001))
           (list (list 31 10 1900 5 19000331)
                 (list 1 11 1900 6 19001101))
           (list (list 30 11 1900 5 19000330)
                 (list 1 12 1900 6 19001201))
           (list (list 31 12 1900 5 19000331)
                 (list 1 1 1901 6 19010101))
           ))
         (monthend-htable (make-hash-table 12))
         (test-label-index 0))
     (begin
       (populate-month-hash! monthend-htable)

       (for-each
        (lambda (this-list)
          (begin
            (let ((test-date (list-ref this-list 0))
                  (shouldbe-date (list-ref this-list 1)))
              (let ((result-date
                     (increment-date test-date monthend-htable)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : test-date=~a, "
                        sub-name test-label-index test-date))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a~%"
                        shouldbe-date result-date)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-date result-date)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; date data structure (list day month year dow yyyymmdd)
(define (date-to-string tdate)
  (begin
    (let ((dow (list-ref tdate 3))
          (dstrings-list
           (list "monday" "tuesday" "wednesday" "thursday"
                 "friday" "saturday" "sunday")))
      (let ((dow-string
             (format
              #f "dow error dow=~a should be >=0 and <=6" dow))
            (day (list-ref tdate 0))
            (month (list-ref tdate 1))
            (year (list-ref tdate 2)))
        (begin
          (if (and (>= dow 0)
                   (<= dow 6))
              (begin
                (set! dow-string (list-ref dstrings-list dow))
                ))

          (let ((this-string
                 (format #f "~a, ~a/~a/~a"
                         dow-string month day year)))
            (begin
              this-string
              ))
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-date-to-string-1 result-hash-table)
 (begin
   (let ((sub-name "test-date-to-string-1")
         (test-list
          (list
           (list (list 1 1 1900 0 19000101) "monday, 1/1/1900")
           (list (list 2 1 1900 1 19000102) "tuesday, 1/2/1900")
           (list (list 3 1 1900 2 19000103) "wednesday, 1/3/1900")
           (list (list 4 1 1900 3 19000104) "thursday, 1/4/1900")
           (list (list 5 1 1900 4 19000105) "friday, 1/5/1900")
           (list (list 6 1 1900 5 19000106) "saturday, 1/6/1900")
           (list (list 7 1 1900 6 19000107) "sunday, 1/7/1900")
           (list (list 8 1 1900 0 19000108) "monday, 1/8/1900")
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((test-date (list-ref this-list 0))
                  (shouldbe-string
                   (list-ref this-list 1)))
              (let ((result-string
                     (date-to-string test-date)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : test-date=~a, "
                        sub-name test-label-index test-date))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a~%"
                        shouldbe-string result-string)))
                  (begin
                    (unittest2:assert?
                     (string-ci=? shouldbe-string result-string)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (recalc-day-of-week init-date find-date)
  (begin
    (let ((this-date init-date)
          (this-yyyymmdd (list-ref init-date 4))
          (find-yyyymmdd (list-ref find-date 4))
          (mdate-htable (make-hash-table 12))
          (dow 0))
      (begin
        (populate-month-hash! mdate-htable)

        (let ((init-complete-flag #f))
          (begin
            (while
             (or (equal? init-complete-flag #f)
                 (> this-yyyymmdd find-yyyymmdd))
             (begin
               (let ((next-date
                      (increment-date this-date mdate-htable)))
                 (let ((next-yyyymmdd
                        (list-ref next-date 4)))
                   (begin
                     (if (>= next-yyyymmdd find-yyyymmdd)
                         (begin
                           (set! dow (list-ref next-date 3))
                           (set! init-complete-flag #t)
                           (break))
                         (begin
                           (set! this-date next-date)
                           ))
                     )))
               ))

            (list
             (list-ref find-date 0) (list-ref find-date 1)
             (list-ref find-date 2) dow (list-ref find-date 4))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-recalc-day-of-week-1 result-hash-table)
 (begin
   (let ((sub-name "test-recalc-day-of-week-1")
         (test-list
          (list
           (list (list 1 1 1900 0 19000101)
                 (list 2 1 1900 0 19000102)
                 (list 2 1 1900 1 19000102))
           (list (list 1 1 1900 0 19000101)
                 (list 3 1 1900 0 19000103)
                 (list 3 1 1900 2 19000103))
           (list (list 1 1 1900 0 19000101)
                 (list 4 1 1900 0 19000104)
                 (list 4 1 1900 3 19000104))
           (list (list 1 1 1900 0 19000101)
                 (list 5 1 1900 0 19000105)
                 (list 5 1 1900 4 19000105))
           (list (list 1 1 1900 0 19000101)
                 (list 6 1 1900 0 19000106)
                 (list 6 1 1900 5 19000106))
           (list (list 1 1 1900 0 19000101)
                 (list 7 1 1900 0 19000107)
                 (list 7 1 1900 6 19000107))
           (list (list 1 1 1900 0 19000101)
                 (list 8 1 1900 0 19000108)
                 (list 8 1 1900 0 19000108))
           (list (list 1 1 1900 0 19000101)
                 (list 9 1 1900 0 19000109)
                 (list 9 1 1900 1 19000109))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((test-init (list-ref this-list 0))
                  (test-start (list-ref this-list 1))
                  (shouldbe-date (list-ref this-list 2)))
              (let ((result-date
                     (recalc-day-of-week test-init test-start)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "test-init=~a, test-start=~a, "
                        test-init test-start))
                      (err-3
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe-date result-date)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-date result-date)
                     sub-name
                     (string-append
                      err-1 err-2 err-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; date data structure (list day month year dow yyyymmdd)
;;; note: dow monday = 0, tuesday = 1, ..., sunday = 6
(define (iterate-date-loop
         init-date start-date end-date debug-flag)
  (begin
    (let ((sunday-count 0)
          (first-month-count 0)
          (sunday-first-count 0)
          (this-date init-date)
          (restart-date start-date)
          (start-yyyymmdd (list-ref start-date 4))
          (mdate-htable (make-hash-table 12)))
      (begin
        (populate-month-hash! mdate-htable)

        ;;; initialization, from init-date to start date
        ;;; need to restart date to find the right day-of-week
        (let ((restart-date
               (recalc-day-of-week init-date start-date)))
          (begin
            (if (equal? debug-flag #t)
                (begin
                  (display
                   (format
                    #f "debug init-date=~a, start-date=~a, "
                    init-date start-date))
                  (display
                   (format
                    #f "restart-date=~a, end-date=~a~%"
                    restart-date end-date))
                  (force-output)
                  ))

            ;;; now start accumulating statistics
            (let ((loop-complete-flag #f)
                  (this-date restart-date)
                  (this-yyyymmdd (list-ref restart-date 4))
                  (end-yyyymmdd (list-ref end-date 4)))
              (begin
                (while
                 (and (equal? loop-complete-flag #f)
                      (<= this-yyyymmdd end-yyyymmdd))
                 (begin
                   (let ((this-dow (list-ref this-date 3))
                         (this-day (list-ref this-date 0)))
                     (begin
                       (if (equal? this-day 1)
                           (begin
                             (set!
                              first-month-count (+ first-month-count 1))
                             ))

                       (if (equal? this-dow 6)
                           (begin
                             (set! sunday-count (+ sunday-count 1))
                             (if (equal? this-day 1)
                                 (begin
                                   (set! sunday-first-count (+ sunday-first-count 1))
                                   (if (equal? debug-flag #t)
                                       (begin
                                         (display
                                          (format
                                           #f "debug iterate-date-loop : "))
                                         (display
                                          (format
                                           #f "first sunday "))
                                         (display
                                          (format
                                           #f "~a : ~a : ~a : ~a~%"
                                           this-date sunday-first-count
                                           sunday-count first-month-count))
                                         (force-output)
                                         ))
                                   ))
                             ))
                       ))

                   (let ((next-date
                          (increment-date this-date mdate-htable)))
                     (let ((next-yyyymmdd (list-ref next-date 4)))
                       (begin
                         (if (> next-yyyymmdd end-yyyymmdd)
                             (begin
                               (set! loop-complete-flag #t))
                             (begin
                               (set! this-date next-date)
                               ))
                         )))
                   ))
                ))
            ))

        (list sunday-first-count
              sunday-count
              first-month-count)
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-iterate-date-loop-1 result-hash-table)
 (begin
   (let ((sub-name "test-iterate-date-loop-1")
         (test-list
          (list
           (list (list 1 1 1900 0 19000101)
                 (list 2 1 1900 0 19000102)
                 (list 1 2 1900 0 19000201)
                 (list 0 4 1))
           (list (list 1 1 1900 0 19000101)
                 (list 2 1 1900 0 19000102)
                 (list 1 1 1901 0 19010101)
                 (list 2 52 12))
           (list (list 1 1 1900 0 19000101)
                 (list 1 1 1901 0 19010101)
                 (list 1 1 1902 0 19020101)
                 (list 2 52 13))
           ))
         (debug-flag #f)
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((test-init (list-ref this-list 0))
                  (test-start (list-ref this-list 1))
                  (test-end (list-ref this-list 2))
                  (shouldbe-list (list-ref this-list 3)))
              (let ((result-list
                     (iterate-date-loop
                      test-init test-start test-end debug-flag)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "test-init=~a, test-start=~a, "
                        test-init test-start))
                      (err-3
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe-list result-list)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-list result-list)
                     sub-name
                     (string-append
                      err-1 err-2 err-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "You are given the following "))
    (display
     (format #f "information, but~%"))
    (display
     (format #f "you may prefer to do some research "))
    (display
     (format #f "for yourself.~%"))
    (newline)
    (display
     (format #f "1 Jan 1900 was a Monday.~%"))
    (display
     (format #f "Thirty days has September, April, "))
    (display
     (format #f "June and November.~%"))
    (display
     (format #f "All the rest have thirty-one, "))
    (display
     (format #f "Saving~%"))
    (display
     (format #f "February alone which has twenty-eight, "))
    (display
     (format #f "rain or shine.~%"))
    (display
     (format #f "And on leap years, twenty-nine.~%"))
    (display
     (format #f "A leap year occurs on any year evenly "))
    (display
     (format #f "divisible by 4,~%"))
    (display
     (format #f "but not on a century unless it is "))
    (display
     (format #f "divisible by 400.~%"))
    (newline)
    (display
     (format #f "How many Sundays fell on the first "))
    (display
     (format #f "of the month~%"))
    (display
     (format #f "during the twentieth century "))
    (display
     (format #f "(1 Jan 1901 to 31 Dec 2000)?~%"))
    (display
     (format #f "see https://projecteuler.net/problem=19~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop init-date start-date end-date debug-flag)
  (begin
    (let ((results
           (iterate-date-loop
            init-date start-date end-date debug-flag)))
      (begin
        (display
         (ice-9-format:format
          #f "from ~a to ~a~%"
          (date-to-string start-date)
          (date-to-string end-date)))
        (display
         (ice-9-format:format
          #f "  there were ~:d first of the months~%"
          (list-ref results 2)))
        (display
         (ice-9-format:format
          #f "  ~:d sundays, and ~:d sundays that "
          (list-ref results 1)
          (list-ref results 0)))
        (display
         (format #f "were first of the months~%"))
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((init-date (list 1 1 1900 0 19000101))
          (start-date (list 1 1 1901 0 19010101))
          (end-date (list 1 1 1902 0 19020101))
          (debug-flag #f))
      (begin
        (sub-main-loop init-date start-date
                       end-date debug-flag)
        ))

    (let ((init-date (list 1 1 1900 0 19000101))
          (start-date (list 1 1 1901 0 19010101))
          (end-date (list 31 12 2000 0 20001231))
          (debug-flag #f))
      (begin
        (sub-main-loop init-date start-date
                       end-date debug-flag)
        (newline)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 19 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  timer-module - time sections of code                 ###
;;;###                                                       ###
;;;###  last updated August 1, 2024                          ###
;;;###                                                       ###
;;;###  updated May 19, 2022                                 ###
;;;###                                                       ###
;;;###  updated February 27, 2020                            ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;### start timer modules

(define-module (timer-module)
  #:export (current-time-string
            current-date-string
            current-date-time-string
            current-date-week-day-string

            time-code-macro

            time-to-short-string
            date-time-to-short-string
            date-time-to-string

            julian-day-difference-to-string
            julian-day-difference-in-minutes

            time-hour-integer
            date-week-day-integer
            date-week-day-string
            ))

;;;#############################################################
;;;#############################################################
;;;### include modules

;;;### srfi-19 for date functions
(use-modules ((srfi srfi-19)
              :renamer (symbol-prefix-proc 'srfi-19:)))

;;;### ice-9-format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;#############################################################
;;;#############################################################
;;;### begin main functions

;;;#############################################################
;;;#############################################################
(define (current-time-string)
  (begin
    (let ((this-date (srfi-19:current-date)))
      (begin
        (string-downcase
         (srfi-19:date->string this-date "~I:~M:~S ~p"))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (current-date-string)
  (begin
    (let ((cdate (srfi-19:current-date)))
      (begin
        (srfi-19:date->string cdate "~B ~d, ~Y")
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (current-date-time-string)
  (begin
    (let ((this-date (srfi-19:current-date)))
      (begin
        (string-downcase
         (srfi-19:date->string
          this-date "~A, ~B ~d, ~Y  ~I:~M:~S ~p"))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (current-date-week-day-string)
  (begin
    (let ((cdate (srfi-19:current-date)))
      (let ((this-dow-string
             (srfi-19:date->string cdate "~A")))
        (begin
          this-dow-string
          )))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax time-code-macro
  (syntax-rules ()
    ((time-code-macro body)
     (begin
       (let ((start-jday (srfi-19:current-julian-day)))
         (begin
           body

           (let ((end-jday (srfi-19:current-julian-day)))
             (begin
               (display
                (format
                 #f "elapsed time = ~a : ~a~%"
                 (julian-day-difference-to-string
                  end-jday start-jday)
                 (current-date-time-string)))
               (force-output)
               ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define-public (time-to-short-string this-datetime)
  (begin
    (if (srfi-19:date? this-datetime)
        (begin
          (string-downcase
           (srfi-19:date->string
            this-datetime "~I:~M:~S ~p")))
        (begin
          #f
          ))
    ))

;;;#############################################################
;;;#############################################################
(define-public (date-time-to-short-string this-datetime)
  (begin
    (if (srfi-19:date? this-datetime)
        (begin
          (let ((s1 (srfi-19:date->string this-datetime "~A"))
                (s2 (string-downcase
                     (srfi-19:date->string
                      this-datetime "~I:~M:~S ~p"))))
            (begin
              (format #f "~a, ~a" s1 s2)
              )))
        (begin
          #f
          ))
    ))

;;;#############################################################
;;;#############################################################
(define-public (date-time-to-string this-datetime)
  (begin
    (if (srfi-19:date? this-datetime)
        (begin
          (let ((s1 (srfi-19:date->string
                     this-datetime "~A, ~B ~d, ~Y"))
                (s2 (string-downcase
                     (srfi-19:date->string
                      this-datetime "~I:~M:~S ~p"))))
            (begin
              (format #f "~a, ~a" s1 s2)
              )))
        (begin
          #f
          ))
    ))

;;;#############################################################
;;;#############################################################
;;;### expects 2 julian days (plain numbers)
;;;### differences between 2 julian days is in days (or a fraction of a day)
(define (julian-day-difference-to-string dend dstart)
  (define (local-process-sub-day day-fraction)
    (begin
      (let ((nsecs (* day-fraction 24.0 60.0 60.0))
            (nmins (truncate (* day-fraction 24.0 60.0)))
            (nhours
             (inexact->exact
              (truncate (* day-fraction 24.0)))))
        (let ((nminutes
               (inexact->exact
                (truncate (- nmins (* nhours 60.0))))))
          (let ((nseconds
                 (* 0.0010
                    (truncate
                     (* 1000.0
                        (- nsecs
                           (+ (* nhours 60.0 60.0)
                              (* nminutes 60.0))))))))
            (let ((seconds-string
                   (string-trim
                    (ice-9-format:format #f "~5,3f seconds" nseconds)))
                  (minutes-string
                   (format #f "~a minutes" nminutes))
                  (hours-string
                   (format #f "~a hours" nhours)))
              (begin
                (if (= nseconds 1.0)
                    (begin
                      (set! seconds-string "1 second")
                      ))
                (if (= nminutes 1)
                    (begin
                      (set! minutes-string "1 minute")
                      ))
                (if (= nhours 1.0)
                    (begin
                      (set! hours-string "1 hour")
                      ))

                (if (<= nhours 0)
                    (begin
                      (if (<= nminutes 0.0)
                          (begin
                            seconds-string)
                          (begin
                            (format
                             #f "~a, ~a"
                             minutes-string seconds-string)
                            )))
                    (begin
                      (if (<= nminutes 0)
                          (begin
                            (string-trim
                             (format
                              #f "~a, ~a"
                              hours-string seconds-string)))
                          (begin
                            (string-trim
                             (format
                              #f "~a, ~a, ~a"
                              hours-string minutes-string
                              seconds-string))
                            ))
                      ))
                )))
          ))
      ))
  (begin
    (if (and (number? dend) (number? dstart))
        (begin
          (let ((jd-diff
                 (exact->inexact (- dend dstart))))
            (begin
              (if (< jd-diff 1.0)
                  (begin
                    (let ((tstring
                           (local-process-sub-day jd-diff)))
                      (begin
                        tstring
                        )))
                  (begin
                    (let ((ndays
                           (inexact->exact (truncate jd-diff))))
                      (let ((dfract-diff (- jd-diff ndays)))
                        (let ((tstring
                               (local-process-sub-day dfract-diff)))
                          (let ((ttstring
                                 (string-trim
                                  (format
                                   #f "~a days, ~a" ndays tstring))))
                            (begin
                              (if (= ndays 1)
                                  (begin
                                    (set!
                                     ttstring
                                     (format #f "1 day, ~a" tstring))
                                    ))

                              ttstring
                              )))
                        ))
                    ))
              )))
        (begin
          #f
          ))
    ))

;;;#############################################################
;;;#############################################################
;;;### expects 2 julian days (plain numbers)
;;;### differences between 2 julian days is in days (or a fraction of a day)
;;;### discards seconds
(define (julian-day-difference-in-minutes dend dstart)
  (begin
    (if (and (number? dend) (number? dstart))
        (begin
          (let ((end-jday dend)
                (start-jday dstart))
            (begin
              (if (< dend dstart)
                  (begin
                    (set! end-jday dstart)
                    (set! start-jday dend)
                    ))

              (let ((jd-diff
                     (exact->inexact (- end-jday start-jday))))
                (begin
                  (if (< jd-diff 1.0)
                      (begin
                        (let ((nmins
                               (inexact->exact
                                (truncate (* jd-diff 24.0 60.0)))))
                          (begin
                            nmins
                            )))
                      (begin
                        (let ((ndays
                               (inexact->exact (truncate jd-diff))))
                          (let ((dfract-diff (- jd-diff ndays)))
                            (let ((nmins
                                   (truncate (* dfract-diff 24.0 60.0))))
                              (let ((total-minutes
                                     (inexact->exact
                                      (+ (* ndays 24 60) nmins))))
                                (begin
                                  total-minutes
                                  )))
                            ))
                        ))
                  ))
              )))
        (begin
          #f
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (time-hour-integer this-date)
  (begin
    (if (srfi-19:date? this-date)
        (begin
          (let ((this-hour-string
                 (srfi-19:date->string this-date "~H")))
            (let ((this-hour-integer
                   (string->number this-hour-string)))
              (begin
                this-hour-integer
                ))
            ))
        (begin
          #f
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (date-week-day-integer this-datetime)
  (begin
    (if (srfi-19:date? this-datetime)
        (begin
          (srfi-19:date-week-day this-datetime))
        (begin
          #f
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (date-week-day-string this-date)
  (begin
    (if (srfi-19:date? this-date)
        (begin
          (let ((this-dow-string
                 (srfi-19:date->string this-date "~A")))
            (begin
              this-dow-string
              )))
        (begin
          #f
          ))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

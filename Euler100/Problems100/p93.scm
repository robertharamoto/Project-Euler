#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 93                                     ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated July 27, 2022                                ###
;;;###                                                       ###
;;;###  updated March 10, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
;;; shuffle operator groupings
(define-syntax parens-lists-macro
  (syntax-rules ()
    ((parens-lists-macro
      num1 op1 num2 op2 num3 op3 num4)
     (begin
       (let ((result-list
              (list
               (list op1 num1 (list op2 num2 (list op3 num3 num4)))
               (list op1 num1 (list op3 (list op2 num2 num3) num4))
               (list op2 (list op1 num1 num2) (list op3 num3 num4))
               (list op3 (list op1 num1 (list op2 num2 num3)) num4)
               (list op3 (list op2 (list op1 num1 num2) num3) num4)
               )))
         (begin
           result-list
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
;;; enumerate unique digits
(define-syntax make-nums-loops-3-and-4-macro
  (syntax-rules ()
    ((make-nums-loops-3-and-4-macro
      start-digit end-digit
      ii-1 ii-2
      c2-list
      acc-list)
     (begin
       (do ((ii-3 start-digit (1+ ii-3)))
           ((> ii-3 end-digit))
         (begin
           (if (and (not (equal? ii-3 ii-1))
                    (not (equal? ii-3 ii-2)))
               (begin
                 (let ((c3-list (cons ii-3 c2-list)))
                   (begin
                     (do ((ii-4 start-digit (1+ ii-4)))
                         ((> ii-4 end-digit))
                       (begin
                         (if (and (not (equal? ii-4 ii-1))
                                  (not (equal? ii-4 ii-2))
                                  (not (equal? ii-4 ii-3)))
                             (begin
                               (let ((c4-list (cons ii-4 c3-list)))
                                 (begin
                                   (set! acc-list (cons c4-list acc-list))
                                   ))
                               ))
                         ))
                     ))
                 ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (make-num-lists-lists start-digit end-digit)
  (begin
    (let ((acc-list (list)))
      (begin
        (do ((ii-1 start-digit (1+ ii-1)))
            ((> ii-1 end-digit))
          (begin
            (let ((c1-list (list ii-1)))
              (begin
                (do ((ii-2 start-digit (1+ ii-2)))
                    ((> ii-2 end-digit))
                  (begin
                    (if (not (equal? ii-1 ii-2))
                        (begin
                          (let ((c2-list (cons ii-2 c1-list)))
                            (begin
                              (make-nums-loops-3-and-4-macro
                               start-digit end-digit
                               ii-1 ii-2
                               c2-list
                               acc-list)
                              ))
                          ))
                    ))
                ))
            ))
        (reverse acc-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-make-num-lists-lists-1 result-hash-table)
 (begin
   (let ((sub-name "test-make-num-lists-lists-1")
         (test-list
          (list
           (list 0 2 (list))
           (list 0 3
                 (list (list 3 2 1 0) (list 2 3 1 0)
                       (list 3 1 2 0) (list 1 3 2 0)
                       (list 2 1 3 0) (list 1 2 3 0)
                       (list 3 2 0 1) (list 2 3 0 1)
                       (list 3 0 2 1) (list 0 3 2 1)
                       (list 2 0 3 1) (list 0 2 3 1)
                       (list 3 1 0 2) (list 1 3 0 2)
                       (list 3 0 1 2) (list 0 3 1 2)
                       (list 1 0 3 2) (list 0 1 3 2)
                       (list 2 1 0 3) (list 1 2 0 3)
                       (list 2 0 1 3) (list 0 2 1 3)
                       (list 1 0 2 3) (list 0 1 2 3)))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((start-digit (list-ref alist 0))
                  (end-digit (list-ref alist 1))
                  (shouldbe (list-ref alist 2)))
              (let ((result
                     (make-num-lists-lists start-digit end-digit)))
                (let ((slen (length shouldbe))
                      (rlen (length result)))
                  (let ((err-1
                         (format
                          #f "~a : error (~a) : "
                          sub-name test-label-index))
                        (err-2
                         (format
                          #f "start-digit=~a, end-digit=~a, "
                          start-digit end-digit))
                        (err-3
                         (format
                          #f "shouldbe length=~a, result length=~a"
                          slen rlen)))
                    (begin
                      (unittest2:assert?
                       (equal? slen rlen)
                       sub-name
                       (string-append
                        err-1 err-2 err-3)
                       result-hash-table)

                      (for-each
                       (lambda (s-list)
                         (begin
                           (let ((err-4
                                  (format
                                   #f "s-list=~a, result=~a"
                                   s-list result)))
                             (begin
                               (unittest2:assert?
                                (not
                                 (equal? (member s-list result)
                                         #f))
                                sub-name
                                (string-append
                                 err-1 err-2 err-4)
                                result-hash-table)
                               ))
                           )) shouldbe)
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; evaluate tree
(define (evaluate-tree-list tree-list)
  (begin
    (if (and (list? tree-list)
             (> (length tree-list) 1))
        (begin
          (let ((operator (list-ref tree-list 0))
                (first-expr (list-ref tree-list 1))
                (second-expr (list-ref tree-list 2)))
            (begin
              (if (list? first-expr)
                  (begin
                    (let ((result
                           (evaluate-tree-list first-expr)))
                      (begin
                        (set! first-expr result)
                        ))
                    ))
              (if (list? second-expr)
                  (begin
                    (let ((result (evaluate-tree-list second-expr)))
                      (begin
                        (set! second-expr result)
                        ))
                    ))
              (if (and (equal? operator /)
                       (equal? second-expr 0))
                  (begin
                    #f)
                  (begin
                    (if (and (not (equal? first-expr #f))
                             (not (equal? second-expr #f)))
                        (begin
                          (operator first-expr second-expr))
                        (begin
                          #f
                          ))
                    ))
              )))
        (begin
          #f
          ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-evaluate-tree-list-1 result-hash-table)
 (begin
   (let ((sub-name "test-evaluate-tree-list-1")
         (test-list
          (list
           (list (list + 1 3) 4)
           (list (list / (list + 1 3) 2) 2)
           (list (list / (list + 3 1) 2) 2)
           (list (list / (list * 5 (list + 1 3)) 2) 10)
           (list (list / (list * 4 (list + 1 3)) 2) 8)
           (list (list - (list * 4 (list + 2 3)) 1) 19)
           (list (list * (list * 3 4) (list + 2 1)) 36)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((input-list (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result (evaluate-tree-list input-list)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : input-list=~a, "
                        sub-name test-label-index input-list))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (find-min-max-from-hash results-htable)
  (begin
    (let ((min -1)
          (max -1))
      (begin
        (hash-for-each
         (lambda (key value)
           (begin
             (if (or (< min 0)
                     (< key min))
                 (begin
                   (set! min key)
                   ))
             (if (or (< max 0)
                     (> key max))
                 (begin
                   (set! max key)
                   ))
             )) results-htable)
        (list min max)
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-find-min-max-from-hash-1 result-hash-table)
 (begin
   (let ((sub-name "test-find-min-max-from-hash-1")
         (test-list
          (list
           (list (list (list 1 2) (list 2 3))
                 (list 1 2))
           (list (list (list 1 2) (list 2 3) (list 3 3))
                 (list 1 3))
           (list (list (list 4 2) (list 2 3) (list 3 3))
                 (list 2 4))
           ))
         (data-htable (make-hash-table 10))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((input-list-list (list-ref this-list 0))
                  (shouldbe-list (list-ref this-list 1)))
              (begin
                (hash-clear! data-htable)
                (for-each
                 (lambda (a-list)
                   (begin
                     (let ((key (list-ref a-list 0))
                           (value (list-ref a-list 1)))
                       (begin
                         (hash-set! data-htable key value)
                         ))
                     )) input-list-list)

                (let ((result-list
                       (find-min-max-from-hash data-htable)))
                  (begin
                    (let ((err-1
                           (format
                            #f "~a : error (~a) : input-list-list=~a, "
                            sub-name test-label-index input-list-list))
                          (err-2
                           (format
                            #f "shouldbe=~a, result=~a"
                            shouldbe-list result-list)))
                      (begin
                        (unittest2:assert?
                         (equal? shouldbe-list result-list)
                         sub-name
                         (string-append err-1 err-2)
                         result-hash-table)
                        ))
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (hash-size results-htable)
  (begin
    (let ((count 0))
      (begin
        (hash-for-each
         (lambda (key value)
           (begin
             (set! count (1+ count))
             )) results-htable)
        count
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-hash-size-1 result-hash-table)
 (begin
   (let ((sub-name "test-hash-size-1")
         (test-list
          (list
           (list (list (list 1 2) (list 2 3)) 2)
           (list (list (list 1 2) (list 2 3) (list 3 3)) 3)
           (list (list (list 4 2) (list 2 3) (list 3 3) (list 9 99)) 4)
           ))
         (data-htable (make-hash-table 10))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((input-list-list (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (begin
                (hash-clear! data-htable)
                (for-each
                 (lambda (a-list)
                   (let ((key (list-ref a-list 0))
                         (value (list-ref a-list 1)))
                     (begin
                       (hash-set! data-htable key value)
                       ))) input-list-list)

                (let ((result (hash-size data-htable)))
                  (let ((err-1
                         (format
                          #f "~a : error (~a) : input-list-list=~a, "
                          sub-name test-label-index input-list-list))
                        (err-2
                         (format
                          #f "shouldbe=~a, result=~a"
                          shouldbe result)))
                    (begin
                      (unittest2:assert?
                       (equal? shouldbe result)
                       sub-name
                       (string-append err-1 err-2)
                       result-hash-table)
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (max-consecutive-keys-hash results-htable)
  (begin
    (let ((ok-flag #t)
          (max-size 0)
          (hsize (hash-size results-htable)))
      (begin
        (do ((ii 1 (1+ ii)))
            ((or (> ii hsize)
                 (equal? ok-flag #f)))
          (begin
            (let ((this-value
                   (hash-ref results-htable ii #f)))
              (begin
                (if (equal? this-value #f)
                    (begin
                      (set! ok-flag #f))
                    (begin
                      (set! max-size ii)
                      ))
                ))
            ))
        max-size
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-max-consecutive-keys-hash-1 result-hash-table)
 (begin
   (let ((sub-name "test-max-consecutive-keys-hash-1")
         (test-list
          (list
           (list (list (list 1 2) (list 2 3)) 2)
           (list (list (list 1 2) (list 2 3) (list 3 3)) 3)
           (list (list (list 4 2) (list 2 3) (list 3 3) (list 1 99)) 4)
           (list (list (list 7 2) (list 2 3) (list 3 3) (list 1 99)) 3)
           ))
         (data-htable (make-hash-table 10))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((input-list-list (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (begin
                (hash-clear! data-htable)
                (for-each
                 (lambda (a-list)
                   (begin
                     (let ((key (list-ref a-list 0))
                           (value (list-ref a-list 1)))
                       (begin
                         (hash-set! data-htable key value)
                         ))
                     )) input-list-list)

                (let ((result
                       (max-consecutive-keys-hash data-htable)))
                  (let ((err-1
                         (format
                          #f "~a : error (~a) : input-list-list=~a, "
                          sub-name test-label-index input-list-list))
                        (err-2
                         (format
                          #f "shouldbe=~a, result=~a"
                          shouldbe result)))
                    (begin
                      (unittest2:assert?
                       (equal? shouldbe result)
                       sub-name
                       (string-append err-1 err-2)
                       result-hash-table)
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (list-to-string a-list-list)
  (begin
    (if (and (list? a-list-list)
             (> (length a-list-list) 1))
        (begin
          (let ((operator (list-ref a-list-list 0))
                (first-expr (list-ref a-list-list 1))
                (second-expr (list-ref a-list-list 2)))
            (begin
              (if (list? first-expr)
                  (begin
                    (let ((result (list-to-string first-expr)))
                      (begin
                        (set! first-expr result)
                        ))
                    ))
              (if (list? second-expr)
                  (begin
                    (let ((result (list-to-string second-expr)))
                      (begin
                        (set! second-expr result)
                        ))
                    ))

              (cond
               ((equal? operator +)
                (begin
                  (set! operator "+")
                  ))
               ((equal? operator -)
                (begin
                  (set! operator "-")
                  ))
               ((equal? operator *)
                (begin
                  (set! operator "*")
                  ))
               ((equal? operator /)
                (begin
                  (set! operator "/")
                  ))
               (else
                (begin
                  (set! operator (format #f "~a" operator))
                  )))
              (format #f "(~a ~a ~a)" operator first-expr second-expr)
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-list-to-string-1 result-hash-table)
 (begin
   (let ((sub-name "test-list-to-string-1")
         (test-list
          (list
           (list (list + 1 3)
                 "(+ 1 3)")
           (list (list / (list + 1 3) 2)
                 "(/ (+ 1 3) 2)")
           (list (list / (list + 3 1) 2)
                 "(/ (+ 3 1) 2)")
           (list (list / (list * 5 (list + 1 3)) 2)
                 "(/ (* 5 (+ 1 3)) 2)")
           (list (list / (list * 4 (list + 1 3)) 2)
                 "(/ (* 4 (+ 1 3)) 2)")
           (list (list - (list * 4 (list + 2 3)) 1)
                 "(- (* 4 (+ 2 3)) 1)")
           (list (list * (list * 3 4) (list + 2 1))
                 "(* (* 3 4) (+ 2 1))")
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((input-list-list (list-ref this-list 0))
                  (shouldbe-string (list-ref this-list 1)))
              (let ((result-string (list-to-string input-list-list)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : input-list-list=~a, "
                        sub-name test-label-index input-list-list))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe-string result-string)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-string result-string)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; define a macro to simplify code
(define-syntax evaluate-all-combinations
  (syntax-rules ()
    ((evaluate-all-combinations
      num1 num2 num3 num4 op1 op2 op3
      num-key seq-htable set-htable)
     (begin
       (let ((combo-lists
              (parens-lists-macro
               num1 op1 num2 op2 num3 op3 num4)))
         (begin
           (for-each
            (lambda (c-list)
              (begin
                (let ((value (evaluate-tree-list c-list)))
                  (begin
                    (if (and (number? value)
                             (integer? value)
                             (> value 0))
                        (begin
                          (let ((c-flag
                                 (hash-ref seq-htable value #f)))
                            (begin
                              (if (equal? c-flag #f)
                                  (begin
                                    (let ((c-string
                                           (list-to-string c-list)))
                                      (begin
                                        (hash-set! seq-htable value c-string)
                                        ))
                                    ))
                              ))
                          ))
                    ))
                )) combo-lists)
           (hash-set! set-htable num-key seq-htable)
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
;;; set-htable key=(list num1 num2 num3 num4), value=sequence-hash-table
;;; sequence-htable key=expr numeric value, value=string representation
(define (populate-set-sequence-hash!
         set-htable all-num-list-list max-len)
  (begin
    (let ((oper-list (list + - * /)))
      (begin
        (for-each
         (lambda (num-list)
           (begin
             (let ((num1 (list-ref num-list 0))
                   (num2 (list-ref num-list 1))
                   (num3 (list-ref num-list 2))
                   (num4 (list-ref num-list 3))
                   (num-key (sort num-list <)))
               (let ((seq-htable
                      (hash-ref
                       set-htable num-key
                       (make-hash-table 100))))
                 (begin
                   (for-each
                    (lambda (op1)
                      (begin
                        (for-each
                         (lambda (op2)
                           (begin
                             (for-each
                              (lambda (op3)
                                (begin
                                  (evaluate-all-combinations
                                   num1 num2 num3 num4 op1 op2 op3
                                   num-key seq-htable set-htable)
                                  )) oper-list)
                             )) oper-list)
                        )) oper-list)
                   )))
             )) all-num-list-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (compute-max-consecutive
         num-list max-nn completed-htable)
  (begin
    (let ((max-consecutive 0)
          (max-set (list)))
      (begin
        (populate-set-sequence-hash!
         completed-htable num-list max-nn)

        (hash-for-each
         (lambda (num-set r-htable)
           (begin
             (let ((hsize (hash-size r-htable))
                   (max-con (max-consecutive-keys-hash r-htable)))
               (begin
                 (if (< max-consecutive max-con)
                     (begin
                       (set! max-consecutive max-con)
                       (set! max-set num-set)
                       ))
                 ))
             )) completed-htable)

        (list max-consecutive max-set)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (find-max-key result-htable)
  (begin
    (let ((max-key
           (hash-fold
            (lambda (key value prior)
              (begin
                (max key prior)
                ))
            0 result-htable)))
      (begin
        max-key
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (display-results
         max-set max-consecutive r-htable debug-flag)
  (begin
    (let ((hsize (hash-size r-htable))
          (key-list
           (sort
            (hash-map->list
             (lambda (key value)
               (begin
                 key
                 )) r-htable)
            <)))
      (begin
        (display
         (ice-9-format:format
          #f "  maximum set = ~a~%" max-set))
        (display
         (ice-9-format:format
          #f "  maximum string = ~a~%"
          (string-join (map number->string max-set) "")))
        (display
         (ice-9-format:format
          #f "  number of results found = ~a~%" hsize))
        (display
         (ice-9-format:format
          #f "  maximum consecutive results to ~a~%"
          max-consecutive))
        (let ((max-key (find-max-key r-htable)))
          (begin
            (display
             (ice-9-format:format
              #f "  largest target number = ~a~%" max-key))
            ))

        (if (equal? debug-flag #t)
            (begin
              (for-each
               (lambda (key)
                 (begin
                   (let ((value
                          (hash-ref r-htable key #f)))
                     (begin
                       (if (and (string? value)
                                (> (string-length value) 0))
                           (begin
                             (display
                              (ice-9-format:format
                               #f "  ~a = ~a~%" key value))
                             ))
                       ))
                   )) key-list)
              ))

        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (reproduce-problem-statement)
  (begin
    (let ((num-list (make-num-lists-lists 1 4))
          (max-nn 4)
          (completed-htable (make-hash-table 100)))
      (let ((max-lists
             (compute-max-consecutive
              num-list max-nn completed-htable)))
        (begin
          (let ((max-consecutive (list-ref max-lists 0))
                (max-set (list-ref max-lists 1)))
            (let ((r-htable
                   (hash-ref completed-htable max-set #f)))
              (begin
                (if (not (equal? r-htable #f))
                    (begin
                      (display-results
                       max-set max-consecutive r-htable #t)
                      ))
                )))
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop start-num end-num max-nn debug-flag)
  (begin
    (let ((num-list
           (make-num-lists-lists start-num end-num))
          (completed-htable
           (make-hash-table 100)))
      (let ((max-lists
             (compute-max-consecutive
              num-list max-nn completed-htable)))
        (begin
          (let ((max-consecutive
                 (list-ref max-lists 0))
                (max-set (list-ref max-lists 1)))
            (let ((r-htable
                   (hash-ref completed-htable max-set #f)))
              (begin
                (if (not (equal? r-htable #f))
                    (begin
                      (display-results
                       max-set max-consecutive r-htable debug-flag)
                      ))
                )))
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "By using each of the digits from "))
    (display
     (format #f "the set, {1, 2, 3, 4},~%"))
    (display
     (format #f "exactly once, and making use of "))
    (display
     (format #f "the four arithmetic~%"))
    (display
     (format #f "operations (+, -, *, /) and "))
    (display
     (format #f "brackets/parentheses,~%"))
    (display
     (format #f "it is possible to form different "))
    (display
     (format #f "positive integer targets.~%"))
    (newline)
    (display
     (format #f "For example,~%"))
    (display
     (format #f "  8 = (4 * (1 + 3)) / 2~%"))
    (display
     (format #f "  14 = 4 * (3 + 1 / 2)~%"))
    (display
     (format #f "  19 = 4 * (2 + 3) - 1~%"))
    (display
     (format #f "  36 = 3 * 4 * (2 + 1)~%"))
    (newline)
    (display
     (format #f "Note that concatenations of the "))
    (display
     (format #f "digits, like 12 + 34,~%"))
    (display
     (format #f "are not allowed.~%"))
    (newline)
    (display
     (format #f "Using the set, {1, 2, 3, 4}, it "))
    (display
     (format #f "is possible to obtain~%"))
    (display
     (format #f "thirty-one different target numbers "))
    (display
     (format #f "of which 36 is~%"))
    (display
     (format #f "the maximum, and each of the numbers "))
    (display
     (format #f "1 to 28 can~%"))
    (display
     (format #f "be obtained before encountering the "))
    (display
     (format #f "first non-expressible~%"))
    (display
     (format #f "number.~%"))
    (newline)
    (display
     (format #f "Find the set of four distinct "))
    (display
     (format #f "digits, a < b < c < d,~%"))
    (display
     (format #f "for which the longest set of "))
    (display
     (format #f "consecutive positive integers,~%"))
    (display
     (format #f "1 to n, can be obtained, giving "))
    (display
     (format #f "your answer as a string: abcd.~%"))
    (newline)
    (display
     (format #f "the solution is described at~%"))
    (display
     (format #f "https://euler.stephan-brumme.com/93/~%"))
    (display
     (format #f "see https://projecteuler.net/problem=93~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (reproduce-problem-statement)

    (newline)
    (let ((start-num 0)
          (end-num 9)
          (max-nn 4)
          (debug-flag #f))
      (begin
        (sub-main-loop
         start-num end-num max-nn debug-flag)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 93 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

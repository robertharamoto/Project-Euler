#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 97                                     ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated July 27, 2022                                ###
;;;###                                                       ###
;;;###  updated March 10, 2020                               ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (modulo-power base exponent mod-num)
  (begin
    (let ((cc 1))
      (begin
        (do ((ii 0 (1+ ii)))
            ((>= ii exponent))
          (begin
            (set! cc (modulo (* cc base) mod-num))
            ))
        cc
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-modulo-power-1 result-hash-table)
 (begin
   (let ((sub-name "test-modulo-power-1")
         (test-list
          (list
           (list 5 3 13 8)
           (list 4 13 497 445)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((base (list-ref alist 0))
                  (exponent (list-ref alist 1))
                  (mod-num (list-ref alist 2))
                  (shouldbe-num (list-ref alist 3)))
              (let ((result-num
                     (modulo-power base exponent mod-num)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "base=~a, exponent=~a, mod-num=~a, "
                        base exponent mod-num))
                      (err-3
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe-num result-num)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-num result-num)
                     sub-name
                     (string-append
                      err-1 err-2 err-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (extract-last-n-digits large-number n-digits)
  (begin
    (let ((result-list (list))
          (ok-flag #t)
          (local-number large-number))
      (begin
        (do ((ii 0 (1+ ii)))
            ((or (>= ii n-digits)
                 (equal? ok-flag #f)))
          (begin
            (if (< local-number 10)
                (begin
                  (set! result-list (cons local-number result-list))
                  (set! local-number 0)
                  (set! ok-flag #f))
                (begin
                  (let ((next-local (euclidean/ local-number 10)))
                    (let ((tmp-local (* 10 next-local)))
                      (let ((this-digit (- local-number tmp-local)))
                        (begin
                          (if (<= next-local 0)
                              (begin
                                (set! ok-flag #f))
                              (begin
                                (set! result-list (cons this-digit result-list))
                                (set! local-number next-local)
                                ))
                          ))
                      ))
                  ))
            ))
        result-list
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-extract-last-n-digits-1 result-hash-table)
 (begin
   (let ((sub-name "test-extract-last-n-digits-1")
         (test-list
          (list
           (list 5 1 (list 5))
           (list 5 2 (list 5))
           (list 12 1 (list 2))
           (list 12 2 (list 1 2))
           (list 12 3 (list 1 2))
           (list 123 1 (list 3))
           (list 123 2 (list 2 3))
           (list 123 3 (list 1 2 3))
           (list 1234 1 (list 4))
           (list 1234 2 (list 3 4))
           (list 1234 3 (list 2 3 4))
           (list 1234 4 (list 1 2 3 4))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((large-num (list-ref alist 0))
                  (n-digits (list-ref alist 1))
                  (shouldbe (list-ref alist 2)))
              (let ((result
                     (extract-last-n-digits large-num n-digits)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "large-num=~a, n-digits=~a, "
                        large-num n-digits))
                      (err-3
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append
                      err-1 err-2 err-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop
         factor1 base exponent modulus n-digits)
  (begin
    (let ((exp-term
           (modulo-power base exponent modulus)))
      (let ((second-term
             (modulo (* factor1 exp-term) modulus)))
        (let ((large-num
               (modulo (+ 1 second-term) modulus)))
          (let ((digit-list
                 (extract-last-n-digits large-num n-digits)))
            (begin
              (display
               (ice-9-format:format
                #f "the last ~a digits of "
                n-digits))
              (display
               (ice-9-format:format
                #f "~:d x ~:d^(~:d) + 1 is ~a~%"
                factor1 base exponent digit-list))
              (force-output)
              )))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "The first known prime found "))
    (display
     (format #f "to exceed one million~%"))
    (display
     (format #f "digits was discovered in 1999, "))
    (display
     (format #f "and is a Mersenne~%"))
    (display
     (format #f "prime of the form (2^6972593)-1; "))
    (display
     (format #f "it contains exactly~%"))
    (display
     (format #f "2,098,960 digits. Subsequently "))
    (display
     (format #f "other Mersenne primes,~%"))
    (display
     (format #f "of the form 2^p-1,~%"))
    (display
     (format #f "have been found which contain "))
    (display
     (format #f "more digits.~%"))
    (newline)
    (display
     (format #f "However, in 2004 there was found "))
    (display
     (format #f "a massive non-Mersenne~%"))
    (display
     (format #f "prime which contains 2,357,207 "))
    (display
     (format #f "digits:~%"))
    (display
     (format #f "28433x(2^7830457)+1.~%"))
    (newline)
    (display
     (format #f "Find the last ten digits of "))
    (display
     (format #f "this prime number.~%"))
    (newline)
    (display
     (format #f "for the solution method: see~%"))
    (display
     (format #f "https://en.wikipedia.org/wiki/Modular_exponentiation~%"))
    (display
     (format #f "modular exponentiation means that you "))
    (display
     (format #f "don't have to~%"))
    (display
     (format #f "compute all 2 million digits.~%"))
    (display
     (format #f "see https://projecteuler.net/problem=97~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((factor1 10)
          (base 2)
          (exponent 3)
          (modulus 100000))
      (begin
        (do ((ii 1 (1+ ii)))
            ((> ii 3))
          (begin
            (sub-main-loop
             factor1 base exponent modulus ii)
            ))
        ))

    (newline)
    (let ((factor1 10)
          (base 2)
          (exponent 4)
          (modulus 100000))
      (begin
        (do ((ii 1 (1+ ii)))
            ((> ii 3))
          (begin
            (sub-main-loop
             factor1 base exponent modulus ii)
            ))
        ))

    (newline)
    (let ((factor1 28433)
          (base 2)
          (exponent 7830457)
          (mod-num 1000000000000)
          (n-digits 10))
      (begin
        (sub-main-loop
         factor1 base exponent mod-num n-digits)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 97 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

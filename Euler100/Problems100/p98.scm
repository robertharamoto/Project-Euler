#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 98                                     ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated July 27, 2022                                ###
;;;###                                                       ###
;;;###  updated March 10, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-0 rdelim for delimited read-line functions
(use-modules ((ice-9 rdelim)
              :renamer (symbol-prefix-proc 'ice-9-rdelim:)))

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### digits-module for digit-list-to-number function
(use-modules ((digits-module)
              :renamer (symbol-prefix-proc 'digits-module:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (squared-numbers-pairs-hash!
         squares-htable start-num end-num)
  (begin
    (let ((intermediate-htable (make-hash-table 100)))
      (begin
        ;;; first load up intermediate-htable with pairs of squares
        (do ((ii start-num (1+ ii)))
            ((> ii end-num))
          (begin
            (let ((num-square (* ii ii)))
              (let ((pair-list (list ii num-square))
                    (num-list
                     (sort
                      (digits-module:split-digits-list num-square)
                      <)))
                (let ((sorted-num
                       (digits-module:digit-list-to-number num-list)))
                  (let ((this-list
                         (hash-ref intermediate-htable num-list (list))))
                    (begin
                      (if (equal? (member pair-list this-list) #f)
                          (begin
                            (hash-set!
                             intermediate-htable
                             num-list (cons pair-list this-list))
                            ))
                      )))
                ))
            ))

        ;;; return only those that have multiple pairs
        (hash-clear! squares-htable)

        (hash-for-each
         (lambda (key value)
           (begin
             (if (> (length value) 1)
                 (begin
                   (hash-set! squares-htable key value)
                   ))
             )) intermediate-htable)
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-squared-numbers-pairs-hash-1 result-hash-table)
 (begin
   (let ((sub-name "test-squared-numbers-pairs-hash-1")
         (test-list
          (list
           (list 1 100
                 (list
                  (list
                   (list 1 2 6 9)
                   (list (list 36 1296)
                         (list 54 2916)
                         (list 96 9216)))))
           ))
         (pairs-htable (make-hash-table 10))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (hash-clear! pairs-htable)

            (let ((start-num (list-ref alist 0))
                  (end-num (list-ref alist 1))
                  (shouldbe-list-list (list-ref alist 2)))
              (let ((err-1
                     (format
                      #f "~a : error (~a) : "
                      sub-name test-label-index))
                    (err-2
                     (format
                      #f "start-num=~a, end-num=~a, "
                      start-num end-num)))
                (begin
                  (squared-numbers-pairs-hash!
                   pairs-htable start-num end-num)

                  (for-each
                   (lambda (b-list)
                     (begin
                       (let ((key (list-ref b-list 0))
                             (shouldbe (list-ref b-list 1)))
                         (let ((result
                                (hash-ref pairs-htable key (list))))
                           (let ((slen (length shouldbe))
                                 (rlen (length result)))
                             (let ((err-3
                                    (format
                                     #f "key=~a, shouldbe length=~a, "
                                     key slen))
                                   (err-4
                                    (format
                                     #f "result length=~a" rlen)))
                               (begin
                                 (unittest2:assert?
                                  (equal? slen rlen)
                                  sub-name
                                  (string-append
                                   err-1 err-2 err-3 err-4)
                                  result-hash-table)

                                 (for-each
                                  (lambda (shouldbe-pair)
                                    (begin
                                      (let ((err-3
                                             (format
                                              #f "shouldbe=~a, result=~a"
                                              shouldbe-pair result)))
                                        (begin
                                          (unittest2:assert?
                                           (not
                                            (equal?
                                             (member shouldbe-pair result)
                                             #f))
                                           sub-name
                                           (string-append
                                            err-1 err-2 err-3)
                                           result-hash-table)
                                          ))
                                      )) shouldbe)
                                 )))
                           ))
                       )) shouldbe-list-list)
                  )))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (word-list-to-multiple-string-hash!
         multiple-htable word-list)
  (begin
    (let ((intermediate-htable (make-hash-table 100)))
      (begin
        (for-each
         (lambda (this-word)
           (begin
             (let ((sorted-word
                    (list->string
                     (sort
                      (string->list this-word)
                      char<?))))
               (let ((this-word-list
                      (hash-ref
                       intermediate-htable sorted-word #f)))
                 (begin
                   (if (equal? this-word-list #f)
                       (begin
                         (hash-set!
                          intermediate-htable
                          sorted-word (list this-word)))
                       (begin
                         (set!
                          this-word-list
                          (append this-word-list (list this-word)))
                         (hash-set!
                          intermediate-htable
                          sorted-word this-word-list)
                         ))
                   )))
             )) word-list)

        (hash-clear! multiple-htable)

        (hash-for-each
         (lambda (key value)
           (begin
             (if (and (list? value) (> (length value) 1))
                 (begin
                   (hash-set! multiple-htable key value)
                   ))
             )) intermediate-htable)

        (hash-clear! intermediate-htable)
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-word-list-to-multiple-string-hash-1 result-hash-table)
 (begin
   (let ((sub-name "test-word-list-to-multiple-string-hash-1")
         (test-list
          (list
           (list (list "care" "race")
                 (list (list "acer" (list "care" "race"))))
           (list (list "cab" "bac" "edfg")
                 (list (list "abc" (list "cab" "bac"))))
           ))
         (wl-htable (make-hash-table 10))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (hash-clear! wl-htable)

            (let ((wlist (list-ref alist 0))
                  (shouldbe-list-list (list-ref alist 1)))
              (begin
                (word-list-to-multiple-string-hash!
                 wl-htable wlist)
                (for-each
                 (lambda (b-list)
                   (begin
                     (let ((key (list-ref b-list 0))
                           (shouldbe (list-ref b-list 1)))
                       (let ((result
                              (hash-ref wl-htable key (list))))
                         (let ((slen (length shouldbe))
                               (rlen (length result)))
                           (let ((err-1
                                  (format
                                   #f "~a : error (~a) : key=~a, "
                                   sub-name test-label-index key))
                                 (err-2
                                  (format
                                   #f "shouldbe length=~a, result=~a"
                                   slen rlen)))
                             (begin
                               (unittest2:assert?
                                (equal? slen rlen)
                                sub-name
                                (string-append err-1 err-2)
                                result-hash-table)

                               (for-each
                                (lambda (shouldbe-word)
                                  (begin
                                    (let ((err-3
                                           (format
                                            #f "shouldbe-word=~a, result=~a"
                                            shouldbe-word result)))
                                      (begin
                                        (unittest2:assert?
                                         (not
                                          (equal?
                                           (member shouldbe-word result)
                                           #f))
                                         sub-name
                                         (string-append err-1 err-3)
                                         result-hash-table)
                                        ))
                                    )) shouldbe)
                               )))
                         ))
                     )) shouldbe-list-list)
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; returns a list of lists
(define (read-in-file fname)
  (begin
    (let ((results-list (list))
          (counter 0))
      (begin
        (if (file-exists? fname)
            (begin
              (with-input-from-file fname
                (lambda ()
                  (begin
                    (do ((line
                          (ice-9-rdelim:read-delimited ",\r\n")
                          (ice-9-rdelim:read-delimited ",\r\n")))
                        ((eof-object? line))
                      (begin
                        (if (and (not (eof-object? line))
                                 (> (string-length line) 0))
                            (begin
                              (let ((this-string
                                     (string-downcase
                                      (string-delete #\" line))))
                                (begin
                                  (set! counter (1+ counter))
                                  (set!
                                   results-list
                                   (cons this-string results-list))
                                  ))
                              ))
                        ))
                    )))

              (display
               (ice-9-format:format
                #f "read in ~a words from ~a~%" counter fname))
              (newline)
              (force-output)

              (reverse results-list))
            (begin
              (list)
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (add-to-chars-htable! char-htable dlen dlist slist)
  (begin
    (let ((ok-flag #t)
          (tmp-htable (make-hash-table 100)))
      (begin
        (hash-clear! char-htable)

        (do ((ii 0 (1+ ii)))
            ((>= ii dlen))
          (begin
            (let ((digit (list-ref dlist ii))
                  (char (list-ref slist ii)))
              (begin
                (let ((this-digit
                       (hash-ref char-htable char #f)))
                  (begin
                    (if (equal? this-digit #f)
                        (begin
                          (hash-set! char-htable char digit))
                        (begin
                          ;;; don't allow the same character to have different digits
                          (if (not (equal? this-digit digit))
                              (begin
                                (set! ok-flag #f)
                                ))
                          ))
                    ))
                ))
            ))

        ;;; don't allow different characters to have the same digit
        (do ((ii 0 (1+ ii)))
            ((>= ii dlen))
          (begin
            (let ((digit (list-ref dlist ii))
                  (char (list-ref slist ii)))
              (let ((this-char
                     (hash-ref tmp-htable digit #f)))
                (begin
                  (if (equal? this-char #f)
                      (begin
                        (hash-set! tmp-htable digit char))
                      (begin
                        (if (not (equal? this-char char))
                            (begin
                              (set! ok-flag #f)
                              ))
                        ))
                  )))
            ))

        (if (equal? ok-flag #f)
            (begin
              (hash-clear! char-htable)
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (digital-substitution slist char-htable)
  (begin
    (let ((sum 0)
          (ok-flag #t))
      (begin
        (for-each
         (lambda (this-char)
           (begin
             (let ((this-digit
                    (hash-ref char-htable this-char #f)))
               (begin
                 (if (equal? this-digit #f)
                     (begin
                       (set! ok-flag #f))
                     (begin
                       (set! sum (+ (* 10 sum) this-digit))
                       ))
                 ))
             )) slist)
        (if (equal? ok-flag #f)
            (begin
              #f)
            (begin
              sum
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-digital-substitution-1 result-hash-table)
 (begin
   (let ((sub-name "test-digital-substitution-1")
         (test-list
          (list
           (list (list #\c #\a #\r #\e) (list 1 2 9 6)
                 (list #\c #\a #\r #\e) 1296)
           (list (list #\c #\a #\r #\e) (list 1 2 9 6)
                 (list #\r #\a #\c #\e) 9216)
           ))
         (char-htable (make-hash-table 10))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (hash-clear! char-htable)

            (let ((slist (list-ref alist 0))
                  (dlist (list-ref alist 1))
                  (char-list (list-ref alist 2))
                  (shouldbe (list-ref alist 3)))
              (let ((dlen (length dlist)))
                (begin
                  (add-to-chars-htable! char-htable dlen dlist slist)

                  (let ((result
                         (digital-substitution char-list char-htable)))
                    (let ((err-1
                           (format
                            #f "~a : error (~a) : "
                            sub-name test-label-index))
                          (err-2
                           (format
                            #f "slist=~a, dlist=~a, char-list=~a, "
                            slist dlist char-list))
                          (err-3
                           (format
                            #f "shouldbe=~a, result=~a"
                            shouldbe result)))
                      (begin
                        (unittest2:assert?
                         (equal? shouldbe result)
                         sub-name
                         (string-append
                          err-1 err-2 err-3)
                         result-hash-table)
                        )))
                  )))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (hash-to-string char-htable)
  (begin
    (let ((sresult "")
          (key-list
           (sort
            (hash-map->list
             (lambda (key value)
               (begin
                 key
                 )) char-htable)
            char-ci<?)))
      (begin
        (for-each
         (lambda (key)
           (begin
             (let ((value (hash-ref char-htable key #f)))
               (let ((s1 (string key))
                     (s2 (number->string value)))
                 (begin
                   (if (string-ci=? sresult "")
                       (begin
                         (set!
                          sresult
                          (string-append "{ " s1 " -> " s2)))
                       (begin
                         (set!
                          sresult
                          (string-append sresult ", " s1 " -> " s2))
                         ))
                   )))
             )) key-list)
        (string-append sresult " }")
        ))
    ))

;;;#############################################################
;;;#############################################################
;;; format of squares-list-list is (list (list n1 n1^2) (list n2 n2^2),...)
(define (extract-squares-only squares-list-list)
  (begin
    (let ((result-list
           (map
            (lambda (a-list)
              (begin
                (list-ref a-list 1)
                )) squares-list-list)
           ))
      (begin
        result-list
        ))
    ))

;;;#############################################################
;;;#############################################################
;;; define macro to simplify the code
(define-syntax check-other-words
  (syntax-rules ()
    ((check-other-words
      this-number this-word char-htable dlen dlist slist
      words-list only-squares-list complete-squares-list
      max-square-number max-square-list
      max-square-word-list max-string-map)
     (begin
       (let ((slist (string->list this-word)))
         (begin
           (add-to-chars-htable! char-htable dlen dlist slist)
           (for-each
            (lambda (next-word)
              (begin
                (if (not (string-ci=? this-word next-word))
                    (begin
                      (let ((ns-list (string->list next-word)))
                        (let ((this-sum
                               (digital-substitution
                                ns-list char-htable)))
                          (begin
                            (if (and
                                 (not (equal? this-sum #f))
                                 (not
                                  (equal?
                                   (member this-sum only-squares-list) #f)))
                                (begin
                                  (if (> this-sum max-square-number)
                                      (begin
                                        (set!
                                         max-square-number this-sum)
                                        (set!
                                         max-square-word-list
                                         (list this-word next-word))
                                        (set!
                                         max-string-map
                                         (hash-to-string char-htable))
                                        (for-each
                                         (lambda (b-list)
                                           (begin
                                             (let ((b-num (list-ref b-list 0))
                                                   (b-square (list-ref b-list 1)))
                                               (begin
                                                 (if (equal? this-sum b-square)
                                                     (begin
                                                       (set!
                                                        max-square-list
                                                        (list this-number b-num))
                                                       ))
                                                 ))
                                             )) complete-squares-list)
                                        ))
                                  ))
                            )))
                      ))
                )) words-list)
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (find-square-anagram nums-list words-list)
  (begin
    (let ((digit-list (list-ref nums-list 0))
          (squares-list-list (list-ref nums-list 1))
          (char-htable (make-hash-table 100))
          (max-square-number 0)
          (max-square-list (list))
          (max-square-word-list (list))
          (max-string-map ""))
      (let ((dlen (length digit-list))
            (slen (string-length (car words-list)))
            (only-squares-list
             (sort
              (extract-squares-only squares-list-list)
              <)))
        (begin
          (if (= dlen slen)
              (begin
                (for-each
                 (lambda (nlist)
                   (begin
                     (let ((this-num (list-ref nlist 0))
                           (this-square (list-ref nlist 1)))
                       (let ((dlist
                              (digits-module:split-digits-list
                               this-square)))
                         (begin
                           (for-each
                            (lambda (this-word)
                              (begin
                                (check-other-words
                                 this-num this-word char-htable dlen dlist slist
                                 words-list only-squares-list squares-list-list
                                 max-square-number max-square-list
                                 max-square-word-list max-string-map)
                                )) words-list)
                           )))
                     )) squares-list-list)

                (list max-square-number max-square-list
                      max-square-word-list max-string-map))
              (begin
                #f
                ))
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-find-square-anagram-1 result-hash-table)
 (begin
   (let ((sub-name "test-find-square-anagram-1")
         (test-list
          (list
           (list (list
                  (list 1 2 6 9)
                  (list (list 36 1296)
                        (list 54 2916)
                        (list 96 9216)))
                 (list "care" "race")
                 (list 9216 (list 36 96)
                       (list "care" "race")
                       "{ a -> 2, c -> 1, e -> 6, r -> 9 }"))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((num-list (list-ref alist 0))
                  (words-list (list-ref alist 1))
                  (shouldbe (list-ref alist 2)))
              (begin
                (let ((result
                       (find-square-anagram num-list words-list)))
                  (let ((err-1
                         (format
                          #f "~a : error (~a) : "
                          sub-name test-label-index))
                        (err-2
                         (format
                          #f "num-list=~a, words-list=~a, "
                          num-list words-list))
                        (err-3
                         (format
                          #f "shouldbe=~a, result=~a"
                          shouldbe result)))
                    (begin
                      (unittest2:assert?
                       (equal? shouldbe result)
                       sub-name
                       (string-append
                        err-1 err-2 err-3)
                       result-hash-table)
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; define macro to simplify the code
(define-syntax subprocess-nums-list-words-list
  (syntax-rules ()
    ((subprocess-nums-list-words-list
      square-number
      square-list
      square-word-list
      string-map
      max-square-number
      max-square-number-pair
      max-square-word-list
      max-square-word-pair
      max-string-map)
     (begin
       (if (and
            (list? max-square-word-pair)
            (> (length max-square-word-pair) 0)
            (string-ci<?
             (car (sort square-word-list string-ci<?))
             (car (sort max-square-word-pair string-ci<?))))
           (begin
             (set! max-square-number square-number)
             (set! max-square-number-pair square-list)
             (set! max-square-word-pair square-word-list)
             (set! max-string-map string-map))
           (begin
             (if (or
                  (not (list? max-square-word-pair))
                  (<= (length max-square-word-pair) 0))
                 (begin
                   (set! max-square-number square-number)
                   (set! max-square-number-pair square-list)
                   (set! max-square-word-pair square-word-list)
                   (set! max-string-map string-map)
                   ))
             ))
       ))
    ))

;;;#############################################################
;;;#############################################################
;;; define macro to simplify the code
(define-syntax process-nums-list-words-list
  (syntax-rules ()
    ((process-nums-list-words-list
      nums-list-list words-key words-list dlen
      max-square-number max-square-number-pair
      max-square-word-pair max-string-map)
     (begin
       (let ((slen (string-length words-key)))
         (begin
           (if (= dlen slen)
               (begin
                 (let ((result
                        (find-square-anagram
                         nums-list-list words-list)))
                   (begin
                     (if (and
                          (not (equal? result #f))
                          (list? result))
                         (begin
                           (let ((square-number (list-ref result 0))
                                 (square-list (list-ref result 1))
                                 (square-word-list (list-ref result 2))
                                 (string-map (list-ref result 3)))
                             (begin
                               (if (>= square-number max-square-number)
                                   (begin
                                     (subprocess-nums-list-words-list
                                      square-number
                                      square-list
                                      square-word-list
                                      string-map
                                      max-square-number
                                      max-square-number-pair
                                      max-square-word-list
                                      max-square-word-pair
                                      max-string-map)
                                     ))
                               ))
                           ))
                     ))
                 ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (process-hashes nums-htable words-htable)
  (begin
    (let ((max-square-number 0)
          (max-square-number-pair (list))
          (max-square-word-pair (list))
          (max-string-map ""))
      (begin
        (hash-for-each
         (lambda (nums-key nums-list)
           (begin
             (let ((dlen (length nums-key)))
               (begin
                 (hash-for-each
                  (lambda (words-key words-list)
                    (begin
                      (process-nums-list-words-list
                       (list nums-key nums-list)
                       words-key words-list dlen
                       max-square-number max-square-number-pair
                       max-square-word-pair max-string-map)
                      )) words-htable)
                 ))
             )) nums-htable)

        (list max-square-number max-square-number-pair
              max-square-word-pair max-string-map)
        ))
    ))

;;;#############################################################
;;;#############################################################
;;; define macro to simplify the code
(define-syntax display-output-results-macro
  (syntax-rules ()
    ((display-output-results-macro
      max-square-number max-square-number-pair
      max-square-word-pair max-string-map)
     (begin
       (display
        (ice-9-format:format
         #f "the largest square number formed is ~:d~%"
         max-square-number))
       (display
        (ice-9-format:format
         #f "  used the words ~a, and the numbers ~a~%"
         max-square-word-pair max-square-number-pair))
       (display
        (ice-9-format:format
         #f "  map = ~a~%" max-string-map))
       (let ((dlen (length max-square-word-pair)))
         (begin
           (do ((ii 0 (1+ ii)))
               ((>= ii dlen))
             (begin
               (let ((this-word
                      (list-ref max-square-word-pair ii))
                     (this-num
                      (list-ref max-square-number-pair ii)))
                 (begin
                   (display
                    (ice-9-format:format
                     #f "    ~s : ~:d^2 = ~:d~%"
                     this-word this-num (* this-num this-num)))
                   ))
               ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop filename start-num end-num)
  (begin
    (let ((words-htable (make-hash-table 100))
          (multiples-htable (make-hash-table 100)))
      (let ((words-list (read-in-file filename)))
        (begin
          (word-list-to-multiple-string-hash!
           words-htable words-list)
          (squared-numbers-pairs-hash!
           multiples-htable start-num end-num)

          (let ((result
                 (process-hashes
                  multiples-htable words-htable)))
            (begin
              (if (and
                   (not (equal? result #f))
                   (list? result))
                  (begin
                    (let ((max-square-number (list-ref result 0))
                          (max-square-number-pair (list-ref result 1))
                          (max-square-word-pair (list-ref result 2))
                          (max-string-map (list-ref result 3)))
                      (begin
                        (display-output-results-macro
                         max-square-number max-square-number-pair
                         max-square-word-pair max-string-map)
                        (force-output)
                        ))
                    ))
              ))
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "By replacing each of the letters in "))
    (display
     (format #f "the word CARE with~%"))
    (display
     (format #f "1, 2, 9, and 6 respectively, we "))
    (display
     (format #f "form a square number:~%"))
    (display
     (format #f "1296 = 36^2. What is remarkable is "))
    (display
     (format #f "that, by using~%"))
    (display
     (format #f "the same digital substitutions, the "))
    (display
     (format #f "anagram, RACE, also~%"))
    (display
     (format #f "forms a square number: 9216 = 96^2. We "))
    (display
     (format #f "shall call CARE~%"))
    (display
     (format #f "(and RACE) a square anagram word pair "))
    (display
     (format #f "and specify further~%"))
    (display
     (format #f "that leading zeroes are not permitted, "))
    (display
     (format #f "neither may a~%"))
    (display
     (format #f "different letter have the same "))
    (display
     (format #f "digital value as~%"))
    (display
     (format #f "another letter.~%"))
    (newline)
    (display
     (format #f "Using words.txt (right click and "))
    (display
     (format #f "'Save Link/Target As...'),~%"))
    (display
     (format #f "a 16K text file containing nearly "))
    (display
     (format #f "two-thousand common~%"))
    (display
     (format #f "English words, find all the square "))
    (display
     (format #f "anagram word pairs~%"))
    (display
     (format #f "(a palindromic word is NOT considered "))
    (display
     (format #f "to be an~%"))
    (display
     (format #f "anagram of itself).~%"))
    (newline)
    (display
     (format #f "What is the largest square number "))
    (display
     (format #f "formed by any~%"))
    (display
     (format #f "member of such a pair?~%"))
    (newline)
    (display
     (format #f "NOTE: All anagrams formed must be "))
    (display
     (format #f "contained in the given~%"))
    (display
     (format #f "text file.~%"))
    (display
     (format #f "see https://projecteuler.net/problem=98~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((filename "words.txt")
          (words-htable (make-hash-table 1000))
          (multiples-htable (make-hash-table 1000))
          (counter 0))
      (begin
        (let ((words-list (read-in-file filename)))
          (begin
            (word-list-to-multiple-string-hash!
             multiples-htable words-list)

            (let ((sorted-value-key-list
                   (sort
                    (hash-map->list
                     (lambda (key value)
                       (begin
                         (list
                          (car (sort value string-ci<?))
                          key)
                         )) multiples-htable)
                    (lambda (alist blist)
                      (begin
                        (string-ci<? (car alist) (car blist))
                        ))
                    )))
              (begin
                (for-each
                 (lambda (key-list)
                   (begin
                     (set! counter (1+ counter))
                     (let ((key (car (cdr key-list))))
                       (let ((value
                              (hash-ref multiples-htable key #f)))
                         (begin
                           (display
                            (format
                             #f "(~a) ~a => ~a~%" counter key value))
                           )))
                     )) sorted-value-key-list)
                ))
            ))
        ))

    (newline)
    (let ((filename "words.txt")
          (start-num 1)
          (end-num 96))
      (begin
        (sub-main-loop filename start-num end-num)
        (newline)
        ))

    (newline)
    (let ((filename "words.txt")
          (start-num 1)
          (end-num 100000))
      (begin
        (sub-main-loop filename start-num end-num)
        ))

    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 98 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (load "digits-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 94                                     ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated July 27, 2022                                ###
;;;###                                                       ###
;;;###  updated March 10, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
;;; define a macro to simplify code
(define-syntax handle-integer-sides-area-case
  (syntax-rules ()
    ((handle-integer-sides-area-case
      aa-times-3 area-times-3 max-perimeter
      sum-of-perimeters num-triangles
      loop-continue-flag plus-minus-num
      debug-flag)
     (begin
       (let ((perimeter-1
              (+ aa-times-3 plus-minus-num))
             (area-1 (euclidean/ area-times-3 3)))
         (begin
           (if (< perimeter-1 max-perimeter)
               (begin
                 (set!
                  sum-of-perimeters
                  (+ sum-of-perimeters perimeter-1))
                 (set!
                  num-triangles
                  (1+ num-triangles))

                 (if (equal? debug-flag #t)
                     (begin
                       (let ((aa-1 (euclidean/ aa-times-3 3)))
                         (let ((side-bb-1 (+ aa-1 plus-minus-num)))
                           (begin
                             (display
                              (ice-9-format:format
                               #f "(~:d, ~:d, ~:d), perimeter = ~:d,~%"
                               aa-1 aa-1 side-bb-1 perimeter-1))
                             (display
                              (ice-9-format:format
                               #f "  area = ~:d, "
                               area-1))
                             (display
                              (ice-9-format:format
                               #f "sum-of-perimeters = ~:d~%"
                               sum-of-perimeters))
                             (force-output)
                             )))
                       )))
               (begin
                 (set! loop-continue-flag #f)
                 ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop max-perimeter debug-flag)
  (begin
    (let ((sum-of-perimeters 0)
          (xx 2)
          (yy 1)
          (num-triangles 0)
          (loop-continue-flag #t)
          (counter 0))
      (begin
        (while
         (equal? loop-continue-flag #t)
         (begin
           (let ((aa-times-3-1 (+ (* 2 xx) 1))
                 (aa-times-3-2 (- (* 2 xx) 1))
                 (area-times-3-1 (* yy (+ xx 2)))
                 (area-times-3-2 (* yy (- xx 2))))
             (begin
                ;;; b = a + 1
               (if (and (> aa-times-3-1 0)
                        (zero? (modulo aa-times-3-1 3))
                        (> area-times-3-1 0)
                        (zero? (modulo area-times-3-1 3)))
                   (begin
                     (handle-integer-sides-area-case
                      aa-times-3-1 area-times-3-1 max-perimeter
                      sum-of-perimeters num-triangles
                      loop-continue-flag 1 debug-flag)
                     ))
                ;;; b = a - 1
               (if (and (> aa-times-3-2 0)
                        (zero? (modulo aa-times-3-2 3))
                        (> area-times-3-2 0)
                        (zero? (modulo area-times-3-2 3)))
                   (begin
                     (handle-integer-sides-area-case
                      aa-times-3-2 area-times-3-2 max-perimeter
                      sum-of-perimeters num-triangles
                      loop-continue-flag -1 debug-flag)
                     ))
               ))

            ;;; next xx and yy, (xx + yy*sqrt(3)) = (2+sqrt(3))^m, m>0
           (let ((next-xx (+ (* xx 2) (* yy 3)))
                 (next-yy (+ (* yy 2) xx)))
             (begin
               (set! xx next-xx)
               (set! yy next-yy)
               ))
           ))

        (display
         (ice-9-format:format
          #f "found ~:d almost equilateral triangles with integral~%"
          num-triangles))
        (display
         (ice-9-format:format
          #f "side lengths and area with perimeter that doesn't~%"))
        (display
         (ice-9-format:format
          #f "exceed ~:d, " max-perimeter))
        (display
         (ice-9-format:format
          #f "sum of perimeters of all almost~%"))
        (display
         (ice-9-format:format
          #f "equilateral triangles with integral side~%"))
        (display
         (ice-9-format:format
          #f "lengths and area = ~:d~%"
          sum-of-perimeters))
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "It is easily proved that no "))
    (display
     (format #f "equilateral triangle exists~%"))
    (display
     (format #f "with integral length sides and "))
    (display
     (format #f "integral area. However,~%"))
    (display
     (format #f "the almost equilateral triangle "))
    (display
     (format #f "5-5-6 has an area of~%"))
    (display
     (format #f "12 square units.~%"))
    (newline)
    (display
     (format #f "We shall define an almost equilateral "))
    (display
     (format #f "triangle to be a~%"))
    (display
     (format #f "triangle for which two sides are "))
    (display
     (format #f "equal and the third~%"))
    (display
     (format #f "differs by no more than one unit.~%"))
    (newline)
    (display
     (format #f "Find the sum of the perimeters of "))
    (display
     (format #f "all almost equilateral~%"))
    (display
     (format #f "triangles with integral side lengths "))
    (display
     (format #f "and area and whose~%"))
    (display
     (format #f "perimeters do not exceed one "))
    (display
     (format #f "billion (1,000,000,000).~%"))
    (newline)
    (display
     (format #f "The solution using Pell's equation can "))
    (display
     (format #f "be found at~%"))
    (display
     (format #f "https://blog.dreamshire.com/project-euler-94-solution/~%"))
    (newline)
    (display
     (format #f "This solution uses the standard "))
    (display
     (format #f "equations for a~%"))
    (display
     (format #f "triangle, area = b*h/2, "))
    (display
     (format #f "h^2=a^2+b^2,~%"))
    (display
     (format #f "and b=a+1 and b=a-1.~%"))
    (newline)
    (display
     (format #f "From the Pythagorean theorem, we have~%"))
    (display
     (format #f "  a^2=h^2+(b/2)^2~%"))
    (display
     (format #f "  or a^2=h^2+((a +/- 1)/2)^2.~%"))
    (newline)
    (display
     (format #f "Squaring and combining terms~%"))
    (display
     (format #f "3a^2 -/+ 2a - 1 - 4h^2 = 0.~%"))
    (display
     (format #f "Multiply through by 3, add and "))
    (display
     (format #f "subtract one to complete~%"))
    (display
     (format #f "the square, gives~%"))
    (display
     (format #f "(9a^2 -/+ 6a + 1) - 4 - 12h^2 = 0.~%"))
    (newline)
    (display
     (format #f "Dividing through by 4, we have~%"))
    (display
     (format #f "((3a -/+ 1)/2)^2-3h^2=1.~%"))
    (display
     (format #f "Let x=(3a -/+ 1)/2 and y=h, then we "))
    (display
     (format #f "get Pell's equation~%"))
    (display
     (format #f "x^2-3y^2=1.~%"))
    (newline)
    (display
     (format #f "To solve Pell's equation, see the "))
    (display
     (format #f "wikipedia at~%"))
    (display
     (format #f "https://en.wikipedia.org/wiki/Pell%27s_equation~%"))
    (newline)
    (display
     (format #f "All non-trivial solutions this version "))
    (display
     (format #f "of Pell's equation~%"))
    (display
     (format #f "are (for x and y), are given by the "))
    (display
     (format #f "equation~%"))
    (display
     (format #f "(x + y*sqrt(3)) = (2 + sqrt(3))^m~%"))
    (display
     (format #f "for m>0.~%"))
    (newline)
    (display
     (format #f "Plugging back in, we see that~%"))
    (display
     (format #f "a = ((2*x) +/- 1) / 3.~%"))
    (newline)
    (display
     (format #f "and the height is h = y, so the~%"))
    (display
     (format #f "area = (a +/- 1) * h / 2~%"))
    (display
     (format #f "= ((2x +/- 1)/3 +/- 1) * y / 2~%"))
    (display
     (format #f "= (x +/- 2) * y / 3.~%"))
    (display
     (format #f "see https://projecteuler.net/problem=94~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((max-perimeter 200)
          (debug-flag #t))
      (begin
        (sub-main-loop max-perimeter debug-flag)
        ))

    (newline)
    (let ((max-perimeter 1000000000)
          (debug-flag #f))
      (begin
        (sub-main-loop max-perimeter debug-flag)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 94 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 100                                    ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated July 27, 2022                                ###
;;;###                                                       ###
;;;###  updated March 10, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### prime-module for is-prime? function
(use-modules ((prime-module)
              :renamer (symbol-prefix-proc 'prime-module:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
;;; define macro to simplify the code
(define-syntax divide-one-specific-factor-macro
  (syntax-rules ()
    ((divide-one-specific-factor-macro
      local-divide-all-factors
      ll-num ii result-list)
     (begin
       (let ((partial-list
              (local-divide-all-factors ll-num ii)))
         (begin
           (set! ll-num (car partial-list))
           (set!
            result-list
            (append result-list (cadr partial-list)))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
;;; define macro to simplify the code
(define-syntax divide-both-specific-factors-macro
  (syntax-rules ()
    ((divide-both-specific-factors-macro
      local-divide-all-factors
      constant-number ll-num ii result-list)
     (begin
       (if (prime-module:is-prime? ii)
           (begin
             (divide-one-specific-factor-macro
              local-divide-all-factors
              ll-num ii result-list)
             ))

       (let ((this-divisor
              (quotient constant-number ii)))
         (begin
           (if (and (> this-divisor ii)
                    (equal? (member this-divisor result-list)
                            #f)
                    (prime-module:is-prime? this-divisor))
               (begin
                 (divide-one-specific-factor-macro
                  local-divide-all-factors
                  ll-num this-divisor result-list)
                 ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (prime-factor-list input-number)
  (define (local-divide-all-factors this-num this-factor)
    (begin
      (let ((ll-num this-num)
            (acc-list (list)))
        (while
         (or (zero? (modulo ll-num this-factor))
             (< ll-num 1))
         (begin
           (set! ll-num (/ ll-num this-factor))
           (set! acc-list (cons this-factor acc-list))
           ))
        (list ll-num acc-list))
      ))
  (begin
    (cond
     ((<= input-number 1)
      (begin
        (list)
        ))
     (else
      (begin
        (let ((result-list (list))
              (constant-number input-number)
              (ll-num input-number)
              (ll-max (1+ (exact-integer-sqrt input-number))))
          (begin
            (if (even? ll-num)
                (begin
                  (divide-both-specific-factors-macro
                   local-divide-all-factors
                   constant-number ll-num 2 result-list)
                  ))

            (do ((ii 3 (+ ii 2)))
                ((or (> ii ll-max)
                     (<= ll-num 1)))
              (begin
                (if (zero? (modulo constant-number ii))
                    (begin
                      (divide-both-specific-factors-macro
                       local-divide-all-factors
                       constant-number ll-num ii result-list)
                      ))
                ))

            (if (> ll-num 1)
                (begin
                  (if (prime-module:is-prime? ll-num)
                      (begin
                        (set!
                         result-list
                         (cons ll-num result-list))
                        ))
                  ))

            (if (< (length result-list) 1)
                (begin
                  (list input-number))
                (begin
                  (sort result-list <)
                  ))
            ))
        )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-prime-factor-list-1 result-hash-table)
 (begin
   (let ((sub-name "test-prime-factor-list-1")
         (test-list
          (list
           (list 2 (list 2)) (list 3 (list 3))
           (list 4 (list 2 2)) (list 5 (list 5))
           (list 6 (list 2 3)) (list 7 (list 7))
           (list 8 (list 2 2 2)) (list 9 (list 3 3))
           (list 10 (list 2 5)) (list 11 (list 11))
           (list 12 (list 2 2 3)) (list 13 (list 13))
           (list 14 (list 2 7)) (list 15 (list 3 5))
           (list 16 (list 2 2 2 2)) (list 17 (list 17))
           (list 18 (list 2 3 3)) (list 19 (list 19))
           (list 20 (list 2 2 5)) (list 21 (list 3 7))
           (list 22 (list 2 11)) (list 23 (list 23))
           (list 100 (list 2 2 5 5))
           (list 492 (list 2 2 3 41))
           (list 696 (list 2 2 2 3 29))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-num (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result (prime-factor-list test-num)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : test-num=~a, "
                        sub-name test-label-index test-num))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (display-results bb rr)
  (begin
    (if (and (> bb 0) (> rr 0))
        (begin
          (let ((total (+ bb rr))
                (bm1 (- bb 1))
                (tm1 (- (+ bb rr) 1)))
            (let ((bfactors (prime-factor-list bb))
                  (tfactors (prime-factor-list total))
                  (bm1factors (prime-factor-list bm1))
                  (tm1factors (prime-factor-list tm1))
                  (term1 (* 2 bb bm1))
                  (term2 (* total tm1))
                  (half (/ 1 2)))
              (begin
                (display
                 (ice-9-format:format
                  #f "blue = ~:d, red = ~:d, total = ~:d~%"
                  bb rr total))
                (if (= term1 term2)
                    (begin
                      (display
                       (ice-9-format:format
                        #f "  prob(bb) = ~a = " half))
                      (display
                       (ice-9-format:format
                        #f "(~:d / ~:d) * (~:d / ~:d)~%"
                        bb total bm1 tm1)))
                    (begin
                      (display
                       (ice-9-format:format
                        #f "  prob(bb) = ~a = "
                        (/ term1 (* term2 2))))
                      (display
                       (ice-9-format:format
                        #f "(~:d / ~:d) * (~:d / ~:d)~%"
                        bb total bm1 tm1))
                      ))
                (display
                 (ice-9-format:format
                  #f "  ~:d -> ~a  :  ~:d -> ~a~%"
                  bb bfactors bm1 bm1factors))
                (display
                 (ice-9-format:format
                  #f "  ~:d -> ~a  :  ~:d -> ~a~%"
                  total tfactors tm1 tm1factors))
                (newline)
                (force-output)
                )))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax inner-loop-process
  (syntax-rules ()
    ((inner-loop-process
      xx yy bb-times-2 tt-times-2
      maximum-total max-bb max-rr
      loop-continue-flag debug-flag)
     (begin
       (if (and (odd? xx) (odd? yy))
           (begin
             (let ((bb (euclidean/ bb-times-2 2))
                   (tt (euclidean/ tt-times-2 2)))
               (begin
                 (if (> bb max-bb)
                     (begin
                       (set! max-bb bb)
                       (set! max-rr (- tt bb))
                       ))

                 (if (equal? debug-flag #t)
                     (begin
                       (display-results max-bb max-rr)
                       (force-output)
                       ))

                 (if (> tt maximum-total)
                     (begin
                       (set! loop-continue-flag #f)
                       ))
                 ))
             ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop maximum-total debug-flag)
  (begin
    (let ((loop-continue-flag #t)
          (xx 1)
          (yy 1)
          (max-bb 0)
          (max-rr 0)
          (final-max-bb 0)
          (final-max-rr 0))
      (begin
        (while
         (equal? loop-continue-flag #t)
         (begin
           (let ((bb-times-2 (+ yy 1))
                 (tt-times-2 (+ xx 1)))
             (begin
               (inner-loop-process
                xx yy bb-times-2 tt-times-2
                maximum-total max-bb max-rr
                loop-continue-flag debug-flag)
               ))

           (let ((next-xx (+ xx (* 2 yy)))
                 (next-yy (+ xx yy)))
             (begin
               (set! xx next-xx)
               (set! yy next-yy)
               ))
           ))

        (display
         (ice-9-format:format
          #f "the first results to exceed ~:d total discs is:~%"
          maximum-total))
        (display-results max-bb max-rr)
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "If a box contains twenty-one "))
    (display
     (format #f "coloured discs, composed~%"))
    (display
     (format #f "of fifteen blue discs and six "))
    (display
     (format #f "red discs, and two~%"))
    (display
     (format #f "discs were taken at random, it "))
    (display
     (format #f "can be seen that~%"))
    (display
     (format #f "the probability of taking two "))
    (display
     (format #f "blue discs,~%"))
    (display
     (format #f "P(BB) = (15/21)(14/20) = 1/2.~%"))
    (newline)
    (display
     (format #f "The next such arrangement, for "))
    (display
     (format #f "which there is~%"))
    (display
     (format #f "exactly 50% chance of taking two "))
    (display
     (format #f "blue discs at random,~%"))
    (display
     (format #f "is a box containing eighty-five "))
    (display
     (format #f "blue discs and~%"))
    (display
     (format #f "thirty-five red discs.~%"))
    (newline)
    (display
     (format #f "By finding the first arrangement "))
    (display
     (format #f "to contain over~%"))
    (display
     (format #f "10^12 = 1,000,000,000,000 discs in "))
    (display
     (format #f "total, determine the~%"))
    (display
     (format #f "number of blue discs that the "))
    (display
     (format #f "box would contain.~%"))
    (newline)
    (display
     (format #f "Let b denote number of blue discs, "))
    (display
     (format #f "r is the number of~%"))
    (display
     (format #f "red discs, and t is the total number "))
    (display
     (format #f "of discs (t = b + r),~%"))
    (display
     (format #f "then prob(BB) = (b/t)*((b-1)/(t-1)).~%"))
    (newline)
    (display
     (format #f "Since we want~%"))
    (display
     (format #f "P(BB) = 1/2 = (b/t)*((b-1)/(t-1)),~%"))
    (display
     (format #f "we can simplify this equation to~%"))
    (display
     (format #f "2b^2 - 2b = t^2 - t.~%"))
    (newline)
    (display
     (format #f "rearranging the equation leads to~%"))
    (display
     (format #f "(2t - 1)^2 - 2(2b - 1)^2 = -1,~%"))
    (display
     (format #f "and if we substitute x=2t-1, y=2b-1,~%"))
    (display
     (format #f "we get Pell's equation, x^2 - 2y^2 = -1~%"))
    (newline)
    (display
     (format #f "The solution for Pell's equation is at~%"))
    (display
     (format #f "https://en.wikipedia.org/wiki/Pell%27s_equation~%"))
    (newline)
    (display
     (format #f "which is, for x and y,~%"))
    (display
     (format #f "(x + y*sqrt(2)) = (1 + sqrt(2))^m, m>0.~%"))
    (newline)
    (display
     (format #f "then b = (y + 1)/2,~%"))
    (display
     (format #f "and t = (x + 1)/2~%"))
    (display
     (format #f "see https://projecteuler.net/problem=100~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((max-total 1000)
          (debug-flag #t))
      (begin
        (sub-main-loop max-total debug-flag)
        ))

    (newline)
    (let ((max-total 1000000000000)
          (debug-flag #f))
      (begin
        (sub-main-loop max-total debug-flag)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 100 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (load "prime-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

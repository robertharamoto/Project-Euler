#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 95                                     ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated July 27, 2022                                ###
;;;###                                                       ###
;;;###  updated March 10, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### srfi-1 for fold function
(use-modules ((srfi srfi-1)
              :renamer (symbol-prefix-proc 'srfi-1:)))

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### utils-module for sequence-list-to-string function
(use-modules ((utils-module)
              :renamer (symbol-prefix-proc 'utils-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
;;; form a list of the sum of proper divisors of nn
;;; 4 -> (list (list 2 1) (list 3 1) (list 4 3))
;;; 6 -> (list (list 2 1) (list 3 1) (list 4 3) (list 5 1) (list 6 6))
(define (populate-sum-of-divisors-hash! div-htable end-num)
  (begin
    (let ((local-array (make-array 1 (1+ end-num))))
      (begin
        (hash-clear! div-htable)
        (array-set! local-array 0 1)
        (hash-set! div-htable 1 0)

        (do ((ii 2 (1+ ii)))
            ((> ii end-num))
          (begin
            (do ((jj (+ ii ii) (+ jj ii)))
                ((> jj end-num))
              (begin
                (let ((jj-sum (array-ref local-array jj)))
                  (let ((next-sum (+ ii jj-sum)))
                    (begin
                      (array-set! local-array next-sum jj)
                      )))
                ))

            (let ((elem (array-ref local-array ii)))
              (begin
                (hash-set! div-htable ii elem)
                ))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-populate-sum-of-divisors-hash-1 result-hash-table)
 (begin
   (let ((sub-name "test-populate-sum-of-divisors-hash-1")
         (test-list
          (list
           (list 1 (list (list 1 0)))
           (list 2
                 (list (list 1 0) (list 2 1)))
           (list 3
                 (list (list 1 0) (list 2 1) (list 3 1)))
           (list 4
                 (list
                  (list 1 0) (list 2 1)
                  (list 3 1) (list 4 3)))
           (list 5
                 (list
                  (list 1 0) (list 2 1) (list 3 1)
                  (list 4 3) (list 5 1)))
           (list 20
                 (list
                  (list 1 0) (list 2 1) (list 3 1)
                  (list 4 3) (list 5 1) (list 6 6)
                  (list 7 1) (list 8 7) (list 9 4)
                  (list 10 8) (list 11 1) (list 12 16)
                  (list 13 1) (list 14 10) (list 15 9)
                  (list 16 15) (list 17 1) (list 18 21)
                  (list 19 1) (list 20 22)))
           (list 30
                 (list
                  (list 1 0) (list 2 1) (list 3 1)
                  (list 4 3) (list 5 1) (list 6 6)
                  (list 7 1) (list 8 7) (list 9 4)
                  (list 10 8) (list 11 1) (list 12 16)
                  (list 13 1) (list 14 10) (list 15 9)
                  (list 16 15) (list 17 1) (list 18 21)
                  (list 19 1) (list 20 22) (list 21 11)
                  (list 22 14) (list 23 1) (list 24 36)
                  (list 25 6) (list 26 16) (list 27 13)
                  (list 28 28) (list 29 1) (list 30 42)))
           ))
         (result-htable (make-hash-table 100))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((max-num (list-ref alist 0))
                  (shouldbe-list-list (list-ref alist 1)))
              (let ((err-1
                     (format
                      #f "~a : error (~a) : max-num=~a, "
                      sub-name test-label-index max-num)))
                (begin
                  (hash-clear! result-htable)
                  (populate-sum-of-divisors-hash! result-htable max-num)

                  (for-each
                   (lambda (s-list)
                     (begin
                       (let ((skey (list-ref s-list 0))
                             (svalue (list-ref s-list 1)))
                         (let ((rvalue (hash-ref result-htable skey 0)))
                           (let ((err-2
                                  (format
                                   #f "skey=~a, svalue=~a, rvalue=~a"
                                   skey svalue rvalue)))
                             (begin
                               (unittest2:assert?
                                (equal? svalue rvalue)
                                sub-name
                                (string-append err-1 err-2)
                                result-hash-table)
                               ))
                           ))
                       )) shouldbe-list-list)
                  )))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (amicable-chain
         start-num max-iterations max-element
         div-sum-htable seen-htable)
  (begin
    (let ((chain-list (list start-num))
          (chain-length 1)
          (current-num start-num)
          (current-iteration 0)
          (continue-loop-flag #t))
      (begin
        (if (> start-num 1)
            (begin
              (while
               (and (equal? continue-loop-flag #t)
                    (<= current-iteration max-iterations))
               (begin
                 (let ((next-num
                        (hash-ref div-sum-htable current-num 0)))
                   (begin
                     (cond
                      ((or (<= next-num 1)
                           (> next-num max-element)
                           (> chain-length max-iterations))
                       (begin
                          ;;; chain terminates
                         (set! continue-loop-flag #f)
                         (set! chain-list #f)
                         ))
                      ((= next-num start-num)
                       (begin
                          ;;; amicable chain found!
                         (set! continue-loop-flag #f)
                         (set! chain-list (cons next-num chain-list))
                         (set! chain-length (1+ chain-length))
                         ))
                      ((not (equal? (member next-num chain-list) #f))
                       (begin
                          ;;; chain forms inner cycle
                         (set! continue-loop-flag #f)
                         (set! chain-list #f)
                         ))
                      (else
                       (begin
                         (set! current-num next-num)
                         (set! chain-list (cons current-num chain-list))
                         (set! chain-length (1+ chain-length))
                         )))

                     (set! current-iteration (1+ current-iteration))
                     ))
                 ))
              ))

        (if (list? chain-list)
            (begin
              (for-each
               (lambda (anum)
                 (begin
                   (hash-set! seen-htable anum #t)
                   )) chain-list)
              (reverse chain-list))
            (begin
              #f
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-amicable-chain-1 result-hash-table)
 (begin
   (let ((sub-name "test-amicable-chain-1")
         (test-list
          (list
           (list 2 #f) (list 3 #f) (list 4 #f)
           (list 5 #f) (list 6 (list 6 6)) (list 7 #f)
           (list 8 #f) (list 9 #f) (list 10 #f)
           (list 11 #f) (list 12 #f) (list 13 #f)
           (list 14 #f) (list 15 #f) (list 16 #f)
           (list 17 #f) (list 18 #f) (list 19 #f)
           (list 20 #f) (list 21 #f) (list 22 #f)
           (list 23 #f) (list 24 #f) (list 25 #f)
           (list 26 #f) (list 27 #f) (list 28 (list 28 28))
           (list 29 #f) (list 220 (list 220 284 220))
           (list 284 (list 284 220 284))
           (list 12496 (list 12496 14288 15472 14536 14264 12496))
           ))
         (max-iterations 1000)
         (div-sum-htable (make-hash-table 100))
         (seen-htable (make-hash-table 100))
         (max-div-table-value 20000)
         (max-element 1000000)
         (test-label-index 0))
     (begin
       (populate-sum-of-divisors-hash! div-sum-htable max-div-table-value)

       (for-each
        (lambda (alist)
          (begin
            (hash-clear! seen-htable)

            (let ((start-num (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result
                     (amicable-chain
                      start-num max-iterations max-element
                      div-sum-htable seen-htable)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : start-num=~a, "
                        sub-name test-label-index start-num))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; print debug information
(define-syntax display-debug-info-macro
  (syntax-rules ()
    ((display-debug-info-macro
      ii amicable-list alen
      longest-chain-list longest-chain-length)
     (begin
       (display
        (ice-9-format:format
         #f "  ~:d : chain = ~a : length = ~:d : "
         ii amicable-list alen))
       (display
        (ice-9-format:format
         #f "longest chain = ~a : length = ~:d~%"
         longest-chain-list longest-chain-length))
       (force-output)
       ))
    ))

;;;#############################################################
;;;#############################################################
;;; print status information
(define-syntax display-status-info-macro
  (syntax-rules ()
    ((display-status-info-macro
      counter end-num longest-chain-start
      longest-chain-list longest-chain-length)
     (begin
       (display
        (ice-9-format:format
         #f "(~:d / ~:d) : longest so far : ~:d~%"
         counter end-num longest-chain-start))
       (display
        (ice-9-format:format
         #f "chain = ~a~%"
         longest-chain-list))
       (display
        (ice-9-format:format
         #f "length = ~:d : ~a~%"
         longest-chain-length
         (timer-module:current-date-time-string)))
       ))
    ))

;;;#############################################################
;;;#############################################################
;;; print summary information
(define-syntax display-summary-info-macro
  (syntax-rules ()
    ((display-summary-info-macro
      start-num end-num
      max-iterations max-element
      longest-chain-start
      longest-chain-list
      longest-chain-length)
     (begin
       (display
        (ice-9-format:format
         #f "for numbers between ~:d and ~:d, "
         start-num end-num))
       (display
        (ice-9-format:format
         #f "maximum iterations = ~:d~%" max-iterations))
       (display
        (ice-9-format:format
         #f "  with max-element < ~:d~%" max-element))
       (display
        (ice-9-format:format
         #f "  longest amicable chain starts at ~:d~%"
         longest-chain-start))
       (display
        (ice-9-format:format
         #f "  chain list = ~a~%"
         (utils-module:sequence-list-to-string
          longest-chain-list)))
       (display
        (ice-9-format:format
         #f "  length = ~:d~%"
         longest-chain-length))

       (display
        (ice-9-format:format
         #f "  smallest member of the longest chain = ~:d~%"
         (srfi-1:fold
          min
          (car longest-chain-list)
          longest-chain-list)))
       (display
        (ice-9-format:format
         #f "  (where no element exceeds ~:d)~%"
         max-element))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop
         start-num end-num max-iterations max-element
         status-num debug-flag)
  (begin
    (let ((longest-chain-length 0)
          (longest-chain-list (list))
          (longest-chain-start 0)
          (counter 0)
          (div-sum-htable (make-hash-table max-element))
          (seen-htable (make-hash-table 100)))
      (begin
        (populate-sum-of-divisors-hash! div-sum-htable max-element)

        (display
         (ice-9-format:format
          #f "completed populate-sum-of-divisors-hash! ~:d : ~a~%"
          max-element
          (timer-module:current-date-time-string)))
        (force-output)
        (gc)

        (do ((ii start-num (1+ ii)))
            ((> ii end-num))
          (begin
            (if (equal? (hash-ref seen-htable ii #f) #f)
                (begin
                  (let ((amicable-list
                         (amicable-chain
                          ii max-iterations max-element
                          div-sum-htable seen-htable)))
                    (begin
                      (if (list? amicable-list)
                          (begin
                            (let ((alen (length amicable-list)))
                              (begin
                                (if (> alen longest-chain-length)
                                    (begin
                                      (if (equal? debug-flag #t)
                                          (begin
                                            (display-debug-info-macro
                                             ii amicable-list alen
                                             longest-chain-list
                                             longest-chain-length)
                                            ))

                                      (set! longest-chain-length alen)
                                      (set! longest-chain-list amicable-list)
                                      (set! longest-chain-start ii)
                                      ))
                                ))
                            ))
                      ))
                  ))

            (set! counter (1+ counter))
            (if (zero? (modulo counter status-num))
                (begin
                  (display-status-info-macro
                   counter end-num longest-chain-start
                   longest-chain-list longest-chain-length)
                  (force-output)
                  ))
            ))

        (newline)
        (display-summary-info-macro
         start-num end-num
         max-iterations max-element
         longest-chain-start
         longest-chain-list
         longest-chain-length)
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "The proper divisors of a number "))
    (display
     (format #f "are all the divisors~%"))
    (display
     (format #f "excluding the number itself. For "))
    (display
     (format #f "example, the proper~%"))
    (display
     (format #f "divisors of 28 are 1, 2, 4, 7, "))
    (display
     (format #f "and 14. As the~%"))
    (display
     (format #f "sum of these divisors is equal to "))
    (display
     (format #f "28, we call it~%"))
    (display
     (format #f "a perfect number.~%"))
    (newline)
    (display
     (format #f "Interestingly the sum of the "))
    (display
     (format #f "proper divisors of 220~%"))
    (display
     (format #f "is 284 and the sum of the proper "))
    (display
     (format #f "divisors of 284 is~%"))
    (display
     (format #f "220, forming a chain of two "))
    (display
     (format #f "numbers. For this reason,~%"))
    (display
     (format #f "220 and 284 are called an amicable "))
    (display
     (format #f "pair.~%"))
    (newline)
    (display
     (format #f "Perhaps less well known are "))
    (display
     (format #f "longer chains. For~%"))
    (display
     (format #f "example, starting with 12496, "))
    (display
     (format #f "we form a chain of five numbers:~%"))
    (newline)
    (display
     (format #f "  12496 -> 14288 -> 15472 -> 14536 "))
    (display
     (format #f "-> 14264 (-> 12496 -> ...)~%"))
    (newline)
    (display
     (format #f "Since this chain returns to its "))
    (display
     (format #f "starting point, it~%"))
    (display
     (format #f "is called an amicable chain.~%"))
    (newline)
    (display
     (format #f "Find the smallest member of the "))
    (display
     (format #f "longest amicable chain~%"))
    (display
     (format #f "with no element exceeding one "))
    (display
     (format #f "million.~%"))
    (newline)
    (display
     (format #f "This solution uses the sieve of "))
    (display
     (format #f "Eratosthenes to quickly~%"))
    (display
     (format #f "find the sum of the proper divisors "))
    (display
     (format #f "for each number up~%"))
    (display
     (format #f "to a million,~%"))
    (display
     (format #f "https://en.wikipedia.org/wiki/Sieve_of_Eratosthenes~%"))
    (display
     (format #f "see https://projecteuler.net/problem=95~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((start-num 1)
          (end-num 1000)
          (max-iterations 1000)
          (max-element 100000)
          (status-num 10000)
          (debug-flag #t))
      (begin
        (sub-main-loop
         start-num end-num max-iterations max-element
         status-num debug-flag)
        ))

    (newline)
    (let ((start-num 1)
          (end-num 1000000)
          (max-iterations 1000)
          (max-element 1000000)
          (status-num 5000000)
          (debug-flag #f))
      (begin
        (sub-main-loop
         start-num end-num max-iterations max-element
         status-num debug-flag)
        ))
    ))


;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 95 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

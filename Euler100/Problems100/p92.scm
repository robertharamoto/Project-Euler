#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 92                                     ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated July 27, 2022                                ###
;;;###                                                       ###
;;;###  updated March 10, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### srfi-1 for fold function
(use-modules ((srfi srfi-1)
              :renamer (symbol-prefix-proc 'srfi-1:)))

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### digits-module for split-digits-list function
(use-modules ((digits-module)
              :renamer (symbol-prefix-proc 'digits-module:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (apply-squared-digits this-num)
  (begin
    (let ((current-list
           (digits-module:split-digits-list this-num)))
      (let ((next-num
             (srfi-1:fold
              + 0
              (map
               (lambda (this-digit)
                 (begin
                   (* this-digit this-digit)
                   )) current-list))))
        (begin
          next-num
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-apply-squared-digits-1 result-hash-table)
 (begin
   (let ((sub-name "test-apply-squared-digits-1")
         (test-list
          (list
           (list 44 32) (list 32 13) (list 13 10)
           (list 10 1) (list 1 1) (list 85 89)
           (list 89 145) (list 42 20) (list 20 4)
           (list 4 16) (list 16 37) (list 37 58)
           (list 58 89)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((num (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result
                     (apply-squared-digits num)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : num=~a, "
                        sub-name test-label-index num))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a~%"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (make-number-chain
         start-num target-1 target-2 memo-htable)
  (begin
    (cond
     ((<= start-num 0)
      (begin
        (list 0)
        ))
     (else
      (begin
        (let ((local-num start-num)
              (chain-list (list start-num))
              (loop-continue-flag #t))
          (begin
            (while
             (equal? loop-continue-flag #t)
             (begin
               (let ((next-num
                      (hash-ref memo-htable local-num #f)))
                 (begin
                   (if (equal? next-num #f)
                       (begin
                         (let ((calc-num
                                (apply-squared-digits local-num)))
                           (begin
                             (set! next-num calc-num)
                             (hash-set!
                              memo-htable local-num calc-num)
                             ))
                         ))

                   (if (equal?
                        (member next-num chain-list)
                        #f)
                       (begin
                         (set!
                          chain-list
                          (cons next-num chain-list)))
                       (begin
                         (if (or (equal? next-num target-1)
                                 (equal? next-num target-2))
                             (begin
                               (set!
                                chain-list
                                (cons next-num chain-list))
                               (set! loop-continue-flag #f)
                               ))
                         ))

                   (set! local-num next-num)
                   ))
               ))
            (reverse chain-list)
            ))
        )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-make-number-chain-1 result-hash-table)
 (begin
   (let ((sub-name "test-make-number-chain-1")
         (test-list
          (list
           (list 44 (list 44 32 13 10 1 1))
           (list 85 (list 85 89 145 42 20 4 16 37 58 89))
           ))
         (target-1 1)
         (target-2 89)
         (memo-htable (make-hash-table 100))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-num (list-ref alist 0))
                  (shouldbe-list (list-ref alist 1)))
              (let ((result-list
                     (make-number-chain
                      test-num target-1 target-2 memo-htable)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : test-num=~a, "
                        sub-name test-label-index test-num))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a~%"
                        shouldbe-list result-list)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-list result-list)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (populate-2nd-opt-hash!
         terminating-htable max-pop target-1 target-2)
  (begin
    (let ((memo-htable (make-hash-table 100)))
      (begin
        (hash-clear! terminating-htable)

        (do ((ii 0 (1+ ii)))
            ((> ii max-pop))
          (begin
            (let ((ii-tmp
                   (hash-ref terminating-htable ii #f)))
              (begin
                (if (equal? ii-tmp #f)
                    (begin
                      (let ((tlist
                             (make-number-chain
                              ii target-1 target-2 memo-htable)))
                        (begin
                          (let ((last-num
                                 (car (last-pair tlist))))
                            (begin
                              (hash-set!
                               terminating-htable ii last-num)
                              ))
                          ))
                      ))
                ))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (first-two-optimizations-loop
         start-num end-num max-pop target-1 target-2)
  (begin
    (let ((target-counter 0)
          (terminating-htable (make-hash-table 600)))
      (begin
        (display
         (format
          #f "first two optimizations (store terminating~%"))
        (display
         (format
          #f "results for ~a numbers in a hash table)~%"
          max-pop))
        (force-output)
        ;;; initialize the loop
        (populate-2nd-opt-hash!
         terminating-htable max-pop target-1 target-2)

        (do ((ii start-num (1+ ii)))
            ((> ii end-num))
          (begin
            (let ((tnum
                   (apply-squared-digits ii)))
              (let ((terminal-number
                     (hash-ref terminating-htable tnum #f)))
                (begin
                  (if (equal? terminal-number #f)
                      (begin
                        (display
                         (format
                          #f "first-two-optimizations-loop error~%"))
                        (display
                         (format
                          #f "  ii=~a, tnum=~a, no matching terminal number!~%"
                          ii tnum))
                        (display
                         (format #f "quitting...~%"))
                        (force-output)
                        (quit))
                      (begin
                        (if (equal? terminal-number target-2)
                            (begin
                              (set! target-counter (1+ target-counter))
                              ))
                        ))
                  )))
            ))

        (display
         (ice-9-format:format
          #f "Number of chains that terminate at ~:d~%"
          target-2))
        (display
         (ice-9-format:format
          #f "between ~:d and ~:d is ~:d~%"
          start-num end-num target-counter))
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (factorial ii-num)
  (begin
    (cond
     ((<= ii-num 1)
      (begin
        1
        ))
     ((= ii-num 2)
      (begin
        2
        ))
     ((= ii-num 3)
      (begin
        6
        ))
     ((= ii-num 4)
      (begin
        24
        ))
     ((= ii-num 5)
      (begin
        120
        ))
     ((= ii-num 6)
      (begin
        720
        ))
     ((= ii-num 7)
      (begin
        5040
        ))
     ((= ii-num 8)
      (begin
        40320
        ))
     ((= ii-num 9)
      (begin
        362880
        ))
     (else
      (begin
        (* ii-num (factorial (- ii-num 1)))
        )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-factorial-1 result-hash-table)
 (begin
   (let ((sub-name "test-factorial-1")
         (test-list
          (list
           (list 0 1) (list 1 1) (list 2 2)
           (list 3 6) (list 4 24) (list 5 120)
           (list 6 720) (list 7 5040)
           (list 8 40320) (list 9 362880)
           (list 10 3628800)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-num (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result (factorial test-num)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : test-nun=~a, "
                        sub-name test-label-index test-num))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a~%"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (calc-combinations digit-list)
  (begin
    (let ((d-htable (make-hash-table 10))
          (d-count 0))
      (begin
        (for-each
         (lambda (digit)
           (begin
             (let ((this-count (hash-ref d-htable digit 0)))
               (begin
                 (hash-set! d-htable digit (1+ this-count))
                 (set! d-count (1+ d-count))
                 ))
             )) digit-list)
        (let ((permutations
               (factorial d-count)))
          (begin
            (hash-for-each
             (lambda (digit d-count)
               (begin
                 (let ((rr (factorial d-count)))
                   (begin
                     (set!
                      permutations
                      (euclidean/ permutations rr))
                     ))
                 )) d-htable)

            permutations
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-calc-combinations-1 result-hash-table)
 (begin
   (let ((sub-name "test-calc-combinations-1")
         (test-list
          (list
           (list (list 1 1) 1) (list (list 1 2) 2)
           (list (list 2 2) 1) (list (list 1 2 3) 6)
           (list (list 1 1 2) 3) (list (list 1 1 2 2) 6)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((input-list (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result (calc-combinations input-list)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : input-list=~a, "
                        sub-name test-label-index input-list))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a~%"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; define a macro to simplify code
(define-syntax update-terminating-hashes
  (syntax-rules ()
    ((update-terminating-hashes tlist terminating-htable perm-htable)
     (begin
       (let ((last-num (car (last-pair tlist))))
         (begin
           (for-each
            (lambda (a-num)
              (begin
                (let ((a-list
                       (sort
                        (digits-module:split-digits-list a-num)
                        <)))
                  (begin
                    (hash-set!
                     terminating-htable a-list last-num)
                    (let ((perm-count
                           (calc-combinations a-list)))
                      (begin
                        (hash-set! perm-htable a-list perm-count)
                        ))
                    ))
                )) tlist)
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (non-decreasing-list-list start-num end-num max-depth)
  (define (local-loop
           depth max-depth current-max end-num
           current-list acc-list)
    (begin
      (cond
       ((>= depth max-depth)
        (begin
          (let ((s-list (sort current-list <)))
            (begin
              (if (equal? (member s-list acc-list))
                  (begin
                    (cons s-list acc-list))
                  (begin
                    acc-list
                    ))
              ))
          ))
       (else
        (begin
          (do ((ii current-max (1+ ii)))
              ((> ii end-num))
            (begin
              (let ((this-list (cons ii current-list)))
                (let ((next-max
                       (srfi-1:fold max ii this-list)))
                  (let ((next-acc-list
                         (local-loop
                          (1+ depth) max-depth next-max
                          end-num this-list acc-list)))
                    (begin
                      (set! acc-list next-acc-list)
                      ))
                  ))
              ))
          acc-list
          )))
      ))
  (begin
    (let ((rlist
           (local-loop
            0 max-depth start-num end-num (list) (list))))
      (begin
        (reverse rlist)
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-non-decreasing-list-list-1 result-hash-table)
 (begin
   (let ((sub-name "test-non-decreasing-list-list-1")
         (test-list
          (list
           (list 0 2 1
                 (list (list 0) (list 1) (list 2)))
           (list 2 3 2
                 (list (list 2 2) (list 2 3) (list 3 3)))
           (list 2 3 3
                 (list (list 2 2 2) (list 2 2 3) (list 2 3 3)
                       (list 3 3 3)))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((start-num (list-ref alist 0))
                  (end-num (list-ref alist 1))
                  (max-depth (list-ref alist 2))
                  (shouldbe (list-ref alist 3)))
              (let ((result
                     (non-decreasing-list-list
                      start-num end-num max-depth)))
                (let ((slen (length shouldbe))
                      (rlen (length result)))
                  (let ((err-1
                         (format
                          #f "~a : error (~a) : start-num=~a, "
                          sub-name test-label-index start-num))
                        (err-2
                         (format
                          #f "end-num=~a, max-depth=~a, "
                          end-num max-depth))
                        (err-3
                         (format
                          #f "shouldbe length=~a, result=~a"
                          slen rlen)))
                    (begin
                      (unittest2:assert?
                       (equal? slen rlen)
                       sub-name
                       (string-append err-1 err-2 err-3)
                       result-hash-table)

                      (for-each
                       (lambda (slist)
                         (begin
                           (let ((err-4
                                  (format
                                   #f "slist=~a, "
                                   slist))
                                 (err-5
                                  (format
                                   #f "result-list-list=~a"
                                   result)))
                             (begin
                               (unittest2:assert?
                                (not
                                 (equal?
                                  (member slist result)
                                  #f))
                                sub-name
                                (string-append
                                 err-1 err-2 err-4 err-5)
                                result-hash-table)
                               ))
                           )) shouldbe)
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (populate-3rd-opt-hash!
         terminating-htable perm-htable
         max-depth target-1 target-2)
  (begin
    (let ((memo-htable (make-hash-table 1000))
          (non-list-list
           (non-decreasing-list-list 0 9 max-depth)))
      (begin
        (hash-clear! terminating-htable)
        (hash-clear! perm-htable)

        (for-each
         (lambda (dlist)
           (begin
             (let ((ii-tmp
                    (hash-ref terminating-htable dlist #f)))
               (begin
                 (if (equal? ii-tmp #f)
                     (begin
                       (let ((dnum
                              (srfi-1:fold
                               (lambda (num prev)
                                 (begin
                                   (+ num (* prev 10))
                                   ))
                               0 dlist)))
                         (let ((tlist
                                (make-number-chain
                                 dnum
                                 target-1
                                 target-2
                                 memo-htable)))
                           (let ((last-num
                                  (car (last-pair tlist))))
                             (begin
                               (hash-set!
                                terminating-htable dlist last-num)

                               (let ((perm-count
                                      (calc-combinations dlist)))
                                 (begin
                                   (hash-set!
                                    perm-htable dlist perm-count)
                                   ))
                               ))
                           ))
                       ))
                 ))
             )) non-list-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
;;; all permutations of digits have the same sum of squares of their digits
(define (third-optimization-loop
         start-num end-num max-depth target-1 target-2)
  (begin
    (let ((total 0)
          (terminating-htable (make-hash-table 1000))
          (perm-htable (make-hash-table 1000)))
      (begin
        (display
         (format
          #f "digit list optimization (store sorted~%"))
        (display
         (format
          #f "digit-list/terminating results in a hash table)~%"))
        (force-output)

        (populate-3rd-opt-hash!
         terminating-htable
         perm-htable max-depth target-1 target-2)

        (hash-for-each
         (lambda (dlist terminating-number)
           (begin
             (if (equal? terminating-number target-2)
                 (begin
                   (let ((perm-count
                          (hash-ref perm-htable dlist 0)))
                     (begin
                       (set! total (+ total perm-count))
                       ))
                   ))
             )) terminating-htable)

        (display
         (ice-9-format:format
          #f "Number of chains that terminate at ~:d~%"
          target-2))
        (display
         (ice-9-format:format
          #f "between ~:d and ~:d is ~:d~%"
          start-num end-num total))
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (list-to-string llist separator)
  (begin
    (let ((result-string
           (string-join
            (map
             (lambda (num)
               (begin
                 (ice-9-format:format #f "~:d" num)
                 )) llist)
            separator)))
      (let ((final-string
             (string-append "{ " result-string " }")))
        (begin
          final-string
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-list-to-string-1 result-hash-table)
 (begin
   (let ((sub-name "test-list-to-string-1")
         (test-list
          (list
           (list (list 1 1) ", " "{ 1, 1 }")
           (list (list 1 2) " -> " "{ 1 -> 2 }")
           (list (list 1 2 3) " -> " "{ 1 -> 2 -> 3 }")
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((num-list (list-ref alist 0))
                  (separator (list-ref alist 1))
                  (shouldbe (list-ref alist 2)))
              (let ((result
                     (list-to-string num-list separator)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : num-list=~a, separator=~a, "
                        sub-name test-label-index num-list separator))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a~%"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; all permutations of digits have the same sum of squares of their digits
(define (sub-main-loop
         start-num end-num target-1 target-2 debug-flag)
  (begin
    (let ((target-counter 0)
          (memo-htable (make-hash-table 1000)))
      (begin
        (display (format #f "brute force method~%"))
        (force-output)

        (do ((ii start-num (1+ ii)))
            ((> ii end-num))
          (begin
            (let ((tlist
                   (make-number-chain
                    ii target-1 target-2 memo-htable)))
              (let ((last-num (car (last-pair tlist))))
                (begin
                  (if (equal? last-num target-2)
                      (begin
                        (set! target-counter (1+ target-counter))

                        (if (equal? debug-flag #t)
                            (begin
                              (let ((lstring
                                     (list-to-string tlist " -> "))
                                    (lcount (length tlist)))
                                (begin
                                  (display
                                   (ice-9-format:format
                                    #f "(~:d) : ~a : "
                                    ii lstring))
                                  (display
                                   (ice-9-format:format
                                    #f "sequence length = ~:d~%"
                                    lcount))
                                  (force-output)
                                  ))
                              ))
                        ))
                  )))
            ))

        (display
         (ice-9-format:format
          #f "Number of chains that terminate at ~:d~%"
          target-2))
        (display
         (ice-9-format:format
          #f "between ~:d and ~:d is ~:d~%"
          start-num end-num target-counter))
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "A number chain is created by "))
    (display
     (format #f "continuously adding~%"))
    (display
     (format #f "the square of the digits in a "))
    (display
     (format #f "number to form a~%"))
    (display
     (format #f "new number until it has been "))
    (display
     (format #f "seen before.~%"))
    (newline)
    (display
     (format #f "For example,~%"))
    (display
     (format #f "  44 -> 32 -> 13 -> 10 -> 1 -> 1~%"))
    (display
     (format #f "  85 -> 89 -> 145 -> 42 -> 20 -> "))
    (display
     (format #f "4 -> 16 -> 37 -> 58 -> 89~%"))
    (newline)
    (display
     (format #f "Therefore any chain that arrives "))
    (display
     (format #f "at 1 or 89 will~%"))
    (display
     (format #f "become stuck in an endless loop. What "))
    (display
     (format #f "is most amazing is~%"))
    (display
     (format #f "that EVERY starting number will "))
    (display
     (format #f "eventually arrive at~%"))
    (display
     (format #f "1 or 89.~%"))
    (newline)
    (display
     (format #f "How many starting numbers below "))
    (display
     (format #f "ten million will arrive~%"))
    (display
     (format #f "at 89?~%"))
    (newline)
    (display
     (format #f "The solution follows the clever ideas "))
    (display
     (format #f "found at~%"))
    (display
     (format #f "https://martin-ueding.de/posts/project-euler-solution-92-square-digit-chains/~%"))
    (display
     (format #f "see https://projecteuler.net/problem=93~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((start-num 1)
          (end-num 10)
          (target-1 1)
          (target-2 89)
          (debug-flag #t))
      (begin
        (sub-main-loop
         start-num end-num target-1 target-2 debug-flag)
        ))

    (newline)
    (let ((start-num 1)
          (end-num 10)
          (max-pop 81)
          (max-depth 2)
          (target-1 1)
          (target-2 89)
          (debug-flag #f))
      (begin
        (first-two-optimizations-loop
         start-num end-num max-pop target-1 target-2)
        ))

    (newline)
    (let ((start-num 1)
          (end-num 10)
          (max-depth 1)
          (target-1 1)
          (target-2 89)
          (debug-flag #f))
      (begin
        (third-optimization-loop
         start-num end-num max-depth target-1 target-2)
        ))

    (newline)
    (let ((start-num 1)
          (end-num 10000000)
          (max-pop 567)
          (target-1 1)
          (target-2 89))
      (begin
        (first-two-optimizations-loop
         start-num end-num max-pop target-1 target-2)
        ))


    (newline)
    (let ((start-num 1)
          (end-num 10000000)
          (max-depth 7)
          (target-1 1)
          (target-2 89))
      (begin
        (third-optimization-loop
         start-num end-num max-depth target-1 target-2)
        ))


    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 92 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (load "digits-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 99                                     ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated July 27, 2022                                ###
;;;###                                                       ###
;;;###  updated March 10, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 rdelim for read-line functions
(use-modules ((ice-9 rdelim)
              :renamer (symbol-prefix-proc 'ice-9-rdelim:)))

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
;;; format of base-exponents list = (list line-counter base exponent)
(define (return-largest-list base-exponents-list)
  (begin
    (let ((result-list (list))
          (max-log 0.0))
      (begin
        (for-each
         (lambda (this-list)
           (begin
             (let ((this-base (list-ref this-list 1))
                   (this-exponent (list-ref this-list 2)))
               (let ((this-log (* this-exponent (log this-base))))
                 (if (> this-log max-log)
                     (begin
                       (set! result-list this-list)
                       (set! max-log this-log)
                       ))
                 ))
             )) base-exponents-list)

        result-list
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-return-largest-list-1 result-hash-table)
 (begin
   (let ((sub-name "test-return-largest-list-1")
         (test-list
          (list
           (list (list (list 1 2 3)
                       (list 2 2 4)
                       (list 3 2 2))
                 (list 2 2 4))
           (list (list (list 1 2 3)
                       (list 2 2 4)
                       (list 3 3 2))
                 (list 2 2 4))
           (list (list (list 1 2 3)
                       (list 2 2 4)
                       (list 3 3 3))
                 (list 3 3 3))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((base-exponents-list (list-ref alist 0))
                  (shouldbe-list (list-ref alist 1)))
              (let ((result-list
                     (return-largest-list base-exponents-list)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "base-exponents-list=~a, "
                        base-exponents-list))
                      (err-3
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe-list result-list)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-list result-list)
                     sub-name
                     (string-append
                      err-1 err-2 err-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; returns a list of lists
(define (read-in-file fname)
  (begin
    (let ((results-list (list))
          (line-counter 0))
      (begin
        (if (file-exists? fname)
            (begin
              (with-input-from-file fname
                (lambda ()
                  (begin
                    (do ((line
                          (ice-9-rdelim:read-delimited "\r\n")
                          (ice-9-rdelim:read-delimited "\r\n")))
                        ((eof-object? line))
                      (begin
                        (if (and (not (eof-object? line))
                                 (> (string-length line) 0))
                            (begin
                              (let ((llist
                                     (string-split line #\,)))
                                (let ((base
                                       (string->number (list-ref llist 0)))
                                      (exponent
                                       (string->number (list-ref llist 1))))
                                  (begin
                                    (set! line-counter (1+ line-counter))
                                    (let ((this-list
                                           (list line-counter base exponent)))
                                      (begin
                                        (set! results-list
                                              (cons this-list results-list))
                                        ))
                                    )))
                              ))
                        ))
                    )))

              (display
               (ice-9-format:format
                #f "read in ~a lines from ~a~%"
                line-counter fname))
              (newline)
              (force-output)

              (reverse results-list))
            (begin
              (list)
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
;;; format of result list = (list line-counter base exponent this-log)
(define (display-result-list result-list)
  (begin
    (let ((line-counter (list-ref result-list 0))
          (base (list-ref result-list 1))
          (exponent (list-ref result-list 2)))
      (let ((logarithm (* exponent (log base))))
        (begin
          (display
           (ice-9-format:format
            #f "~:d is the line number with the "
            line-counter))
          (display
           (ice-9-format:format
            #f "greatest numerical value~%"))
          (display
           (ice-9-format:format
            #f "log(~:d^~:d) = ~a~%"
            base exponent logarithm))
          (newline)
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Comparing two numbers written in "))
    (display
     (format #f "index form like~%"))
    (display
     (format #f "2^11 and 3^7 is not difficult, as "))
    (display
     (format #f "any calculator would~%"))
    (display
     (format #f "confirm that 2^11 = 2048 < 3^7 = 2187.~%"))
    (newline)
    (display
     (format #f "However, confirming that "))
    (display
     (format #f "632382^518061 > 519432^525806~%"))
    (display
     (format #f "would be much more difficult, as "))
    (display
     (format #f "both numbers contain~%"))
    (display
     (format #f "over three million digits.~%"))
    (newline)
    (display
     (format #f "Using base_exp.txt~%"))
    (display
     (format #f "https://projecteuler.net/project/resources/p099_base_exp.txt~%"))
    (display
     (format #f "a 22K text file containing one "))
    (display
     (format #f "thousand lines with~%"))
    (display
     (format #f "a base/exponent pair on each line, "))
    (display
     (format #f "determine which line~%"))
    (display
     (format #f "number has the greatest numerical "))
    (display
     (format #f "value.~%"))
    (newline)
    (display
     (format #f "NOTE: The first two lines in the "))
    (display
     (format #f "file represent the~%"))
    (display
     (format #f "numbers in the example given above.~%"))
    (newline)
    (display
     (format #f "This problem is solved using the "))
    (display
     (format #f "logarithmic function, which~%"))
    (display
     (format #f "has the nice properties of being "))
    (display
     (format #f "one-to-one and an~%"))
    (display
     (format #f "increasing function. It reduces the "))
    (display
     (format #f "problem of calculating~%"))
    (display
     (format #f "all 3 million digits, to calculating "))
    (display
     (format #f "an equivalent number~%"))
    (display
     (format #f "with 7 digits (plus decimal digits). "))
    (display
     (format #f "Since all we are~%"))
    (display
     (format #f "looking for is the largest power, "))
    (display
     (format #f "and since if x > y,~%"))
    (display
     (format #f "then log(x) > log(y), our calculations "))
    (display
     (format #f "will be speeded~%"))
    (display
     (format #f "up tremendously.~%"))
    (newline)
    (display
     (format #f "for more information see~%"))
    (display
     (format #f "https://en.wikipedia.org/wiki/Logarithm~%"))
    (display
     (format #f "see https://projecteuler.net/problem=99~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((base-exponents-list
           (list (list 1 632382 518061)
                 (list 2 519432 525806))))
      (begin
        (let ((max-list
               (return-largest-list
                base-exponents-list)))
          (begin
            (for-each
             (lambda (alist)
               (begin
                 (display (format #f "~a~%" alist))
                 )) base-exponents-list)

            (display-result-list max-list)
            ))
        ))

    (newline)
    (let ((filename "base_exp.txt"))
      (begin
        (let ((base-exponents-list (read-in-file filename)))
          (let ((max-list
                 (return-largest-list
                  base-exponents-list)))
            (begin
              (display-result-list max-list)
              )))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 99 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

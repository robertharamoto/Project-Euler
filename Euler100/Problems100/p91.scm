#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 91                                     ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated July 27, 2022                                ###
;;;###                                                       ###
;;;###  updated March 10, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (calc-t-count ii-xx ii-yy max-num)
  (begin
    (let ((xy-gcd (gcd ii-xx ii-yy)))
      (let ((dx (euclidean/ ii-xx xy-gcd))
            (dy (euclidean/ ii-yy xy-gcd)))
        (let ((xx-count (euclidean/ (- max-num ii-xx) dy))
              (yy-count (euclidean/ ii-yy dx)))
          (let ((result (* 2 (min xx-count yy-count))))
            (begin
              result
              )))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-calc-t-count-1 result-hash-table)
 (begin
   (let ((sub-name "test-calc-t-count-1")
         (test-list
          (list
           (list 1 2 10 4) (list 1 2 6 4) (list 1 2 4 2)
           (list 2 4 11 8) (list 2 4 10 8) (list 2 4 9 6)
           (list 2 4 8 6) (list 2 4 7 4)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((ii-xx (list-ref alist 0))
                  (ii-yy (list-ref alist 1))
                  (max-num (list-ref alist 2))
                  (shouldbe (list-ref alist 3)))
              (let ((result
                     (calc-t-count ii-xx ii-yy max-num)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) :  "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "ii-xx=~a, ii-yy=~a, max-num=~a, "
                        ii-xx ii-yy max-num))
                      (err-3
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append
                      err-1 err-2 err-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop max-num)
  (begin
    (let ((triangle-count (* 3 max-num max-num)))
      (begin
        (do ((ii-xx 1 (1+ ii-xx)))
            ((> ii-xx max-num))
          (begin
            (do ((ii-yy 1 (1+ ii-yy)))
                ((> ii-yy max-num))
              (begin
                (let ((t-count
                       (calc-t-count ii-xx ii-yy max-num)))
                  (begin
                    (set! triangle-count (+ triangle-count t-count))
                    ))
                ))
            ))

        (display
         (ice-9-format:format
          #f "Number of triangles between 0 and ~:d is ~:d~%"
          max-num triangle-count))
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "The points P (x1, y1) and Q (x2, y2) "))
    (display
     (format #f "are plotted at~%"))
    (display
     (format #f "integer co-ordinates and are joined "))
    (display
     (format #f "to the origin,~%"))
    (display
     (format #f "O(0,0), to form triangle(OPQ).~%"))
    (newline)
    (display
     (format #f "There are exactly fourteen triangles "))
    (display
     (format #f "containing a right~%"))
    (display
     (format #f "angle that can be formed when "))
    (display
     (format #f "each co-ordinate lies~%"))
    (display
     (format #f "between 0 and 2 inclusive; that is,~%"))
    (display
     (format #f "0 <= x1, y1, "))
    (display
     (format #f "x2, y2 <= 2.~%"))
    (newline)
    (display
     (format #f "Given that 0 <= x1, y1, x2, y2 <= 50, "))
    (display
     (format #f "how many~%"))
    (display
     (format #f "right triangles can be formed?~%"))
    (newline)
    (display
     (format #f "If you are looking at a triangle "))
    (display
     (format #f "with a point on~%"))
    (display
     (format #f "the x-axis and another point on "))
    (display
     (format #f "the y-axis, then~%"))
    (display
     (format #f "it will be a right triangle, and "))
    (display
     (format #f "those are easy~%"))
    (display
     (format #f "to count.  For the rest, consider "))
    (display
     (format #f "the point (1, 2),~%"))
    (display
     (format #f "then a right triangle will be formed "))
    (display
     (format #f "if you use the~%"))
    (display
     (format #f "origin and the point (2, 1). To see "))
    (display
     (format #f "this, note that~%"))
    (display
     (format #f "the slope of the from the origin "))
    (display
     (format #f "to (1, 2) is 2,~%"))
    (display
     (format #f "and the slope of the line from "))
    (display
     (format #f "(1, 2) to (2, 1)~%"))
    (display
     (format #f "is -1/2, which makes the two lines "))
    (display
     (format #f "meet at right angles.~%"))
    (newline)
    (display
     (format #f "Also, need to account for the fact "))
    (display
     (format #f "that (1, 2), can~%"))
    (display
     (format #f "have multiple right triangles, the "))
    (display
     (format #f "point (3, 1) gives a~%"))
    (display
     (format #f "right triangle, but so does (5, 0). So "))
    (display
     (format #f "the triangles can~%"))
    (display
     (format #f "be obtained by adding a delta to "))
    (display
     (format #f "the initial point,~%"))
    (display
     (format #f "(2, 1) = (1, 2) + 1*(2, -1), and~%"))
    (display
     (format #f "(5, 0) = (1, 2) + 2*(2, -1),~%"))
    (display
     (format #f "this delta can be computed from the "))
    (display
     (format #f "slope (in~%"))
    (display
     (format #f "reduced form). See the "))
    (display
     (format #f "image at~%"))
    (display
     (format #f "http://oop123.files.wordpress.com/2011/08/with_slope_31.png?w=595~%"))
    (display
     (format #f "see https://projecteuler.net/problem=91~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((max-num 2))
      (begin
        (sub-main-loop max-num)
        ))

    (newline)
    (let ((max-num 50))
      (begin
        (sub-main-loop max-num)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 91 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

################################################################
################################################################
###                                                          ###
###  project euler exercises                                 ###
###                                                          ###
###  written by Robert Haramoto                              ###
###                                                          ###
################################################################
################################################################

This is a repository for the project euler problems 1 - 10.
https://projecteuler.net/

Some great hints can be found from:
https://mathproblems123.wordpress.com/2017/03/28/project-euler-tips/


The programs are written in guile 3.0, a version of scheme, to
encourage interest in this nice language.

They can be used as a template or library for commonly used
functions, (prime?, split-digits, ...)

Simple unit tests are included and run each time the program
is run, and help to give confidence that the programs run
correctly.  Example results are reproduced from the problem
statement to also show that the algorithms are ok.

These programs are dedicated to the public domain.
See the UNLICENSE file, or https://unlicense.org/

Assumes that guile 3.0 is located in the /usr/bin directory.

See also:

https://www.gnu.org/software/guile/
https://en.wikipedia.org/wiki/Scheme_(programming_language)

################################################################
################################################################
Some of the functions are used in several exercises.  These
have been moved into their own separate modules under the
"do no repeat yourself" principle.
https://en.wikipedia.org/wiki/Don%27t_repeat_yourself

digits-module - functions used to manipulate digits or lists of digits
  split-digits-list

prime-module - functions used to find prime numbers
  is-prime?

timer-module - functions used to compute the elapsed time
  time-code-macro

utils-module - functions used by timer-module

unittest2.scm - test harness
  define-tests-macro
  assert?


################################################################
################################################################
History

last updated <2024-09-26 Thu>
updated <2022-06-09 Thu>
updated <2020-03-03 Tue>

################################################################
################################################################
###                                                          ###
###  end of file                                             ###
###                                                          ###
################################################################
################################################################

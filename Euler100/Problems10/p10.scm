#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 10                                     ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated June 9, 2022                                 ###
;;;###                                                       ###
;;;###  updated March 4, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
;;; make a list of primes less than or equal to n
;;; sieve of eratosthenes method
(define (sum-prime-array max-num)
  (begin
    (let ((intermediate-array (make-array 0 max-num))
          (prime-sum 0))
      (begin
        (do ((ii 0 (1+ ii)))
            ((>= ii max-num))
          (begin
            (array-set! intermediate-array ii ii)
            ))

        (do ((ii 2 (1+ ii)))
            ((>= ii max-num))
          (begin
            (let ((ii-num (array-ref intermediate-array ii)))
              (begin
                (if (equal? ii ii-num)
                    (begin
                      ;;; we found a prime
                      (set! prime-sum (+ prime-sum ii-num))

                      (do ((jj (+ ii ii) (+ jj ii)))
                          ((>= jj max-num))
                        (begin
                          (let ((jj-num
                                 (array-ref intermediate-array jj)))
                            (begin
                              (array-set! intermediate-array -1 jj)
                              ))
                          ))
                      ))
                ))
            ))

        prime-sum
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-sum-prime-array-1 result-hash-table)
 (begin
   (let ((sub-name "test-sum-prime-array-1")
         (test-list
          (list
           (list 2 0) (list 3 2) (list 4 5)
           (list 5 5) (list 6 10)
           (list 7 10) (list 8 17) (list 9 17)
           (list 10 17) (list 11 17) (list 12 28)
           (list 13 28) (list 14 41) (list 15 41)
           (list 16 41) (list 17 41) (list 18 58)
           (list 19 58) (list 20 77) (list 21 77)
           (list 22 77) (list 23 77) (list 24 100)
           (list 25 100)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((test-num (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result (sum-prime-array test-num)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : num=~a, "
                        sub-name test-label-index test-num))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a~%"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "The sum of the primes below 10 is~%"))
    (display
     (format #f "2 + 3 + 5 + 7 = 17.~%"))
    (display
     (format #f "Find the sum of all the primes "))
    (display
     (format #f "below two~%"))
    (display
     (format #f "million.~%"))
    (display
     (format #f "see https://projecteuler.net/problem=10~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop ii)
  (begin
    (let ((sum-primes (sum-prime-array ii)))
      (begin
        (display
         (ice-9-format:format
          #f "the sum of primes less than ~:d is ~:d~%"
          ii sum-primes))
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (do ((ii 10 (+ ii 1)))
        ((> ii 20))
      (begin
        (sub-main-loop ii)
        ))

    (let ((nn 2000000))
      (begin
        (sub-main-loop nn)
        (newline)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 10 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (force-output)
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <http://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 1                                      ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated June 9, 2022                                 ###
;;;###                                                       ###
;;;###  updated March 4, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (calc-sum max-num debug-flag)
  (begin
    (let ((isum 0))
      (begin
        (do ((ii 1 (+ ii 1)))
            ((>= ii max-num))
          (begin
            (if (or (zero? (modulo ii 3))
                    (zero? (modulo ii 5)))
                (begin
                  (set! isum (+ isum ii))
                  (if (equal? debug-flag #t)
                      (begin
                        (display
                         (format
                          #f "debug ii=~a, isum=~a~%"
                          ii isum)))
                      )))
            ))
        isum
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-calc-sum-1 result-hash-table)
 (begin
   (let ((sub-name "test-calc-sum-1")
         (test-list
          (list
           (list 1 0) (list 2 0) (list 3 0)
           (list 4 3) (list 5 3) (list 6 8)
           (list 7 14) (list 8 14) (list 9 14)
           (list 10 23) (list 11 33) (list 12 33)
           (list 13 45) (list 14 45) (list 15 45)
           ))
         (debug-flag #f)
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-num (list-ref alist 0))
                  (shouldbe-num (list-ref alist 1)))
              (let ((result-num (calc-sum test-num debug-flag)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : num=~a : "
                        sub-name test-label-index test-num))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a, "
                        shouldbe-num result-num)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-num result-num)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "If we list all the natural "))
    (display
     (format #f "numbers below~%"))
    (display
     (format #f "10 that are multiples of 3 or "))
    (display
     (format #f "5, we get~%"))
    (display
     (format #f "3, 5, 6 and 9. The sum of these "))
    (display
     (format #f "multiples is 23.~%"))
    (newline)
    (display
     (format #f "Find the sum of all the multiples "))
    (display
     (format #f "of 3 or 5~%"))
    (display
     (format #f "below 1000.~%"))
    (display
     (format #f "see https://projecteuler.net/problem=1~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop max-num debug-flag)
  (begin
    (let ((sum (calc-sum max-num debug-flag)))
      (begin
        (display
         (ice-9-format:format
          #f "sum from 1 through ~:d is ~:d~%"
          (- max-num 1) sum))
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((max-num 10)
          (debug-flag #f))
      (begin
        (sub-main-loop max-num debug-flag)
        ))

    (let ((max-num 15)
          (debug-flag #f))
      (begin
        (sub-main-loop max-num debug-flag)
        ))

    (let ((max-num 1000)
          (debug-flag #f))
      (begin
        (sub-main-loop max-num debug-flag)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format
              #f "Project Euler 1 (version ~a)"
              version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((details-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string details-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)
          (force-output)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 9                                      ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated June 9, 2022                                 ###
;;;###                                                       ###
;;;###  updated March 4, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### srfi-11 for let-values
(use-modules ((srfi srfi-11)
              :renamer (symbol-prefix-proc 'srfi-11:)))

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (pythagorean-triplet aa bb)
  (begin
    (let ((a2 (* aa aa))
          (b2 (* bb bb)))
      (let ((a2b2sum (+ a2 b2)))
        (begin
          (srfi-11:let-values
           (((cc cc-remainder)
             (exact-integer-sqrt a2b2sum)))
           (begin
             (if (and
                  (zero? cc-remainder)
                  (> cc aa)
                  (> cc bb))
                 (begin
                   cc)
                 (begin
                   #f
                   ))
             ))
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-pythagorean-triplet-1 result-hash-table)
 (begin
   (let ((sub-name "test-pythagorean-triplet-1")
         (test-list
          (list
           (list 1 2 #f) (list 1 3 #f) (list 3 4 5)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-aa (list-ref alist 0))
                  (test-bb (list-ref alist 1))
                  (shouldbe-num (list-ref alist 2)))
              (let ((result-num
                     (pythagorean-triplet test-aa test-bb)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "test-aa=~a, test-bb=~a : "
                        test-aa test-bb))
                      (err-3
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe-num result-num)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-num result-num)
                     sub-name
                     (string-append
                      err-1 err-2 err-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (find-pythagorean-triplet ii jj sum-to-find)
  (begin
    (let ((tmpcc (pythagorean-triplet ii jj)))
      (begin
        (if (not (equal? tmpcc #f))
            (begin
              (let ((tmpsum (+ ii jj tmpcc)))
                (begin
                  (if (equal? tmpsum sum-to-find)
                      (begin
                        (list aa bb cc))
                      (begin
                        #f
                        ))
                  )))
            (begin
              #f
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "A Pythagorean triplet is a set of "))
    (display
     (format #f "three natural~%"))
    (display
     (format #f "numbers, a < b < c, for which, "))
    (display
     (format #f "a^2 + b^2 = c^2~%"))
    (display
     (format #f "For example, 3^2 + 4^2 = "))
    (display
     (format #f "9 + 16 = 25 = 5^2.~%"))
    (display
     (format #f "There exists exactly one Pythagorean "))
    (display
     (format #f "triplet for~%"))
    (display
     (format #f "which a + b + c = 1000. Find "))
    (display
     (format #f "the product~%"))
    (display
     (format #f "abc.~%"))
    (display
     (format #f "see https://projecteuler.net/problem=9~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop sum-to-find max-num debug-flag)
  (begin
    (let ((product 1))
      (begin
        (do ((ii 1 (+ ii 1)))
            ((>= ii max-num))
          (begin
            (do ((jj (+ ii 1) (+ jj 1)))
                ((> jj max-num))
              (begin
                (let ((tmpcc (pythagorean-triplet ii jj)))
                  (begin
                    (if (not (equal? tmpcc #f))
                        (begin
                          (let ((tmpsum (+ ii jj tmpcc)))
                            (begin
                              (if (equal? tmpsum sum-to-find)
                                  (begin
                                    (set! product (* ii jj tmpcc))
                                    ))
                              (if (or (equal? debug-flag #t)
                                      (equal? tmpsum sum-to-find))
                                  (begin
                                    (display
                                     (ice-9-format:format
                                      #f "  ~:d^2 + ~:d^2 = ~:d + ~:d = "
                                      ii jj (* ii ii) (* jj jj)))
                                    (display
                                     (ice-9-format:format
                                      #f "~:d = ~:d^2~%"
                                      (* tmpcc tmpcc) tmpcc))
                                    (display
                                     (ice-9-format:format
                                      #f "  sum : ~:d + ~:d + ~:d "
                                      ii jj tmpcc))
                                    (display
                                     (ice-9-format:format
                                      #f "= ~:d~a~%"
                                      tmpsum
                                      (if (equal? tmpsum sum-to-find)
                                          (begin
                                            "  **********")
                                          (begin
                                            ""))))
                                    (display
                                     (ice-9-format:format
                                      #f "  product : ~:d * ~:d * ~:d = "
                                      ii jj tmpcc))
                                    (display
                                     (ice-9-format:format
                                      #f "~:d~%"
                                      (* ii jj tmpcc)))
                                    ))
                              ))
                          ))
                    ))
                ))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((num-to-find 12)
          (max-num 12)
          (debug-flag #t))
      (begin
        (sub-main-loop num-to-find max-num debug-flag)
        ))

    (let ((num-to-find 1000)
          (max-num 1000)
          (debug-flag #f))
      (begin
        (sub-main-loop num-to-find max-num debug-flag)
        (newline)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 9 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

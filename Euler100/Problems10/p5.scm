#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 5                                      ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated June 9, 2022                                 ###
;;;###                                                       ###
;;;###  updated March 4, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### prime-module for is-prime? function
(use-modules ((prime-module)
              :renamer (symbol-prefix-proc 'prime-module:)))

;;;### timer-module for time-code function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define-syntax process-prime-factor-macro
  (syntax-rules ()
    ((process-prime-factor-macro
      local-divide-all-factors
      ll-num ii result-list)
     (begin
       (let ((partial-list
              (local-divide-all-factors ll-num ii)))
         (begin
           (set! ll-num (car partial-list))
           (set!
            result-list
            (append result-list (list (cdr partial-list))))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (to-prime-factor-list input-number)
  (define (local-divide-all-factors this-num this-factor)
    (begin
      (let ((ll-num this-num)
            (ncount 0))
        (begin
          (while
           (or (zero? (modulo ll-num this-factor))
               (< ll-num 1))
           (begin
             (set!
              ll-num
              (euclidean-quotient ll-num this-factor))
             (set! ncount (1+ ncount))
             ))
          (list ll-num this-factor ncount)
          ))
      ))
  (begin
    (cond
     ((<= input-number 1)
      (begin
        (list)
        ))
     (else
      (begin
        (let ((result-list (list))
              (constant-number input-number)
              (ll-num input-number)
              (ll-max (1+ (exact-integer-sqrt input-number))))
          (begin
            ;;; first take out all factors of 2
            (if (zero? (remainder ll-num 2))
                (begin
                  (process-prime-factor-macro
                   local-divide-all-factors
                   ll-num 2 result-list)

                  (if (prime-module:is-prime? ll-num)
                      (begin
                        (set!
                         result-list
                         (append result-list
                                 (list (list ll-num 1))))
                        (set! ll-num 1)
                        (set! ll-max 1)
                        ))
                  ))

            ;;; then take out all odd prime factors
            (do ((ii 3 (+ ii 2)))
                ((or (>= ii ll-max)
                     (<= ll-num 1)))
              (begin
                (if (zero? (modulo ll-num ii))
                    (begin
                      (if (prime-module:is-prime? ii)
                          (begin
                            (process-prime-factor-macro
                             local-divide-all-factors
                             ll-num ii result-list)

                            (let ((this-divisor
                                   (quotient constant-number ii)))
                              (begin
                                (if (and (> this-divisor ii)
                                         (equal?
                                          (memq this-divisor result-list)
                                          #f)
                                         (prime-module:is-prime?
                                          this-divisor))
                                    (begin
                                      (process-prime-factor-macro
                                       local-divide-all-factors
                                       ll-num this-divisor result-list)
                                      ))
                                ))
                            ))
                      ))
                ))

            (if (< (length result-list) 1)
                (begin
                  (list (list input-number 1)))
                (begin
                  result-list
                  ))
            ))
        )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-to-prime-factor-list-1 result-hash-table)
 (begin
   (let ((sub-name "test-to-prime-factor-list-1")
         (test-list
          (list
           (list 2 (list (list 2 1)))
           (list 3 (list (list 3 1)))
           (list 4 (list (list 2 2)))
           (list 5 (list (list 5 1)))
           (list 6 (list (list 2 1)
                         (list 3 1)))
           (list 7 (list (list 7 1)))
           (list 8 (list (list 2 3)))
           (list 9 (list (list 3 2)))
           (list 10 (list (list 2 1)
                          (list 5 1)))
           (list 11 (list (list 11 1)))
           (list 12 (list (list 2 2)
                          (list 3 1)))
           (list 13 (list (list 13 1)))
           (list 14 (list (list 2 1)
                          (list 7 1)))
           (list 15 (list (list 3 1)
                          (list 5 1)))
           (list 16 (list (list 2 4)))
           (list 17 (list (list 17 1)))
           (list 18 (list (list 2 1)
                          (list 3 2)))
           (list 19 (list (list 19 1)))
           (list 20 (list (list 2 2)
                          (list 5 1)))
           (list 21 (list (list 3 1)
                          (list 7 1)))
           (list 22 (list (list 2 1)
                          (list 11 1)))
           (list 100 (list (list 2 2)
                           (list 5 2)))
           (list 200 (list (list 2 3)
                           (list 5 2)))
           (list 400 (list (list 2 4)
                           (list 5 2)))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-num (list-ref alist 0))
                  (shouldbe-list (list-ref alist 1)))
              (let ((result-list (to-prime-factor-list test-num)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : num=~a : "
                        sub-name test-label-index test-num))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a, "
                        shouldbe-list result-list)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-list result-list)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (integer-power this-number this-exponent)
  (begin
    (cond
     ((= this-exponent 0)
      (begin
        1
        ))
     ((= this-exponent 1)
      (begin
        this-number
        ))
     ((< this-exponent 0)
      (begin
        -1
        ))
     (else
      (begin
        (let ((result-num this-number)
              (max-iter (- this-exponent 1)))
          (begin
            (do ((ii 0 (+ ii 1)))
                ((>= ii max-iter))
              (begin
                (set! result-num (* result-num this-number))
                ))
            result-num
            ))
        )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-integer-power-1 result-hash-table)
 (begin
   (let ((sub-name "test-integer-power-1")
         (test-list
          (list
           (list 10 0 1) (list 11 0 1) (list 12 0 1)
           (list 10 1 10) (list 11 1 11) (list 12 1 12)
           (list 10 2 100) (list 11 2 121) (list 12 2 144)
           (list 2 2 4) (list 2 3 8) (list 2 4 16)
           (list 2 5 32) (list 2 6 64) (list 2 7 128)
           (list 2 8 256) (list 2 9 512)
           (list 2 10 1024)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-num (list-ref alist 0))
                  (test-exp (list-ref alist 1))
                  (shouldbe-num (list-ref alist 2)))
              (let ((result-num (integer-power test-num test-exp)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : num=~a : "
                        sub-name test-label-index test-num))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a, "
                        shouldbe-num result-num)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-num result-num)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (smallest-divisible max-num)
  (begin
    (let ((pfactor-htable (make-hash-table 100)))
      (begin
        (do ((ii 2 (1+ ii)))
            ((> ii max-num))
          (begin
            (let ((plist (to-prime-factor-list ii)))
              (begin
                (for-each
                 (lambda (alist)
                   (begin
                     (let ((pfactor (list-ref alist 0))
                           (pexp (list-ref alist 1)))
                       (let ((current-exp
                              (hash-ref
                               pfactor-htable pfactor 0)))
                         (begin
                           (if (> pexp current-exp)
                               (begin
                                 (hash-set!
                                  pfactor-htable pfactor pexp)
                                 ))
                           )))
                     )) plist)
                ))
            ))

        (let ((result 1))
          (begin
            (hash-for-each
             (lambda (key value)
               (begin
                 (let ((this-prod
                        (integer-power key value)))
                   (begin
                     (set! result (* result this-prod))
                     ))
                 )) pfactor-htable)

            result
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-smallest-divisible-1 result-hash-table)
 (begin
   (let ((sub-name "test-smallest-divisible-1")
         (test-list
          (list
           (list 10 2520)
           (list 11 27720)
           (list 12 27720)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-num (list-ref alist 0))
                  (shouldbe-num (list-ref alist 1)))
              (let ((result-num
                     (smallest-divisible test-num)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : num=~a : "
                        sub-name test-label-index test-num))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a, "
                        shouldbe-num result-num)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-num result-num)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "2520 is the smallest number that "))
    (display
     (format #f "can be divided~%"))
    (display
     (format #f "by each of the numbers from 1 to "))
    (display
     (format #f "10 without~%"))
    (display
     (format #f "any remainder. What is the smallest "))
    (display
     (format #f "positive number~%"))
    (display
     (format #f "that is evenly divisible by all of "))
    (display
     (format #f "the numbers~%"))
    (display
     (format #f "from 1 to 20?~%"))
    (newline)
    (display
     (format #f "notes: this program can be simply "))
    (display
     (format #f "written by~%"))
    (display
     (format #f "breaking down each number from "))
    (display
     (format #f "1 through 20~%"))
    (display
     (format #f "into it's prime constituents. Then "))
    (display
     (format #f "one looks~%"))
    (display
     (format #f "at the largest number of 2's that "))
    (display
     (format #f "occur between~%"))
    (display
     (format #f "1 and 20 and stores that result. Repeat "))
    (display
     (format #f "for the~%"))
    (display
     (format #f "largest number of 3's, 5's, 7's, "))
    (display
     (format #f "11's, 13's,~%"))
    (display
     (format #f "17's, 19's. The solution is the "))
    (display
     (format #f "product of~%"))
    (display
     (format #f "the largest number of primes needed "))
    (display
     (format #f "between 1 and 20.~%"))
    (display
     (format #f "see https://projecteuler.net/problem=5~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop max-num)
  (begin
    (let ((small (smallest-divisible max-num)))
      (begin
        (display
         (ice-9-format:format
          #f "smallest number that's divisble evenly, "))
        (display
         (ice-9-format:format
          #f "up to ~:d is ~:d~%"
          max-num small))
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((max-num 10))
      (begin
        (sub-main-loop max-num)
        (newline)
        ))

    (let ((max-num 20))
      (begin
        (sub-main-loop max-num)
        (newline)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 5 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (force-output)
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 3                                      ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated June 9, 2022                                 ###
;;;###                                                       ###
;;;###  updated March 4, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### prime-module for is-prime? functions
(use-modules ((prime-module)
              :renamer (symbol-prefix-proc 'prime-module:)))

;;;### timer-module for time-code function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (largest-prime-factor nn)
  (define (local-check-both-divisors nn dd)
    (begin
      (let ((result -1))
        (begin
          (if (zero? (modulo nn dd))
              (begin
                (if (and (prime-module:is-prime? dd)
                         (> dd result))
                    (begin
                      (set! result dd)
                      ))

                (let ((div (quotient nn dd)))
                  (begin
                    (if (and (> div result)
                             (prime-module:is-prime? div))
                        (begin
                          (set! result div)
                          ))
                    ))
                ))
          result
          ))
      ))
  (begin
    (let ((max-divisor (1+ (exact-integer-sqrt nn)))
          (largest-result -1)
          (continue-loop-flag #t))
      (begin
        (let ((this-result
               (local-check-both-divisors nn 2)))
          (begin
            (if (> this-result largest-result)
                (begin
                  (set! largest-result this-result)
                  ))
            ))

        (do ((ii 3 (+ ii 2)))
            ((> ii max-divisor))
          (begin
            (let ((this-result
                   (local-check-both-divisors nn ii)))
              (begin
                (if (> this-result largest-result)
                    (begin
                      (set! largest-result this-result)
                      ))
                ))
            ))

        (if (> largest-result 0)
            (begin
              largest-result)
            (begin
              nn
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-largest-prime-factor-1 result-hash-table)
 (begin
   (let ((sub-name "test-largest-prime-factor-1")
         (test-list
          (list
           (list 10 5) (list 11 11) (list 12 3) (list 13 13)
           (list 14 7) (list 15 5) (list 16 2) (list 17 17)
           (list 18 3) (list 19 19) (list 20 5) (list 21 7)
           (list 22 11) (list 23 23) (list 24 3) (list 25 5)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((test-num (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result
                     (largest-prime-factor test-num)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : num=~a, "
                        sub-name test-label-index test-num))
                      (err-2
                       (format
                        #f "largest prime factor shouldbe=~a, result=~a~%"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "The prime factors of 13195 are "))
    (display
     (format #f "5, 7, 13~%"))
    (display
     (format #f "and 29. What is the largest prime "))
    (display
     (format #f "factor of the number "))
    (display
     (format #f "600851475143 ?~%"))
    (newline)
    (display
     (format #f "see also https://community.schemewiki.org/?sicp-ex-1.22~%"))
    (display
     (format #f "see https://projecteuler.net/problem=3~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop this-num)
  (begin
    (let ((lpf (largest-prime-factor this-num)))
      (begin
        (display
         (ice-9-format:format
          #f "largest prime factor of ~:d = ~:d~%"
          this-num lpf))
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (do ((ii 10 (+ ii 1)))
        ((> ii 20))
      (begin
        (sub-main-loop ii)
        ))

    (let ((number 600851475143))
      (begin
        (sub-main-loop number)
        (newline)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 3 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (load "prime-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

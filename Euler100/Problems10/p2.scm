#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 2                                      ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated June 9, 2022                                 ###
;;;###                                                       ###
;;;###  updated March 3, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (even-fib-sum max-num)
  (begin
    (let ((f0 0)
          (f1 1)
          (isum 0))
      (begin
        (do ((f2 1 (+ f0 f1)))
            ((> f2 max-num))
          (begin
            (if (zero? (modulo f2 2))
                (begin
                  (set! isum (+ isum f2))
                  ))

            (set! f0 f1)
            (set! f1 f2)
            ))
        isum
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-even-fib-sum-1 result-hash-table)
 (begin
   (let ((sub-name "test-even-fib-sum-1")
         (test-list
          (list
           (list 3 2) (list 5 2) (list 6 2)
           (list 8 10) (list 12 10) (list 20 10)
           (list 30 10) (list 33 10) (list 34 44)
           (list 35 44) (list 40 44)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((test-max (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result (even-fib-sum test-max)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : max=~a, "
                        sub-name test-label-index test-max))
                      (err-2
                       (format
                        #f "fib-sum shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Each new term in the Fibonacci "))
    (display
     (format #f "sequence is~%"))
    (display
     (format #f "generated by adding the previous "))
    (display
     (format #f "two terms.~%"))
    (display
     (format #f "By starting with 1 and 2, the first "))
    (display
     (format #f "10 terms~%"))
    (display
     (format #f "will be:~%"))
    (newline)
    (display
     (format #f "1, 2, 3, 5, 8, 13, 21, "))
    (display
     (format #f "34, 55, 89, ...~%"))
    (newline)
    (display
     (format #f "By considering the terms in the "))
    (display
     (format #f "Fibonacci~%"))
    (display
     (format #f "sequence whose values do not "))
    (display
     (format #f "exceed four~%"))
    (display
     (format #f "million, find the sum of the "))
    (display
     (format #f "even-valued terms.~%"))
    (newline)
    (display
     (format #f "see also https://en.wikipedia.org/wiki/Fibonacci_number~%"))
    (display
     (format #f "see https://projecteuler.net/problem=2~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop max-num)
  (begin
    (let ((sum (even-fib-sum max-num)))
      (begin
        (display
         (ice-9-format:format
          #f "sum of even fibonacci numbers from "))
        (display
         (ice-9-format:format
          #f "1 through ~:d is ~:d~%"
          (- max-num 1) sum))
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((max-num 10))
      (begin
        (sub-main-loop max-num)
        ))

    (newline)
    (let ((max-num 50))
      (begin
        (sub-main-loop max-num)
        ))

    (newline)
    (let ((max-num 4000000))
      (begin
        (sub-main-loop max-num)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 2 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

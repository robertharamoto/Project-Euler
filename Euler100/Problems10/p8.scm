#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 8                                      ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated June 9, 2022                                 ###
;;;###                                                       ###
;;;###  updated March 4, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### digits-module for split-digits-list function
(use-modules ((digits-module)
              :renamer (symbol-prefix-proc 'digits-module:)))

;;;### timer-module for time-code function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (largest-product digit-list nconsec)
  (begin
    (cond
     ((or
       (not (list? digit-list))
       (< (length digit-list) nconsec))
      (begin
        -1
        ))
     (else
      (begin
        (let ((llength (length digit-list)))
          (let ((end-index (- llength nconsec))
                (max-product 0)
                (max-index 0)
                (max-list (list)))
            (begin
              (do ((ii 0 (+ ii 1)))
                  ((> ii end-index))
                (begin
                  (let ((curr-prod 1)
                        (curr-list (list)))
                    (begin
                      (do ((jj 0 (+ jj 1)))
                          ((>= jj nconsec))
                        (begin
                          (let ((this-index (+ ii jj)))
                            (let ((this-digit
                                   (list-ref
                                    digit-list this-index)))
                              (begin
                                (set!
                                 curr-prod
                                 (* curr-prod this-digit))
                                (set!
                                 curr-list
                                 (cons this-digit curr-list))
                                )))
                          ))
                      (if (> curr-prod max-product)
                          (begin
                            (set! max-product curr-prod)
                            (set! max-index ii)
                            (set! max-list (reverse curr-list))
                            ))
                      ))
                  ))
              (list
               max-product max-index max-list)
              )))
        )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-largest-product-1 result-hash-table)
 (begin
   (let ((sub-name "test-largest-product-1")
         (test-list
          (list
           (list 1234 2 (list 12 2 (list 3 4)))
           (list 12345 2 (list 20 3 (list 4 5)))
           (list 54321 2 (list 20 0 (list 5 4)))
           (list 1234 3 (list 24 1 (list 2 3 4)))
           (list 12345 3 (list 60 2 (list 3 4 5)))
           (list 54321 3 (list 60 0 (list 5 4 3)))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((test-num (list-ref this-list 0))
                  (test-nconsec (list-ref this-list 1))
                  (shouldbe (list-ref this-list 2)))
              (let ((test-dlist
                     (digits-module:split-digits-list test-num)))
                (let ((result (largest-product test-dlist test-nconsec)))
                  (let ((err-1
                         (format
                          #f "~a :: (~a) error : num=~a : "
                          sub-name test-label-index test-num))
                        (err-2
                         (format
                          #f "shouldbe=~a, result=~a, "
                          shouldbe result)))
                    (begin
                      (unittest2:assert?
                       (equal? shouldbe result)
                       sub-name
                       (string-append err-1 err-2)
                       result-hash-table)
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Find the greatest product of five "))
    (display
     (format #f "consecutive~%"))
    (display
     (format #f "digits in the 1000-digit number.~%"))
    (display
     (format #f "7316717653133062491922511967442657474235534919493496983520312774506326239578318016984801869478851843858615607891129494954595017379583319528532088055111254069874715852386305071569329096329522744304355766896648950445244523161731856403098711121722383113622298934233803081353362766142828064444866452387493035890729629049156044077239071381051585930796086670172427121883998797908792274921901699720888093776657273330010533678812202354218097512545405947522435258490771167055601360483958644670632441572215539753697817977846174064955149290862569321978468622482839722413756570560574902614079729686524145351004748216637048440319989000889524345065854122758866688116427171479924442928230863465674813919123162824586178664583591245665294765456828489128831426076900422421902267105562632111110937054421750694165896040807198403850962455444362981230987879927244284909188845801561660979191338754992005240636899125607176060588611646710940507754100225698315520005593572972571636269561882670428252483600823257530420752963450~%"))
    (display
     (format #f "see https://projecteuler.net/problem=8~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop num nconsec)
  (begin
    (let ((dlist
           (digits-module:split-digits-list num)))
      (let ((result-list (largest-product dlist nconsec)))
        (let ((prod (list-ref result-list 0))
              (index (list-ref result-list 1))
              (factors-list (list-ref result-list 2)))
          (let ((fstring
                 (string-join
                  (map number->string factors-list) " * ")))
            (begin
              (display
               (ice-9-format:format
                #f "the largest product of ~:d consecutive "
                nconsec))
              (display
               (ice-9-format:format
                #f "digits of ~:d~%"
                num))
              (display
               (ice-9-format:format
                #f "is ~a = ~:d  (starting from index ~:d)~%"
                fstring prod index))
              (force-output)
              )))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((num 1234567)
          (nconsec 3))
      (begin
        (sub-main-loop num nconsec)
        (newline)
        ))

    (let ((num 7316717653133062491922511967442657474235534919493496983520312774506326239578318016984801869478851843858615607891129494954595017379583319528532088055111254069874715852386305071569329096329522744304355766896648950445244523161731856403098711121722383113622298934233803081353362766142828064444866452387493035890729629049156044077239071381051585930796086670172427121883998797908792274921901699720888093776657273330010533678812202354218097512545405947522435258490771167055601360483958644670632441572215539753697817977846174064955149290862569321978468622482839722413756570560574902614079729686524145351004748216637048440319989000889524345065854122758866688116427171479924442928230863465674813919123162824586178664583591245665294765456828489128831426076900422421902267105562632111110937054421750694165896040807198403850962455444362981230987879927244284909188845801561660979191338754992005240636899125607176060588611646710940507754100225698315520005593572972571636269561882670428252483600823257530420752963450)
          (nconsec 5))
      (begin
        (sub-main-loop num nconsec)
        (newline)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 8 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (load "digits-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

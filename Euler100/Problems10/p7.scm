#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 7                                      ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated June 9, 2022                                 ###
;;;###                                                       ###
;;;###  updated March 4, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### prime-module for is-prime? function
(use-modules ((prime-module)
              :renamer (symbol-prefix-proc 'prime-module:)))

;;;### timer-module for time-code function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (find-nth-prime nn max-prime)
  (begin
    (cond
     ((<= nn 0)
      (begin
        -1
        ))
     ((= nn 1)
      (begin
        2
        ))
     ((= nn 2)
      (begin
        3
        ))
     (else
      (begin
        (let ((nth-prime-index 2)
              (nth-prime-value 3)
              (found-flag #f))
          (begin
            (do ((ii 5 (+ ii 2)))
                ((or
                  (> ii max-prime)
                  (>= nth-prime-index nn)
                  (equal? found-flag #t)))
              (begin
                (if (prime-module:is-prime? ii)
                    (begin
                      (set! nth-prime-index (1+ nth-prime-index))
                      (if (>= nth-prime-index nn)
                          (begin
                            (set! nth-prime-value ii)
                            (set! found-flag #t)
                            ))
                      ))
                ))

            (if (= nth-prime-index nn)
                (begin
                  nth-prime-value)
                (begin
                  -1
                  ))
            ))
        )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-find-nth-prime-1 result-hash-table)
 (begin
   (let ((sub-name "test-find-nth-prime-1")
         (test-list
          (list
           (list 0 100 -1) (list 1 100 2) (list 2 100 3)
           (list 3 100 5) (list 4 100 7) (list 5 100 11)
           (list 6 100 13) (list 7 100 17) (list 8 100 19)
           (list 9 100 23) (list 10 100 29) (list 11 100 31)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((test-num (list-ref this-list 0))
                  (test-max (list-ref this-list 1))
                  (shouldbe (list-ref this-list 2)))
              (let ((result (find-nth-prime test-num test-max)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : num=~a : "
                        sub-name test-label-index test-num))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a, "
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "By listing the first six prime "))
    (display
     (format #f "numbers:~%"))
    (display
     (format #f "2, 3, 5, 7, 11, and 13~%"))
    (display
     (format #f "we can see that the 6th prime "))
    (display
     (format #f "is 13.~%"))
    (display
     (format #f "What is the 10,001 st prime number?~%"))
    (display
     (format #f "see https://projecteuler.net/problem=7~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop nth-index max-prime)
  (begin
    (let ((nth-prime-value
           (find-nth-prime nth-index max-prime)))
      (begin
        (display
         (ice-9-format:format
          #f "the ~:d prime is ~:d~%"
          nth-index nth-prime-value))
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (do ((ii 1 (+ ii 1)))
        ((> ii 20))
      (begin
        (let ((max-prime 100000))
          (begin
            (sub-main-loop ii max-prime)
            ))
        ))

    (let ((nth-index 10001)
          (max-prime 1000000))
      (begin
        (sub-main-loop nth-index max-prime)
        (newline)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 7 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (load "prime-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (force-output)
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

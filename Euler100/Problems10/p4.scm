#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 4                                      ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated June 9, 2022                                 ###
;;;###                                                       ###
;;;###  updated March 4, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### digits-module for split-digits-list function
(use-modules ((digits-module)
              :renamer (symbol-prefix-proc 'digits-module:)))

;;;### timer-module for time-code function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (palindrome? nn)
  (begin
    (cond
     ((< nn 0)
      (begin
        #f
        ))
     ((< nn 10)
      (begin
        #t
        ))
     (else
      (begin
        (let ((plist
               (digits-module:split-digits-list nn)))
          (let ((rlist (reverse plist)))
            (begin
              (equal? plist rlist)
              )))
        )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-palindrome-1 result-hash-table)
 (begin
   (let ((sub-name "test-palindrome-1")
         (test-list
          (list
           (list 10 #f) (list 11 #t) (list 12 #f) (list 13 #f)
           (list 20 #f) (list 21 #f) (list 22 #t) (list 23 #f)
           (list 313 #t) (list 323 #t) (list 333 #t) (list 334 #f)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((test-num (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result (palindrome? test-num)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : num=~a : "
                        sub-name test-label-index test-num))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a, "
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "A palindromic number reads the same "))
    (display
     (format #f "both ways.~%"))
    (display
     (format #f "The largest palindrome made from the "))
    (display
     (format #f "product of~%"))
    (display
     (format #f "two 2-digit numbers is 9009 = 91 x 99.~%"))
    (newline)
    (display
     (format #f "Find the largest palindrome made from "))
    (display
     (format #f "the product of~%"))
    (display
     (format #f "two 3-digit numbers.~%"))
    (display
     (format #f "see https://projecteuler.net/problem=4~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop start-num end-num)
  (begin
    (let ((max-palindrome 0)
          (max-ii 0)
          (max-jj 0))
      (begin
        (do ((ii start-num (+ ii 1)))
            ((>= ii end-num))
          (begin
            (do ((jj ii (+ jj 1)))
                ((>= jj end-num))
              (begin
                (let ((prod (* ii jj)))
                  (begin
                    (if (and
                         (palindrome? prod)
                         (> prod max-palindrome))
                        (begin
                          (set! max-palindrome prod)
                          (set! max-ii ii)
                          (set! max-jj jj)
                          ))
                    ))
                ))
            ))

        (if (> max-palindrome 0)
            (begin
              (display
               (ice-9-format:format
                #f "largest palindrome found: ~:d * ~:d = ~:d, "
                max-ii max-jj max-palindrome))
              (display
               (ice-9-format:format
                #f "(between ~:d and ~:d)~%"
                start-num end-num)))
            (begin
              (display
               (ice-9-format:format
                #f "no palindromes found between ~:d and ~:d~%"
                start-num end-num))
              ))
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((start-num 10)
          (end-num 100))
      (begin
        (sub-main-loop start-num end-num)
        ))

    (let ((start-num 100)
          (end-num 1000))
      (begin
        (sub-main-loop start-num end-num)
        (newline)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 4 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (load "digits-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (force-output)
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 6                                      ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated June 9, 2022                                 ###
;;;###                                                       ###
;;;###  updated March 4, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (sum-of-squares max-nn)
  (begin
    (cond
     ((<= max-nn 0)
      (begin
        -1
        ))
     (else
      (begin
        (let ((isum 0))
          (begin
            (do ((ii 1 (+ ii 1)))
                ((> ii max-nn))
              (begin
                (set! isum (+ isum (* ii ii)))
                ))
            isum
            ))
        )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-sum-of-squares-1 result-hash-table)
 (begin
   (let ((sub-name "test-sum-of-squares-1")
         (test-list
          (list
           (list 0 -1) (list 1 1) (list 2 5) (list 3 14)
           (list 4 30) (list 5 55) (list 6 91)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((test-num (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result (sum-of-squares test-num)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : num=~a : "
                        sub-name test-label-index test-num))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a, "
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (sum-1-to-n-squared max-nn)
  (begin
    (cond
     ((<= max-nn 0)
      (begin
        -1
        ))
     (else
      (begin
        (let ((sum
               (euclidean-quotient
                (* (+ max-nn 1) max-nn) 2)))
          (let ((sum2 (* sum sum)))
            (begin
              sum2
              )))
        )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-sum-1-to-n-squared-1 result-hash-table)
 (begin
   (let ((sub-name "test-sum-1-to-n-squared-1")
         (test-list
          (list
           (list 1 1) (list 2 9) (list 3 36)
           (list 4 100) (list 5 225)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-num (list-ref alist 0))
                  (shouldbe-num (list-ref alist 1)))
              (let ((result-num
                     (sum-1-to-n-squared test-num)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : num=~a : "
                        sub-name test-label-index test-num))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a, "
                        shouldbe-num result-num)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-num result-num)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (squares-difference max-nn)
  (begin
    (let ((ssq (sum-of-squares max-nn))
          (ssum (sum-1-to-n-squared max-nn)))
      (let ((diff (- ssum ssq)))
        (begin
          diff
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-squares-difference-1 result-hash-table)
 (begin
   (let ((sub-name "test-squares-difference-1")
         (test-list
          (list
           (list 10 2640)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-num (list-ref alist 0))
                  (shouldbe-num (list-ref alist 1)))
              (let ((result-num (squares-difference test-num)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : num=~a : "
                        sub-name test-label-index test-num))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a, "
                        shouldbe-num result-num)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-num result-num)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "The sum of the squares of the first "))
    (display
     (format #f "ten natural~%"))
    (display
     (format #f "numbers is,~%"))
    (display
     (format #f "1^2 + 2^2 + ... + 10^2 = 385~%"))
    (display
     (format #f "The square of the sum of the first "))
    (display
     (format #f "ten natural~%"))
    (display
     (format #f "numbers is,~%"))
    (display
     (format #f "(1 + 2 + ... + 10)^2 = 55^2 = 3025~%"))
    (display
     (format #f "Hence the difference between the "))
    (display
     (format #f "sum of the~%"))
    (display
     (format #f "squares of the first ten natural "))
    (display
     (format #f "numbers and the~%"))
    (display
     (format #f "square of the sum is "))
    (display
     (format #f "3025 - 385 = 2640.~%"))
    (display
     (format #f "Find the difference between the "))
    (display
     (format #f "sum of the~%"))
    (display
     (format #f "squares of the first one hundred "))
    (display
     (format #f "natural~%"))
    (display
     (format #f "numbers and the square of the sum.~%"))
    (display
     (format #f "see https://projecteuler.net/problem=6~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop max-num)
  (begin
    (let ((diff (squares-difference max-num))
          (sum-sq (sum-1-to-n-squared max-num))
          (sq-sum (sum-of-squares max-num)))
      (begin
        (display
         (ice-9-format:format
          #f "difference between squared sum "))
        (display
         (ice-9-format:format
          #f "and sum of squares~%"))
        (display
         (ice-9-format:format
          #f "from 1 to ~:d is ~:d - ~:d = ~:d~%"
          max-num sum-sq sq-sum diff))
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((max-num 10))
      (begin
        (sub-main-loop max-num)
        (newline)
        ))

    (let ((max-num 100))
      (begin
        (sub-main-loop max-num)
        (newline)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 6 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (force-output)
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

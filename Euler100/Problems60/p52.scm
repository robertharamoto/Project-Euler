#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 52                                     ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated June 16, 2022                                ###
;;;###                                                       ###
;;;###  updated March 5, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### digits-module for split-digits-list function
(use-modules ((digits-module)
              :renamer (symbol-prefix-proc 'digits-module:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (lists-have-same-digits? ll1 ll2)
  (begin
    (let ((sl1 (sort ll1 <))
          (sl2 (sort ll2 <)))
      (begin
        (equal? sl1 sl2)
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-lists-have-same-digits-1 result-hash-table)
 (begin
   (let ((sub-name "test-lists-have-same-digits-1")
         (test-list
          (list
           (list (list 1 3) (list 3 1) #t)
           (list (list 1 2 3) (list 3 1 2) #t)
           (list (list 1 2 3 4) (list 4 1 3 2) #t)
           (list (list 13 23 43 53 73 83)
                 (list 83 23 43 53 73 13) #t)
           (list (list 13 23 43 53 73 83)
                 (list 13 23 44 53 73 83) #f)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((test-l1 (list-ref this-list 0))
                  (test-l2 (list-ref this-list 1))
                  (shouldbe (list-ref this-list 2)))
              (let ((result
                     (lists-have-same-digits? test-l1 test-l2)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "test-l1=~a, test-l2=~a, "
                        test-l1 test-l2))
                      (err-3
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append
                      err-1 err-2 err-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (+ test-label-index 1))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (list-to-set-string llist)
  (begin
    (let ((stmp
           (string-join
            (map
             (lambda (num)
               (begin
                 (ice-9-format:format #f "~:d" num)
                 )) llist)
            ", ")))
      (begin
        (string-append "{ " stmp " }")
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-list-to-set-string-1 result-hash-table)
 (begin
   (let ((sub-name "test-list-to-set-string-1")
         (test-list
          (list
           (list (list 1) "{ 1 }")
           (list (list 1 2) "{ 1, 2 }")
           (list (list 1 2 3) "{ 1, 2, 3 }")
           (list (list 4 5 6 7) "{ 4, 5, 6, 7 }")
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((input-list (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result (list-to-set-string input-list)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : input-list=~a, "
                        sub-name test-label-index input-list))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (+ test-label-index 1))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop start-num max-num)
  (begin
    (let ((smallest-list (list))
          (smallest-integer max-num)
          (break-flag #f)
          (counter 0))
      (begin
        (do ((ii 1 (+ ii 1)))
            ((or (> ii max-num)
                 (equal? break-flag #t)))
          (begin
            (let ((dlist1
                   (digits-module:split-digits-list ii))
                  (loop-continue-flag #t)
                  (tmp-num ii)
                  (local-list (list ii)))
              (begin
                (do ((jj 0 (1+ jj)))
                    ((or (>= jj 5)
                         (equal? loop-continue-flag #f)))
                  (begin
                    (let ((next-tmp-num (+ tmp-num ii)))
                      (let ((dlist2
                             (digits-module:split-digits-list next-tmp-num)))
                        (begin
                          (if (lists-have-same-digits? dlist1 dlist2)
                              (begin
                                (set!
                                 local-list
                                 (cons next-tmp-num local-list)))
                              (begin
                                (set! loop-continue-flag #f)
                                (set! local-list (list))
                                ))

                          (set! tmp-num next-tmp-num)
                          )))
                    ))

                (if (equal? loop-continue-flag #t)
                    (begin
                      (set! smallest-integer ii)
                      (set! smallest-list local-list)
                      (set! break-flag #t)
                      ))
                ))
            ))

        (if (equal? break-flag #f)
            (begin
              (display
               (ice-9-format:format
                #f "no sequences found for numbers "))
              (display
               (ice-9-format:format
                #f "less than ~:d~%" max-num)))
            (begin
              (display
               (ice-9-format:format
                #f "~:d is the smallest positive integer such that~%"
                smallest-integer))
              (display
               (ice-9-format:format
                #f "2x, 3x, 4x, 5, 6x contains the same digits~%"
                smallest-integer))
              (display
               (ice-9-format:format
                #f "digit list = ~a~%"
                (list-to-set-string (sort smallest-list <))))
              (force-output)
              ))
        break-flag
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "It can be seen that the number, 125874, "))
    (display
     (format #f "and its double,~%"))
    (display
     (format #f "251748, contain exactly the same digits, "))
    (display
     (format #f "but in a~%"))
    (display
     (format #f "different order.~%"))
    (newline)
    (display
     (format #f "Find the smallest positive integer, x, "))
    (display
     (format #f "such that 2x, 3x,~%"))
    (display
     (format #f "4x, 5x, and 6x, contain the same digits.~%"))
    (display
     (format #f "see https://projecteuler.net/problem=52~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((test-list
           (list
            (list 1 1000000)
            (list 1000000 10000000)
            (list 10000000 100000000)
            (list 100000000 1000000000)
            ))
          (loop-continue-flag #t))
      (let ((ntests (length test-list)))
        (begin
          (do ((ii 0 (1+ ii)))
              ((or (>= ii ntests)
                   (equal? loop-continue-flag #f)))
            (begin
              (newline)
              (let ((alist (list-ref test-list ii)))
                (let ((start-num (list-ref alist 0))
                      (end-num (list-ref alist 1)))
                  (begin
                    (let ((found-flag
                           (sub-main-loop
                            start-num end-num)))
                      (begin
                        (if (equal? found-flag #t)
                            (begin
                              (set! loop-continue-flag #f)
                              ))
                        ))
                    (force-output)
                    )))
              ))
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 52 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (load "digits-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

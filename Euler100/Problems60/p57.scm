#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 57                                     ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated June 16, 2022                                ###
;;;###                                                       ###
;;;###  updated March 5, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### digits-module for split-digits-list function
(use-modules ((digits-module)
              :renamer (symbol-prefix-proc 'digits-module:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (calc-nth-iter nn)
  (begin
    (let ((rvalue 1)
          (aa 0))
      (begin
        (do ((ii 0 (+ ii 1)))
            ((> ii nn))
          (begin
            (set! aa (/ 1 (+ 2 aa)))
            ))
        (let ((result (+ 1 aa)))
          (let ((numer (numerator result))
                (denom (denominator result)))
            (begin
              (list numer denom)
              )))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (p57-assert-lists-equal
         shouldbe-list result-list
         sub-name
         err-start
         result-hash-table)
  (begin
    (let ((shouldbe-length (length shouldbe-list))
          (result-length (length result-list))
          (err-1
           (format
            #f ", shouldbe-list=~a, result-list=~a"
            shouldbe-list result-list)))
      (let ((err-2
             (format
              #f ", lengths not equal, shouldbe=~a, result=~a"
              shouldbe-length result-length)))
        (begin
          (unittest2:assert?
           (equal? shouldbe-length result-length)
           sub-name
           (string-append err-start err-1 err-2)
           result-hash-table)

          (if (> shouldbe-length 0)
              (begin
                (for-each
                 (lambda (s-elem)
                   (begin
                     (let ((mflag
                            (member s-elem result-list))
                           (err-3
                            (format
                             #f ", missing element ~a"
                             s-elem)))
                       (begin
                         (unittest2:assert?
                          (not (equal? mflag #f))
                          sub-name
                          (string-append
                           err-start err-1 err-3)
                          result-hash-table)
                         ))
                     )) shouldbe-list)
                ))
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-calc-nth-iter-1 result-hash-table)
 (begin
   (let ((sub-name "test-calc-nth-iter-1")
         (test-list
          (list
           (list 0 (list 3 2))
           (list 1 (list 7 5))
           (list 2 (list 17 12))
           (list 3 (list 41 29))
           (list 4 (list 99 70))
           (list 5 (list 239 169))
           (list 6 (list 577 408))
           (list 7 (list 1393 985))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-num (list-ref alist 0))
                  (shouldbe-list (list-ref alist 1)))
              (let ((result-list
                     (calc-nth-iter test-num)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : test-num=~a"
                        sub-name test-label-index test-num)))
                  (begin
                    (p57-assert-lists-equal
                     shouldbe-list result-list
                     sub-name
                     err-1
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop start-num max-num debug-flag)
  (begin
    (let ((counter 0))
      (begin
        (do ((nn start-num (+ nn 1)))
            ((> nn max-num))
          (begin
            (let ((this-list (calc-nth-iter nn)))
              (let ((numer (list-ref this-list 0))
                    (denom (list-ref this-list 1)))
                (let ((numer-ndigits
                       (length
                        (digits-module:split-digits-list numer)))
                      (denom-ndigits
                       (length
                        (digits-module:split-digits-list denom))))
                  (begin
                    (if (> numer-ndigits denom-ndigits)
                        (begin
                          (set! counter (1+ counter))
                          (if (equal? debug-flag #t)
                              (begin
                                (display
                                 (ice-9-format:format
                                  #f "~:dth expansion has ~:d digits "
                                  (+ nn 1) numer-ndigits))
                                (display
                                 (ice-9-format:format
                                  #f "in the numerator and ~:d digits~%"
                                  denom-ndigits))
                                (display
                                 (ice-9-format:format
                                  #f "in the denominator, ~:d / ~:d~%"
                                  numer denom))
                                ))
                          ))
                    ))
                ))
            ))

        (newline)
        (display
         (ice-9-format:format
          #f "The number of expansions that contain a numerator~%"))
        (display
         (ice-9-format:format
          #f "with more digits than the denominator = ~:d~%"
          counter))
        (display
         (ice-9-format:format
          #f "where the expansions range between ~:d and ~:d~%"
          start-num max-num))
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "It is possible to show that the square "))
    (display
     (format #f "root of two can~%"))
    (display
     (format #f "be expressed as an infinite continued "))
    (display
     (format #f "fraction.~%"))
    (newline)
    (display
     (format #f " sqrt(2) = 1 + 1/(2 + 1/(2 + "))
    (display
     (format #f "1/(2 + ... ))) = 1.414213...~%"))
    (newline)
    (display
     (format #f "By expanding this for the first four "))
    (display
     (format #f "iterations, we get:~%"))
    (display
     (format #f "1 + 1/2 = 3/2 = 1.5~%"))
    (display
     (format #f "1 + 1/(2 + 1/2) = 7/5 = 1.4~%"))
    (display
     (format #f "1 + 1/(2 + 1/(2 + 1/2)) = "))
    (display
     (format #f "17/12 = 1.41666...~%"))
    (display
     (format #f "1 + 1/(2 + 1/(2 + 1/(2 + 1/2))) = "))
    (display
     (format #f "41/29 = 1.41379...~%"))
    (newline)
    (display
     (format #f "The next three expansions are 99/70, "))
    (display
     (format #f "239/169, and 577/408,~%"))
    (display
     (format #f "but the eighth expansion, 1393/985, "))
    (display
     (format #f "is the first example~%"))
    (display
     (format #f "where the number of digits in the "))
    (display
     (format #f "numerator exceeds the~%"))
    (display
     (format #f "number of digits in the denominator.~%"))
    (newline)
    (display
     (format #f "In the first one-thousand expansions, "))
    (display
     (format #f "how many fractions~%"))
    (display
     (format #f "contain a numerator with more digits "))
    (display
     (format #f "than denominator?~%"))
    (display
     (format #f "see https://projecteuler.net/problem=57~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((start-num 1)
          (max-num 20)
          (debug-flag #t))
      (begin
        (sub-main-loop start-num max-num debug-flag)
        ))

    (newline)
    (let ((start-num 1)
          (max-num 1000)
          (debug-flag #f))
      (begin
        (sub-main-loop start-num max-num debug-flag)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 57 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (load "digits-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

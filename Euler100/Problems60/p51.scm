#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 51                                     ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated June 16, 2022                                ###
;;;###                                                       ###
;;;###  updated March 6, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### srfi-1 for fold function
(use-modules ((srfi srfi-1)
              :renamer (symbol-prefix-proc 'srfi-1:)))

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### prime-module for is-prime-array? function
(use-modules ((prime-module)
              :renamer (symbol-prefix-proc 'prime-module:)))

;;;### digits-module for split-digits-list function
(use-modules ((digits-module)
              :renamer (symbol-prefix-proc 'digits-module:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (replace-vector-with-same-digit-list local-vector)
  (begin
    (let ((vlen (vector-length local-vector))
          (replace-index-list (list))
          (results-list (list)))
      (begin
        (do ((ii 0 (+ ii 1)))
            ((>= ii vlen))
          (begin
            (let ((this-digit (vector-ref local-vector ii)))
              (begin
                (if (< this-digit 0)
                    (begin
                      (set!
                       replace-index-list
                       (cons ii replace-index-list))
                      ))
                ))
            ))

        (do ((jj 0 (+ jj 1)))
            ((> jj 9))
          (begin
            (let ((vcopy (vector-copy local-vector)))
              (begin
                (for-each
                 (lambda (this-index)
                   (begin
                     (vector-set! vcopy this-index jj)
                     )) replace-index-list)

                (let ((dlist (vector->list vcopy)))
                  (let ((dnum
                         (digits-module:digit-list-to-number
                          dlist)))
                    (begin
                      (if (prime-module:is-prime? dnum)
                          (begin
                            (if (not (zero? (list-ref dlist 0)))
                                (begin
                                  (set!
                                   results-list
                                   (cons dnum results-list))
                                  ))
                            ))
                      )))
                ))
            ))
        (sort results-list <)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (p51-assert-lists-equal
         shouldbe-list result-list
         sub-name
         err-start
         result-hash-table)
  (begin
    (let ((shouldbe-length (length shouldbe-list))
          (result-length (length result-list))
          (err-1
           (format
            #f ", shouldbe-list=~a, result-list=~a"
            shouldbe-list result-list)))
      (let ((err-2
             (format
              #f ", lengths not equal, shouldbe=~a, result=~a"
              shouldbe-length result-length)))
        (begin
          (unittest2:assert?
           (equal? shouldbe-length result-length)
           sub-name
           (string-append
            err-start err-1 err-2)
           result-hash-table)

          (if (> shouldbe-length 0)
              (begin
                (for-each
                 (lambda (s-elem)
                   (begin
                     (let ((mflag
                            (member s-elem result-list))
                           (err-3
                            (format
                             #f ", missing element ~a"
                             s-elem)))
                       (begin
                         (unittest2:assert?
                          (not (equal? mflag #f))
                          sub-name
                          (string-append
                           err-start err-1 err-3)
                          result-hash-table)
                         ))
                     )) shouldbe-list)
                ))
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-replace-vector-with-same-digit-list-1 result-hash-table)
 (begin
   (let ((sub-name "test-replace-vector-with-same-digit-list-1")
         (test-list
          (list
           (list (list -1 3)
                 (list 13 23 43 53 73 83))
           (list (list 5 6 -1 -1 3)
                 (list 56003 56113 56333 56443 56663 56773 56993))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((input-list (list-ref this-list 0))
                  (shouldbe-list (list-ref this-list 1)))
              (let ((result-list
                     (replace-vector-with-same-digit-list
                      (list->vector input-list))))
                (let ((err-start
                       (format
                        #f "~a :: (~a) error : input-list=~a"
                        sub-name test-label-index input-list)))
                  (begin
                    (p51-assert-lists-equal
                     shouldbe-list result-list
                     sub-name
                     err-start
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (+ test-label-index 1))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (substitute-digits-list
         digit-list config-list num-primes)
  (begin
    (let ((result-list (list))
          (dlen (length digit-list))
          (rlist (reverse digit-list)))
      (let ((tlen (- dlen 1)))
        (begin
          (for-each
           (lambda (cfg-list)
             (begin
               (let ((c-len (- (length cfg-list) 1))
                     (c-num (srfi-1:fold + 0 cfg-list))
                     (this-list (list))
                     (jj tlen))
                 (begin
                   (if (= dlen c-num)
                       (begin
                         (do ((ii c-len (1- ii)))
                             ((< ii 0))
                           (begin
                             (let ((c-flag (list-ref cfg-list ii)))
                               (begin
                                 (if (and (= c-flag 1) (>= jj 0))
                                     (begin
                                       (set!
                                        this-list
                                        (cons
                                         (list-ref digit-list jj)
                                         this-list))
                                       (set! jj (1- jj)))
                                     (begin
                                       (set!
                                        this-list
                                        (cons -1 this-list))
                                       ))
                                 ))
                             ))
                         (let ((plist
                                (replace-vector-with-same-digit-list
                                 (list->vector this-list))))
                           (let ((num-results (length result-list))
                                 (num-plist (length plist)))
                             (begin
                               (if (and (>= num-plist num-primes)
                                        (> num-plist num-results))
                                   (begin
                                     (set! result-list plist)
                                     ))
                               )))
                         ))
                   ))
               )) config-list)
          result-list
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (possible-3-digits-list digit-list num-primes)
  (begin
    (let ((dlen (length digit-list))
          (config-list
           (list (list 1 0 1)
                 (list 0 1 1)
                 (list 0 0 1))))
      (begin
        (cond
         ((<= dlen 1)
          (begin
            #f
            ))
         (else
          (begin
            (let ((result-list
                   (substitute-digits-list
                    digit-list config-list num-primes)))
              (begin
                result-list
                ))
            )))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-possible-3-digits-list-1 result-hash-table)
 (begin
   (let ((sub-name "test-possible-3-digits-list-1")
         (test-list
          (list
           (list (list 2 1) 5 (list 211 241 251 271 281))
           (list (list 3 1) 4 (list 131 331 431 631))
           (list (list 1 3) 2 (list 103 113 163 173 193))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((input-list (list-ref this-list 0))
                  (num-primes (list-ref this-list 1))
                  (shouldbe-list (list-ref this-list 2)))
              (let ((result-list
                     (possible-3-digits-list
                      input-list num-primes)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "input-list=~a, num-primes=~a, "
                        input-list num-primes)))
                  (begin
                    (p51-assert-lists-equal
                     shouldbe-list result-list
                     sub-name
                     (string-append
                      err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (+ test-label-index 1))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (possible-4-digits-list digit-list num-primes)
  (begin
    (let ((dlen (length digit-list))
          (config-list
           (list (list 1 0 0 1)
                 (list 0 1 0 1)
                 (list 0 0 1 1)
                 (list 1 1 0 1)
                 (list 1 0 1 1)
                 (list 0 1 1 1))))
      (begin
        (cond
         ((<= dlen 1)
          (begin
            #f
            ))
         (else
          (begin
            (let ((result-list
                   (substitute-digits-list
                    digit-list config-list num-primes)))
              (begin
                result-list
                ))
            )))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-possible-4-digits-list-1 result-hash-table)
 (begin
   (let ((sub-name "test-possible-4-digits-list-1")
         (test-list
          (list
           (list (list 7 2 1)
                 3 (list 7121 7321 7621))
           (list (list 5 3 1)
                 3 (list 5231 5431 5531))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((input-list (list-ref this-list 0))
                  (num-primes (list-ref this-list 1))
                  (shouldbe-list (list-ref this-list 2)))
              (let ((result-list
                     (possible-4-digits-list
                      input-list num-primes)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "input-list=~a, num-primes=~a, "
                        input-list num-primes)))
                  (begin
                    (p51-assert-lists-equal
                     shouldbe-list result-list
                     sub-name
                     (string-append
                      err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (+ test-label-index 1))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (possible-5-digits-list digit-list num-primes)
  (begin
    (let ((dlen (length digit-list))
          (config-list
           (list (list 1 0 0 0 1)
                 (list 0 1 0 0 1)
                 (list 0 0 1 0 1)
                 (list 0 0 0 1 1)
                 (list 1 1 0 0 1)
                 (list 1 0 1 0 1)
                 (list 1 0 0 1 1)
                 (list 0 1 1 0 1)
                 (list 0 1 0 1 1)
                 (list 0 0 1 1 1)
                 (list 1 1 1 0 1)
                 (list 1 1 0 1 1)
                 (list 1 0 1 1 1)
                 (list 0 1 1 1 1)
                 )))
      (begin
        (cond
         ((<= dlen 1)
          (begin
            #f
            ))
         (else
          (begin
            (let ((result-list
                   (substitute-digits-list
                    digit-list config-list num-primes)))
              (begin
                result-list
                ))
            )))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-possible-5-digits-list-1 result-hash-table)
 (begin
   (let ((sub-name "test-possible-5-digits-list-1")
         (test-list
          (list
           (list (list 3 7 2 1)
                 3 (list 31721 33721 34721 36721))
           (list (list 8 5 3 1)
                 3 (list 85331 85531 85831 85931))
           (list (list 5 6 3)
                 3 (list 56003 56113 56333 56443
                         56663 56773 56993))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((input-list (list-ref this-list 0))
                  (num-primes (list-ref this-list 1))
                  (shouldbe-list (list-ref this-list 2)))
              (let ((result-list
                     (possible-5-digits-list
                      input-list num-primes)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "input-list=~a, num-primes=~a, "
                        input-list num-primes)))
                  (begin
                    (p51-assert-lists-equal
                     shouldbe-list result-list
                     sub-name
                     (string-append
                      err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (+ test-label-index 1))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (possible-6-digits-list digit-list num-primes)
  (begin
    (let ((dlen (length digit-list))
          (config-list
           (list (list 1 0 0 0 0 1)
                 (list 0 1 0 0 0 1)
                 (list 0 0 1 0 0 1)
                 (list 0 0 0 1 0 1)
                 (list 0 0 0 0 1 1)
                 (list 1 1 0 0 0 1)
                 (list 1 0 1 0 0 1)
                 (list 1 0 0 1 0 1)
                 (list 1 0 0 0 1 1)
                 (list 0 1 1 0 0 1)
                 (list 0 1 0 1 0 1)
                 (list 0 1 0 0 1 1)
                 (list 0 0 1 1 0 1)
                 (list 0 0 1 0 1 1)
                 (list 0 0 0 1 1 1)
                 (list 1 1 1 0 0 1)
                 (list 1 1 0 1 0 1)
                 (list 1 1 0 0 1 1)
                 (list 1 0 1 1 0 1)
                 (list 1 0 1 0 1 1)
                 (list 1 0 0 1 1 1)
                 (list 0 1 1 1 0 1)
                 (list 0 1 1 0 1 1)
                 (list 0 1 0 1 1 1)
                 (list 0 0 1 1 1 1)
                 (list 1 1 1 1 0 1)
                 (list 1 1 1 0 1 1)
                 (list 1 1 0 1 1 1)
                 (list 1 0 1 1 1 1)
                 (list 0 1 1 1 1 1)
                 )))
      (begin
        (cond
         ((<= dlen 1)
          (begin
            #f
            ))
         (else
          (begin
            (let ((result-list
                   (substitute-digits-list
                    digit-list config-list num-primes)))
              (begin
                result-list
                ))
            )))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-possible-6-digits-list-1 result-hash-table)
 (begin
   (let ((sub-name "test-possible-6-digits-list-1")
         (test-list
          (list
           (list (list 3 3 7 2 1)
                 3 (list 330721 333721 334721 337721))
           (list (list 3 6 5 3 1)
                 3 (list 361531 365531 367531 368531))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((input-list (list-ref this-list 0))
                  (num-primes (list-ref this-list 1))
                  (shouldbe-list (list-ref this-list 2)))
              (let ((result-list
                     (possible-6-digits-list input-list num-primes)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "input-list=~a, num-primes=~a, "
                        input-list num-primes)))
                  (begin
                    (p51-assert-lists-equal
                     shouldbe-list result-list
                     sub-name
                     (string-append
                      err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (+ test-label-index 1))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; replace nn digits at a time in input vector
;;; return a list of primes
(define (possible-n-digits-list digit-list min-num)
  (begin
    (let ((dlen (length digit-list))
          (min-list (list))
          (min-length 0)
          (min-elem 0)
          (function-list
           (list possible-3-digits-list
                 possible-4-digits-list
                 possible-5-digits-list
                 possible-6-digits-list)))
      (begin
        (for-each
         (lambda (possible-func)
           (begin
             (let ((rlist
                    (possible-func digit-list min-num)))
               (begin
                 (if (and (list? rlist) (> (length rlist) 0))
                     (begin
                       (let ((rlen (length rlist)))
                         (begin
                           (if (= rlen min-num)
                               (begin
                                 (let ((this-elem (car rlist)))
                                   (begin
                                     (if (or (<= min-elem 0)
                                             (< this-elem min-elem))
                                         (begin
                                           (set! min-list rlist)
                                           (set! min-length rlen)
                                           (set! min-elem this-elem)
                                           ))
                                     ))
                                 ))
                           ))
                       ))
                 ))
             )) function-list)

        min-list
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-possible-n-digits-list-1 result-hash-table)
 (begin
   (let ((sub-name "test-possible-n-digits-list-1")
         (test-list
          (list
           (list (list 2 3)
                 5 (list 223 233 263 283 293))
           (list (list 1 2 3)
                 6 (list 1123 1223 1423 1523 1723 1823))
           (list (list 5 6 1 3)
                 3 (list 56113 56713 56813))
           (list (list 5 6 3)
                 7 (list 56003 56113 56333 56443 56663 56773 56993))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((input-list (list-ref this-list 0))
                  (test-nn (list-ref this-list 1))
                  (shouldbe-list (list-ref this-list 2)))
              (let ((result-list
                     (possible-n-digits-list input-list test-nn)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "input-list=~a, test-nn=~a"
                        input-list test-nn)))
                  (begin
                    (p51-assert-lists-equal
                     shouldbe-list result-list
                     sub-name
                     (string-append
                      err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (+ test-label-index 1))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (list-to-set-string llist)
  (begin
    (let ((stmp
           (string-join
            (map
             (lambda (num)
               (begin
                 (ice-9-format:format #f "~:d" num)
                 )) llist)
            ", ")))
      (begin
        (string-append "{ " stmp " }")
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-list-to-set-string-1 result-hash-table)
 (begin
   (let ((sub-name "test-list-to-set-string-1")
         (test-list
          (list
           (list (list 1) "{ 1 }")
           (list (list 1 2) "{ 1, 2 }")
           (list (list 1 2 3) "{ 1, 2, 3 }")
           (list (list 4 5 6 7) "{ 4, 5, 6, 7 }")
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((input-list (list-ref this-list 0))
                  (shouldbe-string (list-ref this-list 1)))
              (let ((result-string
                     (list-to-set-string input-list)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : input-list=~a, "
                        sub-name test-label-index input-list))
                      (err-2
                       (format
                        #f "shouldbe=~s, result=~s~%"
                        shouldbe-string result-string)))
                  (begin
                    (unittest2:assert?
                     (string-ci=? shouldbe-string
                                  result-string)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (+ test-label-index 1))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop start-num end-num min-nn)
  (begin
    (let ((smallest-list (list))
          (smallest-count 0)
          (smallest-elem 0)
          (begin-num start-num)
          (counter 0)
          (break-flag #f))
      (begin
        (if (zero? (modulo start-num 2))
            (begin
              (set! begin-num (1+ begin-num))
              ))

        (do ((ii begin-num (+ ii 2)))
            ((or (> ii end-num)
                 (equal? break-flag #t)))
          (begin
            (if (not (zero? (modulo ii 5)))
                (begin
                  (let ((dlist
                         (digits-module:split-digits-list ii)))
                    (let ((dlen (length dlist)))
                      (begin
                        (let ((this-list
                               (possible-n-digits-list dlist min-nn)))
                          (begin
                            (if (and (list? this-list)
                                     (= (length this-list) min-nn))
                                (begin
                                  (let ((first-elem (car this-list)))
                                    (begin
                                      (if (or (<= smallest-elem 0)
                                              (<= first-elem smallest-elem))
                                          (begin
                                            (set! smallest-list this-list)
                                            (set!
                                             smallest-count (length this-list))
                                            (set! smallest-elem first-elem)
                                            ))
                                      ))
                                  ))
                            ))
                        )))
                  ))
            ))

        (if (not (= smallest-count min-nn))
            (begin
              (display
               (ice-9-format:format
                #f "no sequence of ~:d found~%"
                min-nn start-num end-num))
              (display
               (ice-9-format:format
                #f "  (for numbers between ~:d and ~:d)~%"
                start-num end-num))
              #f)
            (begin
              (display
               (ice-9-format:format
                #f "~:d is the smallest prime which, by "
                smallest-elem))
              (display
               (ice-9-format:format
                #f "replacing part of~%"))
              (display
               (ice-9-format:format
                #f "the number with the same digit, is "))
              (display
               (ice-9-format:format
                #f "part of an~%"))
              (display
               (ice-9-format:format
                #f "~:d-prime value family, for generating "
                (list-ref smallest-list 0)))
              (display
               (ice-9-format:format
                #f "numbers between~%"))
              (display
               (ice-9-format:format
                #f "~:d and ~:d~%"
                start-num end-num))
              (display
               (ice-9-format:format
                #f "    count = ~:d : ~a~%"
                smallest-count
                (list-to-set-string smallest-list)))
              (force-output)
              #t
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "By replacing the 1st digit of *3, "))
    (display
     (format #f "it turns out that~%"))
    (display
     (format #f "six of the nine possible values: "))
    (display
     (format #f "13, 23, 43, 53,~%"))
    (display
     (format #f "73, and 83, are all prime.~%"))
    (newline)
    (display
     (format #f "By replacing the 3rd and 4th "))
    (display
     (format #f "digits of 56**3 with~%"))
    (display
     (format #f "the same digit, this 5-digit number "))
    (display
     (format #f "is the first example~%"))
    (display
     (format #f "having seven primes among the ten "))
    (display
     (format #f "generated numbers,~%"))
    (display
     (format #f "yielding the family: 56003, 56113, "))
    (display
     (format #f "56333, 56443, 56663,~%"))
    (display
     (format #f "56773, and 56993. Consequently 56003, "))
    (display
     (format #f "being the first member~%"))
    (display
     (format #f "of this family, is the smallest prime "))
    (display
     (format #f "with this property.~%"))
    (newline)
    (display
     (format #f "Find the smallest prime which, by "))
    (display
     (format #f "replacing part of the~%"))
    (display
     (format #f "number (not necessarily adjacent "))
    (display
     (format #f "digits) with the same~%"))
    (display
     (format #f "digit, is part of an eight prime "))
    (display
     (format #f "value family.~%"))
    (newline)
    (display
     (format #f "The key idea for solving this problem "))
    (display
     (format #f "was gotten from~%"))
    (display
     (format #f "https://euler.stephan-brumme.com/51/~%"))
    (display
     (format #f "Hard-coding the possible permutations "))
    (display
     (format #f "enabled the program to~%"))
    (display
     (format #f "complete much faster than by standard "))
    (display
     (format #f "recursion over all~%"))
    (display
     (format #f "possible rearrangements of digits.~%"))
    (display
     (format #f "see https://projecteuler.net/problem=51~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((test-list-list
           (list
            (list 10 1000 7)
            (list 10 1000 8)
            ))
          (continue-loop-flag #t))
      (let ((max-tests (length test-list-list)))
        (begin
          (do ((ii 0 (1+ ii)))
              ((>= ii max-tests))
            (begin
              (newline)
              (let ((a-list (list-ref test-list-list ii)))
                (let ((start-num (list-ref a-list 0))
                      (end-num (list-ref a-list 1))
                      (min-nn (list-ref a-list 2)))
                  (begin
                    (sub-main-loop start-num end-num min-nn)
                    )))
              (force-output)
              ))
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 51 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (load "prime-module-tests-1.scm")
                 (load "digits-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

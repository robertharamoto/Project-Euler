#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 55                                     ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated June 16, 2022                                ###
;;;###                                                       ###
;;;###  updated March 6, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### digits-module for split-digits-list function
(use-modules ((digits-module)
              :renamer (symbol-prefix-proc 'digits-module:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (palindrome? nn base)
  (begin
    (cond
     ((< nn 0)
      (begin
        #f
        ))
     (else
      (begin
        (let ((tstring (number->string nn base)))
          (let ((rstring (string-reverse tstring)))
            (begin
              (string-ci=? tstring rstring)
              )))
        )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-palindrome-1 result-hash-table)
 (begin
   (let ((sub-name "test-palindrome-1")
         (test-list
          (list
           (list 3 10 #t) (list 3 2 #t)
           (list 7 10 #t) (list 7 2 #t)
           (list 10 10 #f) (list 11 10 #t)
           (list 12 10 #f) (list 13 10 #f)
           (list 20 10 #f) (list 21 10 #f)
           (list 22 10 #t) (list 23 10 #f)
           (list 313 10 #t) (list 323 10 #t)
           (list 333 10 #t) (list 334 10 #f)
           (list 585 10 #t) (list 585 2 #t)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((test-num (list-ref this-list 0))
                  (test-base (list-ref this-list 1))
                  (shouldbe (list-ref this-list 2)))
              (let ((result
                     (palindrome? test-num test-base)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : test-num=~a, test-base=~a, "
                        sub-name test-label-index test-num test-base))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (lychrel-number nn max-num)
  (begin
    (let ((result-list (list))
          (this-num nn)
          (break-flag #f))
      (begin
        (do ((ii 0 (+ ii 1)))
            ((or (> ii max-num)
                 (equal? break-flag #t)))
          (begin
            (let ((this-list
                   (digits-module:split-digits-list this-num)))
              (let ((rlist (reverse this-list)))
                (let ((rnum (digits-module:digit-list-to-number rlist)))
                  (let ((sum (+ this-num rnum)))
                    (begin
                      (set!
                       result-list
                       (cons (list this-num rnum sum) result-list))

                      (if (palindrome? sum 10)
                          (begin
                            (set! break-flag #t))
                          (begin
                            (set! this-num sum)))
                      )))
                ))
            ))
        (if (equal? break-flag #t)
            (begin
              (list #f (reverse result-list)))
            (begin
              (list #t (reverse result-list))
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (p55-assert-lists-equal
         shouldbe-list result-list
         sub-name
         err-start
         result-hash-table)
  (begin
    (let ((shouldbe-length (length shouldbe-list))
          (result-length (length result-list))
          (err-1
           (format
            #f ", shouldbe-list=~a, result-list=~a"
            shouldbe-list result-list)))
      (let ((err-2
             (format
              #f ", lengths not equal, shouldbe=~a, result=~a"
              shouldbe-length result-length)))
        (begin
          (unittest2:assert?
           (equal? shouldbe-length result-length)
           sub-name
           (string-append
            err-start err-1 err-2)
           result-hash-table)

          (if (> shouldbe-length 0)
              (begin
                (for-each
                 (lambda (s-elem)
                   (begin
                     (let ((mflag
                            (member s-elem result-list))
                           (err-3
                            (format
                             #f ", missing element ~a"
                             s-elem)))
                       (begin
                         (unittest2:assert?
                          (not (equal? mflag #f))
                          sub-name
                          (string-append
                           err-start err-1 err-3)
                          result-hash-table)
                         ))
                     )) shouldbe-list)
                ))
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-lychrel-number-1 result-hash-table)
 (begin
   (let ((sub-name "test-lychrel-number-1")
         (test-list
          (list
           (list 47
                 (list #f (list (list 47 74 121))))
           (list 349
                 (list #f
                       (list (list 349 943 1292)
                             (list 1292 2921 4213)
                             (list 4213 3124 7337))))
           ))
         (max-num 50)
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((test-num (list-ref this-list 0))
                  (shouldbe-list-list (list-ref this-list 1)))
              (let ((result-list-list
                     (lychrel-number test-num max-num)))
                (let ((shouldbe-bool
                       (list-ref shouldbe-list-list 0))
                      (shouldbe-ll
                       (list-ref shouldbe-list-list 1))
                      (result-bool
                       (list-ref result-list-list 0))
                      (result-ll
                       (list-ref result-list-list 1))
                      (err-1
                       (format
                        #f "~a :: (~a) error : test-num=~a, "
                        sub-name test-label-index test-num))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe-list-list result-list-list)))
                  (let ((s-len (length shouldbe-ll))
                        (r-len (length result-ll))
                        (err-3
                         (format
                          #f "shouldbe length=~a, result length=~a"
                          (length shouldbe-ll)
                          (length result-ll))))
                    (begin
                      (unittest2:assert?
                       (equal? shouldbe-bool result-bool)
                       sub-name
                       (string-append err-1 err-2)
                       result-hash-table)

                      (unittest2:assert?
                       (equal? s-len r-len)
                       sub-name
                       (string-append
                        err-1 err-2 ", " err-3)
                       result-hash-table)

                      (if (> s-len 0)
                          (begin
                            (do ((ii 0 (1+ ii)))
                                ((>= ii s-len))
                              (begin
                                (let ((slist
                                       (list-ref shouldbe-ll ii))
                                      (rlist
                                       (list-ref result-ll ii)))
                                  (begin
                                    (p55-assert-lists-equal
                                     slist rlist
                                     sub-name
                                     err-1
                                     result-hash-table)
                                    ))
                                ))
                            ))
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop
         start-num max-num max-iterations debug-flag)
  (begin
    (let ((counter 0))
      (begin
        (do ((ii start-num (1+ ii)))
            ((> ii max-num))
          (begin
            (let ((this-list-list
                   (lychrel-number ii max-iterations)))
              (let ((lychrel-flag (list-ref this-list-list 0))
                    (lychrel-list (list-ref this-list-list 1)))
                (begin
                  (if (equal? lychrel-flag #t)
                      (begin
                        (set! counter (+ counter 1))
                        (if (equal? debug-flag #t)
                            (begin
                              (display
                               (ice-9-format:format
                                #f "(~:d) lychrel number found! ~:d~%"
                                counter ii))
                              (display
                               (ice-9-format:format
                                #f "number of iteratios = ~:d~%"
                                (length lychrel-list)))
                              ))
                        ))
                  )))
            ))

        (newline)
        (display
         (ice-9-format:format
          #f "There are ~:d lychrel numbers~%" counter))
        (display
         (ice-9-format:format
          #f "between = ~:d and ~:d, max iterations = ~:d~%"
          start-num max-num max-iterations))
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "If we take 47, reverse and add, "))
    (display
     (format #f "47 + 74 = 121,~%"))
    (display
     (format #f "which is palindromic.~%"))
    (newline)
    (display
     (format #f "Not all numbers produce palindromes "))
    (display
     (format #f "so quickly. For example,~%"))
    (display
     (format #f "  349 + 943 = 1292~%"))
    (display
     (format #f "  1292 + 2921 = 4213~%"))
    (display
     (format #f "  4213 + 3124 = 7337~%"))
    (newline)
    (display
     (format #f "That is, 349 took three iterations to "))
    (display
     (format #f "arrive at a palindrome.~%"))
    (newline)
    (display
     (format #f "Although no one has proved it yet, it is "))
    (display
     (format #f "thought that some numbers,~%"))
    (display
     (format #f "like 196, never produce a palindrome. A "))
    (display
     (format #f "number that never forms~%"))
    (display
     (format #f "a palindrome through the reverse and add "))
    (display
     (format #f "process is called a~%"))
    (display
     (format #f "Lychrel number. Due to the theoretical "))
    (display
     (format #f "nature of these numbers,~%"))
    (display
     (format #f "and for the purpose of this problem, we "))
    (display
     (format #f "shall assume that a~%"))
    (display
     (format #f "number is Lychrel until proven otherwise. "))
    (display
     (format #f "In addition you are~%"))
    (display
     (format #f "given that for every~%"))
    (display
     (format #f "number below ten-thousand, it will either~%"))
    (display
     (format #f "(i) become a palindrome in less than "))
    (display
     (format #f "fifty iterations, or,~%"))
    (display
     (format #f "(ii) no one, with all the computing power "))
    (display
     (format #f "that exists, has managed~%"))
    (display
     (format #f "so far to map it to a palindrome. In fact, "))
    (display
     (format #f "10677 is the~%"))
    (display
     (format #f "first number to be shown to require "))
    (display
     (format #f "over fifty iterations~%"))
    (display
     (format #f "before producing a palindrome:~%"))
    (display
     (format #f "4668731596684224866951378664~%"))
    (display
     (format #f "(53 iterations, 28-digits).~%"))
    (newline)
    (display
     (format #f "Surprisingly, there are palindromic numbers "))
    (display
     (format #f "that are themselves~%"))
    (display
     (format #f "Lychrel numbers; the first example "))
    (display
     (format #f "is 4994.~%"))
    (newline)
    (display
     (format #f "How many Lychrel numbers are there "))
    (display
     (format #f "below ten-thousand?~%"))
    (newline)
    (display
     (format #f "NOTE: Wording was modified slightly "))
    (display
     (format #f "on 24 April 2007 to~%"))
    (display
     (format #f "emphasise the theoretical nature of "))
    (display
     (format #f "Lychrel numbers.~%"))
    (display
     (format #f "see https://projecteuler.net/problem=55~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((start-num 1)
          (max-num 1000)
          (max-iterations 50)
          (debug-flag #t))
      (begin
        (sub-main-loop start-num max-num
                       max-iterations debug-flag)
        ))

    (newline)
    (let ((start-num 1)
          (max-num 10000)
          (max-iterations 50)
          (debug-flag #f))
      (begin
        (sub-main-loop start-num max-num
                       max-iterations debug-flag)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 55 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (load "digits-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

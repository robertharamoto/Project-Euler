#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 58                                     ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated June 16, 2022                                ###
;;;###                                                       ###
;;;###  updated March 5, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### prime-module for is-prime-array? function
(use-modules ((prime-module)
              :renamer (symbol-prefix-proc 'prime-module:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
;;; spiral anti-clockwise
(define (make-reverse-spiral-array! this-array max-row max-col)
  (begin
    (let ((center-row (euclidean/ max-row 2))
          (center-col (euclidean/ max-col 2))
          (arm-length 1)
          (arm-count 0)
          (current-direction 1)
          (num-counter 1)
          (end-flag #f))
      (let ((this-row center-row)
            (this-col center-col))
        (begin
          (array-set! this-array num-counter this-row this-col)
          (set! num-counter (1+ num-counter))

          (while
           (and (equal? end-flag #f)
                (<= arm-length max-row)
                (<= arm-length max-col))
           (begin
             (cond
              ((or (> this-row max-row)
                   (> this-col max-col))
               (begin
                 (set! end-flag #t)
                 ))
              ((= current-direction 1)
               (begin
                  ;;; then we are incrementing columns
                 (do ((ii 0 (+ ii 1)))
                     ((or (>= ii arm-length)
                          (>= (+ this-col 1) max-col)))
                   (begin
                     (let ((next-col (+ this-col 1)))
                       (begin
                         (if (zero? (array-ref this-array this-row next-col))
                             (begin
                               (array-set! this-array num-counter this-row next-col)
                               (set! num-counter (1+ num-counter))
                               (set! this-col next-col)
                               ))
                         ))
                     ))
                 (set! current-direction -2)
                 (set! arm-count (1+ arm-count))
                 (if (>= arm-count 2)
                     (begin
                       (set! arm-count 0)
                       (set! arm-length (1+ arm-length))
                       ))
                 ))
              ((= current-direction -1)
               (begin
                  ;;; then we are decrementing columns
                 (do ((ii 0 (+ ii 1)))
                     ((or (>= ii arm-length)
                          (< this-col 0)))
                   (begin
                     (let ((next-col (- this-col 1)))
                       (begin
                         (if (zero?
                              (array-ref
                               this-array this-row next-col))
                             (begin
                               (array-set!
                                this-array
                                num-counter this-row next-col)
                               (set! num-counter (1+ num-counter))
                               (set! this-col next-col)
                               ))
                         ))
                     ))
                 (set! current-direction +2)
                 (set! arm-count (1+ arm-count))
                 (if (>= arm-count 2)
                     (begin
                       (set! arm-count 0)
                       (set! arm-length (1+ arm-length))
                       ))
                 ))
              ((= current-direction 2)
               (begin
                  ;;; then we are incrementing rows
                 (do ((ii 0 (+ ii 1)))
                     ((or (>= ii arm-length)
                          (>= (+ this-row 1) max-row)))
                   (begin
                     (let ((next-row (+ this-row 1)))
                       (begin
                         (if (zero?
                              (array-ref
                               this-array next-row this-col))
                             (begin
                               (array-set!
                                this-array
                                num-counter next-row this-col)
                               (set! num-counter (1+ num-counter))
                               (set! this-row next-row)
                               ))
                         ))
                     ))

                 (set! current-direction 1)
                 (set! arm-count (1+ arm-count))
                 (if (>= arm-count 2)
                     (begin
                       (set! arm-count 0)
                       (set! arm-length (1+ arm-length))
                       ))
                 ))
              ((= current-direction -2)
               (begin
                  ;;; then we are decrementing rows
                 (do ((ii 0 (+ ii 1)))
                     ((or (>= ii arm-length)
                          (< this-row 0)))
                   (begin
                     (let ((next-row (- this-row 1)))
                       (begin
                         (if (zero?
                              (array-ref
                               this-array next-row this-col))
                             (begin
                               (array-set!
                                this-array
                                num-counter next-row this-col)
                               (set! num-counter (1+ num-counter))
                               (set! this-row next-row)
                               ))
                         ))
                     ))

                 (set! current-direction -1)
                 (set! arm-count (1+ arm-count))
                 (if (>= arm-count 2)
                     (begin
                       (set! arm-count 0)
                       (set! arm-length (1+ arm-length))
                       ))
                 ))
              (else
               (begin
                 (display
                  (format #f "make-spiral-array! error: "))
                 (display
                  (format #f "invalid current-direction=~a~%"
                          current-direction))
                 (display
                  (format #f "quitting...~%"))
                 (quit)
                 )))
             ))
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-make-reverse-spiral-array-3-1 result-hash-table)
 (begin
   (let ((sub-name "test-make-reverse-spiral-array-3-1")
         (test-array (make-array 0 3 3))
         (shouldbe-list-list
          (list
           (list 5 4 3)
           (list 6 1 2)
           (list 7 8 9)))
         (max-row 3)
         (max-col 3)
         (test-label-index 0))
     (let ((shouldbe-array
            (list->array 2 shouldbe-list-list)))
       (begin
         (make-reverse-spiral-array! test-array max-row max-col)

         (do ((ii-row 0 (+ ii-row 1)))
             ((>= ii-row max-row))
           (begin
             (do ((ii-col 0 (+ ii-col 1)))
                 ((>= ii-col max-col))
               (begin
                 (let ((result-num
                        (array-ref test-array ii-row ii-col))
                       (shouldbe-num
                        (array-ref shouldbe-array ii-row ii-col)))
                   (let ((err-1
                          (format
                           #f "~a :: (~a) error : "
                           sub-name test-label-index))
                         (err-2
                          (format
                           #f "shouldbe-list-list=~a, "
                           shouldbe-list-list))
                         (err-3
                          (format
                           #f "shouldbe=~a, result=~a"
                           shouldbe-num result-num)))
                     (begin
                       (unittest2:assert?
                        (equal? shouldbe-num result-num)
                        sub-name
                        (string-append
                         err-1 err-2 err-3)
                        result-hash-table)
                       )))
                 ))
             ))
         )))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-make-reverse-spiral-array-5-1 result-hash-table)
 (begin
   (let ((sub-name "test-make-reverse-spiral-array-5-1")
         (test-array (make-array 0 5 5))
         (shouldbe-list-list
          (list
           (list 17 16 15 14 13)
           (list 18  5  4  3 12)
           (list 19  6  1  2 11)
           (list 20  7  8  9 10)
           (list 21 22 23 24 25)
           ))
         (max-row 5)
         (max-col 5)
         (test-label-index 0))
     (let ((shouldbe-array
            (list->array 2 shouldbe-list-list)))
       (let ((err-1
              (format
               #f "~a :: (~a) error : shouldbe-list-list=~a, "
               sub-name test-label-index shouldbe-list-list)))
         (begin
           (make-reverse-spiral-array!
            test-array max-row max-col)

           (do ((ii-row 0 (+ ii-row 1)))
               ((>= ii-row max-row))
             (begin
               (do ((ii-col 0 (+ ii-col 1)))
                   ((>= ii-col max-col))
                 (begin
                   (let ((result-num
                          (array-ref test-array ii-row ii-col))
                         (shouldbe-num
                          (array-ref shouldbe-array ii-row ii-col)))
                     (let ((err-2
                            (format
                             #f "shouldbe=~a, result=~a"
                             shouldbe-num result-num)))
                       (begin
                         (unittest2:assert?
                          (equal? shouldbe-num result-num)
                          sub-name
                          (string-append err-1 err-2)
                          result-hash-table)
                         )))
                   ))
               ))
           ))
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (display-array this-array max-row max-col)
  (begin
    (cond
     ((not (array? this-array))
      (begin
        (display
         (format
          #f "display-array error: expecting array, "))
        (display
         (format
          #f "instead received ~a~%"
          this-array))
        (display
         (format #f "quitting...~%"))
        (quit)
        ))
     (else
      (begin
        (do ((ii-row 0 (+ ii-row 1)))
            ((>= ii-row max-row))
          (begin
            (do ((ii-col 0 (+ ii-col 1)))
                ((>= ii-col max-col))
              (begin
                (display
                 (ice-9-format:format
                  #f "~4:d "
                  (array-ref this-array ii-row ii-col)))
                ))
            (newline)
            ))
        )))
    ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop end-num floor-percent debug-flag)
  (begin
    (let ((break-flag #f)
          (total 0)
          (prime-count 0)
          (side-length 0)
          (ratio 0.0))
      (begin
        (do ((ii 1 (1+ ii)))
            ((or (> ii end-num)
                 (equal? break-flag #t)))
          (begin
            ;;; for-each square ii
            (let ((ii-side-length (+ (* 2 ii) 1))
                  (ii-diagonal-count (+ (* 4 ii) 1))
                  (ii-prime-count 0))
              (let ((se-value
                     (* ii-side-length ii-side-length))
                    (delta (* 2 ii)))
                (let ((sw-value (- se-value delta)))
                  (let ((nw-value (- sw-value delta)))
                    (let ((ne-value (- nw-value delta)))
                      (begin
                        (if (prime-module:is-prime? sw-value)
                            (begin
                              (set! ii-prime-count
                                    (1+ ii-prime-count))
                              ))
                        (if (prime-module:is-prime? nw-value)
                            (begin
                              (set! ii-prime-count
                                    (1+ ii-prime-count))
                              ))
                        (if (prime-module:is-prime? ne-value)
                            (begin
                              (set! ii-prime-count
                                    (1+ ii-prime-count))
                              ))
                        (set! total ii-diagonal-count)
                        (set! side-length ii-side-length)
                        (set! prime-count (+ prime-count ii-prime-count))
                        (set!
                         ratio
                         (* 0.00010
                            (truncate
                             (* 10000.0
                                (exact->inexact
                                 (/ prime-count total))))))

                        (if (equal? debug-flag #t)
                            (begin
                              (display
                               (ice-9-format:format
                                #f "for ~:d x ~:d : ~:d / ~:d "
                                side-length side-length
                                prime-count total))
                              (display
                               (format
                                #f "= ~5,2f%~%"
                                (* 100.0 ratio)))
                              (force-output)
                              ))

                        (if (< ratio floor-percent)
                            (begin
                              (set! break-flag #t)
                              (display
                               (ice-9-format:format
                                #f "square spiral of dimensions "))
                              (display
                               (ice-9-format:format
                                #f "~:d x ~:d has a ratio of primes~%"
                                side-length side-length))
                              (display
                               (ice-9-format:format
                                #f "along both diagonals less than "))
                              (display
                               (ice-9-format:format
                                #f "~5,2f%, ratio = ~5,2f% "
                                (* 100.0 floor-percent) (* 100.0 ratio)))
                              (display
                               (ice-9-format:format
                                #f "= ~:d / ~:d~%"
                                prime-count total))
                              (display
                               (ice-9-format:format
                                #f "for square dimensions between "))
                              (display
                               (ice-9-format:format
                                #f "~:d and ~:d~%" 1 end-num))
                              (force-output)
                              ))
                        )))
                  )))
            ))

        (if (equal? break-flag #f)
            (begin
              (display
               (ice-9-format:format
                #f "no square spiral with ratio less than ~5,2f%% "
                (* 100.0 floor-percent)))
              (display
               (ice-9-format:format
                #f "found between dimensions ~:d and ~:d~%"
                3 (+ (* 2 end-num) 1)))
              (force-output)
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Starting with 1 and spiralling "))
    (display
     (format #f "anticlockwise in the~%"))
    (display
     (format #f "following way, a square spiral with "))
    (display
     (format #f "side length 7~%"))
    (display
     (format #f "is formed.~%"))
    (newline)
    (display
     (format #f " 37 36 35 34 33 32 31~%"))
    (display
     (format #f " 38 17 16 15 14 13 30~%"))
    (display
     (format #f " 39 18  5  4  3 12 29~%"))
    (display
     (format #f " 40 19  6  1  2 11 28~%"))
    (display
     (format #f " 41 20  7  8  9 10 27~%"))
    (display
     (format #f " 42 21 22 23 24 25 26~%"))
    (display
     (format #f " 43 44 45 46 47 48 49~%"))
    (newline)
    (display
     (format #f "It is interesting to note that the "))
    (display
     (format #f "odd squares lie along~%"))
    (display
     (format #f "the bottom right diagonal, but what "))
    (display
     (format #f "is more interesting is~%"))
    (display
     (format #f "that 8 out of the 13 numbers lying along "))
    (display
     (format #f "both diagonals are prime;~%"))
    (display
     (format #f "that is, a ratio of 8/13 = 62%.~%"))
    (newline)
    (display
     (format #f "If one complete new layer is wrapped "))
    (display
     (format #f "around the spiral above,~%"))
    (display
     (format #f "a square spiral with side length 9 will "))
    (display
     (format #f "be formed. If this~%"))
    (display
     (format #f "process is continued, what is the side "))
    (display
     (format #f "length of the square~%"))
    (display
     (format #f "spiral for which the ratio of primes along "))
    (display
     (format #f "both diagonals first~%"))
    (display
     (format #f "falls below 10%?~%"))
    (newline)
    (display
     (format #f "A discussion can be found at~%"))
    (display
     (format #f "https://martin-ueding.de/posts/project-euler-solution-58-spiral-primes/~%"))
    (newline)
    (display
     (format #f "Instead of generating the spiral manually, "))
    (display
     (format #f "each diagonal element~%"))
    (display
     (format #f "can be calculated.  For block i, which "))
    (display
     (format #f "has a side length~%"))
    (display
     (format #f "= (2i+1), the south-east diagonal (SE), "))
    (display
     (format #f "has elements (2*i+1)^2~%"))
    (display
     (format #f "(always a square, since the i'th square "))
    (display
     (format #f "is a square). The~%"))
    (display
     (format #f "southwest diagonal has elements SE(i) - 2i, "))
    (display
     (format #f "the northwest diagonal~%"))
    (display
     (format #f "has elements SE(i)-4i, and the northeast "))
    (display
     (format #f "diagonal elements are~%"))
    (display
     (format #f "SE(i)-6i.  For square i, the total "))
    (display
     (format #f "number on each diagonal~%"))
    (display
     (format #f "is 4i+1.~%"))
    (display
     (format #f "see https://projecteuler.net/problem=58~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((end-num 10)
          (floor-percent 0.10)
          (debug-flag #t))
      (begin
        (sub-main-loop end-num floor-percent debug-flag)
        ))

    (newline)
    (let ((end-num 100000)
          (floor-percent 0.10)
          (debug-flag #f))
      (begin
        (sub-main-loop end-num floor-percent debug-flag)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 58 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (load "prime-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

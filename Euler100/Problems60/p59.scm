#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 59                                     ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated June 16, 2022                                ###
;;;###                                                       ###
;;;###  updated March 5, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### srfi-1 for fold function
(use-modules ((srfi srfi-1)
              :renamer (symbol-prefix-proc 'srfi-1:)))

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### ice-9 rdelim for reading delimited text from files
(use-modules ((ice-9 rdelim)
              :renamer (symbol-prefix-proc 'ice-9-rdelim:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
;;; ascii code limits A - 65, Z - 90, a - 97, z - 122
;;; 0 - 48, 9 - 57

;;;#############################################################
;;;#############################################################
(define (populate-frequency-hash!
         letter-htable char1 ii-th ii-delta integer-list)
  (begin
    (let ((char-count 0)
          (trial-char-int (char->integer char1))
          (int-len (length integer-list)))
      (begin
        (hash-clear! letter-htable)

        (do ((ii ii-th (+ ii ii-delta)))
            ((>= ii int-len))
          (begin
            (let ((this-pwd-int
                   (list-ref integer-list ii)))
              (let ((translated-char
                     (logxor this-pwd-int trial-char-int)))
                (begin
                  (if (not (equal? translated-char #\space))
                      (begin
                        (let ((key
                               (integer->char translated-char)))
                          (let ((hvalue
                                 (hash-ref letter-htable key 0)))
                            (begin
                              (hash-set! letter-htable key (1+ hvalue))
                              )))
                        ))
                  )))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (find-max-count-in-hash letter-htable)
  (begin
    (let ((max-count 0)
          (result-list (list)))
      (begin
        (hash-for-each
         (lambda (key value)
           (begin
             (if (> value max-count)
                 (begin
                   (set! max-count value)
                   (set! result-list (list key value))
                   ))
             )) letter-htable)

        result-list
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-find-max-count-in-hash-1 result-hash-table)
 (begin
   (let ((sub-name "test-find-max-count-in-hash-1")
         (test-list
          (list
           (list (list (list #\a 10) (list #\b 20) (list #\c 25)
                       (list #\d 30) (list #\e 40) (list #\f 50)
                       (list #\space 100))
                 (list #\space 100))
           (list (list (list #\a 10) (list #\b 20) (list #\c 25)
                       (list #\d 30) (list #\e 40) (list #\f 50)
                       (list #\space -1))
                 (list #\f 50))
           ))
         (letter-htable (make-hash-table))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (a-list)
          (begin
            (hash-clear! letter-htable)

            (let ((hash-list (list-ref a-list 0))
                  (shouldbe (list-ref a-list 1)))
              (begin
                (for-each
                 (lambda (b-list)
                   (begin
                     (let ((key (list-ref b-list 0))
                           (value (list-ref b-list 1)))
                       (begin
                         (hash-set! letter-htable key value)
                         ))
                     )) hash-list)

                (let ((result
                       (find-max-count-in-hash letter-htable)))
                  (let ((err-1
                         (format
                          #f "~a :: (~a) error : hash-list=~a, "
                          sub-name test-label-index hash-list))
                        (err-2
                         (format
                          #f "shouldbe=~a, result=~a"
                          shouldbe result)))
                    (begin
                      (unittest2:assert?
                       (equal? shouldbe result)
                       sub-name
                       (string-append err-1 err-2)
                       result-hash-table)
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define-syntax max-value-from-hash-macro
  (syntax-rules ()
    ((max-value-from-hash-macro
      letter-htable result-list)
     (begin
       (let ((a-result
              (find-max-count-in-hash letter-htable)))
         (let ((key-char (list-ref a-result 0))
               (key-count (list-ref a-result 1)))
           (begin
             (set!
              result-list (cons a-result result-list))
             (hash-set!
              letter-htable key-char -1)
             )))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (top-three-letters letter-htable)
  (begin
    (let ((result-list (list)))
      (begin
        (max-value-from-hash-macro
         letter-htable result-list)

        (max-value-from-hash-macro
         letter-htable result-list)

        (max-value-from-hash-macro
         letter-htable result-list)

        (reverse result-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-top-three-letters-1 result-hash-table)
 (begin
   (let ((sub-name "test-top-three-letters-1")
         (test-list
          (list
           (list (list (list #\a 10) (list #\b 20) (list #\c 25)
                       (list #\d 30) (list #\e 40) (list #\f 50)
                       (list #\space 100))
                 (list (list #\space 100) (list #\f 50) (list #\e 40)))
           (list (list (list #\a 10) (list #\b 20) (list #\c 25)
                       (list #\d 30) (list #\e 40) (list #\f 50)
                       (list #\space -1))
                 (list (list #\f 50) (list #\e 40) (list #\d 30)))
           ))
         (letter-htable (make-hash-table))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (a-list)
          (begin
            (hash-clear! letter-htable)

            (let ((hash-list (list-ref a-list 0))
                  (shouldbe (list-ref a-list 1)))
              (begin
                (for-each
                 (lambda (b-list)
                   (begin
                     (let ((key (list-ref b-list 0))
                           (value (list-ref b-list 1)))
                       (begin
                         (hash-set! letter-htable key value)
                         ))
                     )) hash-list)

                (let ((result
                       (top-three-letters letter-htable)))
                  (let ((err-1
                         (format
                          #f "~a :: (~a) error : hash-list=~a, "
                          sub-name test-label-index hash-list))
                        (err-2
                         (format
                          #f "shouldbe=~a, result=~a"
                          shouldbe result)))
                    (begin
                      (unittest2:assert?
                       (equal? shouldbe result)
                       sub-name
                       (string-append err-1 err-2)
                       result-hash-table)
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (score-results
         letter-htable total-num top-chars-list-list)
  (begin
    (let ((score 0.0))
      (begin
        (for-each
         (lambda (a-list)
           (begin
             (let ((key-char (list-ref a-list 0))
                   (key-pcnt (list-ref a-list 1)))
               (let ((lcount
                      (hash-ref letter-htable key-char 0.0)))
                 (let ((lscore
                        (abs (- key-pcnt (/ lcount total-num)))))
                   (begin
                     (set! score (+ score lscore))
                     ))
                 ))
             )) top-chars-list-list)
        score
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (find-top-three-scores scores-htable)
  (begin
    (let ((char-0 #f)
          (min-score-0 -1)
          (char-1 #f)
          (min-score-1 -1)
          (char-2 #f)
          (min-score-2 -1)
          (result-list (list)))
      (begin
        (hash-for-each
         (lambda (key value)
           (begin
             (cond
              ((not (equal? (member 0 key) #f))
               (begin
                 (if (or (< min-score-0 0)
                         (<= value min-score-0))
                     (begin
                       (set! char-0 (list-ref key 0))
                       (set! min-score-0 value)
                       ))
                 ))
              ((not (equal? (member 1 key) #f))
               (begin
                 (if (or (< min-score-1 0)
                         (<= value min-score-1))
                     (begin
                       (set! char-1 (list-ref key 0))
                       (set! min-score-1 value)
                       ))
                 ))
              ((not (equal? (member 2 key) #f))
               (begin
                 (if (or (< min-score-2 2)
                         (<= value min-score-2))
                     (begin
                       (set! char-2 (list-ref key 0))
                       (set! min-score-2 value)
                       ))
                 )))
             )) scores-htable)

        (list char-0 char-1 char-2)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (unencrypt-text coded-text-list three-chars-list)
  (begin
    (let ((text-len (length coded-text-list))
          (result-list (list)))
      (begin
        (do ((ii 0 (1+ ii)))
            ((>= ii text-len))
          (begin
            (let ((decode-char
                   (list-ref three-chars-list (modulo ii 3)))
                  (encrypt-int
                   (list-ref coded-text-list ii)))
              (let ((decode-int
                     (char->integer decode-char)))
                (let ((translated-int
                       (logxor encrypt-int decode-int)))
                  (let ((translated-char
                         (integer->char translated-int)))
                    (begin
                      (set!
                       result-list (cons translated-char result-list))
                      )))
                ))
            ))

        (reverse result-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
;;; cipher.txt is a comma separated file
(define (get-ints-from-file filename)
  (begin
    (let ((int-list (list)))
      (begin
        (if (file-exists? filename)
            (begin
              (with-input-from-file filename
                (lambda ()
                  (begin
                    (do ((line
                          (ice-9-rdelim:read-delimited ",\n\r")
                          (ice-9-rdelim:read-delimited ",\n\r")))
                        ((eof-object? line))
                      (begin
                        (if (not (eof-object? line))
                            (begin
                              (let ((this-num (string->number line)))
                                (begin
                                  (if (not (equal? this-num #f))
                                      (begin
                                        (set!
                                         int-list
                                         (cons this-num int-list))
                                        ))
                                  ))
                              ))
                        ))
                    )))
              (reverse int-list)
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
;;; ascii code limits A - 65, Z - 90, a - 97, z - 122
(define (sub-main-loop filename top-chars-list-list)
  (begin
    (let ((max-score 0)
          (max-chars-list (list))
          (max-password-list (list))
          (encrypted-list
           (get-ints-from-file filename))
          (letters-htable (make-hash-table))
          (scores-htable (make-hash-table))
          (min-ascii 97)
          (max-ascii 122))
      (let ((total-num
             (length encrypted-list)))
        (begin
          (do ((ii min-ascii (+ ii 1)))
              ((> ii max-ascii))
            (begin
              (let ((char-1 (integer->char ii)))
                (begin
                  (hash-clear! letters-htable)
                  (populate-frequency-hash!
                   letters-htable char-1 0 3 encrypted-list)
                  (let ((this-score
                         (score-results
                          letters-htable total-num
                          top-chars-list-list)))
                    (begin
                      (hash-set! scores-htable (list char-1 0) this-score)
                      ))

                  (hash-clear! letters-htable)
                  (populate-frequency-hash!
                   letters-htable char-1 1 3 encrypted-list)

                  (let ((this-score
                         (score-results
                          letters-htable total-num
                          top-chars-list-list)))
                    (begin
                      (hash-set!
                       scores-htable (list char-1 1) this-score)
                      ))

                  (hash-clear! letters-htable)
                  (populate-frequency-hash!
                   letters-htable char-1 2 3 encrypted-list)

                  (let ((this-score
                         (score-results
                          letters-htable total-num
                          top-chars-list-list)))
                    (begin
                      (hash-set!
                       scores-htable (list char-1 2) this-score)
                      ))
                  ))
              ))

          (let ((result-list
                 (find-top-three-scores scores-htable)))
            (let ((unencrypted-text-list
                   (unencrypt-text
                    encrypted-list result-list)))
              (begin
                (newline)
                (display
                 (format
                  #f "best three-letter pass-phrase found = ~a~%"
                  (list->string result-list)))
                (newline)
                (display
                 (format
                  #f "unencrypted text:~%'~a'~%"
                  (list->string unencrypted-text-list)))

                (newline)
                (display
                 (format
                  #f "sum of ascii codes = ~a~%"
                  (srfi-1:fold
                   + 0
                   (map char->integer
                        unencrypted-text-list))))
                (force-output)
                )))
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Each character on a computer is "))
    (display
     (format #f "assigned a unique code~%"))
    (display
     (format #f "and the preferred standard is ASCII "))
    (display
     (format #f "(American Standard Code~%"))
    (display
     (format #f "for Information Interchange). For "))
    (display
     (format #f "example, uppercase A = 65,~%"))
    (display
     (format #f "asterisk (*) = 42, and lowercase "))
    (display
     (format #f "k = 107.~%"))
    (newline)
    (display
     (format #f "A modern encryption method is to take "))
    (display
     (format #f "a text file, convert~%"))
    (display
     (format #f "the bytes to ASCII, then XOR each byte "))
    (display
     (format #f "with a given value,~%"))
    (display
     (format #f "taken from a secret key. The advantage "))
    (display
     (format #f "with the XOR~%"))
    (display
     (format #f "function is that using the same "))
    (display
     (format #f "encryption key on~%"))
    (display
     (format #f "the cipher text, restores the plain "))
    (display
     (format #f "text; for example,~%"))
    (display
     (format #f "65 XOR 42 = 107, then "))
    (display
     (format #f "107 XOR 42 = 65.~%"))
    (newline)
    (display
     (format #f "For unbreakable encryption, the key is the "))
    (display
     (format #f "same length as the~%"))
    (display
     (format #f "plain text message, and the key is made "))
    (display
     (format #f "up of random bytes.~%"))
    (display
     (format #f "The user would keep the encrypted "))
    (display
     (format #f "message and the encryption~%"))
    (display
     (format #f "key in different locations, and without "))
    (display
     (format #f "both 'halves', it is~%"))
    (display
     (format #f "impossible to decrypt the message.~%"))
    (newline)
    (display
     (format #f "Unfortunately, this method is impractical "))
    (display
     (format #f "for most users, so~%"))
    (display
     (format #f "the modified method is to use a password "))
    (display
     (format #f "as a key. If~%"))
    (display
     (format #f "The password is shorter than the message, "))
    (display
     (format #f "which is likely,~%"))
    (display
     (format #f "the key is repeated cyclically throughout "))
    (display
     (format #f "the message. The~%"))
    (display
     (format #f "balance for this method is using a "))
    (display
     (format #f "sufficiently long password~%"))
    (display
     (format #f "key for security, but short enough to "))
    (display
     (format #f "be memorable.~%"))
    (newline)
    (display
     (format #f "Your task has been made easy, as the "))
    (display
     (format #f "encryption key consists~%"))
    (display
     (format #f "of three lower case characters. Using "))
    (display
     (format #f "cipher1.txt (right click~%"))
    (display
     (format #f "and 'Save Link/Target As...'), a file "))
    (display
     (format #f "containing the encrypted~%"))
    (display
     (format #f "ASCII codes, and the knowledge that the "))
    (display
     (format #f "plain text must contain~%"))
    (display
     (format #f "common English words, decrypt the "))
    (display
     (format #f "message and find~%"))
    (display
     (format #f "the sum of the ASCII values in the "))
    (display
     (format #f "original text.~%"))
    (newline)
    (display
     (format #f "The solution method was found from the "))
    (display
     (format #f "article at~%"))
    (display
     (format #f "https://euler.stephan-brumme.com/59/~%"))
    (newline)
    (display
     (format #f "It tries all 26 different letters on the "))
    (display
     (format #f "encrypted text, and finds~%"))
    (display
     (format #f "the top letters that produces the most "))
    (display
     (format #f "commonly used letters.~%"))
    (newline)
    (display
     (format #f "see the website~%"))
    (display
     (format #f "https://en.wikipedia.org/wiki/Letter_frequency~%"))
    (display
     (format #f "for the most commonly used letters english.~%"))
    (display
     (format #f "see https://projecteuler.net/problem=59~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((fname "cipher1.txt")
          (top-four-letters
           (list (list #\e 0.127) (list #\t 0.091)
                 (list #\a 0.082) (list #\o 0.075)
                 (list #\i 0.069) (list #\n 0675))))
      (begin
        (sub-main-loop fname top-four-letters)
        (newline)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 59 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 56                                     ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated June 16, 2022                                ###
;;;###                                                       ###
;;;###  updated March 5, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### srfi-1 for fold function
(use-modules ((srfi srfi-1)
              :renamer (symbol-prefix-proc 'srfi-1:)))

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### digits-module for split-digits-list function
(use-modules ((digits-module)
              :renamer (symbol-prefix-proc 'digits-module:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (digit-sum this-num)
  (begin
    (let ((dlist
           (digits-module:split-digits-list this-num)))
      (let ((dsum (srfi-1:fold + 0 dlist)))
        (begin
          dsum
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-digit-sum-1 result-hash-table)
 (begin
   (let ((sub-name "test-digit-sum-1")
         (test-list
          (list
           (list 1 1) (list 2 2) (list 3 3)
           (list 10 1) (list 11 2) (list 12 3)
           (list 100 1) (list 101 2) (list 102 3)
           (list 103 4) (list 104 5) (list 123 6)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((test-num (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result (digit-sum test-num)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : test-num=~a, "
                        sub-name test-label-index test-num))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (integer-power this-number this-exponent)
  (begin
    (cond
     ((= this-exponent 0)
      (begin
        1
        ))
     ((= this-exponent 1)
      (begin
        this-number
        ))
     ((< this-exponent 0)
      (begin
        -1
        ))
     (else
      (begin
        (let ((result-num this-number)
              (max-iter (- this-exponent 1)))
          (begin
            (do ((ii 0 (+ ii 1)))
                ((>= ii max-iter))
              (begin
                (set! result-num (* result-num this-number))
                ))
            result-num
            ))
        )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-integer-power-1 result-hash-table)
 (begin
   (let ((sub-name "test-integer-power-1")
         (test-list
          (list
           (list 10 0 1) (list 11 0 1) (list 12 0 1)
           (list 10 1 10) (list 11 1 11) (list 12 1 12)
           (list 10 2 100) (list 11 2 121) (list 12 2 144)
           (list 2 2 4) (list 2 3 8)
           (list 2 4 16) (list 2 5 32)
           (list 2 6 64) (list 2 7 128)
           (list 2 8 256) (list 2 9 512)
           (list 2 10 1024) (list 2 11 2048)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-num (list-ref alist 0))
                  (test-exp (list-ref alist 1))
                  (shouldbe-num (list-ref alist 2)))
              (let ((result-num
                     (integer-power test-num test-exp)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : test-num=~a, "
                        sub-name test-label-index test-num))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe-num result-num)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-num result-num)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop start-num max-num)
  (begin
    (let ((max-aa 0)
          (max-bb 0)
          (max-aa-bb 0)
          (max-digit-sum 0))
      (begin
        (do ((aa start-num (+ aa 1)))
            ((> aa max-num))
          (begin
            (do ((bb start-num (+ bb 1)))
                ((> bb max-num))
              (begin
                (let ((this-num (integer-power aa bb)))
                  (let ((dsum (digit-sum this-num)))
                    (begin
                      (if (> dsum max-digit-sum)
                          (begin
                            (set! max-aa aa)
                            (set! max-bb bb)
                            (set! max-aa-bb this-num)
                            (set! max-digit-sum dsum)
                            ))
                      )))
                ))
            ))

        (newline)
        (display
         (ice-9-format:format
          #f "The maximum digit sum found = ~:d~%"
          max-digit-sum))
        (display
         (ice-9-format:format
          #f "  a^b = ~:d^~:d = ~:d~%"
          max-aa max-bb max-aa-bb))
        (display
         (ice-9-format:format
          #f "  where a and b are between ~:d and ~:d~%"
          start-num max-num))
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "A googol (10^100) is a massive number: "))
    (display
     (format #f "one followed by~%"))
    (display
     (format #f "one-hundred zeros; 100^100 is almost "))
    (display
     (format #f "unimaginably large: one~%"))
    (display
     (format #f "followed by two-hundred zeros. Despite "))
    (display
     (format #f "their size, the sum~%"))
    (display
     (format #f "of the digits in each number is only 1.~%"))
    (newline)
    (display
     (format #f "Considering natural numbers of the form, "))
    (display
     (format #f "a^b, where a, b < 100,~%"))
    (display
     (format #f "what is the maximum digital sum?~%"))
    (display
     (format #f "see https://projecteuler.net/problem=56~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((start-num 1)
          (max-num 10))
      (begin
        (sub-main-loop start-num max-num)
        ))

    (newline)
    (let ((start-num 1)
          (max-num 100))
      (begin
        (sub-main-loop start-num max-num)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 56 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (load "digits-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 53                                     ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated June 16, 2022                                ###
;;;###                                                       ###
;;;###  updated March 5, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (factorial nn)
  (begin
    (cond
     ((< nn 0)
      (begin
        #f
        ))
     ((= nn 0)
      (begin
        1
        ))
     (else
      (begin
        (let ((result 1))
          (begin
            (do ((ii 1 (1+ ii)))
                ((> ii nn))
              (begin
                (set! result (* ii result))
                ))
            result
            ))
        )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-factorial-1 result-hash-table)
 (begin
   (let ((sub-name "test-factorial-1")
         (test-list
          (list
           (list 0 1) (list 1 1) (list 2 2)
           (list 3 6) (list 4 24) (list 5 120)
           (list 6 720)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-num (list-ref alist 0))
                  (shouldbe-num (list-ref alist 1)))
              (let ((result-num (factorial test-num)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : test-num=~a, "
                        sub-name test-label-index test-num))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe-num result-num)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-num result-num)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (combinatorial nn rr)
  (define (local-semi-fac max-num min-num)
    (begin
      (let ((prod 1)
            (diff (- max-num min-num)))
        (begin
          (do ((ii (+ diff 1) (+ ii 1)))
              ((> ii max-num))
            (begin
              (set! prod (* prod ii))
              ))
          prod
          ))
      ))
  (begin
    (cond
     ((< nn 0)
      (begin
        #f
        ))
     ((= nn 0)
      (begin
        1
        ))
     (else
      (begin
        (let ((numerator (local-semi-fac nn rr))
              (denominator (factorial rr)))
          (begin
            (if (and (number? numerator)
                     (number? denominator))
                (begin
                  (let ((cnr
                         (euclidean/ numerator denominator)))
                    (begin
                      cnr
                      )))
                (begin
                  #f
                  ))
            ))
        )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-combinatorial-1 result-hash-table)
 (begin
   (let ((sub-name "test-combinatorial-1")
         (test-list
          (list
           (list 1 1 1) (list 1 0 1)
           (list 2 0 1) (list 2 1 2) (list 2 2 1)
           (list 3 0 1) (list 3 1 3) (list 3 2 3) (list 3 3 1)
           (list 4 0 1) (list 4 1 4) (list 4 2 6) (list 4 3 4) (list 4 4 1)
           (list 5 0 1) (list 5 1 5) (list 5 2 10) (list 5 3 10)
           (list 5 4 5) (list 5 5 1)
           (list 6 0 1) (list 6 1 6) (list 6 2 15) (list 6 3 20)
           (list 6 4 15) (list 6 5 6) (list 6 6 1)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-nn (list-ref alist 0))
                  (test-rr (list-ref alist 1))
                  (shouldbe-num (list-ref alist 2)))
              (let ((result-num (combinatorial test-nn test-rr)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "test-nn=~a, test-rr=~a, "
                        test-nn test-rr))
                      (err-3
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe-num result-num)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-num result-num)
                     sub-name
                     (string-append
                      err-1 err-2 err-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop max-num min-value debug-flag)
  (begin
    (let ((count 0)
          (comb-list (list)))
      (begin
        (do ((nn 1 (+ nn 1)))
            ((> nn max-num))
          (begin
            (do ((rr 1 (+ rr 1)))
                ((>= rr nn))
              (begin
                (let ((this-value
                       (combinatorial nn rr)))
                  (begin
                    (if (>= this-value min-value)
                        (begin
                          (set! count (+ count 1))
                          (let ((this-list
                                 (list nn rr this-value)))
                            (begin
                              (if (equal? debug-flag #t)
                                  (begin
                                    (set!
                                     comb-list
                                     (cons this-list comb-list))
                                    ))
                              ))
                          ))
                    ))
                ))
            ))

        (if (and (equal? debug-flag #t)
                 (or (not (list? comb-list))
                     (< (length comb-list) 1)))
            (begin
              (display
               (ice-9-format:format
                #f "no binomial factors greater than~%"))
              (display
               (ice-9-format:format
                #f "~:d found for n <= ~:d~%"
                min-value max-num)))
            (begin
              (display
               (ice-9-format:format
                #f "found ~:d binomial factors (greater than~%"
                count))
              (display
               (ice-9-format:format
                #f "~:d, for n <= ~:d)~%"
                min-value max-num))
              (if (equal? debug-flag #t)
                  (begin
                    (let ((counter 0))
                      (begin
                        (for-each
                         (lambda(this-list)
                           (begin
                             (let ((nn (list-ref this-list 0))
                                   (rr (list-ref this-list 1))
                                   (value (list-ref this-list 2)))
                               (begin
                                 (set! counter (+ counter 1))
                                 (display
                                  (ice-9-format:format
                                   #f "  (~:d) : C(~:d, ~:d) = ~:d~%"
                                   counter nn rr value))
                                 ))
                             )) (reverse comb-list))
                        ))
                    ))
              (force-output)
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "There are exactly ten ways of selecting "))
    (display
     (format #f "three from five,~%"))
    (display
     (format #f "12345:  123, 124, 125, 134, 135, "))
    (display
     (format #f "145, 234, 235, 245,~%"))
    (display
     (format #f "and 345~%"))
    (newline)
    (display
     (format #f "In combinatorics, we use the notation, "))
    (display
     (format #f "5C3 = 10.~%"))
    (newline)
    (display
     (format #f "In general,~%"))
    (display
     (format #f "nCr =  n!/r!(nr)!~%"))
    (display
     (format #f "where r <= n, n! = nx(n1)x...3x2x1, "))
    (display
     (format #f "and 0! = 1.~%"))
    (newline)
    (display
     (format #f "It is not until n = 23, that a value "))
    (display
     (format #f "exceeds one-million:~%"))
    (display
     (format #f "23C10 = 1144066.~%"))
    (newline)
    (display
     (format #f "How many, not necessarily distinct, "))
    (display
     (format #f "values of  nCr,~%"))
    (display
     (format #f "for 1 <= n <= 100, are greater than "))
    (display
     (format #f "one-million?~%"))
    (display
     (format #f "see https://projecteuler.net/problem=53~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((end-num 23)
          (min-value 1000000)
          (debug-flag #t))
      (begin
        (sub-main-loop end-num min-value debug-flag)
        ))

    (newline)
    (let ((end-num 100)
          (min-value 1000000)
          (debug-flag #f))
      (begin
        (sub-main-loop end-num min-value debug-flag)
        ))

    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 53 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

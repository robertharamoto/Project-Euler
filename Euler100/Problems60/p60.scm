#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 60                                     ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated June 16, 2022                                ###
;;;###                                                       ###
;;;###  updated March 6, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### srfi-1 for fold function
(use-modules ((srfi srfi-1)
              :renamer (symbol-prefix-proc 'srfi-1:)))

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### prime-module for is-prime-array? function
(use-modules ((prime-module)
              :renamer (symbol-prefix-proc 'prime-module:)))

;;;### digits-module for split-digits-list function
(use-modules ((digits-module)
              :renamer (symbol-prefix-proc 'digits-module:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
;;; make a list of primes less than or equal to n
;;; sieve of eratosthenes method
(define (make-prime-list max-num)
  (begin
    (let ((intermediate-array (make-array 0 (1+ max-num)))
          (result-list (list)))
      (begin
        (do ((ii 0 (1+ ii)))
            ((> ii max-num))
          (begin
            (array-set! intermediate-array ii ii)
            ))

        (do ((ii 2 (1+ ii)))
            ((> ii max-num))
          (begin
            (let ((ii-num (array-ref intermediate-array ii)))
              (begin
                (if (equal? ii ii-num)
                    (begin
                      (set! result-list (cons ii result-list))

                      (do ((jj (+ ii ii) (+ jj ii)))
                          ((> jj max-num))
                        (begin
                          (array-set! intermediate-array -1 jj)
                          ))
                      ))
                ))
            ))
        (reverse result-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (p60-assert-lists-equal
         shouldbe-list result-list
         sub-name
         err-start
         result-hash-table)
  (begin
    (let ((shouldbe-length (length shouldbe-list))
          (result-length (length result-list))
          (err-1
           (format
            #f ", shouldbe-list=~a, result-list=~a"
            shouldbe-list result-list)))
      (let ((err-2
             (format
              #f ", lengths not equal, shouldbe=~a, result=~a"
              shouldbe-length result-length)))
        (begin
          (unittest2:assert?
           (equal? shouldbe-length result-length)
           sub-name
           (string-append
            err-start err-1 err-2)
           result-hash-table)

          (if (> shouldbe-length 0)
              (begin
                (for-each
                 (lambda (s-elem)
                   (begin
                     (let ((mflag
                            (member s-elem result-list))
                           (err-3
                            (format
                             #f ", missing element ~a"
                             s-elem)))
                       (begin
                         (unittest2:assert?
                          (not (equal? mflag #f))
                          sub-name
                          (string-append
                           err-start err-1 err-3)
                          result-hash-table)
                         ))
                     )) shouldbe-list)
                ))
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-make-prime-list-1 result-hash-table)
 (begin
   (let ((sub-name "test-make-prime-list-1")
         (test-list
          (list
           (list 2 (list 2))
           (list 3 (list 2 3))
           (list 4 (list 2 3))
           (list 5 (list 2 3 5))
           (list 6 (list 2 3 5))
           (list 7 (list 2 3 5 7))
           (list 8 (list 2 3 5 7))
           (list 9 (list 2 3 5 7))
           (list 10 (list 2 3 5 7))
           (list 11 (list 2 3 5 7 11))
           (list 13 (list 2 3 5 7 11 13))
           (list 17 (list 2 3 5 7 11 13 17))
           (list 19 (list 2 3 5 7 11 13 17 19))
           (list 23 (list 2 3 5 7 11 13 17 19 23))
           (list 31 (list 2 3 5 7 11 13 17 19 23 29 31))
           (list 40 (list 2 3 5 7 11 13 17 19 23 29 31 37))
           (list
            50 (list 2 3 5 7 11 13 17 19 23 29 31 37 41 43 47))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((test-num (list-ref this-list 0))
                  (shouldbe-list (list-ref this-list 1)))
              (let ((result-list (make-prime-list test-num)))
                (let ((err-start
                       (format
                        #f "~a :: (~a) error : test-num=~a"
                        sub-name test-label-index test-num)))
                  (begin
                    (p60-assert-lists-equal
                     shouldbe-list result-list
                     sub-name
                     err-start
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (concat-two-prime-lists plist1 plist2 prime-array)
  (begin
    (let ((dlist1 (append plist1 plist2))
          (dlist2 (append plist2 plist1)))
      (let ((num1
             (digits-module:digit-list-to-number dlist1))
            (num2
             (digits-module:digit-list-to-number dlist2)))
        (let ((pflag1
               (prime-module:is-array-prime? num1 prime-array))
              (pflag2
               (prime-module:is-array-prime? num2 prime-array)))
          (begin
            (if (and (equal? pflag1 #t)
                     (equal? pflag2 #t))
                (begin
                  (sort (list num1 num2) <))
                (begin
                  #f
                  ))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-concat-two-prime-lists-1 result-hash-table)
 (begin
   (let ((sub-name "test-concat-two-prime-lists-1")
         (test-list
          (list
           (list (list 3) (list 7) (list 37 73))
           (list (list 3) (list 1 0 9) (list 1093 3109))
           (list (list 7) (list 1 0 9) (list 1097 7109))
           (list (list 2) (list 1 0 9) #f)
           (list (list 5) (list 7) #f)
           ))
         (prime-array
          (list->array 1 (make-prime-list 10)))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((plist1 (list-ref alist 0))
                  (plist2 (list-ref alist 1))
                  (shouldbe-list (list-ref alist 2)))
              (let ((result-list
                     (concat-two-prime-lists
                      plist1 plist2 prime-array)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "plist1=~a, plist2=~a"
                        plist1 plist2)))
                  (begin
                    (if (and
                         (list? shouldbe-list)
                         (list? result-list))
                        (begin
                          (p60-assert-lists-equal
                           shouldbe-list result-list
                           sub-name
                           (string-append
                            err-1 err-2)
                           result-hash-table))
                        (begin
                          (let ((err-3
                                 (format
                                  #f ", shouldbe=~a, result=~a"
                                  shouldbe-list result-list)))
                            (begin
                              (unittest2:assert?
                               (equal? shouldbe-list result-list)
                               sub-name
                               (string-append
                                err-1 err-2 err-3)
                               result-hash-table)
                              ))
                          ))
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (populate-prime-digits-hash!
         prime-digits-htable prime-array prime-length)
  (begin
    (do ((ii 0 (1+ ii)))
        ((>= ii prime-length))
      (begin
        (let ((aprime (array-ref prime-array ii)))
          (let ((alist
                 (digits-module:split-digits-list aprime)))
            (begin
              (hash-set! prime-digits-htable aprime alist)
              )))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (populate-valid-prime-pairs-hash!
         valid-prime-pairs-htable prime-digits-htable prime-array)
  (begin
    (let ((plen
           (car (array-dimensions prime-array))))
      (begin
        (hash-clear! valid-prime-pairs-htable)

        (do ((ii 0 (1+ ii)))
            ((>= ii plen))
          (begin
            (let ((ii-prime (array-ref prime-array ii)))
              (let ((ii-dlist
                     (hash-ref
                      prime-digits-htable ii-prime (list))))
                (begin
                  (do ((jj (1+ ii) (1+ jj)))
                      ((>= jj plen))
                    (begin
                      (let ((jj-prime
                             (array-ref prime-array jj)))
                        (let ((jj-dlist
                               (hash-ref
                                prime-digits-htable jj-prime (list))))
                          (let ((pp-flag
                                 (concat-two-prime-lists
                                  ii-dlist jj-dlist prime-array)))
                            (begin
                              (if (not (equal? pp-flag #f))
                                  (begin
                                    (hash-set!
                                     valid-prime-pairs-htable
                                     (list ii-prime jj-prime) pp-flag)
                                    ))
                              ))
                          ))
                      ))
                  )))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-populate-valid-prime-pairs-hash-1 result-hash-table)
 (begin
   (let ((sub-name "test-populate-valid-prime-pairs-hash-1")
         (test-list
          (list
           (list (list 3 7) (list 37 73))
           (list (list 3 11) (list 113 311))
           (list (list 3 17) (list 173 317))
           (list (list 13 19) (list 1319 1913))
           (list (list 2 3) #f)
           (list (list 2 5) #f)
           (list (list 3 5) #f)
           (list (list 5 7) #f)
           ))
         (valid-prime-pairs-htable (make-hash-table 10))
         (prime-digits-htable (make-hash-table 10))
         (prime-array
          (list->array 1 (make-prime-list 20)))
         (test-label-index 0))
     (begin
       (let ((prime-length
              (car (array-dimensions prime-array))))
         (begin
           (populate-prime-digits-hash!
            prime-digits-htable
            prime-array prime-length)

           (populate-valid-prime-pairs-hash!
            valid-prime-pairs-htable
            prime-digits-htable prime-array)
           ))

       (for-each
        (lambda (alist)
          (begin
            (let ((plist1 (list-ref alist 0))
                  (shouldbe-list (list-ref alist 1)))
              (let ((result-list
                     (hash-ref
                      valid-prime-pairs-htable plist1 #f)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : plist1=~a, "
                        sub-name test-label-index plist1))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe-list result-list)))
                  (begin
                    (if (and
                         (list? shouldbe-list)
                         (list? result-list))
                        (begin
                          (p60-assert-lists-equal
                           shouldbe-list result-list
                           sub-name
                           err-1
                           result-hash-table))
                        (begin
                          (unittest2:assert?
                           (equal? shouldbe-list result-list)
                           sub-name
                           (string-append err-1 err-2)
                           result-hash-table)
                          ))
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (prime-concats-to-other-primes
         prime-acc-list this-prime this-pdlist
         prime-array prime-digits-htable
         valid-prime-pairs-htable)
  (begin
    (let ((inner-break-flag #f)
          (ok-flag #t)
          (plen (length prime-acc-list))
          (concatenated-list (list)))
      (begin
        (do ((jj 0 (1+ jj)))
            ((or (>= jj plen)
                 (equal? inner-break-flag #t)))
          (begin
            (let ((tmp-prime (list-ref prime-acc-list jj)))
              (let ((prime-key-list
                     (sort (list this-prime tmp-prime) <)))
                (let ((prime-pair-list
                       (hash-ref
                        valid-prime-pairs-htable
                        prime-key-list #f)))
                  (begin
                    (cond
                     ((equal? prime-pair-list #f)
                      (begin
                        (set! ok-flag #f)
                        (set! inner-break-flag #t)
                        ))
                     (else
                      (begin
                        (set!
                         concatenated-list
                         (cons
                          (append
                           prime-key-list prime-pair-list)
                          concatenated-list))
                        )))
                    ))
                ))
            ))
        (if (equal? ok-flag #t)
            (begin
              concatenated-list)
            (begin
              #f))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-prime-concats-to-other-primes-1 result-hash-table)
 (begin
   (let ((sub-name "test-prime-concats-to-other-primes-1")
         (test-list
          (list
           (list (list 2) 3 (list 3) #f)
           (list (list 7) 3 (list 3) (list (list 3 7 37 73)))
           (list (list 3) 7 (list 7) (list (list 3 7 37 73)))
           (list (list 109) 3 (list 3) (list (list 3 109 1093 3109)))
           ))
         (valid-prime-pairs-htable (make-hash-table))
         (prime-digits-htable (make-hash-table))
         (prime-list (make-prime-list 200))
         (test-label-index 0))
     (let ((prime-array (list->array 1 prime-list))
           (prime-length (length prime-list)))
       (begin
         (populate-prime-digits-hash!
          prime-digits-htable prime-array prime-length)

         (populate-valid-prime-pairs-hash!
          valid-prime-pairs-htable prime-digits-htable prime-array)

         (for-each
          (lambda (this-list)
            (begin
              (let ((prime-acc-list (list-ref this-list 0))
                    (this-prime (list-ref this-list 1))
                    (this-pdlist (list-ref this-list 2))
                    (shouldbe (list-ref this-list 3)))
                (let ((result
                       (prime-concats-to-other-primes
                        prime-acc-list this-prime this-pdlist
                        prime-array prime-digits-htable
                        valid-prime-pairs-htable)))
                  (let ((err-1
                         (format
                          #f "~a :: (~a) error : prime-acc-list=~a, "
                          sub-name test-label-index prime-acc-list))
                        (err-2
                         (format
                          #f "this-prime=~a, this-pdlist=~a"
                          this-prime this-pdlist))
                        (err-3
                         (format
                          #f "shouldbe=~a, result=~a"
                          shouldbe result)))
                    (begin
                      (if (and
                           (list? shouldbe)
                           (list? result))
                          (begin
                            (let ((s-len (length shouldbe))
                                  (r-len (length result)))
                              (let ((err-4
                                     (format
                                      #f ", shouldbe length=~a, result length=~a"
                                      s-len r-len))
                                    (err-start
                                     (string-append err-1 err-2)))
                                (begin
                                  (unittest2:assert?
                                   (equal? s-len r-len)
                                   sub-name
                                   (string-append
                                    err-start err-3 err-4)
                                   result-hash-table)

                                  (do ((ii 0 (1+ ii)))
                                      ((>= ii s-len))
                                    (begin
                                      (let ((s-elem-list
                                             (list-ref shouldbe ii))
                                            (r-elem-list
                                             (list-ref result ii)))
                                        (begin
                                          (p60-assert-lists-equal
                                           s-elem-list
                                           r-elem-list
                                           sub-name
                                           err-start
                                           result-hash-table)
                                          ))
                                      ))
                                  ))
                              ))
                          (begin
                            (let ((err-start
                                   (string-append err-1 err-2 err-3)))
                              (begin
                                (unittest2:assert?
                                 (equal? shouldbe result)
                                 sub-name
                                 err-start
                                 result-hash-table)
                                ))
                            ))
                      ))
                  ))
              (set! test-label-index (1+ test-label-index))
              )) test-list)
         )))
   ))

;;;#############################################################
;;;#############################################################
(define-syntax process-ii-index-macro
  (syntax-rules ()
    ((process-ii-index-macro
      depth max-depth ii max-index
      prime-acc-list this-prime this-pdlist
      prime-array prime-digits-htable
      valid-prime-pairs-htable
      min-sum-so-far master-acc-list quints-list-lists)
     (begin
       (let ((this-pdlist
              (hash-ref prime-digits-htable this-prime)))
         (begin
           ;;; see if this-prime concats with previous primes
           (let ((tmp1
                  (prime-concats-to-other-primes
                   prime-acc-list this-prime this-pdlist
                   prime-array prime-digits-htable
                   valid-prime-pairs-htable)))
             (begin
               (if (not (equal? tmp1 #f))
                   (begin
                     (let ((next-result-list
                            (rec-inner-loop
                             (+ depth 1) max-depth
                             (+ ii 1) max-index
                             prime-array prime-digits-htable
                             valid-prime-pairs-htable
                             (cons this-prime prime-acc-list)
                             (append quints-list-lists tmp1)
                             min-sum-so-far
                             master-acc-list)))
                       (begin
                         (if (and (list? next-result-list)
                                  (> (length next-result-list) 0))
                             (begin
                               (let ((next-sum (list-ref next-result-list 0)))
                                 (begin
                                   (if (or (< min-sum-so-far 0)
                                           (< next-sum min-sum-so-far))
                                       (begin
                                         (set! min-sum-so-far next-sum)
                                         (set! master-acc-list next-result-list)
                                         ))
                                   ))
                               ))
                         ))
                     ))
               ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (rec-inner-loop
         depth max-depth this-index max-index prime-array
         prime-digits-htable valid-prime-pairs-htable
         prime-acc-list quints-list-lists
         min-sum-so-far master-acc-list)
  (begin
    (if (>= depth max-depth)
        (begin
          (if (>= (length prime-acc-list) max-depth)
              (begin
                (let ((current-sum
                       (srfi-1:fold + 0 prime-acc-list)))
                  (begin
                    (if (or (<= min-sum-so-far 0)
                            (< current-sum min-sum-so-far))
                        (begin
                          (set!
                           master-acc-list
                           (list current-sum
                                 (sort prime-acc-list <)
                                 quints-list-lists))
                          ))
                    ))
                ))
          master-acc-list)
        (begin
          (let ((continue-loop-flag #t))
            (begin
              (do ((ii this-index (+ ii 1)))
                  ((or (>= ii max-index)
                       (equal? continue-loop-flag #f)))
                (begin
                  (let ((this-prime (array-ref prime-array ii)))
                    (let ((psum
                           (+ (srfi-1:fold + 0 prime-acc-list)
                              (* (- max-depth depth) this-prime))))
                      (begin
                        (if (and (> min-sum-so-far 0)
                                 (> psum min-sum-so-far))
                            (begin
                              (set! continue-loop-flag #f)
                              ))

                        (process-ii-index-macro
                         depth max-depth ii max-index
                         prime-acc-list this-prime this-pdlist
                         prime-array prime-digits-htable
                         valid-prime-pairs-htable
                         min-sum-so-far master-acc-list quints-list-lists)
                        )))
                  ))

              master-acc-list
              ))
          ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-recursive-loop nseq max-num debug-flag)
  (begin
    (let ((prime-list (sort (make-prime-list max-num) <)))
      (let ((prime-array (list->array 1 prime-list))
            (array-len (length prime-list))
            (prime-digits-htable (make-hash-table))
            (valid-prime-pairs-htable (make-hash-table))
            (prime-acc-list (list)))
        (begin
          (populate-prime-digits-hash!
           prime-digits-htable prime-array array-len)

          (populate-valid-prime-pairs-hash!
           valid-prime-pairs-htable prime-digits-htable prime-array)

          ;;; all variables initialized, so call recursion
          ;;; start from index=1 since we don't need to consider 2
          (let ((master-acc-list-list
                 (rec-inner-loop
                  0 nseq 1 array-len
                  prime-array prime-digits-htable
                  valid-prime-pairs-htable
                  (list) (list) -1 (list))))
            (let ((min-sum (list-ref master-acc-list-list 0))
                  (acc-list (list-ref master-acc-list-list 1))
                  (quints-list-lists (list-ref master-acc-list-list 2)))
              (begin
                (display
                 (ice-9-format:format
                  #f "~:d = the lowest sum for a set of "
                  min-sum))
                (display
                 (ice-9-format:format
                  #f "~:d primes, where any two~%"
                  nseq))
                (display
                 (ice-9-format:format
                  #f "primes concatenate to form a prime, "))
                (display
                 (ice-9-format:format
                  #f "(for primes less than ~:d)~%"
                  max-num))
                (display
                 (ice-9-format:format
                  #f "the primes are: ~a~%" acc-list))

                (if (equal? debug-flag #t)
                    (begin
                      (let ((squints-list-lists
                             (sort
                              quints-list-lists
                              (lambda (a b)
                                (begin
                                  (let ((a1-elem (car a))
                                        (b1-elem (car b)))
                                    (begin
                                      (if (= a1-elem b1-elem)
                                          (begin
                                            (let ((a2-elem (cadr a))
                                                  (b2-elem (cadr b)))
                                              (begin
                                                (< a2-elem b2-elem)
                                                )))
                                          (begin
                                            (< a1-elem b1-elem)
                                            ))
                                      ))
                                  ))
                              )))
                        (begin
                          (for-each
                           (lambda (this-list)
                             (begin
                               (display
                                (ice-9-format:format
                                 #f "    primes ~:d and ~:d concatenate to "
                                 (list-ref this-list 0)
                                 (list-ref this-list 1)))
                               (display
                                (ice-9-format:format
                                 #f "~:d and ~:d which are both prime.~%"
                                 (list-ref this-list 2)
                                 (list-ref this-list 3)))
                               )) squints-list-lists)
                          (newline)
                          (force-output)
                          ))
                      ))
                )))
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "The primes 3, 7, 109, and 673, are "))
    (display
     (format #f "quite remarkable. By~%"))
    (display
     (format #f "taking any two primes and concatenating "))
    (display
     (format #f "them in any order~%"))
    (display
     (format #f "the result will always be prime. For "))
    (display
     (format #f "example, taking 7 and~%"))
    (display
     (format #f "109, both 7109 and 1097 are prime. The "))
    (display
     (format #f "sum of these four~%"))
    (display
     (format #f "primes, 792, represents the lowest sum "))
    (display
     (format #f "for a set of~%"))
    (display
     (format #f "four primes with this property.~%"))
    (newline)
    (display
     (format #f "Find the lowest sum for a set of "))
    (display
     (format #f "five primes for which~%"))
    (display
     (format #f "any two primes concatenate to "))
    (display
     (format #f "produce another prime.~%"))
    (newline)
    (display
     (format #f "The key idea of reducing the possible "))
    (display
     (format #f "solutions is to~%"))
    (display
     (format #f "store valid prime pairs that concatenate "))
    (display
     (format #f "to primes. This was~%"))
    (display
     (format #f "described at https://euler.stephan-brumme.com/60/~%"))
    (display
     (format #f "This program takes around 2 minutes to "))
    (display
     (format #f "run.  It was re-written~%"))
    (display
     (format #f "in c++, and it completed in 20 seconds "))
    (display
     (format #f "(for primes less than 10,000).~%"))
    (display
     (format #f "This means I probably have an ok algorithm.~%"))
    (display
     (format #f "see https://projecteuler.net/problem=60~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((set-max 4)
          (max-num 1000)
          (debug-flag #t))
      (begin
        (main-recursive-loop set-max max-num debug-flag)
        ))

    (newline)
    (let ((set-max 5)
          (max-num 10000)
          (debug-flag #f))
      (begin
        (main-recursive-loop set-max max-num debug-flag)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 60 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (load "prime-module-tests-1.scm")
                 (load "digits-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

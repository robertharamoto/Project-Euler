#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 25                                     ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated June 11, 2022                                ###
;;;###                                                       ###
;;;###  updated March 4, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### digits-module for split-digits-list function
(use-modules ((digits-module)
              :renamer (symbol-prefix-proc 'digits-module:)))

;;;### timer-module for time-code function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (number-of-digits this-num)
  (begin
    (let ((dlist
           (digits-module:split-digits-list this-num)))
      (let ((ndigits (length dlist)))
        (begin
          ndigits
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-number-of-digits-1 result-hash-table)
 (begin
   (let ((sub-name "test-number-of-digits-1")
         (test-list
          (list
           (list 1 1) (list 10 2) (list 100 3) (list 101 3)
           (list 1000 4) (list 10045 5) (list 120111 6)
           (list 1234567 7)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-num (list-ref alist 0))
                  (shouldbe-num (list-ref alist 1)))
              (let ((result-num (number-of-digits test-num)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : test-num=~a, "
                        sub-name test-label-index test-num))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe-num result-num)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-num result-num)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop max-digits debug-flag)
  (begin
    (let ((f1 1)
          (f2 1)
          (fcounter 3)
          (loop-completed #f)
          (final-result 0)
          (final-index 0)
          (final-ndigits 0))
      (begin
        (if (equal? debug-flag #t)
            (begin
              (display
               (ice-9-format:format #f "F1 = 1~%F2 = 1~%"))
              ))

        (while
         (equal? loop-completed #f)
         (begin
           (let ((fn (+ f1 f2)))
             (let ((ndigits (number-of-digits fn)))
               (begin
                 (if (equal? debug-flag #t)
                     (begin
                       (display
                        (ice-9-format:format
                         #f "F~:d = ~:d~%" fcounter fn))
                       ))
                 (if (>= ndigits max-digits)
                     (begin
                       (set! loop-completed #t)
                       (set! final-result fn)
                       (set! final-index fcounter)
                       (set! final-ndigits ndigits)
                       (break)
                       ))
                 (set! f1 f2)
                 (set! f2 fn)
                 (set! fcounter (1+ fcounter))
                 )))
           ))

        (display
         (ice-9-format:format
          #f "the ~:dth term, F~:d = ~:d~%"
          final-index final-index final-result))
        (display
         (ice-9-format:format
          #f "  is the first term to contain ~:d digits~%"
          final-ndigits))
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "The Fibonacci sequence is defined by "))
    (display
     (format #f "the recurrence~%"))
    (display
     (format #f "relation:~%"))
    (display
     (format #f "Fn = Fn1 + Fn2~%"))
    (display
     (format #f "where F1 = 1 and F2 = 1.~%"))
    (display
     (format #f "Hence the first 12 terms will be:~%"))
    (display
     (format #f "F1 = 1, F2 = 1, F3 = 2,~%"))
    (display
     (format #f "F4 = 3, F5 = 5, F6 = 8~%"))
    (display
     (format #f "F7 = 13, F8 = 21, F9 = 34,~%"))
    (display
     (format #f "F10 = 55, F11 = 89, F12 = 144~%"))
    (newline)
    (display
     (format #f "The 12th term, F12, is the first term "))
    (display
     (format #f "to contain~%"))
    (display
     (format #f "three digits.~%"))
    (newline)
    (display
     (format #f "What is the first term in the Fibonacci "))
    (display
     (format #f "sequence~%"))
    (display
     (format #f "to contain 1000 digits?~%"))
    (display
     (format #f "see https://projecteuler.net/problem=25~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((max-digits 3)
          (debug-flag #t))
      (begin
        (sub-main-loop max-digits debug-flag)
        ))

    (let ((max-digits 1000)
          (debug-flag #f))
      (begin
        (sub-main-loop max-digits debug-flag)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 25 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (load "digits-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

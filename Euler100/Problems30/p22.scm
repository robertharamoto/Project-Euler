#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 22                                     ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated June 11, 2022                                ###
;;;###                                                       ###
;;;###  updated March 4, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### ice-9 rdelim for line delimited reading functions
(use-modules ((ice-9 rdelim)
              :renamer (symbol-prefix-proc 'ice-9-rdelim:)))

;;;### timer-module for time-code function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (populate-alpha-hash! alpha-htable)
  (begin
    (let ((alist
           (list
            (list #\a 1) (list #\b 2) (list #\c 3) (list #\d 4)
            (list #\e 5) (list #\f 6) (list #\g 7) (list #\h 8)
            (list #\i 9) (list #\j 10) (list #\k 11) (list #\l 12)
            (list #\m 13) (list #\n 14) (list #\o 15) (list #\p 16)
            (list #\q 17) (list #\r 18) (list #\s 19) (list #\t 20)
            (list #\u 21) (list #\v 22) (list #\w 23) (list #\x 24)
            (list #\y 25) (list #\z 26))))
      (begin
        (for-each
         (lambda (this-list)
           (begin
             (let ((tchar (list-ref this-list 0))
                   (tnum (list-ref this-list 1)))
               (begin
                 (hash-set! alpha-htable tchar tnum)
                 ))
             )) alist)
        ))
    ))

;;;#############################################################
;;;#############################################################
;;; returns a list of names, lower case
(define (read-in-file fname)
  (begin
    (let ((name-list (list))
          (counter 0))
      (begin
        (if (file-exists? fname)
            (begin
              (with-input-from-file fname
                (lambda ()
                  (begin
                    (do ((line
                          (ice-9-rdelim:read-delimited ",\n")
                          (ice-9-rdelim:read-delimited ",\n")))
                        ((eof-object? line))
                      (begin
                        (if (not (eof-object? line))
                            (begin
                              (let ((this-string
                                     (string-downcase
                                      (string-delete #\" line))))
                                (begin
                                  (set!
                                   name-list
                                   (cons this-string name-list))
                                  (set! counter (1+ counter))
                                  ))
                              ))
                        ))
                    )))

              (display
               (ice-9-format:format
                #f "number of names read = ~:d~%" counter))
              (force-output)
              (set! name-list (sort name-list string-ci<?))
              name-list)
            (begin
              (list)
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
;;; returns a list of names, lower case
(define (process-list name-list)
  (begin
    (let ((list-counter 0)
          (name-score 0)
          (char-htable (make-hash-table 30)))
      (begin
        (populate-alpha-hash! char-htable)

        (for-each
         (lambda (this-name)
           (begin
             (set! list-counter (1+ list-counter))

             (let ((slist (string->list this-name))
                   (this-sum 0))
               (begin
                 (for-each
                  (lambda (this-char)
                    (begin
                      (let ((this-value
                             (hash-ref char-htable this-char 0)))
                        (begin
                          (set! this-sum (+ this-sum this-value))
                          ))
                      )) slist)

                 (set!
                  name-score
                  (+ name-score (* this-sum list-counter)))
                 ))
             )) (sort name-list string-ci<?))
        name-score
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-process-list-1 result-hash-table)
 (begin
   (let ((sub-name "test-process-list-1")
         (test-list
          (list
           (list (list "colin") 53)
           (list (list "a" "colin") 107)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((test-num (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result (process-list test-num)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : test-num=~a, "
                        sub-name test-label-index test-num))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a~%"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop filename)
  (begin
    (let ((names-list (read-in-file filename)))
      (let ((name-count (length names-list))
            (name-sum (process-list names-list)))
        (begin
          (display
           (ice-9-format:format
            #f "name score = ~:d  (number of names = ~:d)~%"
            name-sum name-count))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Using names.txt (right click and "))
    (display
     (format #f "'Save Link/Target~%"))
    (display
     (format #f "As...'), a 46K text file containing "))
    (display
     (format #f "over five-thousand~%"))
    (display
     (format #f "first names, begin by sorting it into "))
    (display
     (format #f "alphabetical order.~%"))
    (display
     (format #f "Then working out the alphabetical value "))
    (display
     (format #f "for each name,~%"))
    (display
     (format #f "multiply this value by its alphabetical "))
    (display
     (format #f "position in the~%"))
    (display
     (format #f "list to obtain a name score.~%"))
    (newline)
    (display
     (format #f "For example, when the list is sorted "))
    (display
     (format #f "into alphabetical~%"))
    (display
     (format #f "order, COLIN, which is worth "))
    (display
     (format #f "3 + 15 + 12 + 9 + 14 = 53,~%"))
    (display
     (format #f "is the 938th name in the list. So, "))
    (display
     (format #f "COLIN would obtain~%"))
    (display
     (format #f "a score of 938 x 53 = 49714.~%"))
    (newline)
    (display
     (format #f "What is the total of all the name "))
    (display
     (format #f "scores in the file?~%"))
    (display
     (format #f "see https://projecteuler.net/problem=22~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((filename "names.txt"))
      (begin
        (sub-main-loop filename)
        (newline)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 22 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests title-string  debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

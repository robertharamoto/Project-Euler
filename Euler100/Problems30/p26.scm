#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 26                                     ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated June 11, 2022                                ###
;;;###                                                       ###
;;;###  updated March 6, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### for let-values
(use-modules ((srfi srfi-11)
              :renamer (symbol-prefix-proc 'srfi-11:)))

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### prime-module for is-prime? function
(use-modules ((prime-module)
              :renamer (symbol-prefix-proc 'prime-module:)))

;;;### timer-module for time-code function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (decimal-divisors-digits-list
         numerator denominator max-decimals)
  (begin
    (let ((result-list (list))
          (counter 0)
          (num numerator)
          (break-flag #f))
      (begin
        (while
         (and (equal? break-flag #f)
              (< counter max-decimals))
         (begin
           (srfi-11:let-values
            (((q1 r1) (euclidean/ num denominator)))
            (begin
              (set! result-list (cons q1 result-list))
              (set! num (* r1 10))
              (set! counter (1+ counter))
              (if (zero? r1)
                  (begin
                    (set! break-flag #t)
                    ))
              ))
           ))

        (set! result-list (reverse result-list))

        result-list
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (assert-two-lists-equal
         shouldbe-list result-list
         sub-name err-start
         result-hash-table)
  (begin
    (let ((shouldbe-length (length shouldbe-list))
          (result-length (length result-list)))
      (let ((err-1
             (format
              #f "~a : shouldbe-list=~a, result-list=~a : "
              err-start shouldbe-list result-list))
            (err-2
             (format
              #f "lengths differ shouldbe=~a, result=~a"
              shouldbe-length result-length)))
        (begin
          (unittest2:assert?
           (equal? shouldbe-length result-length)
           sub-name
           (string-append err-1 err-2)
           result-hash-table)

          (if (> shouldbe-length 0)
              (begin
                (for-each
                 (lambda (s-elem)
                   (begin
                     (let ((mflag (member s-elem result-list))
                           (err-3
                            (format #f "missing element ~a"
                                    s-elem)))
                       (begin
                         (unittest2:assert?
                          (not (equal? mflag #f))
                          sub-name
                          (string-append err-1 err-3)
                          result-hash-table)
                         ))
                     )) shouldbe-list))
              (begin
                (unittest2:assert?
                 (equal? shouldbe-list result-list)
                 sub-name
                 (string-append err-1 err-2)
                 result-hash-table)
                ))
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-decimal-divisors-digits-list-1 result-hash-table)
 (begin
   (let ((sub-name "test-decimal-divisors-digits-list-1")
         (test-list
          (list
           (list 1 1 10 (list 1))
           (list 1 2 10 (list 0 5))
           (list 1 3 10 (list 0 3 3 3 3 3 3 3 3 3))
           (list 1 4 10 (list 0 2 5))
           (list 1 5 10 (list 0 2))
           (list 1 6 10 (list 0 1 6 6 6 6 6 6 6 6))
           (list 1 7 10 (list 0 1 4 2 8 5 7 1 4 2))
           (list 1 8 10 (list 0 1 2 5))
           (list 1 9 10 (list 0 1 1 1 1 1 1 1 1 1))
           (list 1 10 10 (list 0 1))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-numer (list-ref alist 0))
                  (test-denom (list-ref alist 1))
                  (test-max (list-ref alist 2))
                  (shouldbe-list (list-ref alist 3)))
              (let ((result-list
                     (decimal-divisors-digits-list
                      test-numer test-denom test-max)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "test-numer=~a, test-denom=~a, "
                        test-numer test-denom)))
                  (begin
                    (assert-two-lists-equal
                     shouldbe-list result-list
                     sub-name
                     (string-append
                      err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; return a list of lists that are each nwidth long starting at
;;; position start-pos
(define (split-list init-list start-pos nwidth)
  (begin
    (let ((llen (length init-list))
          (result-list-list (list))
          (tmp-count 0)
          (tmp-list (list)))
      (begin
        (do ((ii start-pos (1+ ii)))
            ((>= ii llen))
          (begin
            (let ((elem (list-ref init-list ii)))
              (begin
                (set! tmp-list (cons elem tmp-list))
                (set! tmp-count (1+ tmp-count))
                (if (>= tmp-count nwidth)
                    (begin
                      (set! result-list-list
                            (cons
                             (reverse tmp-list)
                             result-list-list))
                      (set! tmp-list (list))
                      (set! tmp-count 0)
                      ))
                ))
            ))
        (if (> tmp-count 0)
            (begin
              (set! result-list-list
                    (cons
                     (reverse tmp-list)
                     result-list-list))
              ))

        result-list-list
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-split-list-1 result-hash-table)
 (begin
   (let ((sub-name "test-split-list-1")
         (test-list
          (list
           (list (list 1 2 3 4 5 6 7 8 9 10 11 12)
                 0 2
                 (list (list 1 2) (list 3 4) (list 5 6)
                       (list 7 8) (list 9 10) (list 11 12)))
           (list (list 1 2 3 4 5 6 7 8 9 10 11 12)
                 0 3
                 (list (list 1 2 3) (list 4 5 6) (list 7 8 9)
                       (list 10 11 12)))
           (list (list 1 2 3 4 5 6 7 8 9 10 11 12)
                 0 4
                 (list (list 1 2 3 4) (list 5 6 7 8)
                       (list 9 10 11 12)))
           (list (list 1 2 3 4 5 6 7 8 9 10 11 12)
                 0 5
                 (list (list 1 2 3 4 5) (list 6 7 8 9 10)
                       (list 11 12)))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((input-list (list-ref alist 0))
                  (start-pos (list-ref alist 1))
                  (nwidth (list-ref alist 2))
                  (shouldbe-list-list (list-ref alist 3)))
              (let ((result-list-list
                     (reverse
                      (split-list input-list start-pos nwidth))))
                (let ((slen (length shouldbe-list-list))
                      (rlen (length result-list-list)))
                  (let ((llen (min slen rlen))
                        (err-1
                         (format
                          #f "~a :: (~a) error : input-list=~a, "
                          sub-name test-label-index input-list))
                        (err-2
                         (format
                          #f "shouldbe-len=~a, result-len=~a, "
                          slen rlen))
                        (err-3
                         (format
                          #f "shouldbe=~a, result=~a"
                          shouldbe-list-list
                          result-list-list)))
                    (begin
                      (unittest2:assert?
                       (equal? slen rlen)
                       sub-name
                       (string-append
                        err-1 err-2 err-3)
                       result-hash-table)

                      (if (> llen 0)
                          (begin
                            (do ((ii 0 (1+ ii)))
                                ((>= ii llen))
                              (begin
                                (let ((slist (list-ref shouldbe-list-list ii))
                                      (rlist (list-ref result-list-list ii)))
                                  (begin
                                    (assert-two-lists-equal
                                     slist rlist
                                     sub-name
                                     err-1
                                     result-hash-table)
                                    ))
                                ))
                            ))
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; how many times does sub-list appear in init-list
(define (count-list-repeats init-list sub-list)
  (begin
    (let ((sub-count 0)
          (llen (length init-list)))
      (begin
        (for-each
         (lambda (ilist)
           (begin
             (if (equal? ilist sub-list)
                 (begin
                   (set! sub-count (1+ sub-count))
                   ))
             )) init-list)

        sub-count
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-count-list-repeats-1 result-hash-table)
 (begin
   (let ((sub-name "test-count-list-repeats-1")
         (test-list
          (list
           (list (list (list 1 2) (list 3 4) (list 1 2)
                       (list 7 8) (list 9 10) (list 11 12))
                 (list 1 2)
                 2)
           (list (list (list 1 2) (list 3 4) (list 1 2)
                       (list 7 8) (list 9 10) (list 11 12))
                 (list 11 12)
                 1)
           (list (list (list 1 2 3) (list 3 4 5) (list 1 2 3)
                       (list 7 8 9) (list 7 8 9) (list 7 8 9))
                 (list 1 2 3)
                 2)
           (list (list (list 1 2 3) (list 3 4 5) (list 1 2 3)
                       (list 7 8 9) (list 7 8 9) (list 7 8 9))
                 (list 3 4 5)
                 1)
           (list (list (list 1 2 3) (list 3 4 5) (list 1 2 3)
                       (list 7 8 9) (list 7 8 9) (list 7 8 9))
                 (list 7 8 9)
                 3)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((input-list-list (list-ref alist 0))
                  (sub-list (list-ref alist 1))
                  (shouldbe (list-ref alist 2)))
              (let ((result
                     (count-list-repeats input-list-list sub-list)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : list=~a : "
                        sub-name test-label-index input-list-list))
                      (err-2
                       (format #f "shouldbe=~a, result=~a"
                               shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; look for repeating sub-lists through to the end of dlist
(define (does-list-repeat? dlist ii-start-pos ii-window)
  (begin
    (let ((local-dlist (list-tail dlist ii-start-pos)))
      (let ((local-dlen (length local-dlist))
            (local-list-list (split-list local-dlist 0 ii-window))
            (sub-list (list-head local-dlist ii-window)))
        (let ((scount
               (count-list-repeats local-list-list sub-list))
              (expected-match-count
               (1- (euclidean/ local-dlen ii-window))))
          (begin
            (if (<= ii-window 2)
                (begin
                  (set!
                   expected-match-count
                   (- expected-match-count 2))
                  ))
            (if (>= scount expected-match-count)
                (begin
                  #t)
                (begin
                  #f
                  ))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-does-list-repeat-1 result-hash-table)
 (begin
   (let ((sub-name "test-does-list-repeat-1")
         (test-list
          (list
           (list (list 3 3 3 3 3 3 3 3 3) 0 3 #t)
           (list (list 0 3 3 3 3 3 3 3 3) 1 3 #t)
           (list (list 4 3 3 3 3 3 3 3 3) 0 3 #f)
           (list (list 6 6 6 6 6 6 6 6 1 0) 0 1 #t)
           (list (list 1 4 2 8 5 7 1 4 2 8 5 7 1 4 2 8 5 7 1 4) 0 6 #t)
           (list (list 0 1 4 2 8 5 7 1 4 2 8 5 7 1 4 2 8 5 7 1 4) 1 6 #t)
           (list (list 0 1 4 2 8 5 7 1 4 2 8 5 7 1 4 2 8 5 7 1 4) 1 5 #f)
           (list (list 0 1 4 2 8 5 7 1 4 2 8 5 7 1 4 2 8 5 7 1 4) 1 3 #f)
           (list (list 0 5 8 8 2 3 5 2 9 4 1 1 7 6 4 7 0 5 8 8 2 3 5 2 9 4 1 1 7 6 4 7)
                 0 16 #t)
           (list (list 0 5 8 8 2 3 5 2 9 4 1 1 7 6 4 7 0 5 8 8 2 3 5 2 9 4 1 1 7 6 4 7 5 8 8)
                 0 4 #f)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-dlist (list-ref alist 0))
                  (start-pos (list-ref alist 1))
                  (ii-window (list-ref alist 2))
                  (shouldbe-bool (list-ref alist 3)))
              (let ((result-bool
                     (does-list-repeat? test-dlist start-pos ii-window)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "test-dlist=~a, start-pos=~a, "
                        test-dlist start-pos))
                      (err-3
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe-bool result-bool)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-bool result-bool)
                     sub-name
                     (string-append
                      err-1 err-2 err-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; makes use of the fact that repeating decimals for 1/p,
;;; where p a prime, repeat at most by the number itself
;;; see https://en.wikipedia.org/wiki/Repeating_decimal
(define (find-repeat-decimal numerator denominator)
  (begin
    (let ((max-num (* 4 denominator))
          (max-window denominator))
      (let ((dlist
             (decimal-divisors-digits-list
              numerator denominator max-num))
            (max-pos 5)
            (max-digits denominator)
            (min-repeat-list (list))
            (min-repeat-length -1)
            (break-flag #f))
        (let ((dlen (length dlist))
              (rdlist (reverse dlist)))
          (begin
            (cond
             ((< dlen max-num)
              (begin
                ;;; return empty list for terminating decimals
                (list)
                ))
             (else
              (begin
                ;;; we have a non-terminating decimal expansion,
                ;;; so look at various window sizes, reversed dlist
                (do ((ii-start 0 (1+ ii-start)))
                    ((or (>= ii-start max-pos)
                         (equal? break-flag #t)))
                  (begin
                    (do ((ii-window 1 (1+ ii-window)))
                        ((or (>= ii-window max-window)
                             (equal? break-flag #t)))
                      (begin
                        (let ((rbool
                               (does-list-repeat?
                                dlist ii-start ii-window)))
                          (begin
                            (if (equal? rbool #t)
                                (begin
                                  (set! min-repeat-list
                                        (list-head
                                         (list-tail dlist ii-start)
                                         ii-window))
                                  (set! min-repeat-length ii-window)
                                  (set! break-flag #t)
                                  ))
                            ))
                        ))
                    ))

                min-repeat-list
                )))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-find-repeat-decimal-1 result-hash-table)
 (begin
   (let ((sub-name "find-repeat-decimal-1")
         (test-list
          (list
           (list 1 2 (list))
           (list 1 3 (list 3))
           (list 1 5 (list))
           (list 1 6 (list 6))
           (list 1 7 (list 1 4 2 8 5 7))
           (list 1 11 (list 0 9))
           (list 1 13 (list 0 7 6 9 2 3))
           (list 1 17 (list 0 5 8 8 2 3 5 2 9 4 1 1 7 6 4 7))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((numerator (list-ref alist 0))
                  (denominator (list-ref alist 1))
                  (shouldbe-list (list-ref alist 2)))
              (let ((result-list
                     (find-repeat-decimal numerator denominator)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "numerator=~a, denominator=~a"
                        numerator denominator)))
                  (begin
                    (assert-two-lists-equal
                     shouldbe-list result-list
                     sub-name
                     (string-append
                      err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; since primes give the maximal number of repeating decimals
;;; only look at odd numbers to reduce the amount of work
;;; by fermat's little theorem, 10^(p-1) = 1 mod p, the
;;; period is equal to the p-1 if 10 a primative root mod p
;;; larger the prime the larger the repeating cycle.
(define (sub-main-loop max-num)
  (begin
    (let ((result-list (list))
          (result-len 0)
          (result-denominator 0)
          (start-num max-num)
          (end-loop-flag #f))
      (begin
        (if (even? start-num)
            (begin
              (set! start-num (1- start-num))
              ))

        (do ((ii-denom start-num (- ii-denom 2)))
            ((or (<= ii-denom 1)
                 (equal? end-loop-flag #t)))
          (begin
            (if (prime-module:is-prime? ii-denom)
                (begin
                  (let ((this-list (find-repeat-decimal 1 ii-denom)))
                    (begin
                      (if (and (list? this-list)
                               (> (length this-list) 0))
                          (begin
                            (let ((this-len (length this-list)))
                              (begin
                                (if (> this-len result-len)
                                    (begin
                                      (set! result-list this-list)
                                      (set! result-len this-len)
                                      (set! result-denominator ii-denom)
                                      ))

                                (if (>= this-len (- ii-denom 1))
                                    (begin
                                      (set! end-loop-flag #t)
                                      ))
                                ))
                            ))
                      ))
                  ))
            ))

        (if (> result-denominator 0)
            (begin
              (let ((rdiv (/ 1.0 result-denominator)))
                (begin
                  (display
                   (ice-9-format:format
                    #f "1/~:d = ~3,8f ..., has a ~:d digit "
                    result-denominator rdiv result-len))
                  (display
                   (ice-9-format:format
                    #f "recurring cycle = ~a~%"
                    result-list))
                  (display
                   (ice-9-format:format
                    #f "(for denominators less than ~:d)~%"
                    max-num))
                  )))
            (begin
              (display
               (ice-9-format:format
                #f "no results found for denominators less than ~:d~%"
                max-num))
              ))
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "A unit fraction contains 1 in "))
    (display
     (format #f "the numerator. The~%"))
    (display
     (format #f "decimal representation of the unit "))
    (display
     (format #f "fractions with~%"))
    (display
     (format #f "denominators 2 to 10 are given:~%"))
    (display
     (format #f "1/2 = 0.5, 1/3 = 0.(3), 1/4 = 0.25, "))
    (display
     (format #f "1/5 = 0.2,~%"))
    (display
     (format #f "1/6 = 0.1(6), 1/7 = 0.(142857), "))
    (display
     (format #f "1/8 = 0.125, 1/9 = 0.(1),~%"))
    (display
     (format #f "1/10 = 0.1~%"))
    (newline)
    (display
     (format #f "Where 0.1(6) means 0.166666..., and "))
    (display
     (format #f "has a 1-digit~%"))
    (display
     (format #f "recurring cycle. It can be seen that 1/7 "))
    (display
     (format #f "has a 6-digit~%"))
    (display
     (format #f "recurring cycle.~%"))
    (newline)
    (display
     (format #f "Find the value of d <= 1000 for which "))
    (display
     (format #f "1/d contains~%"))
    (display
     (format #f "the longest recurring cycle in its "))
    (display
     (format #f "decimal fraction~%"))
    (display
     (format #f "part.~%"))
    (newline)
    (display
     (format #f "The key to speeding this program up "))
    (display
     (format #f "is to note that~%"))
    (display
     (format #f "the repeating decimal period is equal "))
    (display
     (format #f "to p-1 if p~%"))
    (display
     (format #f "a prime, by fermat's little theorem. "))
    (display
     (format #f "This means that~%"))
    (display
     (format #f "one should work backwards, in order "))
    (display
     (format #f "to find the~%"))
    (display
     (format #f "prime with the largest period.~%"))
    (display
     (format #f "See https://en.wikipedia.org/wiki/Repeating_decimal~%"))
    (display
     (format #f "see https://projecteuler.net/problem=26~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((max-num 100))
      (begin
        (sub-main-loop max-num)
        ))


    (let ((max-num 1000))
      (begin
        (sub-main-loop max-num)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 26 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (load "prime-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 21                                     ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated June 11, 2022                                ###
;;;###                                                       ###
;;;###  updated March 6, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### srfi-1 for fold functions
(use-modules ((srfi srfi-1)
              :renamer (symbol-prefix-proc 'srfi-1:)))

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (proper-divisors-list this-num)
  (begin
    (let ((div-list (list 1))
          (max-iter (1+ (exact-integer-sqrt this-num))))
      (begin
        (if (<= this-num 1)
            (begin
              (set! div-list (list))
              ))

        (do ((ii 2 (+ ii 1)))
            ((> ii max-iter))
          (begin
            (if (zero? (modulo this-num ii))
                (begin
                  (let ((other-div (euclidean/ this-num ii)))
                    (begin
                      (if (< ii other-div)
                          (begin
                            (set! div-list
                                  (append
                                   div-list (list ii other-div))))
                          (begin
                            (if (= ii other-div)
                                (begin
                                  (set!
                                   div-list
                                   (append div-list (list ii)))
                                  ))
                            ))
                      ))
                  ))
            ))

        (sort div-list <)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (assert-two-lists-equal
         shouldbe-list result-list
         sub-name err-start
         result-hash-table)
  (begin
    (let ((shouldbe-length (length shouldbe-list))
          (result-length (length result-list)))
      (let ((err-1
             (format
              #f "~a : shouldbe-list=~a, result-list=~a : "
              err-start shouldbe-list result-list))
            (err-2
             (format
              #f "lengths differ shouldbe=~a, result=~a"
              shouldbe-length result-length)))
        (begin
          (unittest2:assert?
           (equal? shouldbe-length result-length)
           sub-name
           (string-append err-1 err-2)
           result-hash-table)

          (if (> shouldbe-length 0)
              (begin
                (for-each
                 (lambda (s-elem)
                   (begin
                     (let ((mflag (member s-elem result-list))
                           (err-3
                            (format #f "missing element ~a" s-elem)))
                       (begin
                         (unittest2:assert?
                          (not (equal? mflag #f))
                          sub-name
                          (string-append err-1 err-3)
                          result-hash-table)
                         ))
                     )) shouldbe-list))
              (begin
                (unittest2:assert?
                 (equal? shouldbe-list result-list)
                 sub-name
                 (string-append err-1 err-2)
                 result-hash-table)
                ))
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-proper-divisors-list-1 result-hash-table)
 (begin
   (let ((sub-name "test-proper-divisors-list-1")
         (test-list
          (list
           (list 1 (list)) (list 3 (list 1))
           (list 6 (list 1 2 3)) (list 10 (list 1 2 5))
           (list 15 (list 1 3 5)) (list 21 (list 1 3 7))
           (list 28 (list 1 2 4 7 14))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((test-num (list-ref this-list 0))
                  (shouldbe-list (list-ref this-list 1)))
              (let ((result-list (proper-divisors-list test-num))
                    (shouldbe-length (length shouldbe-list)))
                (let ((err-start
                       (format
                        #f "~a :: (~a) error : test-num=~a, "
                        sub-name test-label-index test-num)))
                  (begin
                    (assert-two-lists-equal
                     shouldbe-list result-list
                     sub-name
                     err-start
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (list-to-sum-string llist)
  (begin
    (let ((stmp
           (string-join
            (map
             (lambda (this-num)
               (begin
                 (ice-9-format:format #f "~:d" this-num)
                 )) llist)
            " + ")))
      (begin
        stmp
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-list-to-sum-string-1 result-hash-table)
 (begin
   (let ((sub-name "test-list-to-sum-string-1")
         (test-list
          (list
           (list (list 1) "1")
           (list (list 1 2) "1 + 2")
           (list (list 1 2 3) "1 + 2 + 3")
           (list (list 4 5 6 7) "4 + 5 + 6 + 7")
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((input-list (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result (list-to-sum-string input-list)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : input-list=~a, "
                        sub-name test-label-index input-list))
                      (err-2
                       (format
                        #f "shouldbe=~s, result=~s"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (string-ci=? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (+ test-label-index 1))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (divisors-sum this-num)
  (begin
    (let ((div-list (proper-divisors-list this-num)))
      (let ((div-sum (srfi-1:fold + 0 div-list)))
        (begin
          (list div-sum div-list)
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-divisors-sum-1 result-hash-table)
 (begin
   (let ((sub-name "test-divisors-sum-1")
         (test-list
          (list
           (list 2 (list 1 (list 1)))
           (list 3 (list 1 (list 1)))
           (list 4 (list 3 (list 1 2)))
           (list 5 (list 1 (list 1)))
           (list 6 (list 6 (list 1 2 3)))
           (list 8 (list 7 (list 1 2 4)))
           (list 10 (list 8 (list 1 2 5)))
           (list 12 (list 16 (list 1 2 3 4 6)))
           (list 15 (list 9 (list 1 3 5)))
           (list 24 (list 36 (list 1 2 3 4 6 8 12)))
           (list 28 (list 28 (list 1 2 4 7 14)))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((test-num (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result (divisors-sum test-num)))
                (let ((err-start
                       (format
                        #f "~a :: (~a) error : test-num=~a, "
                        sub-name test-label-index test-num)))
                  (begin
                    (assert-two-lists-equal
                     shouldbe result
                     sub-name
                     err-start
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (+ test-label-index 1))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop max-num)
  (begin
    (let ((sum 0)
          (sum-list (list)))
      (begin
        (do ((jj 1 (+ jj 1)))
            ((> jj max-num))
          (begin
            (let ((result-list (divisors-sum jj)))
              (let ((dsum (list-ref result-list 0))
                    (dlist (list-ref result-list 1)))
                (let ((result2-list (divisors-sum dsum)))
                  (let ((dsum2 (list-ref result2-list 0))
                        (dlist2 (list-ref result2-list 1)))
                    (begin
                      (if (and (equal? jj dsum2)
                               (>= dsum jj))
                          (begin
                            (if (not (equal? dsum dsum2))
                                (begin
                                  (set! sum (+ sum dsum dsum2))
                                  (set! sum-list (append sum-list (list dsum dsum2)))

                                  (display
                                   (ice-9-format:format
                                    #f "(~:d, ~:d) is an amicable pair! "
                                    jj dsum))
                                  (display
                                   (ice-9-format:format
                                    #f "sum of divisors of ~:d : ~a~%"
                                    jj (list-to-sum-string dlist)))
                                  (display
                                   (ice-9-format:format
                                    #f "  = ~:d~%" dsum))
                                  (display
                                   (ice-9-format:format
                                    #f "    sum of divisors of ~:d : "
                                    dsum))
                                  (display
                                   (ice-9-format:format
                                    #f "~a = ~:d~%"
                                    (list-to-sum-string dlist2) dsum2))
                                  ))
                            ))
                      )))
                ))
            ))

        (let ((s-list (sort sum-list <)))
          (let ((s-string (list-to-sum-string s-list)))
            (begin
              (display
               (ice-9-format:format
                #f "The sum of all amicable numbers under ~:d~%"
                max-num))
              (display
               (ice-9-format:format
                #f "is ~:d = ~a~%"
                sum s-string))
              )))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Let d(n) be defined as the sum of "))
    (display
     (format #f "proper divisors~%"))
    (display
     (format #f "of n (numbers less than n which "))
    (display
     (format #f "divide evenly~%"))
    (display
     (format #f "into n). If d(a) = b and d(b) = a, "))
    (display
     (format #f "where a != b,~%"))
    (display
     (format #f "then a and b are an amicable pair and "))
    (display
     (format #f "each of a~%"))
    (display
     (format #f "and b are called amicable numbers.~%"))
    (newline)
    (display
     (format #f "For example, the proper divisors "))
    (display
     (format #f "of 220 are~%"))
    (display
     (format #f "1, 2, 4, 5, 10, 11, 20, 22, 44, 55 and 110;~%"))
    (display
     (format #f "therefore d(220) = 284. The proper "))
    (display
     (format #f "divisors of~%"))
    (display
     (format #f "284 are 1, 2, 4, 71 and 142; so "))
    (display
     (format #f "d(284) = 220.~%"))
    (display
     (format #f "Evaluate the sum of all the amicable "))
    (display
     (format #f "numbers~%"))
    (display
     (format #f "under 10000.~%"))
    (display
     (format #f "see https://projecteuler.net/problem=21~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((max-num 300))
      (begin
        (sub-main-loop max-num)
        ))

    (let ((max-num 10000))
      (begin
        (sub-main-loop max-num)
        ))

    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 21 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests
                  title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

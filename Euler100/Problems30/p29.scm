#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 29                                     ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated June 11, 2022                                ###
;;;###                                                       ###
;;;###  updated March 4, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### srfi-1 for fold functions
(use-modules ((srfi srfi-1)
              :renamer (symbol-prefix-proc 'srfi-1:)))

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (integer-power this-number this-exponent)
  (begin
    (cond
     ((= this-exponent 0)
      (begin
        1
        ))
     ((= this-exponent 1)
      (begin
        this-number
        ))
     ((< this-exponent 0)
      (begin
        -1
        ))
     (else
      (begin
        (let ((result-num this-number)
              (max-iter (- this-exponent 1)))
          (begin
            (do ((ii 0 (+ ii 1)))
                ((>= ii max-iter))
              (begin
                (set! result-num (* result-num this-number))
                ))
            result-num
            ))
        )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-integer-power-1 result-hash-table)
 (begin
   (let ((sub-name "test-integer-power-1")
         (test-list
          (list
           (list 10 0 1) (list 11 0 1) (list 12 0 1)
           (list 10 1 10) (list 11 1 11) (list 12 1 12)
           (list 10 2 100) (list 11 2 121) (list 12 2 144)
           (list 2 2 4) (list 2 3 8) (list 2 4 16) (list 2 5 32)
           (list 2 6 64) (list 2 7 128) (list 2 8 256)
           (list 2 9 512) (list 2 10 1024)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-num (list-ref alist 0))
                  (test-exp (list-ref alist 1))
                  (shouldbe-num (list-ref alist 2)))
              (let ((result-num (integer-power test-num test-exp)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "test-num=~a, test-exp=~a, "
                        test-num test-exp))
                      (err-3
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe-num result-num)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-num result-num)
                     sub-name
                     (string-append
                      err-1 err-2 err-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (make-result-list min-aa max-aa min-bb max-bb)
  (begin
    (let ((result-list (list)))
      (begin
        (do ((aa min-aa (+ aa 1)))
            ((> aa max-aa))
          (begin
            (do ((bb min-bb (+ bb 1)))
                ((> bb max-bb))
              (begin
                (let ((this-elem (integer-power aa bb)))
                  (begin
                    (set!
                     result-list (cons this-elem result-list))
                    ))
                ))
            ))

        (let ((r2-list (srfi-1:delete-duplicates result-list)))
          (begin
            (sort r2-list <)
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (assert-two-lists-equal
         shouldbe-list result-list
         sub-name err-start
         result-hash-table)
  (begin
    (let ((shouldbe-length (length shouldbe-list))
          (result-length (length result-list)))
      (let ((err-1
             (format
              #f "~a : shouldbe-list=~a, result-list=~a : "
              err-start shouldbe-list result-list))
            (err-2
             (format
              #f "lengths differ shouldbe=~a, result=~a"
              shouldbe-length result-length)))
        (begin
          (unittest2:assert?
           (equal? shouldbe-length result-length)
           sub-name
           (string-append err-1 err-2)
           result-hash-table)

          (if (> shouldbe-length 0)
              (begin
                (for-each
                 (lambda (s-elem)
                   (begin
                     (let ((mflag (member s-elem result-list))
                           (err-3
                            (format #f "missing element ~a" s-elem)))
                       (begin
                         (unittest2:assert?
                          (not (equal? mflag #f))
                          sub-name
                          (string-append err-1 err-3)
                          result-hash-table)
                         ))
                     )) shouldbe-list))
              (begin
                (unittest2:assert?
                 (equal? shouldbe-list result-list)
                 sub-name
                 (string-append err-1 err-2)
                 result-hash-table)
                ))
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-make-result-list-1 result-hash-table)
 (begin
   (let ((sub-name "test-make-result-list-1")
         (test-list
          (list
           (list 2 5 2 5
                 (list 4 8 9 16 25 27 32 64 81 125
                       243 256 625 1024 3125))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((min-aa (list-ref alist 0))
                  (max-aa (list-ref alist 1))
                  (min-bb (list-ref alist 2))
                  (max-bb (list-ref alist 3))
                  (shouldbe-list (list-ref alist 4)))
              (let ((result-list
                     (make-result-list min-aa max-aa min-bb max-bb)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "min a/max a=~a/~a, min b/max b=~a/~a "
                        min-aa max-aa min-bb max-bb)))
                  (begin
                    (assert-two-lists-equal
                     shouldbe-list result-list
                     sub-name
                     (string-append
                      err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop min-aa max-aa min-bb max-bb debug-flag)
  (begin
    (let ((result-list
           (make-result-list min-aa max-aa
                             min-bb max-bb)))
      (let ((num-results (length result-list)))
        (begin
          (display
           (ice-9-format:format
            #f "number of results = ~:d for ~:d < a < ~:d "
            num-results min-aa max-aa))
          (display
           (ice-9-format:format
            #f "and ~:d < b < ~:d~%"
            min-bb max-bb))
          (if (equal? debug-flag #t)
              (begin
                (display
                 (format
                  #f "sequence = { ~a }~%"
                  (string-join
                   (map
                    (lambda (num)
                      (begin
                        (ice-9-format:format #f "~:d" num)
                        )) result-list)
                   ", ")))
                ))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Consider all integer combinations "))
    (display
     (format #f "of a^b for~%"))
    (display
     (format #f "2 < a < 5 and 2 < b < 5:~%"))
    (newline)
    (display
     (format #f "2^2=4, 2^3=8, 2^4=16, 2^5=32~%"))
    (display
     (format #f "3^2=9, 3^3=27, 3^4=81, 3^5=243~%"))
    (display
     (format #f "4^2=16, 4^3=64, 4^4=256, 4^5=1024~%"))
    (display
     (format #f "5^2=25, 5^3=125, 5^4=625, 5^5=3125~%"))
    (newline)
    (display
     (format #f "If they are then placed in numerical "))
    (display
     (format #f "order, with~%"))
    (display
     (format #f "any repeats removed, we get the "))
    (display
     (format #f "following sequence~%"))
    (display
     (format #f "of 15 distinct terms:~%"))
    (display
     (format #f "  4, 8, 9, 16, 25, 27, 32, 64, 81, "))
    (display
     (format #f "125, 243, 256,~%"))
    (display
     (format #f "625, 1024, 3125~%"))
    (newline)
    (display
     (format #f "How many distinct terms are in the "))
    (display
     (format #f "sequence generated~%"))
    (display
     (format #f "by a^b for 2 < a < 100 and 2 < b < 100?~%"))
    (display
     (format #f "see https://projecteuler.net/problem=29~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((min-aa 2)
          (max-aa 5)
          (min-bb 2)
          (max-bb 5)
          (debug-flag #t))
      (begin
        (sub-main-loop
         min-aa max-aa min-bb max-bb debug-flag)
        ))



    (let ((min-aa 2)
          (max-aa 100)
          (min-bb 2)
          (max-bb 100)
          (debug-flag #f))
      (begin
        (sub-main-loop
         min-aa max-aa min-bb max-bb debug-flag)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 29 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

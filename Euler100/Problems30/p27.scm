#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 27                                     ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated June 11, 2022                                ###
;;;###                                                       ###
;;;###  updated March 4, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### prime-module for is-prime? function
(use-modules ((prime-module)
              :renamer (symbol-prefix-proc 'prime-module:)))

;;;### timer-module for time-code function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
;;; sieve of eratosthenes method
(define (populate-prime-hash! primes-htable max-num)
  (begin
    (let ((intermediate-array (make-array 0 (1+ max-num)))
          (result-list (list)))
      (begin
        (do ((ii 0 (1+ ii)))
            ((> ii max-num))
          (begin
            (array-set! intermediate-array ii ii)
            ))

        (do ((ii 2 (1+ ii)))
            ((> ii max-num))
          (begin
            (let ((ii-num (array-ref intermediate-array ii)))
              (begin
                (if (= ii-num ii)
                    (begin
                      (hash-set! primes-htable ii #t)

                      (do ((jj (+ ii ii) (+ jj ii)))
                          ((> jj max-num))
                        (begin
                          (array-set! intermediate-array -1 jj)
                          ))
                      ))
                ))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (assert-two-lists-equal
         shouldbe-list result-list
         sub-name err-start
         result-hash-table)
  (begin
    (let ((shouldbe-length (length shouldbe-list))
          (result-length (length result-list)))
      (let ((err-1
             (format
              #f "~a : shouldbe-list=~a, result-list=~a : "
              err-start shouldbe-list result-list))
            (err-2
             (format
              #f "lengths differ shouldbe=~a, result=~a"
              shouldbe-length result-length)))
        (begin
          (unittest2:assert?
           (equal? shouldbe-length result-length)
           sub-name
           (string-append err-1 err-2)
           result-hash-table)

          (if (> shouldbe-length 0)
              (begin
                (for-each
                 (lambda (s-elem)
                   (begin
                     (let ((mflag (member s-elem result-list))
                           (err-3
                            (format #f "missing element ~a"
                                    s-elem)))
                       (begin
                         (unittest2:assert?
                          (not (equal? mflag #f))
                          sub-name
                          (string-append err-1 err-3)
                          result-hash-table)
                         ))
                     )) shouldbe-list))
              (begin
                (unittest2:assert?
                 (equal? shouldbe-list result-list)
                 sub-name
                 (string-append err-1 err-2)
                 result-hash-table)
                ))
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-populate-prime-hash-1 result-hash-table)
 (begin
   (let ((sub-name "test-populate-prime-hash-1")
         (test-list
          (list
           (list 2 (list 2)
                 (list 1 4))
           (list 3 (list 2 3)
                 (list 1))
           (list 4 (list 2 3)
                 (list 1 4))
           (list 5 (list 2 3 5)
                 (list 1 4))
           (list 6 (list 2 3 5)
                 (list 1 4 6))
           (list 7 (list 2 3 5 7)
                 (list 1 4 6))
           (list 8 (list 2 3 5 7)
                 (list 1 4 6 8))
           (list 9 (list 2 3 5 7)
                 (list 1 4 6 8 9))
           (list 10 (list 2 3 5 7)
                 (list 1 4 6 8 9))
           (list 11 (list 2 3 5 7 11)
                 (list 1 4 6 8 9 10))
           (list 13 (list 2 3 5 7 11 13)
                 (list 1 4 6 8 9 10 12))
           (list 17 (list 2 3 5 7 11 13 17)
                 (list 1 4 6 8 9 10 12 14 15 16))
           (list 19 (list 2 3 5 7 11 13 17 19)
                 (list 1 4 6 8 9 10 12 14 15 16 18))
           (list 23 (list 2 3 5 7 11 13 17 19 23)
                 (list 1 4 6 8 9 10 12 14 15 16 18 20 21 22))
           (list 31 (list 2 3 5 7 11 13 17 19 23 29 31)
                 (list 1 4 6 8 9 10 12 14 15 16 18 20 21 22 24 25 26 27 28 30))
           ))
         (primes-htable (make-hash-table))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (hash-clear! primes-htable)

            (let ((max-num (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1))
                  (shouldnotbe (list-ref this-list 2)))
              (let ((err-1
                     (format
                      #f "~a :: (~a) error : max-num=~a, "
                      sub-name test-label-index max-num))
                    (err-2
                     (format
                      #f "shouldbe=~a, "
                      shouldbe))
                    (err-3
                     (format
                      #f "shouldnotbe=~a, "
                      shouldnotbe)))
                (begin
                  (populate-prime-hash! primes-htable max-num)

                  (for-each
                   (lambda (sprime)
                     (begin
                       (let ((rprime-flag
                              (hash-ref primes-htable sprime #f)))
                         (let ((err-4
                                (format
                                 #f "shouldbe=~a, result=~a~%"
                                 sprime rprime-flag)))
                           (begin
                             (unittest2:assert?
                              (equal? rprime-flag #t)
                              sub-name
                              (string-append err-1 err-2 err-4)
                              result-hash-table)
                             )))
                       )) shouldbe)

                  (for-each
                   (lambda (snprime)
                     (begin
                       (let ((rprime-flag
                              (hash-ref primes-htable snprime #f)))
                         (let ((err-4
                                (format
                                 #f "shouldnotbe=~a, result=~a~%"
                                 snprime rprime-flag)))
                           (begin
                             (unittest2:assert?
                              (equal? rprime-flag #f)
                              sub-name
                              (string-append err-1 err-3 err-4)
                              result-hash-table)
                             )))
                       )) shouldnotbe)
                  )))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (find-generated-primes aa bb primes-htable)
  (begin
    (let ((rlist (list))
          (nn 0)
          (break-flag #f))
      (begin
        (while
         (equal? break-flag #f)
         (begin
           (let ((this-value
                  (+ (* nn nn) (* aa nn) bb)))
             (let ((pflag
                    (hash-ref primes-htable this-value -1)))
               (begin
                 (cond
                  ((equal? pflag #t)
                   (begin
                     (set! rlist (cons this-value rlist))
                     (set! nn (1+ nn))
                     ))
                  ((equal? pflag #f)
                   (begin
                     (set! break-flag #t)
                     ))
                  (else
                   (begin
                     (let ((ppflag
                            (prime-module:is-prime? this-value)))
                       (begin
                         (if (equal? ppflag #t)
                             (begin
                               (set! rlist (cons this-value rlist))
                               (set! nn (1+ nn))
                               (hash-set! primes-htable this-value #t))
                             (begin
                               (set! break-flag #t)
                               ))
                         ))
                     )))
                 )))
           ))
        (reverse rlist)
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-find-generated-primes-1 result-hash-table)
 (begin
   (let ((sub-name "test-find-generated-primes-1")
         (test-list
          (list
           (list 1 5 (list 5 7 11 17))
           (list 1 11 (list 11 13 17 23 31 41 53 67 83 101))
           ))
         (primes-htable (make-hash-table))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((test-aa (list-ref this-list 0))
                  (test-bb (list-ref this-list 1))
                  (shouldbe-list (list-ref this-list 2)))
              (let ((result-list
                     (find-generated-primes
                      test-aa test-bb primes-htable)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "test-aa=~a, test-bb=~a"
                        test-aa test-bb)))
                  (begin
                    (assert-two-lists-equal
                     shouldbe-list result-list
                     sub-name
                     (string-append
                      err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; define a macro to simplify code
(define-syntax sub-find-maxs-macro
  (syntax-rules ()
    ((sub-find-maxs-macro
      aa bb primes-htable
      max-list max-aa max-bb max-len)
     (begin
       (let ((maa (* -1 aa))
             (mbb (* -1 bb)))
         (let ((rlist1
                (find-generated-primes aa bb primes-htable))
               (rlist2
                (find-generated-primes maa bb primes-htable))
               (rlist3
                (find-generated-primes aa mbb primes-htable))
               (rlist4
                (find-generated-primes maa mbb primes-htable)))
           (let ((rlen1 (length rlist1))
                 (rlen2 (length rlist2))
                 (rlen3 (length rlist3))
                 (rlen4 (length rlist4)))
             (begin
               (if (> rlen1 max-len)
                   (begin
                     (set! max-len rlen1)
                     (set! max-list rlist1)
                     (set! max-aa aa)
                     (set! max-bb bb)
                     ))
               (if (> rlen2 max-len)
                   (begin
                     (set! max-len rlen2)
                     (set! max-list rlist2)
                     (set! max-aa maa)
                     (set! max-bb bb)
                     ))
               (if (> rlen3 max-len)
                   (begin
                     (set! max-len rlen3)
                     (set! max-list rlist3)
                     (set! max-aa aa)
                     (set! max-bb mbb)
                     ))
               (if (> rlen4 max-len)
                   (begin
                     (set! max-len rlen4)
                     (set! max-list rlist4)
                     (set! max-aa maa)
                     (set! max-bb mbb)
                     ))
               ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop end-aa end-bb largest-prime)
  (begin
    (let ((primes-htable (make-hash-table 100))
          (max-list (list))
          (max-aa 0)
          (max-bb 0)
          (max-len 0))
      (begin
        (populate-prime-hash! primes-htable largest-prime)

        (do ((aa 1 (+ aa 2)))
            ((> aa end-aa))
          (begin
            (do ((bb 3 (+ bb 2)))
                ((> bb end-bb))
              (begin
                (let ((pflag (hash-ref primes-htable bb)))
                  (begin
                    (if (equal? pflag #t)
                        (begin
                          (sub-find-maxs-macro
                           aa bb primes-htable
                           max-list max-aa max-bb max-len)
                          ))
                    ))
                ))
            ))

        (let ((result-len (length max-list)))
          (let ((sgn1 (if (>= max-aa 0) "+" "-"))
                (sgn2 (if (>= max-bb 0) "+" "-"))
                (ll-sgn-1 (if (>= max-aa 0) 1 -1))
                (ll-sgn-2 (if (>= max-bb 0) 1 -1)))
            (begin
              (display
               (ice-9-format:format
                #f "the product of the coefficients a and b "))
              (display
               (ice-9-format:format
                #f "is equal to ~:d~%"
                (* max-aa max-bb)))
              (display
               (ice-9-format:format
                #f "the quadratic formula n^2 ~a ~:dn ~a ~:d~%"
                sgn1 (* ll-sgn-1 max-aa)
                sgn2 (* ll-sgn-2 max-bb)))
              (display
               (ice-9-format:format
                #f "  produces ~:d primes for consecutive values of n~%"
                result-len))
              (display
               (ice-9-format:format
                #f "  starting with 0, list of primes = "))
              (display
               (ice-9-format:format
                #f "{ ~a }, (a<=~:d, b<=~:d)~%"
                (string-join
                 (map
                  (lambda (num)
                    (ice-9-format:format #f "~:d" num))
                  max-list) ", ")
                end-aa end-bb))
              (force-output)
              )))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Euler published the remarkable "))
    (display
     (format #f "quadratic formula:~%"))
    (display
     (format #f "  n^2 + n + 41~%"))
    (newline)
    (display
     (format #f "It turns out that the formula will "))
    (display
     (format #f "produce 40 primes~%"))
    (display
     (format #f "for the consecutive values n = 0 to 39. "))
    (display
     (format #f "However, when~%"))
    (display
     (format #f "n = 40, 40^2 + 40 + 41 = 40x(40 + 1) + 41 "))
    (display
     (format #f "is divisible by~%"))
    (display
     (format #f "41, and certainly when n = 41, "))
    (display
     (format #f "41^2 + 41 + 41~%"))
    (display
     (format #f "is clearly divisible by 41.~%"))
    (newline)
    (display
     (format #f "Using computers, the incredible "))
    (display
     (format #f "formula~%"))
    (display
     (format #f "n^2 - 79n + 1601 was discovered, "))
    (display
     (format #f "which produces 80~%"))
    (display
     (format #f "primes for the consecutive values "))
    (display
     (format #f "n = 0 to 79.~%"))
    (display
     (format #f "The product of the coefficients, "))
    (display
     (format #f "79 and 1601,~%"))
    (display
     (format #f "is 126479.~%"))
    (newline)
    (display
     (format #f "Considering quadratics of the form:~%"))
    (display
     (format #f "n^2 + an + b, where |a| < 1000 "))
    (display
     (format #f "and |b| < 1000~%"))
    (display
     (format #f "where |n| is the modulus/absolute "))
    (display
     (format #f "value of n~%"))
    (display
     (format #f "e.g. |11| = 11 and |4| = 4~%"))
    (newline)
    (display
     (format #f "Find the product of the coefficients, "))
    (display
     (format #f "a and b, for~%"))
    (display
     (format #f "the quadratic expression that produces "))
    (display
     (format #f "the maximum number~%"))
    (display
     (format #f "of primes for consecutive values of n, "))
    (display
     (format #f "starting with~%"))
    (display
     (format #f "n = 0.~%"))
    (newline)
    (display
     (format #f "Makes use of the fact that n^2+an+b "))
    (display
     (format #f "is a prime when~%"))
    (display
     (format #f "n=0, so b must be a prime. Also when "))
    (display
     (format #f "n=1, 1+a+b~%"))
    (display
     (format #f "a prime, and since b a prime, a must "))
    (display
     (format #f "be odd. When n=2,~%"))
    (display
     (format #f "4+2a+b excludes b=2.~%"))
    (display
     (format #f "see https://projecteuler.net/problem=27~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((end-aa 10)
          (end-bb 10)
          (largest-prime 10000))
      (begin
        (sub-main-loop end-aa end-bb largest-prime)
        ))

    (let ((end-aa 100)
          (end-bb 100)
          (largest-prime 10000))
      (begin
        (sub-main-loop end-aa end-bb largest-prime)
        ))

    (let ((end-aa 1000)
          (end-bb 1000)
          (largest-prime 10000))
      (begin
        (sub-main-loop end-aa end-bb largest-prime)
        ))

    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 27 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (load "prime-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 23                                     ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated June 11, 2022                                ###
;;;###                                                       ###
;;;###  updated March 6, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### srfi-1 for fold functions
(use-modules ((srfi srfi-1)
              :renamer (symbol-prefix-proc 'srfi-1:)))

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (proper-divisors-list this-num)
  (begin
    (let ((div-list (list 1))
          (max-iter
           (1+ (exact-integer-sqrt this-num))))
      (begin
        (if (<= this-num 1)
            (begin
              (set! div-list (list))
              ))

        (do ((ii 2 (+ ii 1)))
            ((> ii max-iter))
          (begin
            (if (zero? (modulo this-num ii))
                (begin
                  (let ((other-div
                         (euclidean/ this-num ii)))
                    (begin
                      (if (< ii other-div)
                          (begin
                            (set!
                             div-list
                             (append
                              div-list
                              (list ii other-div))))
                          (begin
                            (if (= ii other-div)
                                (begin
                                  (set!
                                   div-list
                                   (append div-list (list ii)))
                                  ))
                            ))
                      ))
                  ))
            ))

        (sort div-list <)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (assert-two-lists-equal
         shouldbe-list result-list
         sub-name err-start
         result-hash-table)
  (begin
    (let ((shouldbe-length (length shouldbe-list))
          (result-length (length result-list)))
      (let ((err-1
             (format
              #f "~a : shouldbe-list=~a, result-list=~a : "
              err-start shouldbe-list result-list))
            (err-2
             (format
              #f "lengths differ shouldbe=~a, result=~a"
              shouldbe-length result-length)))
        (begin
          (unittest2:assert?
           (equal? shouldbe-length result-length)
           sub-name
           (string-append err-1 err-2)
           result-hash-table)

          (if (> shouldbe-length 0)
              (begin
                (for-each
                 (lambda (s-elem)
                   (begin
                     (let ((mflag (member s-elem result-list))
                           (err-3
                            (format #f "missing element ~a"
                                    s-elem)))
                       (begin
                         (unittest2:assert?
                          (not (equal? mflag #f))
                          sub-name
                          (string-append err-1 err-3)
                          result-hash-table)
                         ))
                     )) shouldbe-list))
              (begin
                (unittest2:assert?
                 (equal? shouldbe-list result-list)
                 sub-name
                 (string-append err-1 err-2)
                 result-hash-table)
                ))
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-proper-divisors-list-1 result-hash-table)
 (begin
   (let ((sub-name "test-proper-divisors-list-1")
         (test-list
          (list
           (list 1 (list)) (list 3 (list 1))
           (list 6 (list 1 2 3)) (list 10 (list 1 2 5))
           (list 15 (list 1 3 5)) (list 21 (list 1 3 7))
           (list 28 (list 1 2 4 7 14))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((test-num (list-ref this-list 0))
                  (shouldbe-list (list-ref this-list 1)))
              (let ((result-list
                     (proper-divisors-list test-num)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : test-num=~a"
                        sub-name test-label-index test-num)))
                  (begin
                    (assert-two-lists-equal
                     shouldbe-list result-list
                     sub-name
                     err-1
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (list-to-sum-string llist)
  (begin
    (let ((stmp
           (string-join
            (map
             (lambda (this-num)
               (begin
                 (ice-9-format:format #f "~:d" this-num)
                 )) llist)
            " + ")))
      (begin
        stmp
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-list-to-sum-string-1 result-hash-table)
 (begin
   (let ((sub-name "test-list-to-sum-string-1")
         (test-list
          (list
           (list (list 1) "1")
           (list (list 1 2) "1 + 2")
           (list (list 1 2 3) "1 + 2 + 3")
           (list (list 4 5 6 7) "4 + 5 + 6 + 7")
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((input-list (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result (list-to-sum-string input-list)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : input-list=~a, "
                        sub-name test-label-index input-list))
                      (err-2
                       (format
                        #f "shouldbe=~s, result=~s"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (string-ci=? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (divisors-sum this-num)
  (begin
    (let ((div-list
           (proper-divisors-list this-num)))
      (let ((div-sum (srfi-1:fold + 0 div-list)))
        (begin
          (list div-sum div-list)
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-divisors-sum-1 result-hash-table)
 (begin
   (let ((sub-name "test-divisors-sum-1")
         (test-list
          (list
           (list 2 (list 1 (list 1)))
           (list 3 (list 1 (list 1)))
           (list 4 (list 3 (list 1 2)))
           (list 5 (list 1 (list 1)))
           (list 6 (list 6 (list 1 2 3)))
           (list 8 (list 7 (list 1 2 4)))
           (list 10 (list 8 (list 1 2 5)))
           (list 12 (list 16 (list 1 2 3 4 6)))
           (list 15 (list 9 (list 1 3 5)))
           (list 24 (list 36 (list 1 2 3 4 6 8 12)))
           (list 28 (list 28 (list 1 2 4 7 14)))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((test-num (list-ref this-list 0))
                  (shouldbe-list-list (list-ref this-list 1)))
              (let ((shouldbe-sum (list-ref shouldbe-list-list 0))
                    (shouldbe-list (list-ref shouldbe-list-list 1))
                    (result-list-list (divisors-sum test-num)))
                (let ((result-sum (list-ref result-list-list 0))
                      (result-list (list-ref result-list-list 1)))
                  (let ((err-1
                         (format
                          #f "~a :: (~a) error : test-num=~a"
                          sub-name test-label-index test-num))
                        (err-2
                         (format
                          #f " : shouldbe sum=~a, result sum=~a"
                          shouldbe-sum result-sum)))
                    (begin
                      (unittest2:assert?
                       (equal? shouldbe-sum result-sum)
                       sub-name
                       (string-append err-1 err-2)
                       result-hash-table)

                      (assert-two-lists-equal
                       shouldbe-list result-list
                       sub-name
                       err-1
                       result-hash-table)
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (is-abundant? this-num)
  (begin
    (let ((div-list (proper-divisors-list this-num)))
      (let ((div-sum (srfi-1:fold + 0 div-list)))
        (begin
          (if (> div-sum this-num)
              (begin
                #t)
              (begin
                #f
                ))
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-is-abundant-1 result-hash-table)
 (begin
   (let ((sub-name "test-is-abundant-1")
         (test-list
          (list
           (list 2 #f) (list 3 #f) (list 4 #f) (list 5 #f)
           (list 6 #f) (list 8 #f) (list 9 #f) (list 10 #f)
           (list 11 #f) (list 12 #t) (list 13 #f) (list 14 #f)
           (list 15 #f) (list 16 #f) (list 17 #f) (list 18 #t)
           (list 19 #f) (list 20 #t) (list 21 #f) (list 22 #f)
           (list 23 #f) (list 24 #t)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((test-num (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result (is-abundant? test-num)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : test-num=~a, "
                        sub-name test-label-index test-num))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (populate-abundant-hash! ab-htable max-num)
  (begin
    (do ((ii 12 (+ ii 1)))
        ((> ii max-num))
      (begin
        (if (is-abundant? ii)
            (begin
              (hash-set! ab-htable ii #t)
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
;;; define a macro to simplify code
(define-syntax two-abundant-check-macro
  (syntax-rules ()
    ((two-abundant-check-macro
      jj ab-length ab-list ab-htable
      sum sum-list debug-flag)
     (begin
       (let ((two-ab-flag #f)
             (completed-flag #f))
         (begin
           (do ((ii 0 (+ ii 1)))
               ((or (>= ii ab-length)
                    (equal? completed-flag #t)))
             (begin
               (let ((this-ab (list-ref ab-list ii)))
                 (begin
                   (if (>= this-ab jj)
                       (begin
                         (set! completed-flag #t))
                       (begin
                         (let ((diff (- jj this-ab)))
                           (begin
                             (if (equal?
                                  (hash-ref ab-htable diff #f)
                                  #t)
                                 (begin
                                   (if (equal? debug-flag #t)
                                       (begin
                                         (display
                                          (ice-9-format:format
                                           #f "debug two abundant "))
                                         (display
                                          (ice-9-format:format
                                           #f "numbers sum "))
                                         (display
                                          (ice-9-format:format
                                           #f "~:d + ~:d = ~:d~%"
                                           this-ab diff jj))
                                         ))
                                   (set! two-ab-flag #t)
                                   (set! completed-flag #t)
                                   ))
                             ))
                         ))
                   ))
               ))

           (if (not (equal? two-ab-flag #t))
               (begin
                 (set! sum (+ sum jj))
                 (set! sum-list (cons jj sum-list))
                 ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop max-num debug-flag)
  (begin
    (let ((sum 0)
          (sum-list (list))
          (ab-htable (make-hash-table 10000)))
      (begin
        (populate-abundant-hash! ab-htable max-num)

        (let ((ab-list
               (sort
                (hash-map->list
                 (lambda (key value)
                   (begin
                     key)) ab-htable)
                <)))
          (let ((min-ab-value
                 (list-ref ab-list 0))
                (max-ab-value
                 (list-ref
                  ab-list
                  (- (length ab-list) 1)))
                (ab-length (length ab-list))
                (two-min-ab-value
                 (* 2 (list-ref ab-list 0))))
            (begin
              (do ((jj 1 (+ jj 1)))
                  ((> jj max-num))
                (begin
                  (cond
                   ((< jj two-min-ab-value)
                    (begin
                      (set! sum (+ sum jj))
                      (set! sum-list (cons jj sum-list))
                      ))
                   (else
                    (begin
                      (two-abundant-check-macro
                       jj ab-length ab-list ab-htable
                       sum sum-list debug-flag)
                      )))
                  ))

              (if (equal? debug-flag #t)
                  (begin
                    (let ((tsum (srfi-1:fold + 0 sum-list)))
                      (begin
                        (if (not (equal? tsum sum))
                            (begin
                              (display
                               (ice-9-format:format
                                #f "warning: sum of list ~:d is not~%"
                                tsum))
                              (display
                               (ice-9-format:format
                                #f "equal to accumulated sum ~:d~%"
                                sum))
                              ))
                        (display
                         (ice-9-format:format
                          #f "sum of all positive integers less "))
                        (display
                         (ice-9-format:format
                          #f "than ~:d that are not the sum of "
                          max-num))
                        (display
                         (ice-9-format:format
                          #f "two abundant numbers : ~a = ~:d~%"
                          (list-to-sum-string
                           (reverse sum-list)) sum))
                        )))
                  (begin
                    (display
                     (ice-9-format:format
                      #f "~:d is the sum of all positive "
                      sum))
                    (display
                     (ice-9-format:format
                      #f "integers less than ~:d that are~%"
                      max-num))
                    (display
                     (ice-9-format:format
                      #f "not the sum of two abundant numbers~%"))
                    ))

              (force-output)
              )))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "A perfect number is a number "))
    (display
     (format #f "for which the~%"))
    (display
     (format #f "sum of its proper divisors is "))
    (display
     (format #f "exactly equal to~%"))
    (display
     (format #f "the number. For example, the sum "))
    (display
     (format #f "of the proper~%"))
    (display
     (format #f "divisors of 28 would be "))
    (display
     (format #f "1 + 2 + 4 + 7 + 14 = 28,~%"))
    (display
     (format #f "which means that 28 is a "))
    (display
     (format #f "perfect number.~%"))
    (newline)
    (display
     (format #f "A number n is called deficient "))
    (display
     (format #f "if the sum of its~%"))
    (display
     (format #f "proper divisors is less than n and "))
    (display
     (format #f "it is called~%"))
    (display
     (format #f "abundant if this sum exceeds n.~%"))
    (newline)
    (display
     (format #f "As 12 is the smallest abundant number,~%"))
    (display
     (format #f "1 + 2 + 3 + 4 + 6 = 16,~%"))
    (display
     (format #f "the smallest number that can be "))
    (display
     (format #f "written as the~%"))
    (display
     (format #f "sum of two abundant numbers is 24. By "))
    (display
     (format #f "mathematical analysis,~%"))
    (display
     (format #f "it can be shown that all integers "))
    (display
     (format #f "greater than 28123~%"))
    (display
     (format #f "can be written as the sum of two "))
    (display
     (format #f "abundant numbers.~%"))
    (display
     (format #f "However, this upper limit cannot "))
    (display
     (format #f "be reduced any~%"))
    (display
     (format #f "further by analysis even though it "))
    (display
     (format #f "is known that~%"))
    (display
     (format #f "the greatest number that cannot be "))
    (display
     (format #f "expressed as the~%"))
    (display
     (format #f "sum of two abundant numbers is "))
    (display
     (format #f "less than this limit.~%"))
    (newline)
    (display
     (format #f "Find the sum of all the positive "))
    (display
     (format #f "integers which cannot~%"))
    (display
     (format #f "be written as the sum of two "))
    (display
     (format #f "abundant numbers.~%"))
    (display
     (format #f "see https://projecteuler.net/problem=23~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((max-num 30)
          (debug-flag #t))
      (begin
        (sub-main-loop max-num debug-flag)
        ))

    (newline)
    (let ((max-num 28124)
          (debug-flag #f))
      (begin
        (sub-main-loop max-num debug-flag)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 23 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 30                                     ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated June 11, 2022                                ###
;;;###                                                       ###
;;;###  updated March 6, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### srfi-1 for fold functions
(use-modules ((srfi srfi-1)
              :renamer (symbol-prefix-proc 'srfi-1:)))

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### digits-module for split-digits-list function
(use-modules ((digits-module)
              :renamer (symbol-prefix-proc 'digits-module:)))

;;;### timer-module for time-code function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (integer-power this-number this-exponent)
  (begin
    (cond
     ((= this-exponent 0)
      (begin
        1
        ))
     ((= this-exponent 1)
      (begin
        this-number
        ))
     ((< this-exponent 0)
      (begin
        -1
        ))
     (else
      (begin
        (let ((result-num this-number)
              (max-iter (- this-exponent 1)))
          (begin
            (do ((ii 0 (+ ii 1)))
                ((>= ii max-iter))
              (begin
                (set! result-num (* result-num this-number))
                ))
            result-num
            ))
        )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-integer-power-1 result-hash-table)
 (begin
   (let ((sub-name "test-integer-power-1")
         (test-list
          (list
           (list 10 0 1) (list 11 0 1) (list 12 0 1)
           (list 10 1 10) (list 11 1 11) (list 12 1 12)
           (list 10 2 100) (list 11 2 121) (list 12 2 144)
           (list 2 2 4) (list 2 3 8) (list 2 4 16) (list 2 5 32)
           (list 2 6 64) (list 2 7 128) (list 2 8 256)
           (list 2 9 512) (list 2 10 1024)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-num (list-ref alist 0))
                  (test-exp (list-ref alist 1))
                  (shouldbe-num (list-ref alist 2)))
              (let ((result-num (integer-power test-num test-exp)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "test-num=~a, test-exp=~a, "
                        test-num test-exp))
                      (err-3
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe-num result-num)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-num result-num)
                     sub-name
                     (string-append
                      err-1 err-2 err-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (calc-sum-digits this-num digit-power)
  (begin
    (let ((dlist
           (digits-module:split-digits-list this-num)))
      (let ((d2list
             (map
              (lambda (this-digit)
                (begin
                  (integer-power this-digit digit-power)
                  )) dlist)))
        (begin
          (srfi-1:fold + 0 d2list)
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-calc-sum-digits-1 result-hash-table)
 (begin
   (let ((sub-name "test-calc-sum-digits-1")
         (test-list
          (list
           (list 10 1 1) (list 11 1 2) (list 12 1 3)
           (list 13 1 4) (list 14 1 5)
           (list 21 2 5) (list 22 2 8) (list 23 2 13)
           (list 12 3 9) (list 1634 4 1634)
           (list 8208 4 8208) (list 9474 4 9474)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-num (list-ref alist 0))
                  (test-exp (list-ref alist 1))
                  (shouldbe-num (list-ref alist 2)))
              (let ((result-num
                     (calc-sum-digits test-num test-exp)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : test-num=~a, "
                        sub-name test-label-index test-num))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a~%"
                        shouldbe-num result-num)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-num result-num)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (list-to-sum-string llist)
  (begin
    (let ((stmp
           (string-join
            (map
             (lambda (this-num)
               (begin
                 (ice-9-format:format #f "~:d" this-num)
                 )) llist)
            " + ")))
      (begin
        stmp
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (test-list-to-sum-string-1)
  (begin
    (let ((sub-name "test-list-to-sum-string-1")
          (test-list
           (list
            (list (list 1) "1")
            (list (list 1 2) "1 + 2")
            (list (list 1 2 3) "1 + 2 + 3")
            (list (list 4 5 6 7) "4 + 5 + 6 + 7")
            ))
          (test-label-index 0))
      (begin
        (for-each
         (lambda (this-list)
           (begin
             (let ((input-list (list-ref this-list 0))
                   (shouldbe-string (list-ref this-list 1)))
               (let ((result-string (list-to-sum-string input-list)))
                 (let ((err-1
                        (format
                         #f "~a :: (~a) error : input-list=~a, "
                         sub-name test-label-index input-list))
                       (err-2
                        (format
                         #f "shouldbe=~s, result=~s"
                         shouldbe-string result-string)))
                   (begin
                     (unittest2:assert?
                      (string-ci=? shouldbe result)
                      sub-name
                      (string-append err-1 err-2)
                      result-hash-table)
                     ))
                 ))
             (set! test-label-index (1+ test-label-index))
             )) test-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
;;; define a macro to simplify code
(define-syntax process-jj-macro
  (syntax-rules ()
    ((process-jj-macro
      jj digit-power sum sum-list debug-flag)
     (begin
       (let ((this-sum
              (calc-sum-digits jj digit-power)))
         (begin
           (if (equal? jj this-sum)
               (begin
                 (set! sum (+ sum this-sum))
                 (set! sum-list (cons jj sum-list))
                 (if (equal? debug-flag #t)
                     (begin
                       (let ((dlist
                              (digits-module:split-digits-list jj)))
                         (let ((digit-power-string
                                (string-join
                                 (map
                                  (lambda(this-elem)
                                    (begin
                                      (ice-9-format:format
                                       #f "~:d^~:d"
                                       this-elem digit-power)
                                      )) dlist)
                                 " + "))
                               (sum-string
                                (list-to-sum-string
                                 (reverse sum-list))))
                           (begin
                             (display
                              (ice-9-format:format
                               #f "~:d = ~a  : "
                               jj digit-power-string))
                             (display
                              (ice-9-format:format
                               #f "sum so far ~a = ~:d~%"
                               sum-string sum))
                             (force-output)
                             )))
                       ))
                 ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop max-num max-limit digit-power debug-flag)
  (begin
    (let ((sum 0)
          (sum-list (list)))
      (begin
        (do ((jj 2 (+ jj 1)))
            ((> jj max-limit))
          (begin
            (process-jj-macro
             jj digit-power sum sum-list debug-flag)
            ))

        (let ((sum-string
               (list-to-sum-string (reverse sum-list))))
          (begin
            (display
             (ice-9-format:format
              #f "sum of all numbers that can be written "))
            (display
             (ice-9-format:format
              #f "as the ~a power is ~:d = ~a  (less than ~:d)~%"
              digit-power sum sum-string max-num))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Surprisingly there are only three "))
    (display
     (format #f "numbers that can~%"))
    (display
     (format #f "be written as the sum of fourth "))
    (display
     (format #f "powers of their~%"))
    (display
     (format #f "digits:~%"))
    (newline)
    (display
     (format #f "1634 = 1^4 + 6^4 + 3^4 + 4^4~%"))
    (display
     (format #f "8208 = 8^4 + 2^4 + 0^4 + 8^4~%"))
    (display
     (format #f "9474 = 9^4 + 4^4 + 7^4 + 4^4~%"))
    (newline)
    (display
     (format #f "As 1 = 1^4 is not a sum it is not "))
    (display
     (format #f "included.~%"))
    (display
     (format #f "The sum of these numbers is~%"))
    (display
     (format #f "1634 + 8208 + 9474 = 19316.~%"))
    (newline)
    (display
     (format #f "Find the sum of all the numbers that "))
    (display
     (format #f "can be written~%"))
    (display
     (format #f "as the sum of fifth powers of "))
    (display
     (format #f "their digits.~%"))
    (newline)
    (display
     (format #f "To reduce the amount of work that "))
    (display
     (format #f "needs to be done,~%"))
    (display
     (format #f "we restrict the maximum number to be "))
    (display
     (format #f "354,294 = 6*(9^5),~%"))
    (display
     (format #f "since if you split a 6 digit number "))
    (display
     (format #f "that's the largest~%"))
    (display
     (format #f "number that can be attained.~%"))
    (display
     (format #f "see https://projecteuler.net/problem=30~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((max-num 100000)
          (max-limit 32085)
          (digit-power 4)
          (debug-flag #t))
      (begin
        (sub-main-loop
         max-num max-limit digit-power debug-flag)
        ))


    (let ((max-num 1000000)
          (max-limit 354294)
          (digit-power 5)
          (debug-flag #t))
      (begin
        (sub-main-loop
         max-num max-limit digit-power debug-flag)
        ))

    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 30 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (load "digits-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

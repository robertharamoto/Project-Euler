#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 28                                     ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated June 11, 2022                                ###
;;;###                                                       ###
;;;###  updated March 4, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (make-spiral-array! this-array max-row max-col)
  (begin
    (let ((center-row (euclidean/ max-row 2))
          (center-col (euclidean/ max-col 2))
          (arm-length 1)
          (arm-count 0)
          (current-direction 1)
          (num-counter 1)
          (end-flag #f))
      (let ((this-row center-row)
            (this-col center-col))
        (begin
          (array-set!
           this-array num-counter this-row this-col)
          (set! num-counter (1+ num-counter))

          (while
           (and (equal? end-flag #f)
                (<= arm-length max-row)
                (<= arm-length max-col))
           (begin
             (cond
              ((or (> this-row max-row)
                   (> this-col max-col))
               (begin
                 (set! end-flag #t)
                 ))
              ((= current-direction 1)
               (begin
                  ;;; then we are incrementing columns
                 (do ((ii 0 (+ ii 1)))
                     ((or (>= ii arm-length)
                          (>= (+ this-col 1) max-col)))
                   (begin
                     (let ((next-col (+ this-col 1)))
                       (begin
                         (if (zero?
                              (array-ref this-array this-row next-col))
                             (begin
                               (array-set!
                                this-array
                                num-counter this-row next-col)
                               (set! num-counter (1+ num-counter))
                               (set! this-col next-col)
                               ))
                         ))
                     ))
                 (set! current-direction 2)
                 (set! arm-count (1+ arm-count))
                 (if (>= arm-count 2)
                     (begin
                       (set! arm-count 0)
                       (set! arm-length (1+ arm-length))
                       ))
                 ))
              ((= current-direction -1)
               (begin
                  ;;; then we are decrementing columns
                 (do ((ii 0 (+ ii 1)))
                     ((or (>= ii arm-length)
                          (< this-col 0)))
                   (begin
                     (let ((next-col (- this-col 1)))
                       (begin
                         (if (zero?
                              (array-ref this-array this-row next-col))
                             (begin
                               (array-set!
                                this-array num-counter this-row next-col)
                               (set! num-counter (1+ num-counter))
                               (set! this-col next-col)
                               ))
                         ))
                     ))
                 (set! current-direction -2)
                 (set! arm-count (1+ arm-count))
                 (if (>= arm-count 2)
                     (begin
                       (set! arm-count 0)
                       (set! arm-length (1+ arm-length))
                       ))
                 ))
              ((= current-direction 2)
               (begin
                  ;;; then we are incrementing rows
                 (do ((ii 0 (+ ii 1)))
                     ((or (>= ii arm-length)
                          (>= (+ this-row 1) max-row)))
                   (begin
                     (let ((next-row (+ this-row 1)))
                       (begin
                         (if (zero?
                              (array-ref
                               this-array next-row this-col))
                             (begin
                               (array-set!
                                this-array
                                num-counter next-row this-col)
                               (set! num-counter (1+ num-counter))
                               (set! this-row next-row)
                               ))
                         ))
                     ))

                 (set! current-direction -1)
                 (set! arm-count (1+ arm-count))
                 (if (>= arm-count 2)
                     (begin
                       (set! arm-count 0)
                       (set! arm-length (1+ arm-length))
                       ))
                 ))
              ((= current-direction -2)
               (begin
                  ;;; then we are decrementing rows
                 (do ((ii 0 (+ ii 1)))
                     ((or (>= ii arm-length)
                          (< this-row 0)))
                   (begin
                     (let ((next-row (- this-row 1)))
                       (begin
                         (if (zero?
                              (array-ref
                               this-array next-row this-col))
                             (begin
                               (array-set!
                                this-array
                                num-counter next-row this-col)
                               (set! num-counter (1+ num-counter))
                               (set! this-row next-row)
                               ))
                         ))
                     ))

                 (set! current-direction 1)
                 (set! arm-count (1+ arm-count))
                 (if (>= arm-count 2)
                     (begin
                       (set! arm-count 0)
                       (set! arm-length (1+ arm-length))
                       ))
                 ))
              (else
               (begin
                 (display
                  (format
                   #f "make-spiral-array! error: invalid "))
                 (display
                  (format
                   #f "current-direction=~a~%"
                   current-direction))
                 (quit)
                 )))
             ))
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-make-spiral-array-3-1 result-hash-table)
 (begin
   (let ((sub-name "test-make-spiral-array-3-1")
         (test-array (make-array 0 3 3))
         (shouldbe-list-list
          (list
           (list 7 8 9)
           (list 6 1 2)
           (list 5 4 3)))
         (max-row 3)
         (max-col 3))
     (let ((shouldbe-array
            (list->array 2 shouldbe-list-list)))
       (begin
         (make-spiral-array!
          test-array max-row max-col)

         (do ((ii-row 0 (+ ii-row 1)))
             ((>= ii-row max-row))
           (begin
             (do ((ii-col 0 (+ ii-col 1)))
                 ((>= ii-col max-col))
               (begin
                 (let ((result-num
                        (array-ref test-array ii-row ii-col))
                       (shouldbe-num
                        (array-ref shouldbe-array ii-row ii-col)))
                   (let ((err-1
                          (format
                           #f "~a : error : row/col=(~a/~a), "
                           sub-name ii-row ii-col))
                         (err-2
                          (format
                           #f "shouldbe=~a, result=~a~%"
                           shouldbe-num result-num)))
                     (begin
                       (unittest2:assert?
                        (equal? shouldbe-num result-num)
                        sub-name
                        (string-append err-1 err-2)
                        result-hash-table)
                       )))
                 ))
             ))
         )))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-make-spiral-array-5-1 result-hash-table)
 (begin
   (let ((sub-name "test-make-spiral-array-5-1")
         (test-array (make-array 0 5 5))
         (shouldbe-list-list
          (list
           (list 21 22 23 24 25)
           (list 20  7  8  9 10)
           (list 19  6  1  2 11)
           (list 18  5  4  3 12)
           (list 17 16 15 14 13)
           ))
         (max-row 5)
         (max-col 5))
     (let ((shouldbe-array (list->array 2 shouldbe-list-list)))
       (begin
         (make-spiral-array! test-array max-row max-col)

         (do ((ii-row 0 (+ ii-row 1)))
             ((>= ii-row max-row))
           (begin
             (do ((ii-col 0 (+ ii-col 1)))
                 ((>= ii-col max-col))
               (begin
                 (let ((result-num
                        (array-ref test-array ii-row ii-col))
                       (shouldbe-num
                        (array-ref shouldbe-array ii-row ii-col)))
                   (let ((err-1
                          (format
                           #f "~a : error : row/col=(~a/~a), "
                           sub-name ii-row ii-col))
                         (err-2
                          (format
                           #f "shouldbe=~a, result=~a~%"
                           shouldbe-num result-num)))
                     (begin
                       (unittest2:assert?
                        (equal? shouldbe-num result-num)
                        sub-name
                        (string-append err-1 err-2)
                        result-hash-table)
                       )))
                 ))
             ))
         )))
   ))

;;;#############################################################
;;;#############################################################
(define (display-array this-array max-row max-col)
  (begin
    (cond
     ((not (array? this-array))
      (begin
        (display
         (format
          #f "display-array error: expecting array, "))
        (display
         (format
          #f "instead received ~a~%"
          this-array))
        (quit)
        ))
     (else
      (begin
        (do ((ii-row 0 (+ ii-row 1)))
            ((>= ii-row max-row))
          (begin
            (do ((ii-col 0 (+ ii-col 1)))
                ((>= ii-col max-col))
              (begin
                (display
                 (ice-9-format:format
                  #f "~4:d "
                  (array-ref this-array ii-row ii-col)))
                ))
            (newline)
            ))
        (force-output)
        )))
    ))

;;;#############################################################
;;;#############################################################
;;; assume square array
(define (diag-sum this-array max-row max-col)
  (begin
    (let ((sum 0)
          (center-row (euclidean/ max-row 2))
          (center-col (euclidean/ max-col 2))
          (max-index (- max-row 1)))
      (begin
        (do ((ii-row 0 (+ ii-row 1)))
            ((>= ii-row max-row))
          (begin
            (let ((jj-row (- max-index ii-row)))
              (let ((elem-1 (array-ref this-array ii-row ii-row))
                    (elem-2 (array-ref this-array ii-row jj-row)))
                (begin
                  ;;; don't double count the center row
                  (if (equal? ii-row center-row)
                      (begin
                        (set! sum (+ sum elem-1)))
                      (begin
                        (set! sum (+ sum elem-1 elem-2))
                        ))
                  )))
            ))
        sum
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-diag-sum-3-1 result-hash-table)
 (begin
   (let ((sub-name "test-diag-sum-3-1")
         (test-array (make-array 0 3 3))
         (shouldbe-list-list
          (list
           (list 7 8 9)
           (list 6 1 2)
           (list 5 4 3)))
         (max-row 3)
         (max-col 3)
         (shouldbe-num 25))
     (let ((shouldbe-array
            (list->array 2 shouldbe-list-list)))
       (begin
         (make-spiral-array! test-array max-row max-col)

         (let ((result-num
                (diag-sum test-array max-row max-col)))
           (let ((err-1
                  (format
                   #f "~a : error, diagonal sum, "
                   sub-name))
                 (err-2
                  (format
                   #f "shouldbe=~a, result=~a~%"
                   shouldbe-num result-num)))
             (begin
               (unittest2:assert?
                (equal? shouldbe-num result-num)
                sub-name
                (string-append err-1 err-2)
                result-hash-table)
               )))
         )))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-diag-sum-5-1 result-hash-table)
 (begin
   (let ((sub-name "test-diag-sum-5-1")
         (test-array (make-array 0 5 5))
         (shouldbe-list-list
          (list
           (list 21 22 23 24 25)
           (list 20  7  8  9 10)
           (list 19  6  1  2 11)
           (list 18  5  4  3 12)
           (list 17 16 15 14 13)
           ))
         (max-row 5)
         (max-col 5)
         (shouldbe-num 101))
     (let ((shouldbe-array (list->array 2 shouldbe-list-list)))
       (begin
         (make-spiral-array! test-array max-row max-col)

         (let ((result-num (diag-sum test-array max-row max-col)))
           (let ((err-1
                  (format
                   #f "~a : error, diagnol sum, "
                   sub-name))
                 (err-2
                  (format
                   #f "shouldbe=~a, result=~a~%"
                   shouldbe-num result-num)))
             (begin
               (unittest2:assert?
                (equal? shouldbe-num result-num)
                sub-name
                (string-append err-1 err-2)
                result-hash-table)
               )))
         )))
   ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop max-row max-col debug-flag)
  (begin
    (let ((this-array (make-array 0 max-row max-col)))
      (begin
        (make-spiral-array! this-array max-row max-col)

        (let ((this-sum
               (diag-sum this-array max-row max-col)))
          (begin
            (display
             (ice-9-format:format
              #f "max-row=~:d, max-col=~:d~%"
              max-row max-col))
            (if (equal? debug-flag #t)
                (begin
                  (display-array this-array max-row max-col)
                  ))

            (display
             (ice-9-format:format
              #f "the sum of the numbers on the diagonals "))
            (display
             (ice-9-format:format
              #f "is ~:d~%" this-sum))
            (force-output)
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Starting with the number 1 and "))
    (display
     (format #f "moving to the~%"))
    (display
     (format #f "right in a clockwise direction a "))
    (display
     (format #f "5 by 5 spiral~%"))
    (display
     (format #f "is formed as follows:~%"))
    (newline)
    (display
     (format #f " 21 22 23 24 25~%"))
    (display
     (format #f " 20  7  8  9 10~%"))
    (display
     (format #f " 19  6  1  2 11~%"))
    (display
     (format #f " 18  5  4  3 12~%"))
    (display
     (format #f " 17 16 15 14 13~%"))
    (newline)
    (display
     (format #f "It can be verified that the sum "))
    (display
     (format #f "of the numbers~%"))
    (display
     (format #f "on the diagonals is 101.~%"))
    (newline)
    (display
     (format #f "What is the sum of the numbers on "))
    (display
     (format #f "the diagonals in a~%"))
    (display
     (format #f "1001 by 1001 spiral formed in the same way?~%"))
    (display
     (format #f "see https://projecteuler.net/problem=28~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((max-row 5)
          (max-col 5)
          (debug-flag #t))
      (begin
        (sub-main-loop max-row max-col debug-flag)
        ))

    (let ((max-row 1001)
          (max-col 1001)
          (debug-flag #f))
      (begin
        (sub-main-loop max-row max-col debug-flag)
        (newline)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 28 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

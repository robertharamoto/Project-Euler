#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 24                                     ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated June 11, 2022                                ###
;;;###                                                       ###
;;;###  updated March 4, 2020                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (reverse-vector-k-to-n this-vector k)
  (begin
    (let ((vlen (vector-length this-vector))
          (result-vector (vector-copy this-vector)))
      (let ((index-diff (- vlen k)))
        (begin
          (cond
           ((< index-diff 0)
            (begin
              result-vector
              ))
           (else
            (begin
              (let ((ii1 k)
                    (ii2 (- vlen 1))
                    (half-diff
                     (euclidean/ index-diff 2)))
                (begin
                  (do ((jj 0 (+ jj 1)))
                      ((or (>= jj half-diff)
                           (>= ii1 ii2)
                           (>= ii1 vlen)
                           (< ii2 0)
                           ))
                    (begin
                      (let ((v1 (vector-ref result-vector ii1))
                            (v2 (vector-ref result-vector ii2)))
                        (begin
                          (vector-set! result-vector ii1 v2)
                          (vector-set! result-vector ii2 v1)
                          (set! ii1 (+ ii1 1))
                          (set! ii2 (- ii2 1))
                          ))
                      ))
                  result-vector
                  ))
              )))
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (assert-two-lists-equal
         shouldbe-list result-list
         sub-name err-start
         result-hash-table)
  (begin
    (let ((shouldbe-length (length shouldbe-list))
          (result-length (length result-list)))
      (let ((err-1
             (format
              #f "~a : shouldbe-list=~a, result-list=~a : "
              err-start shouldbe-list result-list))
            (err-2
             (format
              #f "lengths differ shouldbe=~a, result=~a"
              shouldbe-length result-length)))
        (begin
          (unittest2:assert?
           (equal? shouldbe-length result-length)
           sub-name
           (string-append err-1 err-2)
           result-hash-table)

          (if (> shouldbe-length 0)
              (begin
                (for-each
                 (lambda (s-elem)
                   (begin
                     (let ((mflag (member s-elem result-list))
                           (err-3
                            (format #f "missing element ~a" s-elem)))
                       (begin
                         (unittest2:assert?
                          (not (equal? mflag #f))
                          sub-name
                          (string-append err-1 err-3)
                          result-hash-table)
                         ))
                     )) shouldbe-list))
              (begin
                (unittest2:assert?
                 (equal? shouldbe-list result-list)
                 sub-name
                 (string-append err-1 err-2)
                 result-hash-table)
                ))
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-reverse-vector-k-to-n-1 result-hash-table)
 (begin
   (let ((sub-name "test-reverse-vector-k-to-n-1")
         (test-list
          (list
           (list (vector 0 1 2) 1 (vector 0 2 1))
           (list (vector 0 1 2) 0 (vector 2 1 0))
           (list (vector 0 1 2 3) 2 (vector 0 1 3 2))
           (list (vector 0 1 2 3) 1 (vector 0 3 2 1))
           (list (vector 0 1 2 3) 0 (vector 3 2 1 0))
           (list (vector 0 1 2 3 4) 3 (vector 0 1 2 4 3))
           (list (vector 0 1 2 3 4) 2 (vector 0 1 4 3 2))
           (list (vector 0 1 2 3 4) 1 (vector 0 4 3 2 1))
           (list (vector 0 1 2 3 4) 0 (vector 4 3 2 1 0))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((test-vec (list-ref this-list 0))
                  (test-ii (list-ref this-list 1))
                  (shouldbe-vec (list-ref this-list 2)))
              (let ((result-vec (reverse-vector-k-to-n test-vec test-ii)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : test-vec~a, test-ii=~a, "
                        sub-name test-label-index test-vec test-ii)))
                  (begin
                    (assert-two-lists-equal
                     (vector->list shouldbe-vec)
                     (vector->list result-vec)
                     sub-name
                     err-1
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; note assumes that this-vector is sorted in ascending order
(define (next-lexicographic-permutation this-vector)
  (begin
    (let ((vlen (vector-length this-vector))
          (result-vector (vector-copy this-vector))
          (kk 0)
          (aakk 0)
          (ll 0)
          (aall 0)
          (break-flag #f)
          (permutation-exists #f))
      (begin
        ;;; 1) find largest kk such that a[kk] < a[kk+1]
        (do ((ii 0 (+ ii 1)))
            ((>= ii (- vlen 1)))
          (begin
            (let ((v1 (vector-ref result-vector ii))
                  (v2 (vector-ref result-vector (+ ii 1))))
              (begin
                (if (< v1 v2)
                    (begin
                      (set! permutation-exists #t)
                      (set! kk ii)
                      (set! aakk v1)
                      ))
                ))
            ))

        ;;; 2) find the largest ll such that a[kk] < a[ll]
        (if (equal? permutation-exists #t)
            (begin
              (do ((ii (+ kk 1) (+ ii 1)))
                  ((> ii (- vlen 1)))
                (begin
                  (let ((v1 (vector-ref result-vector ii)))
                    (begin
                      (if (< aakk v1)
                          (begin
                            (set! ll ii)
                            (set! aall v1)
                            ))
                      ))
                  ))

              ;;; 3) swap a[kk] with a[ll]
              (vector-set! result-vector kk aall)
              (vector-set! result-vector ll aakk)

              ;;; 4) reverse the sequence from (k+1) on
              (let ((final-result
                     (reverse-vector-k-to-n result-vector (+ kk 1))))
                (begin
                  final-result
                  )))
            (begin
              result-vector
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-next-lexicographic-permutation-1 result-hash-table)
 (begin
   (let ((sub-name "test-next-lexicographic-permutation-1")
         (test-list
          (list
           (list (vector 0 1 2) (vector 0 2 1))
           (list (vector 0 2 1) (vector 1 0 2))
           (list (vector 1 0 2) (vector 1 2 0))
           (list (vector 1 2 0) (vector 2 0 1))
           (list (vector 2 0 1) (vector 2 1 0))
           (list (vector 0 1 2 3 4 5) (vector 0 1 2 3 5 4))
           (list (vector 0 1 2 3 5 4) (vector 0 1 2 4 3 5))
           (list (vector 0 1 2 4 3 5) (vector 0 1 2 4 5 3))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((test-vec (list-ref this-list 0))
                  (shouldbe-vec (list-ref this-list 1)))
              (let ((result-vec (next-lexicographic-permutation test-vec)))
                (let ((err-1
                       (format
                        #f "~a :: (~a) error : test-vec=~a"
                        sub-name test-label-index test-vec)))
                  (begin
                    (assert-two-lists-equal
                     (vector->list shouldbe-vec)
                     (vector->list result-vec)
                     sub-name
                     err-1
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; ensure that the vector is sorted
(define (sub-main-loop this-list max-num)
  (begin
    (let ((this-vector
           (list->vector (sort this-list <))))
      (begin
        (do ((jj 1 (+ jj 1)))
            ((>= jj max-num))
          (begin
            (let ((result-vector
                   (next-lexicographic-permutation this-vector)))
              (begin
                (set!
                 this-vector
                 (vector-copy result-vector))
                ))
            ))

        (let ((rlist (vector->list this-vector)))
          (begin
            (display
             (ice-9-format:format
              #f "the ~:d permuation of ~a is ~a~%"
              max-num this-list rlist))
            (force-output)
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "A permutation is an ordered arrangement "))
    (display
     (format #f "of objects.~%"))
    (display
     (format #f "For example, 3124 is one possible "))
    (display
     (format #f "permutation of the~%"))
    (display
     (format #f "digits 1, 2, 3 and 4. If all of the "))
    (display
     (format #f "permutations~%"))
    (display
     (format #f "are listed numerically alphabetically, "))
    (display
     (format #f "we call it~%"))
    (display
     (format #f "lexicographic order. The lexicographic "))
    (display
     (format #f "permutations~%"))
    (display
     (format #f "of 0, 1 and 2 are:~%"))
    (display
     (format #f "    012   021   102   "))
    (display
     (format #f "120   201   210~%"))
    (newline)
    (display
     (format #f "What is the millionth lexicographic "))
    (display
     (format #f "permutation of~%"))
    (display
     (format #f "the digits 0, 1, 2, 3, 4, 5, 6, "))
    (display
     (format #f "7, 8 and 9?~%"))
    (newline)
    (display
     (format #f "see the permutation wiki page "))
    (display
     (format #f "for a nice~%"))
    (display
     (format #f "discussion of how to compute "))
    (display
     (format #f "lexicographic~%"))
    (display
     (format #f "order https://en.wikipedia.org/wiki/Permutation~%"))
    (display
     (format #f "see https://projecteuler.net/problem=24~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((this-list (list 0 1 2))
          (max-perm 6))
      (begin
        (do ((ii 1 (+ ii 1)))
            ((>= ii max-perm))
          (begin
            (sub-main-loop this-list ii)
            ))
        ))

    (let ((this-list (list 0 1 2 3 4 5 6 7 8 9))
          (nth 1000000))
      (begin
        (sub-main-loop this-list nth)
        (newline)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 24 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 131                                    ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated July 31, 2022                                ###
;;;###                                                       ###
;;;###  updated March 12, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### prime-module for is-prime? function
(use-modules ((prime-module)
              :renamer (symbol-prefix-proc 'prime-module:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define-syntax process-debug-macro
  (syntax-rules ()
    ((process-debug-macro last-cube cube-diff one-third)
     (begin
       (let ((nn last-cube)
             (nn-2 (* last-cube last-cube)))
         (let ((yy-3
                (+ (* nn nn-2) (* nn-2 cube-diff))))
           (let ((yy-root
                  (inexact->exact
                   (truncate (+ 0.50 (expt yy-3 one-third))))))
             (begin
               (display
                (ice-9-format:format
                 #f "  ~:d^3 + ~:d^2 x ~:d = ~:d^3~%"
                 nn nn cube-diff yy-root))
               ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop max-num max-prime debug-flag)
  (begin
    (let ((prime-array
           (prime-module:make-prime-array max-prime))
          (prime-count 0)
          (continue-loop-flag #t)
          (last-jj 1)
          (last-cube 1)
          (one-third (/ 1.0 3.0)))
      (begin
        (do ((jj 2 (1+ jj)))
            ((or (> jj max-num)
                 (equal? continue-loop-flag #f)))
          (begin
            (let ((this-cube (* jj jj jj)))
              (let ((cube-diff (- this-cube last-cube)))
                (begin
                  (if (> cube-diff max-num)
                      (begin
                        (set! continue-loop-flag #f))
                      (begin
                        (if (prime-module:is-array-prime?
                             cube-diff prime-array)
                            (begin
                              (set! prime-count (1+ prime-count))

                              (if (equal? debug-flag #t)
                                  (begin
                                    (process-debug-macro
                                     last-cube cube-diff one-third)
                                    ))
                              ))
                        ))

                  (set! last-jj jj)
                  (set! last-cube this-cube)
                  )))
            ))

        (display
         (ice-9-format:format
          #f "Number of primes = ~:d, with the property "
          prime-count))
        (display
         (ice-9-format:format
          #f "n^3 + n^2*prime = m^3 (for primes less than ~:d).~%"
          max-num))
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "There are some prime values, p, "))
    (display
     (format #f "for which there~%"))
    (display
     (format #f "exists a positive integer, n, such "))
    (display
     (format #f "that the expression~%"))
    (display
     (format #f "n^3 + n^2p is a perfect cube.~%"))
    (newline)
    (display
     (format #f "For example, when p = 19, "))
    (display
     (format #f "8^3 + 8^2x19 = 12^3~%"))
    (newline)
    (display
     (format #f "What is perhaps most surprising is "))
    (display
     (format #f "that for each prime~%"))
    (display
     (format #f "with this property the value of n is "))
    (display
     (format #f "unique, and there~%"))
    (display
     (format #f "are only four such primes below "))
    (display
     (format #f "one-hundred.~%"))
    (newline)
    (display
     (format #f "How many primes below one million "))
    (display
     (format #f "have this remarkable~%"))
    (display
     (format #f "property?~%"))
    (newline)
    (display
     (format #f "The solution was found at~%"))
    (display
     (format #f "http://wiki.san-ss.com.ar/project-euler-problem-131~%"))
    (newline)
    (display
     (format #f "Since n^3 + n^2p = y^3, for some "))
    (display
     (format #f "positive integer~%"))
    (display
     (format #f "n, y, and prime p. Re-writing, n^2(n+p) "))
    (display
     (format #f "= y^3, which~%"))
    (display
     (format #f "implies that n a cube, and (n+p) a cube. "))
    (display
     (format #f "Let n = b^3,~%"))
    (display
     (format #f "then (b^3+p) a cube if p=c^3-b^3. However, "))
    (display
     (format #f "since (c^3-b^3)=~%"))
    (display
     (format #f "(c-b)(c^2+bc+b^2), p a prime only if "))
    (display
     (format #f "c-b=1, so that~%"))
    (display
     (format #f "p has only one factor.~%"))
    (display
     (format #f "see https://projecteuler.net/problem=131~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((max-num 100)
          (max-prime 100)
          (debug-flag #t))
      (begin
        (sub-main-loop max-num max-prime debug-flag)
        ))

    (newline)
    (let ((max-num 1000000)
          (max-prime 100000)
          (debug-flag #f))
      (begin
        (sub-main-loop max-num max-prime debug-flag)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 131 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (load "prime-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 133                                    ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated July 31, 2022                                ###
;;;###                                                       ###
;;;###  updated March 12, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### prime-module for is-prime? function
(use-modules ((prime-module)
              :renamer (symbol-prefix-proc 'prime-module:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop max-prime max-nn debug-flag)
  (begin
    (let ((sum-primes 10)
          (primes-count 3)
          (prime-array
           (prime-module:make-prime-array max-prime)))
      (let ((plen (car (array-dimensions prime-array))))
        (begin
          (do ((ii 3 (1+ ii)))
              ((>= ii plen))
            (begin
              (let ((ii-prime
                     (array-ref prime-array ii)))
                (begin
                  (let ((rr-remain
                         (modulo-expt 10 max-nn ii-prime)))
                    (begin
                      (if (not (= rr-remain 1))
                          (begin
                            (set! sum-primes (+ sum-primes ii-prime))
                            (set! primes-count (1+ primes-count))

                            (if (equal? debug-flag #t)
                                (begin
                                  (display
                                   (ice-9-format:format
                                    #f "  prime=~:d, sum-primes=~:d "
                                    ii-prime sum-primes))
                                  (display
                                   (ice-9-format:format
                                    #f ": (~:d / ~:d)~%"
                                    primes-count plen))
                                  (force-output)
                                  ))
                            ))
                      ))
                  ))
              ))

          (display
           (ice-9-format:format
            #f "Sum(primes) = ~:d, there were ~:d / ~:d primes~%"
            sum-primes primes-count plen))
          (display
           (ice-9-format:format
            #f "  that will never be a factor of R(10^n),~%"))
          (display
           (ice-9-format:format
            #f "  (for primes less than ~:d).~%"
            max-prime))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "A number consisting entirely of "))
    (display
     (format #f "ones is called a~%"))
    (display
     (format #f "repunit. We shall define R(k) "))
    (display
     (format #f "to be a repunit~%"))
    (display
     (format #f "of length k.~%"))
    (newline)
    (display
     (format #f "Let us consider repunits of the "))
    (display
     (format #f "form R(10^n).~%"))
    (newline)
    (display
     (format #f "Although R(10), R(100), or R(1000) "))
    (display
     (format #f "are not divisible~%"))
    (display
     (format #f "by 17, R(10000) is divisible by 17. "))
    (display
     (format #f "Yet there is no~%"))
    (display
     (format #f "value of n for which R(10^n) will "))
    (display
     (format #f "divide by 19. In~%"))
    (display
     (format #f "fact, it is remarkable that 11, 17, "))
    (display
     (format #f "41, and 73 are~%"))
    (display
     (format #f "the only four primes below one-hundred "))
    (display
     (format #f "that can be a~%"))
    (display
     (format #f "factor of R(10^n).~%"))
    (newline)
    (display
     (format #f "Find the sum of all the primes "))
    (display
     (format #f "below one-hundred~%"))
    (display
     (format #f "thousand that will never be a "))
    (display
     (format #f "factor of R(10^n).~%"))
    (newline)
    (display
     (format #f "The solution makes use of the "))
    (display
     (format #f "definition of~%"))
    (display
     (format #f "R(k) = (10^k - 1)/9,~%"))
    (display
     (format #f "and the modulo-expt function. It "))
    (display
     (format #f "also assumes that~%"))
    (display
     (format #f "if the prime doesn't divide R(10^20) "))
    (display
     (format #f "then it will~%"))
    (display
     (format #f "never divide any R(k). This is because "))
    (display
     (format #f "if a divides k~%"))
    (display
     (format #f "(k = a*m), then R(a) divides R(k), "))
    (display
     (format #f "since 10^a =~%"))
    (display
     (format #f "(9R(a)+1) and R(k)=(10^k - 1)/9"))
    (display
     (format #f "=(10^(a*m)-1)/9~%"))
    (display
     (format #f "=((9R(a)+1)^m-1)/9, the 1's cancel "))
    (display
     (format #f "on expansion, and~%"))
    (display
     (format #f "every term in the expansion contains "))
    (display
     (format #f "at least one power~%"))
    (display
     (format #f "of R(a).  This means that if p "))
    (display
     (format #f "divides R(10^n) for~%"))
    (display
     (format #f "some n, then p also divides R(10^m) "))
    (display
     (format #f "if n < m, since~%"))
    (display
     (format #f "R(10^n) divides R(10^m). Some properties "))
    (display
     (format #f "of repunits can~%"))
    (display
     (format #f "be found at:~%"))
    (display
     (format #f "https://oeis.org/A178070/internal~%"))
    (display
     (format #f "see https://projecteuler.net/problem=133~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((max-prime 100)
          (max-nn (inexact->exact (expt 10 20)))
          (debug-flag #t))
      (begin
        (sub-main-loop max-prime max-nn debug-flag)
        ))

    (newline)
    (let ((max-prime 100000)
          (max-nn (inexact->exact (expt 10 20)))
          (debug-flag #f))
      (begin
        (sub-main-loop max-prime max-nn debug-flag)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 133 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (load "prime-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

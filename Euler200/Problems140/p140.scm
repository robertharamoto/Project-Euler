#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 140                                    ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated July 31, 2022                                ###
;;;###                                                       ###
;;;###  updated March 12, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### srfi-1 for fold function
(use-modules ((srfi srfi-1)
              :renamer (symbol-prefix-proc 'srfi-1:)))

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
;;; initial solutions for Pell's equation a^2-5b^2 = 44
(define (find-initial-solutions)
  (begin
    (let ((nn 5)
          (diff 44)
          (max-aa 1000)
          (max-bb 1000))
      (begin
        (do ((aa 1 (1+ aa)))
            ((> aa max-aa))
          (begin
            (let ((aa-2 (* aa aa)))
              (begin
                (do ((bb 1 (1+ bb)))
                    ((> bb max-bb))
                  (begin
                    (let ((bb-2 (* nn bb bb)))
                      (begin
                        (if (= (- aa-2 bb-2) diff)
                            (begin
                              (let ((kk (/ (- aa 7) nn)))
                                (begin
                                  (if (integer? kk)
                                      (begin
                                        (display
                                         (ice-9-format:format
                                          #f "(*****) aa=~:d, bb=~:d, "
                                          aa bb))
                                        (display
                                         (ice-9-format:format
                                          #f "kk=~a~%" kk)))
                                      (begin
                                        (display
                                         (ice-9-format:format
                                          #f "  aa=~:d, bb=~:d, kk=~a~%"
                                          aa bb kk))
                                        ))
                                  (force-output)
                                  ))
                              ))
                        ))
                    ))
                ))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
;;; rational solutions to x = -(k+1)/(2(k+3)) +/- sqrt((k+1)^2+4k(k+3))/(2(k+3))
(define (quadratic-solution kk)
  (begin
    (let ((kp1 (1+ kk))
          (kp3 (+ kk 3)))
      (let ((kk-2 (* kp1 kp1))
            (fkkp3 (* 4 kk kp3)))
        (let ((square-term (+ kk-2 fkkp3)))
          (let ((sqrt-term
                 (exact-integer-sqrt square-term)))
            (begin
              (if (= square-term (* sqrt-term sqrt-term))
                  (begin
                    (let ((aterm (- sqrt-term kp1)))
                      (let ((bterm (/ aterm (* 2 kp3))))
                        (begin
                          bterm
                          ))
                      ))
                  (begin
                    #f
                    ))
              )))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-quadratic-solution-1 result-hash-table)
 (begin
   (let ((sub-name "test-quadratic-solution-1")
         (test-list
          (list
           (list 1 #f) (list 2 2/5) (list 3 #f)
           (list 4 #f) (list 5 1/2)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((kk (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result (quadratic-solution kk)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : kk=~a, "
                        sub-name test-label-index kk))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; cc and dd solve the Pell's equation cc^2 - 5d^2 = 1
;;; aa and bb solve the equation aa^2 - 5bb^2 = 45
(define (loop-over-positives
         aa-0 bb-0 cc-0 dd-0 nn max-count)
  (begin
    (let ((result-list (list))
          (aa-1 aa-0)
          (bb-1 bb-0)
          (cc-1 cc-0)
          (dd-1 dd-0)
          (count 0)
          (continue-loop-flag #t))
      (begin
        (while
         (equal? continue-loop-flag #t)
         (begin
           (let ((kk (/ (- aa-1 7) nn))
                 (next-cc-p (+ (* cc-0 cc-1) (* nn dd-0 dd-1)))
                 (next-dd-p (+ (* cc-0 dd-1) (* dd-0 cc-1)))
                 (next-aa-p (+ (* aa-0 cc-1) (* nn bb-0 dd-1)))
                 (next-bb-p (+ (* aa-0 dd-1) (* cc-1 bb-0))))
             (begin
               (if (and (integer? kk) (> kk 0))
                   (begin
                     (set! result-list (cons kk result-list))
                     (set! count (1+ count))
                     ))

               (set! cc-1 next-cc-p)
               (set! dd-1 next-dd-p)

               (set! aa-1 next-aa-p)
               (set! bb-1 next-bb-p)

               (if (>= count max-count)
                   (begin
                     (set! continue-loop-flag #f)
                     ))
               ))
           ))

        result-list
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-loop-over-positives-1 result-hash-table)
 (begin
   (let ((sub-name "test-loop-over-positives-1")
         (test-list
          (list
           (list 13 5 9 4 5 2 (list 13970 42))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((aa-0 (list-ref this-list 0))
                  (bb-0 (list-ref this-list 1))
                  (cc-0 (list-ref this-list 2))
                  (dd-0 (list-ref this-list 3))
                  (nn (list-ref this-list 4))
                  (max-num (list-ref this-list 5))
                  (shouldbe-list (list-ref this-list 6)))
              (let ((result-list
                     (loop-over-positives
                      aa-0 bb-0 cc-0 dd-0 nn max-num)))
                (let ((slen (length shouldbe-list))
                      (rlen (length result-list)))
                  (let ((err-1
                         (format
                          #f "~a : error (~a) : aa-0=~a, bb-0=~a, "
                          sub-name test-label-index aa-0 bb-0))
                        (err-2
                         (format
                          #f "cc-0=~a, dd-0=~a, nn=~a, max-num=~a, "
                          cc-0 dd-0 nn max-num))
                        (err-3
                         (format
                          #f "shouldbe length=~a, result length=~a"
                          slen rlen)))
                    (begin
                      (unittest2:assert?
                       (equal? slen rlen)
                       sub-name
                       (string-append err-1 err-2 err-3)
                       result-hash-table)

                      (for-each
                       (lambda (s-elem)
                         (begin
                           (let ((err-4
                                  (format
                                   #f "shouldbe element=~a, result=~a"
                                   s-elem result-list)))
                             (begin
                               (unittest2:assert?
                                (not
                                 (equal?
                                  (member s-elem result-list)
                                  #f))
                                sub-name
                                (string-append err-1 err-2 err-4)
                                result-hash-table)
                               ))
                           )) shouldbe-list)
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop target-nugget debug-flag)
  (begin
    (let ((count 0)
          (aa-init-list
           (list (list 7 1) (list 8 2) (list 13 5)
                 (list 17 7) (list 32 14) (list 43 19)))
          (cc-0 9)
          (dd-0 4)
          (nn 5)
          (nugget-sum 0)
          (kk 44)
          (result-list (list))
          (continue-loop-flag #t))
      (begin
        (for-each
         (lambda (a-list)
           (begin
             (let ((aa (list-ref a-list 0))
                   (bb (list-ref a-list 1)))
               (let ((rlist
                      (loop-over-positives
                       aa bb cc-0 dd-0
                       nn target-nugget)))
                 (begin
                   (set!
                    result-list
                    (sort
                     (srfi-1:delete-duplicates
                      (append rlist result-list))
                     <))
                   )))
             )) aa-init-list)

        (let ((rlen (length result-list)))
          (begin
            (do ((ii 0 (1+ ii)))
                ((or (>= ii rlen)
                     (>= ii target-nugget)))
              (begin
                (let ((kk (list-ref result-list ii)))
                  (begin
                    (set! nugget-sum (+ nugget-sum kk))
                    (set! count (1+ count))

                    (if (equal? debug-flag #t)
                        (begin
                          (let ((xx (quadratic-solution kk)))
                            (begin
                              (display
                               (ice-9-format:format
                                #f "  (~:d)  AG(~a) = ~:d, "
                                (1+ ii) xx kk))
                              (display
                               (ice-9-format:format
                                #f "sum so far = ~:d~%"
                                nugget-sum))
                              (force-output)
                              ))
                          ))
                    ))
                ))
            ))

        (display
         (ice-9-format:format
          #f "The sum of the AG(x) gold nuggets = ~:d "
          nugget-sum))
        (display
         (ice-9-format:format
          #f "(first ~:d nuggets)~%" count))
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Consider the infinite polynomial "))
    (display
     (format #f "series~%"))
    (display
     (format #f "AG(x) = xG1 + x^2G2 + x^3G3 + ...,~%"))
    (display
     (format #f "where Gk is the kth term of the "))
    (display
     (format #f "second order~%"))
    (display
     (format #f "recurrence relation Gk = Gk-1 + Gk-2, "))
    (display
     (format #f "G1 = 1~%"))
    (display
     (format #f "and G2 = 4; that is, 1, 4, 5, 9, "))
    (display
     (format #f "14, 23, ... .~%"))
    (newline)
    (display
     (format #f "For this problem we shall be "))
    (display
     (format #f "concerned with values~%"))
    (display
     (format #f "of x for which AG(x) is a positive "))
    (display
     (format #f "integer.~%"))
    (newline)
    (display
     (format #f "The corresponding values of x for "))
    (display
     (format #f "the first five~%"))
    (display
     (format #f "natural numbers are "))
    (display
     (format #f "shown below.~%"))
    (newline)
    (display (format #f "    x             AG(x)~%"))
    (display (format #f "(sqrt(5)-1)/4      1~%"))
    (display (format #f "    2/5            2~%"))
    (display (format #f "(sqrt(22)-2)/6     3~%"))
    (display (format #f "(sqrt(137)-5)/14   4~%"))
    (display (format #f "    1/2            5~%"))
    (newline)
    (display
     (format #f "We shall call AG(x) a golden "))
    (display
     (format #f "nugget if x is~%"))
    (display
     (format #f "rational, because they become "))
    (display
     (format #f "increasingly rarer;~%"))
    (display
     (format #f "for example, the 20th golden "))
    (display
     (format #f "nugget is 211345365.~%"))
    (newline)
    (display
     (format #f "Find the sum of the first "))
    (display
     (format #f "thirty golden nuggets.~%"))
    (newline)
    (display
     (format #f "The solution uses the same "))
    (display
     (format #f "method as~%"))
    (display
     (format #f "https://austinrochford.com/posts/2013-11-01-generating-functions-and-fibonacci-numbers.html~%"))
    (display
     (format #f "which describes a closed form "))
    (display
     (format #f "expression for the~%"))
    (display
     (format #f "Fibonacci generating function.~%"))
    (newline)
    (display
     (format #f "Using the same derivation technique:~%"))
    (display
     (format #f "AG(x) = xG(1) + x^2G(2) + x^3G(3)+.... "))
    (display
     (format #f "= Sum(x^k * G(k))_(k=1->infinity).~%"))
    (display
     (format #f "The recurrance relation "))
    (display
     (format #f "G(k)=G(k-1)+G(k-2)~%"))
    (display
     (format #f "leads to xG(1) + x^2G(2) +~%"))
    (display
     (format #f "Sum((G(k-1) + G(k-2))*x^k)_(k=3->infinity)~%"))
    (display
     (format #f "= xG(1) + x^2G(2) + xSum(G(k)x^k)_(k=2->infinity)~%"))
    (display
     (format #f "+ x^2Sum(G(k)x^(k))_(k=1->infinity)~%"))
    (newline)
    (display
     (format #f "Now the term xSum(G(k)x^k)_(k=2->infinity)~%"))
    (display
     (format #f "is a little tricky, to convert it "))
    (display
     (format #f "into a sum like~%"))
    (display
     (format #f "AG(x), we need to add and subtract "))
    (display
     (format #f "a term, so~%"))
    (display
     (format #f "xSum(G(k)x^k)_(k=2->infinity)~%"))
    (display
     (format #f "= xSum(G(k)x^k)_(k=2->infinity) "))
    (display
     (format #f "+ G(1)x^2 - G(1)x^2~%"))
    (display
     (format #f "= xSum(G(k)x^k)_(k=1->infinity) - G(1)x^2.~%"))
    (newline)
    (display
     (format #f "Finally, AG(x) = xG(1) + x^2G(2) + x^3G(3) "))
    (display
     (format #f "+ ... ~%"))
    (display
     (format #f "= xG(1) + x^2G(2) + xAG(x) - G(1)x^2 "))
    (display
     (format #f "+ x^2AG(x)~%"))
    (display
     (format #f "= xG(1) + (G(2) - G(1))x^2 + "))
    (display
     (format #f "(x + x^2)AG(x).~%"))
    (display
     (format #f "Rearranging,~%"))
    (display
     (format #f "AG(x) = (xG(1) + "))
    (display
     (format #f "(G(2) - G(1))x^2)/(1 - x - x^2)~%"))
    (newline)
    (display
     (format #f "AG(x) = (x + 3x^2)/(1 - x - x^2),~%"))
    (display
     (format #f "since G(1)=1 and G(2)=4.~%"))
    (newline)
    (display
     (format #f "Here we are interested in rational "))
    (display
     (format #f "values of x~%"))
    (display
     (format #f "which make AG(x) an integer, so~%"))
    (display
     (format #f "AG(x) = k = (x + 3x^2)/(1 - x - x^2).~%"))
    (newline)
    (display
     (format #f "Re-arranging we get~%"))
    (display
     (format #f "(k+3)x^2 + (k+1)x - k = 0.~%"))
    (newline)
    (display
     (format #f "The quadratic formula gives:~%"))
    (display
     (format #f "x = -(k+1)/(2(k+3)) "))
    (display
     (format #f "+ sqrt((k+1)^2+4k(k+3))/(2(k+3))~%"))
    (display
     (format #f "positive solutions only "))
    (display
     (format #f "since x>0.~%"))
    (newline)
    (display
     (format #f "When k=1, x=-1/4+sqrt(4+16)/8=(sqrt(5)-1)/4.~%"))
    (display
     (format #f "When k=2, x=-3/10+sqrt(9+40)/10=-3/10 + 7/10=2/5.~%"))
    (display
     (format #f "When k=3, x=-1/3+sqrt(16+72)/12=(sqrt(22)-2)/6.~%"))
    (display
     (format #f "When k=4, x=-5/14+sqrt(25+112)/14=(sqrt(137)-5)/14.~%"))
    (display
     (format #f "When k=5, x=-3/8+sqrt(36+160)/10=-3/8+14/16=1/2.~%"))
    (newline)
    (display
     (format #f "Look for those solutions of x "))
    (display
     (format #f "where the term in~%"))
    (display
     (format #f "the square root is a square, or~%"))
    (display
     (format #f "(k+1)^2+4k(k+3)=b^2.~%"))
    (display
     (format #f "k^2+2k+1+4k^2+12k = 5k^2+14k+1 = b^2~%"))
    (display
     (format #f "then multiply through by 5~%"))
    (display
     (format #f "25k^2+70k+5 = 5b^2~%"))
    (display
     (format #f "Complete the square~%"))
    (display
     (format #f "(5k+7)^2 - 49 + 5 = 5b^2,~%"))
    (display
     (format #f "and a^2 - 5b^2 = 44, "))
    (display
     (format #f "where a=5k+7.~%"))
    (display
     (format #f "This is a Pell's type equation, and "))
    (display
     (format #f "we find an~%"))
    (display
     (format #f "initial solutions (a=7, b=1), "))
    (display
     (format #f "(a=8, b=2),~%"))
    (display
     (format #f "(a=13, b=5), (a=17, b=7), ...,~%"))
    (display
     (format #f "which satisfies a^2-5b^2=44, "))
    (display
     (format #f "and the initial~%"))
    (display
     (format #f "solution (c=9, b=4) satisfies "))
    (display
     (format #f "c^2-5b^2=1~%"))
    (display
     (format #f "(Pell's equation), from which we can "))
    (display
     (format #f "find all solutions of~%"))
    (display
     (format #f "a^2-5b^2=44. Finally transform back "))
    (display
     (format #f "to find k = (a-7)/5.~%"))
    (display
     (format #f "see https://projecteuler.net/problem=140~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((target-nugget 20)
          (debug-flag #t))
      (begin
        (sub-main-loop target-nugget debug-flag)
        ))

    (newline)
    (let ((target-nugget 30)
          (debug-flag #f))
      (begin
        (sub-main-loop target-nugget debug-flag)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 140 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 134                                    ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated July 31, 2022                                ###
;;;###                                                       ###
;;;###  updated March 12, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### prime-module for is-prime? function
(use-modules ((prime-module)
              :renamer (symbol-prefix-proc 'prime-module:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (extended-gcd aa bb)
  (begin
    (let ((xx 0)
          (yy 1)
          (last-xx 1)
          (last-yy 0)
          (local-aa aa)
          (local-bb bb))
      (begin
        (while
         (> local-bb 0)
         (begin
           (let ((qq (quotient local-aa local-bb)))
             (begin
               (let ((tmp-aa local-aa))
                 (begin
                   (set! local-aa local-bb)
                   (set! local-bb (modulo tmp-aa local-bb))
                   ))
               (let ((tmpxx (- last-xx (* qq xx)))
                     (tmpyy (- last-yy (* qq yy))))
                 (begin
                   (set! last-xx xx)
                   (set! xx tmpxx)
                   (set! last-yy yy)
                   (set! yy tmpyy)
                   ))
               ))
           ))
        (list last-xx last-yy local-aa)
        ))
    ))

;;;#############################################################
;;;#############################################################
;;; (extended-gcd 5 7) = (list xx yy gcd) = (list 3 -2 1) where 3*5 - 2*7 = 1
;;; (extended-gcd 7 11) = (list -3 2 1) where -3*7 + 2*11 = 1
;;; (extended-gcd 11 13) = (list 6 -5 1) where 6*11 - 5*13 = 1
(unittest2:define-tests-macro
 (test-extended-gcd-1 result-hash-table)
 (begin
   (let ((sub-name "test-extended-gcd-1")
         (test-list
          (list
           (list 5 7 (list 3 -2 1))
           (list 7 11 (list -3 2 1))
           (list 11 13 (list 6 -5 1))
           (list 19 23 (list -6 5 1))
           (list 120 23 (list -9 47 1))
           (list 23 120 (list 47 -9 1))
           (list 100 23 (list 3 -13 1))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((aa (list-ref this-list 0))
                  (bb (list-ref this-list 1))
                  (shouldbe (list-ref this-list 2)))
              (let ((result (extended-gcd aa bb)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "aa=~a, bb=~a, " aa bb))
                      (err-3
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append
                      err-1 err-2 err-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (factors-of-ten a-num)
  (begin
    (let ((factor 1)
          (b-num a-num))
      (begin
        (while
         (> b-num 0)
         (begin
           (set! factor (* factor 10))
           (set! b-num (euclidean/ b-num 10))
           ))

        factor
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-factors-of-ten-1 result-hash-table)
 (begin
   (let ((sub-name "test-factors-of-ten-1")
         (test-list
          (list
           (list 1 10) (list 2 10) (list 9 10)
           (list 10 100) (list 11 100) (list 99 100)
           (list 100 1000) (list 999 1000)
           (list 1000 10000) (list 9999 10000)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((num (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result (factors-of-ten num)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : num=~a, "
                        sub-name test-label-index num))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (form-number pp-1 pp-2 max-num)
  (begin
    (let ((pfactor (factors-of-ten pp-1)))
      (let ((rlist (extended-gcd pfactor pp-2)))
        (let ((rr (car rlist))
              (diff (- pp-2 pp-1)))
          (let ((xx (modulo (* rr diff) pp-2)))
            (let ((result (+ (* pfactor xx) pp-1)))
              (begin
                result
                )))
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-form-number-1 result-hash-table)
 (begin
   (let ((sub-name "test-form-number-1")
         (test-list
          (list
           (list 5 7 100 35)
           (list 7 11 100 77)
           (list 11 13 100 611)
           (list 13 17 100 1513)
           (list 17 19 100 817)
           (list 19 23 100 1219)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((p1 (list-ref this-list 0))
                  (p2 (list-ref this-list 1))
                  (max-num (list-ref this-list 2))
                  (shouldbe (list-ref this-list 3)))
              (let ((result (form-number p1 p2 max-num)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "p1=~a, p2=~a, max-num=~a, "
                        p1 p2 max-num))
                      (err-3
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append
                      err-1 err-2 err-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define-syntax handle-loop-macro
  (syntax-rules ()
    ((handle-loop-macro
      ii prime-array p1-limit max-nn debug-flag
      smallest-sum smallest-count)
     (begin
       (let ((pp-1 (array-ref prime-array ii))
             (pp-2 (array-ref prime-array (1+ ii))))
         (begin
           (if (<= pp-1 p1-limit)
               (begin
                 (let ((ss (form-number pp-1 pp-2 max-nn)))
                   (begin
                     (if (> ss 0)
                         (begin
                           (set! smallest-sum (+ smallest-sum ss))
                           (set! smallest-count (1+ smallest-count))

                           (if (equal? debug-flag #t)
                               (begin
                                 (display
                                  (ice-9-format:format
                                   #f "  prime-1=~:d, prime-2=~:d, "
                                   pp-1 pp-2))
                                 (display
                                  (ice-9-format:format
                                   #f "smallest=~:d : " ss))
                                 (display
                                  (ice-9-format:format
                                   #f "sum so far=~:d  count=~:d~%"
                                   smallest-sum smallest-count))
                                 (force-output)
                                 )))
                         (begin
                           (display
                            (ice-9-format:format
                             #f "error : for prime-1=~:d, prime-2=~:d, "
                             pp-1 pp-2))
                           (display
                            (ice-9-format:format
                             #f "smallest sum not found for "))
                           (display
                            (ice-9-format:format
                             #f "max-nn=~:d~%" max-nn))
                           (force-output)
                           ))
                     ))
                 ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop max-prime p1-limit max-nn debug-flag)
  (begin
    (let ((smallest-sum 0)
          (smallest-count 0)
          (prime-array
           (prime-module:make-prime-array max-prime)))
      (let ((plen (1- (car (array-dimensions prime-array)))))
        (begin
          (do ((ii 2 (1+ ii)))
              ((>= ii plen))
            (begin
              (handle-loop-macro
               ii prime-array p1-limit max-nn debug-flag
               smallest-sum smallest-count)
              ))

          (display
           (ice-9-format:format
            #f "Sum(smallest) = ~:d, there were ~:d "
            smallest-sum smallest-count))
          (display
           (format #f "consecutive prime pairs found,~%"))
          (display
           (ice-9-format:format
            #f "  (for primes less than ~:d).~%"
            p1-limit))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Consider the consecutive primes "))
    (display
     (format #f "p1 = 19 and~%"))
    (display
     (format #f "p2 = 23. It can be verified that "))
    (display
     (format #f "1219 is the~%"))
    (display
     (format #f "smallest number such that the last "))
    (display
     (format #f "digits are formed~%"))
    (display
     (format #f "by p1 whilst also being divisible "))
    (display
     (format #f "by p2.~%"))
    (newline)
    (display
     (format #f "In fact, with the exception of "))
    (display
     (format #f "p1 = 3 and p2 = 5,~%"))
    (display
     (format #f "for every pair of consecutive primes, "))
    (display
     (format #f "p2 > p1, there~%"))
    (display
     (format #f "exist values of n for which the last "))
    (display
     (format #f "digits are formed~%"))
    (display
     (format #f "by p1 and n is divisible by p2. Let "))
    (display
     (format #f "S be the smallest~%"))
    (display
     (format #f "of these values of n.~%"))
    (newline)
    (display
     (format #f "Find Sum(S) for every pair of "))
    (display
     (format #f "consecutive primes with~%"))
    (display
     (format #f "5 <= p1 <= 1000000.~%"))
    (newline)
    (display
     (format #f "The solution, makes use of the "))
    (display
     (format #f "extended Euclidean algorithm~%"))
    (display
     (format #f "https://en.wikipedia.org/wiki/Extended_Euclidean_algorithm~%"))
    (newline)
    (display
     (format #f "To see how the extended Euclidean "))
    (display
     (format #f "algorithm is used,~%"))
    (display
     (format #f "here are some examples:~%"))
    (display
     (format #f "(extended-gcd 5 7) -> (list xx yy gcd) "))
    (display
     (format #f "= (list 3 -2 1)~%"))
    (display
     (format #f "    where 3*5 - 2*7 = 1~%"))
    (display
     (format #f "(extended-gcd 7 11) -> (list -3 2 1) "))
    (display
     (format #f "where -3*7 + 2*11 = 1~%"))
    (display
     (format #f "(extended-gcd 11 13) -> (list 6 -5 1) "))
    (display
     (format #f "where 6*11 - 5*13 = 1~%"))
    (display
     (format #f "(extended-gcd 19 23) -> (list -6 5 1) "))
    (display
     (format #f "where -6*19 + 5*23 = 1~%"))
    (display
     (format #f "(extended-gcd 100 23) -> (list 3 -13 1) "))
    (display
     (format #f "where 3*100 - 13*23 = 1~%"))
    (newline)
    (display
     (format #f "For example, when p1=19, and p2=23, we "))
    (display
     (format #f "need to find~%"))
    (display
     (format #f "100*x + p1 = k*p2, or 100*x = -p1 mod p2 "))
    (display
     (format #f "= (p2 - p1) mod p2.~%"))
    (display
     (format #f "Here the extended-gcd(100, 23) "))
    (display
     (format #f "= (list 3 -13 1).~%"))
    (display
     (format #f "We want x = (3*(p2 - p1)) mod p2 "))
    (display
     (format #f "= (3*4 mod 23) = 12.~%"))
    (display
     (format #f "So 100*12+19 = 1219 = 53*23, or "))
    (display
     (format #f "1219 has 19 as it's~%"))
    (display
     (format #f "last two digits, and is divisible "))
    (display
     (format #f "by 23.~%"))
    (display
     (format #f "see https://projecteuler.net/problem=134~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((max-prime 40)
          (p1-limit 30)
          (max-nn 500)
          (debug-flag #t))
      (begin
        (sub-main-loop max-prime p1-limit max-nn debug-flag)
        ))

    (newline)
    (let ((max-prime 1100000)
          (p1-limit 1000000)
          (max-nn 1000000)
          (debug-flag #f))
      (begin
        (sub-main-loop max-prime p1-limit max-nn debug-flag)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Project Euler 134 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (load "prime-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))


          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 132                                    ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated July 31, 2022                                ###
;;;###                                                       ###
;;;###  updated March 12, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### prime-module for is-prime? function
(use-modules ((prime-module)
              :renamer (symbol-prefix-proc 'prime-module:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop kk num-primes max-prime debug-flag)
  (begin
    (let ((sum-primes 0)
          (primes-count 0)
          (nn 5)
          (prime-array
           (prime-module:make-prime-array max-prime))
          (continue-loop-flag #t))
      (begin
        (while
         (equal? continue-loop-flag #t)
         (begin
           (set! nn (+ nn 2))
           (if (and (not (zero? (modulo nn 5)))
                    (prime-module:is-array-prime? nn prime-array))
               (begin
                 (let ((rr-remain (modulo-expt 10 kk nn)))
                   (begin
                     (if (= rr-remain 1)
                         (begin
                           (set! sum-primes (+ sum-primes nn))
                           (set! primes-count (1+ primes-count))
                           (if (>= primes-count num-primes)
                               (begin
                                 (set! continue-loop-flag #f)
                                 ))

                           (if (equal? debug-flag #t)
                               (begin
                                 (display
                                  (format
                                   #f "  nn=~a, sum-primes=~a, count=~a~%"
                                   nn sum-primes primes-count))
                                 (force-output)
                                 ))
                           ))
                     ))
                 ))
           ))

        (display
         (ice-9-format:format
          #f "The sum of the first ~:d prime factors R(~:d) is ~:d.~%"
          num-primes kk sum-primes))
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "A number consisting entirely of "))
    (display
     (format #f "ones is called a~%"))
    (display
     (format #f "repunit. We shall define R(k) to "))
    (display
     (format #f "be a repunit~%"))
    (display
     (format #f "of length k.~%"))
    (newline)
    (display
     (format #f "For example, R(10) = 1111111111 "))
    (display
     (format #f "= 11x41x271x9091,~%"))
    (display
     (format #f "and the sum of these prime factors "))
    (display
     (format #f "is 9414.~%"))
    (newline)
    (display
     (format #f "Find the sum of the first forty "))
    (display
     (format #f "prime factors of~%"))
    (display
     (format #f "R(10^9).~%"))
    (newline)
    (display
     (format #f "The solution makes use of the "))
    (display
     (format #f "definition of~%"))
    (display
     (format #f "R(k) = (10^k - 1)/9~%"))
    (display
     (format #f "and the modulo-expt function. It was "))
    (display
     (format #f "found at~%"))
    (display
     (format #f "https://euler.stephan-brumme.com/132/~%"))
    (display
     (format #f "see https://projecteuler.net/problem=132~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((kk 10)
          (num-primes 4)
          (max-prime 1000)
          (debug-flag #t))
      (begin
        (sub-main-loop kk num-primes max-prime debug-flag)
        ))

    (newline)
    (let ((kk (inexact->exact 10e9))
          (num-primes 40)
          (max-prime 100000)
          (debug-flag #f))
      (begin
        (sub-main-loop kk num-primes max-prime debug-flag)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Project Euler 132 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (load "prime-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

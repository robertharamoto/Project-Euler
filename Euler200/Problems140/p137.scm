#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 137                                    ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated July 31, 2022                                ###
;;;###                                                       ###
;;;###  updated March 12, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### srfi-1 for fold function
(use-modules ((srfi srfi-1)
              :renamer (symbol-prefix-proc 'srfi-1:)))

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
;;; initial solutions for Pell's equation a^2-5b^2 = -4
(define (find-initial-solutions)
  (begin
    (let ((nn 5)
          (kk -4)
          (max-aa 2000)
          (max-bb 1000))
      (begin
        (do ((aa 1 (1+ aa)))
            ((> aa max-aa))
          (begin
            (let ((aa-2 (* aa aa)))
              (begin
                (do ((bb 1 (1+ bb)))
                    ((> bb max-bb))
                  (begin
                    (let ((bb-2 (* nn bb bb)))
                      (begin
                        (if (= (- aa-2 bb-2) kk)
                            (begin
                              (display
                               (ice-9-format:format
                                #f "aa=~:d, bb=~:d, kk=~a~%"
                                aa bb kk))
                              (force-output)
                              ))
                        ))
                    ))
                ))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
;;; rational solutions to x = -(k+1)/(2k) +/- sqrt((k+1)^2+4k^2)/2k
(define (quadratic-solution kk)
  (begin
    (let ((kp1 (1+ kk)))
      (let ((kp1-2 (* kp1 kp1))
            (kk-2 (* 4 kk kk)))
        (let ((square-term (+ kp1-2 kk-2)))
          (let ((sqrt-term (exact-integer-sqrt square-term)))
            (begin
              (if (= square-term (* sqrt-term sqrt-term))
                  (begin
                    (let ((aterm (- sqrt-term kp1)))
                      (let ((bterm (/ aterm (* 2 kk))))
                        (begin
                          bterm
                          ))
                      ))
                  (begin
                    #f
                    ))
              )))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-quadratic-solution-1 result-hash-table)
 (begin
   (let ((sub-name "test-quadratic-solution-1")
         (test-list
          (list
           (list 1 #f) (list 2 1/2) (list 3 #f)
           (list 4 #f) (list 5 #f)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((kk (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result (quadratic-solution kk)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : kk=~a, "
                        sub-name test-label-index kk))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a~%"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; cc and dd solve the Pell's equation cc^2 - 5d^2 = 1
;;; aa and bb solve the equation aa^2 - 5bb^2 = -4
(define (loop-over-positives
         aa-0 bb-0 cc-0 dd-0 nn max-count)
  (begin
    (let ((result-list (list))
          (aa-1 aa-0)
          (bb-1 bb-0)
          (cc-1 cc-0)
          (dd-1 dd-0)
          (count 0)
          (continue-loop-flag #t))
      (begin
        (while
         (equal? continue-loop-flag #t)
         (begin
           (let ((kk (/ (1- aa-1) nn))
                 (next-cc-p (+ (* cc-0 cc-1) (* nn dd-0 dd-1)))
                 (next-dd-p (+ (* cc-0 dd-1) (* dd-0 cc-1)))
                 (next-aa-p (+ (* aa-0 cc-1) (* nn bb-0 dd-1)))
                 (next-bb-p (+ (* aa-0 dd-1) (* cc-1 bb-0))))
             (begin
               (if (and (integer? kk) (> kk 0))
                   (begin
                     (set! result-list (cons kk result-list))
                     (set! count (1+ count))
                     ))

               (set! cc-1 next-cc-p)
               (set! dd-1 next-dd-p)

               (set! aa-1 next-aa-p)
               (set! bb-1 next-bb-p)

               (if (>= count max-count)
                   (begin
                     (set! continue-loop-flag #f)
                     ))
               ))
           ))

        result-list
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-loop-over-positives-1 result-hash-table)
 (begin
   (let ((sub-name "test-loop-over-positives-1")
         (test-list
          (list
           (list 1 1 9 4 5 2 (list 104 33552))
           (list 4 2 9 4 5 2 (list 15 4895))
           (list 11 5 9 4 5 2 (list 2 714))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((aa-0 (list-ref this-list 0))
                  (bb-0 (list-ref this-list 1))
                  (cc-0 (list-ref this-list 2))
                  (dd-0 (list-ref this-list 3))
                  (nn (list-ref this-list 4))
                  (max-num (list-ref this-list 5))
                  (shouldbe-list (list-ref this-list 6)))
              (let ((result-list
                     (loop-over-positives
                      aa-0 bb-0 cc-0 dd-0 nn max-num)))
                (let ((slen (length shouldbe-list))
                      (rlen (length result-list)))
                  (let ((err-1
                         (format
                          #f "~a : error (~a) : aa-0=~a, bb-0=~a, "
                          sub-name test-label-index aa-0 bb-0))
                        (err-2
                         (format
                          #f "cc-0=~a, dd-0=~a, nn=~a, max-num=~a, "
                          cc-0 dd-0 nn max-num))
                        (err-3
                         (format
                          #f "shouldbe length=~a, result length=~a"
                          slen rlen)))
                    (begin
                      (unittest2:assert?
                       (equal? slen rlen)
                       sub-name
                       (string-append err-1 err-2 err-3)
                       result-hash-table)

                      (for-each
                       (lambda (s-elem)
                         (begin
                           (let ((err-4
                                  (format
                                   #f "shouldbe=~a, result=~a"
                                   s-elem result-list)))
                             (begin
                               (unittest2:assert?
                                (not
                                 (equal?
                                  (member s-elem result-list)
                                  #f))
                                sub-name
                                (string-append err-1 err-2 err-4)
                                result-hash-table)
                               ))
                           )) shouldbe-list)
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop target-nugget debug-flag)
  (begin
    (let ((count 0)
          (aa-0 1)
          (bb-0 1)
          (aa-1 4)
          (bb-1 2)
          (aa-2 11)
          (bb-2 5)
          (cc-0 9)
          (dd-0 4)
          (nn 5)
          (result-list (list)))
      (begin
        (let ((rlist1
               (loop-over-positives
                aa-0 bb-0 cc-0 dd-0
                nn target-nugget))
              (rlist2
               (loop-over-positives
                aa-1 bb-1 cc-0 dd-0
                nn target-nugget))
              (rlist3
               (loop-over-positives
                aa-2 bb-2 cc-0 dd-0
                nn target-nugget)))
          (begin
            (set!
             result-list
             (sort
              (srfi-1:delete-duplicates
               (append rlist1 rlist2 rlist3))
              <))

            (if (equal? debug-flag #t)
                (begin
                  (let ((rlen (length result-list)))
                    (begin
                      (do ((ii 0 (1+ ii)))
                          ((or (>= ii rlen)
                               (>= ii target-nugget)))
                        (begin
                          (let ((kk (list-ref result-list ii)))
                            (let ((xx (quadratic-solution kk)))
                              (begin
                                (display
                                 (ice-9-format:format
                                  #f "  (~:d)  AF(~a) = ~:d~%"
                                  (1+ ii) xx kk))
                                (force-output)
                                )))
                          ))
                      ))
                  ))

            (let ((result-kk
                   (list-ref result-list (1- target-nugget))))
              (begin
                (display
                 (ice-9-format:format
                  #f "The ~:d-th gold nugget = ~:d~%"
                  target-nugget result-kk))
                (force-output)
                ))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Consider the infinite polynomial "))
    (display
     (format #f "series~%"))
    (display
     (format #f "AF(x) = xF1 + x^2F2 + x^3F3 + ...,~%"))
    (display
     (format #f "where Fk is the kth term in the "))
    (display
     (format #f "Fibonacci sequence:~%"))
    (display
     (format #f "1, 1, 2, 3, 5, 8, ... ;~%"))
    (display
     (format #f "that is, Fk = Fk-1 + Fk-2, F1 = 1 "))
    (display
     (format #f "and F2 = 1.~%"))
    (newline)
    (display
     (format #f "For this problem we shall be interested "))
    (display
     (format #f "in values of~%"))
    (display
     (format #f "x for which AF(x) is a "))
    (display
     (format #f "positive integer.~%"))
    (newline)
    (display
     (format #f "Surprisingly AF(1/2) = (1/2)*1 + (1/2)^2*1 "))
    (display
     (format #f "+ (1/2)^3*2~%"))
    (display
     (format #f "+ (1/2)^4*3 + (1/2)^5*5 + ... = 1/2 + "))
    (display
     (format #f "1/4 + 2/8 + 3/16 + 5/32~%"))
    (display
     (format #f "+ ... = 2~%"))
    (newline)
    (display
     (format #f "The corresponding values of x for the "))
    (display
     (format #f "first five natural~%"))
    (display
     (format #f "numbers are shown below.~%"))
    (newline)
    (display (format #f "    x          AF(x)~%"))
    (display (format #f "  sqrt(2)-1      1~%"))
    (display (format #f "    1/2          2~%"))
    (display (format #f "(sqrt(13)-2)/3   3~%"))
    (display (format #f "(sqrt(89)-5)/8   4~%"))
    (display (format #f "(sqrt(34)-3)/5   5~%"))
    (newline)
    (display
     (format #f "We shall call AF(x) a golden nugget "))
    (display
     (format #f "if x is rational,~%"))
    (display
     (format #f "because they become increasingly "))
    (display
     (format #f "rarer; for example,~%"))
    (display
     (format #f "the 10th golden nugget is "))
    (display
     (format #f "74049690.~%"))
    (newline)
    (display
     (format #f "Find the 15th golden nugget.~%"))
    (newline)
    (display
     (format #f "The solution was found at:~%"))
    (display
     (format #f "https://en.wikipedia.org/wiki/Fibonacci_number#Generating_function~%"))
    (display
     (format #f "which describes a closed form expression "))
    (display
     (format #f "for the Fibonacci~%"))
    (display
     (format #f "generating function.~%"))
    (newline)
    (display
     (format #f "AF(x) = x/(1 - x - x^2)~%"))
    (display
     (format #f "see also: https://www.ivl-projecteuler.com/overview-of-problems/50-difficulty/problem-137~%"))
    (newline)
    (display
     (format #f "Here we are interested in values of x "))
    (display
     (format #f "which make AF(x)~%"))
    (display
     (format #f "an integer, so AF(x) = k = x/(1 - x - x^2).~%"))
    (newline)
    (display
     (format #f "Re-arranging we get kx^2 + (k+1)x - k = 0.~%"))
    (newline)
    (display
     (format #f "The quadratic equation gives:~%"))
    (display
     (format #f "x = -(k+1)/(2k) +/- sqrt((k+1)^2+4k^2)/2k~%"))
    (newline)
    (display
     (format #f "When k=1, x=-1+sqrt(4+4)/2=sqrt(2)-1.~%"))
    (display
     (format #f "When k=2, x=-3/4+sqrt(9+16)/4=-3/4 + 5/4=1/2.~%"))
    (display
     (format #f "When k=3, x=-2/3+sqrt(16+36)/6=(sqrt(13)-2)/3.~%"))
    (display
     (format #f "When k=4, x=-5/8+sqrt(25+64)/8=(sqrt(89)-5)/8.~%"))
    (display
     (format #f "When k=5, x=-3/5+sqrt(36+100)/10=(sqrt(34)-3)/5.~%"))
    (newline)
    (display
     (format #f "Iterating over k is time consuming, "))
    (display
     (format #f "so it's better~%"))
    (display
     (format #f "to look for possible solutions when "))
    (display
     (format #f "the sqrt((k+1)^2+4k^2)~%"))
    (display
     (format #f "is an integer. Let b an integer such "))
    (display
     (format #f "that b^2 = (k+1)^2+4k^2~%"))
    (display
     (format #f "= k^2+2k+1+4k^2 = 5k^2+2k+1. Multiplying "))
    (display
     (format #f "through by 5,~%"))
    (display
     (format #f "and rewriting, 25k^2 + 10k + 1 + 4 = 5b^2. "))
    (display
     (format #f "Then~%"))
    (display
     (format #f "(5k + 1)^2 - 5b^2 = -4. Set a=(5k+1), "))
    (display
     (format #f "then the equation~%"))
    (display
     (format #f "becomes a^2 - 5b^2 = -4, where k=(a-1)/5, "))
    (display
     (format #f "and k and b~%"))
    (display
     (format #f "are integers, and will make the expression "))
    (display
     (format #f "in the square root~%"))
    (display
     (format #f "(k+1)^2+4k^2 a perfect square.~%"))
    (newline)
    (display
     (format #f "To solve the Pell's equation, we "))
    (display
     (format #f "take the base equation~%"))
    (display
     (format #f "a^2 - 5b^2 = -4, with initial solution "))
    (display
     (format #f "(a0=11, b0=5), and~%"))
    (display
     (format #f "generate all solutions of the ordinary "))
    (display
     (format #f "Pell equation~%"))
    (display
     (format #f "c^2-5d^2=1, (with initial solution "))
    (display
     (format #f "(c=9, d=4), and~%"))
    (display
     (format #f "(a0^2-5b0^2)*(c^2-5d^2) = -4. (see:~%"))
    (display
     (format #f "https://mathworld.wolfram.com/PellEquation.html).~%"))
    (newline)
    (display
     (format #f "One last caveat, given an (a, b) pair, "))
    (display
     (format #f "one can generate~%"))
    (display
     (format #f "subsequent solutions of Pell's equation, "))
    (display
     (format #f "however you don't~%"))
    (display
     (format #f "generate all possible solutions. The first "))
    (display
     (format #f "three solutions~%"))
    (display
     (format #f "needed to be identified (a=1, b=1), "))
    (display
     (format #f "(a=4, b=2), and~%"))
    (display
     (format #f "(a=11, b=5) in order to generate all "))
    (display
     (format #f "solutions of~%"))
    (display
     (format #f "AF(x)=integer.~%"))
    (display
     (format #f "see https://projecteuler.net/problem=137~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((target-nugget 10)
          (debug-flag #t))
      (begin
        (sub-main-loop target-nugget debug-flag)
        ))

    (newline)
    (let ((target-nugget 15)
          (debug-flag #f))
      (begin
        (sub-main-loop target-nugget debug-flag)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 137 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)
          (force-output)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

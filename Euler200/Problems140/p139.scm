#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 139                                    ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated July 31, 2022                                ###
;;;###                                                       ###
;;;###  updated March 12, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
;;; initial solutions to x^2-2y^2 = -1
(define (find-initial-solutions)
  (begin
    (let ((max-nn 100)
          (continue-loop-flag #t))
      (begin
        (do ((xx 1 (1+ xx)))
            ((> xx max-nn))
          (begin
            (let ((xx-2 (* xx xx)))
              (begin
                (do ((yy 1 (1+ yy)))
                    ((> yy max-nn))
                  (begin
                    (let ((yy-2 (* 2 yy yy)))
                      (begin
                        (if (= (- xx-2 yy-2) -1)
                            (begin
                              (display
                               (ice-9-format:format
                                #f "  solution found (x=~:d, y=~:d)~%"
                                xx yy))
                              (force-output)
                              ))
                        ))
                    ))
                ))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax display-debug-info-macro
  (syntax-rules ()
    ((display-debug-info-macro
      oo-xx-1 oo-yy-1 aa bb cc
      perimeter max-perimeter count)
     (begin
       (let ((this-count
              (euclidean/ max-perimeter perimeter)))
         (begin
           (display
            (ice-9-format:format
             #f " (xx=~:d, yy=~:d) : aa=~:d, bb=~:d, cc=~:d~%"
             oo-xx-1 oo-yy-1 aa bb cc))
           (display
            (ice-9-format:format
             #f " perimeter=~:d, max-perimeter=~:d~%"
             perimeter max-perimeter))
           (display
            (format
             #f " this-count=~:d, count=~:d~%"
             this-count count))
           (force-output)
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax process-perimeter-macro
  (syntax-rules ()
    ((process-perimeter-macro
      oo-xx-1 oo-yy-1
      alist
      debug-flag
      max-perimeter count
      seen-htable)
     (begin
       (let ((perimeter (+ oo-xx-1 oo-yy-1)))
         (begin
           (if (< perimeter max-perimeter)
               (begin
                 (let ((this-count
                        (euclidean/ max-perimeter perimeter)))
                   (begin
                     (set! count (+ count this-count))
                     (hash-set! seen-htable alist #t)

                     (if (equal? debug-flag #t)
                         (begin
                           (let ((aa (/ (+ oo-xx-1 1) 2))
                                 (cc oo-yy-1))
                             (let ((bb (1- aa)))
                               (begin
                                 (display-debug-info-macro
                                  oo-xx-1 oo-yy-1 aa bb cc
                                  perimeter max-perimeter count)
                                 )))
                           ))
                     ))
                 ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop max-perimeter debug-flag)
  (begin
    (let ((count 0)
          (xx-0 3)
          (yy-0 2)
          (seen-htable (make-hash-table))
          (oo-list (list (list 7 5) (list 41 29))))
      (begin
        (for-each
         (lambda (olist)
           (begin
             (let ((oo-xx-0 (list-ref olist 0))
                   (oo-yy-0 (list-ref olist 1))
                   (xx-1 3)
                   (yy-1 2))
               (let ((oo-xx-1 oo-xx-0)
                     (oo-yy-1 oo-yy-0)
                     (continue-loop-flag #t))
                 (begin
                   (while
                    (equal? continue-loop-flag #t)
                    (begin
                      (let ((alist (list oo-xx-1 oo-yy-1)))
                        (let ((hflag (hash-ref seen-htable alist #f)))
                          (begin
                            (if (and (equal? hflag #f)
                                     (odd? oo-xx-1))
                                (begin
                                  (process-perimeter-macro
                                   oo-xx-1 oo-yy-1
                                   alist
                                   debug-flag
                                   max-perimeter count
                                   seen-htable)
                                  ))

                             ;;; next solution to x^2-2y^2=1
                            (let ((next-xx-1
                                   (+ (* xx-0 xx-1) (* 2 yy-0 yy-1)))
                                  (next-yy-1
                                   (+ (* xx-0 yy-1) (* yy-0 xx-1))))
                              (begin
                                (set! xx-1 next-xx-1)
                                (set! yy-1 next-yy-1)
                                ))

                             ;;; next solution to x^2-2y^2=-1
                            (let ((next-xx-1
                                   (+ (* oo-xx-0 xx-1) (* 2 oo-yy-0 yy-1)))
                                  (next-yy-1
                                   (+ (* oo-xx-0 yy-1) (* oo-yy-0 xx-1))))
                              (begin
                                (set! oo-xx-1 next-xx-1)
                                (set! oo-yy-1 next-yy-1)
                                ))

                            (let ((perimeter (+ oo-xx-1 oo-yy-1)))
                              (begin
                                (if (> perimeter max-perimeter)
                                    (begin
                                      (set! continue-loop-flag #f)
                                      ))
                                ))
                            )))
                      ))
                   )))
             )) oo-list)

        (display
         (ice-9-format:format
          #f "Number of Pythagorean triangles = ~:d, "
          count))
        (display
         (format
          #f "which can tile a square with~%"))
        (display
         (format
          #f "  the hole made by four of those triangles, "))
        (display
         (ice-9-format:format
          #f "with perimeters less than ~:d~%"
          max-perimeter))
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Let (a, b, c) represent the three "))
    (display
     (format #f "sides of a right~%"))
    (display
     (format #f "angle triangle with integral length "))
    (display
     (format #f "sides. It is~%"))
    (display
     (format #f "possible to place four such triangles "))
    (display
     (format #f "together to form~%"))
    (display
     (format #f "a square with length c.~%"))
    (newline)
    (display
     (format #f "For example, (3, 4, 5) triangles can "))
    (display
     (format #f "be placed together~%"))
    (display
     (format #f "to form a 5 by 5 square with a 1 by 1 "))
    (display
     (format #f "hole in the~%"))
    (display
     (format #f "middle and it can be seen that the "))
    (display
     (format #f "5 by 5 square~%"))
    (display
     (format #f "can be tiled with twenty-five 1 by 1 "))
    (display
     (format #f "squares.  See~%"))
    (display
     (format #f "https://projecteuler.net/project/images/p_139.gif~%"))
    (newline)
    (display
     (format #f "However, if (5, 12, 13) triangles "))
    (display
     (format #f "were used then~%"))
    (display
     (format #f "the hole would measure 7 by 7 and "))
    (display
     (format #f "these could not~%"))
    (display
     (format #f "be used to tile the 13 by 13 square.~%"))
    (newline)
    (display
     (format #f "Given that the perimeter of the "))
    (display
     (format #f "right triangle is~%"))
    (display
     (format #f "less than one-hundred million, how "))
    (display
     (format #f "many Pythagorean~%"))
    (display
     (format #f "triangles would allow such a tiling "))
    (display
     (format #f "to take place?~%"))
    (newline)
    (display
     (format #f "A solution was found at~%"))
    (display
     (format #f "https://blog.dreamshire.com/project-euler-139-solution/~%"))
    (newline)
    (display
     (format #f "Instead of generating pythagorean "))
    (display
     (format #f "triples, with a little~%"))
    (display
     (format #f "bit of algebra a much faster "))
    (display
     (format #f "solution was found.~%"))
    (newline)
    (display
     (format #f "By the Pythagorean theorem, "))
    (display
     (format #f "a^2+b^2=c^2. Let a-b=d,~%"))
    (display
     (format #f "then the difference d must evenly "))
    (display
     (format #f "divide c^2 in order~%"))
    (display
     (format #f "for the inner square to tile the "))
    (display
     (format #f "cxc square. This~%"))
    (display
     (format #f "means that c=y*d for some integer y. "))
    (display
     (format #f "So, a^2+b^2=c^2 can~%"))
    (display
     (format #f "be written as a^2+(a-d)^2=y^2*d^2 "))
    (display
     (format #f "or 2a^2-2ad+d^2=y^2*d^2.~%"))
    (display
     (format #f "Multiply by 2 to complete the square, "))
    (display
     (format #f "gives~%"))
    (display
     (format #f "(2a-d)^2+d^2=2y^2*d^2, divide through "))
    (display
     (format #f "by d^2,~%"))
    (display
     (format #f "(2a/d-1)^2-2y^2=-1, let x=2a/d-1, "))
    (display
     (format #f "then x^2-2y^2=-1~%"))
    (display
     (format #f "is Pell's equation. Since (x=1, y=1) "))
    (display
     (format #f "corresponds to a=0,~%"))
    (display
     (format #f "we use another initial solution, "))
    (display
     (format #f "(x=7, y=5).~%"))
    (newline)
    (display
     (format #f "Also, since x=2a/d-1 is an integer, "))
    (display
     (format #f "we have 2a/d-1~%"))
    (display
     (format #f "an integer, or d always divides a "))
    (display
     (format #f "evenly, we are free~%"))
    (display
     (format #f "to choose d=1, then x=2a-1 and x is "))
    (display
     (format #f "odd (look for~%"))
    (display
     (format #f "odd solutions of the Pell's "))
    (display
     (format #f "equation).~%"))
    (newline)
    (display
     (format #f "First find all solutions for "))
    (display
     (format #f "x^2-2y^2=1, initial~%"))
    (display
     (format #f "solution x=3, y=2, see:~%"))
    (display
     (format #f "https://en.wikipedia.org/wiki/Pell's_equation~%"))
    (display
     (format #f "then compose that solution with "))
    (display
     (format #f "x^2-2y^2=-1 to~%"))
    (display
     (format #f "generate all the rest. Second, for "))
    (display
     (format #f "each solution, check~%"))
    (display
     (format #f "to see if the perimeter, a+b+c "))
    (display
     (format #f "= 2a-d+yd = (x+y)*d~%"))
    (display
     (format #f "< 100,000,000.~%"))
    (display
     (format #f "see https://projecteuler.net/problem=139~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (find-initial-solutions)

    (let ((max-perimeter 15)
          (debug-flag #t))
      (begin
        (sub-main-loop max-perimeter debug-flag)
        ))

    (newline)
    (let ((max-perimeter 100000000)
          (debug-flag #f))
      (begin
        (sub-main-loop max-perimeter debug-flag)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 139 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 136                                    ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated July 31, 2022                                ###
;;;###                                                       ###
;;;###  updated March 12, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### srfi-19 for date functions
(use-modules ((srfi srfi-19)
              :renamer (symbol-prefix-proc 'srfi-19:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
;;; make a list of primes less than or equal to n
;;; sieve of eratosthenes method
(define (make-odd-prime-array max-num)
  (begin
    (let ((intermediate-array (make-array 0 (1+ max-num)))
          (result-list (list)))
      (begin
        (do ((ii 0 (1+ ii)))
            ((> ii max-num))
          (begin
            (array-set! intermediate-array ii ii)
            ))

        (do ((ii 2 (1+ ii)))
            ((> ii max-num))
          (begin
            (let ((this-num
                   (array-ref intermediate-array ii)))
              (begin
                (if (= this-num ii)
                    (begin
                      (set! result-list
                            (cons ii result-list))

                      (do ((jj (+ ii ii) (+ jj ii)))
                          ((> jj max-num))
                        (begin
                          (array-set! intermediate-array -1 jj)
                          ))
                      ))
                ))
            ))

        (let ((rlist (reverse result-list)))
          (let ((olist (cdr rlist)))
            (begin
              (list->array 1 olist)
              )))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-make-odd-prime-array-1 result-hash-table)
 (begin
   (let ((sub-name "test-make-odd-prime-array-1")
         (test-list
          (list
           (list 2 (list)) (list 3 (list 3)) (list 4 (list 3))
           (list 5 (list 3 5)) (list 6 (list 3 5))
           (list 7 (list 3 5 7)) (list 8 (list 3 5 7))
           (list 9 (list 3 5 7)) (list 10 (list 3 5 7))
           (list 11 (list 3 5 7 11))
           (list 13 (list 3 5 7 11 13))
           (list 17 (list 3 5 7 11 13 17))
           (list 19 (list 3 5 7 11 13 17 19))
           (list 23 (list 3 5 7 11 13 17 19 23))
           (list 31 (list 3 5 7 11 13 17 19 23 29 31))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((test-num (list-ref this-list 0))
                  (shouldbe-list (list-ref this-list 1)))
              (let ((result-array (make-odd-prime-array test-num)))
                (let ((slen (length shouldbe-list))
                      (rlen (car (array-dimensions result-array))))
                  (let ((err-1
                         (format
                          #f "~a : error (~a) : test-num=~a, "
                          sub-name test-label-index test-num))
                        (err-2
                         (format
                          #f "shouldbe length=~a, result length=~a"
                          slen rlen)))
                    (begin
                      (unittest2:assert?
                       (equal? slen rlen)
                       sub-name
                       (string-append err-1 err-2)
                       result-hash-table)

                      (do ((ii 0 (1+ ii)))
                          ((>= ii slen))
                        (begin
                          (let ((s-elem (list-ref shouldbe-list ii))
                                (r-elem (array-ref result-array ii)))
                            (let ((err-3
                                   (format
                                    #f "shouldbe=~a, result=~a"
                                    s-elem r-elem)))
                              (begin
                                (unittest2:assert?
                                 (equal? s-elem r-elem)
                                 sub-name
                                 (string-append err-1 err-3)
                                 result-hash-table)
                                )))
                          ))
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; u=3d-z, v=d+z
;;; d=(u+v)/4 and z=(3v-u)/4
(define (transform-uu-vv-to-xyzd uu vv)
  (begin
    (let ((result-list (list))
          (dtmp1 (+ uu vv))
          (ztmp1 (- (* 3 vv) uu)))
      (let ((dtmp2 (euclidean/ dtmp1 4))
            (ztmp2 (euclidean/ ztmp1 4)))
        (begin
          (if (and (= (* 4 dtmp2) dtmp1)
                   (= (* 4 ztmp2) ztmp1)
                   (> ztmp2 0))
              (begin
                (let ((xx (+ ztmp2 dtmp2 dtmp2))
                      (yy (+ ztmp2 dtmp2))
                      (zz ztmp2))
                  (begin
                    (set! result-list (list xx yy zz dtmp2))
                    ))
                ))

          result-list
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-transform-uu-vv-to-xyzd-1 result-hash-table)
 (begin
   (let ((sub-name "test-transform-uu-vv-to-xyzd-1")
         (test-list
          (list
           (list 1 1 (list))
           (list 3 9 (list 12 9 6 3))
           (list 1 27 (list 34 27 20 7))
           (list 4 8 (list 11 8 5 3))
           (list 1 39 (list 49 39 29 10))
           (list 1 10 (list))
           (list 2 5 (list))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((uu (list-ref this-list 0))
                  (vv (list-ref this-list 1))
                  (shouldbe-list-list (list-ref this-list 2)))
              (let ((result-list-list (transform-uu-vv-to-xyzd uu vv)))
                (let ((slen (length shouldbe-list-list))
                      (rlen (length result-list-list)))
                  (let ((err-1
                         (format
                          #f "~a : error (~a) : uu=~a, vv=~a, "
                          sub-name test-label-index uu vv))
                        (err-2
                         (format
                          #f "shouldbe length=~a, result length=~a"
                          slen rlen)))
                    (begin
                      (unittest2:assert?
                       (equal? slen rlen)
                       sub-name
                       (string-append err-1 err-2)
                       result-hash-table)

                      (for-each
                       (lambda (slist)
                         (begin
                           (let ((err-3
                                  (format
                                   #f "shouldbe=~a, result=~a"
                                   slist result-list-list)))
                             (begin
                               (unittest2:assert?
                                (not
                                 (equal?
                                  (member slist result-list-list)
                                  #f))
                                sub-name
                                (string-append err-1 err-3)
                                result-hash-table)
                               ))
                           )) shouldbe-list-list)
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; count primes that are p = 3 mod 4, that are less than or equal to n
;;; sieve of eratosthenes method
(define (count-case-1-primes max-num)
  (begin
    (let ((intermediate-array (make-array 0 (1+ max-num)))
          (results-count 0))
      (begin
        (do ((ii 0 (1+ ii)))
            ((> ii max-num))
          (begin
            (array-set! intermediate-array ii ii)
            ))

        (do ((ii 2 (1+ ii)))
            ((> ii max-num))
          (begin
            (let ((this-num
                   (array-ref intermediate-array ii)))
              (begin
                (if (= this-num ii)
                    (begin
                      (if (= (modulo this-num 4) 3)
                          (begin
                            (set! results-count
                                  (1+ results-count))
                            ))
                      (do ((jj (+ ii ii) (+ jj ii)))
                          ((> jj max-num))
                        (begin
                          (array-set!
                           intermediate-array -1 jj)
                          ))
                      ))
                ))
            ))

        results-count
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-count-case-1-primes-1 result-hash-table)
 (begin
   (let ((sub-name "test-count-case-1-primes-1")
         (test-list
          (list
           (list 5 1) (list 7 2) (list 11 3)
           (list 12 3) (list 19 4) (list 20 4)
           (list 23 5) (list 31 6) (list 43 7)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((max-num (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result (count-case-1-primes max-num)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : max-num=~a, "
                        sub-name test-label-index max-num))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define-syntax count-case-4-solutions-macro
  (syntax-rules ()
    ((count-case-4-solutions-macro
      max-num results-count debug-flag)
     (begin
       (let ((max-4-mm (euclidean/ max-num 4)))
         (let ((odd-prime-array (make-odd-prime-array max-4-mm)))
           (begin
             ;;; add in special case n=4
             (set! results-count (1+ results-count))

             ;;; now take care of everything else
             (let ((count (car (array-dimensions odd-prime-array))))
               (begin
                 (set! results-count (+ results-count count))
                 ))
             )))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax count-case-6-solutions-macro
  (syntax-rules ()
    ((count-case-6-solutions-macro
      max-num results-count debug-flag)
     (begin
       (let ((max-16-mm (euclidean/ max-num 16)))
         (let ((odd-prime-array (make-odd-prime-array max-16-mm)))
           (begin
             ;;; add in special case n=16
             (set! results-count (1+ results-count))

             ;;; now take care of everything else
             (let ((count (car (array-dimensions odd-prime-array))))
               (begin
                 (set! results-count (+ results-count count))
                 ))
             )))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop max-num debug-flag)
  (begin
    (let ((results-count 0)
          (start-jday (srfi-19:current-julian-day)))
      (begin
        (let ((c1-count (count-case-1-primes max-num)))
          (begin
            (set! results-count (+ results-count c1-count))
            ))

        (gc)
        (let ((end-jday (srfi-19:current-julian-day)))
          (begin
            (display
             (ice-9-format:format
              #f "completed case 1 : counts = ~:d~%"
              results-count))
            (display
             (ice-9-format:format
              #f "  elapsed time = ~a : ~a~%"
              (timer-module:julian-day-difference-to-string
               end-jday start-jday)
              (timer-module:current-date-time-string)))
            (force-output)
            (set! start-jday end-jday)
            ))

        (count-case-4-solutions-macro
         max-num results-count debug-flag)
        (gc)
        (let ((end-jday (srfi-19:current-julian-day)))
          (begin
            (display
             (ice-9-format:format
              #f "completed case 4 : counts = ~:d~%"
              results-count))
            (display
             (ice-9-format:format
              #f "  elapsed time = ~a : ~a~%"
              (timer-module:julian-day-difference-to-string
               end-jday start-jday)
              (timer-module:current-date-time-string)))
            (force-output)
            (set! start-jday end-jday)
            ))

        (count-case-6-solutions-macro
         max-num results-count debug-flag)
        (gc)
        (let ((end-jday (srfi-19:current-julian-day)))
          (begin
            (display
             (ice-9-format:format
              #f "completed case 6 : counts = ~:d~%"
              results-count))
            (display
             (ice-9-format:format
              #f "  elapsed time = ~a : ~a~%"
              (timer-module:julian-day-difference-to-string
               end-jday start-jday)
              (timer-module:current-date-time-string)))
            (force-output)
            (set! start-jday end-jday)
            ))

        (display
         (ice-9-format:format
          #f "Found ~:d values of n with exactly 1 "
          results-count))
        (display
         (ice-9-format:format
          #f "distinct solutions~%  (less than ~:d).~%"
          max-num))
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "The positive integers, x, y, and "))
    (display
     (format #f "z, are consecutive~%"))
    (display
     (format #f "terms of an arithmetic progression. "))
    (display
     (format #f "Given that n~%"))
    (display
     (format #f "is a positive integer, the "))
    (display
     (format #f "equation,~%"))
    (display
     (format #f "x^2 - y^2 - z^2 = n~%"))
    (display
     (format #f "has exactly one solution "))
    (display
     (format #f "when n = 20:~%"))
    (newline)
    (display
     (format #f "13^2 - 10^2 - 7^2 = 20~%"))
    (newline)
    (display
     (format #f "In fact there are twenty-five "))
    (display
     (format #f "values of n below~%"))
    (display
     (format #f "one hundred for which the "))
    (display
     (format #f "equation has a~%"))
    (display
     (format #f "unique solution.~%"))
    (newline)
    (display
     (format #f "How many values of n less than "))
    (display
     (format #f "fifty million have~%"))
    (display
     (format #f "exactly one solution?~%"))
    (newline)
    (display
     (format #f "The solution was found at~%"))
    (display
     (format #f "https://mycode.doesnot.run/2016/04/11/squares/~%"))
    (newline)
    (display
     (format #f "x^2 - y^2 - z^2 = n, where x, y, and z~%"))
    (display
     (format #f "form an~%"))
    (display
     (format #f "arithmetic progression, which means "))
    (display
     (format #f "y=z+d, x=y+d=z+2d.~%"))
    (newline)
    (display
     (format #f "x^2 - y^2 - z^2~%"))
    (display
     (format #f "= (z + 2d)^2 - (z + d)^2 - z^2~%"))
    (display
     (format #f "= 3d^2 + 2dz - z^2~%"))
    (display
     (format #f "= n > 0.~%"))
    (newline)
    (display
     (format #f "Make a change of variables, u=3d-z, "))
    (display
     (format #f "v=d+z, then~%"))
    (display
     (format #f "3d^2 + 2dz - z^2 = n gets transformed "))
    (display
     (format #f "to u*v=n.~%"))
    (display
     (format #f "Reversing the change of "))
    (display
     (format #f "variables yields~%"))
    (display
     (format #f "d=(u+v)/4 and z=(3v-u)/4, and d and "))
    (display
     (format #f "z are positive~%"))
    (display
     (format #f "integers.~%"))
    (newline)
    (display
     (format #f "n = u*v, where d=(u+v)/4, "))
    (display
     (format #f "z=(3v-u)/4~%"))
    (newline)
    (display
     (format #f "For example, when n=27, the "))
    (display
     (format #f "divisors of 27~%"))
    (display
     (format #f "are 1 and 27, and 3 and 9. For "))
    (display
     (format #f "(1, 27), z=20~%"))
    (display
     (format #f "and d=7, and for (3, 9), z=6 and d=3. "))
    (display
     (format #f "Also when~%"))
    (display
     (format #f "n=10, the divisors of 10 are "))
    (display
     (format #f "(1, 10) and (2, 5).~%"))
    (display
     (format #f "For (1, 10), z=29/4 and d=11/4, and "))
    (display
     (format #f "for (2, 5), z=13/4~%"))
    (display
     (format #f "and d=7/4. When n=u*v=5*7=35, and "))
    (display
     (format #f "d=(u+v)/4=(5+7)/4=12/4=3,~%"))
    (display
     (format #f "and z=(3v-u)/4=(21-5)/4=16/4=4.~%"))
    (newline)
    (display
     (format #f "Consider d=(u+v)/4 and z=(3v-u)/4, "))
    (display
     (format #f "both d>0, z>0.~%"))
    (display
     (format #f "This means that (u+v) and (3v-u) "))
    (display
     (format #f "a multiple of 4.~%"))
    (display
     (format #f "We see that single solution values "))
    (display
     (format #f "of n breaks~%"))
    (display
     (format #f "down into simple cases. Consider "))
    (display
     (format #f "n=(2^exp)*M, where~%"))
    (display
     (format #f "M contains no factors of 2.~%"))
    (newline)
    (display
     (format #f "case (1) exp=0, M is an odd prime, "))
    (display
     (format #f "n=p<max-num.~%"))
    (display
     (format #f "If M is an odd prime p, then there "))
    (display
     (format #f "is only one~%"))
    (display
     (format #f "possible solution u=1 v=p, where "))
    (display
     (format #f "(p+1)/4 an~%"))
    (display
     (format #f "integer and (3p-1)/4 an integer "))
    (display
     (format #f "when p=3 mod 4,~%"))
    (display
     (format #f "or p=4k+3 for some integer k. "))
    (display
     (format #f "(u+v)/4~%"))
    (display
     (format #f "= (4k+3+1)/4 = (4k+4)/4 an integer. "))
    (display
     (format #f "Second condition:~%"))
    (display
     (format #f "(3v-u)/4 = (3*(4k+3)-1)/4 = "))
    (display
     (format #f "(12k+9-1)/4 = (12k+4)/4~%"))
    (display
     (format #f "an integer. Note that v=p and u=1 "))
    (display
     (format #f "means that z<=0,~%"))
    (display
     (format #f "so there is only one valid solution "))
    (display
     (format #f "when (u=1, v=p),~%"))
    (display
     (format #f "p a prime such that p=3 mod 4. For "))
    (display
     (format #f "example (u=1, v=3) :~%"))
    (display
     (format #f "4^2 - 3^2 - 2^2 = 3~%"))
    (newline)
    (display
     (format #f "case (2) exp=0, M odd integer, "))
    (display
     (format #f "M=qr, q and r~%"))
    (display
     (format #f "are odd numbers. (u=1, v=qr) and "))
    (display
     (format #f "(u=q, v=r) have~%"))
    (display
     (format #f "either zero, two, or three solutions "))
    (display
     (format #f "that satisfy~%"))
    (display
     (format #f "(u+v)/4 and (3v-u)/4 an integer. "))
    (display
     (format #f "Let q=2j+1,~%"))
    (display
     (format #f "r=2k+1, with q<r and j<k, and "))
    (display
     (format #f "consider the case~%"))
    (display
     (format #f "(u=1, v=qr) : (u+v)/4 "))
    (display
     (format #f "= (1+ (2j+1)*(2k+1))/4~%"))
    (display
     (format #f "= (4jk+2(j+k)+2)/4 = (2jk+j+k+1)/2. "))
    (display
     (format #f "Since q<r, then j<k~%"))
    (display
     (format #f "and let k=j+a, so (2jk+j+k+1)/2 "))
    (display
     (format #f "= (2jk+2j+a+1)/2~%"))
    (display
     (format #f "is an integer when a is odd, so "))
    (display
     (format #f "no solutions when~%"))
    (display
     (format #f "a is even. For the second condition "))
    (display
     (format #f "of (u=1, v=qr)~%"))
    (display
     (format #f ": (3v-u)/4 = (3(2j+1)*(2k+1)-1)/4 "))
    (display
     (format #f "= (3(4jk+2(j+k)+1)-1)/4~%"))
    (display
     (format #f "= (12jk+6(j+k)+2)/4 "))
    (display
     (format #f "= (12jk+6(2j+a)+2)/4~%"))
    (display
     (format #f "= (12jk+12j+2(3a+1))/4 is a solution "))
    (display
     (format #f "when a is odd~%"))
    (display
     (format #f "(set a=2b+1, then the term (3v-u)/4 is "))
    (display
     (format #f "an integer). For~%"))
    (display
     (format #f "example, (u=1, v=35=5*7) : then "))
    (display
     (format #f "q=5=2j+1=2*2+1,~%"))
    (display
     (format #f "r=7=2k+1=2*3+1, and k=3=j+a=2+1, "))
    (display
     (format #f "where a=1 is odd,~%"))
    (display
     (format #f "and the solution is 44^2 - 35^2 - 26^2 "))
    (display
     (format #f "= 35.~%"))
    (display
     (format #f "There are no solutions when "))
    (display
     (format #f "(u=1, v=45=5*9)~%"))
    (display
     (format #f "where q=5=2j+1=2*2+1, r=9=2k+1=2*4+1 "))
    (display
     (format #f "and a=2 is even.~%"))
    (display
     (format #f "The second case also has a solution "))
    (display
     (format #f "(u=q, v=r) :~%"))
    (display
     (format #f "(u+v)/4 = (2j+1+2k+1)/4 = (2(j+k)+2)/4 "))
    (display
     (format #f "if k=j+a~%"))
    (display
     (format #f "then (u+v)/4 = (2(2j+a)+2)/4 "))
    (display
     (format #f "= (2j+a+1)/2 a~%"))
    (display
     (format #f "solution is possible when a is odd. "))
    (display
     (format #f "Second condition~%"))
    (display
     (format #f "of (u=q, v=r) : (3v-u)/4 "))
    (display
     (format #f "= (3(2k+1)-(2j+1))/4~%"))
    (display
     (format #f "= (6k-2j+2)/4 = (6*(j+a)-2j+2)/4 "))
    (display
     (format #f "= (4j+2(3a+1)/4, and~%"))
    (display
     (format #f "we have a solution when a is odd, "))
    (display
     (format #f "no solution when~%"))
    (display
     (format #f "a is even. The third case (u=r, v=q) "))
    (display
     (format #f "is similar, only~%"))
    (display
     (format #f "the second conditions is different, "))
    (display
     (format #f "(u=r, v=q) :~%"))
    (display
     (format #f "(3v-u)/4 = (3(2j+1)-(2k+1))/4 "))
    (display
     (format #f "= (3(2j+1)-(2*(j+a)+1))/4~%"))
    (display
     (format #f "= (4j-2a+2)/4. For example when "))
    (display
     (format #f "(u=5, v=7) :~%"))
    (display
     (format #f "a=1 odd, 10^2 - 7^2 - 4^2 = 35, "))
    (display
     (format #f "and for (u=7, v=5) :~%"))
    (display
     (format #f "a=1 odd, 8^2 - 5^2 - 2^2 = 35. "))
    (display
     (format #f "There are no~%"))
    (display
     (format #f "solutions when (u=5, v=9) or "))
    (display
     (format #f "(u=9, v=5), since~%"))
    (display
     (format #f "a is even. In summary, in the "))
    (display
     (format #f "cases where~%"))
    (display
     (format #f "M=qr, q<r, q=2j+1, r=2k+1, k=j+a, "))
    (display
     (format #f "and a odd,~%"))
    (display
     (format #f "we have solutions when (u=1, v=qr), "))
    (display
     (format #f "(u=q, v=r),~%"))
    (display
     (format #f "and (u=r, v=q). When a is even, "))
    (display
     (format #f "no solutions.~%"))
    (display
     (format #f "So we either have no solutions or "))
    (display
     (format #f "too many solutions.~%"))
    (newline)
    (display
     (format #f "case (3) exp=1, n=2*M, M odd, this "))
    (display
     (format #f "will never produce~%"))
    (display
     (format #f "a solution since (2+M)/4 will never "))
    (display
     (format #f "be an integer.~%"))
    (newline)
    (display
     (format #f "case (4) exp=2, n=4*M, M an odd "))
    (display
     (format #f "number, 1<=M<max-num/4,~%"))
    (display
     (format #f "there is no possible solution when "))
    (display
     (format #f "(u=1, v=4M) : since~%"))
    (display
     (format #f "the condition (u+v)/4 = (1+4M)/4 is "))
    (display
     (format #f "never an integer.~%"))
    (display
     (format #f "When (u=2, v=2M), then (u+v)/4 "))
    (display
     (format #f "= (2+2M)/4 and M=2k+1,~%"))
    (display
     (format #f "so (u+v)/4 = (2+4k+2)/4 = (k+1) a "))
    (display
     (format #f "possible solution.~%"))
    (display
     (format #f "For the second condition (u=2, v=2M), "))
    (display
     (format #f "(3v-u)/4 = (6M-2)/4~%"))
    (display
     (format #f "= (6(2k+1)-2)/4 = (12k+4)/4 a solution. "))
    (display
     (format #f "When (u=2M, v=2),~%"))
    (display
     (format #f "the first condition is statisfied, "))
    (display
     (format #f "and the second is~%"))
    (display
     (format #f "(3v-u)/4 = (6 - 2M)/4 a positive "))
    (display
     (format #f "integer only for~%"))
    (display
     (format #f "M=1, or n=4. However, when M=1, "))
    (display
     (format #f "then u=2, v=2,~%"))
    (display
     (format #f "the same as in the previous case "))
    (display
     (format #f "of (u=2, v=2M),~%"))
    (display
     (format #f "so n=4 has just one solution, and "))
    (display
     (format #f "the rest n=4*M~%"))
    (display
     (format #f "has one solution when (u=2, v=2M), "))
    (display
     (format #f "1<=M<max-num/4,~%"))
    (display
     (format #f "M an odd integer. When (u=4, v=M), "))
    (display
     (format #f "then (u+v)/4~%"))
    (display
     (format #f "= (4+M)/4 can never be a solution. By "))
    (display
     (format #f "the same reasoning~%"))
    (display
     (format #f "as in case (2), we see that M must "))
    (display
     (format #f "be an odd~%"))
    (display
     (format #f "prime or 1.~%"))
    (newline)
    (display
     (format #f "case (5) exp=3, n=8*M, M odd prime, "))
    (display
     (format #f "M<max-num/8, there~%"))
    (display
     (format #f "are no solutions when (u=1, v=8M), "))
    (display
     (format #f "(u=8M, v=1),~%"))
    (display
     (format #f "(u=2, v=4M), (u=4M, v=2), "))
    (display
     (format #f "(u=4, v=2M),~%"))
    (display
     (format #f "(u=2M, v=4), (u=8, v=M), (u=M, v=8), "))
    (display
     (format #f "since M is an~%"))
    (display
     (format #f "odd integer and those cases do not "))
    (display
     (format #f "satisfy condition 1~%"))
    (display
     (format #f "(u+v)/4 a positive integer~%"))
    (newline)
    (display
     (format #f "case (6) exp=4, n=16*M, M odd prime "))
    (display
     (format #f "(similar reasoning~%"))
    (display
     (format #f "to case 2), 1<=M<max-num/16, only "))
    (display
     (format #f "solutions where~%"))
    (display
     (format #f "(u=4, v=4M) : condition 1 is "))
    (display
     (format #f "(u+v)/4 = (4+4M)/4~%"))
    (display
     (format #f "an integer, and (3v-u)/4 = (12M-4)/4 "))
    (display
     (format #f "an integer.~%"))
    (display
     (format #f "The other possibilities fail "))
    (display
     (format #f "condition 1~%"))
    (display
     (format #f "(u+v)/4 an integer.~%"))
    (newline)
    (display
     (format #f "case (7) exp=e > 4, n=(2^e)*M, M odd "))
    (display
     (format #f "prime, then an~%"))
    (display
     (format #f "arbitrary possbility is (u=2^g, "))
    (display
     (format #f "v=(2^(e-g))*M)~%"))
    (display
     (format #f "condition 1 is (u+v)/4 = (2^g - 2^(e-g)*M)/4 "))
    (display
     (format #f "is a solution~%"))
    (display
     (format #f "if 1<g<e-1, no solution if g=0, "))
    (display
     (format #f "g=1, g=e-1, or g=e.~%"))
    (display
     (format #f "condition 2 is (3v-u)/4 "))
    (display
     (format #f "= (3*2^(e-g)*M - 2^g)/4~%"))
    (display
     (format #f "also a solution when 1<g<e-1. However, "))
    (display
     (format #f "(u=2^(e-g)*M, v=2^g)~%"))
    (display
     (format #f "is also a solution by the same "))
    (display
     (format #f "reasoning. So for~%"))
    (display
     (format #f "all case 7 solutions there are zero, "))
    (display
     (format #f "2 or more solutions,~%"))
    (display
     (format #f "no single solutions.~%"))
    (display
     (format #f "see https://projecteuler.net/problem=136~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((max-num 100)
          (debug-flag #t))
      (begin
        (sub-main-loop max-num debug-flag)
        ))

    (newline)
    (let ((max-num 50000000)
          (debug-flag #f))
      (begin
        (sub-main-loop max-num debug-flag)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-24"))
      (let ((title-string
             (format #f "Project Euler 136 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)
          (force-output)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

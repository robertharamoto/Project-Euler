#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 138                                    ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated July 31, 2022                                ###
;;;###                                                       ###
;;;###  updated March 12, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
;;; initial solutions for Pell's equation y^2-5x^2 = -1
(define (find-initial-solutions)
  (begin
    (let ((nn 5)
          (kk -1)
          (max-xx 1000)
          (max-yy 1000))
      (begin
        (do ((xx 1 (1+ xx)))
            ((> xx max-xx))
          (begin
            (let ((xx-2 (* xx xx)))
              (begin
                (do ((yy 1 (1+ yy)))
                    ((> yy max-yy))
                  (begin
                    (let ((yy-2 (* nn yy yy)))
                      (begin
                        (if (= (- xx-2 yy-2) kk)
                            (begin
                              (display
                               (ice-9-format:format
                                #f "xx=~:d, yy=~:d, kk=~a~%"
                                xx yy kk))
                              (force-output)
                              ))
                        ))
                    ))
                ))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax cons-solution-macro
  (syntax-rules ()
    ((cons-solution-macro
      ll bb hh results-list)
     (begin
       (if (and (integer? bb) (> bb 0))
           (begin
             (set!
              results-list
              (cons (list ll bb hh) results-list))
             ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (calc-bb ll nn)
  (begin
    (let ((ll-2 (1- (* nn ll ll)))
          (results-list (list)))
      (let ((ll-sqrt (exact-integer-sqrt ll-2)))
        (begin
          (if (equal? ll-2 (* ll-sqrt ll-sqrt))
              (begin
                (let ((bb-1 (/ (+ -4 (* 2 ll-sqrt)) 5))
                      (bb-2 (/ (+ 4 (* 2 ll-sqrt)) 5))
                      (bb-3 (/ (- 4 (* 2 ll-sqrt)) 5)))
                  (let ((hh-1 (1+ bb-1))
                        (hh-2 (1- bb-2))
                        (hh-3 (1- bb-3)))
                    (begin
                      (cons-solution-macro
                       ll bb-1 hh-1 results-list)
                      (cons-solution-macro
                       ll bb-2 hh-2 results-list)
                      (cons-solution-macro
                       ll bb-3 hh-3 results-list)
                      )))

                (if (> (length results-list) 0)
                    (begin
                      results-list)
                    (begin
                      #f
                      )))
              (begin
                #f
                ))
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-calc-bb-1 result-hash-table)
 (begin
   (let ((sub-name "test-calc-bb-1")
         (test-list
          (list
           (list 17 5 (list (list 17 16 15)))
           (list 305 5 (list (list 305 272 273)))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((ll (list-ref this-list 0))
                  (nn (list-ref this-list 1))
                  (shouldbe-list (list-ref this-list 2)))
              (let ((result-list (calc-bb ll nn)))
                (let ((slen (length shouldbe-list))
                      (rlen (length result-list)))
                  (let ((err-1
                         (format
                          #f "~a : error (~a) : ll=~a, nn=~a, "
                          sub-name test-label-index ll nn))
                        (err-2
                         (format
                          #f "shouldbe length=~a, result length=~a"
                          slen rlen)))
                    (begin
                      (unittest2:assert?
                       (equal? slen rlen)
                       sub-name
                       (string-append err-1 err-2)
                       result-hash-table)

                      (for-each
                       (lambda (s-elem)
                         (begin
                           (let ((err-3
                                  (format
                                   #f "shouldbe=~a, result=~a"
                                   s-elem result-list)))
                             (begin
                               (unittest2:assert?
                                (not
                                 (equal?
                                  (member s-elem result-list)
                                  #f))
                                sub-name
                                (string-append err-1 err-3)
                                result-hash-table)
                               ))
                           )) shouldbe-list)
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; cc and dd solve the Pell's equation c^2 - 5d^2 = 1
;;; xx and yy solve the equation x^2 - 5y^2 = -1
(define (loop-over-positives
         xx-0 yy-0 cc-0 dd-0 nn max-num)
  (begin
    (let ((result-list (list))
          (xx-1 xx-0)
          (yy-1 yy-0)
          (cc-1 cc-0)
          (dd-1 dd-0)
          (count 0)
          (continue-loop-flag #t))
      (begin
        (while
         (equal? continue-loop-flag #t)
         (begin
           (let ((next-cc (+ (* cc-0 cc-1) (* nn dd-0 dd-1)))
                 (next-dd (+ (* cc-0 dd-1) (* dd-0 cc-1)))
                 (next-xx (+ (* xx-0 cc-1) (* nn yy-0 dd-1)))
                 (next-yy (+ (* xx-0 dd-1) (* cc-1 yy-0))))
             (begin
               (if (and (integer? yy-1)
                        (> yy-1 0))
                   (begin
                     (let ((rlist-list (calc-bb yy-1 nn)))
                       (begin
                         (if (and (list? rlist-list)
                                  (> (length rlist-list) 0))
                             (begin
                               (for-each
                                (lambda (a-list)
                                  (begin
                                    (let ((ll (list-ref a-list 0))
                                          (bb (list-ref a-list 1))
                                          (hh (list-ref a-list 2)))
                                      (begin
                                        (set!
                                         result-list
                                         (cons (list ll bb hh) result-list))
                                        (set! count (1+ count))
                                        ))
                                    )) rlist-list)
                               ))
                         ))
                     ))

               (set! cc-1 next-cc)
               (set! dd-1 next-dd)
               (set! xx-1 next-xx)
               (set! yy-1 next-yy)

               (if (>= count max-num)
                   (begin
                     (set! continue-loop-flag #f)
                     ))
               ))
           ))

        result-list
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-loop-over-positives-1 result-hash-table)
 (begin
   (let ((sub-name "test-loop-over-positives-1")
         (test-list
          (list
           (list 2 1 9 4 5 2 (list (list 17 16 15) (list 305 272 273)))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((xx-0 (list-ref this-list 0))
                  (yy-0 (list-ref this-list 1))
                  (cc-0 (list-ref this-list 2))
                  (dd-0 (list-ref this-list 3))
                  (nn (list-ref this-list 4))
                  (max-num (list-ref this-list 5))
                  (shouldbe-list (list-ref this-list 6)))
              (let ((result-list
                     (loop-over-positives
                      xx-0 yy-0 cc-0 dd-0 nn max-num)))
                (let ((slen (length shouldbe-list))
                      (rlen (length result-list)))
                  (let ((err-1
                         (format
                          #f "~a : error (~a) : xx-0=~a, yy-0=~a, "
                          sub-name test-label-index xx-0 yy-0))
                        (err-2
                         (format
                          #f "cc-0=~a, dd-0=~a, nn=~a, max-num=~a, "
                          cc-0 dd-0 nn max-num))
                        (err-3
                         (format
                          #f "shouldbe length=~a, result length=~a"
                          slen rlen)))
                    (begin
                      (unittest2:assert?
                       (equal? slen rlen)
                       sub-name
                       (string-append err-1 err-2 err-3)
                       result-hash-table)

                      (for-each
                       (lambda (s-elem)
                         (begin
                           (let ((err-4
                                  (format
                                   #f "shouldbe=~a, result=~a"
                                   s-elem result-list)))
                             (begin
                               (unittest2:assert?
                                (not
                                 (equal?
                                  (member s-elem result-list)
                                  #f))
                                sub-name
                                (string-append err-1 err-2 err-4)
                                result-hash-table)
                               ))
                           )) shouldbe-list)
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; cc and dd solve the Pell's equation cc^2 - 5d^2 = 1
;;; xx and yy solve the equation xx^2 - 5yy^2 = -1
(define (sub-main-loop target-count debug-flag)
  (begin
    (let ((count 0)
          (xx-0 2)
          (yy-0 1)
          (cc-0 9)
          (dd-0 4)
          (nn 5)
          (sum-ll 0)
          (results-list (list))
          (continue-loop-flag #t))
      (begin
        (let ((rlist1
               (loop-over-positives
                xx-0 yy-0 cc-0 dd-0
                nn target-count)))
          (begin
            (set!
             results-list
             (sort
              rlist1
              (lambda (a b)
                (begin
                  (< (car a) (car b))
                  ))
              ))

            (let ((rlen (length results-list)))
              (begin
                (do ((ii 0 (1+ ii)))
                    ((or (>= ii rlen)
                         (>= ii target-count)))
                  (begin
                    (let ((rlist (list-ref results-list ii)))
                      (let ((ll (list-ref rlist 0))
                            (bb (list-ref rlist 1))
                            (hh (list-ref rlist 2)))
                        (begin
                          (set! sum-ll (+ sum-ll ll))

                          (if (equal? debug-flag #t)
                              (begin
                                (display
                                 (ice-9-format:format
                                  #f "  (~:d)  L = ~:d, b = ~:d, "
                                  (1+ ii) ll bb))
                                (display
                                 (ice-9-format:format
                                  #f "h = ~:d, sum so far = ~:d~%"
                                  hh sum-ll))
                                (force-output)
                                ))
                          )))
                    ))
                ))

            (display
             (ice-9-format:format
              #f "The sum(L) = ~:d for the ~:d smallest "
              sum-ll target-count))
            (display
             (ice-9-format:format
              #f "isosceles triangles.~%"))
            (force-output)
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Consider the isosceles triangle "))
    (display
     (format #f "with base length,~%"))
    (display
     (format #f "b = 16, and legs, L = 17.~%"))
    (newline)
    (display
     (format #f "By using the Pythagorean theorem "))
    (display
     (format #f "it can be seen~%"))
    (display
     (format #f "that the height of the triangle, "))
    (display
     (format #f "h = sqrt(17^2 - 8^2)~%"))
    (display
     (format #f "= 15, which is one less than the "))
    (display
     (format #f "base length.~%"))
    (newline)
    (display
     (format #f "With b = 272 and L = 305, we get "))
    (display
     (format #f "h = 273, which is~%"))
    (display
     (format #f "one more than the base length, and "))
    (display
     (format #f "this is the second~%"))
    (display
     (format #f "smallest isosceles triangle with the "))
    (display
     (format #f "property that h = b +/- 1.~%"))
    (newline)
    (display
     (format #f "Find Sum(L) for the twelve smallest "))
    (display
     (format #f "isosceles triangles~%"))
    (display
     (format #f "for which h = b +/- 1 and b, L are "))
    (display
     (format #f "positive integers.~%"))
    (newline)
    (display
     (format #f "From the Pythagorean theorem, "))
    (display
     (format #f "L^2 = (b/2)^2 + h^2,~%"))
    (display
     (format #f "and h = b +/- 1, we have "))
    (display
     (format #f "L^2 = b^2/4 + (b +/- 1)^2~%"))
    (display
     (format #f "= b^2/4 + b^2 +/- 2b + 1 "))
    (display
     (format #f "= 5b^2/4 +/- 2b + 1.~%"))
    (newline)
    (display
     (format #f "b^2 +/- 8b/5 - 4(L^2 - 1)/5 = 0~%"))
    (newline)
    (display
     (format #f "Using the quadratic formula, when "))
    (display
     (format #f "8b/5 is positive~%"))
    (display
     (format #f "(h=b+1), b = -4/5 "))
    (display
     (format #f "+/- sqrt(64/25+16(L^2-1)/5)/2~%"))
    (display
     (format #f "= -4/5 +/- 2sqrt(4+5L^2-5)/5 "))
    (display
     (format #f "= -4/5 +/- 2sqrt(5L^2-1)/5.~%"))
    (display
     (format #f "When 8/5 is negative (h=b-1), "))
    (display
     (format #f "b = +4/5 +/- 2sqrt(5L^2-1)/5.~%"))
    (display
     (format #f "We can use Pell's equation to "))
    (display
     (format #f "find those values~%"))
    (display
     (format #f "of L for which 5L^2 - 1 is square. "))
    (display
     (format #f "Let 5y^2 - 1 = x^2,~%"))
    (display
     (format #f "where x an integer and y=L, then "))
    (display
     (format #f "x^2 - 5y^2 = -1.~%"))
    (display
     (format #f "See https://mathworld.wolfram.com/PellEquation.html~%"))
    (newline)
    (display
     (format #f "Initial solutions found "))
    (display
     (format #f "(x=2, y=1),~%"))
    (display
     (format #f "(x=38, y=17), (x=682, y=305).~%"))
    (display
     (format #f "see https://projecteuler.net/problem=138~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((target-count 2)
          (debug-flag #t))
      (begin
        (sub-main-loop target-count debug-flag)
        ))

    (newline)
    (let ((target-count 12)
          (debug-flag #f))
      (begin
        (sub-main-loop target-count debug-flag)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 138 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

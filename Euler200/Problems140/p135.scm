#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 135                                    ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated July 31, 2022                                ###
;;;###                                                       ###
;;;###  updated March 12, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### srfi-19 for date functions
(use-modules ((srfi srfi-19)
              :renamer (symbol-prefix-proc 'srfi-19:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
;;; u=3d-z, v=d+z
;;; d=(u+v)/4 and z=(3v-u)/4
(define (transform-uu-vv-to-xyzd uu vv)
  (begin
    (let ((result-list (list))
          (dtmp1 (+ uu vv))
          (ztmp1 (- (* 3 vv) uu)))
      (let ((dtmp2 (euclidean/ dtmp1 4))
            (ztmp2 (euclidean/ ztmp1 4)))
        (begin
          (if (and (= (* 4 dtmp2) dtmp1)
                   (= (* 4 ztmp2) ztmp1)
                   (> ztmp2 0))
              (begin
                (let ((xx (+ ztmp2 dtmp2 dtmp2))
                      (yy (+ ztmp2 dtmp2))
                      (zz ztmp2))
                  (begin
                    (set! result-list
                          (list xx yy zz dtmp2))
                    ))
                ))

          result-list
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-transform-uu-vv-to-xyzd-1 result-hash-table)
 (begin
   (let ((sub-name "test-transform-uu-vv-to-xyzd-1")
         (test-list
          (list
           (list 1 1 (list))
           (list 3 9 (list 12 9 6 3))
           (list 1 27 (list 34 27 20 7))
           (list 4 8 (list 11 8 5 3))
           (list 1 39 (list 49 39 29 10))
           (list 1 10 (list))
           (list 2 5 (list))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((uu (list-ref this-list 0))
                  (vv (list-ref this-list 1))
                  (shouldbe-list-list (list-ref this-list 2)))
              (let ((result-list-list (transform-uu-vv-to-xyzd uu vv)))
                (let ((slen (length shouldbe-list-list))
                      (rlen (length result-list-list)))
                  (let ((err-1
                         (format
                          #f "~a : error (~a) : "
                          sub-name test-label-index))
                        (err-2
                         (format
                          #f "uu=~a, vv=~a, "
                          uu vv))
                        (err-3
                         (format
                          #f "shouldbe length=~a, result length=~a"
                          slen rlen)))
                    (begin
                      (unittest2:assert?
                       (equal? slen rlen)
                       sub-name
                       (string-append
                        err-1 err-2 err-3)
                       result-hash-table)

                      (for-each
                       (lambda (slist)
                         (begin
                           (let ((err-3
                                  (format
                                   #f "shouldbe=~a, result=~a"
                                   slist result-list-list)))
                             (begin
                               (unittest2:assert?
                                (not
                                 (equal?
                                  (member slist result-list-list)
                                  #f))
                                sub-name
                                (string-append
                                 err-1 err-2 err-3)
                                result-hash-table)
                               ))
                           )) shouldbe-list-list)
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (calc-solutions-list uu vv)
  (begin
    (let ((results-list-list (list)))
      (begin
        (let ((dtmp (+ uu vv)))
          (let ((dd (euclidean/ dtmp 4)))
            (begin
              (if (= (* 4 dd) dtmp)
                  (begin
                    (let ((ztmp-1 (- (* 3 vv) uu))
                          (ztmp-2 (- (* 3 uu) vv)))
                      (let ((zz-1 (euclidean/ ztmp-1 4))
                            (zz-2 (euclidean/ ztmp-2 4)))
                        (begin
                          (if (and (= (* 4 zz-1) ztmp-1)
                                   (> zz-1 0))
                              (begin
                                (set!
                                 results-list-list
                                 (cons (list uu vv)
                                       results-list-list))
                                ))

                          (if (and (not (equal? uu vv))
                                   (= (* 4 zz-2) ztmp-2)
                                   (> zz-2 0))
                              (begin
                                (set!
                                 results-list-list
                                 (cons (list vv uu)
                                       results-list-list))
                                ))
                          )))
                    ))
              )))
        results-list-list
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-calc-solutions-list-1 result-hash-table)
 (begin
   (let ((sub-name "test-calc-solutions-list-1")
         (test-list
          (list
           (list 1 12 (list))
           (list 2 6 (list (list 2 6)))
           (list 3 4 (list))
           (list 1 15 (list (list 1 15)))
           (list 3 5 (list (list 3 5) (list 5 3)))
           (list 1 27 (list (list 1 27)))
           (list 3 9 (list (list 3 9)))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((uu (list-ref this-list 0))
                  (vv (list-ref this-list 1))
                  (shouldbe-list-list (list-ref this-list 2)))
              (let ((result-list-list (calc-solutions-list uu vv)))
                (let ((slen (length shouldbe-list-list))
                      (rlen (length result-list-list)))
                  (let ((err-1
                         (format
                          #f "~a : error (~a) : uu=~a, vv=~a, "
                          sub-name test-label-index uu vv))
                        (err-2
                         (format
                          #f "shouldbe length=~a, result length=~a"
                          slen rlen)))
                    (begin
                      (unittest2:assert?
                       (equal? slen rlen)
                       sub-name
                       (string-append err-1 err-2)
                       result-hash-table)

                      (for-each
                       (lambda (slist)
                         (begin
                           (let ((err-3
                                  (format
                                   #f "shouldbe=~a, result=~a"
                                   slist result-list-list)))
                             (begin
                               (unittest2:assert?
                                (not
                                 (equal?
                                  (member slist result-list-list)
                                  #f))
                                sub-name
                                (string-append err-1 err-3)
                                result-hash-table)
                               ))
                           )) shouldbe-list-list)
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define-syntax debug-display-macro
  (syntax-rules ()
    ((debug-display-macro uu vv nn)
     (begin
       (let ((rlist (transform-uu-vv-to-xyzd uu vv)))
         (let ((xx (list-ref rlist 0))
               (yy (list-ref rlist 1))
               (zz (list-ref rlist 2))
               (dd (list-ref rlist 3)))
           (begin
             (display
              (ice-9-format:format
               #f "    (~:d, ~:d)  ~:d^2 - ~:d^2 - ~:d^2 "
               uu vv xx yy zz))
             (display
              (ice-9-format:format
               #f "= ~:d : delta = ~:d~%"
               nn dd))
             (force-output)
             )))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax process-uu-vv-pair-macro
  (syntax-rules ()
    ((process-uu-vv-pair-macro
      uu vv max-num continue-loop-flag
      debug-flag results-htable)
     (begin
       (let ((nn (* uu vv)))
         (begin
           (if (< nn max-num)
               (begin
                 (let ((this-solution-list
                        (calc-solutions-list uu vv)))
                   (let ((dlen
                          (length this-solution-list)))
                     (begin
                       (cond
                        ((= dlen 1)
                         (begin
                           (let ((tcount
                                  (hash-ref
                                   results-htable nn 0)))
                             (begin
                               (hash-set!
                                results-htable nn (1+ tcount))

                               (if (equal? debug-flag #t)
                                   (begin
                                     (debug-display-macro uu vv nn)
                                     (newline)
                                     ))
                               ))
                           ))
                        ((= dlen 2)
                         (begin
                           (let ((tcount
                                  (hash-ref results-htable nn 0)))
                             (begin
                               (hash-set!
                                results-htable nn (+ tcount 2))

                               (if (equal? debug-flag #t)
                                   (begin
                                     (debug-display-macro uu vv nn)
                                     (debug-display-macro vv uu nn)
                                     (newline)
                                     ))
                               ))
                           )))
                       ))
                   ))
               (begin
                 (set! continue-loop-flag #f)
                 ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
;;; note: xx^2 - yy^2 - zz^2 = (zz+2dd)^2 - (zz+dd)^2 - zz^2
;;; = 3dd^2 + 2dd*zz - zz^2 = nn
;;; let u=3d-z, v=d+z, then xx^2 - yy^2 - zz^2 = u*v = n
;;; where d=(u+v)/4 and z=(3v-u)/4
(define-syntax count-solutions-macro
  (syntax-rules ()
    ((count-solutions-macro
      max-num num-solutions status-num debug-flag
      results-htable)
     (begin
       (let ((start-jday (srfi-19:current-julian-day))
             (max-uu (1+ (exact-integer-sqrt max-num))))
         (begin
           (do ((uu 1 (1+ uu)))
               ((>= uu max-uu))
             (begin
               (let ((continue-loop-flag #t)
                     (max-vv (1+ (euclidean/ max-num uu))))
                 (begin
                   (do ((vv uu (1+ vv)))
                       ((or (> vv max-vv)
                            (equal? continue-loop-flag #f)))
                     (begin
                       (process-uu-vv-pair-macro
                        uu vv max-num continue-loop-flag
                        debug-flag results-htable)
                       ))
                   ))

               (if (zero? (modulo uu status-num))
                   (begin
                     (display
                      (format
                       #f "  ~:d / ~:d : count so far = ~:d : "
                       uu max-uu results-count))
                     (let ((end-jday (srfi-19:current-julian-day)))
                       (begin
                         (display
                          (format
                           #f "elapsed time = ~a : ~a~%"
                           (timer-module:julian-day-difference-to-string
                            end-jday start-jday)
                           (timer-module:current-date-time-string)))
                         (force-output)
                         (set! start-jday end-jday)
                         ))
                     ))
               ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop
         max-num num-solutions status-num debug-flag)
  (begin
    (let ((results-htable (make-hash-table)))
      (begin
        (count-solutions-macro
         max-num num-solutions status-num debug-flag
         results-htable)

        (let ((min-nn -1)
              (results-count 0))
          (begin
            (hash-for-each
             (lambda (nn ncount)
               (begin
                 (if (= ncount num-solutions)
                     (begin
                       (if (or (< min-nn 0)
                               (< nn min-nn))
                           (begin
                             (set! min-nn nn)
                             ))
                       (set! results-count (1+ results-count))
                       ))
                 )) results-htable)

            (display
             (ice-9-format:format
              #f "Found ~:d values of n with exactly ~:d "
              results-count num-solutions))
            (display
             (ice-9-format:format
              #f "distinct solutions (less than ~:d).~%"
              max-num))
            (display
             (ice-9-format:format
              #f "  The smallest is equal to ~:d.~%"
              min-nn))
            (force-output)
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Given the positive integers, x, y, "))
    (display
     (format #f "and z, are~%"))
    (display
     (format #f "consecutive terms of an arithmetic "))
    (display
     (format #f "progression, the~%"))
    (display
     (format #f "least value of the positive integer, "))
    (display
     (format #f "n, for which the~%"))
    (display
     (format #f "equation, x^2 - y^2 - z^2 = n,~%"))
    (display
     (format #f "has exactly two solutions is "))
    (display
     (format #f "n = 27:~%"))
    (newline)
    (display
     (format #f "  34^2 - 27^2 - 20^2 = "))
    (display
     (format #f "12^2 - 9^2 - 6^2 = 27~%"))
    (newline)
    (display
     (format #f "It turns out that n = 1155 is the "))
    (display
     (format #f "least value which~%"))
    (display
     (format #f "has exactly ten solutions.~%"))
    (newline)
    (display
     (format #f "How many values of n less than "))
    (display
     (format #f "one million have~%"))
    (display
     (format #f "exactly ten distinct solutions?~%"))
    (newline)
    (display
     (format #f "The solution was found at~%"))
    (display
     (format #f "https://euler.stephan-brumme.com/135/~%"))
    (newline)
    (display
     (format #f "x^2 - y^2 - z^2 = n~%"))
    (display
     (format #f "where x, y, and z form an arithmetic "))
    (display
     (format #f "progression,~%"))
    (display
     (format #f "which means y=z+d, "))
    (display
     (format #f "x=y+d=z+2d.~%"))
    (newline)
    (display
     (format #f "x^2 - y^2 - z^2 "))
    (display
     (format #f "= (z + 2d)^2 - (z + d)^2 - z^2~%"))
    (display
     (format #f "= 3d^2 + 2dz - z^2 "))
    (display
     (format #f "= n > 0.~%"))
    (newline)
    (display
     (format #f "Make a change of variables, "))
    (display
     (format #f "u=3d-z, v=d+z,~%"))
    (display
     (format #f "then 3d^2 + 2dz - z^2 "))
    (display
     (format #f "= n gets~%"))
    (display
     (format #f "transformed to uv=n. So for each n, "))
    (display
     (format #f "we only need~%"))
    (display
     (format #f "to check the divisor pairs of n "))
    (display
     (format #f "such that d=(u+v)/4~%"))
    (display
     (format #f "and z=(3v-u)/4, and d and z are "))
    (display
     (format #f "positive integers.~%"))
    (display
     (format #f "Alternatively, for each integer pair "))
    (display
     (format #f "u and v, we can~%"))
    (display
     (format #f "compute the product uv=n and see if "))
    (display
     (format #f "d and z~%"))
    (display
     (format #f "are integers, this way there is no "))
    (display
     (format #f "need to compute~%"))
    (display
     (format #f "the factors of n.~%"))
    (newline)
    (display
     (format #f "For example, when n=27, the divisors "))
    (display
     (format #f "of 27 are~%"))
    (display
     (format #f "1 and 27, and 3 and 9. For (1, 27), "))
    (display
     (format #f "z=20 and d=7,~%"))
    (display
     (format #f "and for (3, 9), z=6 and d=3. Also "))
    (display
     (format #f "when n=10, the~%"))
    (display
     (format #f "divisors of 10 are (1, 10) and "))
    (display
     (format #f "(2, 5). For (1, 10),~%"))
    (display
     (format #f "z=29/4 and d=11/4, and for (2, 5), "))
    (display
     (format #f "z=13/4 and d=7/4.~%"))
    (display
     (format #f "see https://projecteuler.net/problem=135~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((max-num 100)
          (num-solutions 2)
          (status-num 10000)
          (debug-flag #f))
      (begin
        (sub-main-loop
         max-num num-solutions status-num debug-flag)
        ))

    (newline)
    (let ((max-num 1000000)
          (num-solutions 10)
          (status-num 10000000)
          (debug-flag #f))
      (begin
        (sub-main-loop
         max-num num-solutions status-num debug-flag)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 135 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

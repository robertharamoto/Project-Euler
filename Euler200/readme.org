################################################################
################################################################
###                                                          ###
###  project euler exercises                                 ###
###                                                          ###
###  written by Robert Haramoto                              ###
###                                                          ###
################################################################
################################################################

This is a repository for the project euler problems.
https://projecteuler.net/

Some great hints can be found from:
https://mathproblems123.wordpress.com/2017/03/28/project-euler-tips/


The programs are written in guile 3, a version of scheme, to
encourage interest in this nice language.

They can be used as a template or library for commonly used
functions, (prime?, split-digits, ...)

Simple unit tests are included and run each time the program
is run, and help to give confidence that the programs run
correctly.  Example results are reproduced from the problem
statement to also show that the algorithms are ok.


These programs are dedicated to the public domain.
See the UNLICENSE file, or https://unlicense.org/

Assumes that guile 3.0 is located in the /usr/bin directory.

See also:

https://www.gnu.org/software/guile/
https://en.wikipedia.org/wiki/Scheme_(programming_language)

################################################################
################################################################
History

last updated <2024-09-26 Thu>
updated <2020-03-13 Fri>

################################################################
################################################################
###                                                          ###
###  end of file                                             ###
###                                                          ###
################################################################
################################################################

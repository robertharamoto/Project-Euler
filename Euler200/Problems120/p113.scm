#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 113                                    ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated July 29, 2022                                ###
;;;###                                                       ###
;;;###  updated March 11, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (factorial ii-num)
  (begin
    (cond
     ((<= ii-num 1)
      (begin
        1
        ))
     ((= ii-num 2)
      (begin
        2
        ))
     ((= ii-num 3)
      (begin
        6
        ))
     ((= ii-num 4)
      (begin
        24
        ))
     ((= ii-num 5)
      (begin
        120
        ))
     ((= ii-num 6)
      (begin
        720
        ))
     ((= ii-num 7)
      (begin
        5040
        ))
     ((= ii-num 8)
      (begin
        40320
        ))
     ((= ii-num 9)
      (begin
        362880
        ))
     (else
      (begin
        (* ii-num (factorial (- ii-num 1)))
        )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-factorial-1 result-hash-table)
 (begin
   (let ((sub-name "test-factorial-1")
         (test-list
          (list
           (list 0 1) (list 1 1) (list 2 2)
           (list 3 6) (list 4 24) (list 5 120)
           (list 6 720) (list 7 5040)
           (list 8 40320) (list 9 362880)
           (list 10 3628800)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-num (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result (factorial test-num)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : test-num=~a, "
                        sub-name test-label-index test-num))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a~%"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (choose kk nn)
  (define (local-partial-factorial larger-num smaller-num)
    (begin
      (let ((result 1))
        (begin
          (do ((ii larger-num (- ii 1)))
              ((<= ii smaller-num))
            (begin
              (set! result (* result ii))
              ))
          result
          ))
      ))
  (begin
    (cond
     ((= kk 0)
      (begin
        0
        ))
     ((= nn 0)
      (begin
        1
        ))
     ((= kk nn)
      (begin
        1
        ))
     ((>= kk nn)
      (begin
        (let ((kfact (local-partial-factorial kk nn))
              (nfact (factorial (- kk nn))))
          (let ((result (/ kfact nfact)))
            (begin
              result
              )))
        ))
     (else
      (begin
      ;;; (< kk nn)
        (let ((kfact (factorial (- nn kk)))
              (nfact (local-partial-factorial nn kk)))
          (let ((result (/ nfact kfact)))
            (begin
              result
              )))
        )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-choose-1 result-hash-table)
 (begin
   (let ((sub-name "test-choose-1")
         (test-list
          (list
           (list 0 0 0) (list 1 0 1) (list 0 1 0)
           (list 2 0 1) (list 2 1 2) (list 2 2 1)
           (list 3 0 1) (list 3 1 3) (list 3 2 3) (list 3 3 1)
           (list 4 0 1) (list 4 1 4) (list 4 2 6) (list 4 3 4)
           (list 4 4 1)
           (list 5 0 1) (list 5 1 5) (list 5 2 10) (list 5 3 10)
           (list 5 4 5) (list 5 5 1)
           (list 6 0 1) (list 6 1 6) (list 6 2 15) (list 6 3 20)
           (list 6 4 15) (list 6 5 6) (list 6 6 1)
           (list 0 6 0) (list 1 6 6) (list 2 6 15) (list 3 6 20)
           (list 4 6 15) (list 5 6 6) (list 6 6 1)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-kk (list-ref alist 0))
                  (test-nn (list-ref alist 1))
                  (shouldbe-num (list-ref alist 2)))
              (let ((result-num
                     (choose test-kk test-nn)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "test-kk=~a, test-nn=~a, "
                        test-kk test-nn))
                      (err-3
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe-num result-num)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-num result-num)
                     sub-name
                     (string-append
                      err-1 err-2 err-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; lattice path algorithm assumes going from (0,0) to (a,b)
;;; to map to the problem, need to reduce 1 - 9 digits to 0 - 8
(define (count-increasing-numbers num-digits)
  (begin
    (cond
     ((<= num-digits 1)
      (begin
        #f
        ))
     (else
      (begin
        (let ((nn (+ num-digits 8)))
          (let ((ncr (choose nn 8)))
            (let ((count (- ncr 9)))
              (begin
                ;;; removed the 9 flat numbers, 11111, 22222,..., 99999
                count
                ))
            ))
        )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-count-increasing-numbers-1 result-hash-table)
 (begin
   (let ((sub-name "test-count-increasing-numbers-1")
         (test-list
          (list
           (list 2 36) (list 3 156)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((num-digits (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result
                     (count-increasing-numbers num-digits)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : num-digits=~a, "
                        sub-name test-label-index num-digits))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; lattice path algorithm assumes going from (0,0) to (a,b)
(define (count-decreasing-numbers num-digits)
  (begin
    (cond
     ((<= num-digits 1)
      (begin
        #f
        ))
     (else
      (begin
        (let ((nn (+ num-digits 9)))
          (let ((ncr (choose nn 9)))
            (let ((count (- ncr 10)))
              (begin
                ;;; removed the 10 flat numbers, 00000, 11111, 22222,..., 99999
                count
                ))
            ))
        )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-count-decreasing-numbers-1 result-hash-table)
 (begin
   (let ((sub-name "test-count-decreasing-numbers-1")
         (test-list
          (list
           (list 2 45) (list 3 210)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((num-digits (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result
                     (count-decreasing-numbers num-digits)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : num-digits=~a, "
                        sub-name test-label-index num-digits))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (reproduce-problem-statement)
  (begin
    (let ((increasing-2d (count-increasing-numbers 2))
          (decreasing-2d (count-decreasing-numbers 2))
          (flat-2d 9)
          (increasing-3d (count-increasing-numbers 3))
          (decreasing-3d (count-decreasing-numbers 3))
          (flat-3d 9))
      (let ((nbouncy-2d (- 90 (+ increasing-2d decreasing-2d flat-2d)))
            (nbouncy-3d (- 900 (+ increasing-3d decreasing-3d flat-3d))))
        (let ((total-2d (+ nbouncy-2d increasing-2d decreasing-2d flat-2d))
              (total-3d (+ nbouncy-3d increasing-3d decreasing-3d flat-3d)))
          (begin
            (display
             (ice-9-format:format
              #f "two digit numbers: increasing=~:d, decreasing=~:d~%"
              increasing-2d decreasing-2d))
            (display
             (ice-9-format:format
              #f "  flat=~:d, bouncy=~:d, total=~:d~%"
              flat-2d nbouncy-2d total-2d))
            (display
             (ice-9-format:format
              #f "three digit numbers: increasing=~:d, decreasing=~:d~%"
              increasing-3d decreasing-3d))
            (display
             (ice-9-format:format
              #f "  flat=~:d, bouncy=~:d, total=~:d~%"
              flat-3d nbouncy-3d total-3d))
            (force-output)
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop max-exponent debug-flag)
  (begin
    (let ((increasing-count 0)
          (decreasing-count 0)
          (flat-count 9)
          (bouncy-count 0)
          (total-count 90))
      (begin
        (do ((ii 2 (1+ ii)))
            ((> ii max-exponent))
          (begin
            (let ((ii-increasing (count-increasing-numbers ii))
                  (ii-decreasing (count-decreasing-numbers ii))
                  (ii-flat 9))
              (begin
                (let ((ii-bouncy
                       (- total-count
                          (+ ii-increasing ii-decreasing ii-flat))))
                  (begin
                    (set!
                     increasing-count (+ increasing-count ii-increasing))
                    (set!
                     decreasing-count (+ decreasing-count ii-decreasing))
                    (set!
                     flat-count (+ flat-count ii-flat))
                    (set!
                     bouncy-count (+ bouncy-count ii-bouncy))

                    (if (equal? debug-flag #t)
                        (begin
                          (display
                           (ice-9-format:format
                            #f "~:d digit numbers: increasing=~:d, "
                            ii increasing-count))
                          (display
                           (ice-9-format:format
                            #f "decreasing=~:d~%" decreasing-count))
                          (display
                           (ice-9-format:format
                            #f "  flat=~:d, bouncy=~:d, total=~:d~%"
                            flat-count bouncy-count total-count))
                          (force-output)
                          ))

                    (set! total-count (* total-count 10))
                    ))
                ))
            ))

        (let ((non-bouncy
               (+ increasing-count decreasing-count flat-count)))
          (let ((total (+ non-bouncy bouncy-count)))
            (begin
              (if (equal? debug-flag #t)
                  (begin
                    (newline)
                    ))
              (display
               (ice-9-format:format
                #f "~:d digit numbers: non-bouncy=~:d~%"
                max-exponent non-bouncy))
              (display
               (ice-9-format:format
                #f "  increasing=~:d, decreasing=~:d, flat=~:d~%"
                increasing-count decreasing-count flat-count))
              (display
               (ice-9-format:format
                #f "  bouncy=~:d, total=~:d~%"
                bouncy-count total))
              (display
               (ice-9-format:format
                #f "  (note: increasing and decreasing counts~%"))
              (display
               (format #f "  are without the flat numbers).~%"))
              (force-output)
              )))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Working from left-to-right if no "))
    (display
     (format #f "digit is exceeded~%"))
    (display
     (format #f "by the digit to its left it is "))
    (display
     (format #f "called an increasing~%"))
    (display
     (format #f "number; for example, 134468.~%"))
    (newline)
    (display
     (format #f "Similarly if no digit is exceeded "))
    (display
     (format #f "by the digit~%"))
    (display
     (format #f "to its right it is called a "))
    (display
     (format #f "decreasing number;~%"))
    (display
     (format #f "for example, 66420.~%"))
    (newline)
    (display
     (format #f "We shall call a positive integer that "))
    (display
     (format #f "is neither~%"))
    (display
     (format #f "increasing nor decreasing a 'bouncy' "))
    (display
     (format #f "number; for~%"))
    (display
     (format #f "example, 155349.~%"))
    (newline)
    (display
     (format #f "As n increases, the proportion of "))
    (display
     (format #f "bouncy numbers below~%"))
    (display
     (format #f "n increases such that there are only "))
    (display
     (format #f "12951 numbers~%"))
    (display
     (format #f "below one-million that are not "))
    (display
     (format #f "bouncy and only~%"))
    (display
     (format #f "277032 non-bouncy numbers "))
    (display
     (format #f "below 10^10.~%"))
    (newline)
    (display
     (format #f "How many numbers below a googol "))
    (display
     (format #f "(10^100) are not~%"))
    (display
     (format #f "bouncy?~%"))
    (newline)
    (display
     (format #f "This algorithm uses the lattice path "))
    (display
     (format #f "approach described~%"))
    (display
     (format #f "at https://en.wikipedia.org/wiki/Lattice_path~%"))
    (newline)
    (display
     (format #f "The north-east lattice path described "))
    (display
     (format #f "at the above~%"))
    (display
     (format #f "wikipedia page corresponds the number "))
    (display
     (format #f "of non-decreasing~%"))
    (display
     (format #f "3 digit numbers in base 2. By "))
    (display
     (format #f "allowing for 100~%"))
    (display
     (format #f "digits in base 10, we can easily "))
    (display
     (format #f "count the number~%"))
    (display
     (format #f "of non-decreasing numbers, using "))
    (display
     (format #f "the binomial coefficient.~%"))
    (display
     (format #f "see https://projecteuler.net/problem=113~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (reproduce-problem-statement)

    (let ((max-exponent 6)
          (debug-flag #t))
      (begin
        (sub-main-loop max-exponent debug-flag)
        ))

    (newline)
    (let ((max-exponent 10)
          (debug-flag #f))
      (begin
        (sub-main-loop max-exponent debug-flag)
        ))

    (newline)
    (let ((max-exponent 100)
          (debug-flag #f))
      (begin
        (sub-main-loop max-exponent debug-flag)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 113 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

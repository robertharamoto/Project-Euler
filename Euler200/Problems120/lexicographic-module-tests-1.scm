;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  unit tests for lexicographic-module.scm              ###
;;;###                                                       ###
;;;###  last updated July 17, 2024                           ###
;;;###                                                       ###
;;;###  created July 17, 2024                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################


;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  load modules                                         ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

(use-modules ((utils-module)
              :renamer (symbol-prefix-proc 'utils-module:)))

(use-modules ((lexicographic-module)
              :renamer (symbol-prefix-proc 'lexicographic-module:)))

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  begin test suite                                     ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-reverse-vector-k-to-n-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-reverse-vector-k-to-n-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (vector 0 1 2) 1 (vector 0 2 1))
           (list (vector 0 1 2) 0 (vector 2 1 0))
           (list (vector 0 1 2 3) 2 (vector 0 1 3 2))
           (list (vector 0 1 2 3) 1 (vector 0 3 2 1))
           (list (vector 0 1 2 3) 0 (vector 3 2 1 0))
           (list (vector 0 1 2 3 4) 3 (vector 0 1 2 4 3))
           (list (vector 0 1 2 3 4) 2 (vector 0 1 4 3 2))
           (list (vector 0 1 2 3 4) 1 (vector 0 4 3 2 1))
           (list (vector 0 1 2 3 4) 0 (vector 4 3 2 1 0))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((test-vec (list-ref this-list 0))
                  (test-ii (list-ref this-list 1))
                  (shouldbe-vec (list-ref this-list 2)))
              (let ((result-vec
                     (lexicographic-module:reverse-vector-k-to-n
                      test-vec test-ii)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "vector=~a, index=~a, "
                        test-vec test-ii))
                      (err-3
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe-vec result-vec)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-vec result-vec)
                     sub-name
                     (string-append
                      err-1 err-2 err-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-next-lexicographic-permutation-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-next-lexicographic-permutation-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (vector 0 1 2) (vector 0 2 1))
           (list (vector 0 2 1) (vector 1 0 2))
           (list (vector 1 0 2) (vector 1 2 0))
           (list (vector 1 2 0) (vector 2 0 1))
           (list (vector 2 0 1) (vector 2 1 0))
           (list (vector 0 1 2 3 4 5) (vector 0 1 2 3 5 4))
           (list (vector 0 1 2 3 5 4) (vector 0 1 2 4 3 5))
           (list (vector 0 1 2 4 3 5) (vector 0 1 2 4 5 3))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((test-vec (list-ref this-list 0))
                  (shouldbe-vec (list-ref this-list 1)))
              (let ((result-vec
                     (lexicographic-module:next-lexicographic-permutation
                      test-vec)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : vector=~a, "
                        sub-name test-label-index test-vec))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe-vec result-vec)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-vec result-vec)
                     sub-name
                     (string-append
                      err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

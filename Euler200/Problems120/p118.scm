#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 118                                    ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated July 29, 2022                                ###
;;;###                                                       ###
;;;###  updated March 11, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### srfi-1 for fold function
(use-modules ((srfi srfi-1)
              :renamer (symbol-prefix-proc 'srfi-1:)))

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### prime-module for is-prime? function
(use-modules ((prime-module)
              :renamer (symbol-prefix-proc 'prime-module:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define-syntax process-digit-macro
  (syntax-rules ()
    ((process-digit-macro
      ii-elem current-primes-list prime-array
      prime-htable
      next-d-list max-digits
      next-current-num next-current-primes-list
      next-current-digits-count
      acc-list local-inner-loop)
     (begin
       ;;; if next-current-num prime, then store it and try for another prime
       (let ((p-flag
              (hash-ref prime-htable next-current-num -1)))
         (begin
           (if (equal? p-flag -1)
               (begin
                 (let ((pr-flag
                        (prime-module:is-array-prime?
                         next-current-num prime-array)))
                   (begin
                     (hash-set! prime-htable next-current-num pr-flag)
                     (set! p-flag pr-flag)
                     ))
                 ))
           (if (equal? p-flag #t)
               (begin
                 (let ((next-acc-list
                        (local-inner-loop
                         next-d-list max-digits prime-array
                         prime-htable
                         0 next-current-digits-count
                         next-current-primes-list acc-list)))
                   (begin
                     (set! acc-list next-acc-list)
                     ))
                 ))
           ))

       ;;; any other digits can be appended to next-current-num to make a prime?
       (let ((next-acc-list
              (local-inner-loop
               next-d-list max-digits prime-array
               prime-htable
               next-current-num next-current-digits-count
               current-primes-list acc-list)))
         (begin
           (set! acc-list next-acc-list)
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (construct-prime-sets digit-list prime-array)
  (define (local-inner-loop
           d-list max-digits prime-array
           prime-htable
           current-num current-digits-count
           current-primes-list acc-list)
    (begin
      (cond
       ((and
         (or (not (list? d-list))
             (< (length d-list) 1))
         (>= current-digits-count max-digits))
        (begin
          (let ((p-flag
                 (hash-ref prime-htable current-num -1)))
            (begin
              (if (equal? p-flag -1)
                  (begin
                    (let ((pr-flag
                           (prime-module:is-array-prime?
                            current-num prime-array)))
                      (begin
                        (hash-set! prime-htable current-num pr-flag)
                        (set! p-flag pr-flag)
                        ))
                    ))
              (if (equal? p-flag #t)
                  (begin
                    (let ((s-list
                           (sort
                            (cons current-num current-primes-list)
                            <)))
                      (begin
                        (set! acc-list (cons s-list acc-list))
                        ))
                    ))
              acc-list
              ))
          ))
       (else
        (begin
          (let ((dlen (length d-list))
                (next-num (* 10 current-num))
                (next-current-digits-count
                 (1+ current-digits-count)))
            (begin
              (do ((ii 0 (1+ ii)))
                  ((>= ii dlen))
                (begin
                  (let ((ii-elem (list-ref d-list ii)))
                    (let ((next-current-num
                           (+ next-num ii-elem)))
                      (let ((next-current-primes-list
                             (cons next-current-num current-primes-list))
                            (next-d-list (delete ii-elem d-list)))
                        (begin
                          (process-digit-macro
                           ii-elem current-primes-list prime-array
                           prime-htable
                           next-d-list max-digits
                           next-current-num next-current-primes-list
                           next-current-digits-count
                           acc-list local-inner-loop)
                          ))
                      ))
                  ))
              acc-list
              ))
          )))
      ))
  (begin
    (let ((max-digits (length digit-list))
          (prime-htable (make-hash-table)))
      (begin
        (do ((ii 1 (1+ ii)))
            ((> ii 100))
          (begin
            (let ((aflag
                   (prime-module:is-array-prime? ii prime-array)))
              (begin
                (hash-set! prime-htable ii aflag)
                ))
            ))

        (let ((acc-list
               (local-inner-loop
                digit-list max-digits prime-array
                prime-htable
                0 0 (list) (list))))
          (begin
            (srfi-1:delete-duplicates acc-list)
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-construct-prime-sets-1 result-hash-table)
 (begin
   (let ((sub-name "test-construct-prime-sets-1")
         (test-list
          (list
           (list (list 1 2 3)
                 (list (list 2 13) (list 2 31)))
           (list (list 1 2 3 4)
                 (list (list 3 421) (list 3 241) (list 2 431)
                       (list 2 3 41) (list 23 41) (list 2143)
                       (list 1423) (list 2341) (list 4231)))
           (list (list 2 3 4 7)
                 (list (list 2 3 47) (list 2 347) (list 2 7 43)
                       (list 2 743) (list 23 47)
                       (list 2347) (list 2437) (list 2473)
                       (list 4273) (list 4327) (list 4723)
                       (list 7243)))
           ))
         (prime-array (prime-module:make-prime-array 200))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((input-list (list-ref alist 0))
                  (shouldbe-list-list (list-ref alist 1)))
              (let ((result-list-list
                     (construct-prime-sets input-list prime-array)))
                (let ((slen (length shouldbe-list-list))
                      (rlen (length result-list-list)))
                  (let ((err-1
                         (format
                          #f "~a : error (~a) : input-list=~a, "
                          sub-name test-label-index input-list))
                        (err-2
                         (format
                          #f "shouldbe length=~a, result length=~a"
                          slen rlen)))
                    (begin
                      (unittest2:assert?
                       (equal? slen rlen)
                       sub-name
                       (string-append err-1 err-2)
                       result-hash-table)

                      (for-each
                       (lambda (s-list)
                         (begin
                           (let ((err-3
                                  (format
                                   #f "shouldbe element=~a, result=~a"
                                   s-list result-list-list)))
                             (begin
                               (unittest2:assert?
                                (not
                                 (equal?
                                  (member s-list result-list-list)
                                  #f))
                                sub-name
                                (string-append
                                 err-1 err-3)
                                result-hash-table)
                               ))
                           )) shouldbe-list-list)
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (reproduce-problem-statement)
  (begin
    (let ((d-list (list 1 2 3))
          (prime-array
           (prime-module:make-prime-array 1000)))
      (let ((prime-list-list
             (construct-prime-sets d-list prime-array))
            (p-counter 1))
        (begin
          (display
           (format
            #f "For the digit list = ~a, the sets of primes are:~%"
            d-list))

          (for-each
           (lambda (p-list)
             (begin
               (let ((p-string
                      (string-join
                       (map
                        (lambda (a-num)
                          (begin
                            (ice-9-format:format #f "~:d" a-num)
                            )) p-list)
                       " ; ")))
                 (begin
                   (display
                    (ice-9-format:format
                     #f "    (~:d) { ~a }~%"
                     p-counter p-string))
                   (set! p-counter (1+ p-counter))
                   ))
               )) prime-list-list)
          )))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax add-to-hash-macro!
  (syntax-rules ()
    ((add-to-hash-macro! length-htable set-list)
     (begin
       (for-each
        (lambda (s-list)
          (begin
            (let ((s-len (length s-list))
                  (sort-list (sort s-list <)))
              (let ((r-list
                     (hash-ref length-htable s-len (list))))
                (begin
                  (if (equal?
                       (member sort-list r-list) #f)
                      (begin
                        (let ((next-list
                               (cons sort-list r-list)))
                          (begin
                            (hash-set!
                             length-htable s-len next-list)
                            ))
                        ))
                  )))
            )) set-list)
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop max-prime)
  (begin
    (let ((d-list (list 1 2 3 4 5 6 7 8 9))
          (prime-array
           (prime-module:make-prime-array max-prime)))
      (let ((continue-loop-flag #t))
        (let ((acc-list-list
               (construct-prime-sets d-list prime-array)))
          (let ((llen (length acc-list-list)))
            (begin
              (display
               (ice-9-format:format
                #f "there are ~:d distinct sets of primes "
                llen))
              (display
               (ice-9-format:format
                #f "that contain exactly one digit.~%"))
              (force-output)
              )))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Using all of the digits 1 through "))
    (display
     (format #f "9 and concatenating~%"))
    (display
     (format #f "them freely to form decimal integers, "))
    (display
     (format #f "different sets~%"))
    (display
     (format #f "can be formed. Interestingly with the "))
    (display
     (format #f "set {2,5,47,89,631},~%"))
    (display
     (format #f "all of the elements belonging to it "))
    (display
     (format #f "are prime.~%"))
    (newline)
    (display
     (format #f "How many distinct sets containing "))
    (display
     (format #f "each of the~%"))
    (display
     (format #f "digits one through nine exactly once "))
    (display
     (format #f "contain only~%"))
    (display
     (format #f "prime elements?~%"))
    (newline)
    (display
     (format #f "This solution can be found at:~%"))
    (display
     (format #f "https://euler.stephan-brumme.com/118/~%"))
    (display
     (format #f "see https://projecteuler.net/problem=118~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (reproduce-problem-statement)

    (newline)
    (force-output)

    (let ((max-prime 10000000))
      (begin
        (sub-main-loop max-prime)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 118 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (load "prime-module-tests-1.scm")
                 (load "digits-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

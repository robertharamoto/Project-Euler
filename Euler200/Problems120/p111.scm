#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 111                                    ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated July 29, 2022                                ###
;;;###                                                       ###
;;;###  updated March 11, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### srfi-1 for fold function
(use-modules ((srfi srfi-1)
              :renamer (symbol-prefix-proc 'srfi-1:)))

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### digits-module for digit-list-to-number function
(use-modules ((digits-module)
              :renamer (symbol-prefix-proc 'digits-module:)))

;;;### prime-module for is-prime? function
(use-modules ((prime-module)
              :renamer (symbol-prefix-proc 'prime-module:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define-syntax count-digits-macro
  (syntax-rules ()
    ((count-digits-macro
      dlist a-digit dcount)
     (begin
       (let ((ncount
              (srfi-1:fold
               (lambda (ldigit prev)
                 (begin
                   (if (= ldigit a-digit)
                       (begin
                         (+ prev 1))
                       (begin
                         prev
                         ))
                   )) 0 dlist)))
         (begin
           (set! dcount ncount)
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (make-repeat-digits-primes
         a-digit num-repeats num-digits prime-array)
  (define (inner-loop
           depth max-depth a-digit num-repeats
           prime-array current-list acc-list)
    (begin
      (cond
       ((>= depth max-depth)
        (begin
          (let ((ncount 0))
            (begin
              (count-digits-macro
               current-list a-digit ncount)

              (if (and (= (length current-list) max-depth)
                       (= ncount num-repeats))
                  (begin
                    (let ((c-number
                           (digits-module:digit-list-to-number
                            (reverse current-list))))
                      (begin
                        (if (prime-module:is-array-prime?
                             c-number prime-array)
                            (begin
                              (set! acc-list (cons c-number acc-list))
                              ))
                        ))
                    ))
              acc-list
              ))
          ))
       (else
        (begin
          (let ((start-num 0))
            (begin
              (if (<= depth 0)
                  (begin
                    (set! start-num 1)
                    ))

              (do ((ii start-num (1+ ii)))
                  ((> ii 9))
                (begin
                  (let ((next-list (cons ii current-list)))
                    (let ((ndigits 0)
                          (depth-to-go (- max-depth depth)))
                      (begin
                        (count-digits-macro
                         next-list a-digit ndigits)

                        (if (>= depth-to-go (- num-repeats ndigits))
                            (begin
                              (let ((next-acc-list
                                     (inner-loop
                                      (1+ depth) max-depth a-digit num-repeats
                                      prime-array next-list acc-list)))
                                (begin
                                  (set! acc-list next-acc-list)
                                  ))
                              ))
                        )))
                  ))
              ))
          acc-list
          )))
      ))
  (begin
    (let ((acc-list
           (inner-loop
            0 num-digits a-digit num-repeats
            prime-array (list) (list))))
      (begin
        acc-list
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-make-repeat-digits-primes-1 result-hash-table)
 (begin
   (let ((sub-name "test-make-repeat-digits-primes-1")
         (test-list
          (list
           (list 1 2 3 100
                 (list 113 191 181 151 131 911 811 311 211 101))
           (list 1 3 4 1000
                 (list 1117 1151 1171 1181 1511 1811 2111 4111 8111))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((a-digit (list-ref alist 0))
                  (num-repeats (list-ref alist 1))
                  (num-digits (list-ref alist 2))
                  (max-prime (list-ref alist 3))
                  (shouldbe-list (list-ref alist 4)))
              (let ((prime-array
                     (prime-module:make-prime-array max-prime)))
                (let ((result-list
                       (make-repeat-digits-primes
                        a-digit num-repeats num-digits
                        prime-array)))
                  (let ((slen (length shouldbe-list))
                        (rlen (length result-list)))
                    (let ((err-1
                           (format
                            #f "~a : error (~a) : "
                            sub-name test-label-index))
                          (err-2
                           (format
                            #f "a-digit=~a, num-repeats=~a, "
                            a-digit num-repeats))
                          (err-3
                           (format
                            #f "num-digits=~a, max-prime=~a, "
                            num-digits max-prime))
                          (err-4
                           (format
                            #f "shouldbe length=~a, result length=~a"
                            slen rlen)))
                      (begin
                        (unittest2:assert?
                         (equal? slen rlen)
                         sub-name
                         (string-append
                          err-1 err-2 err-3 err-4)
                         result-hash-table)

                        (for-each
                         (lambda (s-elem)
                           (begin
                             (let ((err-5
                                    (format
                                     #f "shouldbe element=~a, result=~a"
                                     s-elem result-list)))
                               (begin
                                 (unittest2:assert?
                                  (not
                                   (equal?
                                    (member s-elem result-list)
                                    #f))
                                  sub-name
                                  (string-append
                                   err-1 err-2 err-3 err-5)
                                  result-hash-table)
                                 ))
                             )) shouldbe-list)
                        ))
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; tally-htable key=(list digit num-repeats) value=(list of primes with that key property)
(define (process-tally-statistics
         num-digits tally-htable debug-flag)
  (begin
    (let ((sum 0))
      (begin
        (if (equal? debug-flag #t)
            (begin
              (display
               (ice-9-format:format
                #f "    digit d,    M(~a, d),    "
                num-digits))
              (display
               (ice-9-format:format
                #f "N(~a, d),    S(~a, d)~%"
                num-digits num-digits))
              (force-output)
              ))

        (do ((jj 0 (1+ jj)))
            ((> jj 9))
          (begin
            (let ((max-count 0))
              (begin
                (do ((kk 1 (1+ kk)))
                    ((> kk num-digits))
                  (begin
                    (let ((key-list (list jj kk)))
                      (let ((plist (hash-ref tally-htable key-list #f)))
                        (begin
                          (if (and (list? plist) (> (length plist) 0)
                                   (> kk max-count))
                              (begin
                                (set! max-count kk)
                                ))
                          )))
                    ))

                (let ((key-list (list jj max-count)))
                  (let ((plist (hash-ref tally-htable key-list #f)))
                    (begin
                      (if (list? plist)
                          (begin
                            (let ((mm max-count)
                                  (nn (length plist))
                                  (this-dsum (srfi-1:fold + 0 plist)))
                              (begin
                                (set! sum (+ sum this-dsum))
                                (if (equal? debug-flag #t)
                                    (begin
                                      (display
                                       (ice-9-format:format
                                        #f "    ~:d    ~:d    ~:d    ~:d~%"
                                        jj mm nn this-dsum))
                                      (force-output)
                                      ))
                                ))
                            ))
                      )))
                ))
            ))

        (display
         (ice-9-format:format
          #f "For d = 0 to 9, the sum of all S(~a, d) is ~:d~%"
          num-digits sum))
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (reproduce-problem-statement)
  (begin
    (let ((num-digits 4)
          (largest-num 9999)
          (max-prime 10000)
          (debug-flag #t))
      (let ((tally-htable (make-hash-table))
            (prime-array
             (prime-module:make-prime-array max-prime)))
        (begin
          ;;; group primes into tally-htable
          ;;; key=(list digit num-repeats) value=(list of primes with that key property)
          (do ((ii-digit 0 (1+ ii-digit)))
              ((> ii-digit 9))
            (begin
              (do ((ii-repeats 1 (1+ ii-repeats)))
                  ((>= ii-repeats num-digits))
                (begin
                  (let ((plist
                         (make-repeat-digits-primes
                          ii-digit ii-repeats num-digits prime-array)))
                    (begin
                      (let ((key-list (list ii-digit ii-repeats)))
                        (begin
                          (hash-set! tally-htable key-list plist)
                          ))
                      ))
                  ))
              ))

          (process-tally-statistics
           num-digits tally-htable debug-flag)
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop
         num-digits largest-num max-prime debug-flag)
  (begin
    (let ((tally-htable (make-hash-table))
          (prime-array
           (prime-module:make-prime-array max-prime))
          (last-prime-list (list))
          (last-repeats -1))
      (begin
        ;;; group primes into tally-htable
        ;;; key=(list digit num-repeats) value=(list of primes with that key property)
        (do ((ii-digit 0 (1+ ii-digit)))
            ((> ii-digit 9))
          (begin
            (let ((continue-loop-flag #t))
              (begin
                (do ((ii-repeats
                      (1- num-digits)
                      (1- ii-repeats)))
                    ((or (<= ii-repeats 1)
                         (equal? continue-loop-flag #f)))
                  (begin
                    (let ((plist
                           (make-repeat-digits-primes
                            ii-digit ii-repeats num-digits prime-array)))
                      (begin
                        (if (and (list? plist)
                                 (> (length plist) 0))
                            (begin
                              (let ((key-list
                                     (list ii-digit ii-repeats)))
                                (begin
                                  (hash-set!
                                   tally-htable key-list plist)
                                  ))

                              (set! last-prime-list plist)
                              (set! last-repeats ii-repeats)
                              (set! continue-loop-flag #f)
                              ))
                        ))
                    ))

                (display
                 (ice-9-format:format
                  #f "completed tally for digit = ~:d, "
                  ii-digit))
                (display
                 (ice-9-format:format
                  #f "ii-repeats = ~:d, prime-list = ~a~%"
                  last-repeats last-prime-list))
                (force-output)
                ))
            ))

        (process-tally-statistics
         num-digits tally-htable debug-flag)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Considering 4-digit primes containing "))
    (display
     (format #f "repeated digits it~%"))
    (display
     (format #f "is clear that they cannot all be "))
    (display
     (format #f "the same: 1111~%"))
    (display
     (format #f "is divisible by 11, 2222 is divisible "))
    (display
     (format #f "by 22, and so~%"))
    (display
     (format #f "on. But there are nine 4-digit primes "))
    (display
     (format #f "containing three ones:~%"))
    (newline)
    (display
     (format #f "    1117, 1151, 1171, 1181, 1511, "))
    (display
     (format #f "1811, 2111, 4111, 8111~%"))
    (newline)
    (display
     (format #f "We shall say that M(n, d) represents "))
    (display
     (format #f "the maximum number~%"))
    (display
     (format #f "of repeated digits for an n-digit "))
    (display
     (format #f "prime where d is~%"))
    (display
     (format #f "the repeated digit, N(n, d) "))
    (display
     (format #f "represents the number~%"))
    (display
     (format #f "of such primes, and S(n, d) "))
    (display
     (format #f "represents the sum~%"))
    (display
     (format #f "of these primes.~%"))
    (newline)
    (display
     (format #f "So M(4, 1) = 3 is the maximum number "))
    (display
     (format #f "of repeated digits~%"))
    (display
     (format #f "for a 4-digit prime where one is "))
    (display
     (format #f "the repeated digit,~%"))
    (display
     (format #f "there are N(4, 1) = 9 such primes, "))
    (display
     (format #f "and the sum of~%"))
    (display
     (format #f "these primes is S(4, 1) = 22275. It "))
    (display
     (format #f "turns out that~%"))
    (display
     (format #f "for d = 0, it is only possible to "))
    (display
     (format #f "have M(4, 0) = 2~%"))
    (display
     (format #f "repeated digits, but there are "))
    (display
     (format #f "N(4, 0) = 13 such~%"))
    (display
     (format #f "cases.~%"))
    (newline)
    (display
     (format #f "In the same way we obtain the "))
    (display
     (format #f "following results~%"))
    (display
     (format #f "for 4-digit primes.~%"))
    (newline)
    (display (format #f "Digit, d       M(4, d) N(4, d) S(4, d)~%"))
    (display (format #f "0      2       13      67061~%"))
    (display (format #f "1      3       9       22275~%"))
    (display (format #f "2      3       1       2221~%"))
    (display (format #f "3      3       12      46214~%"))
    (display (format #f "4      3       2       8888~%"))
    (display (format #f "5      3       1       5557~%"))
    (display (format #f "6      3       1       6661~%"))
    (display (format #f "7      3       9       57863~%"))
    (display (format #f "8      3       1       8887~%"))
    (display (format #f "9      3       7       48073~%"))
    (newline)
    (display
     (format #f "For d = 0 to 9, the sum of all "))
    (display
     (format #f "S(4, d) is 273700.~%"))
    (newline)
    (display
     (format #f "Find the sum of all S(10, d).~%"))
    (display
     (format #f "see https://projecteuler.net/problem=111~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (reproduce-problem-statement)

    (newline)
    (let ((ndigits 10)
          (largest-num 9999999999)
          (max-prime 1000000)
          (debug-flag #t))
      (begin
        (sub-main-loop ndigits largest-num max-prime debug-flag)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 111 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (load "prime-module-tests-1.scm")
                 (load "digits-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

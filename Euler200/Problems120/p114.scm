#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 114                                    ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated July 29, 2022                                ###
;;;###                                                       ###
;;;###  updated March 11, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### srfi-1 for fold function
(use-modules ((srfi srfi-1)
              :renamer (symbol-prefix-proc 'srfi-1:)))

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (count-rb-fixed-length max-array-length min-red-len)
  (define (local-count-loop gap-len min-red-len gap-htable)
    (begin
      (let ((hcount (hash-ref gap-htable gap-len #f)))
        (begin
          (cond
           ((< gap-len min-red-len)
            (begin
              0
              ))
           ((= gap-len min-red-len)
            (begin
              1
              ))
           ((not (equal? hcount #f))
            (begin
              hcount
              ))
           (else
            (begin
              (let ((count 0))
                (begin
                  (do ((ii min-red-len (1+ ii)))
                      ((> ii gap-len))
                    (begin
                      (let ((this-count (1+ (- gap-len ii))))
                        (let ((remaining-gap (- gap-len ii 1))
                              (sub-count 0))
                          (begin
                            (do ((jj 0 (1+ jj)))
                                ((> jj remaining-gap))
                              (begin
                                (let ((next-gap (- remaining-gap jj)))
                                  (let ((next-count
                                         (local-count-loop
                                          next-gap min-red-len
                                          gap-htable)))
                                    (begin
                                      (set!
                                       sub-count (+ sub-count next-count))
                                      )))
                                ))

                            (set!
                             count
                             (+ count this-count sub-count))
                            )))
                      ))

                  (hash-set! gap-htable gap-len count)
                  count
                  ))
              )))
          ))
      ))
  (begin
    (let ((gap-htable (make-hash-table)))
      (let ((count
             (local-count-loop max-array-length min-red-len gap-htable)))
        (begin
          ;;; add 1 for the case where there are no red-tiles
          (1+ count)
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-count-rb-fixed-length-1 result-hash-table)
 (begin
   (let ((sub-name "test-count-rb-fixed-length-1")
         (test-list
          (list
           (list 3 3 2) (list 4 3 4) (list 5 3 7)
           (list 7 3 17)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((a-len (list-ref this-list 0))
                  (min-len (list-ref this-list 1))
                  (shouldbe (list-ref this-list 2)))
              (let ((result
                     (count-rb-fixed-length a-len min-len)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "a-len=~a, min-len=~a, "
                        a-len min-len))
                      (err-3
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append
                      err-1 err-2 err-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop max-length min-red-length)
  (begin
    (let ((count
           (count-rb-fixed-length
            max-length min-red-length)))
      (begin
        (display
         (ice-9-format:format
          #f "There are exactly ~:d ways to arrange red and~%"
          count))
        (display
         (ice-9-format:format
          #f "black blocks (no adjacent red-blocks), within~%"))
        (display
         (ice-9-format:format
          #f "an array of length ~:d~%"
          max-length))
        (newline)
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "A row measuring seven units in "))
    (display
     (format #f "length has red~%"))
    (display
     (format #f "blocks with a minimum length "))
    (display
     (format #f "of three units~%"))
    (display
     (format #f "placed on it, such that any two "))
    (display
     (format #f "red blocks (which~%"))
    (display
     (format #f "are allowed to be different "))
    (display
     (format #f "lengths) are separated~%"))
    (display
     (format #f "by at least one black square. "))
    (display
     (format #f "There are exactly~%"))
    (display
     (format #f "seventeen ways of doing this.~%"))
    (newline)
    (display
     (format #f "How many ways can a row measuring "))
    (display
     (format #f "fifty units in~%"))
    (display
     (format #f "length be filled?~%"))
    (newline)
    (display
     (format #f "NOTE: Although the example above "))
    (display
     (format #f "does not lend~%"))
    (display
     (format #f "itself to the possibility, in "))
    (display
     (format #f "general it is~%"))
    (display
     (format #f "permitted to mix block sizes. For "))
    (display
     (format #f "example, on a row~%"))
    (display
     (format #f "measuring eight units in length "))
    (display
     (format #f "you could use~%"))
    (display
     (format #f "red (3), black (1), and "))
    (display
     (format #f "red (4).~%"))
    (newline)
    (display
     (format #f "The solution was found at~%"))
    (display
     (format #f "https://keyzero.wordpress.com/2010/05/20/project-euler-problem-114/~%"))
    (display
     (format #f "see https://projecteuler.net/problem=114~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((max-length 7)
          (red-min-length 3))
      (begin
        (sub-main-loop max-length red-min-length)
        ))

    (newline)
    (let ((max-length 50)
          (red-min-length 3))
      (begin
        (sub-main-loop max-length red-min-length)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 114 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

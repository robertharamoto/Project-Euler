#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 115                                    ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated July 29, 2022                                ###
;;;###                                                       ###
;;;###  updated March 11, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (count-rb-fixed-length max-array-length min-red-len)
  (define (local-count-loop gap-len min-red-len gap-htable)
    (begin
      (let ((hcount
             (hash-ref gap-htable gap-len #f)))
        (begin
          (cond
           ((< gap-len min-red-len)
            (begin
              0
              ))
           ((= gap-len min-red-len)
            (begin
              1
              ))
           ((not (equal? hcount #f))
            (begin
              hcount
              ))
           (else
            (begin
              (let ((count 0))
                (begin
                  (do ((ii min-red-len (1+ ii)))
                      ((> ii gap-len))
                    (begin
                      (let ((this-count
                             (1+ (- gap-len ii))))
                        (let ((remaining-gap
                               (- gap-len ii 1))
                              (sub-count 0))
                          (begin
                            (do ((jj 0 (1+ jj)))
                                ((> jj remaining-gap))
                              (begin
                                (let ((next-gap
                                       (- remaining-gap jj)))
                                  (let ((next-count
                                         (local-count-loop
                                          next-gap min-red-len
                                          gap-htable)))
                                    (begin
                                      (set!
                                       sub-count (+ sub-count next-count))
                                      )))
                                ))

                            (set!
                             count
                             (+ count this-count sub-count))
                            )))
                      ))

                  (hash-set! gap-htable gap-len count)
                  count
                  ))
              )))
          ))
      ))
  (begin
    (let ((gap-htable (make-hash-table)))
      (let ((count
             (local-count-loop max-array-length min-red-len gap-htable)))
        (begin
          ;;; add 1 for the case where there are no red-tiles
          (1+ count)
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-count-rb-fixed-length-1 result-hash-table)
 (begin
   (let ((sub-name "test-count-rb-fixed-length-1")
         (test-list
          (list
           (list 3 3 2) (list 4 3 4) (list 5 3 7)
           (list 7 3 17)
           (list 29 3 673135) (list 30 3 1089155)
           (list 56 10 880711) (list 57 10 1148904)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((a-len (list-ref this-list 0))
                  (min-len (list-ref this-list 1))
                  (shouldbe (list-ref this-list 2)))
              (let ((result (count-rb-fixed-length a-len min-len)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "a-len=~a, min-len=~a, "
                        a-len min-len))
                      (err-3
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append
                      err-1 err-2 err-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define-syntax display-output-results-macro
  (syntax-rules ()
    ((display-output-results-macro
      min-1 max-1 count-1
      min-2 max-2 count-2
      threshold)
     (begin
       (display
        (ice-9-format:format
         #f "F(~:d, ~:d) = ~:d : F(~:d, ~:d) = ~:d~%"
         min-1 max-1 count-1
         min-2 max-2 count-2
         ))
       (display
        (ice-9-format:format
         #f "  n = ~:d is the least value for which~%"
         max-2))
       (display
        (ice-9-format:format
         #f "  the fill-count function first exceeds ~:d~%"
         threshold))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (reproduce-problem-statement)
  (begin
    (let ((a-list-list
           (list (list (list 29 3) (list 30 3) 1000000)
                 (list (list 56 10) (list 57 10) 1000000))))
      (begin
        (for-each
         (lambda (a-pair-list)
           (begin
             (let ((pair-1 (list-ref a-pair-list 0))
                   (pair-2 (list-ref a-pair-list 1))
                   (threshold (list-ref a-pair-list 2)))
               (let ((max-1 (list-ref pair-1 0))
                     (min-1 (list-ref pair-1 1))
                     (max-2 (list-ref pair-2 0))
                     (min-2 (list-ref pair-2 1)))
                 (let ((count-1 (count-rb-fixed-length max-1 min-1))
                       (count-2 (count-rb-fixed-length max-2 min-2)))
                   (begin
                     (display-output-results-macro
                      min-1 max-1 count-1
                      min-2 max-2 count-2
                      threshold)
                     (force-output)
                     ))
                 ))
             )) a-list-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop min-length max-length threshold)
  (begin
    (let ((prev-nn -1)
          (prev-count -1)
          (first-nn -1)
          (first-count -1)
          (continue-loop-flag #t))
      (begin
        (do ((ii min-length (1+ ii)))
            ((or (> ii max-length)
                 (equal? continue-loop-flag #f)))
          (begin
            (let ((curr-count
                   (count-rb-fixed-length ii min-length)))
              (begin
                (if (>= curr-count threshold)
                    (begin
                      (set! continue-loop-flag #f)
                      (set! first-nn ii)
                      (set! first-count curr-count))
                    (begin
                      (set! prev-nn ii)
                      (set! prev-count curr-count)
                      ))
                ))
            ))

        (if (equal? continue-loop-flag #f)
            (begin
              (display-output-results-macro
               min-length prev-nn prev-count
               min-length first-nn first-count
               threshold)
              (force-output))
            (begin
              (display
               (ice-9-format:format
                #f "no results found : F(~:d, ~:d) = ~:d~%"
                min-length prev-nn prev-count))
              (display
               (ice-9-format:format
                #f "  n = ~:d is the largest value found "
                prev-nn))
              (display
               (ice-9-format:format
                #f "so far, with n <= ~:d~%"
                max-length))
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "NOTE: This is a more difficult "))
    (display
     (format #f "version of problem 114.~%"))
    (newline)
    (display
     (format #f "A row measuring n units in length "))
    (display
     (format #f "has red blocks~%"))
    (display
     (format #f "with a minimum length of m units "))
    (display
     (format #f "placed on it,~%"))
    (display
     (format #f "such that any two red blocks "))
    (display
     (format #f "(which are allowed~%"))
    (display
     (format #f "to be different lengths) are "))
    (display
     (format #f "separated by at~%"))
    (display
     (format #f "least one black square.~%"))
    (newline)
    (display
     (format #f "Let the fill-count function, "))
    (display
     (format #f "F(m, n), represent~%"))
    (display
     (format #f "the number of ways that a row "))
    (display
     (format #f "can be filled.~%"))
    (newline)
    (display
     (format #f "For example, F(3, 29) = 673135 "))
    (display
     (format #f "and F(3, 30)~%"))
    (display
     (format #f "= 1089155.~%"))
    (newline)
    (display
     (format #f "That is, for m = 3, it can be seen "))
    (display
     (format #f "that n = 30 is~%"))
    (display
     (format #f "the smallest value for which the "))
    (display
     (format #f "fill-count function~%"))
    (display
     (format #f "first exceeds one million.~%"))
    (newline)
    (display
     (format #f "In the same way, for m = 10, it "))
    (display
     (format #f "can be verified~%"))
    (display
     (format #f "that F(10, 56) = 880711 and "))
    (display
     (format #f "F(10, 57) = 1148904,~%"))
    (display
     (format #f "so n = 57 is the least value for "))
    (display
     (format #f "which the fill-count~%"))
    (display
     (format #f "function first exceeds one million.~%"))
    (newline)
    (display
     (format #f "For m = 50, find the least value "))
    (display
     (format #f "of n for which~%"))
    (display
     (format #f "the fill-count~%"))
    (display
     (format #f "function first exceeds one "))
    (display
     (format #f "million.~%"))
    (display
     (format #f "see https://projecteuler.net/problem=115~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (reproduce-problem-statement)

    (newline)
    (let ((min-length 50)
          (max-length 1000)
          (threshold 1000000))
      (begin
        (sub-main-loop min-length max-length threshold)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 115 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

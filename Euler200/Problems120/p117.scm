#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 117                                    ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated July 29, 2022                                ###
;;;###                                                       ###
;;;###  updated March 11, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (count-rb-fixed-length max-array-length)
  (define (local-count-loop gap-len gap-htable)
    (begin
      (let ((hcount (hash-ref gap-htable gap-len #f))
            (min-color 2)
            (max-color 4))
        (begin
          (cond
           ((< gap-len min-color)
            (begin
              0
              ))
           ((= gap-len min-color)
            (begin
              1
              ))
           ((not (equal? hcount #f))
            (begin
              hcount
              ))
           (else
            (begin
              (let ((count 0))
                (begin
                  (do ((ii min-color (1+ ii)))
                      ((> ii max-color))
                    (begin
                      (let ((ii-count (1+ (- gap-len ii)))
                            (remaining-gap (- gap-len ii))
                            (sub-count 0))
                        (begin
                          (do ((jj 0 (1+ jj)))
                              ((> jj remaining-gap))
                            (begin
                              (let ((next-gap (- remaining-gap jj)))
                                (let ((next-count
                                       (local-count-loop
                                        next-gap gap-htable)))
                                  (begin
                                    (set!
                                     sub-count (+ sub-count next-count))
                                    )))
                              ))

                          (set! count (+ count ii-count sub-count))
                          ))
                      ))

                  (hash-set! gap-htable gap-len count)
                  count
                  ))
              )))
          ))
      ))
  (begin
    (let ((gap-htable (make-hash-table)))
      (let ((count
             (local-count-loop max-array-length gap-htable)))
        (begin
          ;;; add 1 for the case with no color tiles
          (1+ count)
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-count-rb-fixed-length-1 result-hash-table)
 (begin
   (let ((sub-name "test-count-rb-fixed-length-1")
         (test-list
          (list
           (list 1 1)
           (list 2 2)
           (list 3 4)
           (list 4 8)
           (list 5 15)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((a-len (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result (count-rb-fixed-length a-len)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : a-len=~a, "
                        sub-name test-label-index a-len))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop max-length)
  (begin
    (let ((count
           (count-rb-fixed-length max-length)))
      (begin
        (display
         (ice-9-format:format
          #f "there are ~:d ways of tiling a row "
          count))
        (display
         (ice-9-format:format
          #f "measuring ~:d units in length.~%"
          max-length))
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Using a combination of black "))
    (display
     (format #f "square tiles and~%"))
    (display
     (format #f "oblong tiles chosen from: red "))
    (display
     (format #f "tiles measuring~%"))
    (display
     (format #f "two units, green tiles measuring "))
    (display
     (format #f "three units, and~%"))
    (display
     (format #f "blue tiles measuring four units, it "))
    (display
     (format #f "is possible to~%"))
    (display
     (format #f "tile a row measuring five units in "))
    (display
     (format #f "length in exactly~%"))
    (display
     (format #f "fifteen different ways.~%"))
    (newline)
    (display
     (format #f "How many ways can a row measuring "))
    (display
     (format #f "fifty units in~%"))
    (display
     (format #f "length be tiled?~%"))
    (newline)
    (display
     (format #f "NOTE: This is related to problem 116.~%"))
    (display
     (format #f "see https://projecteuler.net/problem=117~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((max-length 5))
      (begin
        (sub-main-loop max-length)
        ))

    (newline)
    (let ((max-length 50))
      (begin
        (sub-main-loop max-length)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 117 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

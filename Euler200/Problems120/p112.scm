#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 112                                    ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated July 29, 2022                                ###
;;;###                                                       ###
;;;###  updated March 11, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### digits-module for split-digits-list function
(use-modules ((digits-module)
              :renamer (symbol-prefix-proc 'digits-module:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (factorial ii-num)
  (begin
    (cond
     ((<= ii-num 1)
      (begin
        1
        ))
     ((= ii-num 2)
      (begin
        2
        ))
     ((= ii-num 3)
      (begin
        6
        ))
     ((= ii-num 4)
      (begin
        24
        ))
     ((= ii-num 5)
      (begin
        120
        ))
     ((= ii-num 6)
      (begin
        720
        ))
     ((= ii-num 7)
      (begin
        5040
        ))
     ((= ii-num 8)
      (begin
        40320
        ))
     ((= ii-num 9)
      (begin
        362880
        ))
     (else
      (begin
        (* ii-num (factorial (- ii-num 1)))
        )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-factorial-1 result-hash-table)
 (begin
   (let ((sub-name "test-factorial-1")
         (test-list
          (list
           (list 0 1) (list 1 1) (list 2 2)
           (list 3 6) (list 4 24) (list 5 120)
           (list 6 720) (list 7 5040)
           (list 8 40320) (list 9 362880)
           (list 10 3628800)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-num (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result (factorial test-num)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : test-num=~a, "
                        sub-name test-label-index test-num))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (choose kk nn)
  (define (local-partial-factorial larger-num smaller-num)
    (begin
      (let ((result 1))
        (begin
          (do ((ii larger-num (- ii 1)))
              ((<= ii smaller-num))
            (begin
              (set! result (* result ii))
              ))
          result
          ))
      ))
  (begin
    (cond
     ((<= kk 0)
      (begin
        0
        ))
     ((<= nn 0)
      (begin
        1
        ))
     ((= kk nn)
      (begin
        1
        ))
     ((>= kk nn)
      (begin
        (let ((kfact
               (local-partial-factorial kk nn))
              (nfact
               (factorial (- kk nn))))
          (let ((result (/ kfact nfact)))
            (begin
              result
              )))
        ))
     (else
      (begin
      ;;; (< kk nn)
        (let ((kfact (factorial (- nn kk)))
              (nfact (local-partial-factorial nn kk)))
          (let ((result (/ nfact kfact)))
            (begin
              result
              )))
        )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-choose-1 result-hash-table)
 (begin
   (let ((sub-name "test-choose-1")
         (test-list
          (list
           (list 0 0 0) (list 1 0 1) (list 0 1 0)
           (list 2 0 1) (list 2 1 2) (list 2 2 1)
           (list 3 0 1) (list 3 1 3) (list 3 2 3) (list 3 3 1)
           (list 4 0 1) (list 4 1 4) (list 4 2 6) (list 4 3 4)
           (list 4 4 1)
           (list 5 0 1) (list 5 1 5) (list 5 2 10) (list 5 3 10)
           (list 5 4 5) (list 5 5 1)
           (list 6 0 1) (list 6 1 6) (list 6 2 15) (list 6 3 20)
           (list 6 4 15) (list 6 5 6) (list 6 6 1)
           (list 0 6 0) (list 1 6 6) (list 2 6 15) (list 3 6 20)
           (list 4 6 15) (list 5 6 6) (list 6 6 1)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-kk (list-ref alist 0))
                  (test-nn (list-ref alist 1))
                  (shouldbe-num (list-ref alist 2)))
              (let ((result-num (choose test-kk test-nn)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "test-kk=~a, test-nn=~a, "
                        test-kk test-nn))
                      (err-3
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe-num result-num)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-num result-num)
                     sub-name
                     (string-append
                      err-1 err-2 err-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (is-bouncy? this-num)
  (begin
    (cond
     ((<= this-num 99)
      (begin
        #f
        ))
     (else
      (begin
        (let ((dlist
               (digits-module:split-digits-list this-num)))
          (let ((dlen (length dlist))
                (up-flag #f)
                (down-flag #f)
                (last-digit (car dlist)))
            (begin
              (do ((ii 1 (1+ ii)))
                  ((>= ii dlen))
                (begin
                  (let ((this-digit (list-ref dlist ii)))
                    (begin
                      (cond
                       ((> last-digit this-digit)
                        (begin
                          (set! down-flag #t)
                          ))
                       ((< last-digit this-digit)
                        (begin
                          (set! up-flag #t)
                          )))
                      (set! last-digit this-digit)
                      ))
                  ))
              (if (and (equal? up-flag #t)
                       (equal? down-flag #t))
                  (begin
                    #t)
                  (begin
                    #f
                    ))
              )))
        )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-is-bouncy-1 result-hash-table)
 (begin
   (let ((sub-name "test-is-bouncy-1")
         (test-list
          (list
           (list 3 #f) (list 99 #f) (list 101 #t) (list 102 #t)
           (list 110 #f) (list 123 #f) (list 523 #t)
           (list 155349 #t)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-num (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result (is-bouncy? test-num)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : test-num=~a, "
                        sub-name test-label-index test-num))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (find-least-number
         max-num target-pcnt
         previous-min previous-bouncy)
  (begin
    (let ((bcounter (max 0 previous-bouncy))
          (total (max 100 previous-min))
          (start-num (1+ (max 100 previous-min)))
          (min-number -1)
          (continue-loop-flag #t))
      (begin
        (do ((ii start-num (1+ ii)))
            ((or (> ii max-num)
                 (equal? continue-loop-flag #f)))
          (begin
            (set! total (1+ total))

            (if (is-bouncy? ii)
                (begin
                  (set! bcounter (1+ bcounter))

                  (let ((this-pcnt (/ bcounter total)))
                    (begin
                      (if (>= this-pcnt target-pcnt)
                          (begin
                            (set! min-number ii)
                            (set! continue-loop-flag #f)
                            ))
                      ))
                  ))
            ))

        (list min-number bcounter)
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-find-least-number-1 result-hash-table)
 (begin
   (let ((sub-name "test-find-least-number-1")
         (test-list
          (list
           (list 2000 (/ 1 2) -1 -1 (list 538 269))
           (list 2000 (/ 1 2) 100 0 (list 538 269))
           (list 50000 (/ 9 10) 538 269 (list 21780 19602))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((max-num (list-ref alist 0))
                  (target-pcnt (list-ref alist 1))
                  (previous-min (list-ref alist 2))
                  (previous-bouncy (list-ref alist 3))
                  (shouldbe (list-ref alist 4)))
              (let ((result
                     (find-least-number
                      max-num target-pcnt
                      previous-min previous-bouncy)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "max-num=~a, target-pcnt=~a, "
                        max-num target-pcnt))
                      (err-3
                       (format
                        #f "previous-min=~a, previous-bouncy=~a, "
                        previous-min previous-bouncy))
                      (err-4
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append
                      err-1 err-2 err-3 err-4)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; lattice path algorithm assumes going from (0,0) to (a,b)
;;; to map to the problem, need to reduce 1 - 9 digits to 0 - 8
(define (count-increasing-numbers num-digits)
  (begin
    (cond
     ((<= num-digits 1)
      (begin
        #f
        ))
     (else
      (begin
        (let ((nn (+ num-digits 8)))
          (let ((ncr (choose nn 8)))
            (let ((count (- ncr 9)))
              (begin
                ;;; removed the 9 flat numbers, 11111, 22222,..., 99999
                count
                ))
            ))
        )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-count-increasing-numbers-1 result-hash-table)
 (begin
   (let ((sub-name "test-count-increasing-numbers-1")
         (test-list
          (list
           (list 2 36) (list 3 156)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((num-digits (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result
                     (count-increasing-numbers num-digits)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : num-digits=~a, "
                        sub-name test-label-index num-digits))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; lattice path algorithm assumes going from (0,0) to (a,b)
(define (count-decreasing-numbers num-digits)
  (begin
    (cond
     ((<= num-digits 1)
      (begin
        #f
        ))
     (else
      (begin
        (let ((nn (+ num-digits 9)))
          (let ((ncr (choose nn 9)))
            (let ((count (- ncr 10)))
              (begin
                ;;; removed the 10 flat numbers, 00000, 11111, 22222,..., 99999
                count
                ))
            ))
        )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-count-decreasing-numbers-1 result-hash-table)
 (begin
   (let ((sub-name "test-count-decreasing-numbers-1")
         (test-list
          (list
           (list 2 45) (list 3 210)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((num-digits (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result
                     (count-decreasing-numbers num-digits)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : num-digits=~a, "
                        sub-name test-label-index num-digits))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (count-bouncy-ndigit-numbers ndigit-numbers)
  (begin
    (let ((flat-count 9)
          (total-count
           (* 9 (inexact->exact
                 (expt 10 (1- ndigit-numbers)))))
          (increasing-count
           (count-increasing-numbers ndigit-numbers))
          (decreasing-count
           (count-decreasing-numbers ndigit-numbers)))
      (let ((bouncy-count
             (- total-count
                (+ increasing-count decreasing-count flat-count))))
        (begin
          bouncy-count
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-count-bouncy-ndigit-numbers-1 result-hash-table)
 (begin
   (let ((sub-name "test-count-bouncy-ndigit-numbers-1")
         (test-list
          (list
           (list 2 0)
           (list 3 525)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((num-digits (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result
                     (count-bouncy-ndigit-numbers num-digits)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : num-digits=~a, "
                        sub-name test-label-index num-digits))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; get a rough estimate, increment by factors of 10 (100, 1000, 10000,...)
(define (find-approximate-least-number target-pcnt max-digits)
  (begin
    (let ((result-list (list))
          (bcount 0)
          (last-bcount 0)
          (last-total 100)
          (total 100)
          (start-num 2)
          (continue-loop-flag #t))
      (begin
        (do ((ii start-num (1+ ii)))
            ((or (> ii max-digits)
                 (equal? continue-loop-flag #f)))
          (begin
            (let ((ii-bouncy-count
                   (count-bouncy-ndigit-numbers ii)))
              (begin
                (set! bcount (+ bcount ii-bouncy-count))
                (let ((exact-pcnt (/ bcount total)))
                  (begin
                    (if (>= exact-pcnt target-pcnt)
                        (begin
                          (set! continue-loop-flag #f))
                        (begin
                          (set! last-bcount bcount)
                          (set! last-total total)
                          (set! total (* total 10))
                          ))
                    ))
                ))
            ))

        (list last-bcount last-total)
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-find-approximate-least-number-1 result-hash-table)
 (begin
   (let ((sub-name "test-find-approximate-least-number-1")
         (test-list
          (list
           (list 0 5 (list 0 100))
           (list (/ 1 2) 5 (list 0 100))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((target-pcnt (list-ref alist 0))
                  (max-digits (list-ref alist 1))
                  (shouldbe (list-ref alist 2)))
              (let ((result
                     (find-approximate-least-number
                      target-pcnt max-digits)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "target-pcnt=~a, max-digits=~a, "
                        target-pcnt max-digits))
                      (err-3
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append
                      err-1 err-2 err-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop target-pcnt max-digits max-num)
  (begin
    (let ((rr-list
           (find-approximate-least-number
            target-pcnt max-digits)))
      (let ((last-bcount (list-ref rr-list 0))
            (last-total (list-ref rr-list 1)))
        (let ((rlist
               (find-least-number
                max-num target-pcnt last-total last-bcount)))
          (let ((least-num (list-ref rlist 0))
                (bouncy-count (list-ref rlist 1)))
            (begin
              (display
               (ice-9-format:format
                #f "~:d = the least number for which the "
                least-num))
              (display
               (ice-9-format:format
                #f "proportion of bouncy numbers~%"
                least-num))
              (display
               (ice-9-format:format
                #f "first reaches ~a, with ~:d bouncy numbers.~%"
                target-pcnt bouncy-count))
              (force-output)
              ))
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Working from left-to-right if no "))
    (display
     (format #f "digit is exceeded~%"))
    (display
     (format #f "by the digit to its left it is "))
    (display
     (format #f "called an~%"))
    (display
     (format #f "increasing number; for example, "))
    (display
     (format #f "134468.~%"))
    (newline)
    (display
     (format #f "Similarly if no digit is exceeded "))
    (display
     (format #f "by the digit~%"))
    (display
     (format #f "to its right it is called a "))
    (display
     (format #f "decreasing number; for~%"))
    (display
     (format #f "example, 66420.~%"))
    (newline)
    (display
     (format #f "We shall call a positive integer "))
    (display
     (format #f "that is neither~%"))
    (display
     (format #f "increasing nor decreasing a 'bouncy' "))
    (display
     (format #f "number; for~%"))
    (display
     (format #f "example, 155349.~%"))
    (newline)
    (display
     (format #f "Clearly there cannot be any bouncy "))
    (display
     (format #f "numbers below~%"))
    (display
     (format #f "one-hundred but just over half of "))
    (display
     (format #f "the numbers below~%"))
    (display
     (format #f "one-thousand (525) are bouncy. In fact, "))
    (display
     (format #f "the least number~%"))
    (display
     (format #f "for which the proportion of bouncy "))
    (display
     (format #f "numbers first~%"))
    (display
     (format #f "reaches 50% is 538.~%"))
    (newline)
    (display
     (format #f "Surprisingly, bouncy numbers "))
    (display
     (format #f "become more and~%"))
    (display
     (format #f "more common and by the time we "))
    (display
     (format #f "reach 21780 the~%"))
    (display
     (format #f "proportion of bouncy numbers is "))
    (display
     (format #f "equal to 90%.~%"))
    (newline)
    (display
     (format #f "Find the least number for which the "))
    (display
     (format #f "proportion of~%"))
    (display
     (format #f "bouncy numbers is exactly 99%.~%"))
    (newline)
    (display
     (format #f "The fast calculation of bouncy numbers "))
    (display
     (format #f "uses the same~%"))
    (display
     (format #f "method from problem 113, a lattice path "))
    (display
     (format #f "calculation~%"))
    (display
     (format #f "method for increasing and decreasing "))
    (display
     (format #f "numbers. A brute-force~%"))
    (display
     (format #f "method is used once we get into the "))
    (display
     (format #f "neighborhood.~%"))
    (display
     (format #f "see https://projecteuler.net/problem=112~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((target-pcnt (/ 1 2))
          (max-num 10000)
          (max-digits 3))
      (begin
        (sub-main-loop target-pcnt max-digits max-num)
        ))

    (newline)
    (let ((target-pcnt (/ 9 10))
          (max-digits 20)
          (max-num 100000))
      (begin
        (sub-main-loop target-pcnt max-digits max-num)
        ))

    (newline)
    (let ((target-pcnt (/ 99 100))
          (max-digits 100)
          (max-num 100000000))
      (begin
        (sub-main-loop target-pcnt max-digits max-num)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 112 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (load "digits-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

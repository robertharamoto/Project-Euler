#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 120                                    ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated July 29, 2022                                ###
;;;###                                                       ###
;;;###  updated March 11, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (find-rmax aa-num max-nn)
  (begin
    (let ((rmax (* 2 aa-num))
          (rexp 1)
          (aa-sqr (* aa-num aa-num)))
      (let ((two-aa (modulo (* 2 aa-num) aa-sqr)))
        (begin
          (do ((ii 2 (1+ ii)))
              ((> ii max-nn))
            (begin
              (cond
               ((even? ii)
                (begin
                  (if (< rmax two-aa)
                      (begin
                        (set! rmax two-aa)
                        (set! rexp ii)
                        ))
                  ))
               (else
                (begin
                  (let ((remain
                         (modulo (* 2 aa-num ii) aa-sqr)))
                    (begin
                      (if (< rmax remain)
                          (begin
                            (set! rmax remain)
                            (set! rexp ii)
                            ))
                      ))
                  )))
              ))
          (list rmax rexp)
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-find-rmax-1 result-hash-table)
 (begin
   (let ((sub-name "test-find-rmax-1")
         (test-list
          (list
           (list 3 10 (list 6 1))
           (list 7 10 (list 42 3))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((aa-num (list-ref alist 0))
                  (max-nn (list-ref alist 1))
                  (shouldbe (list-ref alist 2)))
              (let ((result (find-rmax aa-num max-nn)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "aa-num=~a, max-nn=~a, "
                        aa-num max-nn))
                      (err-3
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append
                      err-1 err-2 err-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (integer-power this-number this-exponent)
  (begin
    (cond
     ((= this-exponent 0)
      (begin
        1
        ))
     ((= this-exponent 1)
      (begin
        this-number
        ))
     ((< this-exponent 0)
      (begin
        -1
        ))
     (else
      (begin
        (let ((result-num this-number)
              (max-iter (- this-exponent 1)))
          (begin
            (do ((ii 0 (+ ii 1)))
                ((>= ii max-iter))
              (begin
                (set! result-num (* result-num this-number))
                ))
            result-num
            ))
        )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-integer-power-1 result-hash-table)
 (begin
   (let ((sub-name "test-integer-power-1")
         (test-list
          (list
           (list 10 0 1) (list 11 0 1) (list 12 0 1)
           (list 10 1 10) (list 11 1 11) (list 12 1 12)
           (list 10 2 100) (list 11 2 121) (list 12 2 144)
           (list 2 2 4) (list 2 3 8) (list 2 4 16) (list 2 5 32)
           (list 2 6 64) (list 2 7 128) (list 2 8 256) (list 2 9 512)
           (list 2 10 1024)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-num (list-ref alist 0))
                  (test-exp (list-ref alist 1))
                  (shouldbe-num (list-ref alist 2)))
              (let ((result-num
                     (integer-power test-num test-exp)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "test-num=~a, test-exp=~a, "
                        test-num test-exp))
                      (err-3
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe-num result-num)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-num result-num)
                     sub-name
                     (string-append
                      err-1 err-2 err-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop
         start-num end-num max-exponent debug-flag)
  (begin
    (let ((sum-rmax 0))
      (begin
        (do ((ii start-num (1+ ii)))
            ((> ii end-num))
          (begin
            (let ((result-list
                   (find-rmax ii max-exponent)))
              (let ((rmax (list-ref result-list 0))
                    (rexp (list-ref result-list 1)))
                (begin
                  (set! sum-rmax (+ sum-rmax rmax))

                  (if (equal? debug-flag #t)
                      (begin
                        (let ((aa-sqr (* ii ii))
                              (aa-m1 (1- ii))
                              (aa-p1 (1+ ii)))
                          (let ((aa-m1-exp
                                 (integer-power aa-m1 rexp))
                                (aa-p1-exp
                                 (integer-power aa-p1 rexp)))
                            (let ((aa-plus
                                   (+ aa-m1-exp aa-p1-exp)))
                              (let ((remain
                                     (modulo aa-plus aa-sqr)))
                                (begin
                                  (display
                                   (ice-9-format:format
                                    #f "  a = ~:d, n = ~:d : "
                                    ii rexp))
                                  (display
                                   (ice-9-format:format
                                    #f "~:d^~:d + ~:d^~:d = ~:d "
                                    aa-m1 rexp aa-p1 rexp aa-plus))
                                  (display
                                   (ice-9-format:format
                                    #f "== ~:d mod ~:d : rmax(~:d) = ~:d~%"
                                    remain aa-sqr ii rmax))
                                  (force-output)
                                  )))
                            ))
                        ))
                  )))
            ))

        (display
         (ice-9-format:format
          #f "sum of the rmax = ~:d  (~:d <= n <= ~:d)~%"
          sum-rmax start-num end-num))
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Let r be the remainder when "))
    (display
     (format #f "(a-1)^n + (a+1)^n~%"))
    (display
     (format #f "is divided by a^2.~%"))
    (newline)
    (display
     (format #f "For example, if a = 7 and n = 3, "))
    (display
     (format #f "then r = 42:~%"))
    (display
     (format #f "63 + 83 = 728 == 42 mod 49. And as "))
    (display
     (format #f "n varies, so too~%"))
    (display
     (format #f "will r, but for a = 7 it turns out "))
    (display
     (format #f "that rmax = 42.~%"))
    (newline)
    (display
     (format #f "For 3 <= a <= 1000, find Sum(rmax).~%"))
    (newline)
    (display
     (format #f "The solution was found at~%"))
    (display
     (format #f "https://blog.dreamshire.com/project-euler-120-solution/~%"))
    (newline)
    (display
     (format #f "Using the binomial theorem,~%"))
    (display
     (format #f "http://en.wikipedia.org/wiki/Binomial_theorem~%"))
    (display
     (format #f "one can rewrite (a-1)^n + (a+1)^n="))
    (display
     (format #f "Sum(Choose(n,k)~%"))
    (display
     (format #f "* a^k * ((-1)^(n-k) + 1)).~%"))
    (newline)
    (display
     (format #f "From the article above there are "))
    (display
     (format #f "three cases to examine:~%"))
    (display
     (format #f "n=1, n even, n odd.  When n=1, "))
    (display
     (format #f "(a-1)+(a+1)=2a.~%"))
    (display
     (format #f "When n is even, then (n-k) is odd "))
    (display
     (format #f "when k is odd,~%"))
    (display
     (format #f "so only the even powers a^k remain "))
    (display
     (format #f "(-1+1 = 0),~%"))
    (display
     (format #f "then a^2t = 0 mod a^2.  (a-1)^n + "))
    (display
     (format #f "(a+1)^n = 2 mod a^2.~%"))
    (display
     (format #f "When n is odd, then (n-k) is even "))
    (display
     (format #f "when k is odd,~%"))
    (display
     (format #f "so only the odd powers of a^k remain. "))
    (display
     (format #f "Every term in~%"))
    (display
     (format #f "the binomial expansion contains a "))
    (display
     (format #f "power of a^2~%"))
    (display
     (format #f "(each a^k is equal to a^2 times a "))
    (display
     (format #f "constant),~%"))
    (display
     (format #f "except for the k=1 term, so~%"))
    (display
     (format #f "(a-1)^n + (a+1)^n = 2na mod a^2.~%"))
    (display
     (format #f "see https://projecteuler.net/problem=120~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((start-num 3)
          (end-num 7)
          (max-exp 10)
          (debug-flag #t))
      (begin
        (sub-main-loop start-num end-num max-exp debug-flag)
        ))

    (newline)
    (force-output)

    (let ((start-num 3)
          (end-num 1000)
          (max-exp 2000)
          (debug-flag #f))
      (begin
        (sub-main-loop start-num end-num max-exp debug-flag)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 120 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

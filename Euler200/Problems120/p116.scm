#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 116                                    ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated July 29, 2022                                ###
;;;###                                                       ###
;;;###  updated March 11, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (count-rb-fixed-length max-array-length red-len)
  (define (local-count-loop gap-len red-len gap-htable)
    (begin
      (let ((hcount (hash-ref gap-htable gap-len #f)))
        (begin
          (cond
           ((< gap-len red-len)
            (begin
              0
              ))
           ((= gap-len red-len)
            (begin
              1
              ))
           ((not (equal? hcount #f))
            (begin
              hcount
              ))
           (else
            (begin
              (let ((count (1+ (- gap-len red-len)))
                    (remaining-gap (- gap-len red-len)))
                (let ((sub-count 0))
                  (begin
                    (do ((jj 0 (1+ jj)))
                        ((> jj remaining-gap))
                      (begin
                        (let ((next-gap (- remaining-gap jj)))
                          (let ((next-count
                                 (local-count-loop
                                  next-gap red-len gap-htable)))
                            (begin
                              (set! sub-count (+ sub-count next-count))
                              )))
                        ))

                    (set! count (+ count sub-count))


                    (hash-set! gap-htable gap-len count)
                    count
                    )))
              )))
          ))
      ))
  (begin
    (let ((gap-htable (make-hash-table)))
      (let ((count
             (local-count-loop
              max-array-length red-len gap-htable)))
        (begin
          count
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-count-rb-fixed-length-1 result-hash-table)
 (begin
   (let ((sub-name "test-count-rb-fixed-length-1")
         (test-list
          (list
           (list 5 2 7) (list 5 3 3) (list 5 4 2)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((a-len (list-ref this-list 0))
                  (min-len (list-ref this-list 1))
                  (shouldbe (list-ref this-list 2)))
              (let ((result (count-rb-fixed-length a-len min-len)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "a-len=~a, min-len=~a, "
                        a-len min-len))
                      (err-3
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append
                      err-1 err-2 err-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop max-length)
  (begin
    (let ((a-list-list (list 2 3 4))
          (total-list (list))
          (total-sum 0))
      (begin
        (for-each
         (lambda (a-color)
           (begin
             (let ((count
                    (count-rb-fixed-length max-length a-color)))
               (begin
                 (set! total-list (cons count total-list))
                 (set! total-sum (+ total-sum count))
                 ))
             )) a-list-list)

        (display
         (ice-9-format:format
          #f "there are ~a = ~:d ways of replacing the~%"
          (string-join
           (map
            number->string (reverse total-list)) " + ")
          total-sum))
        (display
         (ice-9-format:format
          #f "black tiles in a row measuring ~:d units in length.~%"
          max-length))
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "A row of five black square tiles "))
    (display
     (format #f "is to have a~%"))
    (display
     (format #f "number of its tiles replaced with "))
    (display
     (format #f "coloured oblong tiles~%"))
    (display
     (format #f "chosen from red (length two), green "))
    (display
     (format #f "(length three), or~%"))
    (display
     (format #f "blue (length four).~%"))
    (newline)
    (display
     (format #f "If red tiles are chosen there are "))
    (display
     (format #f "exactly seven ways~%"))
    (display
     (format #f "this can be done.~%"))
    (newline)
    (display
     (format #f "If green tiles are chosen there "))
    (display
     (format #f "are three ways.~%"))
    (newline)
    (display
     (format #f "And if blue tiles are chosen "))
    (display
     (format #f "there are two ways.~%"))
    (newline)
    (display
     (format #f "Assuming that colours cannot be "))
    (display
     (format #f "mixed there are~%"))
    (display
     (format #f "7 + 3 + 2 = 12 ways of replacing "))
    (display
     (format #f "the black tiles~%"))
    (display
     (format #f "in a row measuring five units "))
    (display
     (format #f "in length.~%"))
    (newline)
    (display
     (format #f "How many different ways can the "))
    (display
     (format #f "black tiles in a~%"))
    (display
     (format #f "row measuring fifty units in length "))
    (display
     (format #f "be replaced if~%"))
    (display
     (format #f "colours cannot be mixed and at "))
    (display
     (format #f "least one coloured~%"))
    (display
     (format #f "tile must be used?~%"))
    (newline)
    (display
     (format #f "NOTE: This is related to problem 117.~%"))
    (display
     (format #f "see https://projecteuler.net/problem=116~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((max-length 5))
      (begin
        (sub-main-loop max-length)
        ))

    (newline)
    (let ((max-length 50))
      (begin
        (sub-main-loop max-length)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 116 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

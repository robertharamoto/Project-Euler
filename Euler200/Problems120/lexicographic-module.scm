;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  lexicographic-module - ordering functions            ###
;;;###                                                       ###
;;;###  last updated July 17, 2024                           ###
;;;###                                                       ###
;;;###  created July 17, 2024                                ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
(define-module (lexicographic-module)
  #:export (reverse-vector-k-to-n
            next-lexicographic-permutation
            ))


;;;#############################################################
;;;#############################################################
;;;### include modules

;;;### srfi-1 for fold functions and delete-duplicates
(use-modules ((srfi srfi-1)
              :renamer (symbol-prefix-proc 'srfi-1:)))

;;;#############################################################
;;;#############################################################
(define (reverse-vector-k-to-n this-vector k)
  (begin
    (let ((vlen (vector-length this-vector))
          (result-vector (vector-copy this-vector)))
      (let ((index-diff (- vlen k)))
        (begin
          (cond
           ((< index-diff 0)
            (begin
              result-vector
              ))
           (else
            (begin
              (let ((ii1 k)
                    (ii2 (- vlen 1))
                    (half-diff (euclidean/ index-diff 2)))
                (begin
                  (do ((jj 0 (+ jj 1)))
                      ((or (>= jj half-diff)
                           (>= ii1 ii2)
                           (>= ii1 vlen)
                           (< ii2 0)
                           ))
                    (begin
                      (let ((v1 (vector-ref result-vector ii1))
                            (v2 (vector-ref result-vector ii2)))
                        (begin
                          (vector-set! result-vector ii1 v2)
                          (vector-set! result-vector ii2 v1)
                          (set! ii1 (+ ii1 1))
                          (set! ii2 (- ii2 1))
                          ))
                      ))
                  result-vector
                  ))
              )))
          )))
    ))

;;;#############################################################
;;;#############################################################
;;; note assumes that this-vector is sorted in ascending order
(define (next-lexicographic-permutation this-vector)
  (begin
    (let ((vlen (1- (vector-length this-vector)))
          (result-vector (vector-copy this-vector))
          (kk 0)
          (aakk 0)
          (ll 0)
          (aall 0)
          (permutation-exists #f))
      (begin
        ;;; 1) find largest kk such that a[kk] < a[kk+1]
        (do ((ii 0 (1+ ii)))
            ((>= ii vlen))
          (begin
            (let ((v1 (vector-ref result-vector ii))
                  (v2 (vector-ref result-vector (1+ ii))))
              (begin
                (if (< v1 v2)
                    (begin
                      (set! permutation-exists #t)
                      (set! kk ii)
                      (set! aakk v1)
                      ))
                ))
            ))

        ;;; 2) find the largest ll such that a[kk] < a[ll]
        (if (equal? permutation-exists #t)
            (begin
              (do ((ii (+ kk 1) (1+ ii)))
                  ((> ii vlen))
                (begin
                  (let ((v1 (vector-ref result-vector ii)))
                    (begin
                      (if (< aakk v1)
                          (begin
                            (set! ll ii)
                            (set! aall v1)
                            ))
                      ))
                  ))

              ;;; 3) swap a[kk] with a[ll]
              (vector-set! result-vector kk aall)
              (vector-set! result-vector ll aakk)

              ;;; 4) reverse the sequence from (k+1) on
              (let ((final-result
                     (reverse-vector-k-to-n
                      result-vector (+ kk 1))))
                (begin
                  final-result
                  )))
            (begin
              #f
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

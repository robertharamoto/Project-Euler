#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 119                                    ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated July 29, 2022                                ###
;;;###                                                       ###
;;;###  updated March 11, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### srfi-1 for fold function
(use-modules ((srfi srfi-1)
              :renamer (symbol-prefix-proc 'srfi-1:)))

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### digits-module for split-digits-list function
(use-modules ((digits-module)
              :renamer (symbol-prefix-proc 'digits-module:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (list-to-sum-string llist)
  (begin
    (let ((this-string
           (string-join
            (map
             (lambda (this-elem)
               (begin
                 (ice-9-format:format #f "~:d" this-elem)
                 )) llist)
            " + ")))
      (begin
        this-string
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-list-to-sum-string-1 result-hash-table)
 (begin
   (let ((sub-name "test-list-to-sum-string-1")
         (test-list
          (list
           (list (list 1) "1")
           (list (list 1 2) "1 + 2")
           (list (list 1 2 3) "1 + 2 + 3")
           (list (list 4 5 6 7) "4 + 5 + 6 + 7")
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((input-list (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result
                     (list-to-sum-string input-list)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : input-list=~a, "
                        sub-name test-label-index input-list))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (display-statistics
         sequence-counter a-num
         d-string a-base a-exp)
  (begin
    (display
     (ice-9-format:format
      #f "  a(~:d) = ~:d : ~a = ~:d "
      sequence-counter a-num
      d-string a-base))
    (display
     (ice-9-format:format
      #f "  and ~:d^~:d = ~:d~%"
      a-base a-exp a-num))
    (force-output)
    ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop
         max-number max-exponent
         sequence-target-number debug-flag)
  (begin
    (let ((sequence-counter 0)
          (continue-loop-flag #t)
          (result-list (list)))
      (begin
        (do ((ii 2 (1+ ii)))
            ((> ii max-number))
          (begin
            (let ((num ii))
              (begin
                (do ((jj 1 (1+ jj)))
                    ((> jj max-exponent))
                  (begin
                    (let ((digit-list
                           (digits-module:split-digits-list num)))
                      (let ((d-sum
                             (srfi-1:fold + 0 digit-list)))
                        (begin
                          (if (and (= d-sum ii)
                                   (>= num 10))
                              (begin
                                (set!
                                 sequence-counter
                                 (1+ sequence-counter))
                                (set!
                                 result-list
                                 (cons (list num ii jj) result-list))
                                ))

                          (set! num (* num ii))
                          )))
                    ))
                ))
            ))

        (if (< sequence-counter sequence-target-number)
            (begin
              (display
               (ice-9-format:format
                #f "no results found for numbers "))
              (display
               (ice-9-format:format
                #f "between 10 and ~:d~%"
                max-number))
              (force-output))
            (begin
              (let ((sorted-list-list
                     (sort
                      result-list
                      (lambda (a b)
                        (begin
                          (< (list-ref a 0) (list-ref b 0))
                          ))))
                    (rlen (length result-list)))
                (begin
                  (if (equal? debug-flag #t)
                      (begin
                        (let ((sequence-counter 1))
                          (begin
                            (do ((ii 0 (1+ ii)))
                                ((> ii sequence-target-number))
                              (begin
                                (let ((a-list
                                       (list-ref sorted-list-list ii)))
                                  (let ((a-num (list-ref a-list 0))
                                        (a-base (list-ref a-list 1))
                                        (a-exp (list-ref a-list 2)))
                                    (let ((d-list
                                           (digits-module:split-digits-list
                                            a-num)))
                                      (let ((d-string
                                             (list-to-sum-string d-list)))
                                        (begin
                                          (display-statistics
                                           sequence-counter a-num
                                           d-string a-base a-exp)
                                          (force-output)
                                          (set!
                                           sequence-counter
                                           (1+ sequence-counter))
                                          )))
                                    ))
                                ))
                            (newline)
                            ))
                        ))

                  (let ((r-list
                         (list-ref
                          sorted-list-list
                          (1- sequence-target-number))))
                    (let ((a-num (list-ref r-list 0))
                          (a-base (list-ref r-list 1))
                          (a-exp (list-ref r-list 2)))
                      (let ((d-list
                             (digits-module:split-digits-list a-num)))
                        (let ((d-string
                               (list-to-sum-string d-list)))
                          (begin
                            (display-statistics
                             sequence-target-number a-num
                             d-string a-base a-exp)
                            (force-output)
                            )))
                      ))
                  ))
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "The number 512 is interesting "))
    (display
     (format #f "because it is~%"))
    (display
     (format #f "equal to the sum of its digits "))
    (display
     (format #f "raised to some~%"))
    (display
     (format #f "power: 5 + 1 + 2 = 8, and 8^3 = 512. "))
    (display
     (format #f "Another example~%"))
    (display
     (format #f "of a number with this property is "))
    (display
     (format #f "614656 = 28^4.~%"))
    (newline)
    (display
     (format #f "We shall define an to be the nth "))
    (display
     (format #f "term of this~%"))
    (display
     (format #f "sequence and insist that a number "))
    (display
     (format #f "must contain at~%"))
    (display
     (format #f "least two digits to have a sum.~%"))
    (newline)
    (display
     (format #f "You are given that a2 = 512 "))
    (display
     (format #f "and a10 = 614656.~%"))
    (newline)
    (display
     (format #f "Find a30.~%"))
    (display
     (format #f "see https://projecteuler.net/problem=119~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((max-number 100)
          (max-exponent 20)
          (sequence-target-number 10)
          (debug-flag #t))
      (begin
        (sub-main-loop
         max-number max-exponent
         sequence-target-number debug-flag)
        ))

    (newline)
    (let ((max-number 100)
          (max-exponent 20)
          (sequence-target-number 30)
          (debug-flag #f))
      (begin
        (sub-main-loop
         max-number max-exponent
         sequence-target-number debug-flag)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 119 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (load "digits-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  unit tests for prime-module.scm                      ###
;;;###                                                       ###
;;;###  last updated August 1, 2024                          ###
;;;###                                                       ###
;;;###  updated May 19, 2022                                 ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  load modules                                         ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

(use-modules ((utils-module)
              :renamer (symbol-prefix-proc 'utils-module:)))

(use-modules ((prime-module)
              :renamer (symbol-prefix-proc 'prime-module:)))

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  begin test suite                                     ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
(define (prime-simple-test-check
         shouldbe result
         extra-string extra-data
         sub-name test-label-index
         result-hash-table)
  (begin
    (let ((err-1
           (format
            #f "~a : (~a) error : ~a=~a : "
            sub-name test-label-index
            extra-string extra-data))
          (err-2
           (format
            #f "shouldbe = ~a, result = ~a"
            shouldbe result)))
      (let ((error-string
             (string-append err-1 err-2)))
        (begin
          (unittest2:assert?
           (equal? shouldbe result)
           sub-name
           error-string
           result-hash-table)
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-smallest-divisor-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-smallest-divisor-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list 2 2) (list 3 3) (list 4 2) (list 5 5)
           (list 6 2) (list 7 7) (list 8 2) (list 9 3)
           (list 10 2) (list 11 11) (list 12 2) (list 13 13)
           (list 14 2) (list 15 3) (list 16 2) (list 17 17)
           (list 18 2) (list 19 19) (list 20 2)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((num (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result
                     (prime-module:smallest-divisor num)))
                (let ((string-1 "num"))
                  (begin
                    (prime-simple-test-check
                     shouldbe result
                     string-1 num
                     sub-name test-label-index
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-is-prime-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-is-prime-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list 2 #t) (list 3 #t) (list 4 #f) (list 5 #t)
           (list 6 #f) (list 7 #t) (list 8 #f) (list 9 #f)
           (list 10 #f) (list 11 #t) (list 12 #f) (list 13 #t)
           (list 14 #f) (list 15 #f) (list 16 #f) (list 17 #t)
           (list 18 #f) (list 19 #t) (list 20 #f) (list 21 #f)
           (list 22 #f) (list 23 #t) (list 24 #f) (list 25 #f)
           (list 26 #f) (list 27 #f) (list 28 #f) (list 29 #t)
           (list 30 #f) (list 31 #t) (list 32 #f) (list 33 #f)
           (list 34 #f) (list 35 #f) (list 36 #f) (list 37 #t)
           (list 38 #f) (list 39 #f) (list 40 #f) (list 41 #t)
           (list 42 #f) (list 43 #t) (list 44 #f) (list 45 #f)
           (list 46 #f) (list 47 #t) (list 48 #f) (list 49 #f)
           (list 50 #f) (list 51 #f) (list 52 #f) (list 53 #t)
           (list 54 #f) (list 55 #f) (list 56 #f) (list 57 #f)
           (list 58 #f) (list 59 #t) (list 60 #f) (list 61 #t)
           (list 62 #f) (list 63 #f) (list 64 #f) (list 65 #f)
           (list 66 #f) (list 67 #t) (list 68 #f) (list 69 #f)
           (list 70 #f) (list 71 #t) (list 72 #f) (list 73 #t)
           (list 74 #f) (list 75 #f) (list 76 #f) (list 77 #f)
           (list 78 #f) (list 79 #t) (list 80 #f) (list 81 #f)
           (list 82 #f) (list 83 #t) (list 84 #f) (list 85 #f)
           (list 86 #f) (list 77 #f) (list 78 #f) (list 79 #t)
           (list 80 #f) (list 81 #f) (list 82 #f) (list 83 #t)
           (list 84 #f) (list 85 #f) (list 86 #f) (list 87 #f)
           (list 88 #f) (list 89 #t) (list 90 #f) (list 91 #f)
           (list 92 #f) (list 93 #f) (list 94 #f) (list 95 #f)
           (list 96 #f) (list 97 #t) (list 98 #f) (list 99 #f)
           (list 100 #f)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((num (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result
                     (prime-module:is-prime? num)))
                (let ((string-1 "num"))
                  (begin
                    (prime-simple-test-check
                     shouldbe result
                     string-1 num
                     sub-name test-label-index
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-lookup-is-prime-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-lookup-is-prime-1"
           (utils-module:get-basename (current-filename))))
         (prime-htable (make-hash-table 100))
         (test-list
          (list
           (list 2 5 #t) (list 3 5 #t) (list 4 5 #f)
           (list 5 5 #t) (list 6 5 #f) (list 7 5 #t)
           (list 8 5 #f) (list 9 5 #f) (list 10 5 #f)
           (list 11 5 #t) (list 12 5 #f) (list 13 5 #t)
           (list 14 5 #f) (list 15 5 #f) (list 16 5 #f)
           (list 17 5 #t) (list 18 5 #f) (list 19 5 #t)
           (list 20 5 #f) (list 21 5 #f) (list 22 5 #f)
           (list 23 5 #t) (list 24 5 #f) (list 25 5 #f)
           (list 26 5 #f) (list 27 5 #f) (list 28 5 #f)
           (list 29 5 #t) (list 30 5 #f) (list 31 5 #t)
           (list 32 5 #f) (list 33 5 #f) (list 34 5 #f)
           (list 35 5 #f) (list 36 5 #f) (list 37 5 #t)
           (list 38 5 #f) (list 39 5 #f) (list 40 5 #f)
           (list 121 5 #f) (list 122 5 #f) (list 123 5 #f)
           (list 124 5 #f) (list 125 5 #f) (list 126 5 #f)
           (list 127 5 #t) (list 128 5 #f) (list 129 5 #f)
           (list 130 5 #f) (list 131 5 #t) (list 132 5 #f)
           (list 169 5 #f) (list 170 5 #f) (list 171 5 #f)
           (list 172 5 #f) (list 173 5 #t) (list 174 5 #f)
           (list 175 5 #f) (list 176 5 #f) (list 177 5 #f)
           (list 178 5 #f) (list 179 5 #t) (list 180 5 #f)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (hash-clear! prime-htable)
            (let ((test-num (list-ref alist 0))
                  (top-prime (list-ref alist 1))
                  (shouldbe-bool (list-ref alist 2)))
              (let ((max-prime
                     (prime-module:populate-prime-hash!
                      prime-htable top-prime)))
                (let ((result-bool
                       (prime-module:lookup-is-prime?
                        test-num max-prime prime-htable)))
                  (let ((string-1
                         (format
                          #f "number=~a, max-prime" test-num)))
                    (begin
                      (prime-simple-test-check
                       shouldbe-bool result-bool
                       string-1 max-prime
                       sub-name test-label-index
                       result-hash-table)
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-lookup-is-prime-2 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-lookup-is-prime-2"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list 2 11 #t) (list 3 11 #t) (list 4 11 #f)
           (list 5 11 #t) (list 6 11 #f) (list 7 11 #t)
           (list 8 11 #f) (list 9 11 #f) (list 10 11 #f)
           (list 11 11 #t) (list 12 11 #f) (list 13 11 #t)
           (list 14 11 #f) (list 15 11 #f) (list 16 11 #f)
           (list 17 11 #t) (list 18 11 #f) (list 19 11 #t)
           (list 20 11 #f) (list 21 11 #f) (list 22 11 #f)
           (list 23 11 #t) (list 24 11 #f) (list 25 11 #f)
           (list 26 11 #f) (list 27 11 #f) (list 28 11 #f)
           (list 29 11 #t) (list 30 11 #f) (list 31 11 #t)
           (list 32 11 #f) (list 33 11 #f) (list 34 11 #f)
           (list 35 11 #f) (list 36 11 #f) (list 37 11 #t)
           (list 38 11 #f) (list 39 11 #f) (list 40 11 #f)
           (list 121 11 #f) (list 122 11 #f) (list 123 11 #f)
           (list 124 11 #f) (list 125 11 #f) (list 126 11 #f)
           (list 127 11 #t) (list 128 11 #f) (list 129 11 #f)
           (list 130 11 #f) (list 131 11 #t) (list 132 11 #f)
           (list 169 11 #f) (list 170 11 #f) (list 171 11 #f)
           (list 172 11 #f) (list 173 11 #t) (list 174 11 #f)
           (list 175 11 #f) (list 176 11 #f) (list 177 11 #f)
           (list 178 11 #f) (list 179 11 #t) (list 180 11 #f)
           ))
         (prime-htable (make-hash-table 100))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (hash-clear! prime-htable)
            (let ((test-num (list-ref alist 0))
                  (top-prime (list-ref alist 1))
                  (shouldbe-bool (list-ref alist 2)))
              (let ((max-prime
                     (prime-module:populate-prime-hash!
                      prime-htable top-prime)))
                (let ((result-bool
                       (prime-module:lookup-is-prime?
                        test-num max-prime prime-htable)))
                  (let ((string-1
                         (format #f "test-num=~a, max-prime"
                                 test-num)))
                    (begin
                      (prime-simple-test-check
                       shouldbe-bool result-bool
                       string-1 max-prime
                       sub-name test-label-index
                       result-hash-table)
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-populate-prime-hash-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-make-prime-hash-1"
           (utils-module:get-basename (current-filename))))
         (prime-htable (make-hash-table 100))
         (test-list
          (list
           (list 5 5 (list 2 3 5))
           (list 7 7 (list 2 3 5 7))
           (list 9 7 (list 2 3 5 7))
           (list 11 11 (list 2 3 5 7 11))
           (list 20 19 (list 2 3 5 7 11 13 17 19))
           (list 21 19 (list 2 3 5 7 11 13 17 19))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (hash-clear! prime-htable)
            (let ((test-num (list-ref alist 0))
                  (shouldbe-max-prime (list-ref alist 1))
                  (shouldbe-list (list-ref alist 2)))
              (let ((result-max-prime
                     (prime-module:populate-prime-hash!
                      prime-htable test-num)))
                (let ((err-1
                       (format
                        #f "~a : (~a) error : number=~a : "
                        sub-name test-label-index test-num))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a~%"
                        shouldbe-max-prime result-max-prime)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-max-prime result-max-prime)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)

                    (let ((result-list
                           (sort
                            (hash-map->list
                             (lambda (key value)
                               (begin
                                 key)) prime-htable)
                            <)))
                      (begin
                        (prime-assert-lists-equal
                         shouldbe-list result-list
                         sub-name
                         err-1
                         result-hash-table)
                        ))
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-make-prime-array-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-make-prime-array-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list 2 (list 2)) (list 3 (list 2 3))
           (list 4 (list 2 3)) (list 5 (list 2 3 5))
           (list 6 (list 2 3 5)) (list 7 (list 2 3 5 7))
           (list 8 (list 2 3 5 7)) (list 9 (list 2 3 5 7))
           (list 10 (list 2 3 5 7)) (list 11 (list 2 3 5 7 11))
           (list 13 (list 2 3 5 7 11 13))
           (list 17 (list 2 3 5 7 11 13 17))
           (list 19 (list 2 3 5 7 11 13 17 19))
           (list 23 (list 2 3 5 7 11 13 17 19 23))
           (list 31 (list 2 3 5 7 11 13 17 19 23 29 31))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((test-num (list-ref this-list 0))
                  (shouldbe-list (list-ref this-list 1)))
              (let ((result-array
                     (prime-module:make-prime-array test-num)))
                (let ((result-list (array->list result-array)))
                  (let ((err-1
                         (format
                          #f "~a : (~a) error : num=~a"
                          sub-name test-label-index test-num)))
                    (begin
                      (prime-assert-lists-equal
                       shouldbe-list result-list
                       sub-name
                       err-1
                       result-hash-table)
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-binary-search-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-binary-search-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list (list 2 3 5 7 11) 5 -1 #f)
           (list (list 2 3 5 7 11) 5 1 #f)
           (list (list 2 3 5 7 11) 5 2 0)
           (list (list 2 3 5 7 11) 5 3 1)
           (list (list 2 3 5 7 11) 5 4 #f)
           (list (list 2 3 5 7 11) 5 5 2)
           (list (list 2 3 5 7 11) 5 6 #f)
           (list (list 2 3 5 7 11) 5 7 3)
           (list (list 2 3 5 7 11) 5 8 #f)
           (list (list 2 3 5 7 11) 5 9 #f)
           (list (list 2 3 5 7 11) 5 10 #f)
           (list (list 2 3 5 7 11) 5 11 4)
           (list (list 2 3 5 7 11) 5 12 #f)
           (list (list 2 3 5 7 11) 5 13 #f)
           (list (list 2 3 5 7 11 13) 6 1 #f)
           (list (list 2 3 5 7 11 13) 6 5 2)
           (list (list 2 3 5 7 11 13) 6 6 #f)
           (list (list 2 3 5 7 11 13) 6 7 3)
           (list (list 2 3 5 7 11 13) 6 13 5)
           (list (list 2 3 5 7 11 13) 6 14 #f)
           (list (list 2 3 5 7 11 13) 6 20 #f)
           (list (list 2 3 5 7 11 13 17 19) 8 7 3)
           (list (list 2 3 5 7 11 13 17 19) 8 17 6)
           (list (list 2 3 5 7 11 13 17 19) 8 19 7)
           (list (list 2 3 5 7 11 13 17 19) 8 21 #f)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((test-array
                   (list->array 1 (list-ref this-list 0)))
                  (arr-size (list-ref this-list 1))
                  (num (list-ref this-list 2))
                  (shouldbe (list-ref this-list 3)))
              (let ((result
                     (prime-module:binary-search
                      test-array arr-size num)))
                (let ((string-1
                       (format #f "num=~a, array" num)))
                  (begin
                    (prime-simple-test-check
                     shouldbe result
                     string-1 test-array
                     sub-name test-label-index
                     result-hash-table)
                    ))
                ))

            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-is-array-prime-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-is-array-prime-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list 0 #f) (list 1 #f) (list 2 #t) (list 3 #t)
           (list 4 #f) (list 5 #t) (list 6 #f) (list 7 #t)
           (list 8 #f) (list 9 #f) (list 10 #f) (list 11 #t)
           (list 12 #f) (list 13 #t) (list 14 #f) (list 15 #f)
           (list 16 #f) (list 17 #t) (list 18 #f) (list 19 #t)
           (list 20 #f) (list 21 #f) (list 22 #f) (list 23 #t)
           (list 24 #f) (list 25 #f) (list 26 #f) (list 27 #f)
           (list 28 #f) (list 29 #t) (list 30 #f) (list 31 #t)
           (list 32 #f) (list 33 #f) (list 34 #f) (list 35 #f)
           (list 36 #f) (list 37 #t) (list 38 #f) (list 39 #f)
           (list 40 #f) (list 41 #t) (list 42 #f) (list 43 #t)
           (list 44 #f) (list 45 #f) (list 46 #f) (list 47 #t)
           (list 48 #f) (list 49 #f) (list 50 #f) (list 51 #f)
           (list 52 #f) (list 53 #t) (list 54 #f) (list 55 #f)
           (list 56 #f) (list 57 #f) (list 58 #f) (list 59 #t)
           (list 60 #f) (list 61 #t) (list 62 #f) (list 63 #f)
           (list 64 #f) (list 65 #f) (list 66 #f) (list 67 #t)
           (list 68 #f) (list 69 #f) (list 70 #f) (list 71 #t)
           (list 72 #f) (list 73 #t) (list 74 #f) (list 75 #f)
           (list 76 #f) (list 77 #f) (list 78 #f) (list 79 #t)
           (list 80 #f) (list 81 #f) (list 82 #f) (list 83 #t)
           (list 84 #f) (list 85 #f) (list 86 #f) (list 87 #f)
           (list 88 #f) (list 89 #t) (list 90 #f) (list 91 #f)
           (list 92 #f) (list 93 #f) (list 94 #f) (list 95 #f)
           (list 96 #f) (list 97 #t) (list 98 #f) (list 99 #f)
           (list 100 #f)
           ))
         (prime-array (prime-module:make-prime-array 10))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((test-num (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result
                     (prime-module:is-array-prime?
                      test-num prime-array)))
                (let ((string-1 "test-num"))
                  (begin
                    (prime-simple-test-check
                     shouldbe result
                     string-1 test-num
                     sub-name test-label-index
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-is-consecutive-prime-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:is-consecutive-prime-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list 2 3 #t) (list 3 5 #t) (list 5 7 #t)
           (list 7 11 #t) (list 11 13 #t)  (list 13 17 #t)
           (list 2 5 #f) (list 3 7 #f) (list 7 13 #f)
           (list 11 17 #f) (list 11 19 #f) (list 11 23 #f)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((pp-1 (list-ref alist 0))
                  (pp-2 (list-ref alist 1))
                  (shouldbe (list-ref alist 2)))
              (let ((result
                     (prime-module:is-consecutive-prime?
                      pp-1 pp-2)))
                (let ((string-1
                       (format
                        #f "prime-1=~a, prime-2" pp-1)))
                  (begin
                    (prime-simple-test-check
                     shouldbe result
                     string-1 pp-2
                     sub-name test-label-index
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-make-2-consecutive-prime-numbers-1 result-hash-table)
 (begin
   (let ((sub-name
          (format
           #f "~a:test-make-2-consecutive-prime-numbers-1"
           (utils-module:get-basename (current-filename))))
         (test-list
          (list
           (list 4 (list 3 5)) (list 6 (list 5 7))
           (list 9 (list 7 11)) (list 12 (list 11 13))
           (list 15 (list 13 17)) (list 18 (list 17 19))
           (list 21 (list 19 23)) (list 26 (list 23 29))
           (list 30 (list 29 31)) (list 34  (list 31 37))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((psum (list-ref alist 0))
                  (shouldbe-list (list-ref alist 1)))
              (let ((result-list
                     (prime-module:make-2-consecutive-prime-numbers
                      psum)))
                (let ((error-message-1
                       (format
                        #f "~a : (~a) error : psum=~a"
                        sub-name test-label-index psum)))
                  (begin
                    (prime-assert-lists-equal
                     shouldbe-list result-list
                     sub-name
                     error-message-1
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (prime-assert-lists-equal
         shouldbe-list result-list
         sub-name error-message
         result-hash-table)
  (begin
    (let ((shouldbe-length (length shouldbe-list))
          (result-length (length result-list)))
      (let ((slen (min shouldbe-length result-length))
            (err-1
             (format
              #f "~a : shouldbe-list=~a, result-list=~a : "
              error-message shouldbe-list result-list))
            (err-2
             (format
              #f "shouldbe size=~a, result size=~a"
              shouldbe-length result-length)))
        (begin
          (unittest2:assert?
           (equal? shouldbe-length result-length)
           sub-name
           (string-append err-1 err-2)
           result-hash-table)

          (do ((ii 0 (1+ ii)))
              ((>= ii slen))
            (begin
              (let ((s-elem (list-ref shouldbe-list ii)))
                (let ((found-flag (member s-elem result-list)))
                  (let ((err-3
                         (format
                          #f "~a : missing element(~a)=~a"
                          err-1 ii s-elem)))
                    (begin
                      (unittest2:assert?
                       (not (equal? found-flag #f))
                       sub-name
                       err-3
                       result-hash-table)
                      ))
                  ))
              ))
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

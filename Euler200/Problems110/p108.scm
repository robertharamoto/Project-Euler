#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 108                                    ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated July 28, 2022                                ###
;;;###                                                       ###
;;;###  updated March 11, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 receive for receive values function
(use-modules ((ice-9 receive)
              :renamer (symbol-prefix-proc 'ice-9-receive:)))

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
;;; 1/x + 1/y = 1/n which is equivalent to n * (x + y) = x * y
;;; with d = x - n, or x = d + n, and n < x <= 2n
;;; (if x = 3n say, then y < 0)
;;; y = n + (n^2)/d, must find all divisors d of n^2 such that
;;; y an integer and x <= y
(define (calc-x-y-solutions nn)
  (begin
    (let ((solutions-list (list))
          (nsquared (* nn nn))
          (max-dd (+ nn 1)))
      (begin
        (do ((dd 1 (1+ dd)))
            ((> dd max-dd))
          (begin
            (ice-9-receive:receive
             (ee remain)
             (euclidean/ nsquared dd)
             (begin
               (if (zero? remain)
                   (begin
                     (let ((xx (+ dd nn))
                           (yy (+ nn ee)))
                       (begin
                         (if (>= yy xx)
                             (begin
                               (let ((elem (list xx yy)))
                                 (begin
                                   (set!
                                    solutions-list
                                    (cons (list xx yy) solutions-list))
                                   ))
                               ))
                         ))
                     ))
               ))
            ))

        (reverse solutions-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-calc-x-y-solutions-1 result-hash-table)
 (begin
   (let ((sub-name "test-calc-x-y-solutions-1")
         (test-list
          (list
           (list 2 (list (list 3 6) (list 4 4)))
           (list 3 (list (list 4 12) (list 6 6)))
           (list 4 (list (list 5 20) (list 6 12) (list 8 8)))
           (list 5 (list (list 6 30) (list 10 10)))
           (list 6 (list (list 7 42) (list 8 24) (list 9 18)
                         (list 10 15) (list 12 12)))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (a-list)
          (begin
            (let ((nn (list-ref a-list 0))
                  (shouldbe (list-ref a-list 1)))
              (let ((result (calc-x-y-solutions nn)))
                (let ((slen (length shouldbe))
                      (rlen (length result)))
                  (let ((err-1
                         (format
                          #f "~a : error (~a) : nn=~a, "
                          sub-name test-label-index nn))
                        (err-2
                         (format
                          #f "shouldbe length=~a, result length=~a"
                          slen rlen)))
                    (begin
                      (unittest2:assert?
                       (equal? slen rlen)
                       sub-name
                       (string-append err-1 err-2)
                       result-hash-table)

                      (for-each
                       (lambda (s-list)
                         (begin
                           (let ((err-3
                                  (format
                                   #f "shouldbe element=~a, result=~a~%"
                                   s-list result)))
                             (begin
                               (unittest2:assert?
                                (not
                                 (equal?
                                  (member s-list result)
                                  #f))
                                sub-name
                                (string-append err-1 err-3)
                                result-hash-table)
                               ))
                           )) shouldbe)
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (display-entire-list results-list nn)
  (begin
    (for-each
     (lambda (a-list)
       (begin
         (display
          (ice-9-format:format
           #f "    1/~:d + 1/~:d = 1/~:d~%"
           (list-ref a-list 0)
           (list-ref a-list 1) nn))
         )) results-list)
    ))

;;;#############################################################
;;;#############################################################
(define (display-partial-list results-list top-num nn)
  (begin
    (let ((rlen (length results-list))
          (counter 0)
          (loop-continue-flag #t))
      (begin
        (do ((ii 0 (1+ ii)))
            ((or (>= ii rlen)
                 (equal? loop-continue-flag #f)))
          (begin
            (let ((a-list (list-ref results-list ii)))
              (begin
                (display
                 (ice-9-format:format
                  #f "    1/~:d + 1/~:d = 1/~:d~%"
                  (list-ref a-list 0)
                  (list-ref a-list 1) nn))
                (set! counter (1+ counter))
                (if (> counter top-num)
                    (begin
                      (set! loop-continue-flag #f)
                      ))
                ))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop
         max-nn nn-base min-number-solutions debug-flag)
  (begin
    (let ((loop-continue-flag #t)
          (min-result-list (list))
          (min-nn -1))
      (begin
        (do ((nn 1 (1+ nn)))
            ((or (>= nn max-nn)
                 (equal? loop-continue-flag #f)))
          (begin
            (let ((nn-sum (* nn nn-base)))
              (let ((results-list
                     (calc-x-y-solutions nn-sum)))
                (begin
                  (if (and (list? results-list)
                           (> (length results-list) 0))
                      (begin
                        (let ((num-results (length results-list)))
                          (begin
                            (if (> num-results min-number-solutions)
                                (begin
                                  (set! loop-continue-flag #f)

                                  (display
                                   (ice-9-format:format
                                    #f "nn = ~:d, number of solutions = ~:d "
                                    nn-sum num-results))
                                  (display
                                   (ice-9-format:format
                                    #f "(exceeds ~:d solutions)~%"
                                    min-number-solutions))
                                  (if (equal? debug-flag #t)
                                      (begin
                                        (display-entire-list results-list nn-sum))
                                      (begin
                                        (let ((top-results 4))
                                          (begin
                                            (display
                                             (ice-9-format:format
                                              #f "first ~:d results out of ~:d~%"
                                              top-results num-results))
                                            (display-partial-list
                                             results-list top-results nn-sum)
                                            (force-output)
                                            ))
                                        ))
                                  (force-output))
                                (begin
                                  (if (equal? debug-flag #t)
                                      (begin
                                        (display-entire-list results-list nn)
                                        (newline)
                                        (force-output)
                                        ))

                                  (set! min-result-list results-list)
                                  (set! min-nn nn)
                                  ))
                            ))
                        ))
                  )))
            ))

        (if (equal? loop-continue-flag #t)
            (begin
              (display
               (ice-9-format:format
                #f "did not find any results where the number "))
              (display
               (ice-9-format:format
                #f "of solutions exceeded ~:d, for n < ~:d~%"
                min-number-solutions max-nn))
              (let ((top-results 4)
                    (num-results (length min-result-list)))
                (begin
                  (display
                   (ice-9-format:format
                    #f "first ~:d results out of ~:d~%"
                    top-results num-results))
                  (display-partial-list
                   min-result-list top-results min-nn)
                  ))
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "In the following equation x, y, "))
    (display
     (format #f "and n are positive~%"))
    (display
     (format #f "integers.~%"))
    (newline)
    (display
     (format #f "1/x + 1/y = 1/n~%"))
    (newline)
    (display
     (format #f "For n = 4 there are exactly three "))
    (display
     (format #f "distinct solutions:~%"))
    (display
     (format #f "1/5 + 1/20 = 1/4~%"))
    (display
     (format #f "1/6 + 1/12 = 1/4~%"))
    (display
     (format #f "1/8 + 1/8 = 1/4~%"))
    (newline)
    (display
     (format #f "What is the least value of n for "))
    (display
     (format #f "which the number~%"))
    (display
     (format #f "of distinct solutions exceeds "))
    (display
     (format #f "one-thousand?~%"))
    (newline)
    (display
     (format #f "NOTE: This problem is an easier "))
    (display
     (format #f "version of problem~%"))
    (display
     (format #f "110; it is strongly advised that "))
    (display
     (format #f "you solve this~%"))
    (display
     (format #f "one first.~%"))
    (newline)
    (display
     (format #f "Since using rationals are slow, "))
    (display
     (format #f "this algorithm tries~%"))
    (display
     (format #f "to find solutions using the "))
    (display
     (format #f "equation (for fixed n),~%"))
    (display
     (format #f "1/n = 1/x + 1/y,~%"))
    (display
     (format #f "then y is a solution if~%"))
    (display
     (format #f "y = (n * x) / (x - n)~%"))
    (newline)
    (display
     (format #f "The solution was found at~%"))
    (display
     (format #f "https://keyzero.wordpress.com/2010/06/05/project-euler-problem-108/~%"))
    (display
     (format #f "This is an extremely demanding program, "))
    (display
     (format #f "since one must~%"))
    (display
     (format #f "iterate over n and iterate over x "))
    (display
     (format #f "to find y.  Some~%"))
    (display
     (format #f "important limits to reduce the amount "))
    (display
     (format #f "of work needed,~%"))
    (display
     (format #f "n < x <= 2n, and setting d=(n-x), then "))
    (display
     (format #f "realizing that~%"))
    (display
     (format #f "y = n+(n^2)/d, so we just need to "))
    (display
     (format #f "look for~%"))
    (display
     (format #f "n < x <= 2n, and d a divisor of n^2, "))
    (display
     (format #f "where n^2 has~%"))
    (display
     (format #f "a large number of divisors, which "))
    (display
     (format #f "will give a large~%"))
    (display
     (format #f "number of solutions.~%"))
    (newline)
    (display
     (format #f "The number of divisors of a number "))
    (display
     (format #f "n, is given by~%"))
    (display
     (format #f "d(n) = Product(ai + 1), where i ranges "))
    (display
     (format #f "over each divisor~%"))
    (display
     (format #f "of n, and ai is the number of times "))
    (display
     (format #f "each divisor divides~%"))
    (display
     (format #f "n. For example, 24 = 2^3 x 3^1, "))
    (display
     (format #f "so d(24) = (3+1)x(1+1)=8,~%"))
    (display
     (format #f "and the divisors of 24 is 1, 2, 4, "))
    (display
     (format #f "8, 3, 6, 12, 24.~%"))
    (display
     (format #f "See the wikipedia article for more "))
    (display
     (format #f "details~%"))
    (display
     (format #f "https://en.wikipedia.org/wiki/Divisor_function~%"))
    (display
     (format #f "We can simply examine those n that "))
    (display
     (format #f "have a high number~%"))
    (display
     (format #f "of small prime factors, the article "))
    (display
     (format #f "mentioned above shows~%"))
    (display
     (format #f "that one only needs to consider those "))
    (display
     (format #f "values of n that~%"))
    (display
     (format #f "are multiples of 2x3x5x7x11x13=30,030, "))
    (display
     (format #f "since n^2 will have~%"))
    (display
     (format #f "(2+1)^6=729 divisors or more. Since "))
    (display
     (format #f "30,030x17=510,510,~%"))
    (display
     (format #f "and 510,510^2 has (2+1)^7=2187 divisors, "))
    (display
     (format #f "then y=n+n^2/d~%"))
    (display
     (format #f "will have 2,187 possible solutions, so "))
    (display
     (format #f "we can stop our~%"))
    (display
     (format #f "iteration after n = 510,510.~%"))
    (display
     (format #f "see https://projecteuler.net/problem=108~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((max-nn 10)
          (nn-base 1)
          (min-number-solutions 4)
          (debug-flag #t))
      (begin
        (sub-main-loop
         max-nn nn-base min-number-solutions debug-flag)
        ))

    (newline)
    (let ((max-nn 17)
          (nn-base (* 2 3 5 7 11 13))
          (min-number-solutions 1000)
          (debug-flag #f))
      (begin
        (sub-main-loop
         max-nn nn-base min-number-solutions debug-flag)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 108 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

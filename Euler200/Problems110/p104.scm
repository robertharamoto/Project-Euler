#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 104                                    ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated July 28, 2022                                ###
;;;###                                                       ###
;;;###  updated March 11, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### digits-module for split-digits-list function
(use-modules ((digits-module)
              :renamer (symbol-prefix-proc 'digits-module:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (is-list-1-9-pandigital? digit-list)
  (begin
    (let ((max-digit 9)
          (ok-flag #t))
      (let ((llen (length digit-list)))
        (begin
          (if (not (= llen 9))
              (begin
                #f)
              (begin
                (do ((ii 1 (+ ii 1)))
                    ((or (> ii max-digit)
                         (equal? ok-flag #f)))
                  (begin
                    (if (equal? (member ii digit-list) #f)
                        (begin
                          (set! ok-flag #f)
                          #f
                          ))
                    ))
                ))
          ok-flag
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-is-list-1-9-pandigital-1 result-hash-table)
 (begin
   (let ((sub-name "test-is-list-1-9-pandigital-1")
         (test-list
          (list
           (list (list 1 2 3 4 5 6 7 8 9) #t)
           (list (list 9 2 3 4 5 6 7 8 1) #t)
           (list (list 9 2 3 5 4 6 7 8 1) #t)
           (list (list 8 2 3 4 5 6 7 8 9) #f)
           (list (list 8 2 3 4 5 1 7 8 9) #f)
           (list (list 1 2 3 4 5 6 7 8 9 3) #t)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((input-list (list-ref alist 0))
                  (shouldbe-bool (list-ref alist 1)))
              (let ((result-bool
                     (is-list-1-9-pandigital? input-list)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : input-list=~a, "
                        sub-name test-label-index input-list))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe-bool result-bool)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-bool result-bool)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop end-num min-num status-num debug-flag)
  (begin
    (let ((f1 1)
          (f2 1)
          (f3 0)
          (loop-continue-flag #t)
          (first-num 0)
          (first-fib 0))
      (begin
        (do ((ii 3 (1+ ii)))
            ((or (> ii end-num)
                 (equal? loop-continue-flag #f)))
          (begin
            (set! f3 (+ f1 f2))
            (set! f1 f2)
            (set! f2 f3)

            (let ((dlist
                   (digits-module:split-digits-list
                    (modulo f3 min-num))))
              (let ((dlen (length dlist)))
                (begin
                  (if (>= dlen 9)
                      (begin
                        (let ((bottom-digits
                               (list-head (reverse dlist) 9)))
                          (begin
                            (if (is-list-1-9-pandigital? bottom-digits)
                                (begin
                                  (let ((top-digits
                                         (list-head
                                          (digits-module:split-digits-list
                                           f3) 9)))
                                    (begin
                                      (if (is-list-1-9-pandigital? top-digits)
                                          (begin
                                            (set! loop-continue-flag #f)
                                            (set! first-num ii)
                                            (set! first-fib f3)
                                            ))
                                      ))
                                  ))
                            ))
                        ))
                  )))

            (if (zero? (modulo ii status-num))
                (begin
                  (let ((dlist
                         (digits-module:split-digits-list f3)))
                    (let ((dlen (length dlist)))
                      (begin
                        (display
                         (ice-9-format:format
                          #f "~:d / ~:d : fib(~:d) "
                          ii end-num ii))
                        (display
                         (ice-9-format:format
                          #f "digit length = ~:d : ~a~%"
                          dlen
                          (timer-module:current-date-time-string)))
                        (force-output)
                        )))
                  ))

            (if (equal? debug-flag #t)
                (begin
                  (display
                   (ice-9-format:format
                    #f "  F(~:d) = ~:d,  " ii f3))
                  (if (zero? (modulo ii 5))
                      (begin
                        (newline)
                        ))
                  (force-output)
                  ))
            ))

        (if (> first-num 0)
            (begin
              (display
               (ice-9-format:format
                #f "found a fibonacci number such that the "))
              (display
               (ice-9-format:format
                #f "first and last 9 digits are pandigital~%"))
              (let ((dlist
                     (digits-module:split-digits-list f3)))
                (let ((dlen (length dlist))
                      (top-digits (list-head dlist 9)))
                  (let ((bottom-digits (list-tail dlist (- dlen 9))))
                    (begin
                      (display
                       (ice-9-format:format
                        #f "for n = ~:d, fib(~:d) has ~:d digits~%"
                        first-num first-num dlen))
                      (display
                       (ice-9-format:format
                        #f "  the top 9 digits are ~a, "
                        top-digits))
                      (display
                       (ice-9-format:format
                        #f "the last 9 digits are ~a~%"
                        bottom-digits))
                      ))
                  ))

              (if (equal? debug-flag #t)
                  (begin
                    (display
                     (ice-9-format:format
                      #f "fib(~:d) = ~:d~%" first-num first-fib))
                    ))
              (force-output))
            (begin
              (newline)
              (force-output)
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "The Fibonacci sequence is defined "))
    (display
     (format #f "by the recurrence~%"))
    (display
     (format #f "relation:~%"))
    (newline)
    (display
     (format #f "F(n) = F(n-1) + F(n-2)~%"))
    (display
     (format #f "where F(1) = 1 and F(2) = 1.~%"))
    (newline)
    (display
     (format #f "It turns out that F(541), which "))
    (display
     (format #f "contains 113 digits,~%"))
    (display
     (format #f "is the first Fibonacci number for "))
    (display
     (format #f "which the last nine~%"))
    (display
     (format #f "digits are 1-9 pandigital (contain "))
    (display
     (format #f "all the digits~%"))
    (display
     (format #f "1 to 9, but not necessarily in "))
    (display
     (format #f "order). And F(2749),~%"))
    (display
     (format #f "which contains 575 digits, is the "))
    (display
     (format #f "first Fibonacci number~%"))
    (display
     (format #f "for which the first nine digits "))
    (display
     (format #f "are 1-9 pandigital.~%"))
    (newline)
    (display (format #f "Given that Fk is the first "))
    (display
     (format #f "Fibonacci number for~%"))
    (display
     (format #f "which the first nine digits AND the "))
    (display
     (format #f "last nine digits~%"))
    (display
     (format #f "are 1-9 pandigital, find k.~%"))
    (display
     (format #f "see https://projecteuler.net/problem=104~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((end-num 10)
          (min-num 1000000000)
          (status-num 100)
          (debug-flag #t))
      (begin
        (sub-main-loop
         end-num min-num status-num debug-flag)
        ))

    (newline)
    (force-output)

    (let ((end-num 20000000)
          (min-num 1000000000)
          (status-num 1000000)
          (debug-flag #f))
      (begin
        (sub-main-loop
         end-num min-num status-num debug-flag)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 104 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (load "digits-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

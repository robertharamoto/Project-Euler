#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 109                                    ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated July 28, 2022                                ###
;;;###                                                       ###
;;;###  updated March 11, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (p109-assert-list-lists
         sub-name error-start
         shouldbe-list-list result-list-list
         result-hash-table)
  (begin
    (let ((slen (length shouldbe-list-list))
          (rlen (length result-list-list)))
      (let ((err-1
             (string-append
              error-start
              (format
               #f " : checking length, shouldbe = ~a, "
               slen)
              (format
               #f "result = ~a" rlen))))
        (begin
          (unittest2:assert?
           (equal? slen rlen)
           sub-name
           err-1
           result-hash-table)

          (for-each
           (lambda (slist)
             (begin
               (let ((err-3
                      (format
                       #f ", shouldbe element=~a, result=~a"
                       slist result-list-list)))
                 (begin
                   (unittest2:assert?
                    (not
                     (equal?
                      (member slist result-list-list)
                      #f))
                    sub-name
                    (string-append
                     error-start err-3)
                    result-hash-table)
                   ))
               )) shouldbe-list-list)
          )))
    ))

;;;#############################################################
;;;#############################################################
;;; assume each list has max length of 3
(define (are-two-lists-equivalent? a-list-list b-list-list)
  (begin
    (let ((a-last-elem (car (last-pair a-list-list)))
          (b-last-elem (car (last-pair b-list-list)))
          (a-len (length a-list-list))
          (b-len (length b-list-list)))
      (begin
        (if (or (not (equal? a-last-elem b-last-elem))
                (not (equal? a-len b-len)))
            (begin
              #f)
            (begin
              (if (and (= a-len b-len)
                       (equal? a-last-elem b-last-elem))
                  (begin
                    (cond
                     ((= a-len 1)
                      (begin
                        #t
                        ))
                     ((= a-len 2)
                      (begin
                        (let ((a0-elem (list-ref a-list-list 0))
                              (b0-elem (list-ref b-list-list 0)))
                          (begin
                            (if (equal? a0-elem b0-elem)
                                (begin
                                  #t)
                                (begin
                                  #f
                                  ))
                            ))
                        ))
                     ((= a-len 3)
                      (begin
                        (let ((l1 (sort a-list-list string-ci<?))
                              (l2 (sort b-list-list string-ci<?)))
                          (begin
                            (equal? l1 l2)
                            ))
                        ))
                     (else
                      (begin
                        #f
                        ))
                     ))
                  (begin
                    #f
                    ))
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-are-two-lists-equivalent-1 result-hash-table)
 (begin
   (let ((sub-name "test-are-two-lists-equivalent-1")
         (test-list
          (list
           (list (list "d2" "d1")
                 (list "d1" "d2")
                 #f)
           (list (list "s1" "s2" "d3")
                 (list "s2" "s1" "d3")
                 #t)
           (list (list "t1" "s2" "d3")
                 (list "s2" "t1" "d3")
                 #t)
           (list (list "s2" "t1" "d2")
                 (list "s2" "t2" "d3")
                 #f)
           (list (list "s4" "t1" "d3")
                 (list "s2" "t1" "d3")
                 #f)
           (list (list "d1" "d1" "d1")
                 (list "s2" "d1" "d1")
                 #f)
           (list (list "d1" "s2" "d1")
                 (list "s2" "d1" "d1")
                 #t)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((a-list (list-ref this-list 0))
                  (b-list (list-ref this-list 1))
                  (shouldbe (list-ref this-list 2)))
              (let ((result
                     (are-two-lists-equivalent? a-list b-list)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "a-list=~a, b-list=~a, "
                        a-list b-list))
                      (err-3
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append
                      err-1 err-2 err-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (is-result-in-list? result-list acc-list)
  (begin
    (let ((in-list-flag #f)
          (alen (length acc-list)))
      (begin
        (do ((ii 0 (1+ ii)))
            ((or (>= ii alen)
                 (equal? in-list-flag #t)))
          (begin
            (let ((a-list (list-ref acc-list ii)))
              (begin
                (if (are-two-lists-equivalent?
                     result-list a-list)
                    (begin
                      (set! in-list-flag #t)
                      ))
                ))
            ))

        in-list-flag
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-is-result-in-list-1 result-hash-table)
 (begin
   (let ((sub-name "test-is-result-in-list-1")
         (test-list
          (list
           (list (list "s1" "s2" "d3")
                 (list (list "s2" "s1" "d3"))
                 #t)
           (list (list "t1" "s1" "d3")
                 (list (list "t1" "s1" "d3"))
                 #t)
           (list (list "s1" "s2" "d2")
                 (list (list "t1" "s1" "d3"))
                 #f)
           (list (list "s4" "t1" "d3")
                 (list (list "s2" "s1" "d3"))
                 #f)
           (list (list "d1" "d1" "d1")
                 (list (list "s2" "d1" "d1"))
                 #f)
           (list (list "d1" "d1" "d1")
                 (list (list "s2" "d1" "d1")
                       (list "s3" "d1" "d1"))
                 #f)
           (list (list "d1" "d1" "d1")
                 (list (list "s2" "d1" "d1")
                       (list "s3" "d1" "d1")
                       (list "d1" "d1" "d1"))
                 #t)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((a-list (list-ref this-list 0))
                  (b-list (list-ref this-list 1))
                  (shouldbe (list-ref this-list 2)))
              (let ((result
                     (is-result-in-list? a-list b-list)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "a-list=~a, b-list=~a, "
                        a-list b-list))
                      (err-3
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append
                      err-1 err-2 err-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; at most 3 elements for a darts check-out
(define (combine-lists a-list-list a-len b-list-list b-len)
  (define (local-list-not-in-list? ltmp acc-list-list)
    (begin
      (if (equal? (member ltmp acc-list-list) #f)
          (begin
            #t)
          (begin
            #f
            ))
      ))
  (begin
    (let ((acc-list-list (list)))
      (begin
        (do ((ii 0 (1+ ii)))
            ((>= ii a-len))
          (begin
            (let ((sub-a-list
                   (list-ref a-list-list ii)))
              (let ((sub-a-len (length sub-a-list)))
                (begin
                  (if (and (< sub-a-len 3)
                           (> sub-a-len 0))
                      (begin
                        (do ((jj 0 (1+ jj)))
                            ((>= jj b-len))
                          (begin
                            (let ((sub-b-list
                                   (list-ref b-list-list jj)))
                              (let ((sub-b-len
                                     (length sub-b-list)))
                                (begin
                                  (if (<= (+ sub-a-len sub-b-len) 3)
                                      (begin
                                        (let ((ltmp
                                               (sort
                                                (append
                                                 sub-b-list sub-a-list)
                                                string-ci<?)))
                                          (begin
                                            (if (local-list-not-in-list?
                                                 ltmp acc-list-list)
                                                (begin
                                                  (set!
                                                   acc-list-list
                                                   (cons
                                                    ltmp acc-list-list))
                                                  ))
                                            ))
                                        ))
                                  )))
                            ))
                        ))
                  )))
            ))
        acc-list-list
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-combine-lists-1 result-hash-table)
 (begin
   (let ((sub-name "test-combine-lists-1")
         (test-list
          (list
           (list (list (list "s1")) (list (list "d1"))
                 (list (list "s1" "d1")))
           (list (list (list "d1")) (list (list "s1"))
                 (list (list "s1" "d1")))
           (list (list (list "s1" "d1")) (list (list "s1"))
                 (list (list "s1" "s1" "d1")))
           (list (list (list "d2") (list "s4") (list "s2" "d1")
                       (list "d1" "d1"))
                 (list (list "s1" "d1") (list "s1" "s2"))
                 (list (list "d2" "s1" "d1") (list "d2" "s1" "s2")
                       (list "s4" "s1" "d1") (list "s4" "s1" "s2")))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((a-list-list (list-ref this-list 0))
                  (b-list-list (list-ref this-list 1))
                  (shouldbe-list-list (list-ref this-list 2)))
              (let ((alen (length a-list-list))
                    (blen (length b-list-list)))
                (let ((result-list-list
                       (combine-lists a-list-list alen
                                      b-list-list blen)))
                  (let ((sresult-list-list
                         (map (lambda (alist)
                                (begin
                                  (sort alist string-ci<?)
                                  )) result-list-list))
                        (sshouldbe-list-list
                         (map (lambda (alist)
                                (begin
                                  (sort alist string-ci<?)
                                  )) shouldbe-list-list)))
                    (let ((err-1
                           (format
                            #f "~a : error (~a) : "
                            sub-name test-label-index))
                          (err-2
                           (format
                            #f "a-list-list=~a, b-list-list=~a, "
                            shouldbe-list-list result-list-list)))
                      (begin
                        (p109-assert-list-lists
                         sub-name
                         (string-append err-1 err-2)
                         sshouldbe-list-list sresult-list-list
                         result-hash-table)
                        ))
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; makes lists independent of order, so (d1 d2) is the same as (d2 d1)
;;; later, at tally time, each d in the list will count as 1 configuration
;;; so (d1 d2) will count as 2, and (d1 d2 d3) will count as 3.
(define (dynamic-checkout-to-array
         min-score max-score dart-scores-htable)
  (begin
    (let ((score-list-array
           (make-array (list) (1+ max-score))))
      (begin
        ;;; initialize array
        (hash-for-each
         (lambda (str-elem svalue)
           (begin
             (if (<= svalue max-score)
                 (begin
                   (let ((this-list
                          (array-ref score-list-array svalue)))
                     (begin
                       (array-set!
                        score-list-array
                        (cons (list str-elem) this-list) svalue)
                       ))
                   ))
             )) dart-scores-htable)

        (do ((ii 2 (1+ ii)))
            ((> ii max-score))
          (begin
            (let ((ii-list (array-ref score-list-array ii))
                  (max-jj (euclidean/ ii 2)))
              (let ((ii-len (length ii-list))
                    (ii-acc-list (list)))
                (begin
                  (do ((jj 1 (1+ jj)))
                      ((> jj max-jj))
                    (begin
                      (let ((dvalue (- ii jj)))
                        (let ((jj-list
                               (array-ref score-list-array jj))
                              (dd-list
                               (array-ref score-list-array dvalue)))
                          (let ((jj-len (length jj-list))
                                (dd-len (length dd-list)))
                            (let ((combined-list
                                   (combine-lists
                                    jj-list jj-len dd-list dd-len)))
                              (begin
                                (set!
                                 ii-acc-list
                                 (append combined-list ii-acc-list))
                                )))
                          ))
                      ))

                  (let ((next-acc-list (list-copy ii-list)))
                    (begin
                      (for-each
                       (lambda (a-list)
                         (begin
                           (if (equal?
                                (member a-list next-acc-list)
                                #f)
                               (begin
                                 (set!
                                  next-acc-list
                                  (cons a-list next-acc-list))
                                 ))
                           )) ii-acc-list)

                      (array-set! score-list-array next-acc-list ii)
                      ))
                  )))
            ))

        score-list-array
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-dynamic-checkout-to-array-1 result-hash-table)
 (begin
   (let ((sub-name "test-dynamic-checkout-to-array-1")
         (test-list
          (list
           (list 2 6 1 (list (list "s1")))
           (list 2 6 2 (list (list "s1" "s1")
                             (list "d1") (list "s2")))
           (list 2 6 3 (list (list "s1" "s1" "s1")
                             (list "s1" "d1") (list "s1" "s2")
                             (list "s3") (list "t1")))
           (list 2 6 4 (list (list "s1" "s1" "d1")
                             (list "s1" "s1" "s2")
                             (list "s2" "s2")
                             (list "s2" "d1")
                             (list "d1" "d1")
                             (list "s1" "t1")
                             (list "s1" "s3")
                             (list "d2") (list "s4")))
           ))
         (dart-scores-htable (make-hash-table))
         (test-label-index 0))
     (begin
       (populate-dart-score-hash-table! dart-scores-htable)

       (for-each
        (lambda (this-list)
          (begin
            (let ((min-score (list-ref this-list 0))
                  (max-score (list-ref this-list 1))
                  (ii (list-ref this-list 2))
                  (shouldbe-list-list (list-ref this-list 3)))
              (let ((result-list-list
                     (list-ref
                      (array->list
                       (dynamic-checkout-to-array
                        min-score max-score dart-scores-htable))
                      ii)))
                (let ((sresult-list-list
                       (map
                        (lambda (alist)
                          (sort alist string-ci<?))
                        result-list-list))
                      (sshouldbe-list-list
                       (map (lambda (alist)
                              (sort alist string-ci<?))
                            shouldbe-list-list)))
                  (let ((err-1
                         (format
                          #f "~a : error (~a) : "
                          sub-name test-label-index))
                        (err-2
                         (format
                          #f "min-score=~a, max-score=~a, "
                          min-score max-score)))
                    (begin
                      (p109-assert-list-lists
                       sub-name
                       (string-append err-1 err-2)
                       sshouldbe-list-list sresult-list-list
                       result-hash-table)
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define-syntax down-one-more-level
  (syntax-rules ()
    ((down-one-more-level
      fixed-sum current-sum current-list
      sorted-input-list input-list-length dart-scores-htable
      acc-list-list inner-loop)
     (begin
       (let ((clen (length current-list))
             (continue-loop-flag #t))
         (begin
           (if (> clen 3)
               (begin
                 (set! continue-loop-flag #f)
                 ))

           (do ((ii 0 (1+ ii)))
               ((or (>= ii input-list-length)
                    (equal? continue-loop-flag #f)))
             (begin
               (let ((ii-label
                      (list-ref sorted-input-list ii)))
                 (let ((ii-value
                        (hash-ref dart-scores-htable ii-label)))
                   (let ((next-current-sum
                          (+ ii-value current-sum)))
                     (begin
                       (if (<= next-current-sum fixed-sum)
                           (begin
                             (let ((next-current-list
                                    (cons ii-label current-list)))
                               (let ((next-acc-list-list
                                      (inner-loop
                                       next-current-sum fixed-sum
                                       sorted-input-list input-list-length
                                       dart-scores-htable
                                       next-current-list acc-list-list)))
                                 (begin
                                   (set! acc-list-list next-acc-list-list)
                                   ))
                               ))
                           (begin
                             (set! continue-loop-flag #f)
                             ))
                       ))
                   ))
               ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
;;; sorted-input-list sorted by dart-scores (see main-loop)
(define (make-checkout-list-fixed-sum
         sorted-input-list dart-scores-htable fixed-sum)
  (define (inner-loop
           current-sum fixed-sum
           sorted-input-list input-list-length
           dart-scores-htable
           current-list acc-list-list)
    (begin
      (cond
       ((= current-sum fixed-sum)
        (begin
          (let ((last-elem
                 (car (last-pair current-list))))
            (begin
              (if (and (<= (length current-list) 3)
                       (equal?
                        (is-result-in-list?
                         current-list acc-list-list)
                        #f))
                  (begin
                    (cons current-list acc-list-list))
                  (begin
                    acc-list-list
                    ))
              ))
          ))
       ((> current-sum fixed-sum)
        (begin
          acc-list-list
          ))
       (else
        (begin
          (down-one-more-level
           fixed-sum current-sum current-list
           sorted-input-list input-list-length
           dart-scores-htable
           acc-list-list inner-loop)

          acc-list-list
          )))
      ))
  (begin
    (let ((input-list-length
           (length sorted-input-list))
          (acc-list (list)))
      (begin
        (for-each
         (lambda (str-elem)
           (begin
             (if (string-ci= str-elem "d" 0 1)
                 (begin
                   ;;; guarantee that the last throw checks out with a double
                   (let ((current-sum
                          (hash-ref
                           dart-scores-htable str-elem 0)))
                     (let ((next-acc-list
                            (inner-loop
                             current-sum fixed-sum
                             sorted-input-list input-list-length
                             dart-scores-htable
                             (list str-elem) acc-list)))
                       (begin
                         (set! acc-list next-acc-list)
                         )))
                   ))
             )) sorted-input-list)

        acc-list
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-make-checkout-list-fixed-sum-1 result-hash-table)
 (begin
   (let ((sub-name "test-make-checkout-list-fixed-sum-1")
         (test-list
          (list
           (list (list "d3") 6 (list (list "d3")))
           (list (list "d1" "d2" "d3")
                 6 (list (list "d3")
                         (list "d2" "d1")
                         (list "d1" "d2")
                         (list "d1" "d1" "d1")))
           (list (list "s1" "d2" "d3")
                 6 (list (list "d3")
                         (list "s1" "s1" "d2")))
           ))
         (dart-scores-htable (make-hash-table))
         (test-label-index 0))
     (begin
       (populate-dart-score-hash-table! dart-scores-htable)

       (for-each
        (lambda (this-list)
          (begin
            (let ((input-list (list-ref this-list 0))
                  (fixed-sum (list-ref this-list 1))
                  (shouldbe-list-list (list-ref this-list 2)))
              (let ((result-list-list
                     (make-checkout-list-fixed-sum
                      input-list dart-scores-htable fixed-sum)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "input-list=~a, fixed-sum=~a, "
                        input-list fixed-sum)))
                  (begin
                    (p109-assert-list-lists
                     sub-name
                     (string-append err-1 err-2)
                     shouldbe-list-list result-list-list
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (populate-dart-score-hash-table! dart-scores-htable)
  (begin
    (let ((scores-list-list
           (list
            (list "s1" 1) (list "s2" 2) (list "s3" 3)
            (list "s4" 4) (list "s5" 5) (list "s6" 6)
            (list "s7" 7) (list "s8" 8) (list "s9" 9)
            (list "s10" 10) (list "s11" 11) (list "s12" 12)
            (list "s13" 13) (list "s14" 14) (list "s15" 15)
            (list "s16" 16) (list "s17" 17) (list "s18" 18)
            (list "s19" 19) (list "s20" 20)
            (list "d1" 2) (list "d2" 4) (list "d3" 6)
            (list "d4" 8) (list "d5" 10) (list "d6" 12)
            (list "d7" 14) (list "d8" 16) (list "d9" 18)
            (list "d10" 20) (list "d11" 22) (list "d12" 24)
            (list "d13" 26) (list "d14" 28) (list "d15" 30)
            (list "d16" 32) (list "d17" 34) (list "d18" 36)
            (list "d19" 38) (list "d20" 40)
            (list "t1" 3) (list "t2" 6) (list "t3" 9)
            (list "t4" 12) (list "t5" 15) (list "t6" 18)
            (list "t7" 21) (list "t8" 24) (list "t9" 27)
            (list "t10" 30) (list "t11" 33) (list "t12" 36)
            (list "t13" 39) (list "t14" 42) (list "t15" 45)
            (list "t16" 48) (list "t17" 51) (list "t18" 54)
            (list "t19" 57) (list "t20" 60)
            (list "bull" 25) (list "dbull" 50)
            )))
      (begin
        (for-each
         (lambda (alist)
           (begin
             (let ((astring (list-ref alist 0))
                   (avalue (list-ref alist 1)))
               (begin
                 (hash-set! dart-scores-htable astring avalue)
                 ))
             )) scores-list-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (sort-out-list-list out-list-list)
  (begin
    (let ((sorted-list-list
           (sort
            out-list-list
            (lambda (a b)
              (begin
                (let ((alen (length a))
                      (acar (car (last-pair a)))
                      (blen (length b))
                      (bcar (car (last-pair b))))
                  (begin
                    (cond
                     ((= alen blen)
                      (begin
                        (string-ci>? acar bcar)
                        ))
                     (else
                      (begin
                        (< alen blen)
                        )))
                    ))
                )))
           ))
      (begin
        sorted-list-list
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop min-score max-score debug-flag)
  (begin
    (let ((dart-scores-htable (make-hash-table))
          (dart-labels-list (list)))
      (begin
        (populate-dart-score-hash-table! dart-scores-htable)

        (hash-for-each
         (lambda (dstring dvalue)
           (begin
             (set!
              dart-labels-list
              (cons dstring dart-labels-list))
             )) dart-scores-htable)

        (let ((score-sum 0)
              (acc-list-list (list))
              (sorted-dart-labels-list
               (sort
                dart-labels-list
                (lambda (astring bstring)
                  (begin
                    (let ((anum
                           (hash-ref
                            dart-scores-htable astring))
                          (bnum
                           (hash-ref
                            dart-scores-htable bstring)))
                      (begin
                        (< anum bnum)
                        ))
                    )))
               ))
          (begin
            (do ((ii min-score (1+ ii)))
                ((> ii max-score))
              (begin
                (let ((out-list-list
                       (make-checkout-list-fixed-sum
                        sorted-dart-labels-list
                        dart-scores-htable ii)))
                  (let ((out-len (length out-list-list)))
                    (begin
                      (set!
                       acc-list-list
                       (append out-list-list acc-list-list))
                      (set!
                       score-sum (+ score-sum out-len))

                      (if (equal? debug-flag #t)
                          (begin
                            (display
                             (ice-9-format:format
                              #f "score ~:d : ~:d ways : "
                              out-len ii))
                            (display
                             (ice-9-format:format
                              #f "sum so far = ~:d~%"
                              score-sum))
                            (force-output)
                            ))
                      )))
                ))

            (if (equal? debug-flag #t)
                (begin
                  (let ((sorted-list-list
                         (sort-out-list-list acc-list-list)))
                    (begin
                      (for-each
                       (lambda (a-list)
                         (begin
                           (display (format #f "    ~a~%" a-list))
                           )) sorted-list-list)
                      (force-output)
                      ))
                  ))
            (display
             (ice-9-format:format
              #f "~:d = number of ways to checkout with "
              score-sum))
            (display
             (ice-9-format:format
              #f "score less than ~:d~%" max-score))
            (force-output)
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (dynamic-main-loop min-score max-score debug-flag)
  (define (local-count-ds a-sub-list count-htable)
    (begin
      (let ((count 0))
        (begin
          (hash-clear! count-htable)
          ;;; increment for every d found
          (for-each
           (lambda (a-str)
             (begin
               (if (string-ci= a-str "d" 0 1)
                   (begin
                     (set! count (1+ count))
                     (let ((hc (hash-ref count-htable a-str 0)))
                       (begin
                         (hash-set! count-htable a-str (1+ hc))
                         ))
                     ))
               )) a-sub-list)

          ;;; if there are multiples of the same d, then subtract
          (hash-for-each
           (lambda (a-str hc)
             (begin
               (cond
                ((= hc 2)
                 (begin
                   (set! count (1- count))
                   ))
                ((= hc 3)
                 (begin
                   (set! count (- count 2))
                   )))
               )) count-htable)

          count
          ))
      ))
  (begin
    (let ((dart-scores-htable (make-hash-table))
          (count-htable (make-hash-table))
          (score-sum 0)
          (acc-list-list (list)))
      (begin
        (populate-dart-score-hash-table! dart-scores-htable)

        (let ((results-array
               (dynamic-checkout-to-array
                min-score max-score dart-scores-htable)))
          (begin
            (do ((ii min-score (1+ ii)))
                ((> ii max-score))
              (begin
                (let ((out-list-list
                       (array-ref results-array ii))
                      (ii-count 0))
                  (begin
                    (for-each
                     (lambda (a-list)
                       (begin
                         (let ((ds-count
                                (local-count-ds a-list count-htable)))
                           (begin
                             (set! score-sum (+ score-sum ds-count))
                             ))
                         )) out-list-list)

                    (if (equal? debug-flag #t)
                        (begin
                          (display
                           (ice-9-format:format
                            #f "score ~:d : ~:d ways : "
                            ii ii-count))
                          (display
                           (ice-9-format:format
                            #f "sum so far = ~:d~%" score-sum))
                          (force-output)
                          ))
                    ))
                ))

            (display
             (ice-9-format:format
              #f "~:d = number of ways to checkout with "
              score-sum))
            (display
             (ice-9-format:format
              #f "a score between ~:d and ~:d~%"
              min-score max-score))
            (force-output)
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "In the game of darts a player "))
    (display
     (format #f "throws three darts~%"))
    (display
     (format #f "at a target board which is split "))
    (display
     (format #f "into twenty equal~%"))
    (display
     (format #f "sized sections numbered one to twenty.~%"))
    (newline)
    (display
     (format #f "The score of a dart is determined "))
    (display
     (format #f "by the number of~%"))
    (display
     (format #f "the region that the dart lands in. "))
    (display
     (format #f "A dart landing~%"))
    (display
     (format #f "outside the red/green outer ring "))
    (display
     (format #f "scores zero. The~%"))
    (display
     (format #f "black and cream regions inside "))
    (display
     (format #f "this ring represent~%"))
    (display
     (format #f "single scores. However, the "))
    (display
     (format #f "red/green outer ring~%"))
    (display
     (format #f "and middle ring score double and "))
    (display
     (format #f "treble scores~%"))
    (display
     (format #f "respectively.~%"))
    (newline)
    (display
     (format #f "At the centre of the board are two "))
    (display
     (format #f "concentric circles~%"))
    (display
     (format #f "called the bull region, or bulls-eye. "))
    (display
     (format #f "The outer bull~%"))
    (display
     (format #f "is worth 25 points and the inner bull "))
    (display
     (format #f "is a double,~%"))
    (display
     (format #f "worth 50 points.~%"))
    (newline)
    (display
     (format #f "There are many variations of rules but "))
    (display
     (format #f "in the most~%"))
    (display
     (format #f "popular game the players will "))
    (display
     (format #f "begin with a score~%"))
    (display
     (format #f "301 or 501 and the first player "))
    (display
     (format #f "to reduce their running~%"))
    (display
     (format #f "total to zero is a winner. However, "))
    (display
     (format #f "it is normal to~%"))
    (display
     (format #f "play a 'doubles out' system, which "))
    (display
     (format #f "means that the~%"))
    (display
     (format #f "player must land a double (including "))
    (display
     (format #f "the double bulls-eye~%"))
    (display
     (format #f "at the centre of the board) on "))
    (display
     (format #f "their final dart~%"))
    (display
     (format #f "to win; any other dart that would "))
    (display
     (format #f "reduce their running~%"))
    (display
     (format #f "total to one or lower means the "))
    (display
     (format #f "score for that~%"))
    (display
     (format #f "set of three darts is 'bust'.~%"))
    (newline)
    (display
     (format #f "When a player is able to finish on "))
    (display
     (format #f "their current score~%"))
    (display
     (format #f "it is called a 'checkout' and the "))
    (display
     (format #f "highest checkout is~%"))
    (display
     (format #f "170: T20 T20 D25 (two treble 20s and "))
    (display
     (format #f "double bull).~%"))
    (newline)
    (display
     (format #f "There are exactly eleven distinct ways "))
    (display
     (format #f "to checkout on~%"))
    (display
     (format #f "a score of 6:~%"))
    (display (format #f "  D3~%"))
    (display (format #f "  D1   D2~%"))
    (display (format #f "  S2   D2~%"))
    (display (format #f "  D2   D1~%"))
    (display (format #f "  S4   D1~%"))
    (display (format #f "  S1   S1      D2~%"))
    (display (format #f "  S1   T1      D1~%"))
    (display (format #f "  S1   S3      D1~%"))
    (display (format #f "  D1   D1      D1~%"))
    (display (format #f "  D1   S2      D1~%"))
    (display (format #f "  S2   S2      D1~%"))
    (newline)
    (display
     (format #f "Note that D1 D2 is considered "))
    (display
     (format #f "different to D2 D1~%"))
    (display
     (format #f "as they finish on different doubles. "))
    (display
     (format #f "However, the~%"))
    (display
     (format #f "combination S1 T1 D1 is considered "))
    (display
     (format #f "the same as~%"))
    (display
     (format #f "T1 S1 D1.~%"))
    (newline)
    (display
     (format #f "In addition we shall not include misses "))
    (display
     (format #f "in considering~%"))
    (display
     (format #f "combinations; for example, D3 is "))
    (display
     (format #f "the same as~%"))
    (display
     (format #f "0 D3 and 0 0 D3.~%"))
    (newline)
    (display
     (format #f "Incredibly there are 42336 distinct "))
    (display
     (format #f "ways of checking out~%"))
    (display
     (format #f "in total.~%"))
    (newline)
    (display
     (format #f "How many distinct ways can a player "))
    (display
     (format #f "checkout with a~%"))
    (display
     (format #f "score less than 100?~%"))
    (newline)
    (display
     (format #f "Dynamic programming was used to "))
    (display
     (format #f "compute the number~%"))
    (display
     (format #f "of ways of checking out in darts. "))
    (display
     (format #f "An array stores valid~%"))
    (display
     (format #f "configurations of dart throws which "))
    (display
     (format #f "add up to a~%"))
    (display
     (format #f "specific number (which equals the "))
    (display
     (format #f "array index). Order~%"))
    (display
     (format #f "of the throws was not preserved, "))
    (display
     (format #f "only at the end~%"))
    (display
     (format #f "during the tally phase, were the "))
    (display
     (format #f "special cases like~%"))
    (display
     (format #f "(D1 D2) =/= (D2 D1) or (D1 D1) = "))
    (display
     (format #f "(D1 D1), were taken~%"))
    (display
     (format #f "into account.~%"))
    (display
     (format #f "see https://projecteuler.net/problem=109~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((min-score 6)
          (max-score 6)
          (debug-flag #t))
      (begin
        ;;; recursive technique
        (sub-main-loop min-score max-score debug-flag)

        ;;; dynamic programming technique
        (dynamic-main-loop min-score max-score debug-flag)
        ))

    (newline)
    (let ((min-score 2)
          (max-score 99)
          (debug-flag #f))
      (begin
        ;;; dynamic programming technique
        (dynamic-main-loop min-score max-score debug-flag)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 109 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 110                                    ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated July 28, 2022                                ###
;;;###                                                       ###
;;;###  updated March 11, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### srfi-1 for fold function
(use-modules ((srfi srfi-1)
              :renamer (symbol-prefix-proc 'srfi-1:)))

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### prime-module for is-prime? function
(use-modules ((prime-module)
              :renamer (symbol-prefix-proc 'prime-module:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
;;; note: number of solutions given by y = n + (n^2)/d
;;; and number of divisors of n^2 is Prod(ak+1), where ak is the
;;; number of powers of prime(k), that divides n^2.
;;; min-num/count - largest number with num divisors less than min-num-solutions
;;; max-num/count - smallest number with num divisors greater than min-num-solutions
(define (make-list-of-divisor-counts prime-list max-powers
                                     min-num-solutions maximum-number
                                     status-num)
  (define (local-loop depth max-depth max-powers min-num-solutions
                      maximum-number sorted-prime-list
                      status-num counter
                      current-product current-list
                      min-num min-count max-num max-count
                      acc-max-digit-list)
    (begin
      (cond
       ((and (>= depth max-depth)
             (<= current-product maximum-number))
        (begin
          (let ((num-divisors
                 (srfi-1:fold
                  (lambda (aa prev)
                    (begin
                      (* (1+ (* aa 2)) prev)
                      ))
                  1 (reverse current-list))))
            (begin
              (if (and (<= num-divisors min-num-solutions)
                       (> num-divisors min-count)
                       (> num-divisors 0)
                       (> current-product min-num))
                  (begin
                    (set! min-num current-product)
                    (set! min-count num-divisors)
                    ))

              (if (>= num-divisors min-num-solutions)
                  (begin
                    (if (or (< max-num 0)
                            (and (<= num-divisors max-count)
                                 (< current-product max-num)
                                 ))
                        (begin
                          (set! max-num current-product)
                          (set! max-count num-divisors)
                          (set! acc-max-digit-list
                                (reverse current-list))
                          ))
                    ))

              (set! counter (1+ counter))
              (if (zero? (modulo counter status-num))
                  (begin
                    (display
                     (ice-9-format:format
                      #f "(~:d) : current-list = ~a~%"
                      counter (reverse current-list)))
                    (display
                     (ice-9-format:format
                      #f "(min num/count = ~:d / ~:d : "
                      min-num (euclidean/ min-count 2)))
                    (display
                     (ice-9-format:format
                      #f "max num/count = ~:d / ~:d~%"
                      max-num (euclidean/ max-count 2)))
                    (display
                     (ice-9-format:format
                      #f "digit-list = ~a : "
                      acc-max-digit-list))
                    (display
                     (format
                      #f "~a~%"
                      (timer-module:current-date-time-string)))
                    (force-output)
                    ))

              (list counter
                    min-num min-count max-num max-count
                    acc-max-digit-list)
              ))
          ))
       (else
        (begin
          (let ((c-prime (list-ref sorted-prime-list depth)))
            (let ((c-prod 1))
              (begin
                (do ((ii 0 (1+ ii)))
                    ((> ii max-powers))
                  (begin
                    (let ((next-product (* current-product c-prod))
                          (next-current-list (cons ii current-list)))
                      (begin
                        (if (<= next-product maximum-number)
                            (begin
                              (let ((next-list-list
                                     (local-loop (1+ depth) max-depth
                                                 ii min-num-solutions
                                                 maximum-number sorted-prime-list
                                                 status-num counter
                                                 next-product next-current-list
                                                 min-num min-count max-num max-count
                                                 acc-max-digit-list)))
                                (let ((next-counter (list-ref next-list-list 0))
                                      (next-min-num (list-ref next-list-list 1))
                                      (next-min-count (list-ref next-list-list 2))
                                      (next-max-num (list-ref next-list-list 3))
                                      (next-max-count (list-ref next-list-list 4))
                                      (next-acc-max-list (list-ref next-list-list 5)))
                                  (begin
                                    (set! counter next-counter)
                                    (set! min-num next-min-num)
                                    (set! min-count next-min-count)
                                    (set! max-num next-max-num)
                                    (set! max-count next-max-count)
                                    (set! acc-max-digit-list next-acc-max-list)
                                    )))
                              ))
                        ))
                    (set! c-prod (* c-prod c-prime))
                    ))
                )))

          (list counter
                min-num min-count max-num max-count
                acc-max-digit-list)
          )))
      ))
  (begin
    (let ((max-depth (length prime-list))
          (counter 0)
          (min-square (* 2 min-num-solutions))
          (sprime-list (sort prime-list <)))
      (let ((final-list
             (local-loop 0 max-depth max-powers
                         min-square maximum-number sprime-list
                         status-num counter
                         1 (list) -1 -1 -1 -1 (list))))
        (let ((next-counter (list-ref final-list 0))
              (next-min-num (list-ref final-list 1))
              (next-min-count (list-ref final-list 2))
              (next-max-num (list-ref final-list 3))
              (next-max-count (list-ref final-list 4))
              (next-acc-max-list (list-ref final-list 5)))
          (let ((ff-list
                 (list next-min-num next-min-count
                       next-max-num next-max-count
                       next-acc-max-list)))
            (begin
              ff-list
              )))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-make-list-of-divisor-counts-1 result-hash-table)
 (begin
   (let ((sub-name "test-make-list-of-divisor-counts-1")
         (test-list
          (list
           (list (list 2) 2 4 (list 4 5 -1 -1 (list)))
           (list (list 2 3) 4 2 (list 2 3 4 5 (list 2 0)))
           (list (list 2 3 5 7) 3 4 (list 8 7 6 9 (list 1 1 0 0)))
           ))
         (status-num 1000)
         (maximum-number 1000000)
         (test-label-index 0))
     (begin
       (for-each
        (lambda (a-list)
          (begin
            (let ((prime-list (list-ref a-list 0))
                  (max-powers (list-ref a-list 1))
                  (min-num-solutions (list-ref a-list 2))
                  (shouldbe-list (list-ref a-list 3)))
              (let ((result-list
                     (make-list-of-divisor-counts
                      prime-list max-powers min-num-solutions
                      maximum-number status-num)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) :  "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "prime-list=~a, max-powers=~a, "
                        prime-list max-powers))
                      (err-3
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe-list result-list)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-list result-list)
                     sub-name
                     (string-append
                      err-1 err-2 err-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (prime-powers-to-string prime-list power-list)
  (begin
    (let ((p-len (length prime-list))
          (pow-len (length power-list))
          (pstr-list (list)))
      (begin
        (if (equal? p-len pow-len)
            (begin
              (do ((ii 0 (1+ ii)))
                  ((>= ii p-len))
                (begin
                  (let ((a-prime (list-ref prime-list ii))
                        (a-exp (list-ref power-list ii)))
                    (begin
                      (cond
                       ((= a-exp 0)
                        (begin
                          (display
                           (format
                            #f "debug prime-powers-to-string prime-list=~a~%"
                            prime-list))
                          (display
                           (format
                            #f "  power-list=~a, ii=~a, a-prime=~a, a-exp=~a~%"
                            power-list ii a-prime a-exp))
                          (force-output)
                          ))
                       ((= a-exp 1)
                        (begin
                          (let ((t-string
                                 (ice-9-format:format
                                  #f "~:d"
                                  (list-ref prime-list ii)
                                  (list-ref power-list ii))))
                            (begin
                              (set! pstr-list (cons t-string pstr-list))
                              ))
                          ))
                       ((> a-exp 1)
                        (begin
                          (let ((t-string
                                 (ice-9-format:format
                                  #f "~:d^~:d"
                                  (list-ref prime-list ii)
                                  (list-ref power-list ii))))
                            (begin
                              (set! pstr-list (cons t-string pstr-list))
                              ))
                          )))
                      ))
                  ))

              (let ((fstr
                     (string-join (reverse pstr-list) "x")))
                (begin
                  fstr
                  )))
            (begin
              #f
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop
         prime-list max-powers min-number-solutions
         maximum-number status-num)
  (begin
    (let ((results-list-list
           (make-list-of-divisor-counts
            prime-list max-powers
            min-number-solutions
            maximum-number status-num)))
      (let ((min-num (list-ref results-list-list 0))
            (min-count (list-ref results-list-list 1))
            (max-num (list-ref results-list-list 2))
            (max-count (list-ref results-list-list 3))
            (acc-max-digit-list (list-ref results-list-list 4)))
        (let ((factor-string
               (prime-powers-to-string
                prime-list acc-max-digit-list)))
          (begin
            (newline)
            (display
             (ice-9-format:format
              #f "n = ~:d = ~a~%"
              max-num factor-string))
            (display
             (ice-9-format:format
              #f "  number of solutions = ~:d~%"
              (euclidean/ max-count 2)))
            (display
             (ice-9-format:format
              #f "  (exceeds ~:d solutions)~%"
              min-number-solutions))
            (display
             (ice-9-format:format
              #f "  previous n = ~:d~%"
              min-num))
            (display
             (ice-9-format:format
              #f "  number of solutions = ~:d~%"
              (euclidean/ min-count 2)))
            (display
             (ice-9-format:format
              #f "  (less than ~:d solutions)~%"
              min-number-solutions))
            (newline)
            (force-output)
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "In the following equation x, y, "))
    (display
     (format #f "and n are~%"))
    (display
     (format #f "positive integers.~%"))
    (newline)
    (display
     (format #f "1/x + 1/y = 1/n~%"))
    (newline)
    (display
     (format #f "It can be verified that when "))
    (display
     (format #f "n = 1260 there~%"))
    (display
     (format #f "are 113 distinct solutions and "))
    (display
     (format #f "this is the least~%"))
    (display
     (format #f "value of n for which the total "))
    (display
     (format #f "number of distinct~%"))
    (display
     (format #f "solutions exceeds one hundred.~%"))
    (newline)
    (display
     (format #f "What is the least value of n for "))
    (display
     (format #f "which the number of~%"))
    (display
     (format #f "distinct solutions exceeds "))
    (display
     (format #f "four million?~%"))
    (newline)
    (display
     (format #f "NOTE: This problem is a much more "))
    (display
     (format #f "difficult version of~%"))
    (display
     (format #f "problem 108 and as it is well beyond "))
    (display
     (format #f "the limitations of a~%"))
    (display
     (format #f "brute force approach it requires a "))
    (display
     (format #f "clever implementation.~%"))
    (newline)
    (display
     (format #f "The solution was found at~%"))
    (display
     (format #f "https://euler.stephan-brumme.com/110/~%"))
    (newline)
    (display
     (format #f "Just like problem 108, "))
    (display
     (format #f "1/x + 1/y = 1/n~%"))
    (display
     (format #f "which is equivalent to "))
    (display
     (format #f "n * (x + y) = x * y.~%"))
    (display
     (format #f "with d = x - n, or x = d + n, "))
    (display
     (format #f "and n < x <= 2n~%"))
    (newline)
    (display
     (format #f "y = n + (n^2)/d, must find all "))
    (display
     (format #f "divisors d of n^2,~%"))
    (display
     (format #f "so the key is to find an n such "))
    (display
     (format #f "that n^2 has~%"))
    (display
     (format #f "4 million divisors~%"))
    (newline)
    (display
     (format #f "The key idea is to construct the "))
    (display
     (format #f "smallest composite number~%"))
    (display
     (format #f "that are made up of primes,~%"))
    (display
     (format #f "p1^(a1)*p2^(a2)*...*pn^(an) "))
    (display
     (format #f "where~%"))
    (display
     (format #f "p1>p2>p3..., and a1>=a2>=a3... "))
    (display
     (format #f "If ai<aj when i<j,~%"))
    (display
     (format #f "p1^(a1)*p2^(a2)*...*pai^(ai)*..."))
    (display
     (format #f "*paj^(aj)*...*pn^(an)~%"))
    (display
     (format #f "is bigger than p1^(a1)*p2^(a2)*..."))
    (display
     (format #f "*pai^(aj)*...*paj^(ai)*~%"))
    (display
     (format #f "...*pn^(an), but still has the "))
    (display
     (format #f "same number of~%"))
    (display
     (format #f "divisors. Once a configuration of "))
    (display
     (format #f "exponents was found,~%"))
    (display
     (format #f "the divisor function can be "))
    (display
     (format #f "used to calculate~%"))
    (display
     (format #f "the sum of divisors, "))
    (display
     (format #f "(see the wikipedia article for more "))
    (display
     (format #f "details~%"))
    (display
     (format #f "https://en.wikipedia.org/wiki/Divisor_function)~%"))
    (display
     (format #f "see https://projecteuler.net/problem=110~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((prime-list (list 2 3))
          (max-power 2)
          (min-number-solutions 4)
          (maximum-number (inexact->exact 10e18))
          (status-num 1000))
      (begin
        (sub-main-loop
         prime-list max-power min-number-solutions
         maximum-number status-num)
        ))

    (newline)
    (force-output)

    ;;; 614,889,782,588,491,410 = Product(list 2 3 5 7 11 13 17 19 23 29 31 37 41 43 47)
    (let ((prime-list (list 2 3 5 7 11 13 17 19 23 29 31 37))
          (max-powers 3)
          (min-number-solutions 4000000)
          (maximum-number (inexact->exact 10e18))
          (status-num 30000000))
      (begin
        (sub-main-loop
         prime-list max-powers min-number-solutions
         maximum-number status-num)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 110 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

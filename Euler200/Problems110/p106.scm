#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 106                                    ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated July 28, 2022                                ###
;;;###                                                       ###
;;;###  updated March 11, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (are-lists-disjoint? llist1 llist2)
  (begin
    (let ((ok-flag #t))
      (begin
        (for-each
         (lambda (elem1)
           (begin
             (if (not
                  (equal?
                   (member elem1 llist2)
                   #f))
                 (begin
                   (set! ok-flag #f)
                   ))
             )) llist1)
        ok-flag
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-are-lists-disjoint-1 result-hash-table)
 (begin
   (let ((sub-name "test-are-lists-disjoint-1")
         (test-list
          (list
           (list (list 1) (list 2) #t)
           (list (list 1 2) (list 3 4) #t)
           (list (list 1 2 3) (list 4 5 6) #t)
           (list (list 1 2 3) (list 4 5 6 3) #f)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((set-1-list (list-ref this-list 0))
                  (set-2-list (list-ref this-list 1))
                  (shouldbe (list-ref this-list 2)))
              (let ((result
                     (are-lists-disjoint? set-1-list set-2-list)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "set-1-list=~a, set-2-list=~a, "
                        set-1-list set-2-list))
                      (err-3
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append
                      err-1 err-2 err-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (are-subset-pairs-dominant? subset-1-list subset-2-list)
  (define (local-l1-greater-l2 llist1 llist2 llen1)
    (begin
      (let ((ok-flag #t)
            (loop-continue-flag #t))
        (begin
          (do ((ii 0 (1+ ii)))
              ((or (>= ii llen1)
                   (equal? loop-continue-flag #f)))
            (begin
              (let ((elem1 (list-ref llist1 ii))
                    (elem2 (list-ref llist2 ii)))
                (begin
                  (if (<= elem1 elem2)
                      (begin
                        (set! ok-flag #f)
                        (set! loop-continue-flag #f)
                        ))
                  ))
              ))
          ok-flag
          ))
      ))
  (begin
    (let ((llen1 (length subset-1-list))
          (llen2 (length subset-2-list)))
      (begin
        (if (equal? llen1 llen2)
            (begin
              (let ((elem1 (car subset-1-list))
                    (elem2 (car subset-2-list)))
                (begin
                  (if (>= elem1 elem2)
                      (begin
                        (local-l1-greater-l2
                         subset-1-list subset-2-list llen1))
                      (begin
                        (local-l1-greater-l2
                         subset-2-list subset-1-list llen1)
                        ))
                  )))
            (begin
              #f
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-are-subset-pairs-dominant-1 result-hash-table)
 (begin
   (let ((sub-name "test-are-subset-pairs-dominant-1")
         (test-list
          (list
           (list (list 1 2) (list 3 4) #t)
           (list (list 3 4) (list 1 2) #t)
           (list (list 1 3) (list 2 4) #t)
           (list (list 2 4) (list 1 3) #t)
           (list (list 2 3) (list 1 4) #f)
           (list (list 1 4) (list 2 3) #f)
           (list (list 1 2 3) (list 4 5 6) #t)
           (list (list 4 5 6) (list 1 2 3) #t)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((subset1 (list-ref this-list 0))
                  (subset2 (list-ref this-list 1))
                  (shouldbe (list-ref this-list 2)))
              (let ((result
                     (are-subset-pairs-dominant?
                      subset1 subset2)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "subset1=~a, subset2=~a, "
                        subset1 subset2))
                      (err-3
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append
                      err-1 err-2 err-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (make-power-set start-list)
  (define (local-loop
           depth max-depth start-index end-index start-list
           current-list acc-list)
    (begin
      (cond
       ((>= depth max-depth)
        (begin
          (let ((sorted-list (sort current-list <)))
            (begin
              (cons sorted-list acc-list)
              ))
          ))
       (else
        (begin
          (do ((ii start-index (1+ ii)))
              ((>= ii end-index))
            (begin
              (let ((anum (list-ref start-list ii)))
                (let ((next-current-list
                       (cons anum current-list)))
                  (let ((next-acc-list
                         (local-loop
                          (1+ depth) max-depth (1+ ii)
                          end-index start-list
                          next-current-list acc-list)))
                    (begin
                      (set! acc-list next-acc-list)
                      ))
                  ))
              ))

          acc-list
          )))
      ))
  (begin
    (let ((result-list-list (list))
          (slen (length start-list)))
      (begin
        (do ((ii 1 (1+ ii)))
            ((> ii slen))
          (begin
            (let ((rr-list-list
                   (local-loop
                    0 ii 0 slen start-list
                    (list) (list))))
              (begin
                (set!
                 result-list-list
                 (append result-list-list rr-list-list))
                ))
            ))

        result-list-list
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-make-power-set-1 result-hash-table)
 (begin
   (let ((sub-name "test-make-power-set-1")
         (test-list
          (list
           (list (list 1 2)
                 (list (list 1) (list 2) (list 1 2)))
           (list (list 1 2 3)
                 (list (list 1) (list 2) (list 3)
                       (list 1 2) (list 1 3) (list 2 3)
                       (list 1 2 3)))
           (list (list 1 2 3 4)
                 (list (list 1) (list 2) (list 3) (list 4)
                       (list 1 2) (list 1 3) (list 1 4)
                       (list 2 3) (list 2 4) (list 3 4)
                       (list 1 2 3) (list 1 2 4) (list 1 3 4)
                       (list 2 3 4) (list 1 2 3 4)))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((start-set (list-ref this-list 0))
                  (shouldbe-list-list (list-ref this-list 1)))
              (let ((result-list-list
                     (make-power-set start-set)))
                (let ((slen (length shouldbe-list-list))
                      (rlen (length result-list-list)))
                  (let ((err-1
                         (format
                          #f "~a : error (~a) : start-set=~a, "
                          sub-name test-label-index start-set))
                        (err-2
                         (format
                          #f "shouldbe length=~a, result length=~a"
                          slen rlen)))
                    (begin
                      (unittest2:assert?
                       (equal? slen rlen)
                       sub-name
                       (string-append err-1 err-2)
                       result-hash-table)

                      (for-each
                       (lambda (s-list)
                         (begin
                           (let ((err-3
                                  (format
                                   #f "shouldbe element=~a, result=~a"
                                   s-list result-list-list)))
                             (begin
                               (unittest2:assert?
                                (not
                                 (equal?
                                  (member s-list result-list-list)
                                  #f))
                                sub-name
                                (string-append
                                 err-1 err-3)
                                result-hash-table)
                               ))
                           )) shouldbe-list-list)
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (extract-subsets-fixed-len powerset-list asize)
  (begin
    (let ((result-list-list (list)))
      (begin
        (for-each
         (lambda (a-list)
           (begin
             (let ((a-len (length a-list)))
               (begin
                 (if (equal? a-len asize)
                     (begin
                       (set!
                        result-list-list
                        (cons a-list result-list-list))
                       ))
                 ))
             )) powerset-list)
        result-list-list
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-extract-subsets-fixed-len-1 result-hash-table)
 (begin
   (let ((sub-name "test-extract-subsets-fixed-len-1")
         (test-list
          (list
           (list (list (list 1) (list 2) (list 1 2))
                 1 (list (list 1) (list 2)))
           (list (list (list 1) (list 2) (list 1 2))
                 2 (list (list 1 2)))
           (list (list (list 1) (list 2) (list 3)
                       (list 1 2) (list 1 3) (list 2 3)
                       (list 1 2 3))
                 2 (list (list 1 2) (list 1 3) (list 2 3)))
           (list (list (list 1) (list 2) (list 3)
                       (list 1 2) (list 1 3) (list 2 3)
                       (list 1 2 3))
                 3 (list (list 1 2 3)))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((powerset-list (list-ref this-list 0))
                  (asize (list-ref this-list 1))
                  (shouldbe-list-list (list-ref this-list 2)))
              (let ((result-list-list
                     (extract-subsets-fixed-len powerset-list asize)))
                (let ((slen (length shouldbe-list-list))
                      (rlen (length result-list-list)))
                  (let ((err-1
                         (format
                          #f "~a : error (~a) : "
                          sub-name test-label-index))
                        (err-2
                         (format
                          #f "powerset-list=~a, asize=~a, "
                          powerset-list asize))
                        (err-3
                         (format
                          #f "shouldbe length=~a, result length=~a"
                          slen rlen)))
                    (begin
                      (unittest2:assert?
                       (equal? slen rlen)
                       sub-name
                       (string-append
                        err-1 err-2 err-3)
                       result-hash-table)

                      (for-each
                       (lambda (s-list)
                         (begin
                           (let((err-4
                                 (format
                                  #f "shouldbe element=~a, result=~a"
                                  s-list result-list-list)))
                             (begin
                               (unittest2:assert?
                                (not
                                 (equal?
                                  (member s-list result-list-list)
                                  #f))
                                sub-name
                                (string-append
                                 err-1 err-2 err-4)
                                result-hash-table)
                               ))
                           )) shouldbe-list-list)
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; all subsets are the same size
(define (count-non-dominated-fixed-size subset-list)
  (begin
    (let ((count 0)
          (slen (length subset-list)))
      (begin
        (do ((ii 0 (1+ ii)))
            ((>= ii slen))
          (begin
            (let ((alist (list-ref subset-list ii)))
              (begin
                (do ((jj (1+ ii) (1+ jj)))
                    ((>= jj slen))
                  (begin
                    (let ((blist (list-ref subset-list jj)))
                      (begin
                        (if (are-lists-disjoint?
                             alist blist)
                            (begin
                              (if (not
                                   (are-subset-pairs-dominant?
                                    alist blist))
                                  (begin
                                    (set! count (1+ count))
                                    ))
                              ))
                        ))
                    ))
                ))
            ))

        count
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-count-non-dominated-fixed-size-1 result-hash-table)
 (begin
   (let ((sub-name "test-count-non-dominated-fixed-size-1")
         (test-list
          (list
           (list
            (list (list 1 2) (list 1 3) (list 1 4)
                  (list 2 3) (list 2 4) (list 3 4))
            1)
           (list
            (list (list 1 2) (list 1 3) (list 1 4) (list 1 5)
                  (list 2 3) (list 2 4) (list 2 5) (list 3 4)
                  (list 3 5) (list 4 5))
            5)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((powerset-list (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result
                     (count-non-dominated-fixed-size powerset-list)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : powerset-list=~a, "
                        sub-name test-label-index powerset-list))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; all subsets are the same size
(define (count-all-disjointed-subset-pairs powerset-list)
  (begin
    (let ((count 0)
          (plen (length powerset-list)))
      (begin
        (do ((ii 0 (1+ ii)))
            ((>= ii plen))
          (begin
            (let ((ii-list (list-ref powerset-list ii)))
              (begin
                (do ((jj (1+ ii) (1+ jj)))
                    ((>= jj plen))
                  (begin
                    (let ((jj-list (list-ref powerset-list jj)))
                      (begin
                        (if (are-lists-disjoint? ii-list jj-list)
                            (begin
                              (set! count (1+ count))
                              ))
                        ))
                    ))
                ))
            ))
        count
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-count-all-disjointed-subset-pairs-1 result-hash-table)
 (begin
   (let ((sub-name "test-count-all-disjointed-subset-pairs-1")
         (test-list
          (list
           (list (list 1 2) 1)
           (list (list 1 2 3) 6)
           (list (list 1 2 3 4) 25)
           (list (list 1 2 3 4 5 6 7) 966)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((set-list (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((powerset-list (make-power-set set-list)))
                (let ((result
                       (count-all-disjointed-subset-pairs powerset-list)))
                  (let ((err-1
                         (format
                          #f "~a : error (~a) : set-list=~a, "
                          sub-name test-label-index set-list))
                        (err-2
                         (format
                          #f "shouldbe=~a, result=~a"
                          shouldbe result)))
                    (begin
                      (unittest2:assert?
                       (equal? shouldbe result)
                       sub-name
                       (string-append err-1 err-2)
                       result-hash-table)
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop set-list)
  (begin
    (let ((powerset-list (make-power-set set-list))
          (counter 0)
          (slen (length set-list)))
      (let ((plen (length powerset-list))
            (max-pair-size (euclidean/ slen 2)))
        (begin
          (do ((ii 2 (1+ ii)))
              ((> ii max-pair-size))
            (begin
              (let ((ii-pairs-list
                     (extract-subsets-fixed-len powerset-list ii)))
                (let ((subcount
                       (count-non-dominated-fixed-size ii-pairs-list)))
                  (begin
                    (set! counter (+ counter subcount))
                    )))
              ))

          (display
           (ice-9-format:format
            #f "~:d is the number of sets that need to be "
            counter))
          (display
           (ice-9-format:format
            #f "tested for equality : n = ~:d, set = ~a~%"
            (length set-list) set-list))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Let S(A) represent the sum of "))
    (display
     (format #f "elements in set A~%"))
    (display
     (format #f "of size n. We shall call it a "))
    (display
     (format #f "special sum set~%"))
    (display
     (format #f "if for any two non-empty disjoint "))
    (display
     (format #f "subsets, B and C,~%"))
    (display
     (format #f "the following properties are true:~%"))
    (newline)
    (display
     (format #f "i.  S(B) != S(C); that is, sums "))
    (display
     (format #f "of subsets~%"))
    (display
     (format #f "cannot be equal.~%"))
    (display
     (format #f "ii. If B contains more elements "))
    (display
     (format #f "than C then~%"))
    (display
     (format #f "S(B) > S(C).~%"))
    (newline)
    (display
     (format #f "For this problem we shall assume "))
    (display
     (format #f "that a given~%"))
    (display
     (format #f "set contains n strictly increasing "))
    (display
     (format #f "elements and it~%"))
    (display
     (format #f "already satisfies the second rule.~%"))
    (newline)
    (display
     (format #f "Surprisingly, out of the 25 possible "))
    (display
     (format #f "subset pairs that~%"))
    (display
     (format #f "can be obtained from a set for "))
    (display
     (format #f "which n = 4, only~%"))
    (display
     (format #f "1 of these pairs need to be tested "))
    (display
     (format #f "for equality~%"))
    (display
     (format #f "(first rule). Similarly, when n = 7, "))
    (display
     (format #f "only 70 out of~%"))
    (display
     (format #f "the 966 subset pairs need to "))
    (display
     (format #f "be tested.~%"))
    (newline)
    (display
     (format #f "For n = 12, how many of the "))
    (display
     (format #f "261625 subset pairs~%"))
    (display
     (format #f "that can be obtained need to be "))
    (display
     (format #f "tested for equality?~%"))
    (newline)
    (display
     (format #f "NOTE: This problem is related to "))
    (display
     (format #f "problems 103 and 105.~%"))
    (newline)
    (display
     (format #f "The solution was described at~%"))
    (display
     (format #f "https://jsomers.net/blog/pe-oeis.~%"))
    (display
     (format #f "The only sets that need to be checked "))
    (display
     (format #f "are those that~%"))
    (display
     (format #f "have the same length, and are not "))
    (display
     (format #f "dominated. This~%"))
    (display
     (format #f "algorithm is a straight-forward "))
    (display
     (format #f "version, which calculates~%"))
    (display
     (format #f "the possible subsets and does not use "))
    (display
     (format #f "the more advanced~%"))
    (display
     (format #f "Catalan sequence.~%"))
    (display
     (format #f "see https://projecteuler.net/problem=106~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((sets-list
           (list
            (list 1 2 3 4)
            (list 1 2 3 4 5 6 7)
            (list 1 2 3 4 5 6 7 8 9 10 11 12)
            )))
      (begin
        (for-each
         (lambda (set-list)
           (begin
             (newline)
             (sub-main-loop set-list)
             (force-output)
             )) sets-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 106 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

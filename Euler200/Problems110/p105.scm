#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 105                                    ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated July 28, 2022                                ###
;;;###                                                       ###
;;;###  updated March 11, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 rdelim for delimited read-line functions
(use-modules ((ice-9 rdelim)
              :renamer (symbol-prefix-proc 'ice-9-rdelim:)))

;;;### srfi-1 for fold function
(use-modules ((srfi srfi-1)
              :renamer (symbol-prefix-proc 'srfi-1:)))

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (are-lists-disjoint? llist1 llist2)
  (begin
    (let ((ok-flag #t))
      (begin
        (for-each
         (lambda (elem1)
           (begin
             (if (not (equal? (member elem1 llist2) #f))
                 (begin
                   (set! ok-flag #f)
                   ))
             )) llist1)
        ok-flag
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-are-lists-disjoint-1 result-hash-table)
 (begin
   (let ((sub-name "test-are-lists-disjoint-1")
         (test-list
          (list
           (list (list 1) (list 2) #t)
           (list (list 1 2) (list 3 4) #t)
           (list (list 1 2 3) (list 4 5 6) #t)
           (list (list 1 2 3) (list 4 5 6 3) #f)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((set-1-list (list-ref this-list 0))
                  (set-2-list (list-ref this-list 1))
                  (shouldbe (list-ref this-list 2)))
              (let ((result
                     (are-lists-disjoint? set-1-list set-2-list)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "set-1-list=~a, set-2-list=~a, "
                        set-1-list set-2-list))
                      (err-3
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append
                      err-1 err-2 err-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define-syntax assign-value-to-list
  (syntax-rules ()
    ((assign-value-to-list
      ii-sum jj set-value-array sum-list-array)
     (begin
       (let ((jj-value (array-ref set-value-array jj)))
         (begin
           (cond
            ((= jj-value ii-sum)
             (begin
               (let ((row-list
                      (array-ref sum-list-array ii-sum)))
                 (begin
                   (array-set!
                    sum-list-array
                    (cons (list jj-value) row-list)
                    ii-sum)
                   ))
               ))
            ((< jj-value ii-sum)
             (begin
               ;;; try to make target value = ii-sum, using previously found diff-value
               (let ((diff-value (- ii-sum jj-value)))
                 (begin
                   (if (and (<= diff-value ii-sum)
                            (>= diff-value 0))
                       (begin
                         (let ((dlist
                                (array-ref
                                 sum-list-array diff-value))
                               (rlist
                                (array-ref
                                 sum-list-array ii-sum)))
                           (begin
                             (for-each
                              (lambda (alist)
                                (begin
                                  (if (equal? (member jj-value alist) #f)
                                      (begin
                                        (let ((next-alist
                                               (sort
                                                (cons jj-value alist)
                                                <)))
                                          (begin
                                            (if (equal?
                                                 (member next-alist rlist)
                                                 #f)
                                                (begin
                                                  (set!
                                                   rlist
                                                   (cons next-alist rlist))
                                                  ))
                                            ))
                                        ))
                                  )) dlist)
                             (array-set! sum-list-array rlist ii-sum)
                             ))
                         ))
                   ))
               )))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (make-array-of-sum-lists set-list)
  (begin
    (let ((ok-flag #t)
          (set-value-array (list->array 1 set-list))
          (llen (length set-list))
          (max-sum
           (srfi-1:fold + 0 set-list)))
      (let ((sum-list-array
             (make-array (list) (1+ max-sum))))
        (begin
          (if (<= llen 1)
              (begin
                (array-set! sum-list-array (list set-list) 0)
                sum-list-array)
              (begin
                (do ((ii-sum 1 (1+ ii-sum)))
                    ((or (> ii-sum max-sum)
                         (equal? ok-flag #f)))
                  (begin
                    (do ((jj 0 (1+ jj)))
                        ((>= jj llen))
                      (begin
                        (assign-value-to-list
                         ii-sum jj set-value-array sum-list-array)
                        ))
                    ))

                sum-list-array
                ))
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-make-array-of-sum-lists-1 result-hash-table)
 (begin
   (let ((sub-name "test-make-array-of-sum-lists-1")
         (test-list
          (list
           (list (list 1)
                 (list (list) (list (list 1))))
           (list (list 1 2)
                 (list (list) (list (list 1)) (list (list 2))
                       (list (list 1 2))))
           (list (list 1 3)
                 (list (list) (list (list 1)) (list (list 3))
                       (list (list 1 3)) (list)))
           (list (list 2 3 5)
                 (list (list) (list) (list (list 2)) (list (list 3))
                       (list) (list (list 5) (list 2 3)) (list)
                       (list (list 2 5)) (list (list 3 5)) (list)
                       (list (list 2 3 5))))
           (list (list 1 2 3 4)
                 (list (list) (list (list 1)) (list (list 2))
                       (list (list 3) (list 1 2))
                       (list (list 4) (list 1 3))
                       (list (list 2 3) (list 1 4))
                       (list (list 2 4) (list 1 2 3))
                       (list (list 3 4) (list 1 2 4))
                       (list (list 1 3 4))
                       (list (list 2 3 4))
                       (list (list 1 2 3 4))))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((a-list (list-ref this-list 0))
                  (shouldbe-list-list (list-ref this-list 1)))
              (let ((result-array (make-array-of-sum-lists a-list)))
                (let ((result-list-list (array->list result-array)))
                  (let ((slen (length shouldbe-list-list))
                        (rlen (length result-list-list)))
                    (let ((err-1
                           (format
                            #f "~a : error (~a) : a-list=~a, "
                            sub-name test-label-index a-list))
                          (err-2
                           (format
                            #f "shouldbe length~a, result=~a"
                            slen rlen)))
                      (begin
                        (unittest2:assert?
                         (equal? slen rlen)
                         sub-name
                         (string-append err-1 err-2)
                         result-hash-table)

                        (for-each
                         (lambda (slist)
                           (begin
                             (let ((err-3
                                    (format
                                     #f "shouldbe element=~a, result=~a"
                                     slist result-list-list)))
                               (begin
                                 (unittest2:assert?
                                  (not
                                   (equal?
                                    (member slist result-list-list)
                                    #f))
                                  sub-name
                                  (string-append err-1 err-3)
                                  result-hash-table)
                                 ))
                             )) shouldbe-list-list)
                        ))
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define-syntax cond-1-process-subsets
  (syntax-rules ()
    ((cond-1-process-subsets len-rr row-rr ok-flag)
     (begin
       (do ((mm 0 (1+ mm)))
           ((or (>= mm len-rr)
                (equal? ok-flag #f)))
         (begin
           (let ((ll1 (list-ref row-rr mm)))
             (begin
               (do ((nn (1+ mm) (1+ nn)))
                   ((or (>= nn len-rr)
                        (equal? ok-flag #f)))
                 (begin
                   (let ((ll2 (list-ref row-rr nn)))
                     (begin
                       (if (are-lists-disjoint? ll1 ll2)
                           (begin
                             ;;; if disjoint then condition 1 is failed
                             (set! ok-flag #f)
                             ))
                       ))
                   ))
               ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
;;; if there are two or more ways of summing sets to the same ii-sum
;;; then this condition is false.
(define (condition-1-ok? set-list sum-list-array)
  (begin
    (let ((ok-flag #t)
          (max-sum (srfi-1:fold + 0 set-list)))
      (begin
        ;;; all lists in this row have sum = ii-sum
        (do ((rr 0 (1+ rr)))
            ((or (> rr max-sum)
                 (equal? ok-flag #f)))
          (begin
            (let ((row-rr
                   (array-ref sum-list-array rr)))
              (let ((len-rr (length row-rr)))
                (begin
                  (if (> len-rr 1)
                      (begin
                        (cond-1-process-subsets
                         len-rr row-rr ok-flag)
                        ))
                  )))
            ))

        ok-flag
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-condition-1-ok-1 result-hash-table)
 (begin
   (let ((sub-name "test-condition-1-ok-1")
         (test-list
          (list
           (list (list 1) #t)
           (list (list 1 2) #t)
           (list (list 1 2 3) #f)
           (list (list 1 2 4) #t)
           (list (list 2 3 4) #t)
           (list (list 1 2 3 4) #f)
           (list (list 2 3 4 5) #f)
           (list (list 3 5 6 7) #t)
           (list (list 6 9 11 12 13) #t)
           (list (list 11 18 19 20 22 25) #t)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((a-list (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((sum-list-array
                     (make-array-of-sum-lists a-list)))
                (let ((result
                       (condition-1-ok? a-list sum-list-array)))
                  (let ((err-1
                         (format
                          #f "~a : error (~a) : a-list=~a, "
                          sub-name test-label-index a-list))
                        (err-2
                         (format
                          #f "shouldbe=~a, result=~a"
                          shouldbe result)))
                    (begin
                      (unittest2:assert?
                       (equal? shouldbe result)
                       sub-name
                       (string-append err-1 err-2)
                       result-hash-table)
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (populate-length-sum-hash!
         length-sum-htable set-list sum-list-array)
  (begin
    (let ((ok-flag #t)
          (set-value-array (list->array 1 set-list))
          (llen (length set-list))
          (max-sum (srfi-1:fold + 0 set-list)))
      (begin
        (if (<= llen 1)
            (begin
              (let ((sum
                     (srfi-1:fold + 0 set-list)))
                (let ((next-list
                       (list (list sum llen set-list))))
                  (begin
                    (hash-set! length-sum-htable llen next-list)
                    ))
                ))
            (begin
              (do ((ii-sum 1 (1+ ii-sum)))
                  ((or (> ii-sum max-sum)
                       (equal? ok-flag #f)))
                (begin
                  (let ((ii-list
                         (array-ref sum-list-array ii-sum)))
                    (let ((ii-len (length ii-list)))
                      (begin
                        (if (> ii-len 0)
                            (begin
                              (for-each
                               (lambda (alist)
                                 (begin
                                   (let ((asum ii-sum)
                                         (alen (length alist)))
                                     (let ((next-list
                                            (list asum alen alist))
                                           (this-list
                                            (hash-ref
                                             length-sum-htable
                                             alen (list))))
                                       (begin
                                         (hash-set!
                                          length-sum-htable alen
                                          (cons next-list this-list))
                                         )))
                                   )) ii-list)
                              ))
                        )))
                  ))
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-populate-length-sum-hash-1 result-hash-table)
 (begin
   (let ((sub-name "test-populate-length-sum-hash-1")
         (test-list
          (list
           (list (list 1)
                 (list
                  (list 1 (list (list 1 1 (list 1))))))
           (list (list 1 2)
                 (list
                  (list 1 (list (list 1 1 (list 1))
                                (list 2 1 (list 2))))
                  (list 2 (list (list 3 2 (list 1 2))))))
           (list (list 1 3)
                 (list
                  (list 1 (list (list 1 1 (list 1))
                                (list 3 1 (list 3))))
                  (list 2 (list (list 4 2 (list 1 3))))))
           (list (list 2 3 5)
                 (list
                  (list 1 (list (list 2 1 (list 2))
                                (list 3 1 (list 3))
                                (list 5 1 (list 5))))
                  (list 2 (list (list 5 2 (list 2 3))
                                (list 7 2 (list 2 5))
                                (list 8 2 (list 3 5))))
                  (list 3 (list (list 10 3 (list 2 3 5))))))
           ))
         (length-sum-htable (make-hash-table))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((a-list (list-ref this-list 0))
                  (shouldbe-list-list (list-ref this-list 1)))
              (let ((sum-list-array
                     (make-array-of-sum-lists a-list)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : a-list=~a, "
                        sub-name test-label-index a-list)))
                  (begin
                    (hash-clear! length-sum-htable)
                    (populate-length-sum-hash!
                     length-sum-htable a-list sum-list-array)

                    (for-each
                     (lambda (ll-list-list)
                       (begin
                         (let ((ll-len (list-ref ll-list-list 0))
                               (shouldbe-ll-list
                                (list-ref ll-list-list 1)))
                           (let ((result-list-list
                                  (hash-ref
                                   length-sum-htable ll-len (list))))
                             (begin
                               (for-each
                                (lambda (b-list)
                                  (begin
                                    (let ((err-2
                                           (format
                                            #f "shouldbe element=~a, result=~a"
                                            b-list result-list-list)))
                                      (begin
                                        (unittest2:assert?
                                         (not
                                          (equal?
                                           (member
                                            b-list result-list-list)
                                           #f))
                                         sub-name
                                         (string-append
                                          err-1 err-2)
                                         result-hash-table)
                                        ))
                                    )) shouldbe-ll-list)
                               )))
                         )) shouldbe-list-list)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define-syntax compare-ii-to-the-rest
  (syntax-rules ()
    ((compare-ii-to-the-rest
      ii max-len ii-sum ii-len ii-llist len-htable ok-flag)
     (begin
       (do ((jj (1+ ii) (1+ jj)))
           ((or (> jj max-len)
                (equal? ok-flag #f)))
         (begin
           (let ((jj-subset-list-list
                  (hash-ref len-htable jj (list))))
             (let ((jj-len-len
                    (length jj-subset-list-list)))
               (begin
                 ;;; since jj>ii, all lengths in jj-subset-list > all lengths in ii-subset-list
                 (if (> jj-len-len 0)
                     (begin
                       (for-each
                        (lambda (jj-subset-ll)
                          (begin
                            (let ((jj-sum
                                   (list-ref jj-subset-ll 0))
                                  (jj-len
                                   (list-ref jj-subset-ll 1))
                                  (jj-llist
                                   (list-ref jj-subset-ll 2)))
                              (begin
                                ;;; ii < jj
                                (if (and
                                     (are-lists-disjoint?
                                      ii-llist jj-llist)
                                     (>= ii-sum jj-sum))
                                    (begin
                                      (set! ok-flag #f)
                                      ))
                                ))
                            )) jj-subset-list-list)
                       ))
                 )))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
;;; check that longer length sets have bigger sums
(define (condition-2-ok? set-list)
  (begin
    (let ((ok-flag #t)
          (max-ii (euclidean/ (length set-list) 2))
          (slen (length set-list))
          (sum-small (car set-list))
          (sum-large 0))
      (begin
        ;;; a1+a2 > an, a1+a2+a3>an+an-1, ...
        (do ((ii 0 (1+ ii)))
            ((or (>= ii max-ii)
                 (equal? ok-flag #f)))
          (begin
            (let ((next-small
                   (+ sum-small
                      (list-ref set-list (+ ii 1))))
                  (next-large
                   (+ sum-large
                      (list-ref set-list (- slen ii 1)))))
              (begin
                (if (<= next-small next-large)
                    (begin
                      (set! ok-flag #f))
                    (begin
                      (set! sum-small next-small)
                      (set! sum-large next-large)
                      ))
                ))
            ))

        ok-flag
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-condition-2-ok-1 result-hash-table)
 (begin
   (let ((sub-name "test-condition-2-ok-1")
         (test-list
          (list
           (list (list 1) #t)
           (list (list 1 2) #t)
           (list (list 1 2 3) #f)
           (list (list 1 2 4) #f)
           (list (list 2 3 4) #t)
           (list (list 1 2 3 4) #f)
           (list (list 2 3 4 5) #f)
           (list (list 3 5 6 7) #t)
           (list (list 6 9 11 12 13) #t)
           (list (list 11 18 19 20 22 25) #t)
           (list (list 11 18 19 20 22 26) #f)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((a-list (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result (condition-2-ok? a-list)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : a-list=~a, "
                        sub-name test-label-index a-list))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a~%"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define-syntax handle-final-current-list
  (syntax-rules ()
    ((handle-final-current-list
      counter status-num max-depth
      length-htable sum-htable curr-list acc-list)
     (begin
       (set! counter (1+ counter))

       (let ((ll1 (sort curr-list <))
             (s1 (srfi-1:fold + 0 curr-list))
             (acc-sum -1))
         (begin
           (if (and
                (list? acc-list)
                (> (length acc-list) 0))
               (begin
                 (set! acc-sum (srfi-1:fold + 0 acc-list))
                 ))

           (if (zero? (modulo counter status-num))
               (begin
                 (display
                  (ice-9-format:format
                   #f "(~:d ; ~:d) : current-list = ~a, "
                   max-depth counter ll1))
                 (display
                  (ice-9-format:format
                   #f "sum = ~:d~%" s1))
                 (display
                  (ice-9-format:format
                   #f "  acc-list = ~a, acc-sum = ~:d : ~a~%"
                   acc-list acc-sum
                   (timer-module:current-date-time-string)))
                 (force-output)
                 ))

           (if (or (< acc-sum 0)
                   (< s1 acc-sum))
               (begin
                 (list counter ll1))
               (begin
                 (list counter acc-list)
                 ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
;;; returns a list of lists
(define (read-in-file fname)
  (begin
    (let ((results-list (list))
          (line-counter 0))
      (begin
        (if (file-exists? fname)
            (begin
              (with-input-from-file fname
                (lambda ()
                  (begin
                    (do ((line
                          (ice-9-rdelim:read-delimited "\r\n")
                          (ice-9-rdelim:read-delimited "\r\n")))
                        ((eof-object? line))
                      (begin
                        (if (and
                             (not (eof-object? line))
                             (> (string-length line) 0))
                            (begin
                              (let ((llist
                                     (string-split line #\,)))
                                (let ((plist
                                       (map string->number llist)))
                                  (begin
                                    (set!
                                     line-counter (1+ line-counter))
                                    (set!
                                     results-list
                                     (cons plist results-list))
                                    )))
                              ))
                        ))
                    )))

              (display
               (ice-9-format:format
                #f "read in ~a lines from ~a~%" line-counter fname))
              (newline)
              (force-output)

              (reverse results-list))
            (begin
              (list)
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop filename status-num debug-flag)
  (begin
    (let ((num-sss 0)
          (num-non-sss 0)
          (num-total 0)
          (sum-sa 0)
          (sa-string-list (list))
          (sa-sum-list (list))
          (set-list-list (read-in-file filename))
          (length-htable (make-hash-table 1000)))
      (let ((slen (length set-list-list)))
        (begin
          (for-each
           (lambda (set-list)
             (begin
               (set! num-total (1+ num-total))

               (let ((sorted-set-list (sort set-list <)))
                 (let ((sum-array
                        (make-array-of-sum-lists sorted-set-list)))
                   (begin
                     (if (condition-1-ok? sorted-set-list sum-array)
                         (begin
                           (if (condition-2-ok? sorted-set-list)
                               (begin
                                 (set! num-sss (1+ num-sss))
                                 (let ((this-sa
                                        (srfi-1:fold + 0 set-list))
                                       (a-string
                                        (string-append
                                         "{ "
                                         (string-join
                                          (map number->string sorted-set-list)
                                          ", ")
                                         " }")))
                                   (begin
                                     (set!
                                      sum-sa (+ sum-sa this-sa))
                                     (set!
                                      sa-string-list
                                      (cons a-string sa-string-list))
                                     (set!
                                      sa-sum-list
                                      (cons this-sa sa-sum-list))
                                     (if (equal? debug-flag #t)
                                         (begin
                                           (display
                                            (ice-9-format:format
                                             #f "    (~:d) set = ~a, sum = ~:d, "
                                             num-total a-string this-sa))
                                           (display
                                            (ice-9-format:format
                                             #f "sum total so far = ~:d~%"
                                             sum-sa))
                                           (force-output)
                                           ))
                                     )))
                               (begin
                                 (set! num-non-sss (1+ num-non-sss))
                                 )))
                         (begin
                           (set! num-non-sss (1+ num-non-sss))
                           ))
                     )))

               (if (zero? (modulo num-total status-num))
                   (begin
                     (display
                      (ice-9-format:format
                       #f "  ~:d / ~:d : "
                       num-total slen))
                     (display
                      (format
                       #f "~a~%"
                       (timer-module:current-date-time-string)))
                     (force-output)
                     ))
               )) set-list-list)

          (let ((slen (length sa-string-list))
                (stotal 0))
            (begin
              (do ((ii 0 (1+ ii)))
                  ((>= ii slen))
                (begin
                  (let ((sa-string
                         (list-ref sa-string-list ii))
                        (sa-sum
                         (list-ref sa-sum-list ii)))
                    (begin
                      (set! stotal (+ stotal sa-sum))
                      (display
                       (ice-9-format:format
                        #f "    (~:d) set = ~a, sum = ~:d, "
                        ii sa-string sa-sum))
                      (display
                       (ice-9-format:format
                        #f "sum total so far = ~:d~%"
                        stotal))
                      ))
                  ))

              (newline)
              (display
               (ice-9-format:format
                #f "number of special sum sets = ~:d~%" num-sss))
              (display
               (ice-9-format:format
                #f "number of non-special sum sets = ~:d~%" num-non-sss))
              (display
               (ice-9-format:format
                #f "total number of sets = ~:d~%" num-total))
              (display
               (ice-9-format:format
                #f "sum of special sets total = ~:d~%" sum-sa))

              (force-output)
              ))
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Let S(A) represent the sum of "))
    (display
     (format #f "elements in set A~%"))
    (display
     (format #f "of size n. We shall call it a "))
    (display
     (format #f "special sum set~%"))
    (display
     (format #f "if for any two non-empty disjoint "))
    (display
     (format #f "subsets, B and C,~%"))
    (display
     (format #f "the following properties are true:~%"))
    (newline)
    (display
     (format #f "i.  S(B) != S(C); that is, sums "))
    (display
     (format #f "of subsets cannot~%"))
    (display
     (format #f "be equal.~%"))
    (display
     (format #f "ii. If B contains more elements "))
    (display
     (format #f "than C then~%"))
    (display
     (format #f "S(B) > S(C).~%"))
    (newline)
    (display
     (format #f "For example, {81, 88, 75, 42, 87, 84, 86, 65} "))
    (display
     (format #f "is not a~%"))
    (display
     (format #f "special sum set because "))
    (display
     (format #f "65 + 87 + 88 = 75 + 81 + 84,~%"))
    (display
     (format #f "whereas {157, 150, 164, 119, 79, "))
    (display
     (format #f "159, 161, 139, 158}~%"))
    (display
     (format #f "satisfies both rules for all "))
    (display
     (format #f "possible subset pair~%"))
    (display
     (format #f "combinations and S(A) = 1286.~%"))
    (newline)
    (display (format #f "Using sets.txt (right "))
    (display
     (format #f "click and 'Save~%"))
    (display
     (format #f "Link/Target As...'), a 4K text "))
    (display
     (format #f "file with one-hundred~%"))
    (display
     (format #f "sets containing seven to twelve "))
    (display
     (format #f "elements (the two~%"))
    (display
     (format #f "examples given above are the "))
    (display
     (format #f "first two sets~%"))
    (display
     (format #f "in the file), identify all the "))
    (display
     (format #f "special sum sets,~%"))
    (display
     (format #f "A1, A2, ..., Ak, and find the "))
    (display
     (format #f "value of S(A1) + S(A2) + ... + S(Ak).~%"))
    (newline)
    (display
     (format #f "NOTE: This problem is related "))
    (display
     (format #f "to problems 103 and 106.~%"))
    (newline)
    (display
     (format #f "A speed up was found at~%"))
    (display
     (format #f "https://medium.com/andys-coding-blog/hackerranks-project-euler-problem-105-special-subset-sums-testing-60640306dd81~%"))
    (display
     (format #f "where checking condition 2 involves "))
    (display
     (format #f "just checking~%"))
    (display
     (format #f "the sum of the smallest 2 elements "))
    (display
     (format #f "against the largest~%"))
    (display
     (format #f "element, and checking the sum of "))
    (display
     (format #f "the smallest 3~%"))
    (display
     (format #f "elements against the sum of the "))
    (display
     (format #f "largest 2 elements.~%"))
    (display
     (format #f "see https://projecteuler.net/problem=105~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((filename "sets.txt")
          (status-num 100)
          (debug-flag #f))
      (begin
        (sub-main-loop filename status-num debug-flag)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 105 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 103                                    ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated July 28, 2022                                ###
;;;###                                                       ###
;;;###  updated March 11, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### srfi-1 for fold function
(use-modules ((srfi srfi-1)
              :renamer (symbol-prefix-proc 'srfi-1:)))

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (are-lists-disjoint? llist1 llist2)
  (begin
    (let ((intersect-list
           (srfi-1:lset-intersection = llist1 llist2)))
      (begin
        (if (equal? intersect-list (list))
            (begin
              #t)
            (begin
              #f
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-are-lists-disjoint-1 result-hash-table)
 (begin
   (let ((sub-name "test-are-lists-disjoint-1")
         (test-list
          (list
           (list (list 1) (list 2) #t)
           (list (list 1 2) (list 3 4) #t)
           (list (list 1 2 3) (list 4 5 6) #t)
           (list (list 1 2 3) (list 4 5 6 3) #f)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((set-1-list (list-ref this-list 0))
                  (set-2-list (list-ref this-list 1))
                  (shouldbe (list-ref this-list 2)))
              (let ((result
                     (are-lists-disjoint?
                      set-1-list set-2-list)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "set-1-list=~a, set-2-list=~a, "
                        set-1-list set-2-list))
                      (err-3
                       (format
                        #f "shouldbe=~a, result~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append
                      err-1 err-2 err-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (make-power-set start-list)
  (define (local-loop
           depth max-depth start-index end-index start-list
           current-list acc-list)
    (begin
      (cond
       ((>= depth max-depth)
        (begin
          (let ((sorted-list (sort current-list <)))
            (begin
              (cons sorted-list acc-list)
              ))
          ))
       (else
        (begin
          (do ((ii start-index (1+ ii)))
              ((>= ii end-index))
            (begin
              (let ((anum (list-ref start-list ii)))
                (let ((next-current-list
                       (cons anum current-list)))
                  (let ((next-acc-list
                         (local-loop
                          (1+ depth) max-depth (1+ ii) end-index
                          start-list next-current-list acc-list)))
                    (begin
                      (set! acc-list next-acc-list)
                      ))
                  ))
              ))

          acc-list
          )))
      ))
  (begin
    (let ((result-list-list (list))
          (slen (length start-list)))
      (begin
        (do ((ii 1 (1+ ii)))
            ((> ii slen))
          (begin
            (let ((rr-list-list
                   (local-loop
                    0 ii 0 slen start-list
                    (list) (list))))
              (begin
                (set!
                 result-list-list
                 (append result-list-list
                         rr-list-list))
                ))
            ))

        result-list-list
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-make-power-set-1 result-hash-table)
 (begin
   (let ((sub-name "test-make-power-set-1")
         (test-list
          (list
           (list (list 1 2)
                 (list (list 1) (list 2) (list 1 2)))
           (list (list 1 2 3)
                 (list (list 1) (list 2) (list 3)
                       (list 1 2) (list 1 3) (list 2 3)
                       (list 1 2 3)))
           (list (list 1 2 3 4)
                 (list (list 1) (list 2) (list 3) (list 4)
                       (list 1 2) (list 1 3) (list 1 4)
                       (list 2 3) (list 2 4) (list 3 4)
                       (list 1 2 3) (list 1 2 4) (list 1 3 4)
                       (list 2 3 4) (list 1 2 3 4)))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((start-set (list-ref this-list 0))
                  (shouldbe-list-list (list-ref this-list 1)))
              (let ((result-list-list
                     (make-power-set start-set)))
                (let ((slen (length shouldbe-list-list))
                      (rlen (length result-list-list)))
                  (let ((err-1
                         (format
                          #f "~a : error (~a) : start-set~a, "
                          sub-name test-label-index start-set))
                        (err-2
                         (format
                          #f "shouldbe length=~a, result length=~a"
                          slen rlen)))
                    (begin
                      (unittest2:assert?
                       (equal? slen rlen)
                       sub-name
                       (string-append err-1 err-2)
                       result-hash-table)

                      (for-each
                       (lambda (s-list)
                         (begin
                           (let ((err-3
                                  (format
                                   #f "shouldbe element=~a, result=~a"
                                   s-list result-list-list)))
                             (begin
                               (unittest2:assert?
                                (not
                                 (equal?
                                  (member s-list result-list-list)
                                  #f))
                                sub-name
                                (string-append err-1 err-3)
                                result-hash-table)
                               ))
                           )) shouldbe-list-list)
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (make-incremental-powerset subset-list next-element)
  (begin
    (let ((next-subset-list
           (cons (list next-element) subset-list)))
      (begin
        (for-each
         (lambda (sub-list)
           (begin
             (let ((ne-sub-list
                    (sort
                     (cons next-element sub-list)
                     <)))
               (begin
                 (set!
                  next-subset-list
                  (cons ne-sub-list next-subset-list))
                 ))
             )) subset-list)

        next-subset-list
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-make-incremental-powerset-1 result-hash-table)
 (begin
   (let ((sub-name "test-make-incremental-powerset-1")
         (test-list
          (list
           (list (list (list 1)) 2
                 (list (list 1) (list 2) (list 1 2)))
           (list (list (list 1) (list 2) (list 1 2)) 3
                 (list (list 1) (list 2) (list 1 2)
                       (list 3) (list 1 3) (list 2 3)
                       (list 1 2 3)))
           (list (list
                  (list 1) (list 2) (list 3)
                  (list 1 2) (list 1 3) (list 2 3)
                  (list 1 2 3)) 4
                  (list
                   (list 1) (list 2) (list 3)
                   (list 1 2) (list 1 3) (list 2 3)
                   (list 1 2 3)
                   (list 4) (list 1 4) (list 2 4)
                   (list 3 4) (list 1 2 4) (list 1 3 4)
                   (list 2 3 4) (list 1 2 3 4)))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((a-list-list (list-ref this-list 0))
                  (a-elem (list-ref this-list 1))
                  (shouldbe-list-list (list-ref this-list 2)))
              (let ((rr-list-list
                     (make-incremental-powerset a-list-list a-elem)))
                (let ((slen (length shouldbe-list-list))
                      (rlen (length rr-list-list))
                      (result-list-list (list)))
                  (let ((err-1
                         (format
                          #f "~a : error (~a) : "
                          sub-name test-label-index))
                        (err-2
                         (format
                          #f "a-list-list=~a, a-elem=~a, "
                          a-list-list a-elem))
                        (err-3
                         (format
                          #f "shouldbe length=~a, result length=~a"
                          slen rlen)))
                    (begin
                      (unittest2:assert?
                       (equal? slen rlen)
                       sub-name
                       (string-append
                        err-1 err-2 err-3)
                       result-hash-table)

                      (for-each
                       (lambda (alist)
                         (begin
                           (let ((sr-list (sort alist <)))
                             (begin
                               (set! result-list-list
                                     (cons sr-list result-list-list))
                               ))
                           )) rr-list-list)

                      (for-each
                       (lambda (s-list)
                         (begin
                           (let ((err-4
                                  (format
                                   #f "shouldbe element=~a, result=~a"
                                   s-list result-list-list)))
                             (begin
                               (unittest2:assert?
                                (not
                                 (equal?
                                  (member s-list result-list-list)
                                  #f))
                                sub-name
                                (string-append
                                 err-1 err-2 err-4)
                                result-hash-table)
                               ))
                           )) shouldbe-list-list)
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (are-subset-pairs-dominant? subset-1-list subset-2-list)
  (define (local-l1-greater-l2 llist1 llist2 llen1)
    (begin
      (let ((ok-flag #t)
            (loop-continue-flag #t))
        (begin
          (do ((ii 0 (1+ ii)))
              ((or (>= ii llen1)
                   (equal? loop-continue-flag #f)))
            (begin
              (let ((elem1 (list-ref llist1 ii))
                    (elem2 (list-ref llist2 ii)))
                (begin
                  (if (<= elem1 elem2)
                      (begin
                        (set! ok-flag #f)
                        (set! loop-continue-flag #f)
                        ))
                  ))
              ))
          ok-flag
          ))
      ))
  (begin
    (let ((llen1 (length subset-1-list))
          (llen2 (length subset-2-list)))
      (begin
        (if (equal? llen1 llen2)
            (begin
              (let ((elem1 (car subset-1-list))
                    (elem2 (car subset-2-list)))
                (begin
                  (if (>= elem1 elem2)
                      (begin
                        (local-l1-greater-l2
                         subset-1-list subset-2-list llen1))
                      (begin
                        (local-l1-greater-l2
                         subset-2-list subset-1-list llen1)
                        ))
                  )))
            (begin
              #f
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-are-subset-pairs-dominant-1 result-hash-table)
 (begin
   (let ((sub-name "test-are-subset-pairs-dominant-1")
         (test-list
          (list
           (list (list 1 2) (list 3 4) #t)
           (list (list 3 4) (list 1 2) #t)
           (list (list 1 3) (list 2 4) #t)
           (list (list 2 4) (list 1 3) #t)
           (list (list 2 3) (list 1 4) #f)
           (list (list 1 4) (list 2 3) #f)
           (list (list 1 2 3) (list 4 5 6) #t)
           (list (list 4 5 6) (list 1 2 3) #t)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((subset1 (list-ref this-list 0))
                  (subset2 (list-ref this-list 1))
                  (shouldbe (list-ref this-list 2)))
              (let ((result
                     (are-subset-pairs-dominant?
                      subset1 subset2)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "subset1=~a, subset2=~a, "
                        subset1 subset2))
                      (err-3
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append
                      err-1 err-2 err-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (extract-subsets-fixed-len powerset-list asize)
  (begin
    (let ((result-list-list (list)))
      (begin
        (for-each
         (lambda (a-list)
           (begin
             (let ((a-len (length a-list)))
               (begin
                 (if (equal? a-len asize)
                     (begin
                       (set!
                        result-list-list
                        (cons a-list result-list-list))
                       ))
                 ))
             )) powerset-list)
        result-list-list
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-extract-subsets-fixed-len-1 result-hash-table)
 (begin
   (let ((sub-name "test-extract-subsets-fixed-len-1")
         (test-list
          (list
           (list (list (list 1) (list 2) (list 1 2))
                 1 (list (list 1) (list 2)))
           (list (list (list 1) (list 2) (list 1 2))
                 2 (list (list 1 2)))
           (list (list (list 1) (list 2) (list 3)
                       (list 1 2) (list 1 3) (list 2 3)
                       (list 1 2 3))
                 2 (list (list 1 2) (list 1 3) (list 2 3)))
           (list (list (list 1) (list 2) (list 3)
                       (list 1 2) (list 1 3) (list 2 3)
                       (list 1 2 3))
                 3 (list (list 1 2 3)))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((powerset-list (list-ref this-list 0))
                  (asize (list-ref this-list 1))
                  (shouldbe-list-list (list-ref this-list 2)))
              (let ((result-list-list
                     (extract-subsets-fixed-len
                      powerset-list asize)))
                (let ((slen (length shouldbe-list-list))
                      (rlen (length result-list-list)))
                  (let ((err-1
                         (format
                          #f "~a : error (~a) : "
                          sub-name test-label-index))
                        (err-2
                         (format
                          #f "powerset-list=~a, asize=~a, "
                          powerset-list asize))
                        (err-3
                         (format
                          #f "shouldbe length=~a, result length=~a"
                          slen rlen)))
                    (begin
                      (unittest2:assert?
                       (equal? slen rlen)
                       sub-name
                       (string-append
                        err-1 err-2 err-3)
                       result-hash-table)

                      (for-each
                       (lambda (s-list)
                         (begin
                           (let ((err-4
                                  (format
                                   #f "shouldbe element=~a, result=~a"
                                   s-list result-list-list)))
                             (begin
                               (unittest2:assert?
                                (not
                                 (equal?
                                  (member s-list result-list-list)
                                  #f))
                                sub-name
                                (string-append
                                 err-1 err-2 err-4)
                                result-hash-table)
                               ))
                           )) shouldbe-list-list)
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; all subsets are the same size
(define (condition-1-same-size-subsets? subset-list)
  (begin
    (let ((ok-flag #t)
          (slen (length subset-list)))
      (begin
        (do ((ii 0 (1+ ii)))
            ((or (>= ii slen)
                 (equal? ok-flag #f)))
          (begin
            (let ((alist (list-ref subset-list ii)))
              (begin
                (do ((jj (1+ ii) (1+ jj)))
                    ((or (>= jj slen)
                         (equal? ok-flag #f)))
                  (begin
                    (let ((blist (list-ref subset-list jj)))
                      (begin
                        (if (are-lists-disjoint?
                             alist blist)
                            (begin
                              (if (not (are-subset-pairs-dominant?
                                        alist blist))
                                  (begin
                                    (let ((asum (srfi-1:fold + 0 alist))
                                          (bsum (srfi-1:fold + 0 blist)))
                                      (begin
                                        (if (= asum bsum)
                                            (begin
                                              (set! ok-flag #f)
                                              ))
                                        ))
                                    ))
                              ))
                        ))
                    ))
                ))
            ))

        ok-flag
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-condition-1-same-size-subsets-1 result-hash-table)
 (begin
   (let ((sub-name "test-condition-1-same-size-subsets-1")
         (test-list
          (list
           (list
            (list (list 2 3) (list 2 4) (list 3 4))
            #t)
           (list
            (list (list 1 2) (list 1 3) (list 1 4) (list 1 5)
                  (list 2 3) (list 2 4) (list 2 5) (list 3 4)
                  (list 3 5) (list 4 5))
            #f)
           (list
            (list (list 3 5) (list 3 6) (list 3 7)
                  (list 5 6) (list 5 7) (list 6 7))
            #t)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((powerset-list (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result
                     (condition-1-same-size-subsets? powerset-list)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : powerset-list=~a, "
                        sub-name test-label-index powerset-list))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a~%"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; condition-1-ok? assumes that condition-2-ok? first!
(define (condition-1-ok? set-list powerset-list)
  (begin
    (let ((ok-flag #t)
          (slen (length set-list))
          (plen (length powerset-list)))
      (let ((max-pair-size (euclidean/ slen 2)))
        (begin
          (do ((ii 2 (1+ ii)))
              ((or (> ii max-pair-size)
                   (equal? ok-flag #f)))
            (begin
              (let ((ii-pairs-list
                     (extract-subsets-fixed-len powerset-list ii)))
                (let ((aflag
                       (condition-1-same-size-subsets? ii-pairs-list)))
                  (begin
                    (if (equal? aflag #f)
                        (begin
                          (set! ok-flag #f)
                          ))
                    )))
              ))
          ok-flag
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-condition-1-ok-1 result-hash-table)
 (begin
   (let ((sub-name "test-condition-1-ok-1")
         (test-list
          (list
           (list (list 1) #t)
           (list (list 1 2) #t)
           (list (list 2 3 4) #t)
           (list (list 1 2 3 4) #f)
           (list (list 2 3 4 5) #f)
           (list (list 3 5 6 7) #t)
           (list (list 6 9 11 12 13) #t)
           (list (list 11 18 19 20 22 25) #t)
           (list (list 11 18 19 20 22 26) #f)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((set-list (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((power-set (make-power-set set-list)))
                (let ((result (condition-1-ok? set-list power-set)))
                  (let ((err-1
                         (format
                          #f "~a : error (~a) : set-list=~a, "
                          sub-name test-label-index set-list))
                        (err-2
                         (format
                          #f "shouldbe=~a, result=~a"
                          shouldbe result)))
                    (begin
                      (unittest2:assert?
                       (equal? shouldbe result)
                       sub-name
                       (string-append err-1 err-2)
                       result-hash-table)
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (condition-2-ok? set-list)
  (begin
    (let ((ok-flag #t)
          (sum-small (car set-list))
          (sum-large 0)
          (slen (length set-list)))
      (let ((max-ii (euclidean/ slen 2)))
        (begin
          (do ((ii 0 (1+ ii)))
              ((or (>= ii max-ii)
                   (equal? ok-flag #f)))
            (begin
              (let ((next-small
                     (+ sum-small
                        (list-ref set-list (+ ii 1))))
                    (next-large
                     (+ sum-large
                        (list-ref set-list (- slen ii 1)))))
                (begin
                  (if (<= next-small next-large)
                      (begin
                        (set! ok-flag #f))
                      (begin
                        (set! sum-small next-small)
                        (set! sum-large next-large)
                        ))
                  ))
              ))
          ok-flag
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-condition-2-ok-1 result-hash-table)
 (begin
   (let ((sub-name "test-condition-2-ok-1")
         (test-list
          (list
           (list (list 1) #t)
           (list (list 1 2) #t)
           (list (list 1 2 3) #f)
           (list (list 1 2 4) #f)
           (list (list 2 3 4) #t)
           (list (list 1 2 3 4) #f)
           (list (list 2 3 4 5) #f)
           (list (list 3 5 6 7) #t)
           (list (list 6 9 11 12 13) #t)
           (list (list 11 18 19 20 22 25) #t)
           (list (list 11 18 19 20 22 26) #f)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((a-list (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result (condition-2-ok? a-list)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : a-list=~a, "
                        sub-name test-label-index a-list))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define-syntax check-subsets
  (syntax-rules ()
    ((check-subsets
      full-set-list powerset-list ss-set-flag)
     (begin
       (let ((cond-2-flag
              (condition-2-ok? full-set-list)))
         (begin
           (if (equal? cond-2-flag #t)
               (begin
                 (let ((cond-1-flag
                        (condition-1-ok?
                         full-set-list powerset-list)))
                   (begin
                     (if (equal? cond-1-flag #t)
                         (begin
                           (set! ss-set-flag #t))
                         (begin
                           (set! ss-set-flag #f)
                           ))
                     )))
               (begin
                 (set! ss-set-flag #f)
                 ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
;;; assumes sets of length n-1 is consistent,
;;; add 1 element, then check if its ok
(define (find-minimum-special-sum-set nn start-num end-num)
  (define (generate-sets-of-length-nn
           depth max-depth start-num end-num
           current-sum current-list current-powerset-list
           acc-sum acc-list)
    (begin
      (cond
       ((>= depth max-depth)
        (begin
          (if (or (<= acc-sum 0)
                  (< current-sum acc-sum))
              (begin
                (list current-sum (sort current-list <)))
              (begin
                (list acc-sum acc-list)
                ))
          ))
       (else
        (begin
          (let ((continue-loop-flag #t)
                (s-current-list (sort current-list <)))
            (let ((stop-num end-num))
              (begin
                (if (>= (length current-list) 2)
                    (begin
                      (let ((sum-first-two
                             (+ (car current-list)
                                (cadr current-list))))
                        (begin
                          (set!
                           stop-num (min sum-first-two end-num))
                          ))
                      ))

                (do ((kk start-num (1+ kk)))
                    ((or (> kk stop-num)
                         (equal? continue-loop-flag #f)))
                  (begin
                    (let ((next-sum (+ current-sum kk)))
                      (begin
                        (if (or (<= acc-sum 0)
                                (<= next-sum acc-sum))
                            (begin
                              (let ((next-powerset-list
                                     (make-incremental-powerset
                                      current-powerset-list kk)))
                                (let ((next-current-list
                                       (append current-list (list kk)))
                                      (ss-set-flag #t))
                                  (begin
                                    (check-subsets
                                     next-current-list next-powerset-list
                                     ss-set-flag)

                                    (if (equal? ss-set-flag #t)
                                        (begin
                                          (let ((next-list-list
                                                 (generate-sets-of-length-nn
                                                  (1+ depth) max-depth (1+ kk) end-num
                                                  next-sum next-current-list
                                                  next-powerset-list
                                                  acc-sum acc-list)))
                                            (let ((next-acc-sum
                                                   (list-ref next-list-list 0))
                                                  (next-acc-list
                                                   (list-ref next-list-list 1)))
                                              (begin
                                                (set! acc-sum next-acc-sum)
                                                (set! acc-list next-acc-list)
                                                )))
                                          ))
                                    ))
                                ))
                            (begin
                              (set! continue-loop-flag #f)
                              ))
                        ))
                    ))
                (list acc-sum acc-list)
                )))
          )))
      ))
  (begin
    (let ((results-list
           (generate-sets-of-length-nn
            0 nn start-num end-num
            0 (list) (list)
            -1 (list))))
      (begin
        (cadr results-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-find-mininum-special-sum-set-1 result-hash-table)
 (begin
   (let ((sub-name "test-find-minimum-special-sum-set-1")
         (test-list
          (list
           (list 1 1 10 (list 1))
           (list 2 1 10 (list 1 2))
           (list 3 1 10 (list 2 3 4))
           (list 4 1 10 (list 3 5 6 7))
           (list 5 1 20 (list 6 9 11 12 13))
           (list 6 1 30 (list 11 18 19 20 22 25))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((nn (list-ref this-list 0))
                  (start-num (list-ref this-list 1))
                  (end-num (list-ref this-list 2))
                  (shouldbe-list (list-ref this-list 3)))
              (let ((result-list
                     (find-minimum-special-sum-set
                      nn start-num end-num)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "nn=~a, start-num=~a, end-num=~a, "
                        nn start-num end-num))
                      (err-3
                       (format
                        #f "shouldbe list=~a, result list=~a"
                        shouldbe-list result-list)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-list result-list)
                     sub-name
                     (string-append
                      err-1 err-2 err-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop nn-size start-num end-num)
  (begin
    (let ((min-list
           (find-minimum-special-sum-set
            nn-size start-num end-num)))
      (let ((min-sum-string
             (string-join
              (map
               number->string min-list)
              ", "))
            (set-string
             (string-join
              (map number->string min-list)
              "")))
        (begin
          (display
           (format
            #f "  n = ~a : { ~a } : set-string = ~a~%"
            nn-size min-sum-string set-string))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Let S(A) represent the sum of "))
    (display
     (format #f "elements in set A~%"))
    (display
     (format #f "of size n. We shall call it a "))
    (display
     (format #f "special sum set~%"))
    (display
     (format #f "if for any two non-empty disjoint "))
    (display
     (format #f "subsets, B and C,~%"))
    (display
     (format #f "the following properties are true:~%"))
    (newline)
    (display
     (format #f "i.  S(B) != S(C); that is, sums "))
    (display
     (format #f "of subsets cannot be equal.~%"))
    (display
     (format #f "ii. If B contains more elements "))
    (display
     (format #f "than C then S(B) > S(C).~%"))
    (newline)
    (display
     (format #f "If S(A) is minimised for a given n, "))
    (display
     (format #f "we shall call it~%"))
    (display
     (format #f "an optimum special sum set. The first "))
    (display
     (format #f "five optimum special~%"))
    (display
     (format #f "sum sets are given below.~%"))
    (newline)
    (display
     (format #f "  n = 1: {1}~%"))
    (display
     (format #f "  n = 2: {1, 2}~%"))
    (display
     (format #f "  n = 3: {2, 3, 4}~%"))
    (display
     (format #f "  n = 4: {3, 5, 6, 7}~%"))
    (display
     (format #f "  n = 5: {6, 9, 11, 12, 13}~%"))
    (newline)
    (display
     (format #f "It seems that for a given optimum "))
    (display
     (format #f "set,~%"))
    (display
     (format #f "A = {a1, a2, ... , an},~%"))
    (display
     (format #f "the next optimum set is of the form~%"))
    (display
     (format #f "B = {b, a1+b, a2+b, ... ,an+b},~%"))
    (display
     (format #f "where b is the 'middle' element "))
    (display
     (format #f "on the previous row.~%"))
    (newline)
    (display
     (format #f "By applying this 'rule' we would "))
    (display
     (format #f "expect the optimum~%"))
    (display
     (format #f "set for n = 6 to be~%"))
    (display
     (format #f "A = {11, 17, 20, 22, 23, 24},~%"))
    (display
     (format #f "with S(A) = 117. However, this is "))
    (display
     (format #f "not the optimum set,~%"))
    (display
     (format #f "as we have merely applied an "))
    (display
     (format #f "algorithm to provide~%"))
    (display
     (format #f "a near optimum set. The optimum set "))
    (display
     (format #f "for n = 6 is~%"))
    (display
     (format #f "A = {11, 18, 19, 20, 22, 25},~%"))
    (display
     (format #f "with S(A) = 115 and corresponding "))
    (display
     (format #f "set string: 111819202225.~%"))
    (newline)
    (display
     (format #f "Given that A is an optimum special "))
    (display
     (format #f "sum set for n = 7,~%"))
    (display
     (format #f "find its set string.~%"))
    (newline)
    (display
     (format #f "NOTE: This problem is related to "))
    (display
     (format #f "problems 105 and 106.~%"))
    (newline)
    (display
     (format #f "The forward reference to problems "))
    (display
     (format #f "105 and 106 is~%"))
    (display
     (format #f "an important clue. Problem 105 tells "))
    (display
     (format #f "you how to speed~%"))
    (display
     (format #f "up condition 2, and problem 106 shows "))
    (display
     (format #f "how to speed up~%"))
    (display
     (format #f "condition 1 (given that condition 2 "))
    (display
     (format #f "is satisfied).~%"))
    (display
     (format #f "A nice discussion of the problem was "))
    (display
     (format #f "found at~%"))
    (display
     (format #f "https://bartriordan.wordpress.com/2014/04/16/project-euler-problem-103-solution/~%"))
    (display
     (format #f "The idea is to check the sum of "))
    (display
     (format #f "the smallest 2 elements~%"))
    (display
     (format #f "against the largest element, and "))
    (display
     (format #f "checking the sum of~%"))
    (display
     (format #f "the smallest 3 elements against the "))
    (display
     (format #f "sum of the largest~%"))
    (display
     (format #f "2 elements. The relation a1+a2>an was "))
    (display
     (format #f "also used to reduce~%"))
    (display
     (format #f "the largest number in the "))
    (display
     (format #f "do loops.~%"))
    (display
     (format #f "A fast way to check condition 1 "))
    (display
     (format #f "was found at~%"))
    (display
     (format #f "https://jsomers.net/blog/pe-oeis~%"))
    (display
     (format #f "where only non-dominated sets of "))
    (display
     (format #f "equal length need to~%"))
    (display
     (format #f "be compared.~%"))
    (display
     (format #f "see https://projecteuler.net/problem=103~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((num-list-list
           (list (list 1 1 10) (list 2 1 10)
                 (list 3 1 10) (list 4 1 10))))
      (begin
        (for-each
         (lambda (a-list)
           (begin
             (let ((nn-size (list-ref a-list 0))
                   (start-num (list-ref a-list 1))
                   (end-num (list-ref a-list 2)))
               (begin
                 (sub-main-loop nn-size start-num end-num)
                 ))
             )) num-list-list)
        ))

    (newline)
    (let ((num-list-list
           (list (list 5 1 15) (list 6 6 30) (list 7 11 50))))
      (begin
        (for-each
         (lambda (a-list)
           (begin
             (let ((nn-size (list-ref a-list 0))
                   (start-num (list-ref a-list 1))
                   (end-num (list-ref a-list 2)))
               (begin
                 (sub-main-loop nn-size start-num end-num)
                 ))
             (force-output)
             )) num-list-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 103 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

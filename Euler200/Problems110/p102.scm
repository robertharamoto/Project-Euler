#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 102                                    ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated July 28, 2022                                ###
;;;###                                                       ###
;;;###  updated March 11, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-0 rdelim for read-line functions
(use-modules ((ice-9 rdelim)
              :renamer (symbol-prefix-proc 'ice-9-rdelim:)))

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
;;; assume data-list = (p-a-x p-a-y p-b-x p-b-y p-c-x p-c-y)
(define (is-origin-within-triangle? data-list)
  (begin
    (let ((p-a-x (list-ref data-list 0))
          (p-a-y (list-ref data-list 1))
          (p-b-x (list-ref data-list 2))
          (p-b-y (list-ref data-list 3))
          (p-c-x (list-ref data-list 4))
          (p-c-y (list-ref data-list 5))
          (or-x 0)
          (or-y 0))
      (let ((v0-x (- p-b-x p-a-x))
            (v0-y (- p-b-y p-a-y))
            (v1-x (- p-c-x p-a-x))
            (v1-y (- p-c-y p-a-y))
            (v2-x (- or-x p-a-x))
            (v2-y (- or-y p-a-y)))
        (let ((vv00 (+ (* v0-x v0-x) (* v0-y v0-y)))
              (vv11 (+ (* v1-x v1-x) (* v1-y v1-y)))
              (vv01 (+ (* v0-x v1-x) (* v0-y v1-y)))
              (vv20 (+ (* v2-x v0-x) (* v2-y v0-y)))
              (vv21 (+ (* v2-x v1-x) (* v2-y v1-y))))
          (let ((det (- (* vv00 vv11) (* vv01 vv01))))
            (begin
              (if (not (zero? det))
                  (begin
                    (let ((u (/ (- (* vv20 vv11) (* vv21 vv01)) det))
                          (v (/ (- (* vv21 vv00) (* vv20 vv01)) det)))
                      (begin
                        (if (and (>= u 0)
                                 (>= v 0)
                                 (<= (+ u v) 1))
                            (begin
                              #t)
                            (begin
                              #f
                              ))
                        )))
                  (begin
                    (display
                     (format
                      #f "is-origin-within-triangle? ~a error~%"
                      data-list))
                    (display
                     (format
                      #f "determinent = 0~%"))
                    (display
                     (format
                      #f "quitting...~%"))
                    (force-output)
                    (quit)
                    ))
              )))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-is-origin-within-triangle-1 result-hash-table)
 (begin
   (let ((sub-name "test-is-origin-within-triangle-1")
         (test-list
          (list
           (list (list -340 495 -153 -910 835 -947) #t)
           (list (list -175 41 -421 -714 574 -645) #f)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((points-list (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result (is-origin-within-triangle? points-list)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : points-list=~a, "
                        sub-name test-label-index points-list))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; returns a list of lists
(define (read-in-file fname)
  (begin
    (let ((results-list (list))
          (line-counter 0))
      (begin
        (if (file-exists? fname)
            (begin
              (with-input-from-file fname
                (lambda ()
                  (begin
                    (do ((line
                          (ice-9-rdelim:read-delimited "\r\n")
                          (ice-9-rdelim:read-delimited "\r\n")))
                        ((eof-object? line))
                      (begin
                        (if (and (not (eof-object? line))
                                 (> (string-length line) 0))
                            (begin
                              (let ((llist (string-split line #\,)))
                                (let ((plist
                                       (map string->number llist)))
                                  (begin
                                    (set! line-counter (1+ line-counter))
                                    (set!
                                     results-list
                                     (cons plist results-list))
                                    )))
                              ))
                        ))
                    )))

              (display
               (ice-9-format:format
                #f "read in ~a lines from ~a~%"
                line-counter fname))
              (newline)
              (force-output)

              (reverse results-list))
            (begin
              (list)
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (sub-loop triangles-list debug-flag)
  (begin
    (let ((triangles-counter 0)
          (origin-counter 0))
      (begin
        (if (and (list? triangles-list)
                 (> (length triangles-list) 1))
            (begin
              (for-each
               (lambda (a-list)
                 (begin
                   (set!
                    triangles-counter
                    (1+ triangles-counter))

                   (let ((bflag
                          (is-origin-within-triangle? a-list)))
                     (begin
                       (if (equal? bflag #t)
                           (begin
                             (set!
                              origin-counter
                              (1+ origin-counter))
                             ))
                       (if (equal? debug-flag #t)
                           (begin
                             (display
                              (ice-9-format:format
                               #f "points list = ~a : " a-list))
                             (display
                              (ice-9-format:format
                               #f "contains origin = ~a~%"
                               (if (equal? bflag #t) "true" "false")))
                             (force-output)
                             ))
                       ))
                   )) triangles-list)

              (display
               (ice-9-format:format
                #f "there are ~:d triangles that contain the origin, "
                origin-counter))
              (display
               (ice-9-format:format
                #f "out of ~:d~%"
                triangles-counter))
              (force-output)
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop filename debug-flag)
  (begin
    (let ((triangles-list (read-in-file filename)))
      (begin
        (sub-loop triangles-list debug-flag)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Three distinct points are plotted "))
    (display
     (format #f "at random on a~%"))
    (display
     (format #f "Cartesian plane, for which "))
    (display
     (format #f "-1000 <= x, y <= 1000,~%"))
    (display
     (format #f "such that a triangle is formed.~%"))
    (newline)
    (display
     (format #f "Consider the following two triangles:~%"))
    (newline)
    (display
     (format #f "    A(-340,495), B(-153,-910), C(835,-947)~%"))
    (display
     (format #f "    X(-175,41), Y(-421,-714), Z(574,-645)~%"))
    (newline)
    (display
     (format #f "It can be verified that triangle ABC "))
    (display
     (format #f "contains the origin,~%"))
    (display
     (format #f "whereas triangle XYZ does not.~%"))
    (newline)
    (display
     (format #f "Using triangles.txt (right click "))
    (display
     (format #f "and 'Save Link/Target As...'),~%"))
    (display
     (format #f "a 27K text file containing the "))
    (display
     (format #f "co-ordinates of one~%"))
    (display
     (format #f "thousand 'random' triangles, find the "))
    (display
     (format #f "number of triangles~%"))
    (display
     (format #f "for which the interior contains "))
    (display
     (format #f "the origin.~%"))
    (newline)
    (display
     (format #f "NOTE: The first two examples in "))
    (display
     (format #f "the file represent~%"))
    (display
     (format #f "the triangles in the example "))
    (display
     (format #f "given above.~%"))
    (newline)
    (display
     (format #f "this algorithm uses barycentric "))
    (display
     (format #f "coordinates to see if~%"))
    (display
     (format #f "a point is in a triangle,~%"))
    (display
     (format #f "https://euler.stephan-brumme.com/102/~%"))
    (display
     (format #f "see https://projecteuler.net/problem=102~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((points-list-list
           (list (list -340 495 -153 -910 835 -947)
                 (list -175 41 -421 -714 574 -645)))
          (debug-flag #t))
      (begin
        (sub-loop points-list-list debug-flag)
        ))

    (newline)
    (let ((filename "triangles.txt")
          (debug-flag #f))
      (begin
        (sub-main-loop filename debug-flag)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 102 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

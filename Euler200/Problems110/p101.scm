#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 101                                    ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated July 28, 2022                                ###
;;;###                                                       ###
;;;###  updated March 11, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
;;; assume points-list = (p0 p1 p2 p3 ...)
;;; and p0 = f(1), p1 = f(2), p2 = f(3), p3 = f(4) ...
(define (nevilles-algorithm points-list nn)
  (begin
    (let ((points-array (list->array 1 points-list))
          (max-loops (- (length points-list) 1))
          (max-points (- (length points-list) 1)))
      (begin
        (do ((loop 0 (1+ loop)))
            ((>= loop max-loops))
          (begin
            (let ((arr-index (- loop 1)))
              (begin
                (do ((ii 0 (1+ ii)))
                    ((>= ii max-points))
                  (begin
                    (let ((t0 ii)
                          (t1 (+ ii 1 loop)))
                      (let ((p0 (array-ref points-array ii))
                            (p1 (array-ref points-array (+ ii 1))))
                        (let ((denom (- t1 t0)))
                          (let ((factor1 (- t1 nn))
                                (factor2 (- nn t0)))
                            (let ((p0prime
                                   (/ (+
                                       (* p0 factor1)
                                       (* p1 factor2))
                                      denom)))
                              (begin
                                (array-set! points-array p0prime ii)
                                ))
                            ))
                        ))
                    ))
                ))
            ))
        (array-ref points-array 0)
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-nevilles-algorithm-1 result-hash-table)
 (begin
   (let ((sub-name "test-nevilles-algorithm-1")
         (test-list
          (list
           (list (list 1) 1 1)
           (list (list 1 8) 2 15)
           (list (list 1 8 27) 3 58)
           (list (list 1 8 27 64) 4 125)
           (list (list 1 8 27 64 125) 5 216)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((points-list (list-ref this-list 0))
                  (nn (list-ref this-list 1))
                  (shouldbe (list-ref this-list 2)))
              (let ((result (nevilles-algorithm points-list nn)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "points-list=~a, nn=~a : "
                        points-list nn))
                      (err-3
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append
                      err-1 err-2 err-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (third-degree-polynomial nn)
  (begin
    (let ((sum (* nn nn nn)))
      (begin
        sum
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-third-degree-polynomial-1 result-hash-table)
 (begin
   (let ((sub-name "test-third-degree-polynomial-1")
         (test-list
          (list
           (list 1 1)
           (list 2 8)
           (list 3 27)
           (list 4 64)
           (list 5 125)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((nn (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result
                     (third-degree-polynomial nn)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : nn=~a, "
                        sub-name test-label-index nn))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (tenth-degree-polynomial nn)
  (begin
    (let ((coefficents
           (list 1 -1 1 -1 1
                 -1 1 -1 1 -1 1))
          (sum 0))
      (begin
        (do ((jj 0 (1+ jj)))
            ((> jj 10))
          (begin
            (let ((this-coeff (list-ref coefficents jj)))
              (begin
                (set! sum (+ (* nn sum) this-coeff))
                ))
            ))
        sum
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-tenth-degree-polynomial-1 result-hash-table)
 (begin
   (let ((sub-name "test-tenth-degree-polynomial-1")
         (test-list
          (list
           (list 1 1)
           (list 2 683)
           (list 3 44287)
           (list 4 838861)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((nn (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result (tenth-degree-polynomial nn)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : nn=~a, "
                        sub-name test-label-index nn))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; using the tenth-degree-polynomial function compute a list from n = 1 to kk
(define (make-function-list func kk)
  (begin
    (let ((klist (list)))
      (begin
        (do ((ii 1 (1+ ii)))
            ((> ii kk))
          (begin
            (let ((fresult (func ii)))
              (begin
                (set! klist (cons fresult klist))
                ))
            ))

        (reverse klist)
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-make-function-list-1 result-hash-table)
 (begin
   (let ((sub-name "test-make-function-list-1")
         (test-list
          (list
           (list third-degree-polynomial 1 (list 1))
           (list third-degree-polynomial 2 (list 1 8))
           (list third-degree-polynomial 3 (list 1 8 27))
           (list third-degree-polynomial 4 (list 1 8 27 64))
           (list tenth-degree-polynomial 1 (list 1))
           (list tenth-degree-polynomial 2 (list 1 683))
           (list tenth-degree-polynomial 3 (list 1 683 44287))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((func (list-ref alist 0))
                  (kk (list-ref alist 1))
                  (shouldbe (list-ref alist 2)))
              (let ((result (make-function-list func kk)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "func=~a, kk=~a, "
                        func kk))
                      (err-3
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append
                      err-1 err-2 err-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop func end-num)
  (begin
    (let ((sum 0)
          (sum-list (list)))
      (begin
        (do ((kk 1 (1+ kk)))
            ((> kk end-num))
          (begin
            (let ((points-list
                   (make-function-list func kk)))
              (let ((fit
                     (nevilles-algorithm points-list kk))
                    (good-op (func (+ kk 1))))
                (begin
                  (if (not (= fit good-op))
                      (begin
                        (set! sum (+ sum fit))
                        (set! sum-list (cons fit sum-list))
                        ))
                  )))
            ))

        (let ((slist (reverse sum-list)))
          (let ((sum-string
                 (string-join
                  (map
                   (lambda (num)
                     (begin
                       (ice-9-format:format #f "~:d" num)
                       )) slist)
                  " + ")))
            (begin
              (display
               (ice-9-format:format
                #f "sum of the FITs generated by the "))
              (display
               (ice-9-format:format
                #f "BOPs ~a = ~:d~%"
                sum-string sum))
              (force-output)
              )))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "If we are presented with the "))
    (display
     (format #f "first k terms of~%"))
    (display
     (format #f "a sequence it is impossible to "))
    (display
     (format #f "say with certainty~%"))
    (display
     (format #f "the value of the next term, as "))
    (display
     (format #f "there are infinitely~%"))
    (display
     (format #f "many polynomial functions that "))
    (display
     (format #f "can model the sequence.~%"))
    (newline)
    (display
     (format #f "As an example, let us consider "))
    (display
     (format #f "the sequence of~%"))
    (display
     (format #f "cube numbers. This is defined by "))
    (display
     (format #f "the generating function,~%"))
    (display
     (format #f "u(n) = n^3: 1, 8, 27, 64, "))
    (display
     (format #f "125, 216, ...~%"))
    (newline)
    (display
     (format #f "Suppose we were only given the "))
    (display
     (format #f "first two terms of~%"))
    (display
     (format #f "this sequence. Working on the "))
    (display
     (format #f "principle that~%"))
    (display
     (format #f "'simple is best' we should assume "))
    (display
     (format #f "a linear relationship~%"))
    (display
     (format #f "and predict the next term to be 15 "))
    (display
     (format #f "(common difference 7).~%"))
    (display
     (format #f "Even if we were presented with the "))
    (display
     (format #f "first three terms,~%"))
    (display
     (format #f "by the same principle of simplicity, "))
    (display
     (format #f "a quadratic relationship~%"))
    (display
     (format #f "should be assumed.~%"))
    (newline)
    (display
     (format #f "We shall define OP(k, n) to be the "))
    (display
     (format #f "nth term of the~%"))
    (display
     (format #f "optimum polynomial generating "))
    (display
     (format #f "function for the~%"))
    (display
     (format #f "first k terms of a sequence. It "))
    (display
     (format #f "should be clear~%"))
    (display
     (format #f "that OP(k, n) will accurately "))
    (display
     (format #f "generate the terms~%"))
    (display
     (format #f "of the sequence for n <= k, and "))
    (display
     (format #f "potentially the first~%"))
    (display
     (format #f "incorrect term (FIT) will be "))
    (display
     (format #f "OP(k, k+1); in which~%"))
    (display
     (format #f "case we shall call it a bad "))
    (display
     (format #f "OP (BOP).~%"))
    (newline)
    (display
     (format #f "As a basis, if we were only "))
    (display
     (format #f "given the first term~%"))
    (display
     (format #f "of sequence, it would be most "))
    (display
     (format #f "sensible to assume~%"))
    (display
     (format #f "constancy; that is, for n >= 2, "))
    (display
     (format #f "OP(1, n) = u1.~%"))
    (newline)
    (display
     (format #f "Hence we obtain the following "))
    (display
     (format #f "OPs for the~%"))
    (display
     (format #f "cubic sequence:~%"))
    (newline)
    (display
     (format #f "OP(1, n) = 1   1, 1, 1, 1, ...~%"))
    (display
     (format #f "OP(2, n) = 7n-6   1, 8, 15, ...~%"))
    (display
     (format #f "OP(3, n) = 6n^2-11n+6   ~%"))
    (display
     (format #f "1, 8, 27, 58, ...~%"))
    (display
     (format #f "OP(4, n) = n^3   1, 8, 27, 64, 125, ...~%"))
    (newline)
    (display
     (format #f "Clearly no BOPs exist for "))
    (display
     (format #f "k >= 4.~%"))
    (newline)
    (display
     (format #f "By considering the sum of FITs "))
    (display
     (format #f "generated by the BOPs~%"))
    (display
     (format #f "(indicated in red above), we obtain~%"))
    (display
     (format #f "1 + 15 + 58 = 74.~%"))
    (newline)
    (display
     (format #f "Consider the following tenth "))
    (display
     (format #f "degree polynomial generating~%"))
    (display
     (format #f "function:~%"))
    (newline)
    (display
     (format #f "u(n) = 1 - n + n^2 - n^3 + n^4 - n^5 "))
    (display
     (format #f "+ n^6 - n^7 + n^8~%"))
    (display
     (format #f "- n^9 + n^10~%"))
    (newline)
    (display
     (format #f "Find the sum of FITs for the BOPs.~%"))
    (newline)
    (display
     (format #f "the solution to this problem "))
    (display
     (format #f "uses Neville's algorithm~%"))
    (display
     (format #f "https://en.wikipedia.org/wiki/Neville's_algorithm~%"))
    (display
     (format #f "for more details of the derivation "))
    (display
     (format #f "of Neville's algorithm,~%"))
    (display
     (format #f "see https://www.r-bloggers.com/nevilles-method-of-polynomial-interpolation/~%"))
    (display
     (format #f "see https://projecteuler.net/problem=101~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((kth-term 4))
      (begin
        (display (format #f "function = n^3~%"))
        (sub-main-loop third-degree-polynomial kth-term)
        ))

    (newline)
    (let ((kth-term 10))
      (begin
        (display
         (format #f "function = 1 - n + n^2 - n^3 + n^4 - n^5 "))
        (display
         (format #f "+ n^6 - n^7 + n^8 - n^9 + n^10~%"))
        (sub-main-loop tenth-degree-polynomial kth-term)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 101 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

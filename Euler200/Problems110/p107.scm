#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 107                                    ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated July 28, 2022                                ###
;;;###                                                       ###
;;;###  updated March 11, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-0 rdelim for read-line functions
(use-modules ((ice-9 rdelim)
              :renamer (symbol-prefix-proc 'ice-9-rdelim:)))

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (update-next-edge-node
         weight-array-2d min-array-2d max-rows max-cols
         current-node-list remaining-node-list)
  (begin
    (let ((remain-len (length remaining-node-list))
          (current-len (length current-node-list))
          (min-weight -1)
          (min-curr-node -1)
          (min-remain-node -1))
      (begin
        (if (<= current-len 0)
            (begin
              (set! current-len 1)
              (set! current-node-list (list 0))
              ))
        (for-each
         (lambda (curr-node)
           (begin
             (for-each
              (lambda (remain-node)
                (begin
                  (if (not (= remain-node curr-node))
                      (begin
                        (let ((weight
                               (array-ref
                                weight-array-2d curr-node remain-node)))
                          (begin
                            (if (and (> weight 0)
                                     (or (<= min-weight 0)
                                         (< weight min-weight)))
                                (begin
                                  (set! min-weight weight)
                                  (set! min-curr-node curr-node)
                                  (set! min-remain-node remain-node)
                                  ))
                            ))
                        ))
                  )) remaining-node-list)
             )) current-node-list)

        (array-set!
         min-array-2d min-weight min-curr-node min-remain-node)
        (array-set!
         min-array-2d min-weight min-remain-node min-curr-node)

        min-remain-node
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-update-next-edge-node-1 result-hash-table)
 (begin
   (let ((sub-name "test-update-next-edge-node-1")
         (test-list
          (list
           (list (list (list -1 99 88) (list 99 -1 101) (list 88 101 -1))
                 3 3 (list 0) (list 1 2)
                 2 (list (list -1 -1 88) (list -1 -1 -1) (list 88 -1 -1)))
           (list (list (list -1 16 12 21 -1 -1 -1)
                       (list 16 -1 -1 17 20 -1 -1)
                       (list 12 -1 -1 28 -1 31 -1)
                       (list 21 17 28 -1 18 19 23)
                       (list -1 20 -1 18 -1 -1 11)
                       (list -1 -1 31 19 -1 -1 27)
                       (list -1 -1 -1 23 11 27 -1))
                 7 7 (list 0) (list 1 2 3 4 5 6)
                 2 (list (list -1 -1 12 -1 -1 -1 -1)
                         (list -1 -1 -1 -1 -1 -1 -1)
                         (list 12 -1 -1 -1 -1 -1 -1)
                         (list -1 -1 -1 -1 -1 -1 -1)
                         (list -1 -1 -1 -1 -1 -1 -1)
                         (list -1 -1 -1 -1 -1 -1 -1)
                         (list -1 -1 -1 -1 -1 -1 -1)))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((weight-list-list (list-ref this-list 0))
                  (max-rows (list-ref this-list 1))
                  (max-cols (list-ref this-list 2))
                  (current-nodes (list-ref this-list 3))
                  (remaining-nodes (list-ref this-list 4))
                  (shouldbe-node (list-ref this-list 5))
                  (shouldbe-list-list (list-ref this-list 6)))
              (let ((weight-arr-2d
                     (list->array 2 weight-list-list))
                    (result-min-arr-2d
                     (make-array -1 max-rows max-cols))
                    (shouldbe-min-arr-2d
                     (list->array 2 shouldbe-list-list)))
                (let ((result-node
                       (update-next-edge-node
                        weight-arr-2d result-min-arr-2d
                        max-rows max-cols
                        current-nodes remaining-nodes)))
                  (let ((err-1
                         (format
                          #f "~a : error (~a) : "
                          sub-name test-label-index))
                        (err-2
                         (format
                          #f "weight-list=~a, current-nodes=~a, "
                          weight-list-list current-nodes))
                        (err-3
                         (format
                          #f "shouldbe node=~a, result node=~a"
                          shouldbe-node result-node)))
                    (begin
                      (unittest2:assert?
                       (equal? shouldbe-node result-node)
                       sub-name
                       (string-append
                        err-1 err-2 err-3)
                       result-hash-table)

                      (do ((ii 0 (1+ ii)))
                          ((>= ii max-rows))
                        (begin
                          (let ((shouldbe-row
                                 (list-ref shouldbe-list-list ii)))
                            (begin
                              (do ((jj 0 (1+ jj)))
                                  ((>= jj max-rows))
                                (begin
                                  (let ((sweight
                                         (list-ref shouldbe-row jj))
                                        (rweight
                                         (array-ref result-min-arr-2d ii jj)))
                                    (let ((err-4
                                           (format
                                            #f "[~a][~a] shouldbe weight=~a, "
                                            ii jj sweight))
                                          (err-5
                                           (format
                                            #f "result weight=~a"
                                            rweight)))
                                      (begin
                                        (unittest2:assert?
                                         (equal? sweight rweight)
                                         sub-name
                                         (string-append
                                          err-1 err-2 err-4 err-5)
                                         result-hash-table)
                                        )))
                                  ))
                              ))
                          ))
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (remove-current-nodes-from-remaining-nodes
         current-node-list remaining-node-list)
  (begin
    (for-each
     (lambda (c-node)
       (begin
         (if (not
              (equal?
               (member c-node remaining-node-list)
               #f))
             (begin
               (set!
                remaining-node-list
                (delete c-node remaining-node-list))
               ))
         )) current-node-list)

    remaining-node-list
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-remove-current-nodes-from-remaining-nodes-1 result-hash-table)
 (begin
   (let ((sub-name "test-remove-current-nodes-from-remaining-nodes-1")
         (test-list
          (list
           (list (list 0) (list 0 1 2 3) (list 1 2 3))
           (list (list 0 1) (list 0 1 2 3) (list 2 3))
           (list (list 0 1 2) (list 0 1 2 3) (list 3))
           (list (list 0 1 2 3) (list 0 1 2 3) (list))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (a-list)
          (begin
            (let ((current-list (list-ref a-list 0))
                  (remaining-list (list-ref a-list 1))
                  (shouldbe-list (list-ref a-list 2)))
              (let ((result-list
                     (remove-current-nodes-from-remaining-nodes
                      current-list remaining-list)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "current-list=~a, remaining-list=~a, "
                        current-list remaining-list))
                      (err-3
                       (format
                        #f "shouldbe-list=~a, result-list=~a"
                        shouldbe-list result-list)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-list result-list)
                     sub-name
                     (string-append
                      err-1 err-2 err-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (find-minimal-spanning-network
         weight-array-2d min-array-2d
         max-rows max-cols)
  (begin
    (let ((remaining-node-list (list))
          (current-node-list (list 0))
          (continue-loop-flag #t))
      (begin
        (do ((ii 1 (1+ ii)))
            ((>= ii max-rows))
          (begin
            (set!
             remaining-node-list
             (cons ii remaining-node-list))
            ))
        (set!
         remaining-node-list
         (reverse remaining-node-list))

        (while
         (equal? continue-loop-flag #t)
         (begin
           (let ((next-node
                  (update-next-edge-node
                   weight-array-2d min-array-2d
                   max-rows max-cols
                   current-node-list remaining-node-list)))
             (begin
               (if (< next-node 0)
                   (begin
                     (set! continue-loop-flag #f))
                   (begin
                     (set!
                      current-node-list
                      (cons next-node current-node-list))
                     (let ((next-remaining-node-list
                            (remove-current-nodes-from-remaining-nodes
                             current-node-list remaining-node-list)))
                       (begin
                         (if (or
                              (not (list? next-remaining-node-list))
                              (<= (length next-remaining-node-list) 0))
                             (begin
                               (set! continue-loop-flag #f))
                             (begin
                               (set!
                                remaining-node-list
                                next-remaining-node-list)
                               ))
                         ))
                     ))
               ))
           ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-find-minimal-spanning-network-1 result-hash-table)
 (begin
   (let ((sub-name "test-find-minimal-spanning-network-1")
         (test-list
          (list
           (list (list (list -1 99 88)
                       (list 99 -1 101)
                       (list 88 101 -1))
                 3 3 (list (list -1 99 88)
                           (list 99 -1 -1)
                           (list 88 -1 -1)))
           (list (list (list -1 99 88)
                       (list 99 -1 50)
                       (list 88 50 -1))
                 3 3 (list (list -1 -1 88)
                           (list -1 -1 50)
                           (list 88 50 -1)))
           (list (list (list -1 -1 88)
                       (list -1 -1 101)
                       (list 88 101 -1))
                 3 3 (list (list -1 -1 88)
                           (list -1 -1 101)
                           (list 88 101 -1)))
           (list (list (list -1 16 12 21 -1 -1 -1)
                       (list 16 -1 -1 17 20 -1 -1)
                       (list 12 -1 -1 28 -1 31 -1)
                       (list 21 17 28 -1 18 19 23)
                       (list -1 20 -1 18 -1 -1 11)
                       (list -1 -1 31 19 -1 -1 27)
                       (list -1 -1 -1 23 11 27 -1))
                 7 7
                 (list (list -1 16 12 -1 -1 -1 -1)
                       (list 16 -1 -1 17 -1 -1 -1)
                       (list 12 -1 -1 -1 -1 -1 -1)
                       (list -1 17 -1 -1 18 19 -1)
                       (list -1 -1 -1 18 -1 -1 11)
                       (list -1 -1 -1 19 -1 -1 -1)
                       (list -1 -1 -1 -1 11 -1 -1)))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((input-list-list (list-ref this-list 0))
                  (max-rows (list-ref this-list 1))
                  (max-cols (list-ref this-list 2))
                  (shouldbe-list-list (list-ref this-list 3)))
              (let ((arr-2d (list->array 2 input-list-list))
                    (min-array-2d (make-array -1 max-rows max-cols)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : input-list-list=~a, "
                        sub-name test-label-index input-list-list)))
                  (begin
                    (find-minimal-spanning-network
                     arr-2d min-array-2d max-rows max-cols)

                    (do ((ii 0 (1+ ii)))
                        ((>= ii max-rows))
                      (begin
                        (let ((shouldbe-row
                               (list-ref shouldbe-list-list ii)))
                          (begin
                            (do ((jj 0 (1+ jj)))
                                ((>= jj max-cols))
                              (begin
                                (let ((sweight (list-ref shouldbe-row jj))
                                      (rweight (array-ref min-array-2d ii jj)))
                                  (let ((err-2
                                         (format
                                          #f "[~a][~a] shouldbe weight=~a, "
                                          ii jj sweight))
                                        (err-3
                                         (format
                                          #f "result weight=~a"
                                          rweight)))
                                    (begin
                                      (unittest2:assert?
                                       (equal? sweight rweight)
                                       sub-name
                                       (string-append
                                        err-1 err-2 err-3)
                                       result-hash-table)
                                      )))
                                ))
                            ))
                        ))
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (sum-of-the-weights array-2d max-rows max-cols)
  (begin
    (let ((weight-sum 0))
      (begin
        (do ((ii 0 (1+ ii)))
            ((>= ii max-rows))
          (begin
            (do ((jj ii (1+ jj)))
                ((>= jj max-cols))
              (begin
                (let ((elem (array-ref array-2d ii jj)))
                  (begin
                    (if (> elem 0)
                        (begin
                          (set! weight-sum (+ weight-sum elem))
                          ))
                    ))
                ))
            ))

        weight-sum
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-sum-of-the-weights-1 result-hash-table)
 (begin
   (let ((sub-name "test-sum-of-the-weights-1")
         (test-list
          (list
           (list 3 3 (list (list -1 99 88) (list 99 -1 101) (list 88 101 -1)) 288)
           (list 3 3 (list (list -1 -1 88) (list -1 -1 101) (list 88 101 -1)) 189)
           (list 7 7 (list (list -1 16 12 21 -1 -1 -1)
                           (list 16 -1 -1 17 20 -1 -1)
                           (list 12 -1 -1 28 -1 31 -1)
                           (list 21 17 28 -1 18 19 23)
                           (list -1 20 -1 18 -1 -1 11)
                           (list -1 -1 31 19 -1 -1 27)
                           (list -1 -1 -1 23 11 27 -1)) 243)
           (list 7 7 (list (list -1 16 12 21 -1 -1 -1)
                           (list 16 -1 -1 17 20 -1 -1)
                           (list 12 -1 -1 28 -1 31 -1)
                           (list 21 17 28 -1 18 19 -1)
                           (list -1 20 -1 18 -1 -1 -1)
                           (list -1 -1 31 19 -1 -1 -1)
                           (list -1 -1 -1 -1 -1 -1 -1)) 182)
           (list 7 7 (list (list -1 16 12 -1 -1 -1 -1)
                           (list 16 -1 -1 17 -1 -1 -1)
                           (list 12 -1 -1 -1 -1 -1 -1)
                           (list -1 17 -1 -1 18 19 -1)
                           (list -1 -1 -1 18 -1 -1 11)
                           (list -1 -1 -1 19 -1 -1 -1)
                           (list -1 -1 -1 -1 11 -1 -1)) 93)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((max-rows (list-ref this-list 0))
                  (max-cols (list-ref this-list 1))
                  (llist (list-ref this-list 2))
                  (shouldbe (list-ref this-list 3)))
              (let ((arr-2d (list->array 2 llist)))
                (let ((result (sum-of-the-weights arr-2d max-rows max-cols)))
                  (let ((err-1
                         (format
                          #f "~a : error (~a) : llist=~a, "
                          sub-name test-label-index llist))
                        (err-2
                         (format
                          #f "shouldbe=~a, result=~a"
                          shouldbe result)))
                    (begin
                      (unittest2:assert?
                       (equal? shouldbe result)
                       sub-name
                       (string-append err-1 err-2)
                       result-hash-table)
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (count-of-the-weights array-2d max-rows max-cols)
  (begin
    (let ((weight-count 0))
      (begin
        (do ((ii 0 (1+ ii)))
            ((>= ii max-rows))
          (begin
            (do ((jj ii (1+ jj)))
                ((>= jj max-cols))
              (begin
                (let ((elem (array-ref array-2d ii jj)))
                  (begin
                    (if (> elem 0)
                        (begin
                          (set! weight-count (1+ weight-count))
                          ))
                    ))
                ))
            ))

        weight-count
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-count-of-the-weights-1 result-hash-table)
 (begin
   (let ((sub-name "test-count-of-the-weights-1")
         (test-list
          (list
           (list 3 3 (list (list -1 99 88) (list 99 -1 101) (list 88 101 -1)) 3)
           (list 3 3 (list (list -1 -1 88) (list -1 -1 101) (list 88 101 -1)) 2)
           (list 7 7 (list (list -1 16 12 21 -1 -1 -1)
                           (list 16 -1 -1 17 20 -1 -1)
                           (list 12 -1 -1 28 -1 31 -1)
                           (list 21 17 28 -1 18 19 23)
                           (list -1 20 -1 18 -1 -1 11)
                           (list -1 -1 31 19 -1 -1 27)
                           (list -1 -1 -1 23 11 27 -1)) 12)
           (list 7 7 (list (list -1 16 12 21 -1 -1 -1)
                           (list 16 -1 -1 17 20 -1 -1)
                           (list 12 -1 -1 28 -1 31 -1)
                           (list 21 17 28 -1 18 19 -1)
                           (list -1 20 -1 18 -1 -1 -1)
                           (list -1 -1 31 19 -1 -1 -1)
                           (list -1 -1 -1 -1 -1 -1 -1)) 9)
           (list 7 7 (list (list -1 16 12 -1 -1 -1 -1)
                           (list 16 -1 -1 17 -1 -1 -1)
                           (list 12 -1 -1 -1 -1 -1 -1)
                           (list -1 17 -1 -1 18 19 -1)
                           (list -1 -1 -1 18 -1 -1 11)
                           (list -1 -1 -1 19 -1 -1 -1)
                           (list -1 -1 -1 -1 11 -1 -1)) 6)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((max-rows (list-ref this-list 0))
                  (max-cols (list-ref this-list 1))
                  (llist (list-ref this-list 2))
                  (shouldbe (list-ref this-list 3)))
              (let ((arr-2d (list->array 2 llist)))
                (let ((result (count-of-the-weights arr-2d max-rows max-cols)))
                  (let ((err-1
                         (format
                          #f "~a : error (~a) : llist=~a, "
                          sub-name test-label-index llist))
                        (err-2
                         (format
                          #f "shouldbe=~a, result=~a"
                          shouldbe result)))
                    (begin
                      (unittest2:assert?
                       (equal? shouldbe result)
                       sub-name
                       (string-append err-1 err-2)
                       result-hash-table)
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; returns a list of lists
(define (read-in-file fname)
  (begin
    (let ((results-list (list))
          (line-counter 0))
      (begin
        (if (file-exists? fname)
            (begin
              (with-input-from-file fname
                (lambda ()
                  (begin
                    (do ((line
                          (ice-9-rdelim:read-delimited "\r\n")
                          (ice-9-rdelim:read-delimited "\r\n")))
                        ((eof-object? line))
                      (begin
                        (if (and
                             (not (eof-object? line))
                             (> (string-length line) 0))
                            (begin
                              (let ((llist (string-split line #\,)))
                                (let ((plist
                                       (map
                                        (lambda (this-string)
                                          (begin
                                            (let ((this-num
                                                   (string->number this-string)))
                                              (begin
                                                (cond
                                                 ((equal? this-num #f)
                                                  (begin
                                                    -1
                                                    ))
                                                 (else
                                                  (begin
                                                    this-num
                                                    )))
                                                ))
                                            )) llist)))
                                  (begin
                                    (set! line-counter (1+ line-counter))
                                    (set!
                                     results-list
                                     (cons plist results-list))
                                    )))
                              ))
                        ))
                    )))

              (display
               (ice-9-format:format
                #f "read in ~a lines from ~a~%" line-counter fname))
              (newline)
              (force-output)

              (reverse results-list))
            (begin
              (list)
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (display-results
         original-weights-array min-array-2d
         max-rows max-cols)
  (begin
    (let ((initial-weights-sum
           (sum-of-the-weights
            original-weights-array max-rows max-cols))
          (initial-weights-count
           (count-of-the-weights
            original-weights-array max-rows max-cols))
          (min-weights-sum
           (sum-of-the-weights
            min-array-2d max-rows max-cols))
          (min-weights-count
           (count-of-the-weights
            min-array-2d max-rows max-cols)))
      (begin
        (display
         (ice-9-format:format
          #f "initial weight sum = ~:d, "
          initial-weights-sum))
        (display
         (ice-9-format:format
          #f "number of weights = ~:d~%"
          initial-weights-count))
        (display
         (ice-9-format:format
          #f "minimum weight sum = ~:d, "
          min-weights-sum))
        (display
         (ice-9-format:format
          #f "number of weights = ~:d~%"
          min-weights-count))
        (display
         (ice-9-format:format
          #f "maximum weight savings = ~:d, "
          (- initial-weights-sum min-weights-sum)))
        (display
         (ice-9-format:format
          #f "change in number of weights = ~:d~%"
          (- initial-weights-count min-weights-count)))
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (reproduce-problem-statement)
  (let ((init-list
         (list (list -1 16 12 21 -1 -1 -1)
               (list 16 -1 -1 17 20 -1 -1)
               (list 12 -1 -1 28 -1 31 -1)
               (list 21 17 28 -1 18 19 23)
               (list -1 20 -1 18 -1 -1 11)
               (list -1 -1 31 19 -1 -1 27)
               (list -1 -1 -1 23 11 27 -1)))
        (max-rows 7)
        (max-cols 7))
    (let ((network-array (list->array 2 init-list))
          (min-array-2d (make-array -1 max-rows max-cols)))
      (begin
        (display (format #f "initial matrix:~%"))
        (do ((ii 0 (1+ ii)))
            ((>= ii max-rows))
          (begin
            (let ((this-row (list-ref init-list ii)))
              (begin
                (display (format #f "    ~a~%" this-row))
                ))
            ))
        (force-output)

        (find-minimal-spanning-network
         network-array min-array-2d max-rows max-cols)

        (display-results
         network-array min-array-2d max-rows max-cols)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop filename)
  (begin
    (let ((network-list-list (read-in-file filename)))
      (let ((max-rows (length network-list-list))
            (max-cols (length (list-ref network-list-list 0)))
            (network-array (list->array 2 network-list-list)))
        (let ((min-array-2d (make-array -1 max-rows max-cols)))
          (begin
            (find-minimal-spanning-network
             network-array min-array-2d max-rows max-cols)

            (display-results
             network-array min-array-2d max-rows max-cols)
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "The following undirected network "))
    (display
     (format #f "consists of seven~%"))
    (display
     (format #f "vertices and twelve edges with a "))
    (display
     (format #f "total weight of 243.~%"))
    (newline)
    (display
     (format #f "The same network can be represented "))
    (display
     (format #f "by the matrix below.~%"))
    (newline)
    (display
     (format #f "       A       B       C       D       "))
    (display
     (format #f "E       F       G~%"))
    (display
     (format #f "A      -       16      12      21      "))
    (display
     (format #f "-       -       -~%"))
    (display
     (format #f "B      16      -       -       17      "))
    (display
     (format #f "20      -       -~%"))
    (display
     (format #f "C      12      -       -       28      "))
    (display
     (format #f "-       31      -~%"))
    (display
     (format #f "D      21      17      28      -       "))
    (display
     (format #f "18      19      23~%"))
    (display
     (format #f "E      -       20      -       18      "))
    (display
     (format #f "-       -       11~%"))
    (display
     (format #f "F      -       -       31      19      "))
    (display
     (format #f "-       -       27~%"))
    (display
     (format #f "G      -       -       -       23      "))
    (display
     (format #f "11      27      -~%"))
    (newline)
    (display
     (format #f "However, it is possible to optimise "))
    (display
     (format #f "the network by~%"))
    (display
     (format #f "removing some edges and still ensure "))
    (display
     (format #f "that all points~%"))
    (display
     (format #f "on the network remain connected. The "))
    (display
     (format #f "network which achieves~%"))
    (display
     (format #f "the maximum saving is shown below. It "))
    (display
     (format #f "has a weight of 93,~%"))
    (display
     (format #f "representing a saving of 243 - 93 = "))
    (display
     (format #f "150 from the~%"))
    (display
     (format #f "original network.~%"))
    (newline)
    (display
     (format #f "Using network.txt (right click and "))
    (display
     (format #f "'Save Link/Target As...'),~%"))
    (display
     (format #f "a 6K text file containing a network "))
    (display
     (format #f "with forty vertices,~%"))
    (display
     (format #f "and given in matrix form, find the "))
    (display
     (format #f "maximum saving which~%"))
    (display
     (format #f "can be achieved by removing redundant "))
    (display
     (format #f "edges whilst ensuring~%"))
    (display
     (format #f "that the network remains connected.~%"))
    (newline)
    (display
     (format #f "For an introduction to minimum spanning "))
    (display
     (format #f "trees see~%"))
    (display
     (format #f "https://en.wikipedia.org/wiki/Minimum_spanning_tree~%"))
    (newline)
    (display
     (format #f "The solution used was Prim's algorithm, "))
    (display
     (format #f "described at~%"))
    (display
     (format #f "https://en.wikipedia.org/wiki/Prim's_algorithm~%"))
    (display
     (format #f "see https://projecteuler.net/problem=107~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (reproduce-problem-statement)

    (newline)
    (let ((filename "network.txt"))
      (begin
        (sub-main-loop filename)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 107 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

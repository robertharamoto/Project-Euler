#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 142                                    ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated August 1, 2022                               ###
;;;###                                                       ###
;;;###  updated March 12, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### srfi-19 for date functions
(use-modules ((srfi srfi-19)
              :renamer (symbol-prefix-proc 'srfi-19:)))

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
;;;### uses the algorithm found at https://stackoverflow.com/questions/295579/fastest-way-to-determine-if-an-integers-square-root-is-an-integer
;;;### (logand 1^2 15) = 1, (logand 2^2 15) = 4, (logand 3^2 15) = 9,
;;;### (logand 4^2 15) = 0, (logand 5^2 15) = 9, (logand 6^2 15) = 4,
;;;### (logand 7^2 15) = 1, (logand 8^2 15) = 0, (logand 9^2 15) = 1,
;;;### (logand 10^2 15) = 4, (logand 11^2 15) = 9, (logand 12^2 15) = 0,
;;;### ...
(define (is-perfect-square? anum)
  (begin
    (let ((lan (logand anum 15)))
      (begin
        (cond
         ((< anum 0)
          (begin
            #f
            ))
         ((or (equal? lan 0) (equal? lan 1)
              (equal? lan 4) (equal? lan 9))
          (begin
            (let ((ltmp (exact-integer-sqrt anum)))
              (begin
                (equal? (* ltmp ltmp) anum)
                ))
            ))
         (else
          (begin
            #f
            )))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-is-perfect-square-1 result-hash-table)
 (begin
   (let ((sub-name "test-is-perfect-square-1")
         (test-list
          (list
           (list -10 #f) (list 0 #t) (list 1 #t)
           (list 2 #f) (list 3 #f) (list 4 #t) (list 5 #f)
           (list 6 #f) (list 7 #f) (list 8 #f) (list 9 #t)
           (list 10 #f) (list 11 #f) (list 12 #f) (list 13 #f)
           (list 14 #f) (list 15 #f) (list 16 #t) (list 17 #f)
           (list 18 #f) (list 19 #f) (list 20 #f) (list 21 #f)
           (list 22 #f) (list 23 #f) (list 24 #f) (list 25 #t)
           (list 26 #f) (list 27 #f) (list 28 #f) (list 29 #f)
           (list 30 #f) (list 31 #f) (list 32 #f) (list 33 #f)
           (list 36 #t) (list 49 #t) (list 64 #t) (list 81 #t)
           (list 100 #t) (list 121 #t) (list 144 #t) (list 169 #t)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((nn (list-ref this-list 0))
                  (shouldbe-bool (list-ref this-list 1)))
              (let ((result-bool (is-perfect-square? nn)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : nn=~a, "
                        sub-name test-label-index nn))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe-bool result-bool)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-bool result-bool)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define-syntax display-is-perfect-square-macro
  (syntax-rules ()
    ((display-is-perfect-square-macro sq-num dstring)
     (begin
       (if (>= sq-num 0)
           (begin
             (let ((bflag (is-perfect-square? sq-num))
                   (sqrt-num (exact-integer-sqrt sq-num)))
               (begin
                 (display
                  (ice-9-format:format
                   #f "~a = ~:d = ~d^2~a~%"
                   dstring sq-num sqrt-num
                   (if bflag "" "******error*******")
                   ))
                 ))
             ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax display-debug-info-macro
  (syntax-rules ()
    ((display-debug-info-macro
      xx yy zz rsum min-xx min-yy min-zz min-sum count)
     (begin
       (display
        (ice-9-format:format
         #f "  (xx, yy, zz) = (~:d, ~:d, ~:d), sum = ~:d~%"
         xx yy zz rsum))
       (display
        (ice-9-format:format
         #f "  min = (~:d, ~:d, ~:d), sum = ~:d, count=~:d~%"
         min-xx min-yy min-zz min-sum count))
       (display-is-perfect-square-macro (+ xx yy) "x+y")
       (display-is-perfect-square-macro (- xx yy) "x-y")
       (display-is-perfect-square-macro (+ xx zz) "x+z")
       (display-is-perfect-square-macro (- xx zz) "x-z")
       (display-is-perfect-square-macro (+ yy zz) "y+z")
       (display-is-perfect-square-macro (- yy zz) "y-z")
       (force-output)
       ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax display-status-info-macro
  (syntax-rules ()
    ((display-status-info-macro
      sq-counter max-ii sum count start-jday)
     (begin
       (let ((end-jday (srfi-19:current-julian-day)))
         (begin
           (display
            (ice-9-format:format
             #f "~:d / ~:d : sum = ~:d, count = ~:d~%"
             sq-counter max-ii sum count))
           (display
            (format
             #f "    elapsed time = ~a : ~a~%"
             (timer-module:julian-day-difference-to-string
              end-jday start-jday)
             (timer-module:current-date-time-string)))
           (force-output)
           (set! start-jday end-jday)
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax display-plus-line-macro
  (syntax-rules ()
    ((display-plus-line-macro astring min-xx min-yy)
     (begin
       (let ((xy (+ min-xx min-yy)))
         (let ((xy-sqr (exact-integer-sqrt xy)))
           (begin
             (display
              (ice-9-format:format
               #f "  ~a = ~:d = ~:d^2~%" astring xy xy-sqr))
             (force-output)
             )))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax display-minus-line-macro
  (syntax-rules ()
    ((display-minus-line-macro astring min-xx min-yy)
     (begin
       (let ((xy (- min-xx min-yy)))
         (let ((xy-sqr (exact-integer-sqrt xy)))
           (begin
             (display
              (ice-9-format:format
               #f "  ~a = ~:d = ~:d^2~%" astring xy xy-sqr))
             (force-output)
             )))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax process-inner-loop-macro
  (syntax-rules ()
    ((process-inner-loop-macro
      max-num aa-2 bb bb-2
      min-sum min-xx min-yy min-zz
      count debug-flag)
     (begin
       (let ((end-loop-flag #f)
             (start-ee (+ bb 2))
             (xtmp (+ aa-2 aa-2 bb-2)))
         (begin
           (do ((ee start-ee (+ ee 2)))
               ((or (> ee max-num)
                    (equal? end-loop-flag #t)))
             (begin
               (let ((ee-2 (* ee ee)))
                 (let ((xx (/ (+ xtmp ee-2) 2))
                       (yy (/ (+ ee-2 bb-2) 2))
                       (zz (/ (- ee-2 bb-2) 2)))
                   (begin
                     (let ((sum (+ xx yy zz)))
                       (begin
                         (if (and
                              (is-perfect-square? (+ xx yy))
                              (is-perfect-square? (+ xx zz))
                              (is-perfect-square? (- xx zz)))
                             (begin
                               (set! count (1+ count))

                               (if (equal? debug-flag #t)
                                   (begin
                                     (display-debug-info-macro
                                      xx yy zz sum min-xx min-yy min-zz
                                      min-sum count)
                                     ))

                               (if (or (< min-sum 0)
                                       (< sum min-sum))
                                   (begin
                                     (set! min-sum sum)
                                     (set! min-xx xx)
                                     (set! min-yy yy)
                                     (set! min-zz zz)
                                     ))
                               ))

                         (if (and (> min-sum 0)
                                  (> sum min-sum))
                             (begin
                               (set! end-loop-flag #t)
                               ))
                         ))
                     )))
               ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop max-num status-num debug-flag)
  (begin
    (let ((min-xx -1)
          (min-yy -1)
          (min-zz -1)
          (min-sum -1)
          (count 0)
          (start-jday (srfi-19:current-julian-day)))
      (begin
        (do ((aa 1 (1+ aa)))
            ((> aa max-num))
          (begin
            (let ((aa-2 (* aa aa)))
              (begin
                (do ((bb (1+ aa) (1+ bb)))
                    ((> bb max-num))
                  (begin
                    (let ((bb-2 (* bb bb)))
                      (begin
                        (if (is-perfect-square? (+ aa-2 bb-2))
                            (begin
                              (process-inner-loop-macro
                               max-num aa-2 bb bb-2
                               min-sum min-xx min-yy min-zz
                               count debug-flag)
                              ))
                        ))
                    ))
                ))

            (if (zero? (modulo aa status-num))
                (begin
                  (display-status-info-macro
                   aa max-num min-sum count
                   start-jday)
                  ))
            ))

        (if (> min-sum 0)
            (begin
              (display
               (ice-9-format:format
                #f "The minimum sum = ~:d, (x, y, z) = (~:d, ~:d, ~:d).~%"
                min-sum min-xx min-yy min-zz))
              (force-output)

              (display-minus-line-macro "x-y" min-xx min-yy)
              (display-minus-line-macro "y-z" min-yy min-zz)
              (display-minus-line-macro "x-z" min-xx min-zz)
              (display-plus-line-macro "y+z" min-yy min-zz)
              (display-plus-line-macro "x+y" min-xx min-yy)
              (display-plus-line-macro "x+z" min-xx min-zz))
            (begin
              (display (format #f "no results found.~%"))
              ))

        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Find the smallest x + y + z "))
    (display
     (format #f "with integers~%"))
    (display
     (format #f "x > y > z > 0 such that x + y, "))
    (display
     (format #f "x - y, x + z,~%"))
    (display
     (format #f "x - z, y + z, y - z are all "))
    (display
     (format #f "perfect squares.~%"))
    (newline)
    (display
     (format #f "Let x-y=a^2, y-z=b^2, x-z=c^2, "))
    (display
     (format #f "x+y=d^2, y+z=e^2,~%"))
    (display
     (format #f "x+z=f^2.~%"))
    (newline)
    (display
     (format #f "(1) x=(a^2+d^2)/2, (2) y=(b^2+e^2)/2, "))
    (display
     (format #f "(3) z=(f^2-c^2)/2~%"))
    (display
     (format #f "(4) x=(c^2+f^2)/2, (5) y=(d^2-a^2)/2, "))
    (display
     (format #f "(6) z=(e^2-b^2)/2.~%"))
    (newline)
    (display
     (format #f "from x=(a^2+d^2)/2=(c^2+f^2)/2 "))
    (display
     (format #f "we see that~%"))
    (display
     (format #f "f^2=a^2+d^2-c^2.~%"))
    (display
     (format #f "from y=(b^2+e^2)/2=(d^2-a^2)/2 "))
    (display
     (format #f "we see that~%"))
    (display
     (format #f "d^2=e^2+a^2+b^2. (7)~%"))
    (display
     (format #f "from z=(f^2-c^2)/2=(e^2-b^2)/2 "))
    (display
     (format #f "we see that~%"))
    (display
     (format #f "f^2=e^2-b^2+c^2.~%"))
    (display
     (format #f "from d^2=x+y=(c^2+f^2+b^2+e^2)/2"))
    (display
     (format #f "=(c^2+e^2-b^2+c^2+b^2+e^2)/2~%"))
    (display
     (format #f "=e^2+c^2~%"))
    (display
     (format #f "from (7), d^2=e^2+c^2=e^2+a^2+b^2 "))
    (display
     (format #f "-> a^2+b^2=c^2~%"))
    (display
     (format #f "(pythagorean theorem)~%"))
    (display
     (format #f "which is from the identity "))
    (display
     (format #f "(x-y) + (y-z) = (x-z)~%"))
    (display
     (format #f "see also: https://euler.stephan-brumme.com/142/~%"))
    (newline)
    (display
     (format #f "So we can eliminate c, d, and f.~%"))
    (display
     (format #f "(8) x=(2a^2+b^2+e^2)/2,~%"))
    (display
     (format #f "(9) y=(e^2+b^2)/2,~%"))
    (display
     (format #f "(10) z=(e^2-b^2)/2~%"))
    (display
     (format #f "Since x, y, and z are integers, "))
    (display
     (format #f "we see that when~%"))
    (display
     (format #f "b is odd, then e must be odd, "))
    (display
     (format #f "and when b is~%"))
    (display
     (format #f "even, e must be even. This will "))
    (display
     (format #f "help to reduce the~%"))
    (display
     (format #f "amount of work done in the "))
    (display
     (format #f "innner-most loop.~%"))
    (newline)
    (display
     (format #f "from x>y, (2a^2+b^2+e^2)/2>(e^2+b^2)/2, "))
    (display
     (format #f "always true.~%"))
    (display
     (format #f "from y>z, (e^2+b^2)/2>(e^2-b^2)/2, "))
    (display
     (format #f "always true.~%"))
    (display
     (format #f "from z>0, (e^2-b^2)/2>0, "))
    (display
     (format #f "implies e^2>b^2~%"))
    (display
     (format #f "see https://projecteuler.net/problem=142~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((max-num 1000)
          (status-num 1000)
          (debug-flag #t))
      (begin
        (sub-main-loop max-num status-num debug-flag)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 142 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

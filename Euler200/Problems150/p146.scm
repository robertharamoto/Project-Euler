#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 146                                    ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated August 1, 2022                               ###
;;;###                                                       ###
;;;###  updated March 12, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### srfi-11 for let-values (multiple value bind)
(use-modules ((srfi srfi-11)
              :renamer (symbol-prefix-proc 'srfi-11:)))

;;;### srfi-19 for date functions
(use-modules ((srfi srfi-19)
              :renamer (symbol-prefix-proc 'srfi-19:)))

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
;;; miller-rabin version of exp-mod
(define (mr-exp-mod base exp mm)
  (begin
    (cond
     ((= exp 0)
      (begin
        1
        ))
     ((even? exp)
      (begin
        (let ((xx (mr-exp-mod base (/ exp 2) mm)))
          (let ((xx-2 (remainder (* xx xx) mm)))
            (begin
              (if (zero? xx-2)
                  (begin
                    0)
                  (begin
                    xx-2
                    ))
              )))
        ))
     (else
      (begin
        (remainder
         (* base (mr-exp-mod base (1- exp) mm))
         mm)
        )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-mr-exp-mod-1 result-hash-table)
 (begin
   (let ((sub-name "~a:test-mr-exp-mod-1")
         (test-list
          (list
           (list 2 0 2 1) (list 2 1 2 0)
           (list 3 0 11 1) (list 3 1 11 3)
           (list 3 2 11 9) (list 3 3 11 5)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((base (list-ref this-list 0))
                  (exp (list-ref this-list 1))
                  (mm (list-ref this-list 2))
                  (shouldbe (list-ref this-list 3)))
              (let ((result
                     (mr-exp-mod base exp mm)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "base=~a, exp=~a, mm=~a, "
                        base exp mm))
                      (err-3
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append
                      err-1 err-2 err-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (miller-rabin-test nn aa)
  (begin
    (let ((result
           (mr-exp-mod aa (1- nn) nn)))
      (let ((rr-2
             (remainder (* result result) nn)))
        (begin
          (cond
           ((= rr-2 0)
            (begin
              #f
              ))
           ((= rr-2 1)
            (begin
              #t
              ))
           (else
            (begin
              (= result 1)
              )))
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-miller-rabin-test-1 result-hash-table)
 (begin
   (let ((sub-name "test-miller-rabin-test-1")
         (test-list
          (list
           (list 5 3 #t) (list 7 4 #t)
           (list 11 2 #t) (list 13 10 #t) (list 15 3 #f)
           (list 17 10 #t) (list 19 11 #t)
           (list 561 3 #f) (list 561 6 #f) (list 561 12 #f)
           (list 1105 5 #f) (list 1105 10 #f) (list 1105 15 #f)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((nn (list-ref this-list 0))
                  (aa (list-ref this-list 1))
                  (shouldbe (list-ref this-list 2)))
              (let ((result
                     (miller-rabin-test nn aa)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : nn=~a, aa=~a, "
                        sub-name test-label-index nn aa))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (mr-rand-prime? nn times)
  (define (local-mr-test nn)
    (define (try-it aa)
      (begin
        (miller-rabin-test nn aa)
        ))
    (begin
      (try-it (1+ (random (1- nn))))
      ))
  (begin
    (cond
     ((<= nn 1)
      (begin
        #f
        ))
     ((= nn 2)
      (begin
        #t
        ))
     ((even? nn)
      (begin
        #f
        ))
     ((<= times 0)
      (begin
        #t
        ))
     ((local-mr-test nn)
      (begin
        (mr-rand-prime? nn (1- times))
        ))
     (else
      (begin
        #f
        )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-mr-rand-prime-1 result-hash-table)
 (begin
   (let ((sub-name "test-mr-rand-prime-1")
         (test-list
          (list
           (list 0 #f) (list 1 #f)
           (list 2 #t) (list 3 #t) (list 4 #f) (list 5 #t)
           (list 6 #f) (list 7 #t) (list 8 #f) (list 9 #f)
           (list 10 #f) (list 11 #t) (list 12 #f) (list 13 #t)
           (list 14 #f) (list 15 #f) (list 16 #f) (list 17 #t)
           (list 18 #f) (list 19 #t) (list 20 #f) (list 21 #f)
           (list 22 #f) (list 23 #t) (list 24 #f) (list 25 #f)
           (list 26 #f) (list 27 #f) (list 28 #f) (list 29 #t)
           (list 30 #f) (list 31 #t) (list 32 #f) (list 33 #f)
           (list 34 #f) (list 35 #f) (list 36 #f) (list 37 #t)
           (list 38 #f) (list 39 #f) (list 40 #f) (list 41 #t)
           (list 42 #f) (list 43 #t) (list 44 #f) (list 45 #f)
           (list 46 #f) (list 47 #t) (list 48 #f) (list 49 #f)
           (list 50 #f) (list 51 #f) (list 52 #f) (list 53 #t)
           (list 54 #f) (list 55 #f) (list 56 #f) (list 57 #f)
           (list 58 #f) (list 59 #t) (list 60 #f) (list 61 #t)
           (list 62 #f) (list 63 #f) (list 64 #f) (list 65 #f)
           (list 66 #f) (list 67 #t) (list 68 #f) (list 69 #f)
           (list 70 #f) (list 71 #t) (list 72 #f) (list 73 #t)
           (list 74 #f) (list 75 #f) (list 76 #f) (list 77 #f)
           (list 78 #f) (list 79 #t) (list 80 #f) (list 81 #f)
           (list 82 #f) (list 83 #t) (list 84 #f) (list 85 #f)
           (list 86 #f) (list 77 #f) (list 78 #f) (list 79 #t)
           (list 80 #f) (list 81 #f) (list 82 #f) (list 83 #t)
           (list 84 #f) (list 85 #f) (list 86 #f) (list 87 #f)
           (list 88 #f) (list 89 #t) (list 90 #f) (list 91 #f)
           (list 92 #f) (list 93 #f) (list 94 #f) (list 95 #f)
           (list 96 #f) (list 97 #t) (list 98 #f) (list 99 #f)
           (list 100 #f)
           ))
         (times 20)
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((nn (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result
                     (mr-rand-prime? nn times)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : nn=~a, "
                        sub-name test-label-index nn))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (div-two-and-odd even-num)
  (begin
    (let ((done-flag #f)
          (dd 0)
          (ss 0)
          (local-num even-num))
      (begin
        (while (equal? done-flag #f)
               (begin
                 (srfi-11:let-values
                  (((div-num remainder) (euclidean/ local-num 2)))
                  (begin
                    (if (or (odd? div-num)
                            (equal? div-num 1))
                        (begin
                          (set! dd div-num)
                          (set! done-flag #t)
                          ))

                    (set! ss (1+ ss))
                    (set! local-num div-num)
                    ))
                 ))

        (list ss dd)
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-div-two-and-odd-1 result-hash-table)
 (begin
   (let ((sub-name "test-div-two-and-odd-1")
         (test-list
          (list
           (list 2 (list 1 1)) (list 4 (list 2 1))
           (list 6 (list 1 3)) (list 8 (list 3 1))
           (list 10 (list 1 5)) (list 12 (list 2 3))
           (list 14 (list 1 7)) (list 16 (list 4 1))
           (list 18 (list 1 9)) (list 20 (list 2 5))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((even-num (list-ref this-list 0))
                  (shouldbe-list (list-ref this-list 1)))
              (let ((result-list
                     (div-two-and-odd even-num)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : even-num=~a, "
                        sub-name test-label-index even-num))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe-list result-list)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-list result-list)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (probable-prime? aa dd nn ss)
  (begin
    (let ((ad-tmp (mr-exp-mod aa dd nn)))
      (begin
        (if (equal? ad-tmp 1)
            (begin
              #t)
            (begin
              (let ((continue-flag #t)
                    (sprp-flag #f)
                    (nn-m-1 (- nn 1)))
                (begin
                  (do ((ii 0 (1+ ii)))
                      ((or (>= ii ss)
                           (equal? continue-flag #f)))
                    (begin
                      (if (equal? ad-tmp nn-m-1)
                          (begin
                            (set! continue-flag #f)
                            (set! sprp-flag #t))
                          (begin
                            (let ((next-ad-tmp (* ad-tmp ad-tmp)))
                              (begin
                                (set! ad-tmp (modulo next-ad-tmp nn))
                                ))
                            ))
                      ))
                  sprp-flag
                  ))
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-probable-prime-1 result-hash-table)
 (begin
   (let ((sub-name "test-probable-prime-1")
         (test-list
          (list
           (list 5 2 3 #t) (list 7 2 3 #t) (list 9 2 3 #f)
           (list 11 2 3 #t) (list 13 2 3 #t) (list 15 2 3 #f)
           (list 17 2 3 #t) (list 19 2 3 #t) (list 21 2 3 #f)
           (list 23 2 3 #t) (list 25 2 3 #f) (list 27 2 3 #f)
           (list 29 2 3 #t) (list 31 2 3 #t) (list 33 2 3 #f)
           (list 35 2 3 #f) (list 37 2 3 #t) (list 39 2 3 #f)
           (list 41 2 3 #t) (list 43 2 3 #t) (list 45 2 3 #f)
           (list 47 2 3 #t) (list 49 2 3 #f) (list 51 2 3 #f)
           (list 53 2 3 #t) (list 55 2 3 #f) (list 57 2 3 #f)
           (list 59 2 3 #t) (list 61 2 3 #t) (list 63 2 3 #f)
           (list 65 2 3 #f) (list 67 2 3 #t) (list 69 2 3 #f)
           (list 71 2 3 #t) (list 73 2 3 #t) (list 75 2 3 #f)
           (list 77 2 3 #f) (list 79 2 3 #t) (list 81 2 3 #f)
           (list 83 2 3 #t) (list 85 2 3 #f) (list 87 2 3 #f)
           (list 89 2 3 #t) (list 91 2 3 #f) (list 93 2 3 #f)
           (list 95 2 3 #f) (list 97 2 3 #t) (list 99 2 3 #f)
           (list 101 2 3 #t)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((num (list-ref this-list 0))
                  (aa-1 (list-ref this-list 1))
                  (aa-2 (list-ref this-list 2))
                  (shouldbe (list-ref this-list 3)))
              (let ((sd-list
                     (div-two-and-odd (1- num))))
                (let ((ss (list-ref sd-list 0))
                      (dd (list-ref sd-list 1)))
                  (let ((result-1
                         (probable-prime? aa-1 dd num ss))
                        (result-2
                         (probable-prime? aa-2 dd num ss)))
                    (let ((result (and result-1 result-2)))
                      (let ((err-1
                             (format
                              #f "~a : error (~a) : "
                              sub-name test-label-index))
                            (err-2
                             (format
                              #f "num=~a, aa-1=~a, aa-2=~a, "
                              num aa-1 aa-2))
                            (err-3
                             (format
                              #f "shouldbe=~a, result=~a"
                              shouldbe result)))
                        (begin
                          (unittest2:assert?
                           (equal? shouldbe result)
                           sub-name
                           (string-append
                            err-1 err-2 err-3)
                           result-hash-table)
                          )))
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (sprp-prime? nn)
  (begin
    (cond
     ((<= nn 1)
      (begin
        #f
        ))
     ((or (= nn 2)
          (= nn 3))
      (begin
        #t
        ))
     ((even? nn)
      (begin
        #f
        ))
     ((zero? (modulo nn 3))
      (begin
        #f
        ))
     ((< nn 1373653)
      (begin
        (let ((aa-1 2)
              (aa-2 3)
              (sd-list
               (div-two-and-odd (- nn 1))))
          (let ((ss (list-ref sd-list 0))
                (dd (list-ref sd-list 1)))
            (begin
              (and (probable-prime? aa-1 dd nn ss)
                   (probable-prime? aa-2 dd nn ss))
              )))
        ))
     ((< nn 25326001)
      (begin
        (let ((aa-1 2)
              (aa-2 3)
              (aa-3 5)
              (sd-list
               (div-two-and-odd (- nn 1))))
          (let ((ss (list-ref sd-list 0))
                (dd (list-ref sd-list 1)))
            (begin
              (and (probable-prime? aa-1 dd nn ss)
                   (probable-prime? aa-2 dd nn ss)
                   (probable-prime? aa-3 dd nn ss))
              )))
        ))
     ((< nn 118670087467)
      (begin
        (if (= nn 3215031751)
            (begin
              #f)
            (begin
              (let ((aa-1 2)
                    (aa-2 3)
                    (aa-3 5)
                    (aa-4 7)
                    (sd-list
                     (div-two-and-odd (- nn 1))))
                (let ((ss (list-ref sd-list 0))
                      (dd (list-ref sd-list 1)))
                  (begin
                    (and (probable-prime? aa-1 dd nn ss)
                         (probable-prime? aa-2 dd nn ss)
                         (probable-prime? aa-3 dd nn ss)
                         (probable-prime? aa-4 dd nn ss))
                    )))
              ))
        ))
     ((< nn 2152302898747)
      (begin
        (let ((aa-1 2)
              (aa-2 3)
              (aa-3 5)
              (aa-4 7)
              (aa-5 11)
              (sd-list
               (div-two-and-odd (- nn 1))))
          (let ((ss (list-ref sd-list 0))
                (dd (list-ref sd-list 1)))
            (begin
              (and (probable-prime? aa-1 dd nn ss)
                   (probable-prime? aa-2 dd nn ss)
                   (probable-prime? aa-3 dd nn ss)
                   (probable-prime? aa-4 dd nn ss)
                   (probable-prime? aa-5 dd nn ss))
              )))
        ))
     ((< nn 3474749660383)
      (begin
        (let ((aa-1 2)
              (aa-2 3)
              (aa-3 5)
              (aa-4 7)
              (aa-5 11)
              (aa-6 13)
              (sd-list
               (div-two-and-odd (- nn 1))))
          (let ((ss (list-ref sd-list 0))
                (dd (list-ref sd-list 1)))
            (begin
              (and (probable-prime? aa-1 dd nn ss)
                   (probable-prime? aa-2 dd nn ss)
                   (probable-prime? aa-3 dd nn ss)
                   (probable-prime? aa-4 dd nn ss)
                   (probable-prime? aa-5 dd nn ss)
                   (probable-prime? aa-6 dd nn ss))
              )))
        ))
     ((< nn 341550071728321)
      (begin
        (let ((aa-1 2)
              (aa-2 3)
              (aa-3 5)
              (aa-4 7)
              (aa-5 11)
              (aa-6 13)
              (aa-7 17)
              (sd-list
               (div-two-and-odd (- nn 1))))
          (let ((ss (list-ref sd-list 0))
                (dd (list-ref sd-list 1)))
            (begin
              (and (probable-prime? aa-1 dd nn ss)
                   (probable-prime? aa-2 dd nn ss)
                   (probable-prime? aa-3 dd nn ss)
                   (probable-prime? aa-4 dd nn ss)
                   (probable-prime? aa-5 dd nn ss)
                   (probable-prime? aa-6 dd nn ss)
                   (probable-prime? aa-7 dd nn ss))
              )))
        ))
     (else
      (begin
        (let ((ntimes 100))
          (begin
          ;;; probable prime
            (mr-rand-prime? nn ntimes)
            ))
        )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-sprp-prime-1 result-hash-table)
 (begin
   (let ((sub-name "test-sprp-prime-1")
         (test-list
          (list
           (list 0 #f) (list 1 #f)
           (list 2 #t) (list 3 #t) (list 4 #f) (list 5 #t)
           (list 6 #f) (list 7 #t) (list 8 #f) (list 9 #f)
           (list 10 #f) (list 11 #t) (list 12 #f) (list 13 #t)
           (list 14 #f) (list 15 #f) (list 16 #f) (list 17 #t)
           (list 18 #f) (list 19 #t) (list 20 #f) (list 21 #f)
           (list 22 #f) (list 23 #t) (list 24 #f) (list 25 #f)
           (list 26 #f) (list 27 #f) (list 28 #f) (list 29 #t)
           (list 30 #f) (list 31 #t) (list 32 #f) (list 33 #f)
           (list 34 #f) (list 35 #f) (list 36 #f) (list 37 #t)
           (list 38 #f) (list 39 #f) (list 40 #f) (list 41 #t)
           (list 42 #f) (list 43 #t) (list 44 #f) (list 45 #f)
           (list 46 #f) (list 47 #t) (list 48 #f) (list 49 #f)
           (list 50 #f) (list 51 #f) (list 52 #f) (list 53 #t)
           (list 54 #f) (list 55 #f) (list 56 #f) (list 57 #f)
           (list 58 #f) (list 59 #t) (list 60 #f) (list 61 #t)
           (list 62 #f) (list 63 #f) (list 64 #f) (list 65 #f)
           (list 66 #f) (list 67 #t) (list 68 #f) (list 69 #f)
           (list 70 #f) (list 71 #t) (list 72 #f) (list 73 #t)
           (list 74 #f) (list 75 #f) (list 76 #f) (list 77 #f)
           (list 78 #f) (list 79 #t) (list 80 #f) (list 81 #f)
           (list 82 #f) (list 83 #t) (list 84 #f) (list 85 #f)
           (list 86 #f) (list 77 #f) (list 78 #f) (list 79 #t)
           (list 80 #f) (list 81 #f) (list 82 #f) (list 83 #t)
           (list 84 #f) (list 85 #f) (list 86 #f) (list 87 #f)
           (list 88 #f) (list 89 #t) (list 90 #f) (list 91 #f)
           (list 92 #f) (list 93 #f) (list 94 #f) (list 95 #f)
           (list 96 #f) (list 97 #t) (list 98 #f) (list 99 #f)
           (list 100 #f)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((nn (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result
                     (sprp-prime? nn)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : nn=~a, "
                        sub-name test-label-index nn))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (slow-is-sequence-prime? nn plist clist)
  (begin
    (let ((nn-2 (* nn nn))
          (result-flag #t))
      (begin
        ;;; first check primes
        (for-each
         (lambda (pnum)
           (begin
             (let ((p-tmp (+ nn-2 pnum)))
               (begin
                 (if (not (sprp-prime? p-tmp))
                     (begin
                       (set! result-flag #f)
                       ))
                 ))
             )) plist)

        (if (equal? result-flag #t)
            (begin
              (for-each
               (lambda (cnum)
                 (begin
                   (let ((c-tmp (+ nn-2 cnum)))
                     (begin
                       (if (sprp-prime? c-tmp)
                           (begin
                             (set! result-flag #f)
                             ))
                       ))
                   )) clist)
              ))

        result-flag
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-slow-is-sequence-prime-1 result-hash-table)
 (begin
   (let ((sub-name "test-slow-is-sequence-prime-1")
         (test-list
          (list
           (list 0 #f) (list 2 #f)
           (list 10 #t) (list 20 #f) (list 30 #f)
           (list 315410 #t) (list 315420 #f)
           ))
         (plist (list 1 3 7 9 13 27))
         (clist (list 5 11 15 17 19 21 23 25))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((test-num (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result
                     (slow-is-sequence-prime? test-num plist clist)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : test-num=~a, "
                        sub-name test-label-index test-num))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (is-sequence-prime? nn)
  (begin
    (let ((nn-2 (* nn nn))
          (nn-sqrt-max (exact-integer-sqrt (+ (* nn nn) 28)))
          (result-flag #t))
      (begin
        ;;; first check that the following numbers
        (if (odd? nn)
            (begin
              (set! result-flag #f)
              ))

        (if (equal? result-flag #t)
            (begin
              (let ((plist
                     (list
                      (+ nn-2 1) (+ nn-2 3)
                      (+ nn-2 7) (+ nn-2 9)
                      (+ nn-2 13) (+ nn-2 27))))
                (begin
                  (do ((ii 3 (+ ii 2)))
                      ((or (> ii nn-sqrt-max)
                           (equal? result-flag #f)))
                    (begin
                      (for-each
                       (lambda (pnum)
                         (begin
                           (if (zero? (remainder pnum ii))
                               (begin
                                 (set! result-flag #f)
                                 ))
                           )) plist)
                      ))
                  ))

              (if (equal? result-flag #t)
                  (begin
                    (let ((clist
                           (list
                            (+ nn-2 5) (+ nn-2 11)
                            (+ nn-2 15) (+ nn-2 17)
                            (+ nn-2 19) (+ nn-2 21)
                            (+ nn-2 23) (+ nn-2 25))))
                      (begin
                        (for-each
                         (lambda (cnum)
                           (begin
                             (let ((sflag (sprp-prime? cnum)))
                               (begin
                                 (if (equal? sflag #t)
                                     (begin
                                       (set! result-flag #f)
                                       ))
                                 ))
                             )) clist)
                        ))
                    ))
              ))

        result-flag
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-is-sequence-prime-1 result-hash-table)
 (begin
   (let ((sub-name "test-is-sequence-prime-1")
         (test-list
          (list
           (list 0 #f) (list 2 #f)
           (list 10 #t) (list 20 #f) (list 30 #f)
           (list 315410 #t) (list 315420 #f)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((test-num (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result
                     (is-sequence-prime? test-num)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : test-num=~a, "
                        sub-name test-label-index test-num))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define-syntax display-debug-info-macro
  (syntax-rules ()
    ((display-debug-info-macro nn count sum)
     (begin
       (display
        (ice-9-format:format
         #f "nn = ~:d, nn^2 = ~:d : count = ~:d, sum = ~:d~%"
         nn (* nn nn) count sum))
       (force-output)
       ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax display-status-info-macro
  (syntax-rules ()
    ((display-status-info-macro
      this-num max-num sum count start-jday)
     (begin
       (let ((end-jday (srfi-19:current-julian-day)))
         (begin
           (display
            (ice-9-format:format
             #f "~:d / ~:d : sum = ~:d, count = ~:d~%"
             this-num max-num sum count))
           (display
            (format
             #f "  elapsed time = ~a : ~a~%"
             (timer-module:julian-day-difference-to-string
              end-jday start-jday)
             (timer-module:current-date-time-string)))
           (force-output)
           (set! start-jday end-jday)
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop max-num status-num gc-num debug-flag)
  (begin
    (let ((start-jday (srfi-19:current-julian-day))
          (count 0)
          (sum 0)
          (plist (list 1 3 7 9 13 27))
          (clist (list 11 17 19 21 23)))
      (begin
        (do ((nn 10 (+ nn 10)))
            ((>= nn max-num))
          (begin
            (if (and
                 (not (zero? (modulo nn 3)))
                 (not (zero? (modulo nn 7)))
                 (not (zero? (modulo nn 13))))
                (begin
                  (if (is-sequence-prime? nn)
                      (begin
                        (set! count (1+ count))
                        (set! sum (+ sum nn))

                        (if (equal? debug-flag #t)
                            (begin
                              (display-debug-info-macro nn count sum)
                              ))
                        ))

                  (if (zero? (modulo nn gc-num))
                      (begin
                        (gc)
                        ))
                  ))

            (if (zero? (modulo nn status-num))
                (begin
                  (display-status-info-macro
                   nn max-num sum count start-jday)
                  ))
            ))

        (display
         (ice-9-format:format
          #f "The sum of integers = ~:d : count = ~:d~%"
          sum count))
        (display
         (ice-9-format:format
          #f "  (which make consecutive primes "))
        (display
         (ice-9-format:format
          #f "using ~a, below ~:d)~%"
          plist max-num))
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format
      #f "The smallest positive integer n "))
    (display
     (format
      #f "for which the numbers~%"))
    (display
     (format
      #f "n^2+1, n^2+3, n^2+7, n^2+9, n^2+13,~%"))
    (display
     (format
      #f "and n^2+27 are consecutive primes "))
    (display
     (format
      #f "is 10. The sum of all~%"))
    (display
     (format
      #f "such integers n below one-million "))
    (display
     (format
      #f "is 1242490.~%"))
    (newline)
    (display
     (format
      #f "What is the sum of all such integers "))
    (display
     (format
      #f "n below 150 million?~%"))
    (newline)
    (display
     (format #f "A key to speeding up the "))
    (display
     (format
      #f "program is to look~%"))
    (display
     (format #f "at the entire sequence when "))
    (display
     (format
      #f "determining if they~%"))
    (display
     (format
      #f "are prime.~%"))
    (display
     (format
      #f "This idea was found at~%"))
    (display
     (format
      #f "https://euler.stephan-brumme.com/146/~%"))
    (newline)
    (display
     (format
      #f "Another key is to look only at n=ak, "))
    (display
     (format
      #f "where a is even since~%"))
    (display
     (format
      #f "all the sums numbers must be odd, and "))
    (display
     (format
      #f "a is a multiple~%"))
    (display
     (format
      #f "of 5 and a multiple of 2, since all~%"))
    (display
     (format
      #f "numbers can't be divisible by 5.~%"))
    (display
     (format
      #f "This idea was found at https://mukeshiiitm.wordpress.com/2011/06/22/project-euler-146/~%"))
    (newline)
    (display
     (format
      #f "Note: this program takes around 25 "))
    (display
     (format
      #f "minutes to complete.~%"))
    (display
     (format
      #f "It was re-written in c++ and completed "))
    (display
     (format
      #f "around 1 minute. It's probably~%"))
    (display
     (format
      #f "due to c++ using 64-bit integers "))
    (display
     (format
      #f "(int64_t), while~%"))
    (display
     (format
      #f "guile is likely using bignums to "))
    (display
     (format
      #f "do the calculations.~%"))
    (display
     (format #f "see https://projecteuler.net/problem=146~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((max-num 1000000)
          (status-num 10000000)
          (gc-num 100000)
          (debug-flag #t))
      (begin
        (sub-main-loop max-num status-num gc-num debug-flag)
        (gc)
        ))


    (newline)
    (let ((max-num 150000000)
          (status-num 10000000)
          (gc-num 1000000)
          (debug-flag #t))
      (begin
        (sub-main-loop max-num status-num gc-num debug-flag)
        (gc)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 146 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

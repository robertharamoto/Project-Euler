#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 145                                    ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated August 1, 2022                               ###
;;;###                                                       ###
;;;###  updated March 12, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### srfi-19 for date/time functions
(use-modules ((srfi srfi-19)
              :renamer (symbol-prefix-proc 'srfi-19:)))

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (even-digits-count nn)
  (begin
    (cond
     ((<= nn 0)
      (begin
        #f
        ))
     ((odd? nn)
      (begin
        #f
        ))
     (else
      (begin
        (let ((kk (euclidean/ nn 2)))
          (let ((result 20))
            (begin
              (do ((ii (1- kk) (1- ii)))
                  ((<= ii 0))
                (begin
                  (set! result (* result 30))
                  ))

              result
              )))
        )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-even-digits-count-1 result-hash-table)
 (begin
   (let ((sub-name "test-even-digits-count-1")
         (test-list
          (list
           (list 0 #f) (list 2 20) (list 4 600)
           (list 6 18000) (list 8 540000)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((nn (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result (even-digits-count nn)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : nn=~a, "
                        sub-name test-label-index nn))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (odd-digits-count nn)
  (begin
    (cond
     ((<= nn 0)
      (begin
        #f
        ))
     ((= nn 1)
      (begin
        0
        ))
     ((even? nn)
      (begin
        #f
        ))
     (else
      (begin
        (let ((rr (modulo nn 4))
              (kk (quotient nn 4)))
          (begin
            (cond
             ((= rr 3)
              (begin
                (let ((result 100))
                  (begin
                    (do ((ii 0 (1+ ii)))
                        ((>= ii kk))
                      (begin
                        (let ((next-result (* result 500)))
                          (begin
                            (set! result next-result)
                            ))
                        ))
                    result
                    ))
                ))
             (else
              (begin
                0
                )))
            ))
        )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-odd-digits-count-1 result-hash-table)
 (begin
   (let ((sub-name "test-odd-digits-count-1")
         (test-list
          (list
           (list 0 #f) (list 1 0) (list 3 100)
           (list 5 0) (list 7 50000) (list 9 0)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((nn (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result (odd-digits-count nn)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : nn=~a, "
                        sub-name test-label-index nn))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop max-digits debug-flag)
  (begin
    (let ((count 0)
          (start-jday (srfi-19:current-julian-day)))
      (begin
        (do ((nn 2 (1+ nn)))
            ((> nn max-digits))
          (begin
            (if (even? nn)
                (begin
                  (let ((this-count (even-digits-count nn)))
                    (begin
                      (set! count (+ count this-count))
                      )))
                (begin
                  (let ((this-count (odd-digits-count nn)))
                    (begin
                      (set! count (+ count this-count))
                      ))
                  ))
            ))

        (display
         (ice-9-format:format
          #f "The number of reversible numbers = ~:d "
          count))
        (display
         (ice-9-format:format
          #f "(below 10^~:d)~%" max-digits))
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Some positive integers n have the "))
    (display
     (format #f "property that the~%"))
    (display
     (format #f "sum [ n + reverse(n) ] consists "))
    (display
     (format #f "entirely of odd~%"))
    (display
     (format #f "(decimal) digits. For instance, "))
    (display
     (format #f "36 + 63 = 99~%"))
    (display
     (format #f "and 409 + 904 = 1313. We will "))
    (display
     (format #f "call such numbers~%"))
    (display
     (format #f "reversible; so 36, 63, 409, and "))
    (display
     (format #f "904 are reversible.~%"))
    (display
     (format #f "Leading zeroes are not allowed "))
    (display
     (format #f "in either n or~%"))
    (display
     (format #f "reverse(n).~%"))
    (newline)
    (display
     (format #f "There are 120 reversible numbers "))
    (display
     (format #f "below one-thousand.~%"))
    (newline)
    (display
     (format #f "How many reversible numbers are "))
    (display
     (format #f "there below~%"))
    (display
     (format #f "one-billion (10^9)?~%"))
    (newline)
    (display
     (format #f "Ideas for solutions were found at~%"))
    (display
     (format #f "https://euler.stephan-brumme.com/145/~%"))
    (display
     (format #f "https://blog.dreamshire.com/project-euler-145/~%"))
    (newline)
    (display
     (format #f "A number can be represented as~%"))
    (display
     (format #f "n = a0 + a1*10 + a2*10^2 + a3*10^3 +...~%"))
    (display
     (format #f "And when adding digits, it's "))
    (display
     (format #f "helpful to break~%"))
    (display
     (format #f "it up the sum of digits into 2 "))
    (display
     (format #f "cases, if the~%"))
    (display
     (format #f "terms are all greater than or "))
    (display
     (format #f "less than 10."))
    (newline)
    (display
     (format #f "Two digit reversible numbers: we "))
    (display
     (format #f "must have~%"))
    (display
     (format #f "n+reverse(n) = a0+a1*10^1+a1+a0*10^1~%"))
    (display
     (format #f "= (a0+a1)+(a0+a1)*10^1,~%"))
    (display
     (format #f "where (a0+a1)<10 and odd. (a0+a1) "))
    (display
     (format #f "cannot be greater~%"))
    (display
     (format #f "than 10, since there will be a "))
    (display
     (format #f "carry from the~%"))
    (display
     (format #f "first digit into the ten's digit, "))
    (display
     (format #f "and if (a0+a1) is~%"))
    (display
     (format #f "odd, then the 10's factor (a0+a1)+1 "))
    (display
     (format #f "will be even.~%"))
    (display
     (format #f "Possible values for (a0, a1) "))
    (display
     (format #f "are (1, 2),~%"))
    (display
     (format #f "(1, 4), (1, 6), (1, 8), (2, 3), "))
    (display
     (format #f "(2, 5), (2, 7),~%"))
    (display
     (format #f "(3, 4), (3, 6), (4, 5) plus the "))
    (display
     (format #f "reverse numbers,~%"))
    (display
     (format #f "for a total of 20.~%"))
    (newline)
    (display
     (format #f "Three digit reversible numbers: we "))
    (display
     (format #f "must have~%"))
    (display
     (format #f "n+reverse(n) = (a0+a2)+(a1+a1)*10^1 "))
    (display
     (format #f "+(a0+a2)*10^2.~%"))
    (display
     (format #f "Note that since (a1+a1) is always "))
    (display
     (format #f "even, the only~%"))
    (display
     (format #f "way for there to be three digit "))
    (display
     (format #f "reversible numbers~%"))
    (display
     (format #f "is if (a1+a1)<10, and if a0+a2>10 "))
    (display
     (format #f "and odd, (for~%"))
    (display
     (format #f "example 409 or 904). Possible "))
    (display
     (format #f "values for (a0, a2)~%"))
    (display
     (format #f "are (2, 9), (3, 8), (4, 7), (4, 9), "))
    (display
     (format #f "(5, 6), (5, 8),~%"))
    (display
     (format #f "(6, 7), (6, 9), (7, 8), (8, 9) "))
    (display
     (format #f "plus the reverses,~%"))
    (display
     (format #f "20 possible values in all. For a1, "))
    (display
     (format #f "there are 5~%"))
    (display
     (format #f "possible values (0 through 4), so "))
    (display
     (format #f "the possible~%"))
    (display
     (format #f "solutions are 20*5=100.~%"))
    (newline)
    (display
     (format #f "Four digit reversible numbers: we "))
    (display
     (format #f "must have~%"))
    (display
     (format #f "n+reverse(n) "))
    (display
     (format #f "= (a0+a3)+(a1+a2)*10^1~%"))
    (display
     (format #f "+(a1+a2)*10^2+(a0+a3)*10^3.~%"))
    (display
     (format #f "Case (1) all terms are less than "))
    (display
     (format #f "10 and are odd.~%"))
    (display
     (format #f "There are 20 pairs of (a0, a3) "))
    (display
     (format #f "(like in the two~%"))
    (display
     (format #f "digit case), and there are 30 "))
    (display
     (format #f "pairs of (a1, a2),~%"))
    (display
     (format #f "(20 pairs 2-digit pairs, plus "))
    (display
     (format #f "(0, 1), (0, 3),~%"))
    (display
     (format #f "(0, 5), (0, 7), (0, 9) and the "))
    (display
     (format #f "reverse order). So~%"))
    (display
     (format #f "there are 20*30=600 solutions.~%"))
    (display
     (format #f "Case (2) (a0+a3)>10 and odd, "))
    (display
     (format #f "then the 10's factor~%"))
    (display
     (format #f "must be even and (a1+a2)>10, but "))
    (display
     (format #f "this will require~%"))
    (display
     (format #f "that the 100's factor produce a "))
    (display
     (format #f "carry over into~%"))
    (display
     (format #f "the 10^3 term, turning it from an "))
    (display
     (format #f "odd number into~%"))
    (display
     (format #f "an even number, so it can't be "))
    (display
     (format #f "a solution.~%"))
    (newline)
    (display
     (format #f "Five digit reversible numbers: "))
    (display
     (format #f "we must have~%"))
    (display
     (format #f "n+reverse(n) = (a0+a4)+(a1+a3)*10~%"))
    (display
     (format #f "+(a2+a2)*10^2+(a1+a3)*10^3+(a0+a4)*10^4.~%"))
    (display
     (format #f "Case (1) all terms are odd and "))
    (display
     (format #f "less than 10,~%"))
    (display
     (format #f "this has no solution since the 10^2 "))
    (display
     (format #f "term is always even.~%"))
    (display
     (format #f "Case (2) (a1+a3)>10 since (a2+a2) "))
    (display
     (format #f "always even, and you~%"))
    (display
     (format #f "need a carry to increase (a2+a2) by 1. "))
    (display
     (format #f "a0+a4 must be~%"))
    (display
     (format #f "odd from the 10^0 factor, but with "))
    (display
     (format #f "a carry from~%"))
    (display
     (format #f "(a1+a3)>10 on the 10^3 factor, we "))
    (display
     (format #f "see that the~%"))
    (display
     (format #f "10^4 factor will always be even. So "))
    (display
     (format #f "there cannot be~%"))
    (display
     (format #f "any five digit reversible numbers.~%"))
    (newline)
    (display
     (format #f "Six digit reversible numbers: "))
    (display
     (format #f "we must have~%"))
    (display
     (format #f "n+reverse(n) = (a0+a5)+(a1+a4)*10^1~%"))
    (display
     (format #f "+(a2+a3)*10^2+(a2+a3)*10^3~%"))
    (display
     (format #f "+(a1+a4)*10^4+(a0+a5)*10^5.~%"))
    (display
     (format #f "Case (1) If all sums are less than "))
    (display
     (format #f "10 and odd then~%"))
    (display
     (format #f "solutions will be possible. There are "))
    (display
     (format #f "20*30*30=18,000~%"))
    (display
     (format #f "solutions.~%"))
    (display
     (format #f "Case (2) If (a0+a5)>10 and odd, "))
    (display
     (format #f "then (a1+a4)~%"))
    (display
     (format #f "(the 10's factor), must be even, "))
    (display
     (format #f "and if (a1+a4)>10,~%"))
    (display
     (format #f "the 10^4 factor will carry a one "))
    (display
     (format #f "over to the~%"))
    (display
     (format #f "10^5 term, which will make it even, "))
    (display
     (format #f "so (a1+a4)>10~%"))
    (display
     (format #f "is not allowed. (a1+a4)<10 and even. "))
    (display
     (format #f "(a2+a3)>10 since~%"))
    (display
     (format #f "the 10^4 factor is even, but this is "))
    (display
     (format #f "not allowed since~%"))
    (display
     (format #f "the two consecutive (a2+a3) terms "))
    (display
     (format #f "will cause one~%"))
    (display
     (format #f "term to be odd and the other to "))
    (display
     (format #f "be even. This~%"))
    (display
     (format #f "means that case (2) has no "))
    (display
     (format #f "solutions.~%"))
    (newline)
    (display
     (format #f "Seven digit reversible numbers: "))
    (display
     (format #f "we must have~%"))
    (display
     (format #f "n+reverse(n) = (a0+a6)+(a1+a5)*10^1~%"))
    (display
     (format #f "+(a2+a4)*10^2+(a3+a3)*10^3+(a2+a4)*10^4~%"))
    (display
     (format #f "+(a1+a5)*10^5+(a0+a6)*10^6.~%"))
    (display
     (format #f "Case (1) There are no solutions "))
    (display
     (format #f "when all factors~%"))
    (display
     (format #f "are less than 10, since (a3+a3) "))
    (display
     (format #f "will always be even.~%"))
    (display
     (format #f "Case (2) (a0+a6)>10 and odd : "))
    (display
     (format #f "(a1+a5)<10 and even,~%"))
    (display
     (format #f "so (a2+a4)>10 and odd, since the "))
    (display
     (format #f "10^4 factor needs~%"))
    (display
     (format #f "a carry to make it odd, (a3+a3)>=10. "))
    (display
     (format #f "Possible odd~%"))
    (display
     (format #f "pairs for (a0+a6)>10 are "))
    (display
     (format #f "(2, 9), (3, 8), (4, 7),~%"))
    (display
     (format #f "(4, 9), (5, 6), (5, 8), (6, 7), "))
    (display
     (format #f "(6, 9), (7, 8),~%"))
    (display
     (format #f "(8, 9), plus reverses, 20 in all. "))
    (display
     (format #f "The allowable~%"))
    (display
     (format #f "even pairs for (a1+a5) are (0, 2), "))
    (display
     (format #f "(0, 4), (0, 6),~%"))
    (display
     (format #f "(0, 8), (2, 4), (2, 6), (1, 3), "))
    (display
     (format #f "(1, 5), (1, 7),~%"))
    (display
     (format #f "(3, 5), (0, 0), (1, 1), (2, 2), "))
    (display
     (format #f "(3, 3) (4, 4),~%"))
    (display
     (format #f "plus reverses, 25 in all. The "))
    (display
     (format #f "allowable odd pairs~%"))
    (display
     (format #f "for (a2+a4)>10 and odd are the "))
    (display
     (format #f "same as for~%"))
    (display
     (format #f "(a0+a6), 20 pairs. If (a3+a3)>=10, "))
    (display
     (format #f "then a3 can be~%"))
    (display
     (format #f "5, 6, 7, 8, 9, or 5 possible values. "))
    (display
     (format #f "This means~%"))
    (display
     (format #f "there are 20*25*20*5=50,000.~%"))
    (display
     (format #f "Case (3) (a0+a6)<10 and odd : "))
    (display
     (format #f "(a1+a5)<10 and odd,~%"))
    (display
     (format #f "(a2+a4)>10 so that the carry can "))
    (display
     (format #f "make the (a3+a3)~%"))
    (display
     (format #f "term odd, but the 10^4 term will "))
    (display
     (format #f "turn the 10^5 term~%"))
    (display
     (format #f "into an even number, so there are "))
    (display
     (format #f "no solutions when~%"))
    (display
     (format #f "(a0+a6)<10.~%"))
    (newline)
    (display
     (format #f "Eight digit reversible numbers: "))
    (display
     (format #f "we must have~%"))
    (display
     (format #f "n+reverse(n) = (a0+a7)+(a1+a6)*10^1~%"))
    (display
     (format #f "+(a2+a5)*10^2+(a3+a4)*10^3+(a3+a4)*10^4~%"))
    (display
     (format #f "+(a2+a5)*10^5+(a1+a6)*10^6+(a0+a7)*10^7.~%"))
    (display
     (format #f "Case (1) all factors are less than "))
    (display
     (format #f "10 and odd,~%"))
    (display
     (format #f "there are 20*30*30*30=540,000.~%"))
    (display
     (format #f "Case (2) (a0+a7)>10 and odd : "))
    (display
     (format #f "(a1+a6) even, if~%"))
    (display
     (format #f "(a1+a6)>10, then it will carry "))
    (display
     (format #f "over to the~%"))
    (display
     (format #f "10^7 factor and make it even, so "))
    (display
     (format #f "(a1+a6)<10 and~%"))
    (display
     (format #f "even. Must have (a2+a5)>10 since "))
    (display
     (format #f "the 10^6 factor~%"))
    (display
     (format #f "is even, (a2+a5) odd because the "))
    (display
     (format #f "10^1 factor~%"))
    (display
     (format #f "(a1+a6)<10 will not contribute "))
    (display
     (format #f "a carry. Must have~%"))
    (display
     (format #f "(a3+a4)<10 and even because of "))
    (display
     (format #f "the carry from the~%"))
    (display
     (format #f "(a2+a5)>10, but since there is "))
    (display
     (format #f "no carry from the~%"))
    (display
     (format #f "10^3 term, the 10^4 term will "))
    (display
     (format #f "remain even. So~%"))
    (display
     (format #f "there are no solutions for "))
    (display
     (format #f "case (2).~%"))
    (newline)
    (display
     (format #f "Nine digit reversible numbers: "))
    (display
     (format #f "we must have~%"))
    (display
     (format #f "n+reverse(n) = (a0+a8)+(a1+a7)*10^1~%"))
    (display
     (format #f "+(a2+a6)*10^2+(a3+a5)*10^3+(a4+a4)*10^4~%"))
    (display
     (format #f "+(a3+a5)*10^5+(a2+a6)*10^6+(a1+a7)*10^7~%"))
    (display
     (format #f "+(a0+a8)*10^8.~%"))
    (display
     (format #f "Case (1) there are no solutions "))
    (display
     (format #f "if all factors~%"))
    (display
     (format #f "are less than 10 and odd since "))
    (display
     (format #f "the 10^4 factor~%"))
    (display
     (format #f "is always even.~%"))
    (display
     (format #f "Case (2) (a0+a8)>10 and odd : "))
    (display
     (format #f "(a1+a7)<10 and~%"))
    (display
     (format #f "even because of the 10^7 term, "))
    (display
     (format #f "must have~%"))
    (display
     (format #f "(a2+a6)>10 because of the 10^6 "))
    (display
     (format #f "term, and odd~%"))
    (display
     (format #f "because of the 10^2 term. Then "))
    (display
     (format #f "(a3+a5)>10 because~%"))
    (display
     (format #f "of the 10^5 term and even because "))
    (display
     (format #f "of the 10^3 term~%"))
    (display
     (format #f "(need to absorb a carry from (a2+a6)), "))
    (display
     (format #f "this will~%"))
    (display
     (format #f "make the 10^4 factor odd. a4>5 "))
    (display
     (format #f "and even, but~%"))
    (display
     (format #f "will turn the 10^6 factor (a2+a6) "))
    (display
     (format #f "even, so there~%"))
    (display
     (format #f "are no solutions for case (2).~%"))
    (newline)
    (display
     (format #f "There are some patterns to observe. "))
    (display
     (format #f "For an even~%"))
    (display
     (format #f "number of digits n=2k, k>=1, there "))
    (display
     (format #f "are 20*30^(k-1)~%"))
    (display
     (format #f "solutions.~%"))
    (newline)
    (display
     (format #f "For an odd number of digits "))
    (display
     (format #f "there are two~%"))
    (display
     (format #f "distinct types, one with "))
    (display
     (format #f "solutions and one~%"))
    (display
     (format #f "without. For those with solutions "))
    (display
     (format #f "(n=3, or n=7), say~%"))
    (display
     (format #f "n=4k+3, k>=0, then there are an "))
    (display
     (format #f "odd number of~%"))
    (display
     (format #f "terms on either side of the central "))
    (display
     (format #f "term (the (a1+a1)~%"))
    (display
     (format #f "factor for 3 digit numbers, or "))
    (display
     (format #f "the (a3+a3) factor~%"))
    (display
     (format #f "for 7 digit numbers). This is what "))
    (display
     (format #f "is required since~%"))
    (display
     (format #f "each term greater than 10 needs "))
    (display
     (format #f "a neighboring term~%"))
    (display
     (format #f "that is less than 10, until you "))
    (display
     (format #f "reach one before~%"))
    (display
     (format #f "the center term, it must be odd "))
    (display
     (format #f "and greater than~%"))
    (display
     (format #f "10, the center term will be even, "))
    (display
     (format #f "and one will be~%"))
    (display
     (format #f "the same as the one before. The "))
    (display
     (format #f "number of solutions~%"))
    (display
     (format #f "are 20^(k+1)*25^k*5.~%"))
    (newline)
    (display
     (format #f "For those odd number of digits "))
    (display
     (format #f "without solutions,~%"))
    (display
     (format #f "there are an even number of "))
    (display
     (format #f "factors on either~%"))
    (display
     (format #f "side of the central term (the "))
    (display
     (format #f "(a2+a2) term for~%"))
    (display
     (format #f "5 digit numbers or the (a4+a4) "))
    (display
     (format #f "term for the~%"))
    (display
     (format #f "9 digit numbers), and it can "))
    (display
     (format #f "be seen it cannot~%"))
    (display
     (format #f "support the delicate balance of "))
    (display
     (format #f "odd greater than~%"))
    (display
     (format #f "10/even less than 10 terms.~%"))
    (display
     (format #f "see https://projecteuler.net/problem=145~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((max-digits 3)
          (debug-flag #t))
      (begin
        (sub-main-loop max-digits debug-flag)
        ))

    (newline)
    (let ((max-digits 9)
          (debug-flag #f))
      (begin
        (sub-main-loop max-digits debug-flag)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 145 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

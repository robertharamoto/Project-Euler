#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 147                                    ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated August 1, 2022                               ###
;;;###                                                       ###
;;;###  updated March 12, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### srfi-19 for date functions
(use-modules ((srfi srfi-19)
              :renamer (symbol-prefix-proc 'srfi-19:)))

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (count-normal-blocks nrows ncols)
  (begin
    (let ((result (euclidean/
                   (* nrows (1+ nrows)
                      ncols (1+ ncols))
                   4)))
      (begin
        result
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-count-normal-blocks-1 result-hash-table)
 (begin
   (let ((sub-name "test-count-normal-blocks-1")
         (test-list
          (list
           (list 1 1 1) (list 1 2 3) (list 2 1 3)
           (list 1 3 6) (list 1 4 10)
           (list 2 2 9) (list 2 3 18)
           (list 2 4 30) (list 4 2 30)
           (list 3 3 36) (list 3 4 60)
           (list 4 4 100)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((nrows (list-ref this-list 0))
                  (ncols (list-ref this-list 1))
                  (shouldbe (list-ref this-list 2)))
              (let ((result
                     (count-normal-blocks nrows ncols)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : nrows=~a, ncols=~a, "
                        sub-name test-label-index nrows ncols))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (count-hb-2x2-diamonds min-size max-size)
  (begin
    (cond
     ((< min-size 1)
      (begin
        #f
        ))
     ((= min-size 1)
      (begin
        (let ((ncount (1- max-size)))
          (begin
            ncount
            ))
        ))
     ((= min-size 2)
      (begin
        (let ((mm-m-1 (1- max-size))
              (nn-m-1 (1- min-size)))
          (let ((ndiamonds (* mm-m-1 nn-m-1)))
            (begin
              (cond
               ((= ndiamonds 1) 9)
               (else
                (begin
                  (- (* 9 mm-m-1) (- max-size 2))
                  ))
               ))
            ))
        ))
     (else
      (begin
        (let ((mm-m-1 (1- max-size))
              (nn-m-1 (1- min-size)))
          (let ((num-blocks (* 9 mm-m-1 nn-m-1))
                (adj-term-1 (+ (* -3 mm-m-1 nn-m-1) 4))
                (adj-term-2 0))
            (begin
              (cond
               ((= min-size 3)
                (begin
                  (let ((mtmp (- max-size 3)))
                    (let ((num-2x2 (1+ mtmp))
                          (num-3x3 mtmp))
                      (begin
                        (set! adj-term-2 (+ num-2x2 num-3x3))
                        )))
                  ))
               (else
                (begin
                  (let ((mtmp (- max-size 3))
                        (ntmp (- min-size 2)))
                    (let ((num-2x2 (* ntmp (1+ mtmp)))
                          (num-3x3 (* ntmp mtmp)))
                      (begin
                        (set! adj-term-2 (+ num-2x2 num-3x3))
                        )))

                  (- num-blocks adj-term-1)
                  )))
              )))
        )))
    ))

;;;#############################################################
;;;#############################################################
(define (count-hb-extra-2x2s nrows ncols)
  (begin
    (let ((result
           (euclidean/
            (* nrows (1+ nrows)
               ncols (1+ ncols))
            4)))
      (begin
        result
        ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax display-debug-info-macro
  (syntax-rules ()
    ((display-debug-info-macro
      xx yy zz rsum min-xx min-yy min-zz min-sum count)
     (begin
       (display
        (ice9-format:format
         #f "  (xx, yy, zz) = (~:d, ~:d, ~:d), sum = ~:d~%"
         xx yy zz rsum))
       (display
        (ice9-format:format
         #f "  min = (~:d, ~:d, ~:d), sum = ~:d, count=~:d~%"
         min-xx min-yy min-zz min-sum count))
       (force-output)
       ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax display-status-info-macro
  (syntax-rules ()
    ((display-status-info-macro
      sq-counter max-ii sum count start-jday)
     (begin
       (let ((end-jday (srfi-19:current-julian-day)))
         (begin
           (display
            (ice9-format:format
             #f "~:d / ~:d : sum = ~:d, count = ~:d~%"
             sq-counter max-ii sum count))
           (display
            (format
             #f "  elapsed time = ~a : ~a~%"
             (julian-day-difference-to-string
              end-jday start-jday)
             (current-date-time-string)))
           (force-output)
           (set! start-jday end-jday)
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax jj-bb-loop-macro
  (syntax-rules ()
    ((jj-bb-loop-macro
      min-bb max-bb aa-1-2 aa-2-2 xx yy
      found-flag min-xx min-yy min-zz min-sum
      debug-flag count)
     (begin
       (do ((bb-1 min-bb (1+ bb-1)))
           ((or (> bb-1 max-bb)
                (equal? found-flag #t)))
         (begin
           (let ((bb-1-2 (* bb-1 bb-1)))
             (let ((zz (/ (- (* 2 bb-1-2) aa-1-2 aa-2-2) 2))
                   (bb-2-2 (- (+ aa-1-2 aa-2-2) bb-1-2))
                   (cc-1-2 (- bb-1-2 aa-2-2)))
               (let ((rsum (+ xx yy zz))
                     (cc-2-2 (- aa-1-2 (+ aa-2-2 cc-1-2)))
                     (bb-2 (exact-integer-sqrt bb-2-2))
                     (cc-1 (exact-integer-sqrt cc-1-2))
                     (cc-2
                      (exact-integer-sqrt
                       (- aa-1-2 (+ aa-2-2 cc-1-2)))))
                 (begin
                   (if (and (> zz 0) (> yy zz)
                            (integer? zz)
                            (= bb-2-2 (* bb-2 bb-2))
                            (= cc-1-2 (* cc-1 cc-1))
                            (= cc-2-2 (* cc-2 cc-2)))
                       (begin
                         (if (or (< min-sum 0)
                                 (< rsum min-sum))
                             (begin
                               (set! min-xx xx)
                               (set! min-yy yy)
                               (set! min-zz zz)
                               (set! min-sum rsum)

                               (if (equal? debug-flag #t)
                                   (begin
                                     (display-debug-info-macro
                                      xx yy zz rsum
                                      min-xx min-yy min-zz min-sum
                                      count)
                                     ))
                               ))
                         (set! count (1+ count))
                         ))
                   ))
               ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax display-plus-line-macro
  (syntax-rules ()
    ((display-plus-line-macro astring min-xx min-yy)
     (begin
       (let ((xy (+ min-xx min-yy)))
         (let ((xy-sqr (exact-integer-sqrt xy)))
           (begin
             (display
              (ice9-format:format
               #f "  ~a = ~:d = ~:d^2~%" astring xy xy-sqr))
             (force-output)
             )))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax display-minus-line-macro
  (syntax-rules ()
    ((display-minus-line-macro astring min-xx min-yy)
     (begin
       (let ((xy (- min-xx min-yy)))
         (let ((xy-sqr (exact-integer-sqrt xy)))
           (begin
             (display
              (ice9-format:format
               #f "  ~a = ~:d = ~:d^2~%" astring xy xy-sqr))
             (force-output)
             )))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop max-num status-num debug-flag)
  (begin
    (let ((min-xx -1)
          (min-yy -1)
          (min-zz -1)
          (min-sum -1)
          (count 0)
          (start-jday (srfi-19:current-julian-day))
          (found-flag #f))
      (begin
        (do ((aa-2 1 (1+ aa-2)))
            ((or (> aa-2 max-num)
                 (equal? found-flag #t)))
          (begin
            (let ((aa-2-2 (* aa-2 aa-2)))
              (begin
                (do ((aa-1 (1+ aa-2) (1+ aa-1)))
                    ((or (> aa-1 max-num)
                         (equal? found-flag #t)))
                  (begin
                    (let ((aa-1-2 (* aa-1 aa-1))
                          (max-bb aa-1))
                      (let ((min-bb
                             (exact-integer-sqrt
                              (euclidean/ (+ aa-1-2 aa-2-2) 2)))
                            (xx (/ (+ aa-1-2 aa-2-2) 2))
                            (yy (/ (- aa-1-2 aa-2-2) 2)))
                        (begin
                          (if (and (integer? xx) (integer? yy)
                                   (> yy 0))
                              (begin
                                (jj-bb-loop-macro
                                 min-bb max-bb aa-1-2 aa-2-2 xx yy
                                 found-flag min-xx min-yy min-zz min-sum
                                 debug-flag count)
                                ))
                          )))
                    ))
                ))

            (if (zero? (modulo aa-2 status-num))
                (begin
                  (display-status-info-macro
                   aa-2 max-num sum count start-jday)
                  ))
            ))

        (display
         (ice9-format:format
          #f "The minimum sum = ~:d, (x, y, z) = (~:d, ~:d, ~:d).~%"
          min-sum min-xx min-yy min-zz))
        (force-output)

        (display-plus-line-macro "x+y" min-xx min-yy)
        (display-minus-line-macro "x-y" min-xx min-yy)

        (display-plus-line-macro "x+z" min-xx min-zz)
        (display-minus-line-macro "x-z" min-xx min-zz)

        (display-plus-line-macro "y+z" min-yy min-zz)
        (display-minus-line-macro "y-z" min-yy min-zz)

        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "In a 3x2 cross-hatched grid, a "))
    (display
     (format #f "total of 37 different~%"))
    (display
     (format #f "rectangles could be situated "))
    (display
     (format #f "within that grid as~%"))
    (display
     (format #f "indicated in the sketch.~%"))
    (newline)
    (display
     (format #f "See the image at~%"))
    (display
     (format #f "https://projecteuler.net/resources/images/0147.png~%"))
    (newline)
    (display
     (format #f "There are 5 grids smaller than "))
    (display
     (format #f "3x2, vertical and~%"))
    (display
     (format #f "horizontal dimensions being "))
    (display
     (format #f "important, i.e. 1x1,~%"))
    (display
     (format #f "2x1, 3x1, 1x2 and 2x2. If each "))
    (display
     (format #f "of them is cross-hatched,~%"))
    (display
     (format #f "the following number of different "))
    (display
     (format #f "rectangles could be~%"))
    (display
     (format #f "situated within those smaller grids:~%"))
    (newline)
    (display (format #f "1x1: 1~%"))
    (display (format #f "2x1: 4~%"))
    (display (format #f "3x1: 8~%"))
    (display (format #f "1x2: 4~%"))
    (display (format #f "2x2: 18~%"))
    (newline)
    (display
     (format #f "Adding those to the 37 of the "))
    (display
     (format #f "3x2 grid, a total~%"))
    (display
     (format #f "of 72 different rectangles could "))
    (display
     (format #f "be situated within~%"))
    (display
     (format #f "3x2 and smaller grids.~%"))
    (newline)
    (display
     (format #f "How many different rectangles "))
    (display
     (format #f "could be situated~%"))
    (display
     (format #f "within 47x43 and smaller grids?~%"))
    (newline)
    (display
     (format #f "To solve this problem, look at "))
    (display
     (format #f "individual cases where~%"))
    (display
     (format #f "the minimum size is fixed, and "))
    (display
     (format #f "then construct formulas~%"))
    (display
     (format #f "that calculate the number of "))
    (display
     (format #f "blocks. Once that~%"))
    (display
     (format #f "has been done for a few cases, "))
    (display
     (format #f "then generalize to~%"))
    (display
     (format #f "arbitrary minimum size. This allows "))
    (display
     (format #f "you to see the~%"))
    (display
     (format #f "patterns quicker, since you only "))
    (display
     (format #f "need to deal with~%"))
    (display
     (format #f "one parameter at a time. The "))
    (display
     (format #f "cross-hatched blocks~%"))
    (display
     (format #f "are a little tricky to count, the "))
    (display
     (format #f "trick is to count~%"))
    (display
     (format #f "the number of 2x2 diamonds that "))
    (display
     (format #f "are present (accounting~%"))
    (display
     (format #f "for the overlaps), then add in "))
    (display
     (format #f "the 1xn blocks (n>2).~%"))
    (newline)
    (display
     (format #f "Let the min size be n, the max "))
    (display
     (format #f "size be x, n and~%"))
    (display
     (format #f "x are fixed : normal blocks~%"))
    (display
     (format #f "Sum(Sum(j*(x-k))_k=0_to_x)_j=0_to_n~%"))
    (display
     (format #f "= Sum(j*x*(x+1)/2)_j=0_to_n~%"))
    (display
     (format #f "= n*(n+1)*x*(x+1)/4.~%"))
    (display (format #f "Cross-hatched blocks:~%"))
    (display (format #f "single 1x1 blocks = 2*n*(n-1)+(2n-1)*max(0,m-n)~%"))
    (display (format #f "1x2 blocks = 4*(n-1)*(m-1)~%"))
    (display (format #f "1x3 blocks = ~%"))
    (display (format #f "1x4 blocks = ~%"))
    (display (format #f "1x5 blocks = ~%"))
    (display (format #f "1xt blocks = ~%"))
    (newline)
    (display (format #f "2x2 blocks = ~%"))
    (display (format #f "~%"))
    (display (format #f "~%"))
    (display (format #f "~%"))
    (newline)
    (display (format #f "~%"))
    (display
     (format #f "see https://projecteuler.net/problem=147~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((max-num 1000)
          (status-num 1000000)
          (debug-flag #t))
      (begin
        (sub-main-loop max-num status-num debug-flag)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 147 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (quit)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

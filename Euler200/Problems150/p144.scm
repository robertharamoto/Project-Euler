#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 144                                    ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated August 1, 2022                               ###
;;;###                                                       ###
;;;###  updated March 12, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### srfi-19 for date functions
(use-modules ((srfi srfi-19)
              :renamer (symbol-prefix-proc 'srfi-19:)))

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
;;; ellipse hardwired as 4x^2+y^2=100
(define (is-point-on-ellipse? xx-0 yy-0 tolerance)
  (begin
    (let ((sum
           (+
            (* 4.0 xx-0 xx-0)
            (* yy-0 yy-0))))
      (begin
        (if (< (abs (- 100.0 sum)) tolerance)
            (begin
              #t)
            (begin
              #f
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-is-point-on-ellipse-1 result-hash-table)
 (begin
   (let ((sub-name "test-is-point-on-ellipse-1")
         (test-list
          (list
           (list 0.0 10.0 #t)
           (list 5.0 0.0 #t)
           (list 0.0 -10.0 #t)
           (list -5.0 0.0 #t)
           (list 0.0 0.0 #f)
           (list 1.0 2.0 #f)
           ))
         (tolerance 1e-12)
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((xx-0 (list-ref this-list 0))
                  (yy-0 (list-ref this-list 1))
                  (shouldbe (list-ref this-list 2)))
              (let ((result
                     (is-point-on-ellipse? xx-0 yy-0 tolerance)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : xx-0=~a, yy-0=~a, "
                        sub-name test-label-index xx-0 yy-0))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define-syntax check-point-macro
  (syntax-rules ()
    ((check-point-macro
      xx-2 yy-2 xx-1 yy-1 m-out tolerance)
     (begin
       (if (not (is-point-on-ellipse? xx-2 yy-2 tolerance))
           (begin
             (display
              (format
               #f "check-point error : (xx-2=~a, yy-2=~a) "
               xx-2 yy-2))
             (display
              (format
               #f "point not on ellipse 4x^2+y^2=100.~%"))
             (display
              (format
               #f "Start from (xx-1=~a, yy-1=~a), slope_out=~a~%"
               xx-1 yy-1 m-out))
             (force-output)
             ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax calc-point-macro
  (syntax-rules ()
    ((calc-point-macro
      xx-1 yy-1 m-out tolerance output-list)
     (begin
       (let ((m-out-2 (* m-out m-out)))
         (let ((aa (+ 4.0 m-out-2))
               (bb (* 2.0 m-out (- yy-1 (* m-out xx-1))))
               (cc (- (+ (* m-out-2 xx-1 xx-1) (* yy-1 yy-1))
                      (+ (* 2.0 m-out xx-1 yy-1) 100.0))))
           (let ((denom (/ 1.0 (* 2.0 aa)))
                 (bb-m-4ac (- (* bb bb) (* 4.0 aa cc))))
             (let ((term1 (* -1.0 bb))
                   (term2 (sqrt bb-m-4ac)))
               (begin
                 (if (>= bb-m-4ac 0.0)
                     (begin
                       (let ((xx-soln-1 (* denom (+ term1 term2)))
                             (xx-soln-2 (* denom (- term1 term2))))
                         (begin
                           (if (< (abs (- xx-soln-1 xx-1)) tolerance)
                               (begin
                                 (let ((xx-2 xx-soln-2)
                                       (yy-2
                                        (+ (* m-out (- xx-soln-2 xx-1))
                                           yy-1)))
                                   (begin
                                     (set! output-list (list xx-2 yy-2))
                                     (check-point-macro
                                      xx-2 yy-2 xx-1 yy-1 m-out tolerance)
                                     )))
                               (begin
                                 (let ((xx-2 xx-soln-1)
                                       (yy-2 (+ (* m-out (- xx-soln-1 xx-1))
                                                yy-1)))
                                   (begin
                                     (set! output-list (list xx-2 yy-2))
                                     (check-point-macro
                                      xx-2 yy-2 xx-1 yy-1 m-out tolerance)
                                     ))
                                 ))
                           )))
                     (begin
                       (display
                        (format
                         #f "error condition: no real solution for~%"))
                       (display
                        (format
                         #f "  xx-1=~a, yy-1=~a, m-out=~a~%"
                         xx-1 yy-1 m-out))
                       (display
                        (format
                         #f "  b^2-4ac=~a~%"
                         bb-m-4ac))
                       (display
                        (format #f "quitting...~%"))
                       (force-output)
                       (quit)
                       ))
                 )))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
;;; (x0, y0) is the previous point on the ellipse
;;; (x1, y1) is the current point on the ellipse (where ray struck last)
;;; (x2, y2) is the next point on the ellipse
(define (calculate-next-point xx-0 yy-0 xx-1 yy-1)
  (begin
    (let ((result-list (list))
          (tolerance 1e-10))
      (begin
        (cond
         ((< (abs xx-1) tolerance)
          (begin
            ;;; normal slope is vertical (infinite)
            (if (< (abs xx-0) tolerance)
                (begin
                  ;;; incoming ray is straight down
                  ;;; outgoing ray is straight back up
                  (list xx-0 yy-0))
                (begin
                  ;;; incoming ray is non-vertical
                  (let ((m-out
                         (* -1.0
                            (/ (- yy-1 yy-0) (- xx-1 xx-0)))))
                    (let ((output-list (list)))
                      (begin
                        (calc-point-macro
                         xx-1 yy-1 m-out tolerance output-list)
                        output-list
                        )))
                  ))
            ))
         (else
          (begin
            (if (< (abs (- xx-1 xx-0)) tolerance)
                (begin
                  ;;; input line is a vertical
                  (let ((m-normal (/ yy-1 (* 4.0 xx-1))))
                    (let ((m-normal-2 (* m-normal m-normal)))
                      (let ((m-numer (1- m-normal-2))
                            (m-denom (* 2.0 m-normal)))
                        (begin
                          (if (> (abs m-denom) tolerance)
                              (begin
                                (let ((m-out (/ m-numer m-denom))
                                      (output-list (list)))
                                  (begin
                                    (calc-point-macro
                                     xx-1 yy-1 m-out
                                     tolerance output-list)
                                    output-list
                                    )))
                              (begin
                                ;;; output line is a vertical
                                (check-point-macro
                                 xx-1 yy-0 xx-1 yy-1
                                 999999.99 tolerance)
                                (list xx-1 yy-0)
                                ))
                          ))
                      )))
                (begin
                  (let ((m-in (/ (- yy-1 yy-0) (- xx-1 xx-0)))
                        (m-normal (/ yy-1 (* 4.0 xx-1))))
                    (let ((m-normal-2 (* m-normal m-normal)))
                      (let ((m-numer (+ (* m-in (1- m-normal-2))
                                        (* 2.0 m-normal)))
                            (m-denom (- (1+ (* 2.0 m-in m-normal))
                                        m-normal-2)))
                        (begin
                          (if (> (abs m-denom) tolerance)
                              (begin
                                (let ((m-out (/ m-numer m-denom))
                                      (output-list (list)))
                                  (begin
                                    (calc-point-macro
                                     xx-1 yy-1 m-out
                                     tolerance output-list)
                                    output-list
                                    )))
                              (begin
                                ;;; output line is a vertical
                                (check-point-macro
                                 xx-1 yy-1 xx-1 yy-1
                                 999999.99 tolerance)
                                (list xx-1 yy-1)
                                ))
                          ))
                      ))
                  ))
            )))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-calculate-next-point-1 result-hash-table)
 (begin
   (let ((sub-name "test-calculate-next-point-1")
         (test-list
          (list
           (list 0.0 10.0 0.0 -10.0 1e-12 (list 0.0 10.0))
           (list 0.0 10.0 5.0 0.0 1e-12 (list 0.0 -10.0))
           (list 5.0 0.0 0.0 -10.0 1e-12 (list -5.0 0.0))
           (list 0.0 -10.0 -5.0 0.0 1e-12 (list 0.0 10.0))
           (list 0.0 10.0 -5.0 0.0 1e-12 (list 0.0 -10.0))
           (list -5.0 0.0 0.0 -10.0 1e-12 (list 5.0 0.0))
           (list 0.0 -10.0 5.0 0.0 1e-12 (list 0.0 10.0))
           (list 0.0 10.1 1.4 -9.6 1e-2 (list -3.99 -6.02))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((xx-0 (list-ref this-list 0))
                  (yy-0 (list-ref this-list 1))
                  (xx-1 (list-ref this-list 2))
                  (yy-1 (list-ref this-list 3))
                  (tolerance (list-ref this-list 4))
                  (shouldbe-list (list-ref this-list 5)))
              (let ((result-list
                     (calculate-next-point xx-0 yy-0 xx-1 yy-1)))
                (let ((slen (length shouldbe-list))
                      (rlen (length result-list)))
                  (let ((err-1
                         (format
                          #f "~a : error (~a) : xx-0=~a, yy-0=~a, "
                          sub-name test-label-index xx-0 yy-0))
                        (err-2
                         (format
                          #f "xx-1=~a, yy-1=~a, "
                          xx-1 yy-1))
                        (err-3
                         (format
                          #f "shouldbe length=~a, result length=~a"
                          slen rlen)))
                    (begin
                      (unittest2:assert?
                       (equal? slen rlen)
                       sub-name
                       (string-append err-1 err-2 err-3)
                       result-hash-table)

                      (do ((ii 0 (1+ ii)))
                          ((>= ii slen))
                        (begin
                          (let ((s-elem (list-ref shouldbe-list ii))
                                (r-elem (list-ref result-list ii)))
                            (let ((err-4
                                   (format
                                    #f "[~a] shouldbe=~a, result=~a"
                                    ii s-elem r-elem)))
                              (begin
                                (unittest2:assert?
                                 (<=
                                  (abs
                                   (- s-elem r-elem)) tolerance)
                                 sub-name
                                 (string-append err-1 err-2 err-4)
                                 result-hash-table)
                                )))
                          ))
                      )))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define-syntax display-status-info-macro
  (syntax-rules ()
    ((display-status-info-macro
      counter xx yy start-jday)
     (begin
       (let ((end-jday (srfi-19:current-julian-day)))
         (begin
           (display
            (ice-9-format:format
             #f "count = ~:d : last point (~2,3f, ~2,3f)~%"
             counter xx yy))
           (display
            (format
             #f "  elapsed time = ~a : ~a~%"
             (timer-module:julian-day-difference-to-string
              end-jday start-jday)
             (timer-module:current-date-time-string)))
           (force-output)
           (set! start-jday end-jday)
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop
         xx-0 yy-0 xx-1 yy-1 min-xx max-xx
         status-num debug-flag)
  (begin
    (let ((count 1)
          (ll-xx-0 xx-0)
          (ll-yy-0 yy-0)
          (ll-xx-1 xx-1)
          (ll-yy-1 yy-1)
          (min-yy (sqrt (- 100.0 (* 4.0 min-xx min-xx))))
          (max-yy (sqrt (- 100.0 (* 4.0 max-xx max-xx))))
          (tolerance 1e-10)
          (continue-loop-flag #t)
          (start-jday (srfi-19:current-julian-day)))
      (begin
        (while
         (equal? continue-loop-flag #t)
         (begin
           (if (and (>= ll-xx-1 min-xx)
                    (<= ll-xx-1 max-xx)
                    (>= ll-yy-1 min-yy))
               (begin
                 ;;; ray has escaped the white cell
                 ;;; adjust count, which counted the escape point
                 (set! count (1- count))
                 (set! continue-loop-flag #f))
               (begin
                 ;;; ray still within white cell
                 (let ((next-list
                        (calculate-next-point
                         ll-xx-0 ll-yy-0 ll-xx-1 ll-yy-1)))
                   (let ((next-xx (list-ref next-list 0))
                         (next-yy (list-ref next-list 1)))
                     (begin
                       (if (equal? debug-flag #t)
                           (begin
                             (check-point-macro
                              next-xx next-yy
                              ll-xx-1 ll-yy-1 88888888.88 tolerance)
                             (display
                              (ice-9-format:format
                               #f "(~:d) last point = (~2,3f, ~2,3f)~%"
                               count ll-xx-1 ll-yy-1))
                             (display
                              (ice-9-format:format
                               #f "    next point = (~2,3f, ~2,3f)~%"
                               next-xx next-yy))
                             (force-output)
                             ))

                       (set! count (1+ count))

                       (set! ll-xx-0 ll-xx-1)
                       (set! ll-yy-0 ll-yy-1)

                       (set! ll-xx-1 next-xx)
                       (set! ll-yy-1 next-yy)
                       )))
                 ))

           (if (zero? (modulo count status-num))
               (begin
                 (display-status-info-macro
                  count ll-xx-1 ll-yy-1 start-jday)
                 ))
           ))

        (display
         (ice-9-format:format
          #f "The number of times the beam hits the~%"))
        (display
         (ice-9-format:format
          #f "cell wall is = ~:d~%"
          count))
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "In laser physics, a 'white cell' "))
    (display
     (format #f "is a mirror~%"))
    (display
     (format #f "system that acts as a delay line "))
    (display
     (format #f "for the laser~%"))
    (display
     (format #f "beam. The beam enters the cell, "))
    (display
     (format #f "bounces around on~%"))
    (display
     (format #f "the mirrors, and eventually works "))
    (display
     (format #f "its way back out.~%"))
    (newline)
    (display
     (format #f "The specific white cell we will "))
    (display
     (format #f "be considering is~%"))
    (display
     (format #f "an ellipse with the equation~%"))
    (display
     (format #f "4x^2 + y^2 = 100~%"))
    (newline)
    (display
     (format #f "The section corresponding to "))
    (display
     (format #f "0.01 <= x <= +0.01~%"))
    (display
     (format #f "at the top is missing, allowing "))
    (display
     (format #f "the light to enter~%"))
    (display
     (format #f "and exit through the hole.~%"))
    (newline)
    (display
     (format #f "The light beam in this problem "))
    (display
     (format #f "starts at the point~%"))
    (display
     (format #f "(0.0,10.1) just outside the white "))
    (display
     (format #f "cell, and the~%"))
    (display
     (format #f "beam first impacts the mirror at "))
    (display
     (format #f "(1.4,-9.6).~%"))
    (newline)
    (display
     (format #f "Each time the laser beam hits the "))
    (display
     (format #f "surface of the~%"))
    (display
     (format #f "ellipse, it follows the usual law "))
    (display
     (format #f "of reflection 'angle~%"))
    (display
     (format #f "of incidence equals angle of reflection.' "))
    (display
     (format #f "That is,~%"))
    (display
     (format #f "both the incident and reflected "))
    (display
     (format #f "beams make the same~%"))
    (display
     (format #f "angle with the normal line at the "))
    (display
     (format #f "point of incidence.~%"))
    (newline)
    (display
     (format #f "In the figure on the left, the red "))
    (display
     (format #f "line shows the~%"))
    (display
     (format #f "first two points of contact between "))
    (display
     (format #f "the laser beam and~%"))
    (display
     (format #f "the wall of the white cell; the blue "))
    (display
     (format #f "line shows the line~%"))
    (display
     (format #f "tangent to the ellipse at the point "))
    (display
     (format #f "of incidence of the~%"))
    (display
     (format #f "first bounce.~%"))
    (newline)
    (display
     (format #f "The slope m of the tangent line at "))
    (display
     (format #f "any point (x,y)~%"))
    (display
     (format #f "of the given ellipse is: "))
    (display
     (format #f "m = -4x/y~%"))
    (newline)
    (display
     (format #f "The normal line is perpendicular to "))
    (display
     (format #f "this tangent line~%"))
    (display
     (format #f "at the point of incidence.~%"))
    (newline)
    (display
     (format #f "The animation on the right shows "))
    (display
     (format #f "the first 10~%"))
    (display
     (format #f "reflections of the beam.~%"))
    (newline)
    (display
     (format #f "How many times does the beam hit "))
    (display
     (format #f "the internal surface~%"))
    (display
     (format #f "of the white cell "))
    (display
     (format #f "before exiting?~%"))
    (newline)
    (display
     (format #f "The equations needed to describe "))
    (display
     (format #f "the reflections within~%"))
    (display
     (format #f "the ellipse can be found at~%"))
    (display
     (format #f "https://www.emathzone.com/tutorials/geometry/equation-of-tangent-and-normal-to-ellipse.html~%"))
    (display
     (format #f "which gives the equations for "))
    (display
     (format #f "the tangent and~%"))
    (display
     (format #f "normal lines at each point on "))
    (display
     (format #f "the ellipse. We~%"))
    (display
     (format #f "also need to find the angle that "))
    (display
     (format #f "the normal and the~%"))
    (display
     (format #f "ray make, which is given at~%"))
    (display
     (format #f "https://www.algebra.com/algebra/homework/Quadratic-relations-and-conic-sections/Optical-property-of-an-ellipse.lesson~%"))
    (newline)
    (display
     (format #f "When the ray hits point (x, y), "))
    (display
     (format #f "the slope of the~%"))
    (display
     (format #f "line can be found initially as "))
    (display
     (format #f "m say, and the~%"))
    (display
     (format #f "slope of the tangent line is -4x/y, "))
    (display
     (format #f "so the slope of~%"))
    (display
     (format #f "the normal line is m_n=+y/(4x), so "))
    (display
     (format #f "we can find the~%"))
    (display
     (format #f "angle between the two lines as~%"))
    (display
     (format #f "phi = arctan((m-m_n)/(1+m*m_n)).~%"))
    (display
     (format #f "Since the angle of incidence equals "))
    (display
     (format #f "the angle of reflection,~%"))
    (display
     (format #f "this says~%"))
    (display
     (format #f "arctan((m-m_n)/(1+m*m_n)) "))
    (display
     (format #f "= arctan((m_n-m_out)/(1+m_n*m_out))~%"))
    (display
     (format #f "or (m-m_n)/(1+m*m_n) "))
    (display
     (format #f "= (m_n-m_out)/(1+m_n*m_out).~%"))
    (display
     (format #f "Re-arranging we have~%"))
    (display
     (format #f "m_out=(m*(m_n^2-1)+2m_n)/(1+2m*m_n-m_n^2)~%"))
    (newline)
    (display
     (format #f "Since we know the point (x1, y1), "))
    (display
     (format #f "and we have a~%"))
    (display
     (format #f "new slope, we can find the "))
    (display
     (format #f "equation for the~%"))
    (display
     (format #f "next line (y-y1) = m_out*(x-x1). "))
    (display
     (format #f "Next we calculate~%"))
    (display
     (format #f "the intersection of this new line "))
    (display
     (format #f "with the ellipse.~%"))
    (display
     (format #f "Let (x2, y2) be the intersection "))
    (display
     (format #f "between this~%"))
    (display
     (format #f "reflected ray and the ellipse, then "))
    (display
     (format #f "(y2-y1) = m_out*(x2-x1)~%"))
    (display
     (format #f "and 4x2^2+y2^2=100.~%"))
    (display
     (format #f "4x2^2+(m_out*(x2-x1)+y1)^2=100,~%"))
    (display
     (format #f "4x2^2+m_out^2*(x2^2-2x2*x1+x1^2)"))
    (display
     (format #f "+2m_out*(x2-x1)*y1+y1^2=100,~%"))
    (display
     (format #f "(4+m_out^2)*x2^2"))
    (display
     (format #f "+(-2m_out^2*x1+2m_out*y1)*x2~%"))
    (display
     (format #f "+(m_out^2*x1^2-2m_out*x1*y1+y1^2-100)=0.~%"))
    (display
     (format #f "Let a=(4+m_out^2),~%"))
    (display
     (format #f "b=2*m_out(-m_out*x1+y1),~%"))
    (display
     (format #f "c=m_out^2*x1^2-2m_out*x1*y1+y1^2-100,~%"))
    (display
     (format #f "the equation becomes "))
    (display
     (format #f "ax2^2+bx2+c=0,~%"))
    (display
     (format #f "and we can use the quadratic "))
    (display
     (format #f "equation to find~%"))
    (display
     (format #f "the two points on the ellipse "))
    (display
     (format #f "where the line~%"))
    (display
     (format #f "intersects, x=-b/2a +/- (sqrt(b^2-4ac))/2a. "))
    (display
     (format #f "One point~%"))
    (display
     (format #f "should be the initial point x1, the "))
    (display
     (format #f "other should~%"))
    (display
     (format #f "be the next point where the beam "))
    (display
     (format #f "hits the internal~%"))
    (display
     (format #f "surface of the white cell.~%"))
    (display
     (format #f "see https://projecteuler.net/problem=144~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((xx-0 0.0)
          (yy-0 10.1)
          (xx-1 1.4)
          (yy-1 -9.6)
          (min-xx -0.01)
          (max-xx 0.01)
          (status-num 1000)
          (debug-flag #f))
      (begin
        (sub-main-loop xx-0 yy-0 xx-1 yy-1 min-xx max-xx
                       status-num debug-flag)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 144 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

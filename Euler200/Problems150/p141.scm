#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 141                                    ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated August 1, 2022                               ###
;;;###                                                       ###
;;;###  updated March 12, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### srfi-11 for multiple value let functions
(use-modules ((srfi srfi-11)
              :renamer (symbol-prefix-proc 'srfi-11:)))

;;;### srfi-19 for time/date functions
(use-modules ((srfi srfi-19)
              :renamer (symbol-prefix-proc 'srfi-19:)))

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (progressive-number-list nn nn-root)
  (begin
    (let ((result-list (list))
          (continue-loop-flag #t))
      (begin
        (do ((dd 2 (1+ dd)))
            ((or (>= dd nn-root)
                 (equal? continue-loop-flag #f)))
          (begin
            (let ((dd-2 (* dd dd)))
              (begin
                (srfi-11:let-values
                 (((qq rr) (euclidean/ nn dd)))
                 (begin
                   (if (and (>= qq dd) (> rr 0))
                       (begin
                         (if (= (* qq rr) dd-2)
                             (begin
                               (set! result-list (list dd qq rr))
                               (set! continue-loop-flag #f)
                               ))
                         ))
                   ))
                ))
            ))

        (if (equal? continue-loop-flag #t)
            (begin
              #f)
            (begin
              result-list
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-progressive-number-list-1 result-hash-table)
 (begin
   (let ((sub-name "test-progressive-number-list-1")
         (test-list
          (list
           (list 2 #f) (list 3 #f)
           (list 9 (list 2 4 1))
           (list 58 (list 6 9 4))
           (list 10404 (list 72 144 36))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((nn (list-ref this-list 0))
                  (shouldbe-list (list-ref this-list 1)))
              (let ((result-list
                     (progressive-number-list
                      nn (exact-integer-sqrt nn))))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : nn=~a, "
                        sub-name test-label-index nn))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe-list result-list)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-list result-list)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (is-progressive-number? nn nn-root)
  (begin
    (let ((result-list
           (progressive-number-list nn nn-root)))
      (begin
        (if (and (list? result-list)
                 (> (length result-list) 1))
            (begin
              #t)
            (begin
              #f
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-is-progressive-number-1 result-hash-table)
 (begin
   (let ((sub-name "test-is-progressive-number-1")
         (test-list
          (list
           (list 2 #f) (list 3 #f)
           (list 58 #t)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((nn (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result
                     (is-progressive-number?
                      nn (exact-integer-sqrt nn))))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : nn=~a, "
                        sub-name test-label-index nn))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define-syntax display-debug-info-macro
  (syntax-rules ()
    ((display-debug-info-macro
      nn dd qq rr)
     (begin
       (let ((r1 (/ qq dd)))
         (begin
           (display
            (ice-9-format:format
             #f "  nn=~:d, quotient=~:d, divisor=~:d, "
             nn qq dd))
           (display
            (ice-9-format:format
             #f "remainder=~a, ratio=~a~%" rr r1))
           (force-output)
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax display-status-info-macro
  (syntax-rules ()
    ((display-status-info-macro
      sq-counter max-ii sum count start-jday)
     (begin
       (let ((end-jday (srfi-19:current-julian-day)))
         (begin
           (display
            (ice-9-format:format
             #f "~:d / ~:d : sum = ~:d, count = ~:d : "
             sq-counter max-ii sum count))
           (display
            (format
             #f "elapsed time = ~a : ~a~%"
             (timer-module:julian-day-difference-to-string
              end-jday start-jday)
             (timer-module:current-date-time-string)))
           (force-output)
           (set! start-jday end-jday)
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax run-inner-vloop-macro
  (syntax-rules ()
    ((run-inner-vloop-macro
      tt ss vv ss-3 vv-num max-num
      count sum debug-flag)
     (begin
       (do ((vv 1 (1+ vv)))
           ((> vv-num max-num))
         (begin
           (set! vv-num (* vv tt (+ (* ss-3 vv) tt)))

           (let ((vv-la (logand vv-num 15)))
             (begin
               (if (or (= vv-la 0) (= vv-la 1)
                       (= vv-la 4) (= vv-la 9))
                   (begin
                     (let ((mm-2 vv-num)
                           (mm (exact-integer-sqrt vv-num)))
                       (begin
                         (if (and (= mm-2 (* mm mm))
                                  (< mm-2 max-num))
                             (begin
                               (set! count (1+ count))
                               (set! sum (+ sum mm-2))

                               (if (equal? debug-flag #t)
                                   (begin
                                     (let ((rr (* vv tt tt))
                                           (qq (* ss vv tt))
                                           (dd (* ss ss vv)))
                                       (begin
                                         (display-debug-info-macro
                                          mm-2 qq dd rr)
                                         ))
                                     ))
                               ))
                         ))
                     ))
               ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop max-num status-num debug-flag)
  (begin
    (let ((count 0)
          (sum 0)
          (tt-num -1)
          (start-jday (srfi-19:current-julian-day)))
      (begin
        (do ((tt 1 (1+ tt)))
            ((> tt-num max-num))
          (begin
            (let ((tt-p1 (1+ tt))
                  (ss-num -1))
              (begin
                (set!
                 tt-num
                 (+ (* tt tt-p1 tt-p1 tt-p1) (* tt tt)))

                (do ((ss (1+ tt) (1+ ss)))
                    ((> ss-num max-num))
                  (begin
                    (let ((ss-3 (* ss ss ss))
                          (vv-num -1))
                      (begin
                        (set! ss-num (* tt (+ ss-3 tt)))
                        (if (= (gcd tt ss) 1)
                            (begin
                              (run-inner-vloop-macro
                               tt ss vv ss-3 vv-num max-num
                               count sum debug-flag)
                              ))
                        ))
                    ))
                ))

            (if (zero? (modulo tt status-num))
                (begin
                  (display-status-info-macro
                   tt-num max-num sum count start-jday)
                  ))
            ))

        (display
         (ice-9-format:format
          #f "The sum of progressive perfect squares = ~:d.~%"
          sum))
        (display
         (ice-9-format:format
          #f "  Found ~:d progressive perfect squares~%"
          count))
        (display
         (ice-9-format:format
          #f "  (for numbers less than ~:d).~%"
          max-num))
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "A positive integer, n, is divided "))
    (display
     (format #f "by d and the~%"))
    (display
     (format #f "quotient and remainder are q and r "))
    (display
     (format #f "respectively. In~%"))
    (display
     (format #f "addition d, q, and r are consecutive "))
    (display
     (format #f "positive integer~%"))
    (display
     (format #f "terms in a geometric sequence, but "))
    (display
     (format #f "not necessarily in~%"))
    (display
     (format #f "that order.~%"))
    (newline)
    (display
     (format #f "For example, 58 divided by 6 has "))
    (display
     (format #f "quotient 9 and~%"))
    (display
     (format #f "remainder 4. It can also be seen "))
    (display
     (format #f "that 4, 6, 9 are~%"))
    (display
     (format #f "consecutive terms in a geometric "))
    (display
     (format #f "sequence (common~%"))
    (display
     (format #f "ratio 3/2). We will call such "))
    (display
     (format #f "numbers, n,~%"))
    (display
     (format #f "progressive.~%"))
    (newline)
    (display
     (format #f "Some progressive numbers, such "))
    (display
     (format #f "as 9 and~%"))
    (display
     (format #f "10404 = 102^2, happen to also "))
    (display
     (format #f "be perfect squares.~%"))
    (display
     (format #f "The sum of all progressive perfect "))
    (display
     (format #f "squares below one~%"))
    (display
     (format #f "hundred thousand~%"))
    (display
     (format #f "is 124657.~%"))
    (newline)
    (display
     (format #f "Find the sum of all progressive "))
    (display
     (format #f "perfect squares below~%"))
    (display
     (format #f "one trillion (10^12).~%"))
    (newline)
    (display
     (format #f "Assume that n=q*d+r, and that r<q<d, "))
    (display
     (format #f "where d a divisor~%"))
    (display
     (format #f "less than sqrt(n), q is the "))
    (display
     (format #f "quotient, and r is~%"))
    (display
     (format #f "the remainder. The requirement "))
    (display
     (format #f "that n is progressive~%"))
    (display
     (format #f "means that d/q = q/r = s/t >= 1. "))
    (display
     (format #f "Then d=qs/t, and~%"))
    (display
     (format #f "q=rs/t, so d=rs^2/t^2, and "))
    (display
     (format #f "n=q*d+r=s^3*r^2/t^3+r.~%"))
    (display
     (format #f "The requirement that n is a "))
    (display
     (format #f "square, means that~%"))
    (display
     (format #f "n = m^2 = s^3*r^2/t^3+r.~%"))
    (display
     (format #f "Let r=v*t^2, then "))
    (display
     (format #f "m^2 = (s^3*v^2*t+v*t^2)~%"))
    (display
     (format #f "or m^2 = v*t*(s^3*v+t).~%"))
    (newline)
    (display
     (format #f "Reversing the variables, gives "))
    (display
     (format #f "r=v*t^2, q=s*v*t,~%"))
    (display
     (format #f "d=s^2*v, and n = m^2 = v*t*(s^3*v+t)~%"))
    (display
     (format #f "see https://projecteuler.net/problem=141~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((max-num 100000)
          (status-num 1000000)
          (debug-flag #t))
      (begin
        (sub-main-loop max-num status-num debug-flag)
        ))

    (newline)
    (let ((max-num (inexact->exact 1e12))
          (status-num 1000)
          (debug-flag #f))
      (begin
        (sub-main-loop max-num status-num debug-flag)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 141 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

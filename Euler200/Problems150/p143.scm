#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 143                                    ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated August 1, 2022                               ###
;;;###                                                       ###
;;;###  updated March 12, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### srfi-19 for date functions
(use-modules ((srfi srfi-19)
              :renamer (symbol-prefix-proc 'srfi-19:)))

;;;### srfi-1 for fold function
(use-modules ((srfi srfi-1)
              :renamer (symbol-prefix-proc 'srfi-1:)))

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
;;;### uses the algorithm found at https://stackoverflow.com/questions/295579/fastest-way-to-determine-if-an-integers-square-root-is-an-integer
;;;### (logand 1^2 15) = 1, (logand 2^2 15) = 4, (logand 3^2 15) 9,
;;;### (logand 4^2 15) = 0, (logand 5^2 15) = 9, (logand 6^2 15) = 4,
;;;### (logand 7^2 15) = 1, (logand 8^2 15) = 0, (logand 9^2 15) = 1,
;;;### (logand 10^2 15) = 4, (logand 11^2 15) = 9, (logand 12^2 15) = 0,...
(define (is-perfect-square? anum)
  (begin
    (let ((lan (logand anum 15)))
      (begin
        (cond
         ((< anum 0)
          (begin
            #f
            ))
         ((or (equal? lan 0)
              (equal? lan 1)
              (equal? lan 4)
              (equal? lan 9))
          (begin
            (let ((ltmp (exact-integer-sqrt anum)))
              (begin
                (equal? (* ltmp ltmp) anum)
                ))
            ))
         (else
          (begin
            #f
            )))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-is-perfect-square-1 result-hash-table)
 (begin
   (let ((sub-name "test-is-perfect-square-1")
         (test-list
          (list
           (list -10 #f) (list 0 #t) (list 1 #t)
           (list 2 #f) (list 3 #f) (list 4 #t) (list 5 #f)
           (list 6 #f) (list 7 #f) (list 8 #f) (list 9 #t)
           (list 10 #f) (list 11 #f) (list 12 #f) (list 13 #f)
           (list 14 #f) (list 15 #f) (list 16 #t) (list 17 #f)
           (list 18 #f) (list 19 #f) (list 20 #f) (list 21 #f)
           (list 22 #f) (list 23 #f) (list 24 #f) (list 25 #t)
           (list 26 #f) (list 27 #f) (list 28 #f) (list 29 #f)
           (list 30 #f) (list 31 #f) (list 32 #f) (list 33 #f)
           (list 36 #t) (list 49 #t) (list 64 #t) (list 81 #t)
           (list 100 #t) (list 121 #t) (list 144 #t) (list 169 #t)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((nn (list-ref this-list 0))
                  (shouldbe-bool (list-ref this-list 1)))
              (let ((result-bool (is-perfect-square? nn)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : nn=~a, "
                        sub-name test-label-index nn))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe-bool result-bool)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-bool result-bool)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define-syntax perfect-square-result-list-macro
  (syntax-rules ()
    ((perfect-square-result-list-macro
      aa bb aa-2 bb-2 aabb
      max-num
      result-list)
     (begin
       (let ((tmp-ab (+ aa bb))
             (klen (+ aa bb))
             (tmp-aabb
              (+ aa-2 bb-2 aabb)))
         (begin
           (if (< aa bb)
               (begin
                 (let ((tmp bb))
                   (begin
                     (set! bb aa)
                     (set! aa tmp)
                     ))
                 ))
           (do ((kk 1 (1+ kk)))
               ((> klen max-num))
             (begin
               (let ((kaa (* kk aa))
                     (kbb (* kk bb))
                     (tmp-klen (* kk tmp-ab))
                     (ksqr (* kk kk tmp-aabb)))
                 (let ((t1 (list kbb kaa)))
                   (begin
                     (if (and (< kaa max-num)
                              (< kbb max-num))
                         (begin
                           (set!
                            result-list
                            (cons t1 result-list))
                           ))

                     (set! klen tmp-klen)
                     )))
               ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (generate-pairs-list max-num)
  (begin
    (let ((max-nn (1+ (exact-integer-sqrt max-num)))
          (result-list (list)))
      (begin
        (do ((nn 1 (1+ nn)))
            ((> nn max-nn))
          (begin
            (let ((nn-2 (* nn nn)))
              (begin
                (do ((mm (1+ nn) (1+ mm)))
                    ((>= mm max-nn))
                  (begin
                    (if (and
                         (= (gcd mm nn) 1)
                         (not (zero? (modulo (- mm nn) 3))))
                        (begin
                          (let ((mn (* mm nn))
                                (mm-2 (* mm mm)))
                            (let ((aa (+ (* 2 mn) nn-2))
                                  (bb (- mm-2 nn-2))
                                  (cc (+ mm-2 mn nn-2)))
                              (let ((aa-2 (* aa aa))
                                    (bb-2 (* bb bb))
                                    (aabb (* aa bb)))
                                (begin
                                  (if (is-perfect-square?
                                       (+ aa-2 bb-2 aabb))
                                      (begin
                                        (perfect-square-result-list-macro
                                         aa bb aa-2 bb-2 aabb
                                         max-num
                                         result-list)
                                        ))
                                  ))
                              ))
                          ))
                    ))
                ))
            ))

        (sort
         result-list
         (lambda (a b)
           (begin
             (let ((a-1 (car a))
                   (b-1 (car b)))
               (begin
                 (cond
                  ((= a-1 b-1)
                   (begin
                     (< (cadr a) (cadr b))
                     ))
                  (else
                   (begin
                     (< a-1 b-1)
                     )))
                 ))
             )))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (p143-assert-list-lists-equal
         sub-name error-start
         shouldbe-list-list result-list-list
         result-hash-table)
  (begin
    (let ((slen (length shouldbe-list-list))
          (rlen (length result-list-list)))
      (let ((err-2
             (format
              #f "shouldbe length=~a, result length=~a"
              slen rlen)))
        (begin
          (unittest2:assert?
           (equal? slen rlen)
           sub-name
           (string-append error-start err-2)
           result-hash-table)
          )))

    (for-each
     (lambda (slist)
       (begin
         (let ((err-3
                (format
                 #f " missing shouldbe=~a, result=~a"
                 slist result-list-list)))
           (begin
             (unittest2:assert?
              (not
               (equal?
                (member slist result-list-list)
                #f))
              sub-name
              (string-append error-start err-3)
              result-hash-table)
             ))
         )) shouldbe-list-list)
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-generate-pairs-list-1 result-hash-table)
 (begin
   (let ((sub-name "test-generate-pairs-list-1")
         (test-list
          (list
           (list 10 (list (list 3 5)))
           (list 20 (list (list 3 5) (list 6 10) (list 7 8)
                          (list 9 15) (list 14 16)))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((max-num (list-ref this-list 0))
                  (shouldbe-list-list (list-ref this-list 1)))
              (let ((result-list-list
                     (generate-pairs-list max-num)))
                (let ((error-start
                       (format
                        #f "~a : error (~a) : max-num=~a, "
                        sub-name test-label-index max-num)))
                  (begin
                    (p143-assert-list-lists-equal
                     sub-name error-start
                     shouldbe-list-list result-list-list
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (extract-all-first-items pairs-list)
  (begin
    (let ((result-list (list))
          (plen (length pairs-list)))
      (begin
        (do ((ii 0 (1+ ii)))
            ((>= ii plen))
          (begin
            (let ((this-pair (list-ref pairs-list ii)))
              (let ((this-elem (car this-pair)))
                (begin
                  (set!
                   result-list
                   (cons this-elem result-list))
                  )))
            ))

        (srfi-1:delete-duplicates result-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-extract-all-first-items-1 result-hash-table)
 (begin
   (let ((sub-name "test-extract-all-first-items-1")
         (test-list
          (list
           (list (list (list 3 5) (list 1 2) (list 2 3) (list 4 3))
                 (list 1 2 3 4))
           (list (list (list 3 5) (list 1 2) (list 2 3) (list 1 7))
                 (list 1 2 3))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((pairs-list-list (list-ref this-list 0))
                  (shouldbe-list (list-ref this-list 1)))
              (let ((result-list
                     (extract-all-first-items pairs-list-list)))
                (let ((error-start
                       (format
                        #f "~a : error (~a) : pairs-list-list=~a, "
                        sub-name test-label-index pairs-list-list)))
                  (begin
                    (p143-assert-list-lists-equal
                     sub-name error-start
                     shouldbe-list result-list
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (extract-all-second-items pairs-list)
  (begin
    (let ((result-list (list))
          (plen (length pairs-list)))
      (begin
        (do ((ii 0 (1+ ii)))
            ((>= ii plen))
          (begin
            (let ((this-pair (list-ref pairs-list ii)))
              (let ((this-elem (cadr this-pair)))
                (begin
                  (set!
                   result-list (cons this-elem result-list))
                  )))
            ))

        (srfi-1:delete-duplicates result-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-extract-all-second-items-1 result-hash-table)
 (begin
   (let ((sub-name "test-extract-all-second-items-1")
         (test-list
          (list
           (list (list (list 3 5) (list 1 2) (list 2 3) (list 4 3))
                 (list 2 3 5))
           (list (list (list 3 5) (list 1 2) (list 2 3) (list 4 7))
                 (list 2 3 5 7))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((pairs-list-list (list-ref this-list 0))
                  (shouldbe-list (list-ref this-list 1)))
              (let ((result-list
                     (extract-all-second-items pairs-list-list)))
                (let ((error-start
                       (format
                        #f "~a : error (~a) : pairs-list-list=~a, "
                        sub-name test-label-index pairs-list-list)))
                  (begin
                    (p143-assert-list-lists-equal
                     sub-name error-start
                     shouldbe-list result-list
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (populate-index-hash! index-htable pairs-list)
  (begin
    (let ((plen (length pairs-list)))
      (begin
        (hash-clear! index-htable)

        (do ((ii 0 (1+ ii)))
            ((>= ii plen))
          (begin
            (let ((alist (list-ref pairs-list ii)))
              (let ((pp (car alist)))
                (let ((hindex (hash-ref index-htable pp -1)))
                  (begin
                    (if (< hindex 0)
                        (begin
                          (hash-set! index-htable pp ii)
                          ))
                    ))
                ))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-populate-index-hash-1 result-hash-table)
 (begin
   (let ((sub-name "test-populate-index-hash-1")
         (pairs-list
          (list
           (list 3 4) (list 3 5) (list 3 6) (list 5 5)
           (list 5 7) (list 5 9) (list 5 10) (list 7 11)
           (list 7 12) (list 7 13) (list 7 14) (list 8 88)))
         (test-list
          (list
           (list 3 0) (list 5 3) (list 7 7)
           (list 8 11)
           ))
         (index-htable (make-hash-table 10))
         (test-label-index 0))
     (begin
       (populate-index-hash! index-htable pairs-list)

       (for-each
        (lambda (a-list)
          (begin
            (let ((a-num (list-ref a-list 0))
                  (shouldbe-index (list-ref a-list 1)))
              (let ((result-index
                     (hash-ref index-htable a-num -1)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : a-num=~a, "
                        sub-name test-label-index a-num))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a~%"
                        shouldbe-index result-index)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe-index result-index)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (extract-all-pairs-first-equals
         pp pairs-array psize index-htable)
  (begin
    (let ((result-list (list))
          (start-index (hash-ref index-htable pp 0))
          (break-flag #f)
          (last-elem pp))
      (begin
        (do ((ii start-index (1+ ii)))
            ((or (>= ii psize)
                 (equal? break-flag #t)))
          (begin
            (let ((this-pair (array-ref pairs-array ii)))
              (let ((this-elem (car this-pair)))
                (begin
                  (if (= this-elem last-elem)
                      (begin
                        (set!
                         result-list (cons this-pair result-list)))
                      (begin
                        (set! break-flag #t)
                        ))

                  (set! this-elem last-elem)
                  )))
            ))
        (reverse result-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-extract-all-pairs-first-equals-1 result-hash-table)
 (begin
   (let ((sub-name "test-extract-all-pairs-first-equals-1")
         (pairs-list
          (list
           (list 3 5) (list 3 7) (list 3 9) (list 3 10)
           (list 5 5) (list 7 7) (list 8 8) (list 8 9)))
         (test-list
          (list
           (list 3 (list (list 3 5) (list 3 7)
                         (list 3 9) (list 3 10)))
           (list 5 (list (list 5 5)))
           (list 7 (list (list 7 7)))
           (list 8 (list (list 8 8) (list 8 9)))
           ))
         (index-htable (make-hash-table 10))
         (test-label-index 0))
     (begin
       (populate-index-hash! index-htable pairs-list)

       (let ((parray (list->array 1 pairs-list))
             (plen (length pairs-list)))
         (begin
           (for-each
            (lambda (this-list)
              (begin
                (let ((pp (list-ref this-list 0))
                      (shouldbe-list-list (list-ref this-list 1)))
                  (let ((result-list-list
                         (extract-all-pairs-first-equals
                          pp parray plen index-htable)))
                    (let ((error-start
                           (format
                            #f "~a : error (~a) : pp=~a, "
                            sub-name test-label-index pp)))
                      (begin
                        (p143-assert-list-lists-equal
                         sub-name error-start
                         shouldbe-list-list result-list-list
                         result-hash-table)
                        ))
                    ))
                (set! test-label-index (1+ test-label-index))
                )) test-list)
           ))
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (pairs-to-triples
         pp pairs-array psize index-htable)
  (begin
    (let ((result-list (list)))
      (let ((first-list
             (extract-all-pairs-first-equals
              pp pairs-array psize index-htable)))
        (let ((first-elements-list
               (extract-all-second-items first-list)))
          (begin
            (for-each
             (lambda (qq)
               (begin
                 (if (not (= pp qq))
                     (begin
                       (let ((second-list
                              (extract-all-pairs-first-equals
                               qq pairs-array psize index-htable)))
                         (let ((second-elements-list
                                (extract-all-second-items
                                 second-list)))
                           (begin
                             (for-each
                              (lambda (rr)
                                (begin
                                  (if (not
                                       (equal?
                                        (member
                                         rr first-elements-list)
                                        #f))
                                      (begin
                                        (if (and
                                             (not (= pp rr))
                                             (not (= qq rr)))
                                            (begin
                                              (set!
                                               result-list
                                               (cons (list pp qq rr)
                                                     result-list))
                                              ))
                                        ))
                                  )) second-elements-list)
                             )))
                       ))
                 )) first-elements-list)

            (reverse result-list)
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-pairs-to-triples-1 result-hash-table)
 (begin
   (let ((sub-name "test-pairs-to-triples-1")
         (pairs-list
          (list
           (list 3 5) (list 3 7) (list 3 9) (list 3 10)
           (list 5 6) (list 5 7) (list 7 13)
           (list 8 9) (list 8 10) (list 8 11) (list 9 10)
           (list 10 11)))
         (test-list
          (list
           (list 3 (list (list 3 5 7) (list 3 9 10)))
           (list 5 (list))
           (list 7 (list))
           (list 8 (list (list 8 9 10) (list 8 10 11)))
           ))
         (index-htable (make-hash-table 10))
         (test-label-index 0))
     (begin
       (populate-index-hash! index-htable pairs-list)

       (let ((parray (list->array 1 pairs-list))
             (plen (length pairs-list)))
         (begin
           (for-each
            (lambda (this-list)
              (begin
                (let ((pp (list-ref this-list 0))
                      (shouldbe-list-list (list-ref this-list 1)))
                  (let ((result-list-list
                         (pairs-to-triples pp parray plen index-htable)))
                    (let ((error-start
                           (format
                            #f "~a : error (~a) : pp=~a, "
                            sub-name test-label-index pp)))
                      (begin
                        (p143-assert-list-lists-equal
                         sub-name error-start
                         shouldbe-list-list result-list-list
                         result-hash-table)
                        ))
                    ))
                (set! test-label-index (1+ test-label-index))
                )) test-list)
           ))
       ))
   ))

;;;#############################################################
;;;#############################################################
(define-syntax display-debug-info-macro
  (syntax-rules ()
    ((display-debug-info-macro
      pp qq rr this-sum sum count max-num)
     (begin
       (display
        (ice-9-format:format
         #f "  (pp, qq, rr) = (~:d, ~:d, ~:d), " pp qq rr))
       (let ((aa-2 (+ (* pp pp) (* qq qq) (* pp qq)))
             (bb-2 (+ (* pp pp) (* rr rr) (* pp rr)))
             (cc-2 (+ (* qq qq) (* rr rr) (* qq rr))))
         (let ((aa (exact-integer-sqrt aa-2))
               (bb (exact-integer-sqrt bb-2))
               (cc (exact-integer-sqrt cc-2)))
           (begin
             (display
              (ice-9-format:format
               #f "(aa, bb, cc) = (~:d~a, ~:d~a, ~:d~a) : "
               aa (if (= (* aa aa) aa-2) "" "***error***")
               bb (if (= (* bb bb) bb-2) "" "***error***")
               cc (if (= (* cc cc) cc-2) "" "***error***")))
             )))

       (let ((ptmp (+ pp qq rr)))
         (begin
           (display
            (ice-9-format:format
             #f "this-sum = ~:d~a, sum = ~:d, count=~:d~%"
             this-sum (if (> ptmp max-num) "***error***" "")
             sum count))
           ))

       (force-output)
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop max-num debug-flag)
  (begin
    (let ((sum-abc 0)
          (count 0)
          (start-jday (srfi-19:current-julian-day))
          (index-htable (make-hash-table 100))
          (dup-htable (make-hash-table 100))
          (pairs-list #f))
      (begin
        (timer-module:time-code-macro
         (begin
           (let ((plist (generate-pairs-list max-num)))
             (begin
               ;;; generate-pairs-list sorts resulting list
               (set! pairs-list plist)
               ))
           (display
            (ice-9-format:format
             #f "completed generating pairs-list (~:d) : "
             (length pairs-list)))
           (force-output)
           ))

        (let ((first-list
               (extract-all-first-items pairs-list))
              (plen (length pairs-list))
              (pairs-array (list->array 1 pairs-list))
              (result-list (list)))
          (begin
            (populate-index-hash! index-htable pairs-list)

            (for-each
             (lambda (pp)
               (begin
                 (let ((triples-list
                        (pairs-to-triples
                         pp pairs-array plen index-htable)))
                   (begin
                     (for-each
                      (lambda (alist)
                        (begin
                          (let ((pp (list-ref alist 0))
                                (qq (list-ref alist 1))
                                (rr (list-ref alist 2)))
                            (let ((sum (+ pp qq rr)))
                              (begin
                                (if (<= sum max-num)
                                    (begin
                                      (let ((hcount (hash-ref dup-htable sum 0)))
                                        (begin
                                          (if (= hcount 0)
                                              (begin
                                                (set! sum-abc (+ sum-abc sum))
                                                (set! count (1+ count))

                                                (if (equal? debug-flag #t)
                                                    (begin
                                                      (display-debug-info-macro
                                                       pp qq rr sum sum-abc
                                                       count max-num)
                                                      ))
                                                ))
                                          (hash-set! dup-htable sum (1+ hcount))
                                          ))
                                      ))
                                )))
                          )) triples-list)
                     ))
                 )) first-list)

            (display
             (ice-9-format:format
              #f "Sum = ~:d, number of Torricelli triangles = ~:d~%"
              sum-abc count))
            (display
             (ice-9-format:format
              #f "    (p+q+r<=~:d)~%"
              max-num))
            (force-output)
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Let ABC be a triangle with all "))
    (display
     (format #f "interior angles being~%"))
    (display
     (format #f "less than 120 degrees. Let X be "))
    (display
     (format #f "any point inside~%"))
    (display
     (format #f "the triangle and let XA = p, "))
    (display
     (format #f "XB = q, and~%"))
    (display
     (format #f "XC = r.~%"))
    (newline)
    (display
     (format #f "Fermat challenged Torricelli to "))
    (display
     (format #f "find the position~%"))
    (display
     (format #f "of X such that p + q + r was "))
    (display
     (format #f "minimised.~%"))
    (newline)
    (display
     (format #f "Torricelli was able to prove that "))
    (display
     (format #f "if equilateral triangles~%"))
    (display
     (format #f "AOB, BNC and AMC are constructed "))
    (display
     (format #f "on each side of~%"))
    (display
     (format #f "triangle ABC, the circumscribed "))
    (display
     (format #f "circles of AOB,~%"))
    (display
     (format #f "BNC, and AMC will intersect at a "))
    (display
     (format #f "single point, T,~%"))
    (display
     (format #f "inside the triangle. Moreover he "))
    (display
     (format #f "proved that T,~%"))
    (display
     (format #f "called the Torricelli/Fermat point, "))
    (display
     (format #f "minimises p + q + r.~%"))
    (display
     (format #f "Even more remarkable, it can be shown "))
    (display
     (format #f "that when the sum~%"))
    (display
     (format #f "is minimised, AN = BM = CO = p + q + r "))
    (display
     (format #f "and that AN, BM~%"))
    (display
     (format #f "and CO also intersect at T.~%"))
    (newline)
    (display
     (format #f "If the sum is minimised and "))
    (display
     (format #f "a, b, c, p, q~%"))
    (display
     (format #f "and r are all positive integers "))
    (display
     (format #f "we shall call~%"))
    (display
     (format #f "triangle ABC a Torricelli triangle. "))
    (display
     (format #f "For example, a = 399,~%"))
    (display
     (format #f "b = 455, c = 511 is an example of a "))
    (display
     (format #f "Torricelli triangle,~%"))
    (display
     (format #f "with p + q + r = 784.~%"))
    (newline)
    (display
     (format #f "Find the sum of all distinct "))
    (display
     (format #f "values of p + q + r~%"))
    (display
     (format #f "<= 120000 for Torricelli triangles.~%"))
    (newline)
    (display
     (format #f "Note: This problem has been changed "))
    (display
     (format #f "recently, please~%"))
    (display
     (format #f "check that you are using the "))
    (display
     (format #f "right parameters.~%"))
    (newline)
    (display
     (format #f "The solution, which includes a "))
    (display
     (format #f "terrific explanation,~%"))
    (display
     (format #f "was found at~%"))
    (display
     (format #f "https://luckytoilet.wordpress.com/2010/08/25/fermat-points-and-parameterizing-the-120-degree-integer-triangle-project-euler-143/~%"))
    (newline)
    (display
     (format #f "To solve this problem one makes "))
    (display
     (format #f "use of the fact~%"))
    (display
     (format #f "that all three angles of the "))
    (display
     (format #f "Torricelli point~%"))
    (display
     (format #f "are 120 degrees, and so~%"))
    (display
     (format #f "a^2 = q^2+r^2-2rq*cos(120)=q^2+r^2+rq,~%"))
    (display
     (format #f "b^2=p^2+r^2+pr,~%"))
    (display
     (format #f "c^2=p^2+q^2+pq~%"))
    (newline)
    (display
     (format #f "All pairs (a, b) that satisfy "))
    (display
     (format #f "a^2+b^2+ab = square~%"))
    (display
     (format #f "are stored in a list. The algorithm "))
    (display
     (format #f "then looks for~%"))
    (display
     (format #f "all pairs where (p, q), (p, r), "))
    (display
     (format #f "and (q, r) exist,~%"))
    (display
     (format #f "then we have found the "))
    (display
     (format #f "desired solution.~%"))
    (display
     (format #f "see https://projecteuler.net/problem=143~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((max-num 1000)
          (debug-flag #t))
      (begin
        (sub-main-loop max-num debug-flag)
        ))

    (newline)
    (let ((max-num 120000)
          (debug-flag #f))
      (begin
        (sub-main-loop max-num debug-flag)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 143 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

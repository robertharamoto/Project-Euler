#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 127                                    ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated July 30, 2022                                ###
;;;###                                                       ###
;;;###  updated March 11, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
;;; radical is the product of the distinct prime factors of a number
(define (calc-radical-array max-num)
  (begin
    (let ((radical-array (make-array 1 (1+ max-num))))
      (begin
        (do ((cc 2 (1+ cc)))
            ((> cc max-num))
          (begin
            (let ((cc-elem (array-ref radical-array cc)))
              (begin
                (if (= cc-elem 1)
                    (begin
                      (array-set! radical-array cc cc)

                      (do ((jj (+ cc cc) (+ jj cc)))
                          ((> jj max-num))
                        (begin
                          (let ((rad-elem
                                 (array-ref radical-array jj)))
                            (begin
                              (array-set!
                               radical-array (* rad-elem cc) jj)
                              ))
                          ))
                      ))
                ))
            ))
        radical-array
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-calc-radical-array-1 result-hash-table)
 (begin
   (let ((sub-name "test-calc-radical-array-1")
         (test-list
          (list
           (list 2 2) (list 3 3) (list 4 2)
           (list 5 5) (list 6 6) (list 7 7)
           (list 8 2) (list 9 3) (list 10 10)
           (list 11 11) (list 12 6) (list 13 13)
           (list 14 14) (list 15 15) (list 16 2)
           (list 17 17) (list 18 6) (list 19 19)
           (list 20 10)
           ))
         (max-num 20)
         (test-label-index 0))
     (let ((result-array
            (calc-radical-array max-num)))
       (begin
         (for-each
          (lambda (a-list)
            (begin
              (let ((aindex (list-ref a-list 0))
                    (svalue (list-ref a-list 1)))
                (let ((rvalue
                       (array-ref result-array aindex)))
                  (let ((err-1
                         (format
                          #f "~a : error (~a) : aindex=~a, "
                          sub-name test-label-index aindex))
                        (err-2
                         (format
                          #f "shouldbe=~a, result=~a"
                          svalue rvalue)))
                    (begin
                      (unittest2:assert?
                       (equal? svalue rvalue)
                       sub-name
                       (string-append err-1 err-2)
                       result-hash-table)
                      ))
                  ))
              (set! test-label-index (1+ test-label-index))
              )) test-list)
         )))
   ))

;;;#############################################################
;;;#############################################################
(define-syntax aa-cc-inner-loop-macro
  (syntax-rules ()
    ((aa-cc-inner-loop-macro
      aa cc cc-less-rad rad-cc
      result-list-list
      radical-array)
     (begin
       (let ((bb (- cc aa)))
         (begin
           (if (> bb aa)
               (begin
                 (let ((rad-aa (array-ref radical-array aa))
                       (rad-bb (array-ref radical-array bb)))
                   (let ((rad-ab (* rad-aa rad-bb)))
                     (begin
                       (if (< rad-ab cc-less-rad)
                           (begin
                             ;;; if gcd(a, b)=1 then gcd(a, c) = gcd(b, c) = 1
                             (if (= (gcd rad-aa rad-bb) 1)
                                 (begin
                                   (set!
                                    result-list-list
                                    (cons
                                     (list aa bb cc
                                           rad-aa rad-bb rad-cc
                                           (* rad-ab rad-cc))
                                     result-list-list))
                                   ))
                             ))
                       )))
                 ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
;;; radical is the product of the distinct prime factors of a number
(define (calc-radical-product-list radical-array max-num)
  (begin
    (let ((result-list-list (list)))
      (begin
        (do ((cc 2 (1+ cc)))
            ((> cc max-num))
          (begin
            (let ((half-cc (euclidean/ cc 2))
                  (rad-cc (array-ref radical-array cc))
                  (delta 1))
              (let ((cc-less-rad (euclidean/ cc rad-cc)))
                (begin
                  (if (< rad-cc half-cc)
                      (begin
                        (if (even? cc)
                            (begin
                              (set! delta 2)
                              ))

                        (do ((aa 1 (+ aa delta)))
                            ((> aa half-cc))
                          (begin
                            (aa-cc-inner-loop-macro
                             aa cc cc-less-rad rad-cc
                             result-list-list
                             radical-array)
                            ))
                        ))
                  )))
            ))

        (reverse result-list-list)
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-calc-radical-product-list-1 result-hash-table)
 (begin
   (let ((sub-name "test-calc-radical-product-list-1")
         (test-list
          (list
           (list 1 8 9 1 2 3 6)
           (list 1 48 49 1 6 7 42)
           (list 1 63 64 1 21 2 42)
           (list 1 80 81 1 10 3 30)
           (list 5 27 32 5 3 2 30)
           (list 32 49 81 2 7 3 42)
           ))
         (max-num 100)
         (test-label-index 0))
     (let ((radical-array
            (calc-radical-array max-num)))
       (let ((result-list-list
              (calc-radical-product-list
               radical-array max-num)))
         (begin
           (for-each
            (lambda (shouldbe-list)
              (begin
                (let ((err-1
                       (format
                        #f "~a : error (~a) : "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe-list result-list-list)))
                  (begin
                    (unittest2:assert?
                     (not
                      (equal?
                       (member shouldbe-list
                               result-list-list)
                       #f))
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                (set! test-label-index (1+ test-label-index))
                )) test-list)
           ))
       ))
   ))

;;;#############################################################
;;;#############################################################
(define-syntax display-debug-info-macro
  (syntax-rules ()
    ((display-debug-info-macro
      aa bb cc rad-aa rad-bb rad-cc
      sum-cc abc-count)
     (begin
       (display
        (ice-9-format:format
         #f "abc-hit = (~:d; ~:d; ~:d) : rad(~:d) = ~:d < ~:d : "
         aa bb cc (* aa bb cc)
         (* rad-aa rad-bb rad-cc) cc))
       (display
        (ice-9-format:format
         #f "sum-cc = ~:d, abc-count = ~:d~%"
         sum-cc abc-count))
       (display
        (ice-9-format:format
         #f "    aa = ~:d : rad-aa = ~:d~%"
         aa rad-aa))
       (display
        (ice-9-format:format
         #f "    bb = ~:d : rad-bb = ~:d~%"
         bb rad-bb))
       (display
        (ice-9-format:format
         #f "    cc = ~:d : rad-cc = ~:d~%"
         cc rad-cc))
       (force-output)
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop max-cc debug-flag)
  (begin
    (let ((sum-cc 0)
          (abc-count 0)
          (radical-array (calc-radical-array max-cc)))
      (let ((result-list-list
             (calc-radical-product-list radical-array max-cc)))
        (begin
        ;;; summarize results
          (for-each
           (lambda (a-list)
             (begin
               (let ((aa (list-ref a-list 0))
                     (bb (list-ref a-list 1))
                     (cc (list-ref a-list 2))
                     (rad-aa (list-ref a-list 3))
                     (rad-bb (list-ref a-list 4))
                     (rad-cc (list-ref a-list 5))
                     (rad-abc (list-ref a-list 6)))
                 (begin
                   (set! sum-cc (+ sum-cc cc))
                   (set! abc-count (1+ abc-count))

                   (if (equal? debug-flag #t)
                       (begin
                         (display-debug-info-macro
                          aa bb cc rad-aa rad-bb rad-cc
                          sum-cc abc-count)
                         ))
                   ))
               )) result-list-list)

          (display
           (ice-9-format:format
            #f "Sum(c) = ~:d, and there are ~:d "
            sum-cc abc-count))
          (display
           (ice-9-format:format
            #f "abc hits (where c <= ~:d).~%"
            max-cc))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "The radical of n, rad(n), is "))
    (display
     (format #f "the product of distinct~%"))
    (display
     (format #f "prime factors of n. For example, "))
    (display
     (format #f "504 = 2^3 x 3^2 x 7~%"))
    (display
     (format #f "so rad(504) = 2 x 3 x 7 = 42.~%"))
    (newline)
    (display
     (format #f "We shall define the triplet of "))
    (display
     (format #f "positive integers~%"))
    (display
     (format #f "(a, b, c) to be an abc-hit if:~%"))
    (display (format #f "1. GCD(a, b) = GCD(a, c) = GCD(b, c) = 1~%"))
    (display (format #f "2. a < b~%"))
    (display (format #f "3. a + b = c~%"))
    (display (format #f "4. rad(abc) < c~%"))
    (newline)
    (display (format #f "For example, (5, 27, 32) is an abc-hit, because:~%"))
    (display (format #f "1. GCD(5, 27) = GCD(5, 32) = GCD(27, 32) = 1~%"))
    (display (format #f "2. 5 < 27~%"))
    (display (format #f "3. 5 + 27 = 32~%"))
    (display (format #f "4. rad(4320) = 30 < 32~%"))
    (newline)
    (display
     (format #f "It turns out that abc-hits are "))
    (display
     (format #f "quite rare and~%"))
    (display
     (format #f "there are only thirty-one abc-hits "))
    (display
     (format #f "for c < 1000,~%"))
    (display
     (format #f "with Sum(c) = 12523.~%"))
    (newline)
    (display
     (format #f "Find Sum(c) for c < 120000.~%"))
    (newline)
    (display
     (format #f "Note: This problem has been changed "))
    (display
     (format #f "recently, please~%"))
    (display
     (format #f "check that you are using the "))
    (display
     (format #f "right parameters.~%"))
    (newline)
    (display
     (format #f "The first step is to use a method "))
    (display
     (format #f "similar to the~%"))
    (display
     (format #f "Sieve of Eratosthenes (see~%"))
    (display
     (format #f "https://en.wikipedia.org/wiki/Sieve_of_Eratosthenes~%"))
    (display
     (format #f "for more details). The idea is that "))
    (display
     (format #f "every 2nd array~%"))
    (display
     (format #f "element starting with index 2 is "))
    (display
     (format #f "divisible by 2,~%"))
    (display
     (format #f "every third element starting with "))
    (display
     (format #f "index 3 is~%"))
    (display
     (format #f "divisible by 3, and so the radical "))
    (display
     (format #f "for each number~%"))
    (display
     (format #f "is just the product of the primes "))
    (display
     (format #f "that are found~%"))
    (display
     (format #f "with this sieve.~%"))
    (display
     (format #f "Also, if gcd(a, b) = 1, then "))
    (display
     (format #f "gcd(a, a+b) = 1, see~%"))
    (display
     (format #f "https://euler.stephan-brumme.com/127/~%"))
    (display
     (format #f "Since a+b=c, then "))
    (display
     (format #f "rad(abc)=rad(a)*rad(b)*rad(c)>=2*rad(c).~%"))
    (display
     (format #f "b=c-a>= rad(abc) - a >=2*rad(c) - a. "))
    (display
     (format #f "b+a>2*rad(c),~%"))
    (display
     (format #f "or 2b>a+b>2*rad(c). This suggests "))
    (display
     (format #f "looping over c,~%"))
    (display
     (format #f "then checking to see if rad(c)<c/2, "))
    (display
     (format #f "to reduce the~%"))
    (display
     (format #f "amount of work done.~%"))
    (display
     (format #f "This program still took around "))
    (display
     (format #f "5 minutes to run,~%"))
    (display
     (format #f "so it was re-written in c++, and "))
    (display
     (format #f "completed in about~%"))
    (display
     (format #f "1 second.  The algorithm is probably ok.~%"))
    (display
     (format #f "see https://projecteuler.net/problem=127~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((max-cc 1000)
          (debug-flag #t))
      (begin
        (sub-main-loop max-cc debug-flag)
        ))

    (newline)
    (let ((max-cc 120000)
          (debug-flag #f))
      (begin
        (sub-main-loop max-cc debug-flag)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 127 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

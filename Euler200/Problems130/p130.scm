#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 130                                    ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated July 30, 2022                                ###
;;;###                                                       ###
;;;###  updated March 11, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### prime-module for is-prime? function
(use-modules ((prime-module)
              :renamer (symbol-prefix-proc 'prime-module:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (brute-force-aa-calc nn)
  (begin
    (let ((min-kk -1)
          (kk 1)
          (rr-kk 1)
          (continue-loop-flag #t))
      (begin
        (while
         (equal? continue-loop-flag #t)
         (begin
           (if (zero? rr-kk)
               (begin
                 (set! min-kk kk)
                 (set! continue-loop-flag #f)
                 ))
           (set! rr-kk (modulo (1+ (* 10 rr-kk)) nn))
           (set! kk (1+ kk))
           ))

        min-kk
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-brute-force-aa-calc-1 result-hash-table)
 (begin
   (let ((sub-name "test-brute-force-aa-calc-1")
         (test-list
          (list
           (list 7 6) (list 41 5)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((nn (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result (brute-force-aa-calc nn)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : nn=~a, "
                        sub-name test-label-index nn))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop
         max-num max-composites max-prime
         status-num debug-flag)
  (begin
    (let ((sum-composites 0)
          (composites-count 0)
          (prime-array
           (prime-module:make-prime-array max-prime))
          (continue-loop-flag #t))
      (begin
        (do ((nn 9 (+ nn 2)))
            ((or (> nn max-num)
                 (equal? continue-loop-flag #f)))
          (begin
            (if (and (not (zero? (modulo nn 5)))
                     (equal?
                      (prime-module:is-array-prime? nn prime-array)
                      #f))
                (begin
                  (let ((aa-kk (brute-force-aa-calc nn))
                        (nn-minus-1 (1- nn)))
                    (begin
                      (if (zero? (modulo nn-minus-1 aa-kk))
                          (begin
                            (set! sum-composites (+ sum-composites nn))
                            (set! composites-count (1+ composites-count))
                            (if (>= composites-count max-composites)
                                (begin
                                  (set! continue-loop-flag #f)
                                  ))

                            (if (equal? debug-flag #t)
                                (begin
                                  (display
                                   (format
                                    #f "  nn=~a, nn-minus-1=~a, "
                                    nn nn-minus-1))
                                  (display
                                   (format
                                    #f "aa-kk=~a, modulo=~a, "
                                    aa-kk (modulo nn-minus-1 aa-kk)))
                                  (display
                                   (format
                                    #f "sum-composites=~a, count=~a~%"
                                    sum-composites composites-count))
                                  (force-output)
                                  ))
                            ))
                      ))
                  ))

            (if (zero? (modulo nn status-num))
                (begin
                  (display
                   (ice-9-format:format
                    #f "completed ~:d / ~:d : "
                    nn max-num))
                  (display
                   (format
                    #f "~a~%"
                    (timer-module:current-date-time-string)))
                  (force-output)
                  ))
            ))

        (if (equal? continue-loop-flag #f)
            (begin
              (display
               (ice-9-format:format
                #f "The sum of the first ~:d "
                max-composites))
              (display
               (ice-9-format:format
                #f "composite values of A(n) is ~:d.~%"
                sum-composites)))
            (begin
              (display
               (ice-9-format:format
                #f "Insufficient composites found in "))
              (display
               (ice-9-format:format
                #f "the range 9 <= n <= ~:d.~%"
                max-num))
              (display
               (ice-9-format:format
                #f "Number found = ~:d, sum so far = ~:d.~%"
                composites-count sum-composites))
              ))

        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "A number consisting entirely of "))
    (display
     (format #f "ones is called~%"))
    (display
     (format #f "a repunit. We shall define R(k) "))
    (display
     (format #f "to be a repunit~%"))
    (display
     (format #f "of length k; for example, "))
    (display
     (format #f "R(6) = 111111.~%"))
    (newline)
    (display
     (format #f "Given that n is a positive integer "))
    (display
     (format #f "and GCD(n, 10) = 1,~%"))
    (display
     (format #f "it can be shown that there always "))
    (display
     (format #f "exists a value, k,~%"))
    (display
     (format #f "for which R(k) is divisible by n, "))
    (display
     (format #f "and let A(n) be~%"))
    (display
     (format #f "the least such value of k; for "))
    (display
     (format #f "example,~%"))
    (display
     (format #f "A(7) = 6 and A(41) = 5.~%"))
    (newline)
    (display
     (format #f "You are given that for all primes, "))
    (display
     (format #f "p > 5, that~%"))
    (display
     (format #f "p - 1 is divisible by A(p). For "))
    (display
     (format #f "example, when~%"))
    (display
     (format #f "p = 41, A(41) = 5, and 40 is "))
    (display
     (format #f "divisible by 5.~%"))
    (newline)
    (display
     (format #f "However, there are rare composite "))
    (display
     (format #f "values for which~%"))
    (display
     (format #f "this is also true; the first five "))
    (display
     (format #f "examples being~%"))
    (display
     (format #f "91, 259, 451, 481, and 703.~%"))
    (newline)
    (display
     (format #f "Find the sum of the first "))
    (display
     (format #f "twenty-five composite~%"))
    (display
     (format #f "values of n for which GCD(n, 10) = 1 "))
    (display
     (format #f "and n - 1~%"))
    (display
     (format #f "is divisible by A(n).~%"))
    (newline)
    (display
     (format #f "For more on A(n) see~%"))
    (display
     (format #f "https://oeis.org/A099679~%"))
    (display
     (format #f "see https://projecteuler.net/problem=130~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((max-num 1000)
          (max-composites 5)
          (max-prime 1000)
          (status-num 10000)
          (debug-flag #t))
      (begin
        (sub-main-loop
         max-num max-composites max-prime
         status-num debug-flag)
        ))

    (newline)
    (let ((max-num 100000)
          (max-composites 25)
          (max-prime 100000)
          (status-num 10000)
          (debug-flag #f))
      (begin
        (sub-main-loop
         max-num max-composites max-prime
         status-num debug-flag)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 130 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (load "prime-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 126                                    ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated July 30, 2022                                ###
;;;###                                                       ###
;;;###  updated March 11, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (count-cubes-in-layer
         this-layer init-xx init-yy init-zz)
  (begin
    (let ((l1
           (* 2 (+ (* init-xx init-yy) (* init-xx init-zz)
                   (* init-yy init-zz))))
          (delta (* 4 (+ init-xx init-yy init-zz))))
      (begin
        (cond
         ((< this-layer 1)
          (begin
            #f
            ))
         ((= this-layer 1)
          (begin
            l1
            ))
         ((= this-layer 2)
          (begin
            (+ l1 delta)
            ))
         (else
          (begin
            (let ((ntmp (1- this-layer)))
              (let ((ntmp-2 (* 4 (1- ntmp) ntmp)))
                (let ((result
                       (+ l1 (* ntmp delta) ntmp-2)))
                  (begin
                    result
                    ))
                ))
            )))
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-count-cubes-in-layer-1 result-hash-table)
 (begin
   (let ((sub-name "test-count-cubes-in-layer-1")
         (test-list
          (list
           (list 1 3 2 1 22) (list 2 3 2 1 46)
           (list 3 3 2 1 78) (list 4 3 2 1 118)
           (list 1 5 1 1 22) (list 1 5 3 1 46)
           (list 1 7 2 1 46) (list 1 11 1 1 46)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((level-number (list-ref alist 0))
                  (init-xx (list-ref alist 1))
                  (init-yy (list-ref alist 2))
                  (init-zz (list-ref alist 3))
                  (shouldbe (list-ref alist 4)))
              (let ((result
                     (count-cubes-in-layer
                      level-number init-xx init-yy init-zz)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : level-number=~a, "
                        sub-name test-label-index level-number))
                      (err-2
                       (format
                        #f "init-xx=~a, init-yy=~a, init-zz=~a, "
                        init-xx init-yy init-zz))
                      (err-3
                       (format
                        #f "shouldbe=~a, result=~a~%"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append
                      err-1 err-2 err-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define-syntax update-hash-lists-macro
  (syntax-rules ()
    ((update-hash-lists-macro
      levels-htable loop-flag
      ll xx yy zz max-l1-limit)
     (begin
       (let ((lcount
              (count-cubes-in-layer ll xx yy zz)))
         (begin
           (if (<= lcount max-l1-limit)
               (begin
                 (let ((llist
                        (hash-ref levels-htable lcount (list))))
                   (begin
                     (hash-set!
                      levels-htable
                      lcount
                      (cons (list ll xx yy zz) llist))
                     )))
               (begin
                 (set! loop-flag #f)
                 ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (populate-level-count-list-hash!
         levels-htable max-l1-limit
         max-levels max-zz)
  (begin
    (hash-clear! levels-htable)

    (do ((zz 1 (1+ zz)))
        ((> zz max-zz))
      (begin
        (do ((yy zz (1+ yy)))
            ((> yy max-zz))
          (begin
            (let ((xx-loop-flag #t))
              (begin
                (do ((xx yy (1+ xx)))
                    ((or (> xx max-zz)
                         (equal? xx-loop-flag #f)))
                  (begin
                    (let ((ll-1 (+ (* xx (+ yy zz)) (* yy zz))))
                      (begin
                        (if (> ll-1 max-l1-limit)
                            (begin
                              (set! xx-loop-flag #f))
                            (begin
                              (let ((loop-flag #t))
                                (begin
                                  (do ((ll 1 (1+ ll)))
                                      ((or (> ll max-levels)
                                           (equal? loop-flag #f)))
                                    (begin
                                      (update-hash-lists-macro
                                       levels-htable loop-flag
                                       ll xx yy zz max-l1-limit)
                                      ))
                                  ))
                              ))
                        ))
                    ))
                ))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax update-hash-counts-macro
  (syntax-rules ()
    ((update-hash-counts-macro
      counts-htable loop-flag
      this-layer ll-count-1 delta max-l1-limit)
     (begin
       (let ((ntmp (1- this-layer)))
         (let ((ntmp-2 (* 4 (1- ntmp) ntmp)))
           (let ((layer-count
                  (+ ll-count-1 (* ntmp delta) ntmp-2)))
             (begin
               (if (<= layer-count max-l1-limit)
                   (begin
                     (let ((prev-count
                            (hash-ref counts-htable layer-count 0)))
                       (begin
                         (hash-set!
                          counts-htable layer-count (1+ prev-count))
                         )))
                   (begin
                     (set! loop-flag #f)
                     ))
               ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax process-xyz-inner-loop-macro
  (syntax-rules ()
    ((process-xyz-inner-loop-macro
      xx ytmp yz-tmp
      xx-loop-flag
      this-layer ll-count-1 delta max-l1-limit
      max-levels
      counts-htable)
     (begin
       (let ((ll-1 (* 2 (+ (* xx ytmp) yz-tmp)))
             (delta (* 4 (+ xx ytmp))))
         (begin
           (if (> ll-1 max-l1-limit)
               (begin
                 (set! xx-loop-flag #f))
               (begin
                 ;;; update layer-1 count
                 (let ((prev-count
                        (hash-ref counts-htable ll-1 0)))
                   (begin
                     (hash-set! counts-htable ll-1 (1+ prev-count))
                     ))
                  ;;; update layer-2 count
                 (let ((level-2 (+ ll-1 delta)))
                   (let ((prev-count
                          (hash-ref counts-htable level-2 0)))
                     (begin
                       (hash-set!
                        counts-htable level-2 (1+ prev-count))
                       )))

                 (let ((nn-loop-flag #t))
                   (begin
                     ;;; start from layer-3
                     (do ((ll 3 (1+ ll)))
                         ((or (> ll max-levels)
                              (equal? nn-loop-flag #f)))
                       (begin
                         (update-hash-counts-macro
                          counts-htable nn-loop-flag
                          ll ll-1 delta max-l1-limit)
                         ))
                     ))
                 ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (populate-level-count-hash!
         counts-htable max-l1-limit
         max-levels max-zz status-num)
  (begin
    (hash-clear! counts-htable)

    (do ((zz 1 (1+ zz)))
        ((> zz max-zz))
      (begin
        (if (zero? (modulo zz status-num))
            (begin
              (display
               (ice-9-format:format
                #f "~:d / ~:d : " zz max-zz))
              (display
               (format
                #f "~a~%"
                (timer-module:current-date-time-string)))
              (force-output)
              ))

        (do ((yy zz (1+ yy)))
            ((> yy max-zz))
          (begin
            (let ((xx-loop-flag #t)
                  (ytmp (+ yy zz))
                  (yz-tmp (* yy zz)))
              (begin
                (do ((xx yy (1+ xx)))
                    ((or (> xx max-zz)
                         (equal? xx-loop-flag #f)))
                  (begin
                    (process-xyz-inner-loop-macro
                     xx ytmp yz-tmp
                     xx-loop-flag
                     this-layer ll-count-1 delta max-l1-limit
                     max-levels
                     counts-htable)
                    ))
                ))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax process-lists-macro
  (syntax-rules ()
    ((process-lists-macro
      level-htable target-level-count
      max-levels max-zz debug-flag)
     (begin
       (let ((result-list-list #f)
             (min-nn -1))
         (begin
           (hash-for-each
            (lambda (key value)
              (begin
                (if (= (length value) target-level-count)
                    (begin
                      (if (or (< min-nn 0)
                              (< key min-nn))
                          (begin
                            (set! min-nn key)
                            (set! result-list-list (list key value))
                            ))
                      ))
                )) level-htable)

           (if (< min-nn 0)
               (begin
                 (display
                  (ice-9-format:format
                   #f "no results found for number of cubes=~:d~%"
                   target-level-count))
                 (display
                  (ice-9-format:format
                   #f "  with max-level=~:d, max-zz=~:d~%"
                   max-levels max-zz))
                 (force-output))
               (begin
                 (if (equal? debug-flag #t)
                     (begin
                       (let ((ii-count 0)
                             (lcount (car result-list-list))
                             (tail-list (cadr result-list-list)))
                         (begin
                           (for-each
                            (lambda (r-list)
                              (begin
                                (let ((r-level (list-ref r-list 0))
                                      (r-xx (list-ref r-list 1))
                                      (r-yy (list-ref r-list 2))
                                      (r-zz (list-ref r-list 3)))
                                  (begin
                                    (set! ii-count (1+ ii-count))
                                    (display
                                     (ice-9-format:format
                                      #f "  (~:d) layer=~:d : "
                                      ii-count r-level))
                                    (display
                                     (ice-9-format:format
                                      #f "cuboid=~:dx~:dx~:d~%"
                                      r-xx r-yy r-zz))
                                    (display
                                     (ice-9-format:format
                                      #f "  (number of cubes=~:d)~%"
                                      lcount))
                                    ))
                                )) tail-list)
                           (newline)

                           (display
                            (ice-9-format:format
                             #f "~:d is the least value of n "
                             min-nn))
                           (display
                            (ice-9-format:format
                             #f "such that C(n) = ~:d.~%"
                             target-level-count))
                           (force-output)
                           ))
                       ))
                 ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax process-counts-macro
  (syntax-rules ()
    ((process-counts-macro
      level-htable target-level-count
      max-levels max-zz debug-flag)
     (begin
       (let ((result-count 0)
             (min-nn -1)
             (key-list
              (sort
               (hash-map->list
                (lambda (key value)
                  (begin
                    key
                    )) level-htable)
               <)))
         (begin
           (for-each
            (lambda (key)
              (begin
                (let ((value (hash-ref level-htable key -1)))
                  (begin
                    (if (= value target-level-count)
                        (begin
                          (if (or (< min-nn 0)
                                  (< key min-nn))
                              (begin
                                (set! min-nn key)
                                (set! result-count value)
                                ))
                          (if (equal? debug-flag #t)
                              (begin
                                (display
                                 (format
                                  #f "  C(~:d) = ~:d~%" key value))
                                ))
                          ))
                    ))
                )) key-list)

           (if (< min-nn 0)
               (begin
                 (display
                  (ice-9-format:format
                   #f "no results found for number of cubes=~:d~%"
                   target-level-count))
                 (display
                  (ice-9-format:format
                   #f "  with max-level=~:d, max-zz=~:d~%"
                   max-levels max-zz))
                 (force-output))
               (begin
                 (display
                  (ice-9-format:format
                   #f "~:d is the least value of n "
                   min-nn))
                 (display
                  (ice-9-format:format
                   #f "such that C(n) = ~:d.~%"
                   target-level-count))
                 (force-output)
                 ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (reproduce-problem-statement)
  (begin
    (let ((target-level-count 10)
          (max-l1-limit 1000000)
          (max-levels 5)
          (max-zz 50))
      (let ((level-htable
             (make-hash-table)))
        (begin
          (populate-level-count-list-hash!
           level-htable max-l1-limit
           max-levels max-zz)

          (let ((nn-list (list 22 46 78 118)))
            (begin
              (for-each
               (lambda (nn)
                 (begin
                   (let ((result-list
                          (hash-ref level-htable nn #f)))
                     (let ((rlen (length result-list)))
                       (begin
                         (display
                          (ice-9-format:format
                           #f "C(~:d) = ~:d~%" nn rlen))
                         )))
                   )) nn-list)
              ))
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop
         target-level-count max-l1-limit
         max-levels max-zz status-num debug-flag)
  (begin
    (let ((level-htable (make-hash-table)))
      (begin
        (if (equal? debug-flag #t)
            (begin
              (populate-level-count-list-hash!
               level-htable max-l1-limit
               max-levels max-zz)

              (process-lists-macro
               level-htable target-level-count
               max-levels max-zz debug-flag))
            (begin
              (populate-level-count-hash!
               level-htable max-l1-limit
               max-levels max-zz status-num)

              (process-counts-macro
               level-htable target-level-count
               max-levels max-zz #t)
              ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "The minimum number of cubes to "))
    (display
     (format #f "cover every visible~%"))
    (display
     (format #f "face on a cuboid measuring 3 x 2 x 1 "))
    (display
     (format #f "is twenty-two.~%"))
    (newline)
    (display
     (format #f "If we then add a second layer "))
    (display
     (format #f "to this solid it~%"))
    (display
     (format #f "would require forty-six cubes to "))
    (display
     (format #f "cover every visible face,~%"))
    (display
     (format #f "the third layer would require "))
    (display
     (format #f "seventy-eight cubes, and~%"))
    (display
     (format #f "the fourth layer would require "))
    (display
     (format #f "one-hundred and eighteen~%"))
    (display
     (format #f "cubes to cover every "))
    (display
     (format #f "visible face.~%"))
    (newline)
    (display
     (format #f "However, the first layer on a "))
    (display
     (format #f "cuboid measuring 5 x 1 x 1~%"))
    (display
     (format #f "also requires twenty-two cubes; "))
    (display
     (format #f "similarly the first~%"))
    (display
     (format #f "layer on cuboids measuring "))
    (display
     (format #f "5 x 3 x 1, 7 x 2 x 1,~%"))
    (display
     (format #f "and 11 x 1 x 1 all contain "))
    (display
     (format #f "forty-six cubes.~%"))
    (newline)
    (display
     (format #f "We shall define C(n) to represent "))
    (display
     (format #f "the number of cuboids~%"))
    (display
     (format #f "that contain n cubes in one of its "))
    (display
     (format #f "layers. So C(22) = 2,~%"))
    (display
     (format #f "C(46) = 4, C(78) = 5, and C(118) = 8.~%"))
    (newline)
    (display
     (format #f "It turns out that 154 is the least "))
    (display
     (format #f "value of n for~%"))
    (display
     (format #f "which C(n) = 10.~%"))
    (newline)
    (display
     (format #f "Find the least value of n for "))
    (display
     (format #f "which C(n) = 1000.~%"))
    (newline)
    (display
     (format #f "The solution was found at~%"))
    (display
     (format #f "https://euler.stephan-brumme.com/126/~%"))
    (newline)
    (display
     (format #f "The for a 3x2x1 initial cuboid, "))
    (display
     (format #f "the number of cubes~%"))
    (display
     (format #f "needed for the first layer is~%"))
    (display
     (format #f "L1=2*(wx*wy+wx*wz+wy*wz)=2*(3*2+3*1+2*1)=22.~%"))
    (display
     (format #f "The number of cubes needed for the "))
    (display
     (format #f "second layer is~%"))
    (display
     (format #f "L2=2*(wx*wy+wx*wz+wy*wz)+4*(wx+wy+wz)~%"))
    (display
     (format #f "=L1+4*(3+2+1)=22+24=46.~%"))
    (display
     (format #f "The number of cubes needed for the "))
    (display
     (format #f "third layer is~%"))
    (display
     (format #f "L3=2*(wx*wy+wx*wz+wy*wz)+4*(wx+wy+wz)"))
    (display
     (format #f "+4*(wx+wy+wz)+8~%"))
    (display
     (format #f "= L1+2*delta+8=22+48+8=78, "))
    (display
     (format #f "(where delta=4*(wx+wy+wz)).~%"))
    (display
     (format #f "The number of cubes needed for "))
    (display
     (format #f "the fourth layer is~%"))
    (display
     (format #f "L4=L1+3*delta+1*8+2*8=22+72+24=118.~%"))
    (newline)
    (display
     (format #f "So the equation for calculating "))
    (display
     (format #f "the number of cubes~%"))
    (display
     (format #f "for the n-th level (n>2), is~%"))
    (display
     (format #f "Ln=L1+(n-1)*delta+((n-2)*(n-1)/2)*8~%"))
    (display
     (format #f "see https://projecteuler.net/problem=126~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (reproduce-problem-statement)


    (newline)
    (let ((target-level-count 10)
          (max-l1-limit 100000)
          (max-levels 10)
          (max-zz 50)
          (status-num 10000)
          (debug-flag #t))
      (begin
        (sub-main-loop
         target-level-count max-l1-limit
         max-levels max-zz status-num debug-flag)
        ))

    (newline)
    (let ((target-level-count 1000)
          (max-l1-limit 30000)
          (max-levels 1000)
          (max-zz 5000)
          (status-num 10000)
          (debug-flag #f))
      (begin
        (sub-main-loop
         target-level-count max-l1-limit
         max-levels max-zz status-num debug-flag)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 126 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

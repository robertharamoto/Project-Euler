#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 125                                    ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated July 30, 2022                                ###
;;;###                                                       ###
;;;###  updated March 11, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### digits-module for split-digits-list function
(use-modules ((digits-module)
              :renamer (symbol-prefix-proc 'digits-module:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (is-number-palindrome? anum)
  (begin
    (let ((dlist
           (digits-module:split-digits-list anum)))
      (let ((rlist (reverse dlist)))
        (begin
          (equal? dlist rlist)
          )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-is-number-palindrome-1 result-hash-table)
 (begin
   (let ((sub-name "test-is-number-palindrome-1")
         (test-list
          (list
           (list 11 #t) (list 12 #f) (list 999 #t)
           (list 12321 #t) (list 11223 #f)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((anum (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result (is-number-palindrome? anum)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : anum=~a, "
                        sub-name test-label-index anum))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a~%"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (list-to-square-string a-list)
  (begin
    (let ((astring
           (string-join
            (map
             (lambda (a-num)
               (begin
                 (ice-9-format:format #f "~:d^2" a-num)
                 )) a-list)
            " + ")))
      (begin
        astring
        ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax process-palindrome-macro
  (syntax-rules ()
    ((process-palindrome-macro
      jj-sum rlist square-count square-sum
      debug-flag seen-htable)
     (begin
       (let ((sflag (hash-ref seen-htable jj-sum #f)))
         (begin
           (if (equal? sflag #f)
               (begin
                 (hash-set! seen-htable jj-sum #t)
                 (set! square-sum (+ square-sum jj-sum))
                 (set! square-count (1+ square-count))
                 (if (equal? debug-flag #t)
                     (begin
                       (let ((slist (sort rlist <)))
                         (let ((s-string
                                (list-to-square-string slist)))
                           (begin
                             (display
                              (ice-9-format:format
                               #f "  (~:d) ~:d = ~a, "
                               square-count jj-sum s-string))
                             (display
                              (ice-9-format:format
                               #f "sum so far = ~:d~%"
                               square-sum))
                             (force-output)
                             )))
                       ))
                 ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop max-sum debug-flag)
  (begin
    (let ((square-sum 0)
          (square-count 0)
          (seen-htable (make-hash-table))
          (max-ii
           (1+ (exact-integer-sqrt max-sum))))
      (begin
        (do ((ii 1 (1+ ii)))
            ((> ii max-ii))
          (begin
            (let ((rlist (list))
                  (continue-inner-loop-flag #t)
                  (jj-sum 0))
              (begin
                (do ((jj ii (1+ jj)))
                    ((or (> jj max-ii)
                         (equal? continue-inner-loop-flag #f)))
                  (begin
                    (let ((jj-sqr (* jj jj)))
                      (begin
                        (set! rlist (cons jj rlist))
                        (set! jj-sum (+ jj-sum jj-sqr))
                        (if (<= jj-sum max-sum)
                            (begin
                              (if (and (> (length rlist) 1)
                                       (is-number-palindrome? jj-sum))
                                  (begin
                                    (process-palindrome-macro
                                     jj-sum rlist square-count square-sum
                                     debug-flag seen-htable)
                                    )))
                            (begin
                              (set! continue-inner-loop-flag #f)
                              ))
                        ))
                    ))
                ))
            ))

        (display
         (ice-9-format:format
          #f "the sum of palindromes is ~:d.~%" square-sum))
        (display
         (ice-9-format:format
          #f "  found exactly ~:d palindromes that "
          square-count))
        (display
         (ice-9-format:format
          #f "are less than ~:d.~%"
          max-sum))
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "The palindromic number 595 is "))
    (display
     (format #f "interesting because~%"))
    (display
     (format #f "it can be written as the sum of "))
    (display
     (format #f "consecutive squares:~%"))
    (display
     (format #f "6^2 + 7^2 + 8^2 + 9^2 + 10^2 "))
    (display
     (format #f "+ 11^2 + 12^2.~%"))
    (newline)
    (display
     (format #f "There are exactly eleven palindromes "))
    (display
     (format #f "below one-thousand~%"))
    (display
     (format #f "that can be written as consecutive "))
    (display
     (format #f "square sums, and~%"))
    (display
     (format #f "the sum of these palindromes is 4164. "))
    (display
     (format #f "Note that~%"))
    (display
     (format #f "1 = 0^2 + 1^2 has not been included "))
    (display
     (format #f "as this problem is~%"))
    (display
     (format #f "concerned with the squares of "))
    (display
     (format #f "positive integers.~%"))
    (newline)
    (display
     (format #f "Find the sum of all the numbers less "))
    (display
     (format #f "than 10^8 that are~%"))
    (display
     (format #f "both palindromic and can be written "))
    (display
     (format #f "as the sum~%"))
    (display
     (format #f "of consecutive squares.~%"))
    (display
     (format #f "see https://projecteuler.net/problem=125~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((max-sum 1000)
          (debug-flag #t))
      (begin
        (sub-main-loop max-sum debug-flag)
        ))

    (newline)
    (let ((max-sum 100000000)
          (debug-flag #f))
      (begin
        (sub-main-loop max-sum debug-flag)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 125 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (load "digits-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

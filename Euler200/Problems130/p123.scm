#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 123                                    ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated July 30, 2022                                ###
;;;###                                                       ###
;;;###  updated March 11, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### prime-module for is-prime? function
(use-modules ((prime-module)
              :renamer (symbol-prefix-proc 'prime-module:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
;;; only need to consider odd nn, then (pn-1)^n+(pn+1)^n = 2npn mod pn^2
(define (sub-main-loop target-remainder max-prime)
  (begin
    (let ((previous-nn -1)
          (previous-remainder -1)
          (min-nn -1)
          (min-remainder -1)
          (continue-loop-flag #t)
          (prime-array
           (prime-module:make-prime-array max-prime)))
      (let ((array-length
             (car (array-dimensions prime-array))))
        (begin
          (do ((nn 1 (+ nn 2)))
              ((or (>= nn array-length)
                   (equal? continue-loop-flag #f)))
            (begin
              (set! previous-nn min-nn)
              (set! previous-remainder min-remainder)

              (let ((pindex (1- nn)))
                (let ((pn (array-ref prime-array pindex)))
                  (let ((this-remainder (* 2 nn pn)))
                    (begin
                      (set! min-nn nn)
                      (set! min-remainder this-remainder)

                      (if (> this-remainder target-remainder)
                          (begin
                            (set! continue-loop-flag #f)
                            ))
                      ))
                  ))
              ))

          (display
           (ice-9-format:format
            #f "~:d = the least value of n for which the remainder "
            min-nn))
          (display
           (ice-9-format:format
            #f "first exceeds ~:d, with a remainder of ~:d.~%"
            target-remainder min-remainder))
          (display
           (ice-9-format:format
            #f "the previous value of n is ~:d "
            previous-nn))
          (display
           (ice-9-format:format
            #f "(with a remainder of ~:d).~%"
            previous-remainder))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "Let pn be the nth prime: "))
    (display
     (format #f "2, 3, 5, 7, 11, ...,~%"))
    (display
     (format #f "and let r be the remainder when "))
    (display
     (format #f "(pn-1)^n + (pn+1)^n~%"))
    (display
     (format #f "is divided by pn^2.~%"))
    (newline)
    (display
     (format #f "For example, when n = 3, p3 = 5, "))
    (display
     (format #f "and 43 + 63~%"))
    (display
     (format #f "= 280 == 5 mod 25.~%"))
    (newline)
    (display
     (format #f "The least value of n for which the "))
    (display
     (format #f "remainder first~%"))
    (display
     (format #f "exceeds 10^9 is 7037.~%"))
    (newline)
    (display
     (format #f "Find the least value of n for which "))
    (display
     (format #f "the remainder first~%"))
    (display
     (format #f "exceeds 10^10.~%"))
    (newline)
    (display
     (format #f "This algorithm is similar to problem "))
    (display
     (format #f "120, and uses~%"))
    (display
     (format #f "the ideas found in~%"))
    (display
     (format #f "https://blog.dreamshire.com/project-euler-123-solution/~%"))
    (newline)
    (display
     (format #f "Using the binomial theorem,~%"))
    (display
     (format #f "https://en.wikipedia.org/wiki/Binomial_theorem~%"))
    (display
     (format #f "one can rewrite (a-1)^n + (a+1)^n="))
    (display
     (format #f "Sum(Choose(n,k)~%"))
    (display
     (format #f "* a^k * ((-1)^(n-k) + 1)).~%"))
    (newline)
    (display
     (format #f "From the article above there are "))
    (display
     (format #f "three cases to examine:~%"))
    (display
     (format #f "n=1, n even, n odd.~%"))
    (display
     (format #f "When n=1, (a-1)+(a+1)=2a.~%"))
    (display
     (format #f "When n is even, then (n-k) is "))
    (display
     (format #f "odd when k is~%"))
    (display
     (format #f "odd, so only the even powers a^k "))
    (display
     (format #f "remain (-1+1 = 0),~%"))
    (display
     (format #f "then a^2t = 0 mod a^2.  (a-1)^n + "))
    (display
     (format #f "(a+1)^n = 2 mod a^2.~%"))
    (display
     (format #f "When n is odd, then (n-k) is even "))
    (display
     (format #f "when k is odd,~%"))
    (display
     (format #f "so only the odd powers of a^k remain. "))
    (display
     (format #f "Every term in~%"))
    (display
     (format #f "the binomial expansion contains a "))
    (display
     (format #f "power of a^2~%"))
    (display
     (format #f "(each a^k is equal to a^2 times a "))
    (display
     (format #f "constant), except for~%"))
    (display
     (format #f "the k=1 term, so (a-1)^n + "))
    (display
     (format #f "(a+1)^n = 2na mod a^2.~%"))
    (display
     (format #f "see https://projecteuler.net/problem=123~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((target-remainder 1000000000)
          (max-prime 500000))
      (begin
        (sub-main-loop target-remainder max-prime)
        ))

    (newline)
    (let ((target-remainder 10000000000)
          (max-prime 1000000))
      (begin
        (sub-main-loop target-remainder max-prime)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 123 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (load "prime-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)
          (force-output)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 129                                    ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated July 30, 2022                                ###
;;;###                                                       ###
;;;###  updated March 11, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (brute-force-aa-calc nn)
  (begin
    (cond
     ((<= nn 0)
      (begin
        -1
        ))
     ((even? nn)
      (begin
        -1
        ))
     ((zero? (modulo nn 5))
      (begin
        -1
        ))
     ((= nn 1)
      (begin
        1
        ))
     (else
      (begin
        (let ((min-kk -1)
              (kk 1)
              (rr-kk 1)
              (continue-loop-flag #t))
          (begin
            (while
             (equal? continue-loop-flag #t)
             (begin
               (if (zero? rr-kk)
                   (begin
                     (set! min-kk kk)
                     (set! continue-loop-flag #f)
                     ))
               (set! rr-kk (modulo (1+ (* 10 rr-kk)) nn))
               (set! kk (1+ kk))
               ))

            min-kk
            ))
        )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-brute-force-aa-calc-1 result-hash-table)
 (begin
   (let ((sub-name "test-brute-force-aa-calc-1")
         (test-list
          (list
           (list 7 6) (list 41 5)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (this-list)
          (begin
            (let ((nn (list-ref this-list 0))
                  (shouldbe (list-ref this-list 1)))
              (let ((result (brute-force-aa-calc nn)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : nn=~a, "
                        sub-name test-label-index nn))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define-syntax brute-force-check-macro
  (syntax-rules ()
    ((brute-force-check-macro
      nn min-target continue-loop-flag min-nn min-aa-kk)
     (begin
       (let ((aa-kk (brute-force-aa-calc nn)))
         (begin
           (if (> aa-kk min-target)
               (begin
                 (set! min-aa-kk aa-kk)
                 (set! min-nn nn)
                 (set! continue-loop-flag #f)
                 ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop
         min-target max-num max-num-factors
         max-prime status-num)
  (begin
    (let ((min-nn -1)
          (min-aa-kk -1)
          (counter 0)
          (start-num min-target)
          (continue-loop-flag #t))
      (begin
        (do ((ii start-num (1+ ii)))
            ((or (>= ii max-num)
                 (equal? continue-loop-flag #f)))
          (begin
            (if (= (gcd ii 10) 1)
                (begin
                  (brute-force-check-macro
                   ii min-target
                   continue-loop-flag min-nn min-aa-kk)
                  ))

            (set! counter (1+ counter))
            (if (zero? (modulo counter status-num))
                (begin
                  (display
                   (ice-9-format:format
                    #f "completed ~:d in (~:d - ~:d) : "
                    ii start-num max-num))
                  (display
                   (ice-9-format:format
                    #f "so far A(~:d) = ~:d~%"
                    ii start-num max-num min-nn min-aa-kk))
                  (display
                   (format
                    #f "  ~a~%"
                    (timer-module:current-date-time-string)))
                  (force-output)
                  ))
            ))

        (if (> min-nn 0)
            (begin
              (display
               (ice-9-format:format
                #f "The least value of n for which A(n) "))
              (display
               (ice-9-format:format
                #f "first exceeds ~:d is ~:d, A(~:d) = ~:d~%"
                min-target min-nn min-nn min-aa-kk)))
            (begin
              (display
               (ice-9-format:format
                #f "Least value n for which A(n) exceeds "))
              (display
               (ice-9-format:format
                #f "~:d not found (for nn <= ~:d)~%"
                min-target max-num))
              ))

        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "A number consisting entirely of "))
    (display
     (format #f "ones is called a~%"))
    (display
     (format #f "repunit. We shall define R(k) to "))
    (display
     (format #f "be a repunit of~%"))
    (display
     (format #f "length k; for example, "))
    (display
     (format #f "R(6) = 111111.~%"))
    (newline)
    (display
     (format #f "Given that n is a positive integer "))
    (display
     (format #f "and GCD(n, 10) = 1,~%"))
    (display
     (format #f "it can be shown that there always "))
    (display
     (format #f "exists a value,~%"))
    (display
     (format #f "k, for which R(k) is divisible by "))
    (display
     (format #f "n, and let A(n)~%"))
    (display
     (format #f "be the least such value of k; for "))
    (display
     (format #f "example, A(7) = 6~%"))
    (display
     (format #f "and A(41) = 5.~%"))
    (newline)
    (display
     (format #f "The least value of n for which "))
    (display
     (format #f "A(n) first exceeds~%"))
    (display
     (format #f "ten is 17.~%"))
    (newline)
    (display
     (format #f "Find the least value of n for "))
    (display
     (format #f "which A(n) first~%"))
    (display
     (format #f "exceeds one-million.~%"))
    (newline)
    (display
     (format #f "For more information on A(n) "))
    (display
     (format #f "see the Online~%"))
    (display
     (format #f "Encyclopedia of Integer Sequences~%"))
    (display
     (format #f "https://oeis.org/A099679~%"))
    (display
     (format #f "see https://projecteuler.net/problem=129~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((min-target 10)
          (max-num 100)
          (max-num-factors 10)
          (max-prime 100)
          (status-num 1000))
      (begin
        (sub-main-loop
         min-target max-num max-num-factors max-prime
         status-num)
        ))

    (newline)
    (let ((min-target 1000000)
          (max-num 2000000)
          (max-num-factors 20)
          (max-prime 100000)
          (status-num 200000))
      (begin
        (sub-main-loop
         min-target max-num max-num-factors max-prime
         status-num)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 129 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

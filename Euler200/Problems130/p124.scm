#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 124                                    ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated July 30, 2022                                ###
;;;###                                                       ###
;;;###  updated March 11, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (dynamic-radical-array nn)
  (begin
    (let ((radical-array (make-array 1 (1+ nn)))
          (prime-array (make-array 0 (1+ nn))))
      (begin
        (do ((ii 1 (1+ ii)))
            ((> ii nn))
          (begin
            (array-set! prime-array ii ii)
            ))

        (do ((ii 2 (1+ ii)))
            ((> ii nn))
          (begin
            (let ((this-num (array-ref prime-array ii)))
              (begin
                (if (= this-num ii)
                    (begin
                      (array-set! radical-array ii ii)

                      (do ((jj (+ ii ii) (+ jj ii)))
                          ((> jj nn))
                        (begin
                          (array-set! prime-array -1 jj)

                          (let ((jj-num (array-ref radical-array jj)))
                            (let ((next-num (* ii jj-num)))
                              (begin
                                (array-set! radical-array next-num jj)
                                )))
                          ))
                      ))
                ))
            ))

        radical-array
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-dynamic-radical-array-1 result-hash-table)
 (begin
   (let ((sub-name "test-dynamic-radical-array-1")
         (test-list
          (list
           (list 2 2) (list 3 3) (list 4 2)
           (list 5 5) (list 6 6) (list 7 7)
           (list 8 2) (list 9 3) (list 10 10)
           (list 11 11) (list 12 6) (list 13 13)
           (list 14 14) (list 15 15) (list 16 2)
           (list 17 17) (list 18 6) (list 19 19)
           (list 20 10) (list 21 21) (list 22 22)
           (list 32 2) (list 64 2) (list 128 2)
           (list 27 3) (list 81 3) (list 243 3)
           (list 25 5) (list 125 5) (list 625 5)
           (list 63 21) (list 441 21) (list 1323 21)
           (list 147 21)
           (list 100 10) (list 504 42)
           ))
         (result-array
          (dynamic-radical-array 2000))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-index (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result
                     (array-ref result-array test-index)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : test-index=~a, "
                        sub-name test-label-index test-index))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop max-index target-index debug-flag)
  (begin
    (let ((radical-array (dynamic-radical-array max-index))
          (result-list-list (list)))
      (begin
        (if (< max-index target-index)
            (begin
              (display
               (ice-9-format:format
                #f "main-loop error must have max-index = ~:d "
                max-index))
              (display
               (ice-9-format:format
                #f "greater than target-index = ~:d.~%"
                target-index))
              (display
               (format #f "quitting...~%"))
              (quit)
              ))

        (do ((nn 1 (1+ nn)))
            ((> nn max-index))
          (begin
            (let ((rad (array-ref radical-array nn)))
              (let ((a-list (list nn rad)))
                (begin
                  (set!
                   result-list-list
                   (cons a-list result-list-list))
                  )))
            ))

        (let ((sorted-list-list
               (sort
                result-list-list
                (lambda (a b)
                  (begin
                    (let ((a-r (list-ref a 1))
                          (b-r (list-ref b 1)))
                      (begin
                        (if (= a-r b-r)
                            (begin
                              (< (list-ref a 0) (list-ref b 0)))
                            (begin
                              (< a-r b-r)
                              ))
                        ))
                    ))
                )))
          (begin
            (if (equal? debug-flag #t)
                (begin
                  (display (format #f "    Sorted~%"))
                  (display (format #f "  n    rad(n)    k~%"))
                  (let ((slen (length sorted-list-list)))
                    (begin
                      (do ((kk 1 (1+ kk)))
                          ((> kk slen))
                        (begin
                          (let ((s-elem
                                 (list-ref sorted-list-list (1- kk))))
                            (let ((s-n (list-ref s-elem 0))
                                  (s-r (list-ref s-elem 1)))
                              (begin
                                (display
                                 (ice-9-format:format
                                  #f "  ~:d      ~:d      ~:d~%"
                                  s-n s-r kk))
                                )))
                          ))
                      (force-output)
                      ))
                  ))

            (let ((e-elem
                   (list-ref
                    sorted-list-list (1- target-index))))
              (let ((e-n (list-ref e-elem 0)))
                (begin
                  (display
                   (ice-9-format:format
                    #f "E(~:d) = ~:d (where 1 <= n <= ~:d).~%"
                    target-index e-n max-index))
                  (force-output)
                  )))
            ))
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "The radical of n, rad(n), is the "))
    (display
     (format #f "product of distinct~%"))
    (display
     (format #f "prime factors of n. For example, "))
    (display
     (format #f "504 = 2^3 x 3^2 x 7,~%"))
    (display
     (format #f "so rad(504) = 2 x 3 x 7 = 42.~%"))
    (newline)
    (display
     (format #f "If we calculate rad(n) for "))
    (display
     (format #f "1 <= n <= 10, then~%"))
    (display
     (format #f "sort them on rad(n), and sorting "))
    (display
     (format #f "on n if the~%"))
    (display
     (format #f "radical values are equal, we get:~%"))
    (newline)
    (display (format #f "  Unsorted     Sorted~%"))
    (display (format #f "  n  rad(n)    n  rad(n)  k~%"))
    (display (format #f "  1  1         1  1       1~%"))
    (display (format #f "  2  2         2  2       2~%"))
    (display (format #f "  3  3         4  2       3~%"))
    (display (format #f "  4  2         8  2       4~%"))
    (display (format #f "  5  5         3  3       5~%"))
    (display (format #f "  6  6         9  3       6~%"))
    (display (format #f "  7  7         5  5       7~%"))
    (display (format #f "  8  2         6  6       8~%"))
    (display (format #f "  9  3         7  7       9~%"))
    (display (format #f "  10 10        10 10     10~%"))
    (newline)
    (display
     (format #f "Let E(k) be the kth element in the "))
    (display
     (format #f "sorted n column;~%"))
    (display
     (format #f "for example, E(4) = 8 and E(6) = 9.~%"))
    (newline)
    (display
     (format #f "If rad(n) is sorted for "))
    (display
     (format #f "1 <= n <= 100000,~%"))
    (display
     (format #f "find E(10000).~%"))
    (newline)
    (display
     (format #f "This algorithm uses a sieve of "))
    (display
     (format #f "Eratosthenes to~%"))
    (display
     (format #f "compute the radical of each number.~%"))
    (display
     (format #f "see https://projecteuler.net/problem=124~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((max-index 10)
          (target-index 6)
          (debug-flag #t))
      (begin
        (sub-main-loop max-index target-index debug-flag)
        ))

    (newline)
    (let ((max-index 10)
          (target-index-list (list 4 6))
          (debug-flag #f))
      (begin
        (for-each
         (lambda (tindex)
           (begin
             (sub-main-loop max-index tindex debug-flag)
             )) target-index-list)
        ))

    (force-output)

    (let ((max-index 100000)
          (target-index 10000)
          (debug-flag #f))
      (begin
        (sub-main-loop max-index target-index debug-flag)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 124 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 122                                    ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated July 30, 2022                                ###
;;;###                                                       ###
;;;###  updated March 11, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (is-num-in-list? a-num a-list-list)
  (begin
    (let ((in-list-flag #f)
          (a-len (length a-list-list)))
      (begin
        (do ((ii 0 (1+ ii)))
            ((or (>= ii a-len)
                 (equal? in-list-flag #t)))
          (begin
            (let ((b-list
                   (list-ref a-list-list ii)))
              (begin
                (if (not (equal? (member a-num b-list) #f))
                    (begin
                      (set! in-list-flag #t)
                      ))
                ))
            ))

        in-list-flag
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-is-num-in-list-1 result-hash-table)
 (begin
   (let ((sub-name "test-is-num-in-list-1")
         (test-list
          (list
           (list 2 (list (list 1 1 2)) #t)
           (list 3 (list (list 1 1 2)) #f)
           (list 3 (list (list 2 1 3) (list 1 1 2)) #t)
           (list 5 (list (list 2 1 3) (list 1 1 2)) #f)
           (list 4 (list (list 2 2 4) (list 2 1 3) (list 1 1 2)) #t)
           (list 5 (list (list 2 2 4) (list 2 1 3) (list 1 1 2)) #f)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((a-num (list-ref alist 0))
                  (llist (list-ref alist 1))
                  (shouldbe (list-ref alist 2)))
              (let ((result (is-num-in-list? a-num llist)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : "
                        sub-name test-label-index))
                      (err-2
                       (format
                        #f "a-num=~a, llist=~a, "
                        a-num llist))
                      (err-3
                       (format
                        #f "shouldbe=~a, result=~a"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append
                      err-1 err-2 err-3)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define-syntax process-new-result-list-macro
  (syntax-rules ()
    ((process-new-result-list-macro
      mm-2 tlist result-list result-len
      this-list)
     (begin
       (for-each
        (lambda (a-list)
          (begin
            (if (is-num-in-list? mm-2 a-list)
                (begin
                  (let ((next-list
                         (cons tlist a-list)))
                    (begin
                      (if (equal?
                           (member next-list result-list) #f)
                          (begin
                            (set!
                             result-list
                             (cons next-list result-list))
                            (set!
                             result-len (length next-list))
                            ))
                      ))
                  ))
            )) this-list)
       ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax update-nn-list-macro
  (syntax-rules ()
    ((update-nn-list-macro
      nn-list nn-len
      result-list result-len)
     (begin
       (if (or (< nn-len 0)
               (and (> result-len 0)
                    (< result-len nn-len)))
           (begin
             (set! nn-list result-list)
             (set! nn-len result-len))
           (begin
             (if (= result-len nn-len)
                 (begin
                   (set!
                    nn-list (append nn-list result-list))
                   ))
             ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define (dynamic-addition-chains max-num)
  (begin
    (let ((sum-array
           (make-array (list) (1+ max-num))))
      (begin
        (array-set!
         sum-array (list (list (list 1 1 2))) 2)
        (array-set!
         sum-array (list (list (list 2 1 3) (list 1 1 2))) 3)
        (array-set!
         sum-array (list (list (list 2 2 4) (list 1 1 2))) 4)

        (do ((nn 5 (1+ nn)))
            ((> nn max-num))
          (begin
            (let ((end-mm (euclidean/ nn 2))
                  (nn-list (list))
                  (nn-len -1))
              (begin
                (do ((mm-1 (1- nn) (1- mm-1)))
                    ((< mm-1 end-mm))
                  (begin
                    (let ((mm-2 (- nn mm-1)))
                      (begin
                        (if (and (>= mm-1 mm-2)
                                 (>= mm-1 2))
                            (begin
                              (let ((this-list
                                     (array-ref sum-array mm-1))
                                    (result-list (list))
                                    (result-len -1)
                                    (tlist (list mm-1 mm-2 nn)))
                                (begin
                                  (process-new-result-list-macro
                                   mm-2 tlist result-list result-len
                                   this-list)

                                  (update-nn-list-macro
                                   nn-list nn-len
                                   result-list result-len)
                                  ))
                              ))
                        ))
                    ))

                (array-set! sum-array nn-list nn)
                ))
            ))

        sum-array
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-dynamic-addition-chains-1 result-hash-table)
 (begin
   (let ((sub-name "test-dynamic-addition-chains-1")
         (test-list
          (list
           (list 2 (list (list 1 1 2)))
           (list 3 (list (list 2 1 3) (list 1 1 2)))
           (list 4 (list (list 2 2 4) (list 1 1 2)))
           (list 5 (list (list 3 2 5) (list 2 1 3)
                         (list 1 1 2)))
           (list 5 (list (list 4 1 5) (list 2 2 4)
                         (list 1 1 2)))
           (list 6 (list (list 3 3 6) (list 2 1 3)
                         (list 1 1 2)))
           (list 6 (list (list 4 2 6) (list 2 2 4)
                         (list 1 1 2)))
           (list 7 (list (list 5 2 7) (list 3 2 5)
                         (list 2 1 3)
                         (list 1 1 2)))
           (list 7 (list (list 5 2 7) (list 4 1 5)
                         (list 2 2 4)
                         (list 1 1 2)))
           (list 7 (list (list 6 1 7) (list 3 3 6)
                         (list 2 1 3)
                         (list 1 1 2)))
           (list 7 (list (list 6 1 7) (list 4 2 6)
                         (list 2 2 4)
                         (list 1 1 2)))
           (list 8 (list (list 4 4 8) (list 2 2 4)
                         (list 1 1 2)))
           (list 9 (list (list 5 4 9) (list 4 1 5)
                         (list 2 2 4)
                         (list 1 1 2)))
           (list 9 (list (list 6 3 9) (list 3 3 6)
                         (list 2 1 3)
                         (list 1 1 2)))
           (list 9 (list (list 8 1 9) (list 4 4 8)
                         (list 2 2 4)
                         (list 1 1 2)))
           (list 10 (list (list 5 5 10) (list 3 2 5)
                          (list 2 1 3)
                          (list 1 1 2)))
           (list 10 (list (list 5 5 10) (list 4 1 5)
                          (list 2 2 4)
                          (list 1 1 2)))
           (list 10 (list (list 6 4 10) (list 4 2 6)
                          (list 2 2 4)
                          (list 1 1 2)))
           (list 10 (list (list 8 2 10) (list 4 4 8)
                          (list 2 2 4)
                          (list 1 1 2)))
           (list 11 (list (list 7 4 11) (list 6 1 7)
                          (list 4 2 6)
                          (list 2 2 4) (list 1 1 2)))
           (list 11 (list (list 7 4 11) (list 5 2 7)
                          (list 4 1 5)
                          (list 2 2 4) (list 1 1 2)))
           (list 11 (list (list 10 1 11) (list 5 5 10)
                          (list 3 2 5)
                          (list 2 1 3) (list 1 1 2)))
           (list 11 (list (list 10 1 11) (list 5 5 10)
                          (list 4 1 5)
                          (list 2 2 4) (list 1 1 2)))
           (list 11 (list (list 10 1 11) (list 6 4 10)
                          (list 4 2 6)
                          (list 2 2 4) (list 1 1 2)))
           (list 11 (list (list 10 1 11) (list 8 2 10)
                          (list 4 4 8)
                          (list 2 2 4) (list 1 1 2)))
           (list 12 (list (list 6 6 12) (list 3 3 6)
                          (list 2 1 3) (list 1 1 2)))
           (list 12 (list (list 6 6 12) (list 4 2 6)
                          (list 2 2 4) (list 1 1 2)))
           (list 12 (list (list 8 4 12) (list 4 4 8)
                          (list 2 2 4) (list 1 1 2)))
           (list 13 (list (list 12 1 13) (list 6 6 12)
                          (list 3 3 6) (list 2 1 3)
                          (list 1 1 2)))
           (list 13 (list (list 12 1 13) (list 6 6 12)
                          (list 4 2 6) (list 2 2 4)
                          (list 1 1 2)))
           (list 13 (list (list 10 3 13) (list 5 5 10)
                          (list 3 2 5) (list 2 1 3)
                          (list 1 1 2)))
           (list 14 (list (list 12 2 14) (list 6 6 12)
                          (list 3 3 6) (list 2 1 3)
                          (list 1 1 2)))
           (list 14 (list (list 12 2 14) (list 6 6 12)
                          (list 4 2 6) (list 2 2 4)
                          (list 1 1 2)))
           (list 14 (list (list 7 7 14) (list 5 2 7)
                          (list 3 2 5) (list 2 1 3)
                          (list 1 1 2)))
           (list 14 (list (list 7 7 14) (list 6 1 7)
                          (list 3 3 6) (list 2 1 3)
                          (list 1 1 2)))
           (list 15 (list (list 10 5 15) (list 5 5 10)
                          (list 3 2 5)
                          (list 2 1 3) (list 1 1 2)))
           (list 15 (list (list 10 5 15) (list 5 5 10)
                          (list 4 1 5)
                          (list 2 2 4) (list 1 1 2)))
           (list 15 (list (list 12 3 15) (list 6 6 12)
                          (list 3 3 6)
                          (list 2 1 3) (list 1 1 2)))
           (list 16 (list (list 8 8 16) (list 4 4 8)
                          (list 2 2 4) (list 1 1 2)))
           (list 17 (list (list 9 8 17) (list 8 1 9)
                          (list 4 4 8) (list 2 2 4)
                          (list 1 1 2)))
           (list 17 (list (list 16 1 17) (list 8 8 16)
                          (list 4 4 8) (list 2 2 4)
                          (list 1 1 2)))
           (list 18 (list (list 9 9 18) (list 5 4 9)
                          (list 4 1 5)
                          (list 2 2 4) (list 1 1 2)))
           (list 18 (list (list 12 6 18) (list 6 6 12)
                          (list 3 3 6)
                          (list 2 1 3) (list 1 1 2)))
           (list 18 (list (list 16 2 18) (list 8 8 16)
                          (list 4 4 8)
                          (list 2 2 4) (list 1 1 2)))
           (list 19 (list (list 11 8 19) (list 9 2 11)
                          (list 8 1 9)
                          (list 4 4 8) (list 2 2 4)
                          (list 1 1 2)))
           (list 19 (list (list 13 6 19) (list 7 6 13)
                          (list 6 1 7)
                          (list 3 3 6) (list 2 1 3)
                          (list 1 1 2)))
           (list 19 (list (list 18 1 19) (list 9 9 18)
                          (list 8 1 9)
                          (list 4 4 8) (list 2 2 4)
                          (list 1 1 2)))
           (list 20 (list (list 10 10 20) (list 5 5 10)
                          (list 3 2 5)
                          (list 2 1 3) (list 1 1 2)))
           (list 20 (list (list 10 10 20) (list 6 4 10)
                          (list 4 2 6)
                          (list 2 2 4) (list 1 1 2)))
           (list 20 (list (list 12 8 20) (list 8 4 12)
                          (list 4 4 8)
                          (list 2 2 4) (list 1 1 2)))
           ))
         (max-nn 20)
         (test-label-index 0))
     (begin
       (let ((result-array
              (dynamic-addition-chains max-nn)))
         (begin
           (for-each
            (lambda (alist)
              (begin
                (let ((nn (list-ref alist 0))
                      (shouldbe-list-list (list-ref alist 1)))
                  (let ((result-list-list
                         (array-ref result-array nn)))
                    (let ((slen (length shouldbe-list-list))
                          (rlen (length (car result-list-list)))
                          (rr-list-len (length result-list-list)))
                      (let ((err-1
                             (format
                              #f "~a : error (~a) : nn=~a, "
                              sub-name test-label-index nn))
                            (err-2
                             (format
                              #f "shouldbe length=~a, result length=~a"
                              slen rlen)))
                        (begin
                          (unittest2:assert?
                           (equal? slen rlen)
                           sub-name
                           (string-append err-1 err-2)
                           result-hash-table)

                          (for-each
                           (lambda (s-list)
                             (begin
                               (let ((err-3
                                      (format
                                       #f "shouldbe=~a, result=~a"
                                       s-list result-list-list))
                                     (s-list-found-flag #f))
                                 (begin
                                   (do ((ii 0 (1+ ii)))
                                       ((or (> ii rr-list-len)
                                            (equal? s-list-found-flag #t)))
                                     (begin
                                       (let ((rr-list
                                              (list-ref result-list-list ii)))
                                         (begin
                                           (if (not
                                                (equal?
                                                 (member s-list rr-list)
                                                 #f))
                                               (begin
                                                 (set! s-list-found-flag #t)
                                                 ))
                                           ))
                                       ))
                                   (unittest2:assert?
                                    (equal? s-list-found-flag #t)
                                    sub-name
                                    (string-append err-1 err-3)
                                    result-hash-table)
                                   ))
                               )) shouldbe-list-list)
                          )))
                    ))
                (set! test-label-index (1+ test-label-index))
                )) test-list)
           ))
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop max-num debug-flag)
  (begin
    (let ((sum-array
           (dynamic-addition-chains max-num))
          (sum 0))
      (begin
        (do ((ii 2 (1+ ii)))
            ((> ii max-num))
          (begin
            (let ((min-sum-list (array-ref sum-array ii)))
              (let ((first-list (car min-sum-list)))
                (let ((min-len (length first-list)))
                  (begin
                    (set! sum (+ sum min-len))

                    (if (equal? debug-flag #t)
                        (begin
                          (display
                           (ice-9-format:format
                            #f "    (~:d) min length = ~:d : "
                            ii min-len))
                          (display
                           (ice-9-format:format
                            #f "sum so far = ~:d~%"
                            sum
                            (timer-module:current-date-time-string)))
                          (force-output)

                          (for-each
                           (lambda (a-list)
                             (begin
                               (let ((exp-1 (list-ref a-list 0))
                                     (exp-2 (list-ref a-list 1))
                                     (exp-3 (list-ref a-list 2)))
                                 (begin
                                   (display
                                    (ice-9-format:format
                                     #f "    n^~:d x n^~:d = n^~:d~%"
                                     exp-1 exp-2 exp-3))
                                   ))
                               )) (reverse first-list))
                          (newline)
                          (force-output)
                          ))
                    ))
                ))
            ))

        (display
         (ice-9-format:format
          #f "Sum(m(k)) = ~:d, for 1 <= k <= ~:d~%"
          sum max-num))
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "The most naive way of computing "))
    (display
     (format #f "n^15 requires~%"))
    (display
     (format #f "fourteen multiplications:~%"))
    (newline)
    (display
     (format #f "    n x n x ... x n = n^15~%"))
    (newline)
    (display
     (format #f "But using a 'binary' method you "))
    (display
     (format #f "can compute it~%"))
    (display
     (format #f "in six multiplications:~%"))
    (newline)
    (display
     (format #f "    n x n = n^2~%"))
    (display
     (format #f "    n^2 x n^2 = n^4~%"))
    (display
     (format #f "    n^4 x n^4 = n^8~%"))
    (display
     (format #f "    n^8 x n^4 = n^12~%"))
    (display
     (format #f "    n^12 x n^2 = n^14~%"))
    (display
     (format #f "    n^14 x n = n^15~%"))
    (newline)
    (display
     (format #f "However it is yet possible to "))
    (display
     (format #f "compute it in~%"))
    (display
     (format #f "only five multiplications:~%"))
    (newline)
    (display
     (format #f "    n x n = n^2~%"))
    (display
     (format #f "    n^2 x n = n^3~%"))
    (display
     (format #f "    n^3 x n^3 = n^6~%"))
    (display
     (format #f "    n^6 x n^6 = n^12~%"))
    (display
     (format #f "    n^12 x n^3 = n^15~%"))
    (newline)
    (display
     (format #f "We shall define m(k) to be the "))
    (display
     (format #f "minimum number of~%"))
    (display
     (format #f "multiplications to compute nk; "))
    (display
     (format #f "for example~%"))
    (display
     (format #f "m(15) = 5.~%"))
    (newline)
    (display
     (format #f "For 1 <= k <= 200, find Sum(m(k)).~%"))
    (newline)
    (display
     (format #f "To understand this problem, see "))
    (display
     (format #f "the following article~%"))
    (display
     (format #f "on the shortest addition chains,~%"))
    (display
     (format #f "https://wwwhomes.uni-bielefeld.de/achim/addition_chain.html~%"))
    (newline)
    (display
     (format #f "The number of possible additions "))
    (display
     (format #f "(starting from~%"))
    (display
     (format #f "1 + 1 = 2), are too large as k "))
    (display
     (format #f "gets big. The key~%"))
    (display
     (format #f "to making this program go fast, "))
    (display
     (format #f "is to work backwards,~%"))
    (display
     (format #f "(starting from 10 + 10 = 20). For "))
    (display
     (format #f "example,~%"))
    (display
     (format #f "9 = 5+4 = 6+3 = 7+2 = 8+1, then you "))
    (display
     (format #f "need to check~%"))
    (display
     (format #f "a few possibilities. The next key is "))
    (display
     (format #f "to use a dynamic~%"))
    (display
     (format #f "programming approach, and finally, to "))
    (display
     (format #f "make sure that if~%"))
    (display
     (format #f "9=5+4, that the sequence of 5 contains "))
    (display
     (format #f "4, so that you~%"))
    (display
     (format #f "don't have to add extra additions to "))
    (display
     (format #f "make 4.~%"))
    (display
     (format #f "see https://projecteuler.net/problem=122~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((max-num 20)
          (debug-flag #t))
      (begin
        (sub-main-loop max-num debug-flag)
        ))

    (force-output)
    (newline)
    (let ((max-num 200)
          (debug-flag #f))
      (begin
        (sub-main-loop max-num debug-flag)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 122 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))


          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 128                                    ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated July 30, 2022                                ###
;;;###                                                       ###
;;;###  updated March 11, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### srfi-1 for fold function
(use-modules ((srfi srfi-1)
              :renamer (symbol-prefix-proc 'srfi-1:)))

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### prime-module for is-prime? function
(use-modules ((prime-module)
              :renamer (symbol-prefix-proc 'prime-module:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define-syntax check-element-0-macro
  (syntax-rules ()
    ((check-element-0-macro
      nn this-start-num this-last-elem
      outer-start-num outer-last-elem
      inner-start-num inner-last-elem
      last-tile pd-count target-count
      prime-array
      continue-loop-flag debug-flag)
     (begin
       (let ((diff-1-0 (- (1+ outer-start-num) this-start-num))
             (diff-2-0 (- outer-last-elem this-start-num))
             (diff-3-0 (- this-last-elem this-start-num)))
         (begin
           (if (and
                (prime-module:is-array-prime? diff-1-0 prime-array)
                (prime-module:is-array-prime? diff-2-0 prime-array)
                (prime-module:is-array-prime? diff-3-0 prime-array))
               (begin
                 (set! pd-count (1+ pd-count))
                 (set! last-tile this-start-num)
                 (if (equal? debug-flag #t)
                     (begin
                       (display
                        (format
                         #f "debug num=~a, layer=~a~%"
                         this-start-num nn))
                       (display
                        (format
                         #f "  neighbors=~a, ~a, ~a, ~a, ~a~%"
                         this-last-elem outer-start-num
                         outer-last-elem inner-start-num
                         inner-last-elem))
                       (display
                        (format
                         #f "  count = ~a~%" pd-count))
                       (force-output)
                       ))
                 (if (>= pd-count target-count)
                     (begin
                       (set! continue-loop-flag #f)
                       ))
                 ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
(define-syntax check-element-last-macro
  (syntax-rules ()
    ((check-element-last-macro
      nn this-start-num this-last-elem
      outer-start-num outer-last-elem
      inner-start-num inner-last-elem
      last-tile pd-count target-count
      prime-array
      continue-loop-flag debug-flag)
     (begin
       (let ((diff-1-1 (- this-last-elem inner-start-num))
             (diff-2-1 (- this-last-elem this-start-num))
             (diff-3-1
              (- (1- outer-last-elem) this-last-elem)))
         (begin
           (if (and
                (prime-module:is-array-prime? diff-1-1 prime-array)
                (prime-module:is-array-prime? diff-2-1 prime-array)
                (prime-module:is-array-prime? diff-3-1 prime-array))
               (begin
                 (set! pd-count (1+ pd-count))
                 (set! last-tile this-last-elem)
                 (if (equal? debug-flag #t)
                     (begin
                       (display
                        (format
                         #f "debug num=~a, layer=~a~%"
                         this-start-num nn))
                       (display
                        (format
                         #f "  neighbors=~a, ~a, ~a, ~a, ~a~%"
                         this-last-elem outer-start-num
                         outer-last-elem inner-start-num
                         inner-last-elem))
                       (display
                        (format
                         #f "  count = ~a~%" pd-count))
                       (force-output)
                       ))
                 (if (>= pd-count target-count)
                     (begin
                       (set! continue-loop-flag #f)
                       ))
                 ))
           ))
       ))
    ))

;;;#############################################################
;;;#############################################################
;;; just two hexagons to be checked for each layer
;;; offset=0 and offset=num-in-layer-1
(define (sub-main-loop
         target-count max-layers max-prime debug-flag)
  (begin
    (let ((pd-count 2)
          (last-tile 2)
          (prime-array
           (prime-module:make-prime-array max-prime))
          (continue-loop-flag #t)
          (inner-start-num 2)
          (inner-last-elem 7)
          (num-in-inner-layer 6))
      (begin
        ;;; start from the second layer
        (do ((nn 2 (1+ nn)))
            ((or (> nn max-layers)
                 (equal? continue-loop-flag #f)))
          (begin
            (let ((num-in-this-layer (+ 6 num-in-inner-layer))
                  (num-in-outer-layer (+ 12 num-in-inner-layer)))
              (let ((this-start-num (1+ inner-last-elem))
                    (this-last-elem
                     (+ inner-last-elem num-in-this-layer)))
                (let ((outer-start-num
                       (+ this-start-num num-in-this-layer)))
                  (let ((outer-last-elem
                         (+ this-last-elem num-in-outer-layer)))
                    (begin
                      (check-element-0-macro
                       nn this-start-num this-last-elem
                       outer-start-num outer-last-elem
                       inner-start-num inner-last-elem
                       last-tile pd-count target-count
                       prime-array
                       continue-loop-flag debug-flag)

                      (if (equal? continue-loop-flag #t)
                          (begin
                            (check-element-last-macro
                             nn this-start-num this-last-elem
                             outer-start-num outer-last-elem
                             inner-start-num inner-last-elem
                             last-tile pd-count target-count
                             prime-array
                             continue-loop-flag debug-flag)
                            ))

                      (set! inner-start-num this-start-num)
                      (set! inner-last-elem this-last-elem)
                      (set! num-in-inner-layer num-in-this-layer)
                      ))
                  )))
            ))

        (display
         (ice-9-format:format
          #f "~:d = the ~:d tile in the sequence~%"
          last-tile target-count))
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "A hexagonal tile with number 1 "))
    (display
     (format #f "is surrounded by~%"))
    (display
     (format #f "a ring of six hexagonal tiles, "))
    (display
     (format #f "starting at '12 o'clock'~%"))
    (display
     (format #f "and numbering the tiles 2 to 7 "))
    (display
     (format #f "in an~%"))
    (display
     (format #f "anti-clockwise direction.~%"))
    (newline)
    (display
     (format #f "New rings are added in the "))
    (display
     (format #f "same fashion, with~%"))
    (display
     (format #f "the next rings being numbered "))
    (display
     (format #f "8 to 19,~%"))
    (display
     (format #f "20 to 37, 38 to 61, and so on. "))
    (display
     (format #f "The diagram below~%"))
    (display
     (format #f "shows the first three rings.~%"))
    (newline)
    (display
     (format #f "By finding the difference between "))
    (display
     (format #f "tile n and each~%"))
    (display
     (format #f "its six neighbours we shall define "))
    (display
     (format #f "PD(n) to be the~%"))
    (display
     (format #f "number of those differences "))
    (display
     (format #f "which are prime.~%"))
    (newline)
    (display
     (format #f "For example, working clockwise "))
    (display
     (format #f "around tile 8 the~%"))
    (display
     (format #f "differences are 12, 29, 11, 6, 1, "))
    (display
     (format #f "and 13. So~%"))
    (display
     (format #f "PD(8) = 3.~%"))
    (newline)
    (display
     (format #f "In the same way, the differences "))
    (display
     (format #f "around tile 17 are~%"))
    (display
     (format #f "1, 17, 16, 1, 11, and 10, hence "))
    (display
     (format #f "PD(17) = 2.~%"))
    (newline)
    (display
     (format #f "It can be shown that the maximum "))
    (display
     (format #f "value of~%"))
    (display
     (format #f "PD(n) is 3.~%"))
    (newline)
    (display
     (format #f "If all of the tiles for which PD(n) = 3 "))
    (display
     (format #f "are listed~%"))
    (display
     (format #f "in ascending order to form a sequence, "))
    (display
     (format #f "the 10th tile~%"))
    (display
     (format #f "would be 271.~%"))
    (newline)
    (display
     (format #f "Find the 2000th tile in "))
    (display
     (format #f "this sequence.~%"))
    (newline)
    (display
     (format #f "This problem is easily solved by "))
    (display
     (format #f "noting that each~%"))
    (display
     (format #f "layer's additional layer is 6 greater "))
    (display
     (format #f "than the layer~%"))
    (display
     (format #f "before it. The first layer has 6 "))
    (display
     (format #f "hexagons, the second~%"))
    (display
     (format #f "has 12, the third "))
    (display
     (format #f "has 18,...~%"))
    (newline)
    (display
     (format #f "Every hexagon can be numbered "))
    (display
     (format #f "by label~%"))
    (display
     (format #f "l = Sum_k=1^(n-1)(6k) + offset "))
    (display
     (format #f "where the sum is~%"))
    (display
     (format #f "from 1 through n (the layer number), "))
    (display
     (format #f "and the offset~%"))
    (display
     (format #f "goes from 0 through 6n-1. There "))
    (display
     (format #f "are three types~%"))
    (display
     (format #f "of hexagons, middle elements, "))
    (display
     (format #f "corner elements,~%"))
    (display
     (format #f "and the special case "))
    (display
     (format #f "offset=6m-1.~%"))
    (display
     (format #f "Middle elements have 2 neighbors "))
    (display
     (format #f "on the interior~%"))
    (display
     (format #f "layer, 2 neighbors on the same "))
    (display
     (format #f "layer, and 2 neighbors~%"))
    (display
     (format #f "on the outer layer. Each neighbor "))
    (display
     (format #f "within the same layer~%"))
    (display
     (format #f "has a difference of 1, not a prime. "))
    (display
     (format #f "So of the~%"))
    (display
     (format #f "remaining four hexagon neighbors to "))
    (display
     (format #f "be considered, 3~%"))
    (display
     (format #f "must have prime differences, which is "))
    (display
     (format #f "impossible, since both~%"))
    (display
     (format #f "the inner layer and outer layer are "))
    (display
     (format #f "consecutive, so if~%"))
    (display
     (format #f "one of them has a prime difference, "))
    (display
     (format #f "the other won't.~%"))
    (display
     (format #f "This means we don't have to "))
    (display
     (format #f "consider middle elements~%"))
    (display
     (format #f "at all.~%"))
    (newline)
    (display
     (format #f "Corner elements have 1 neighbor "))
    (display
     (format #f "on the inner~%"))
    (display
     (format #f "layer, 2 neighbors in the same "))
    (display
     (format #f "layer, and 3~%"))
    (display
     (format #f "neighbors on the outer layer, and "))
    (display
     (format #f "there are 6 of~%"))
    (display
     (format #f "them per layer. There are two "))
    (display
     (format #f "types to consider.~%"))
    (display
     (format #f "The first type is has offset "))
    (display
     (format #f "greater than zero,~%"))
    (display
     (format #f "and the second has an offset "))
    (display
     (format #f "equal to zero. When~%"))
    (display
     (format #f "the offset is greater than zero, "))
    (display
     (format #f "the hexagon label~%"))
    (display
     (format #f "can be written as~%"))
    (display
     (format #f "l = 1 + Sum_k=1^(n-1)(6k) + n*m,~%"))
    (display
     (format #f "where n is the layer number, and m is "))
    (display
     (format #f "a counter from~%"))
    (display
     (format #f "1 through 5 (m=0 gives the offset=0 "))
    (display
     (format #f "case). The middle outside layer "))
    (display
     (format #f "element is given by the formula~%"))
    (display
     (format #f "l-out = 1 + Sum_k=1^(n)(6k) + (n+1)*m~%"))
    (display
     (format #f "(with the same offset m). Subtracting "))
    (display
     (format #f "the two gives,~%"))
    (display
     (format #f "diff = 6n + m, where n is the layer "))
    (display
     (format #f "number, and m~%"))
    (display
     (format #f "goes from 1 through 5. If this "))
    (display
     (format #f "difference is prime,~%"))
    (display
     (format #f "then the other two outer neighbors "))
    (display
     (format #f "cannot have a~%"))
    (display
     (format #f "prime difference, so at most this "))
    (display
     (format #f "possibility has~%"))
    (display
     (format #f "PD = 2. If 6n+m is even, then "))
    (display
     (format #f "6n+m-1 and 6n+m+1~%"))
    (display
     (format #f "must be prime if PD=3. However, "))
    (display
     (format #f "since twin primes~%"))
    (display
     (format #f "must have the form 6x-1/6x+1, we "))
    (display
     (format #f "see that it's~%"))
    (display
     (format #f "impossible for the two outer neighbors "))
    (display
     (format #f "to be twin~%"))
    (display
     (format #f "primes. The exception is the case "))
    (display
     (format #f "where m=0.~%"))
    (display
     (format #f "To see that that twin primes can only "))
    (display
     (format #f "have the form~%"))
    (display
     (format #f "6x-1/6x+1 see~%"))
    (display
     (format #f "http://numbers.computation.free.fr/Constants/Primes/twin.html~%"))
    (display
     (format #f "Finally, the special case of the "))
    (display
     (format #f "middle element where~%"))
    (display
     (format #f "the offset = 6m-1 must be checked, "))
    (display
     (format #f "since the same~%"))
    (display
     (format #f "layer may have a prime difference, "))
    (display
     (format #f "in addition to a~%"))
    (display
     (format #f "prime difference in the interior "))
    (display
     (format #f "and outer layers.~%"))
    (display
     (format #f "see https://projecteuler.net/problem=128~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((target-count 10)
          (max-prime 1000)
          (max-layers 1000)
          (debug-flag #t))
      (begin
        (sub-main-loop
         target-count max-layers
         max-prime debug-flag)
        ))

    (newline)
    (let ((target-count 2000)
          (max-prime 10000)
          (max-layers 100000)
          (debug-flag #f))
      (begin
        (sub-main-loop
         target-count max-layers
         max-prime debug-flag)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 128 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (load "prime-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

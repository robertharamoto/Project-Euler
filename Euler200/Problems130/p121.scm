#! /usr/bin/guile \
--no-auto-compile -e main -s
!#

;;;### This is free and unencumbered software released into the public domain.
;;;###
;;;### Anyone is free to copy, modify, publish, use, compile, sell, or
;;;### distribute this software, either in source code form or as a compiled
;;;### binary, for any purpose, commercial or non-commercial, and by any
;;;### means.
;;;###
;;;### In jurisdictions that recognize copyright laws, the author or authors
;;;### of this software dedicate any and all copyright interest in the
;;;### software to the public domain. We make this dedication for the benefit
;;;### of the public at large and to the detriment of our heirs and
;;;### successors. We intend this dedication to be an overt act of
;;;### relinquishment in perpetuity of all present and future rights to this
;;;### software under copyright law.
;;;###
;;;### THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;### EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;### MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
;;;### IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
;;;### OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
;;;### ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
;;;### OTHER DEALINGS IN THE SOFTWARE.
;;;###
;;;### For more information, please refer to <https://unlicense.org/>
;;;###

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  project euler 121                                    ###
;;;###                                                       ###
;;;###  last updated September 28, 2024                      ###
;;;###                                                       ###
;;;###  updated July 30, 2022                                ###
;;;###                                                       ###
;;;###  updated March 11, 2020                               ###
;;;###                                                       ###
;;;###  written by Robert Haramoto                           ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################

;;;### need to add this directory to the load-path
(for-each
 (lambda (adir)
   (begin
     (add-to-load-path adir)
     )) (list "."))

(set! %load-compiled-path
      (append %load-compiled-path (list ".")))

;;;#############################################################
;;;#############################################################
;;;### srfi-1 for fold function
(use-modules ((srfi srfi-1)
              :renamer (symbol-prefix-proc 'srfi-1:)))

;;;### ice-9 format for advanced format
(use-modules ((ice-9 format)
              :renamer (symbol-prefix-proc 'ice-9-format:)))

;;;### timer-module for time-code-macro function
(use-modules ((timer-module)
              :renamer (symbol-prefix-proc 'timer-module:)))

;;;### unittest2-module for test functions
(use-modules ((unittest2)
              :renamer (symbol-prefix-proc 'unittest2:)))

;;;#############################################################
;;;#############################################################
(define (factorial ii-num)
  (begin
    (cond
     ((<= ii-num 1)
      (begin
        1
        ))
     ((= ii-num 2)
      (begin
        2
        ))
     ((= ii-num 3)
      (begin
        6
        ))
     ((= ii-num 4)
      (begin
        24
        ))
     ((= ii-num 5)
      (begin
        120
        ))
     ((= ii-num 6)
      (begin
        720
        ))
     ((= ii-num 7)
      (begin
        5040
        ))
     ((= ii-num 8)
      (begin
        40320
        ))
     ((= ii-num 9)
      (begin
        362880
        ))
     (else
      (begin
        (* ii-num (factorial (- ii-num 1)))
        )))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-factorial-1 result-hash-table)
 (begin
   (let ((sub-name "test-factorial-1")
         (test-list
          (list
           (list 0 1) (list 1 1) (list 2 2)
           (list 3 6) (list 4 24) (list 5 120)
           (list 6 720) (list 7 5040)
           (list 8 40320) (list 9 362880)
           (list 10 3628800)
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((test-num (list-ref alist 0))
                  (shouldbe (list-ref alist 1)))
              (let ((result (factorial test-num)))
                (let ((err-1
                       (format
                        #f "~a : error (~a) : test-num=~a, "
                        sub-name test-label-index test-num))
                      (err-2
                       (format
                        #f "shouldbe=~a, result=~a~%"
                        shouldbe result)))
                  (begin
                    (unittest2:assert?
                     (equal? shouldbe result)
                     sub-name
                     (string-append err-1 err-2)
                     result-hash-table)
                    ))
                ))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
;;; if you pull a blue chip or red chip, replace the chip and add a red
(define (make-count-array num-turns)
  (begin
    (let ((count-array (make-array 0 (1+ num-turns))))
      (begin
      ;;; just 1 blue chip
        (array-set! count-array 1 0)

        (do ((ii 1 (1+ ii)))
            ((> ii num-turns))
          (begin
          ;;; number of red chips in the bag at turn ii is ii
            (do ((jj ii (1- jj)))
                ((<= jj 0))
              (begin
                (let ((jj-count
                       (array-ref count-array jj))
                      (jj-m1-count
                       (array-ref count-array (1- jj))))
                  (let ((next-count
                         (+ (* jj-m1-count ii) jj-count)))
                    (begin
                      (array-set! count-array next-count jj)
                      )))
                ))
            ))

        count-array
        ))
    ))

;;;#############################################################
;;;#############################################################
(unittest2:define-tests-macro
 (test-make-count-array-1 result-hash-table)
 (begin
   (let ((sub-name "test-make-count-array-1")
         (test-list
          (list
           (list 1
                 (list (list 0 1) (list 1 1)))
           (list 2
                 (list (list 0 1) (list 1 3) (list 2 2)))
           (list 3
                 (list
                  (list 0 1) (list 1 6) (list 2 11)
                  (list 3 6)))
           (list 4
                 (list
                  (list 0 1) (list 1 10) (list 2 35)
                  (list 3 50) (list 4 24)))
           ))
         (test-label-index 0))
     (begin
       (for-each
        (lambda (alist)
          (begin
            (let ((num-turns (list-ref alist 0))
                  (shouldbe-list-list (list-ref alist 1)))
              (let ((result-array (make-count-array num-turns))
                    (err-1
                     (format
                      #f "~a : error (~a) : num-turns=~a, "
                      sub-name test-label-index num-turns)))
                (begin
                  (for-each
                   (lambda (slist)
                     (begin
                       (let ((sindex (list-ref slist 0))
                             (svalue (list-ref slist 1)))
                         (let ((rvalue
                                (array-ref result-array sindex)))
                           (let ((err-2
                                  (format
                                   #f "[~a] shouldbe=~a, result=~a"
                                   sindex svalue rvalue)))
                             (begin
                               (unittest2:assert?
                                (equal? svalue rvalue)
                                sub-name
                                (string-append err-1 err-2)
                                result-hash-table)
                               ))
                           ))
                       )) shouldbe-list-list)
                  )))
            (set! test-label-index (1+ test-label-index))
            )) test-list)
       ))
   ))

;;;#############################################################
;;;#############################################################
(define (sub-main-loop num-turns)
  (begin
    (let ((sum 0)
          (total 0)
          (count-array (make-count-array num-turns))
          (min-blue (euclidean/ num-turns 2)))
      (begin
        (do ((ii 0 (1+ ii)))
            ((> ii min-blue))
          (begin
            (let ((count (array-ref count-array ii)))
              (begin
                (set! sum (+ sum count))
                ))
            ))
        (do ((ii 0 (1+ ii)))
            ((> ii num-turns))
          (begin
            (let ((count (array-ref count-array ii)))
              (begin
                (set! total (+ total count))
                ))
            ))

        (newline)
        (display
         (ice-9-format:format
          #f "the maximum prize fund = ~:d : number games won = ~:d~%"
          (euclidean/ total sum) sum))
        (display
         (ice-9-format:format
          #f "total = ~:d (single game with ~:d turns).~%"
          total num-turns))
        (force-output)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main-discussion)
  (begin
    (display
     (format #f "A bag contains one red disc and "))
    (display
     (format #f "one blue disc. In~%"))
    (display
     (format #f "a game of chance a player takes a "))
    (display
     (format #f "disc at random and~%"))
    (display
     (format #f "its colour is noted. After each "))
    (display
     (format #f "turn the disc is~%"))
    (display
     (format #f "returned to the bag, an extra red "))
    (display
     (format #f "disc is added, and~%"))
    (display
     (format #f "another disc is taken at random.~%"))
    (newline)
    (display
     (format #f "The player pays 1 to play and wins "))
    (display
     (format #f "if they have~%"))
    (display
     (format #f "taken more blue discs than red "))
    (display
     (format #f "discs at the end~%"))
    (display
     (format #f "of the game.~%"))
    (newline)
    (display
     (format #f "If the game is played for four "))
    (display
     (format #f "turns, the probability~%"))
    (display
     (format #f "of a player winning is exactly "))
    (display
     (format #f "11/120, and so the~%"))
    (display
     (format #f "maximum prize fund the banker "))
    (display
     (format #f "should allocate for~%"))
    (display
     (format #f "winning in this game would be 10 "))
    (display
     (format #f "before they would~%"))
    (display
     (format #f "expect to incur a loss. Note that "))
    (display
     (format #f "any payout will~%"))
    (display
     (format #f "be a whole number of pounds and "))
    (display
     (format #f "also includes the~%"))
    (display
     (format #f "original 1 paid to play the game, "))
    (display
     (format #f "so in the~%"))
    (display
     (format #f "example given the player actually "))
    (display
     (format #f "wins 9.~%"))
    (newline)
    (display
     (format #f "Find the maximum prize fund that should "))
    (display
     (format #f "be allocated to a~%"))
    (display
     (format #f "single game in which fifteen "))
    (display
     (format #f "turns are played.~%"))
    (newline)
    (display
     (format #f "The solution was found at~%"))
    (display
     (format #f "https://www.educative.io/answers/project-euler-disc-game-prize-fund~%"))
    (display
     (format #f "see https://projecteuler.net/problem=121~%"))
    ))

;;;#############################################################
;;;#############################################################
(define (main-loop)
  (begin
    (let ((num-turns 4))
      (begin
        (sub-main-loop num-turns)
        ))

    (newline)
    (let ((num-turns 15))
      (begin
        (sub-main-loop num-turns)
        ))
    ))

;;;#############################################################
;;;#############################################################
(define (main args)
  (begin
    (let ((version-string "2024-09-28"))
      (let ((title-string
             (format #f "Project Euler 121 (version ~a)"
                     version-string)))
        (begin
          ;;; run tests
          (display (format #f "running tests...~%"))
          (let ((debug-flag #f))
            (begin
              (timer-module:time-code-macro
               (begin
                 (load "timer-module-tests-1.scm")
                 (load "utils-module-tests-1.scm")
                 (unittest2:run-all-tests title-string debug-flag)
                 ))
              ))

          (newline)
          (display (format #f "~a~%" title-string))
          (newline)

          (main-discussion)

          (newline)
          (timer-module:time-code-macro
           (begin
             (main-loop)
             (newline)
             ))

          (newline)
          (display
           (format #f "~a~%"
                   (timer-module:current-date-time-string)))
          (force-output)
          )))
    ))

;;;#############################################################
;;;#############################################################

;;;#############################################################
;;;#############################################################
;;;###                                                       ###
;;;###  end of file                                          ###
;;;###                                                       ###
;;;#############################################################
;;;#############################################################
